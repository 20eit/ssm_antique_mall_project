package com.lhfeiyu.action.front.auction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.action.common.CreditMoney;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.User;
import com.lhfeiyu.po.base.BaseApply;
import com.lhfeiyu.po.base.BaseDict;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseApplyService;
import com.lhfeiyu.service.base.BaseDictService;
import com.lhfeiyu.service.base.BaseUserFundService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.thirdparty.wx.business.AuthAccess;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.DateFormat;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/ap")
public class AuctionProfessionAction {
	@Autowired
	private AuctionProfessionService apService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private BaseDictService dictService;
	@Autowired
	private AuctionInstService auctionInstService;
	@Autowired
	private BaseUserFundService ufService;
	@Autowired
	private BaseUserService userService;
	@Autowired
	private BaseApplyService applyService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="")
	public ModelAndView auctions(ModelMap modelMap,HttpServletRequest request
			,@RequestParam(required=false)Integer instId
			,@RequestParam(required=false) String r) {
		String path = Page.auctionProfession;
		try{
			JSONObject json = new JSONObject();
			apService.updateCheckProfessionNotice();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			modelMap.put("instId", instId);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/articleAgreement")
	public ModelAndView articleAgreement(ModelMap modelMap,HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.professionArticleAgreement;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/ap/page/articleAgreement";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/articleAgreement", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/typeChoose")
	public ModelAndView typeChoose(ModelMap modelMap, HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.auctionProfessionTypeChoose;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			modelMap.put("from", "ap");
			json.put("from", "ap");//标识来源：与微拍对应am
			modelMap.put("fromUrl", "/ap/page/typeChoose");
			json.put("fromUrl", "/ap/page/typeChoose");
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/typeChoose", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/creditMoney")
	public ModelAndView creditMoney(ModelMap modelMap, HttpServletRequest request,
			@RequestParam(required=false) String apSerial,
			@RequestParam(required=false) String r) {
		String path = Page.creditMoney;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/login";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = user.getId();
			if(Check.isNotNull(apSerial)){//专场保证金
				AuctionProfession profession = apService.selectBySerial(apSerial);
				if(null != profession){
					json.put("ap", profession);
					modelMap.put("ap", profession);
				}
				//检查保证金是否足够，不能则跳转到支付保证金页面
				if(profession.getTypeId() == 2){//需要保证金的专场
					BaseUserFund uf = ufService.selectUserFundByUserId(userId);
					if(null == uf){
						Result.failure(json, "您的资金账户存在异常，请联系客服人员", "uf_null");
						return new ModelAndView(path, modelMap);
					}
					modelMap = CreditMoney.checkCreditMoneyForAP(profession, modelMap, json, uf, path);
				}
			}
			modelMap.put("paramJson", json);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/addOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/addOrUpdate")
	public ModelAndView addOrUpdatePage(ModelMap modelMap, HttpServletRequest request,
			@RequestParam(required=false, defaultValue="1") Integer typeId,
			@RequestParam(required=false) String apSerial,
			@RequestParam(required=false) String r) {
		String path = Page.professionAddOrUpdate;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/login";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = user.getId();
			if(Check.isNotNull(apSerial)){
				AuctionProfession ap = apService.selectBySerial(apSerial);
				if(null != ap){
					typeId = ap.getTypeId();
					json.put("ap", ap);
					modelMap.put("ap", ap);
				}
			}
			Map<String, Object> map = CommonGenerator.getHashMap();
			/*//加载拍品类型下拉列表数据
			map.put("parentCode", "goods_type");
			String[] keys = {"value", "title", "parent"};
			JSONArray goodsTypeAry = dictService.getDictArrayByParentCode("goods_type", keys);
			json.put("typeId", typeId);
			json.put("goodsTypeAry", goodsTypeAry);*/
			
			//加载拍品类型下拉列表数据
			map.put("parentCode", "goods_type");
			String[] keys = {"value", "title", "parent"};
			//JSONArray goodsTypeAry = dictService.getDictArrayByParentCode("goods_type", keys);
			JSONArray array = new JSONArray();
			List<BaseDict> dictList = dictService.selectListByCondition(map);
			for(BaseDict d : dictList){
				JSONObject jsonItem = new JSONObject();
				jsonItem.put(keys[0], d.getCode());
				String codeName = d.getCodeName();
				if(Check.isNotNull(d.getDictValue())){
					codeName += "<span>"+d.getDictValue()+"</span>";
				}
				jsonItem.put(keys[1], codeName);
				jsonItem.put(keys[2], d.getParentCode());
				array.add(jsonItem);
			}
			json.put("goodsTypeAry", array);
			json.put("typeId", typeId);
			
			//检查保证金是否足够，不能则跳转到支付保证金页面
			if(typeId == 2){//需要保证金的专场
				BaseUserFund uf = ufService.selectUserFundByUserId(userId);
				if(null == uf){
					Result.failure(json, "您的资金账户存在异常，请联系客服人员", "uf_null");
					return new ModelAndView(path, modelMap);
				}
				BigDecimal creditMoney = uf.getOtherFund();
				BigDecimal avaliableMoney = uf.getAvaliableMoney();
				if(null == creditMoney)creditMoney = new BigDecimal(0);
				if(null == avaliableMoney)avaliableMoney = new BigDecimal(0);
				modelMap.put("from", "ap");
				json.put("from", "ap");//标识来源：与微拍对应am
				modelMap.put("fromUrl", "/ap/page/addOrUpdate?typeId=2");
				json.put("fromUrl", "/ap/page/addOrUpdate?typeId=2");
				double leftMoney = 0;
				if(creditMoney.doubleValue() == 0){
					leftMoney = 1000;
					modelMap.put("leftMoney", leftMoney);
					path = Page.creditMoney;
				}else if(creditMoney.doubleValue() < 1000){
					leftMoney = 1000 - creditMoney.doubleValue();
					modelMap.put("leftMoney", leftMoney);
					path = Page.creditMoney;
				}
				json.put("leftMoney", leftMoney);
				if(null == avaliableMoney || avaliableMoney.doubleValue() < leftMoney){
					modelMap.put("moneyLack", 1);
					json.put("moneyLack", 1);
				}
				if(Check.isNotNull(uf.getPayPassword())){
					modelMap.put("payPass", 1);
					json.put("payPass", 1);
					modelMap.put("avaliableMoney", avaliableMoney.doubleValue());
					json.put("avaliableMoney", avaliableMoney.doubleValue());
				}
			}
			
			//微信验证
			String ticket = AuthAccess.getWxDataFromProperty("ticket");//从Property文件中获取ticket,如果文件中没有，则会远程获取
			String url = "http://weipaike.net/ap/page/addOrUpdate";
			if(Check.isNotNull(apSerial)){
				url += "?apSerial="+apSerial;
			}else{
				if(Check.isNotNull(typeId)){
					url += "?typeId="+typeId;
				}
			}
			url = ActionUtil.buildPromoterUrl(url, r);
			//System.out.println("/ap/page/addOrUpdate url: "+url);
			modelMap = AuthAccess.getSign(modelMap, ticket, url);
			
			modelMap.put("paramJson", json);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/addOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	@RequestMapping(value="/page/applyAP")
	public ModelAndView applyAP(ModelMap modelMap, HttpServletRequest request,
			@RequestParam(required=false, defaultValue="1") Integer typeId,
			@RequestParam(required=false) String apSerial,
			@RequestParam(required=false) String r) {
		String path = Page.professionApply;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/login";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = user.getId();
			if(Check.isNotNull(apSerial)){
				AuctionProfession ap = apService.selectBySerial(apSerial);
				if(null != ap){
					typeId = ap.getTypeId();
					json.put("ap", ap);
					modelMap.put("ap", ap);
				}
			}
			
			Map<String, Object> map = CommonGenerator.getHashMap();
			/*//加载拍品类型下拉列表数据
			map.put("parentCode", "goods_type");
			String[] keys = {"value", "title", "parent"};
			JSONArray goodsTypeAry = dictService.getDictArrayByParentCode("goods_type", keys);
			json.put("typeId", typeId);
			json.put("goodsTypeAry", goodsTypeAry);*/
			
		
			
			//微信验证
			String ticket = AuthAccess.getWxDataFromProperty("ticket");//从Property文件中获取ticket,如果文件中没有，则会远程获取
			String url = "http://weipaike.net/ap/page/applyAP";
			if(Check.isNotNull(apSerial)){
				url += "?apSerial="+apSerial;
			}else{
				if(Check.isNotNull(typeId)){
					url += "?typeId="+typeId;
				}
			}
			url = ActionUtil.buildPromoterUrl(url, r);
			modelMap = AuthAccess.getSign(modelMap, ticket, url);
			
			modelMap.put("paramJson", json);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/addOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	
	@RequestMapping(value="/page/agAddOrUpdate")
	public ModelAndView agAddOrUpdatePage(ModelMap modelMap, HttpServletRequest request
			,@RequestParam String apSerial
			,@RequestParam(required=false) String r) {
		String path = Page.professionGoodsAddOrUpdate;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			AuctionProfession ap = apService.selectBySerial(apSerial);
			if(null == ap){
				return new ModelAndView(Page.auctionProfessionManage, modelMap);
			}
			json.put("ap", ap);
			String typeName = "普通专场";
			if(null != ap.getTypeId() && ap.getTypeId() == 2){
				typeName = "精品专场";
			}
			ap.setTypeName(typeName);
			json.put("apSerial", apSerial);
			modelMap.put("apSerial", apSerial);
			modelMap.put("ap", ap);
			modelMap.put("paramJson", json);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/agAddOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	/*@RequestMapping(value="/page/addOrUpdate")
	public ModelAndView addOrUpdatePage(ModelMap modelMap,HttpServletRequest request
			,@RequestParam Integer instId
			,@RequestParam(required=false) Integer professionId
			,@RequestParam(required=false) String r) {
		String path = Page.professionAddOrUpdate;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/ap/page/addOrUpdate?instId="+instId;
				if(null != professionId)jumpUrl += "&professionId="+professionId;
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			AuctionInst inst = aiService.selectByPrimaryKey(instId);
			int instUserId = inst.getUserId();
			int userId = user.getId();
			if(userId == instUserId){
				if(null != professionId){
					AuctionProfession ap = apService.selectByPrimaryKey(professionId);
					modelMap.put("ap", ap);
				}
				modelMap.put("manager", 1);
				modelMap.put("inst", inst);
			}else{
				path = Page.index;
			}
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/addOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}*/
	
	@RequestMapping(value="/page/search")
	public ModelAndView searchPage(ModelMap modelMap, HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.auctionProfessionSearch;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/search", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/manage")
	public ModelAndView managePage(ModelMap modelMap, HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.auctionProfessionManage;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/manage", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/page/edit")
	public ModelAndView editPage(ModelMap modelMap, HttpSession session, HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.professionAddOrUpdate;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/ap/page/edit";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("parentId",120);
			List<BaseDict> dictList = dictService.selectListByCondition(map);//产品类型
			if(dictList.size() > 0){
				modelMap.put("dictList", dictList);
			}
			map.clear();
			map.put("userId", user.getId());
			List<AuctionInst> auctionInstList = auctionInstService.selectListByCondition(map);
			if(null != auctionInstList){
				if(auctionInstList.size() > 0){
					modelMap.put("auctionInst", auctionInstList.get(0));
				}
			}
			/*List<AuctionQuickInst> auctionQuickInstList = auctionQuickInstService.selectListByCondition(map);
			if(null != auctionQuickInstList){
				if(auctionQuickInstList.size() > 0){
					modelMap.put("auctionQuickInst", auctionQuickInstList.get(0));
				}
			}*/
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/ap/page/edit", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdate", method=RequestMethod.POST)
	public JSONObject addOrUpdate(HttpServletRequest request
			,@ModelAttribute AuctionProfession ap
			,@RequestParam(required=false) String auctionGoodsAry
			,@RequestParam(required=false) String payPassword){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			String basePath = request.getServletContext().getRealPath("/");
			apService.addOrUpdateAuctionProfession(json, user, ap, auctionGoodsAry, basePath, payPassword);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/addOrUpdate", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addprofessionApply", method=RequestMethod.POST)
	public JSONObject addprofessionApply(HttpServletRequest request
			,@ModelAttribute AuctionProfession ap
			,@RequestParam(required=false) String auctionGoodsAry
			,@RequestParam(required=false) String payPassword){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			BaseUser baseUser = userService.selectByPrimaryKey(user.getId());
			BaseApply apply = new BaseApply();
			apply.setUserId(user.getId());
			apply.setAttr1(ap.getAuctionName());//机构名
			apply.setAttr2(ap.getDescription());//机构介绍
			apply.setAttr3(ap.getGoodsTypeCode());//机构电话
			apply.setMainStatus(1);
			apply.setFile1(ap.getPicPaths());//机构logo
			apply.setApplyType(1);
			Date date = new Date();
			apply.setApplyDate(date);
			apply.setCreatedAt(date);
			apply.setCreatedBy(baseUser.getUsername());
			
			applyService.insert(apply);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/addOrUpdate", json);
		}
		
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAGList", method=RequestMethod.POST)
	public JSONObject addOrUpdateAGList(HttpServletRequest request,
			@RequestParam String apSerial, 
			@RequestParam String auctionGoodsAry,
			@RequestParam String opt){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			if(Check.isNull(apSerial)){
				return Result.failure(json, "拍卖编号不能为空", "apSerial_null");
			}
			AuctionProfession ap = apService.selectBySerial(apSerial);
			if(null == ap){
				return Result.failure(json, "拍卖场次不存在", "ap_null");
			}
			apService.addOrUpdateAGList(json, user, null, auctionGoodsAry, ap.getId(), opt);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/addAuctionGoodsList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getList", method=RequestMethod.POST)
	public JSONObject getList(HttpServletRequest request) {
		List<AuctionProfession> auctionProfessionList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionProfessionList = apService.selectListByCondition(map);
			json.put("rows", auctionProfessionList);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/getList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getHaveBegun", method=RequestMethod.POST)
	public JSONObject getHaveBegun(HttpServletRequest request,
			@RequestParam Integer professionId) {
		JSONObject json = new JSONObject();
		try {
			//Map<String, Object> map = ActionUtil.getAllParam(request);
			AuctionProfession ap = apService.selectByPrimaryKey(professionId);
			Integer haveBegun = ap.getHaveBegun();
			if(null == haveBegun)haveBegun = 1;
			json.put("haveBegun", haveBegun);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/getHaveBegun", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyProfessionList", method=RequestMethod.POST)
	public JSONObject getMyProfessionList(HttpServletRequest request,
			@RequestParam(required = false) String show) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			map.put("agCount", 1);
			if(Check.isNotNull(show)){
				if("pre".equals(show)){
					map.put("notBegin", 1);
				}else if("ing".equals(show)){
					map.put("haveBegun", 2);
				}else if("done".equals(show)){
					map.put("haveBegun", 3);
				}
			}
			List<AuctionProfession> apList = apService.selectListByCondition(map);
			json.put("rows", apList);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/getMyProfessionList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/doDelete", method=RequestMethod.POST)
	public JSONObject doDelete(HttpServletRequest request, @RequestParam String serial){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			apService.doDelete(json, user, serial);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/doDelete", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/doSubmit", method=RequestMethod.POST)
	public JSONObject doSubmit(HttpServletRequest request, @RequestParam String serial){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			apService.doSubmit(json, user, serial);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/ap/doSubmit", json);
		}
		return Result.success(json);
	}
	
}
