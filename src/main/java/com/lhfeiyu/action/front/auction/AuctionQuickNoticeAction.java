/*package com.lhfeiyu.action.front.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionQuickNotice;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionQuickNoticeService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Result;

@Controller
public class AuctionQuickNoticeAction {
	@Autowired
	private AuctionQuickNoticeService aqnService;
	@Autowired
	private AuthCheckService authCheckService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/auctionQuickNotice")
	public ModelAndView auctionQuickNotice(ModelMap modelMap,HttpSession session,
			@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionQuickNotice;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/auctionQuickNotice";
				if(null != r)jumpUrl = "/auctionQuickNotice?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Integer userId = user.getId();
			String noticeTypeIds = aqnService.getMyAuctionQuickNotice(userId);
			if(Check.isNotNull(noticeTypeIds)){
				modelMap.put("noticeTypeIds", noticeTypeIds);
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍提醒设置页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getMyAuctionQuickNotice", method=RequestMethod.POST)
	public JSONObject getMyAuctionQuickNotice(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			String noticeTypeIds = aqnService.getMyAuctionQuickNotice(userId);
			if(Check.isNotNull(noticeTypeIds)){
				json.put("noticeTypeIds", noticeTypeIds);
			}
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载即时拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/aqNoticeAddOrUpdate", method=RequestMethod.POST)
	public JSONObject aqNoticeAddOrUpdate(@ModelAttribute AuctionQuickNotice aqn,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String,Object> map = new HashMap<String,Object>();
			Integer userId = user.getId();
			map.put("userId", userId);
			List<AuctionQuickNotice> aqnList = aqnService.selectListByCondition(map);
			if(Check.isNotNull(aqnList)){
				AuctionQuickNotice oldAQN = aqnList.get(0);
				oldAQN.setGoodsTypeIds(aqn.getGoodsTypeIds());
				aqnService.updateByPrimaryKey(oldAQN);
			}else{
				aqn.setId(null);
				aqn.setUserId(userId);
				aqn.setMainStatus(1);
				aqn.setCreatedAt(new Date());
				aqn.setCreatedBy(user.getUsername());
				aqn.setAttrStr(user.getPhone());//电话:可用户短信通知
				aqnService.insertSelective(aqn);
			}
			json.put("status", "success");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改即时拍开拍提醒出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	
	
}
*/