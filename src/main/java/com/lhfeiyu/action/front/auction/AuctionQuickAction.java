/*package com.lhfeiyu.action.front.auction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.AuctionQuickGoodsService;
import com.lhfeiyu.service.AuctionQuickInstService;
import com.lhfeiyu.service.AuctionQuickService;
import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.ShopService;
import com.lhfeiyu.service.base.DictService;
import com.lhfeiyu.service.base.GoodsService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.AuctionQuick;
import com.lhfeiyu.po.AuctionQuickGoods;
import com.lhfeiyu.po.AuctionQuickInst;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.AuctionMicroGoods;
import com.lhfeiyu.po.base.Dict;
import com.lhfeiyu.po.base.Goods;
import com.lhfeiyu.po.base.Shop;
import com.lhfeiyu.po.base.User;

@Controller
public class AuctionQuickAction {
	@Autowired
	private AuctionQuickService aqService;
	@Autowired
	private AuctionQuickInstService aqiService;
	@Autowired
	private AuctionQuickGoodsService aqgService;
	@Autowired
	private ShopService shopService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private AuctionProfessionService apService;
	@Autowired
	private AuctionInstService aiService;
	@Autowired
	private AuctionMicroService asService;
	@Autowired
	private DictService dictService;
	@Autowired
	private AuctionMicroGoodsService asgService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/aqIndex")
	public ModelAndView aqIndex(ModelMap modelMap,HttpSession session,
			@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionQuickIndex;
		path = Page.frontIndex;//TODO-tempHide-即时拍
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/aqIndex";
				if(null != r){
					jumpUrl = "/aqIndex?r="+r;
				}
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Integer userId = user.getId();
			Map<String, Object> map = new HashMap<String, Object>();//自动获取所有参数（查询条件）
			modelMap = aqService.getAuctionQuickGoodsNumByType(map, modelMap);
			map.clear();
			map.put("userId", userId);
			int count = aqiService.selectCountByCondition(map);
			if(count > 0)modelMap.put("aqi", 1);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍首页出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/aq")
	public ModelAndView auctionQuick(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false,value="aqId")Integer aqId
			,@RequestParam(required=false,value="typeId")Integer typeId
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionQuick;
		path = Page.frontIndex;//TODO-tempHide-即时拍
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/aq";
				if(null != aqId){
					if(null != r){
						jumpUrl = "/aq?aqId="+aqId+"&r="+r;
					}else{
						jumpUrl = "/aq?aqId="+aqId;
					}
				}else if(null != typeId){
					if(null != r){
						jumpUrl = "/aq?typeId="+typeId+"&r="+r;
					}else{
						jumpUrl = "/aq?typeId="+typeId;
					}
				}else{
					if(null != r){
						jumpUrl = "/aq?r="+r;
					}
				}
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			if(null == typeId)typeId = 129;
			String typeName = "杂珍";
			if(typeId == 121){
				typeName = "陶瓷";
			}else if(typeId == 122){
				typeName = "金石";
			}else if(typeId == 123){
				typeName = "竹木";
			}else if(typeId == 124){
				typeName = "玉器";
			}else if(typeId == 125){
				typeName = "珠宝";
			}else if(typeId == 126){
				typeName = "书画";
			}else if(typeId == 127){
				typeName = "钱证";
			}else if(typeId == 128){
				typeName = "文玩";
			}
			modelMap.put("typeId", typeId);
			modelMap.put("typeName", typeName);
			Map<String, Object> map = new HashMap<String, Object>();//自动获取所有参数（查询条件）
			map.put("notOver", 1);
			map.put("groupBy", "id");
			map.put("ascOrdesc", "ASC");
			map.put("typeId", typeId);
			//List<AuctionQuick> auctionQuickList = aqService.selectListByCondition(map);
			Integer availableCount = aqService.selectCountByCondition(map);
			map.clear();
			map.put("status", 3);//3:已经成交的
			Integer doneCount = aqgService.selectCountByCondition(map);
			map.clear();
			map.put("userId", user.getId());
			int aqiCount = aqiService.selectCountByCondition(map);
			
			if(aqiCount > 0){
				modelMap.put("aqi", 1);
			}
			
			modelMap.put("availableCount", availableCount);
			//modelMap.put("doneCount", doneCount);
			modelMap.put("aqId", aqId);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/myAuctionQuick")
	public ModelAndView myAuctionQuick(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myAuctionQuick;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/myAuctionQuick";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<AuctionQuickInst> aqiList = aqiService.selectListByCondition(map);
			if(null != aqiList && aqiList.size()>0){
				modelMap.put("inst", aqiList.get(0));
			}else{
				path = Page.auctionQuickInstApply;
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/aqAddOrUpdate")
	public ModelAndView aqAddOrUpdate(ModelMap modelMap,HttpSession session,
			@RequestParam(required=false) Integer instId,@RequestParam(required=false) Integer auctionId
			,@RequestParam(required=false) Integer professionId
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.aqAddOrUpdate;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				//String jumpUrl = "/aqAddOrUpdate?instId="+instId;
				//if(null != auctionId)jumpUrl += "&auctionId="+auctionId+"r="+r;
				String jumpUrl = "/goodsManageType";
				if(null != r) jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			int userId = user.getId();
			//Integer professionId = Integer.valueOf(request.getParameter("professionId"));
			if(null != professionId){//专场
				AuctionInst ainst = aiService.selectByPrimaryKey(instId);
				int ainstUserId = ainst.getUserId();
				if(userId == ainstUserId){
					if(null != professionId){
						AuctionProfession ap = apService.selectByPrimaryKey(professionId);
						modelMap.put("ap", ap);
					}
					modelMap.put("manager", 1);
					modelMap.put("ainst", ainst);
					modelMap.put("comeFrom", 1);
					modelMap.put("professionId", professionId);
				}else{
					path = Page.index;
				}
			}else {
				if(null != auctionId){//即时拍
					AuctionQuickInst aqinst = aqiService.selectByPrimaryKey(instId);
					int instUserId = aqinst.getUserId();
					if(userId == instUserId){
						if(null != auctionId){
							AuctionQuick aq = aqService.selectByPrimaryKey(auctionId);
							modelMap.put("aq", aq);
						}
						modelMap.put("manager", 1);
						modelMap.put("aqinst", aqinst);
						modelMap.put("auctionQuickId", auctionId);
						Map<String,Object> map = new HashMap<String,Object>();
						map.put("parentId",120);
						List<Dict> dictList = dictService.selectListByCondition(map);//产品类型
						if(dictList.size() > 0){
							modelMap.put("dictList", dictList);
						}
						modelMap.put("comeFrom", 2);
					}else{
						path = Page.index;
					}
				}else{
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("userId", userId);
					map.put("mainStatusNull", 1);
					List<AuctionMicro> asList = asService.selectListByCondition(map);
					if(null != asList && asList.size() > 0){
						AuctionMicro as = asList.get(0);
						modelMap.put("as", as);
						map.put("auctionId", as.getId());
						List<AuctionMicroGoods> asgList = asgService.selectListByCondition(map);//微拍
						if(null != asgList && asgList.size() > 0){
							AuctionMicroGoods asg = asgList.get(0);
							String pics = asg.getPicPaths();
							int idx = pics.indexOf(",");
							if(idx>0){
								asg.setPicPaths(pics.substring(0, idx));
							}else{
								asg.setPicPaths(pics);
							}
							modelMap.put("asg", asg);
						}
					}else{
						AuctionMicro as = new AuctionMicro();
						as.setUserId(userId);
						as.setCreatedAt(new Date());
						as.setCreatedBy(user.getUsername());
						as.setMainStatus(1);
						asService.insertSelective(as);
						modelMap.put("as", as);
					}
					modelMap.put("comeFrom", 3);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载修改拍卖页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickList", method=RequestMethod.POST)
	public JSONObject getAuctionQuickList(HttpServletRequest request) {
		List<AuctionQuick> auctionQuickList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			// TODO userFocusFansIds noticeUserId
			//map.put("orderBy", "prefessionStatus DESC,A.grade DESC");
			//map.put("orderByNotA", 1);
			//map.put("prefessionStatus", 1);
			//map.put("goodsCount", 1);
			//map.put("sc_order_self", "1");
			//sc_order格式例子：id___asc,created_at___desc,如果传递了request,则可自动获取分页参数
			User user = ActionUtil.checkSession4User(request.getSession());
			if(null != user){
				Integer userId = user.getId();
				map.put("userFocusFansId", userId);//检查是否关注
				map.put("noticeUserId", userId);//检查是否开拍提醒
			}else{
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			auctionQuickList = aqService.selectListByCondition(map);
			json.put("rows", auctionQuickList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载即时拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionQuick", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionQuick(@ModelAttribute AuctionQuick aq,HttpServletRequest request
			,@RequestParam String auctionQuickGoodsAry){
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String,Object> map = new HashMap<String,Object>();
			Integer aqId = aq.getId();
			Integer userId = user.getId();
			
			BigDecimal bailBd = aq.getBail();
			map.put("userId", userId);
			List<Shop> shopList = shopService.selectListByCondition(map);
			map.clear();
			Shop shop = null;
			if(Check.isNotNull(shopList)){
				shop = shopList.get(0);
				user.setShopId(shop.getId());
			}else{
				json.put("status", "failure");
				json.put("msg", "您的账号存在异常，请联系管理员");
				return Result.success(json);
			}
			if(null != bailBd && bailBd.doubleValue() > 0){
				Integer creditMoney = shop.getCreditMargin();
				if(null == creditMoney || creditMoney <= 0){
					json.put("status", "failure");
					json.put("msg", "您尚未交纳诚信保证金，是否交纳诚信保证金？");
					json.put("code", "jumpToMyShop");
					return Result.success(json);
				}else if(creditMoney.intValue() < bailBd.doubleValue()){
					json.put("status", "failure");
					json.put("msg", "您目前交纳的诚信保证金为："+creditMoney+"元，是否增加交纳诚信保证金？");
					json.put("code", "jumpToMyShop");
					return Result.success(json);
				}
			}
			if(null == aqId){//添加
				String msg = checkProfession(aq);
				if(null != msg){
					json.put("status", "failure");
					json.put("msg", msg);
					return Result.success(json);
				}
				map.put("userId", userId);
				map.put("notOver", 1);//检查有无还没结束的拍卖
				Integer count = aqService.selectCountByCondition(map);
				if(null != count && count>0){
					json.put("status", "failure");
					json.put("msg", "您还有未结束的拍卖，等拍卖结束后再添加吧");
					return Result.success(json);
				}
				String aqMsg = aqService.addAuctionQuick(user, aq, auctionQuickGoodsAry);
				json.put("status", "success");
				if(null != aqMsg){
					json.put("status", "failure");
					json.put("msg", aqMsg);
				}
				return Result.success(json);
			}else{//修改
				map.put("userId", userId);
				map.put("notOver", 1);//检查有无还没结束的拍卖
				AuctionQuick dbAQ = aqService.selectByPrimaryKey(aqId);
				if(null == dbAQ){
					json.put("status", "failure");
					json.put("msg", "该次拍卖已被删除");
					return Result.success(json);
				}
				Integer haveBegun = dbAQ.getHaveBegun();
				if(null != haveBegun){
					if(haveBegun == 3){
						json.put("status", "failure");
						json.put("msg", "该次拍卖已经结束，不能再进行修改");
						return Result.success(json);
					}
					if(haveBegun == 2){
						json.put("status", "failure");
						json.put("msg", "该次拍卖正在拍卖中，不能进行修改");
						return Result.success(json);
					}
				}
				String msg = checkProfession(aq);
				if(null != msg){
					json.put("status", "failure");
					json.put("msg", msg);
					return Result.success(json);
				}
				AuctionQuick newAQ = new AuctionQuick();
				newAQ.setId(aqId);
				newAQ.setAuctionName(aq.getAuctionName());
				newAQ.setBail(aq.getBail());
				newAQ.setSpreadPackets(aq.getSpreadPackets());
				newAQ.setPicPaths(aq.getPicPaths());
				newAQ.setStartTime(aq.getStartTime());
				newAQ.setUpdatedAt(new Date());
				newAQ.setUpdatedBy(user.getUsername());
				aqService.updateByPrimaryKeySelective(newAQ);
				aqService.addAuctionGoodsList(null, auctionQuickGoodsAry, user, aqId);
			}
			json.put("status", "success");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改关注出现异常_", json);
		}
		return Result.success(json);
	}
	
	private String checkProfession(AuctionQuick aq){
		String msg = null;
		Integer instId = aq.getInstId();
		//Date startDate = aq.getStartTime();
		if(null != instId){
			AuctionQuickInst aqi = aqiService.selectByPrimaryKey(instId);
			if(null == aqi){msg = "该拍场已不存在";}
		}else{
			msg = "所属即时拍不能为空";
		}
		if(null == startDate || (new Date().after(startDate))){
			msg = "拍卖开始日期不正确";
		}
		return msg;
	}
	
	@ResponseBody
	@RequestMapping(value="/loadAuctionQuickGoods", method=RequestMethod.POST)
	public JSONObject loadAuctionQuickGoods(HttpServletRequest request,@RequestParam Integer auctionId) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("auctionId", auctionId);
			String ids = aqgService.selectGoodsIdsOfQuick(map);
			if(null != ids && !"".equals(ids)){
				map.put("goodsIds", ids);
				List<Goods> goodsList = goodsService.selectListByCondition(map);
				json.put("rows", goodsList);
			}
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/loadQuickAuctionGoods", method=RequestMethod.POST)
	public JSONObject loadQuickAuctionGoods(HttpServletRequest request,@RequestParam Integer auctionId) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("auctionId", auctionId);
			map.put("aqgUserId", user.getId());
			List<AuctionQuickGoods> goodsList = aqgService.selectListByCondition(map);
			json.put("rows", goodsList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyAuctionQuickList", method=RequestMethod.POST)
	public JSONObject getMyAuctionQuickList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			map.put("widthGoodsPic", 1);
			List<AuctionQuick> aqList = aqService.selectListByCondition(map);
			if(null != aqList && aqList.size()>0){
				json.put("rows", aqList);
				json.put("status", "success");
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	
	
}
*/