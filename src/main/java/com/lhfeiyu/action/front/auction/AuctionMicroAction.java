package com.lhfeiyu.action.front.auction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.base.BaseTip;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.AuctionMicroOffers;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.service.AuctionMicroOffersService;
import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseCommonFundService;
import com.lhfeiyu.service.base.BaseGoodsService;
import com.lhfeiyu.service.base.BaseUserFundService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.DateFormat;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.tools.base.WxMessageFactory;
import com.lhfeiyu.vo.base.TimeUtil;

@Controller
public class AuctionMicroAction {
	@Autowired
	private AuctionMicroService amService;
	@Autowired
	private AuctionMicroOffersService asoService;
	@Autowired
	private BaseGoodsService goodsService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private BaseUserService userService;
	@Autowired
	private BaseUserFundService ufService;
	@Autowired
	BaseCommonFundService cfService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/am")
	public ModelAndView auctionMicro(ModelMap modelMap, HttpServletRequest request
			,@RequestParam(required=false)String amSerial
			,@RequestParam(required=false)String goodsName
			,@RequestParam(required=false)String userSerial
			,@RequestParam(required=false) String r) {
		String path = Page.auctionMicro;
		try{
			JSONObject paramJson = new JSONObject();
//			amService.updateCheckAuctionMicroOver(paramJson);
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if(null == sessionUser){
				String jumpUrl = "/am";
				if(null != amSerial){
					jumpUrl = "/am?amSerial="+amSerial;
					if(null != userSerial)jumpUrl += "&userSerial="+userSerial;
				}else if(null != userSerial){
					jumpUrl = "/am?userSerial="+userSerial;
				}
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			int userId = sessionUser.getId();
			BaseUser db_user = userService.selectByPrimaryKey(userId);
			if(Check.isNull(db_user.getPhone())){
				paramJson.put("bindPhone", 1);//未绑定手机号码
			}
			BaseUserFund userFund = ufService.selectUserFundByUserId(userId);
			/*BaseShop shop = shopService.selectShopByUserId(userId);
			Integer creditMoney = 0;
			if(null != shop){
				creditMoney = shop.getCreditMargin();
				if(null == creditMoney)creditMoney = 0;
			}*/
			
			BigDecimal creditMoney = userFund.getOtherFund();
			if(null == creditMoney){
				creditMoney = new BigDecimal(0);
			}
			paramJson.put("creditMoney", creditMoney.doubleValue());
			paramJson.put("amSerial", amSerial);
			paramJson.put("userSerial", userSerial);
			paramJson.put("userId", userId);
			paramJson.put("goodsName", goodsName);
			modelMap.put("paramJson", paramJson);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/am", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/am/page/manage")
	public ModelAndView amPageManage(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionMicroManage;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/auctionMicroManage";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = user.getId();
			/*Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			List<AuctionMicro> asList = amService.selectListByCondition(map);
			if(null != asList && asList.size()>0){
				modelMap.put("am", asList.get(0));
			}else{
				AuctionMicro am = new AuctionMicro();
				am.setUserId(user.getId());
				am.setCreatedAt(new Date());
				am.setCreatedBy(user.getUsername());
				am.setMainStatus(1);
				amService.insertSelective(am);
				modelMap.put("am", am);
			}*/
			modelMap.put("userId", userId);
			modelMap.put("paramJson", json);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载微拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/am/page/addOrUpdate")
	public ModelAndView addOrUpdatePage(ModelMap modelMap, HttpServletRequest request,
			@RequestParam(required=false) String amSerial,
			@RequestParam(required=false) Integer goodsId,
			@RequestParam(required=false) String r) {
		String path = Page.auctionMicroAddOrUpdate;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/login";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Map<String, Object> map = CommonGenerator.getHashMap();
			if(Check.isNotNull(amSerial)){
				map.clear();
				map.put("serial", amSerial);
				AuctionMicro am = amService.selectByCondition(map);
				modelMap.put("am", am);
				json.put("am", am);
				goodsId = am.getGoodsId();//同时加载藏品
			}
			if(null != goodsId){
				map.clear();
				map.put("id", goodsId);
				BaseGoods goods = goodsService.selectByCondition(map);
				modelMap.put("goods", goods);
				json.put("goods", goods);
			}
			modelMap.put("paramJson", json);
		}catch(Exception e){
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName()+"/am/page/addOrUpdate", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyGoodsAuctionMicro", method=RequestMethod.POST)
	public JSONObject getMyGoodsAuctionMicro(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = new HashMap<String, Object>();
			/*map.put("userId", userId);
			List<AuctionMicro> asList = aiService.selectListByCondition(map);
			if(null == asList || aiList.size()<=0){
				json.put("status", "failure");
				json.put("msg", "你还没有开通拍卖专场");
				return Result.success(json);
			}
			AuctionInst ai = aiList.get(0);*/
			//map.remove("userId");
			map.put("forAuctionAndSend", 1);//处于可拍卖状态和送拍状态
			map.put("for_userId", userId);
			map.put("moduleId", 4);//专场
			String asId = request.getParameter("asId");
			String catId = request.getParameter("catId");
			if(null != asId && !"".equals(asId)){
				map.put("for_linkId", asId);//拍卖机构ID
			}
			if(null != catId && !"".equals(catId)){
				map.put("catId", catId);//拍卖机构ID
			}
			map.put("orderBy", "main_status DESC, A.id");
			map.put("ascOrdesc", "DESC");
			List<BaseGoods> goodsList = goodsService.selectListByCondition(map);
			json.put("status", "success");
			json.put("rows", goodsList);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_getMyGoodsAuctionMicro_加载我的藏品数据列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionMicroList", method=RequestMethod.POST)
	public JSONObject getAuctionMicroList(HttpServletRequest request,
			@RequestParam(required=false)String amFlag) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			//map.put("orderBy", "prefessionStatus DESC,A.grade DESC");
			//map.put("orderByNotA", 1);
			//map.put("prefessionStatus", 1);
			//map.put("goodsCount", 1);
			//map.put("sc_order_self", "1");
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null != user){
				//map.put("checkFocus", 1);//检查是否关注
				//map.put("currentUserId", user.getId());
			}
			map.put("mainStatusNull", 1);//只加载正在拍卖中的
			
			//amFlag: focus newest nearend
			
			if(Check.isNotNull(amFlag)){
				if(amFlag.equals("newest")){
					map.put("orderBy", "end_time");
					map.put("ascOrdesc", "desc");
				}else if(amFlag.equals("nearend")){
					map.put("orderBy", "end_time");
					map.put("ascOrdesc", "asc");
				}
				map.put("fansUserId", user.getId());
			}
			
			List<AuctionMicro> auctionMicroList = amService.updateGetAuctionMicroList(map);
			json.put("rows", auctionMicroList);
			//amService.updateGetListAndCreaseVisitNum(json, map);//累加访问次数
			//json.put("sessionUser", user);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载微拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyAuctionMicroList", method=RequestMethod.POST)
	public JSONObject getMyAuctionMicroList(HttpServletRequest request,
			@RequestParam(required = false) String show) {
		List<AuctionMicro> auctionMicroList = null;
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = ActionUtil.getAllParam(request);
			
			//Map<String, Object> map = new HashMap<String,Object>();
//			map.put("userId", userId);
			//map.put("agCount", 1);
			if(Check.isNotNull(show)){
				if("ing".equals(show)){
					map.put("ing", 1);
				}else if("done".equals(show)){
					map.put("done", 1);
				}
			}
			//map.put("mainStatusNull", 1);//只加载正在拍卖中的
			auctionMicroList = amService.updateGetAuctionMicroList(map);
			for (AuctionMicro auctionMicro : auctionMicroList) {
				long remainSeconds = TimeUtil.getRemainSeconds(auctionMicro.getEndTime());
				auctionMicro.setRemainSeconds(remainSeconds);
			}
			json.put("rows", auctionMicroList);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载微拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	@ResponseBody
	@RequestMapping(value="/getAllAuctionMicroList", method=RequestMethod.POST)
	public JSONObject getAllAuctionMicroList(HttpServletRequest request,
			@RequestParam(required = false) String show) {
		List<AuctionMicro> auctionMicroList = null;
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = ActionUtil.getAllParam(request);
	
			//map.put("agCount", 1);
//			if(Check.isNotNull(show)){
//				if("ing".equals(show)){
//					map.put("ing", 1);
//				}else if("done".equals(show)){
//					map.put("done", 1);
//				}
//			}
			//map.put("mainStatusNull", 1);//只加载正在拍卖中的
			auctionMicroList = amService.updateGetAuctionMicroList(map);
			Integer total = amService.selectCountByCondition(map);
			
			json.put("total", total);
			json.put("rows", auctionMicroList);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载微拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/am/addOrUpdate", method=RequestMethod.POST)
	public JSONObject addOrUpdateAM(HttpServletRequest request,
			@ModelAttribute AuctionMicro am){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			amService.addUpdateAuctionMicro(json, user, am);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addAuctionMicroOffer", method=RequestMethod.POST)
	public JSONObject addAuctionMicroOffer(HttpServletRequest request,
			@ModelAttribute AuctionMicroOffers aso,
			@RequestParam(required=false) String promoteUserSerial) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				return Result.userSessionInvalid(json);
			}
			Date date = new Date();
			int userId = user.getId();
			String username = user.getUsername();
			String currentUserSerial = user.getSerial();
			Integer auctionId = aso.getAuctionId();
			if(null == auctionId){
				return Result.failure(json, "请选择拍卖", "auctionId_null");
			}
			BigDecimal offerPriceBD = aso.getOfferPrice();
			if(null == offerPriceBD){
				return Result.failure(json, "请输入您的出价", "offerPriceBD_null");
			}
			double offerPrice = offerPriceBD.doubleValue();
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", auctionId);
			AuctionMicro am = amService.selectByCondition(map);
			if(null == am){
				return Result.failure(json, "本次拍卖已经被删除", "am_null");
			}
			
			//TODO 验证前台输入的各类价格是否合法 - 检查保证金
			//相同代码：AuctionMicroAction-addAuctionMicroOffer，AuctionMicroService-addUpdateAuctionMicro
			//AuctionProfessionAction-addOrUpdateAuctionProfession
			BigDecimal bailBD = am.getBail();
			if(null != bailBD && bailBD.doubleValue() > 0){
				BaseUserFund uf = cfService.selectUserFundByUserId(userId);
				BigDecimal creditMoney = uf.getOtherFund();
				//Integer creditMoney = shop.getCreditMargin();
				if(null == creditMoney || creditMoney.doubleValue() <= 0){//诚信保证金为空
					json.put("moneyLack", bailBD.doubleValue());
					json.put("bail", bailBD.doubleValue());
					return Result.failure(json, BaseTip.msg_creditMoney_null, BaseTip.code_creditMoney_null);
				}else if(creditMoney.doubleValue() < bailBD.doubleValue()){//诚信保证金不足
					String tip = BaseTip.msg_creditMoney_lack.replace("@money@", creditMoney.toString());
					Double moneyLack = bailBD.doubleValue() - creditMoney.doubleValue();
					json.put("moneyLack", moneyLack);
					json.put("bail", bailBD.doubleValue());
					json.put("creditMoney", creditMoney.doubleValue());
					return Result.failure(json, tip, BaseTip.code_creditMoney_lack);
				}
			}
			
			Integer amUserId = am.getUserId();
			if(null != amUserId && amUserId.intValue() == userId){
				return Result.failure(json, "您不能对自己发布的拍品出价", "offer_self");
			}
			BigDecimal dbOfferPriceBD = am.getOfferPrice();
			Integer lastOfferUserId = am.getOfferUserId();
			if(null != dbOfferPriceBD){
				double db_offerPrice = dbOfferPriceBD.doubleValue();
				if(Check.integerEqual(lastOfferUserId, userId)){
					return Result.failure(json, "您的出价目前处于领先状态，不能出价", "offer_highest");
				}
				if(offerPrice <= db_offerPrice){
					return Result.failure(json, "您的出价低于当前出价", "offer_lower");
				}else{
					BigDecimal increaseBD = am.getIncreaseRangePrice();
					if(null != increaseBD){
						double increase = increaseBD.doubleValue();
						double lowestPrice = increase + db_offerPrice;
						if(offerPrice < lowestPrice){
							return Result.failure(json, "加价幅度不能小于"+increase, "offer_lower");
						}
					}
				}
			}
			BigDecimal buyoutBD = am.getBuyoutPrice();
			boolean finishFlag = false;
			if(null != buyoutBD){
				double buyoutPrice = buyoutBD.doubleValue();
				if(offerPrice >= buyoutPrice){
					offerPrice = buyoutPrice;//TODO 超过一口价，直接成交：生成订单，前往付款页面或当前页面付款
					finishFlag = true;
				}
			}
			
			AuctionMicro new_am = new AuctionMicro();//更新AuctionMicro
			new_am.setId(auctionId);
			new_am.setOfferPrice(offerPriceBD);
			new_am.setOfferUserId(userId);
			new_am.setOfferUsername(username);
			Integer times = am.getOfferTimes();
			if(null == times || times <= 0)times = 0;
			new_am.setOfferTimes(++times);//增加出价次数
			new_am.setOfferAt(date);
			new_am.setUpdatedAt(date);
			new_am.setUpdatedBy(username);
			if(currentUserSerial != promoteUserSerial){
				new_am.setPromoteUserSerial(promoteUserSerial);
			}
			amService.updateByPrimaryKeySelective(new_am);
			
			aso.setId(null);
			aso.setOfferUserId(userId);
			aso.setOfferUsername(username);
			aso.setOfferAt(date);
			aso.setGoodsId(am.getGoodsId());
			aso.setSerial(CommonGenerator.getSerialByDate("aso"));
			aso.setMainStatus(1);
			aso.setCreatedAt(date);
			aso.setCreatedBy(username);
			asoService.insert(aso);
			
			if(finishFlag){
				amService.finishAuctionMicro(json, auctionId);//结束拍卖，生成订单
			}

			//出价被超越通知
			// _@微信消息通知WxNotice@_
			BaseUser lastOfferUser = userService.selectByPrimaryKey(lastOfferUserId);
			if(null != lastOfferUser){
				String price = "￥"+String.valueOf(am.getOfferPrice());
				String openId = lastOfferUser.getWxOpenid();
				String remarkValue = null;
				Date endDate = am.getEndTime();
				if(null != endDate && endDate.after(date)){
					String end = DateFormat.daySdf.format(endDate);
					remarkValue = "本场拍卖结束时间："+end;
				}
				WxMessageFactory.auctionMicroOfferSurpass(openId, "感谢您参与拍卖，您的出价"+price+"已被超越！", am.getAuctionSerial(), am.getGoodsName(), remarkValue);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载微拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionMicroOffer", method=RequestMethod.POST)
	public JSONObject getAuctionMicroOffer(HttpServletRequest request, @RequestParam Integer auctionId) {
		JSONObject json = new JSONObject();
		try {
			if(Check.isLtZero(auctionId))return Result.failure(json, BaseTip.msg_param_null, BaseTip.code_param_null);
			//Map<String, Object> map = RequestUtil.getRequestParam(request);//自动获取所有参数（查询条件）
			Map<String, Object> map = CommonGenerator.getHashMap();
			map.put("orderBy", "created_at");
			map.put("ascOrdesc", "DESC");
			map.put("auctionId", auctionId);
			List<AuctionMicroOffers> asoList = asoService.selectListByCondition(map);
			if(Check.isNotNull(asoList)){
				json.put("auctionId", auctionId);
				json.put("asoList", asoList);
			}
			
			map.clear();
			map.put("id", auctionId);
			AuctionMicro am = amService.selectByCondition(map);
			json.put("am", am);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/getAuctionMicroOffer", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/finishAuctionMicro", method=RequestMethod.POST)
	public JSONObject finishAuctionMicro(@RequestParam Integer auctionId) {
		JSONObject json = new JSONObject();
		try {
			if(Check.isLtZero(auctionId))return Result.failure(json, BaseTip.msg_param_null, BaseTip.code_param_null);
			AuctionMicro am = amService.selectByPrimaryKey(auctionId);
			long remainSeconds = TimeUtil.getRemainSeconds(am.getEndTime());
			if(remainSeconds > 0){
				json.put("remainSeconds", remainSeconds);
			}else{
				amService.finishAuctionMicro(json, auctionId);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/finishAuctionMicroGoods", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/am/doDelete", method=RequestMethod.POST)
	public JSONObject doDelete(HttpServletRequest request, @RequestParam String serial){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			amService.doDelete(json, user, serial);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/am/doDelete", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/loadAuctionMicroGoods", method=RequestMethod.POST)
	public JSONObject loadAuctionMicroGoods(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			/*User user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				return Result.userSessionInvalid(json);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", user.getId());
			List<AuctionMicroGoods> asgList = asgService.selectListByCondition(map);//微拍
			if(null != asgList && asgList.size() > 0){
				//AuctionMicroGoods asg = asgList.get(0);
				json.put("rows", asgList);
			}*/
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载微拍藏品列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	/*@ResponseBody
	@RequestMapping(value="/deleteAsg", method=RequestMethod.POST)
	public JSONObject deleteAsg(HttpServletRequest request,@RequestParam Integer asgId){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			amService.deleteAuctionMicro(json, user, asgId);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_删除微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAsg", method=RequestMethod.POST)
	public JSONObject updateAsg(@ModelAttribute AuctionMicroGoods asg,
			HttpServletRequest request,@RequestParam Integer day){
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			amService.updateAuctionMicro(json, user, asg, day);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_修改微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addAsg", method=RequestMethod.POST)
	public JSONObject addOrUpdateAsg(@ModelAttribute AuctionMicroGoods asg,
			HttpServletRequest request,@RequestParam Integer day){
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			amService.addAuctionMicro(json, user, asg, day);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addBatchAsg", method=RequestMethod.POST)
	public JSONObject addBatchAsg(HttpServletRequest request,
			@RequestParam String auctionMicroGoodsAry,@RequestParam Integer day){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Calendar c = Calendar.getInstance();
			if(null != day && !"".equals(day)){
				c.add(Calendar.DATE, day);
			}else{
				c.add(Calendar.DATE, 15);
			}
			Date endTime = c.getTime();
			//asgService.addBatchAuctionMicroGoods(json, auctionMicroGoodsAry, endTime, user);
			Result.success(json, null, null);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_批量添加微拍出现异常_", json);
		}
		return Result.success(json);
	}*/
	
}
