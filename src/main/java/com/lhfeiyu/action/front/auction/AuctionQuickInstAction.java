/*package com.lhfeiyu.action.front.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionQuickInst;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionQuickInstService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Result;

@Controller
public class AuctionQuickInstAction {
	@Autowired
	private AuctionQuickInstService aqiService;
	@Autowired
	private AuthCheckService authCheckService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/auctionQuickInstApplyDesc")
	public ModelAndView auctionQuickInstApplyDesc(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionQuickInstApplyDesc;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			if(null == sessionUser){
				String jumpUrl = "/auctionQuickInstApplyDesc";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载申请即时拍说明页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	@RequestMapping(value="/auctionQuickInstApply")
	public ModelAndView auctionQuickInstApply(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.auctionQuickInstApply;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/auctionQuickInstApply";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<AuctionQuickInst> aiList = aqiService.selectListByCondition(map);
			if(null != aiList && aiList.size()>0){
				modelMap.put("inst", aiList.get(0));
				path = Page.myAuctionQuick;
			}
			modelMap.put("username", user.getUsername());
			modelMap.put("phone", user.getPhone());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍申请页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/myAuctionQuickInst")
	public ModelAndView myAuctionQuickInst(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myAuctionQuick;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/myAuctionQuickInst";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<AuctionQuickInst> aiList = aqiService.selectListByCondition(map);
			if(null != aiList && aiList.size()>0){
				modelMap.put("inst", aiList.get(0));
			}else{
				modelMap.put("username", user.getUsername());
				modelMap.put("phone", user.getPhone());
				path = Page.auctionQuickInstApply;
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickInstList", method=RequestMethod.POST)
	public JSONObject getAuctionQuickInstList(HttpServletRequest request) {
		List<AuctionQuickInst> auctionQuickInstList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			map.put("orderBy", "prefessionStatus DESC,A.grade DESC");
			map.put("orderByNotA", 1);
			map.put("prefessionStatus", 1);
			map.put("goodsCount", 1);
			map.put("sc_order_self", "1");
			auctionQuickInstList = aqiService.selectListByCondition(map);
			json.put("rows", auctionQuickInstList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyAuctionQuickInst", method=RequestMethod.POST)
	public JSONObject getMyAuctionQuickInst(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<AuctionQuickInst> aiList = aqiService.selectListByCondition(map);
			if(null != aiList && aiList.size()>0){
				json.put("auctionQuickInst", aiList.get(0));
				json.put("status", "success");
			}else{
				json.put("auctionQuickInst", aiList.get(0));
				json.put("status", "failure");
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickInst", method=RequestMethod.POST)
	public JSONObject updateAuctionQuickInst(@ModelAttribute AuctionQuickInst auctionQuickInst,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer instId = auctionQuickInst.getId();
			if(null != instId){
				AuctionQuickInst inst = aqiService.selectByPrimaryKey(instId);
				int instUserId = inst.getUserId();
				int userId = user.getId();
				if(userId == instUserId){
					AuctionQuickInst aqi = new AuctionQuickInst();
					aqi.setId(auctionQuickInst.getId());
					aqi.setName(auctionQuickInst.getName());
					aqi.setAddress(auctionQuickInst.getAddress());
					aqi.setTel(auctionQuickInst.getTel());
					aqi.setPicPaths(auctionQuickInst.getPicPaths());
					aqi.setIntroduce(auctionQuickInst.getIntroduce());
					aqi.setUpdatedAt(new Date());
					aqi.setUpdatedBy(user.getUsername());
					aqiService.updateByPrimaryKeySelective(aqi);
					json.put("status", "success");
				}else{
					json.put("status", "failure");
					json.put("msg","您没有该权限");

				}
			}else{
				json.put("status", "failure");
				json.put("msg","无法识别该机构");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_修改机构信息出现异常_", json);
		}
		return Result.success(json);
	}

}
*/