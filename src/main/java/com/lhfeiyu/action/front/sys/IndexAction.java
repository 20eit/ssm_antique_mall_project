package com.lhfeiyu.action.front.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseForumArticle;
import com.lhfeiyu.po.base.BasePicture;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseForumArticleService;
import com.lhfeiyu.service.base.BaseForumService;
import com.lhfeiyu.service.base.BaseGoodsService;
import com.lhfeiyu.service.base.BasePictureService;
import com.lhfeiyu.tools.MapUtil;

@Controller
public class IndexAction {
	@Autowired
	private BasePictureService pictureService;
	@Autowired
	private BaseForumArticleService articleService;
	@Autowired
	private BaseForumService forumService;
	@Autowired
	private AuctionProfessionService apService;
	@Autowired
	private BaseGoodsService goodsService;
	@Autowired
	private AuthCheckService authCheckService;

	private static Logger logger = Logger.getLogger("R");

	/** 主页面 */
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET) // ,"/index"
	public ModelAndView index(ModelMap modelMap, HttpServletRequest request, 
			@RequestParam(required = false) String path,
			@RequestParam(required = false) String r) {
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			// 加载首页Banner图
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("typeId", 88);// 88:首页Banner图
			map.put("count", 3);
			map.put("start", 0);
			map.put("orderBy", "created_at");
			map.put("ascOrdesc", "DESC");
			List<BasePicture> bannerList = pictureService.selectListByCondition(map);
			modelMap.put("bannerList", bannerList);
			// TODO tempHide
			// 加载活动资讯
			map.clear();
			// map.put("orderBy", "moduleId");
			// map.put("ascOrdesc", "desc");
			map.put("isTop", 1);
			map.put("count", 3);
			map.put("start", 0);
			map.put("orderBy", "created_at");
			map.put("ascOrdesc", "DESC");
			List<BaseForumArticle> articleList = articleService.selectListByCondition(map);
			modelMap.put("articleList", articleList);
			/*
			 * //加载推荐店铺 map.clear(); map.put("orderBy",
			 * "credit_margin DESC,u.is_real_auth DESC,A.created_at");
			 * map.put("ascOrdesc", "desc");
			 * map.put("creditMarginISNOTNULLAndIsRealAuth", 1);
			 * map.put("count", 6); map.put("start", 0); List<Shop> shopList =
			 * shopService.selectListByCondition(map); modelMap.put("shopList", shopList);
			 */
			// 加载推荐论坛
			/*
			 * map.clear(); map.put("orderBy", "visit_num");
			 * map.put("ascOrdesc", "DESC"); map.put("count", 6);
			 * map.put("start", 0); List<Forum> forumList =
			 * forumService.selectListByCondition(map);
			 * modelMap.put("forumList", forumList);
			 */
			// 加载推荐专场
			map.clear();
			map.put("orderBy", "sequence");
			map.put("ascOrdesc", "asc");
			map.put("notOver", 1);
			List<AuctionProfession> apList = apService.selectListByCondition(map);
			if (apList.size() > 0) {
				for (AuctionProfession ap : apList) {
					if (null != ap.getPicPath()) {
						String picPaths[] = ap.getPicPath().split(",");
						if (picPaths.length > 0) {
							List<String> picPathList = new ArrayList<String>(picPaths.length);
							for (int i = 0; i < picPaths.length; i++) {
								picPathList.add(picPaths[i]);
							}
							ap.setApPicPaths(picPathList);
						}
					}
				}
				modelMap.put("apList", apList);
			}
			// 加载推荐藏品
			map.clear();
//			map.put("orderBy", "sequence");
//			map.put("ascOrdesc", "asc");
//			map.put("count", 4);
//			map.put("start", 0);
//			List<BaseGoods> goodsList = goodsService.selectListByCondition(map);
//			modelMap.put("goodsList", goodsList);
			MapUtil.printMap(modelMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("LH_ERROR_首页加载异常_" + e.getMessage());
		}
		return new ModelAndView(Page.frontIndex, modelMap);
	}
	
	
	
}