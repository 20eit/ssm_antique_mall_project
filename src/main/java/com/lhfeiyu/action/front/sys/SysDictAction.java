package com.lhfeiyu.action.front.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.po.base.BaseSysDict;
import com.lhfeiyu.service.base.BaseSysDictService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
public class SysDictAction {
	@Autowired
	private BaseSysDictService sysDictService;
	private static Logger logger = Logger.getLogger("R");
	
	
	
	@ResponseBody
	@RequestMapping(value="/getSysDictList", method=RequestMethod.POST)
	public JSONObject getSysDictList(HttpServletRequest request) {
		List<BaseSysDict> sysDictList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			sysDictList = sysDictService.selectListByCondition(map);
			Integer total = sysDictService.selectCountByCondition(map);
			json.put("rows", sysDictList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载系统字典列表出现异常_", json);
		}
		return Result.success(json);
	}
	
}
