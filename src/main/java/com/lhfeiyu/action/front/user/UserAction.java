package com.lhfeiyu.action.front.user;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AntiqueCity;
import com.lhfeiyu.po.AuctionGoods;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseFans;
import com.lhfeiyu.po.base.BaseForumMember;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseProvinceCityArea;
import com.lhfeiyu.po.base.BaseShop;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.po.base.BaseUserRelation;
import com.lhfeiyu.service.AntiqueCityService;
import com.lhfeiyu.service.AuctionGoodsService;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.CreditService;
import com.lhfeiyu.service.GoodsService;
import com.lhfeiyu.service.UserJobService;
import com.lhfeiyu.service.base.BaseFansService;
import com.lhfeiyu.service.base.BaseForumMemberService;
import com.lhfeiyu.service.base.BaseProvinceCityAreaService;
import com.lhfeiyu.service.base.BaseShopService;
import com.lhfeiyu.service.base.BaseUserFundService;
import com.lhfeiyu.service.base.BaseUserRelationService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.MapUtil;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.DateFormat;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.tools.base.WxMessageFactory;
import com.lhfeiyu.util.base.Md5Util;
import com.lhfeiyu.util.base.SendMsgUtil;
import com.lhfeiyu.util.base.UploadUtil;

@Controller
public class UserAction {
	@Autowired
	private BaseUserService userService;
	@Autowired
	private BaseShopService shopService;
	@Autowired
	private BaseFansService fansService;
	@Autowired
	private BaseProvinceCityAreaService provinceCityAreaService;
	@Autowired
	private AntiqueCityService antiqueCityService;
	@Autowired
	private BaseUserFundService userFundService;
	@Autowired
	private BaseUserRelationService userRelationService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private AuctionMicroService auctionMicroService;
	@Autowired
	private AuctionProfessionService auctionProfessionService;
	@Autowired
	private AuctionGoodsService auctionGoodsService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private BaseForumMemberService forumMemberService;
	@Autowired
	private CreditService creditService;
	@Autowired
	private AuctionInstService auctionInstService;
	//@Autowired
	//private UserJobService userJobService;
	
	private static Logger logger = Logger.getLogger("R");

	/** 用户主页 */
	@RequestMapping(value = "/user/{serial}")
	public ModelAndView user(ModelMap modelMap, HttpServletRequest request, @PathVariable String serial, @RequestParam(required = false) String r) {
		String path = Page.userCenterShop;
		//userJobService.cleanUserNum();
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/index";
				new ModelAndView(jumpUrl, modelMap);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("serial", serial);
			BaseUser user = userService.selectByCondition(map);
			Integer id = user.getId();
			map.put("id", id);
			modelMap.put("user", user);// 用户
			if (null !=user.getIsEnterpriseAuth() && -1 == user.getIsEnterpriseAuth()) {
				modelMap.put("enisRealAuth", "-1");
			} else {
				modelMap.put("enisRealAuth", "1");
			}
			if (null == user.getIsRealAuth()) {
				modelMap.put("isRealAuth", "1");
			} else {
				modelMap.put("isRealAuth", "-1");
			}
			map.clear();
			map.put("userId", id);
			modelMap.put("focusCount", fansService.selectCountByCondition(map));// 粉丝数
			map.clear();
			map.put("fansId", id);
			modelMap.put("fansCount", fansService.selectCountByCondition(map));// 关注数
			map.clear();
			map.put("userId", id);
			modelMap.put("userFun", userFundService.selectByCondition(map));
			// 是否关注
			map.clear();
			map.put("fansId", sessionUser.getId());
			map.put("userId", id);
			if (id == sessionUser.getId()) {
				modelMap.put("isFans", -1);
			} else {
				modelMap.put("isFans", fansService.selectCountByCondition(map));
				Integer num = 0;
				if (null != user.getAttrInt()) {
					num = user.getAttrInt();
				} 
				/*if (new Date().getDate() != user.getUpdatedAt().getDate()) {
					user.setAttrInt(0);
				} else {
				}*/
				user.setAttrInt(++num);
				userService.updateByPrimaryKey(user);
			}
			map.clear();
			map.put("userId", id);
			modelMap.put("credit", creditService.selectByCondition(map));
			MapUtil.printMap(modelMap);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/index", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 用户主页 */
	@RequestMapping(value = "/userCenter/{serial}")
	public ModelAndView userCenter(ModelMap modelMap, HttpServletRequest request, @PathVariable String serial, @RequestParam(required = false) String r) {
		String path = Page.userCenterUser;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/index";
				new ModelAndView(jumpUrl, modelMap);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("serial", serial);
			BaseUser user = userService.selectByCondition(map);
			Integer id = user.getId();
			map.put("id", id);
			modelMap.put("user", user);// 用户
			if (null == user.getIsRealAuth()) {
				modelMap.put("isRealAuth", "1");
			} else {
				modelMap.put("isRealAuth", "-1");
			}
			if (null != user.getIsEnterpriseAuth() && -1 == user.getIsEnterpriseAuth()) {
				modelMap.put("enisRealAuth", "-1");
			} else {
				modelMap.put("enisRealAuth", "1");
			}
			map.clear();
			map.put("userId", id);
			modelMap.put("focusCount", fansService.selectCountByCondition(map));// 粉丝数
			map.clear();
			map.put("fansId", id);
			modelMap.put("fansCount", fansService.selectCountByCondition(map));// 关注数
			map.clear();
			map.put("userId", id);
			modelMap.put("userFun", userFundService.selectByCondition(map));
			// 是否关注
			map.clear();
			map.put("fansId", sessionUser.getId());
			map.put("userId", id);
			if (id == sessionUser.getId()) {
				modelMap.put("isFans", -1);
			} else {
				modelMap.put("isFans", fansService.selectCountByCondition(map));
			}
			map.clear();
			map.put("userId", id);
			modelMap.put("credit", creditService.selectByCondition(map));

			MapUtil.printMap(modelMap);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/index", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	/**
	 * 取消关注
	 */
	@ResponseBody
	@RequestMapping(value = "/cancelFans", method = RequestMethod.POST)
	public JSONObject cancelFans(HttpServletRequest request, @RequestParam(value = "fansId") String fansId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == sessionUser) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			Integer userId = sessionUser.getId();
			fansService.deleteByCondition("1=1 AND user_id = " + fansId + " AND fans_id = " + userId);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取用户微拍商品出现异常_", json);
		}
		return Result.success(json, "已取消");
	}
	/**
	 * 添加关注
	 */
	@ResponseBody
	@RequestMapping(value = "/joinFans", method = RequestMethod.POST)
	public JSONObject joinFans(HttpServletRequest request, @RequestParam(value = "fansId") Integer fansId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == sessionUser) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			Integer userId = sessionUser.getId();
			// fansService.deleteByCondition("1=1 AND user_id = "+userId + " AND
			// fans_id = " + fansId);
			BaseFans fans = new BaseFans();
			if(fansId == userId){
				return Result.success(json, "不能关注你自己!!");
			}
			fans.setUserId(fansId);
			fans.setFansId(userId);
			fansService.insert(fans);
		
			BaseUser fansUser = userService.selectByPrimaryKey(userId);
			BaseUser user = userService.selectByPrimaryKey(fansId);
			/*发送微信通知*/
			String openId = user.getWxOpenid();
			String content = fansUser.getUsername();
			String money = fansUser.getSerial();
			String dates = DateFormat.format(new Date(), DateFormat.MINUTES_FORMAT);
			String timeStr = dates;
			String userSerial = fansUser.getSerial();
			WxMessageFactory.fansJoinSuccess(openId, content, money, timeStr,userSerial);
			
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取用户微拍商品出现异常_", json);
		}
		return Result.success(json, "已关注");
	}
	/**
	 * 获取微拍商品
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsMicro", method = RequestMethod.POST)
	public JSONObject getGoodsMicro(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == sessionUser) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			AuctionMicro micro = auctionMicroService.selectByCondition(map);
			if (null != micro) {
				BaseGoods goodsMicro = goodsService.selectByPrimaryKey(micro.getGoodsId());
				json.put("rows", goodsMicro);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取用户微拍商品出现异常_", json);
		}
		return Result.success(json);
	}
	/**
	 * 获取专场商品
	 */
	@ResponseBody
	@RequestMapping(value = "/getGoodsAuction", method = RequestMethod.POST)
	public JSONObject getGoodsAuction(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == sessionUser) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			AuctionGoods auctionGoods = auctionGoodsService.selectByCondition(map);
			if (null != auctionGoods) {
				BaseGoods goodsAuction = goodsService.selectByPrimaryKey(auctionGoods.getGoodsId());
				json.put("rows", goodsAuction);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取用户微拍商品出现异常_", json);
		}
		return Result.success(json);
	}

	@RequestMapping(value = "/user")
	public ModelAndView user(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.userIndex;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/user";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			// int userId = sessionUser.getId();
			Map<String, Object> map = CommonGenerator.getHashMap();
			map.put("id", sessionUser.getId());
			map.put("uf_money", "1");
			map.put("nowOrderCount", "1");
			BaseUser user = userService.selectByCondition(map);
			if (null == user.getNowShopPrice()) {
				user.setNowShopPrice(BigDecimal.valueOf(0.00));
			}
			if (null == user.getAttrInt()) {
				user.setAttrInt(0);
			}
			//System.out.println(user);
			modelMap.put("user", user);
			json.put("user", user);
			json.put("paramJson", json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/user", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@RequestMapping(value = "/userMerchantsIndex")
	public ModelAndView userMerchants(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.userMerchantsIndex;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/user";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			// int userId = sessionUser.getId();
			Map<String, Object> map = CommonGenerator.getHashMap();
			map.put("id", sessionUser.getId());
			map.put("uf_money", "1");
			map.put("nowOrderCount", "1");
			BaseUser user = userService.selectByCondition(map);
			if (null == user.getNowShopPrice()) {
				user.setNowShopPrice(BigDecimal.valueOf(0.00));
			}
			if (null == user.getAttrInt()) {
				user.setAttrInt(0);
			}
			map.clear();
			map.put("userId", sessionUser.getId());
			AuctionInst aProfession = auctionInstService.selectByCondition(map);
			if(null == aProfession){
				modelMap.put("isNotAp", "1");
			}
			modelMap.put("user", user);
			json.put("user", user);
			json.put("paramJson", json);
			
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/user", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 个人信息 */
	@RequestMapping(value = "/userInfo")
	public ModelAndView userInfo(ModelMap modelMap, @RequestParam(required = false) String r, HttpServletRequest request) {
		String path = Page.personInformation;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/userInfo";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = sessionUser.getId();
			if (null != userId && !"".equals(userId)) {
				BaseUser user = userService.selectByPrimaryKey(userId);
				if (null == user.getPhone() || "".equals(user.getPhone())) {
					modelMap.put("noPhone", "noPhone");
				}
				String passwd = Md5Util.encrypt("888888");
				if (passwd == user.getPassword() || passwd.equals(user.getPassword())) {
					modelMap.put("noEditPassword", "noEditPassword");
				}
				modelMap.put("user", user);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userId", sessionUser.getId());
				List<BaseFans> focusList = fansService.selectListByCondition(map);
				List<BaseUserFund> userFundList = userFundService.selectListByCondition(map);
				if (userFundList.size() > 0) {
					BaseUserFund userFund = userFundList.get(0);
					modelMap.put("userFund", userFund);
					if (null == userFund.getPayPassword() || "".equals(userFund.getPayPassword())) {
						modelMap.put("noPayPassword", "noPayPassword");
					}
				}
				modelMap.put("focusCount", focusList.size());
				map.clear();
				map.put("fansId", userId);
				List<BaseFans> fansList = fansService.selectListByCondition(map);
				modelMap.put("fansCount", fansList.size());
				map.clear();
				map.put("userId", userId);
				map.put("withGoods", 1);
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				modelMap.put("shopList", shopList);
				map.clear();
				map.put("userId", userId);
				List<BaseForumMember> ForumMemberList = forumMemberService.selectListByCondition(map);
				modelMap.put("ForumMemberList", ForumMemberList);
				modelMap.put("userId", sessionUser.getId());
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/user", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 我的交易 */
	@RequestMapping(value = "/myTrade")
	public ModelAndView myTrade(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.myTrade;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/myTrade";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = sessionUser.getId();
			if (null != userId && !"".equals(userId)) {
				BaseUser user = userService.selectByPrimaryKey(userId);
				modelMap.put("user", user);
				if (null == user.getPhone() || "".equals(user.getPhone())) {
					modelMap.put("noPhone", "noPhone");
				}
				String passwd = Md5Util.encrypt("888888");
				if (passwd == user.getPassword() || passwd.equals(user.getPassword())) {
					modelMap.put("noEditPassword", "noEditPassword");
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userId", userId);
				List<BaseFans> focusList = fansService.selectListByCondition(map);
				List<BaseUserFund> userFundList = userFundService.selectListByCondition(map);
				if (userFundList.size() > 0) {
					BaseUserFund userFund = userFundList.get(0);
					modelMap.put("userFund", userFund);
					if (null == userFund.getPayPassword() || "".equals(userFund.getPayPassword())) {
						modelMap.put("noPayPassword", "noPayPassword");
					}
				}
				modelMap.put("focusCount", focusList.size());
				map.clear();
				map.put("fansId", userId);
				List<BaseFans> fansList = fansService.selectListByCondition(map);
				modelMap.put("fansCount", fansList.size());
				map.clear();
				map.put("userId", userId);
				List<BaseForumMember> ForumMemberList = forumMemberService.selectListByCondition(map);
				modelMap.put("ForumMemberList", ForumMemberList);
				modelMap.put("userId", sessionUser.getId());
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/myTrade", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 账户信息 */
	@RequestMapping(value = "/accountInformation")
	public ModelAndView accountInformation(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.accountInformation;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/accountInformation";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("relationType", 64);
				map.put("userId", sessionUser.getId());
				List<BaseUserRelation> userRelationList = userRelationService.selectListByCondition(map);
				if (null != userRelationList) {
					if (userRelationList.size() > 0) {
						BaseUserRelation userRelation = userRelationList.get(0);
						Integer antiqueCityId = userRelation.getRelationId();
						modelMap.put("antiqueCityId", antiqueCityId);
						AntiqueCity antiqueCity = antiqueCityService.selectByPrimaryKey(antiqueCityId);
						modelMap.put("antiqueCity", antiqueCity);
					}
				}
				List<AntiqueCity> antiqueCityList = antiqueCityService.selectListByCondition(null);
				modelMap.put("antiqueCityList", antiqueCityList);
				map.clear();
				map.put("allCity", 1);
				List<BaseProvinceCityArea> provinceCityAreaList = provinceCityAreaService.selectListByCondition(map);
				modelMap.put("provinceCityAreaList", provinceCityAreaList);
				map.clear();
				map.put("allProvince", 1);
				List<BaseProvinceCityArea> provinceList = provinceCityAreaService.selectListByCondition(map);
				modelMap.put("provinceList", provinceList);
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/accountInformation", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@RequestMapping(value = "/editUser")
	public ModelAndView editUser(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.editUser;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/editUser";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			Integer userId = sessionUser.getId();
			BaseUser user = userService.selectByPrimaryKey(userId);
			modelMap.put("user", user);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/editUser", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 绑定用户手机号码 **/
	@RequestMapping(value = "/bindUserPhone")
	public ModelAndView bindUserPhone(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.bindUserPhone;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if (null == user) {
				String jumpUrl = "/bindUserPhone/";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			modelMap.put("userId", user.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/bindUserPhone", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	/** 资金明细 */
	@RequestMapping(value = "/fundDetails")
	public ModelAndView FundDetails(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.FundDetails;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/fundDetails";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userId", sessionUser.getId());
				List<BaseUserFund> userFundList = userFundService.selectListByCondition(map);
				BaseUserFund userFund = userFundList.get(0);
				modelMap.put("userFund", userFund);
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/fundDetails", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 资金明细 */
	@RequestMapping(value = "/logisticsResults")
	public ModelAndView logisticsResults(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.logisticsResults;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/logisticsResults";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userId", sessionUser.getId());
				List<BaseUserFund> userFundList = userFundService.selectListByCondition(map);
				BaseUserFund userFund = userFundList.get(0);
				modelMap.put("userFund", userFund);
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/logisticsResults", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 实名认证 */
	@RequestMapping(value = "/realName")
	public ModelAndView realName(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.realName;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/realName";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/realName", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/*
	 * 买家报价
	 * 
	 * @RequestMapping(value="/buyerBid") public ModelAndView buyerBid(ModelMap
	 * modelMap,HttpSession session ,@RequestParam(required=false) String
	 * r,HttpServletRequest request) { String path = Page.buyerBid; try{
	 * modelMap = authCheckService.checkWxLogin(request, modelMap); modelMap =
	 * Check.actionPagePromoterCheck(session, modelMap, r); BaseUser sessionUser
	 * = ActionUtil.checkSession4User(session); if(null == sessionUser){ String
	 * jumpUrl = "/buyerBid"; if(null != r)jumpUrl += "?r="+r; return
	 * Result.userSessionInvalid(modelMap, jumpUrl); } if(null !=
	 * sessionUser.getId() && !"".equals(sessionUser.getId())){ BaseUser user =
	 * userService.selectByPrimaryKey(sessionUser.getId()); modelMap.put("user",
	 * user); } modelMap.put("userId", sessionUser.getId()); }catch(Exception
	 * e){ e.printStackTrace(); path = Page.error;
	 * logger.error("LH_ERROR_加载用户报价页面出现异常_"+e.getMessage()); } return new
	 * ModelAndView(path, modelMap); }
	 */

	

	/** 找回密码发送验证码 */
	@ResponseBody
	@RequestMapping(value = "/getFindPasswordVerifycode", method = RequestMethod.POST)
	public JSONObject getFindPasswordVerifycode(HttpServletRequest request, HttpSession session, @RequestParam String phone) {
		JSONObject json = new JSONObject();
		try {
			int verifyCodeNum = 1;
			Object verifyCodeNumObj = session.getAttribute("verifyCodeNum");
			if (null != verifyCodeNumObj) {
				verifyCodeNum = (Integer) verifyCodeNumObj;
				if (verifyCodeNum > 20) {
					json.put("status", "max_errors");
					json.put("msg", "请求验证码的次数太多，请直接联系管理员进行修改密码");
					return Result.success(json);
				}
			}
			// phone : 短信接口发送短信验证码
			String verifycode = SendMsgUtil.createRandomVcode();
			//System.out.println("reg_verifycode: " + verifycode);
			String mobanId = Const.rl_ytx_msg_moban_id;
			String[] params = new String[] { verifycode, "30" };
			// TODO 修改
			// YTX_MSG.send(phone, mobanId, params);
			session.setAttribute("verifycode", verifycode);
			json.put("verifycode", verifycode);
			json.put("status", "success");
			json.put("msg", "验证码已发送到您的手机,请及时查看.!");
			session.setAttribute("verifyCodeNum", ++verifyCodeNum);

		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_找回密码验证码异常_", json);
		}
		return Result.success(json);
	}

	/** 绑定手机号码发送验证码 */
	@ResponseBody
	@RequestMapping(value = "/getBindUserPhoneVerifycode", method = RequestMethod.POST)
	public JSONObject getBindUserPhoneVerifycode(HttpServletRequest request, HttpSession session, @RequestParam String phone) {
		JSONObject json = new JSONObject();
		try {
			int verifyCodeNum = 1;
			Object verifyCodeNumObj = session.getAttribute("verifyCodeNum");
			if (null != verifyCodeNumObj) {
				verifyCodeNum = (Integer) verifyCodeNumObj;
				if (verifyCodeNum > 20) {
					json.put("status", "max_errors");
					json.put("msg", "请求验证码的次数太多，请直接联系管理员进行修改密码");
					return Result.success(json);
				}
			}
			// phone : 短信接口发送短信验证码
			String verifycode = SendMsgUtil.createRandomVcode();
			//System.out.println("reg_verifycode: " + verifycode);
			String mobanId = Const.rl_ytx_msg_moban_id;
			String[] params = new String[] { verifycode, "30" };
			// TODO 修改
			// YTX_MSG.send(phone, mobanId, params);
			session.setAttribute("verifycode", verifycode);
			session.setAttribute("verifyPhone", phone);
			json.put("verifycode", verifycode);
			json.put("phone", phone);
			json.put("status", "success");
			json.put("msg", "验证码已发送到您的手机,请及时查看.!");
			session.setAttribute("verifyCodeNum", ++verifyCodeNum);

		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_找回密码验证码异常_", json);
		}
		return Result.success(json);
	}

	@ResponseBody
	@RequestMapping(value = "/getUserList", method = RequestMethod.POST)
	public JSONObject getUserList(HttpServletRequest request, HttpServletResponse response) {
		List<BaseUser> userList = null;
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			if (null != user && !"".equals(user)) {
				map.put("currentUserId", user.getId());
			}
			userList = userService.selectListByCondition(map);
			Integer total = userService.selectCountByCondition(map);
			json.put("rows", userList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取用户信息出现异常_", json);
		}
		return Result.success(json);
	}

	@RequestMapping(value = "/initSafetyPage", method = RequestMethod.GET)
	public ModelAndView initSafetyPage(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.initSafety;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/initSafetyPage";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (!Check.isNotNull(sessionUser.getPhone())) {
				path = Page.userCenterShop;
			}
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/initSafetyPage", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@ResponseBody
	@RequestMapping(value = "/initSafety", method = RequestMethod.POST)
	public JSONObject initSafety(HttpServletRequest request, HttpSession session, @RequestParam String verifyCode, @RequestParam String loginPass, @RequestParam String payPass) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Integer userId = user.getId();

			String verifyPhone = null;
			Object verifyCodeObj = session.getAttribute("verifyCode");
			if (null != verifyCodeObj) {
				String sessionVerifyCode = (String) verifyCodeObj;
				if (sessionVerifyCode.equals(verifyCode)) {
					verifyPhone = (String) session.getAttribute("verifyPhone");
				}
			}
			if (Check.isNull(verifyPhone)) {
				return Result.failure(json, "验证码输入不正确，请重新获取验证码", "code_error");
			}
			Map<String, Object> map = new HashMap<String, Object>();// 验证手机号是否重复
			map.put("phone", verifyPhone);
			int userCount = userService.selectCountByCondition(map);
			if (userCount > 0) {
				return Result.failure(json, "该手机号已经被绑定，请确认手机号是否输入正确", "phone_exist");
			}

			if (loginPass.length() < 5) {
				return Result.failure(json, "支付密码不能小于5个字符", "loginPass_short");
			}
			if (!Check.haveNoSpecialChar(loginPass)) {
				return Result.failure(json, "支付密码不能包含特殊字符", "loginPass_specialChar");
			}
			BaseUserFund userFund = userFundService.selectUserFundByUserId(userId);
			if (null == userFund) {
				return Result.failure(json, "您的账户存在异常，无法修改", "userFund_null");
			}
			if (payPass.length() < 5) {
				return Result.failure(json, "支付密码不能小于5个字符", "payPass_short");
			}
			if (!Check.haveNoSpecialChar(payPass)) {
				return Result.failure(json, "支付密码不能包含特殊字符", "payPass_specialChar");
			}
			BaseUser newUser = new BaseUser();
			newUser.setId(userId);
			newUser.setPassword(Md5Util.encrypt(loginPass));
			newUser.setPhone(verifyPhone);
			userService.updateByPrimaryKeySelective(newUser);

			userFund.setPayPassword(Md5Util.encrypt(payPass));
			userFundService.updatePayPasswordById(userFund);

		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_设置用户基本安全数据（手机，登陆密码，支付密码）出现异常_", json);
		}
		return Result.success(json);
	}

	/**
	 * 修改用户信息
	 */
	@ResponseBody
	@RequestMapping(value = "/addOrUpdateUser", method = RequestMethod.POST)
	public JSONObject addOrUpdateUser(@ModelAttribute BaseUser user, HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			user.setMainStatus(1);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == sessionUser) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Integer sessionUserId = sessionUser.getId();
			Integer antiqueCityId = user.getAntiqueCityId();
			Integer userId = user.getId();
			String username = user.getUsername();
			Date date = new Date();
			if (null == user.getId()) {// 添加
				if (null == user.getCreatedAt()) {
					user.setCreatedAt(date);
				}
				user.setCreatedBy(username);
				String userPassword = Md5Util.encrypt(user.getPassword());
				user.setPassword(userPassword);
				user.setRoleId(1);
				userService.insert(user);
				if (null != antiqueCityId && !"".equals(antiqueCityId)) {
					BaseUserRelation userRelation = new BaseUserRelation();
					userRelation.setRelationId(antiqueCityId);
					userRelation.setUserId(userId);
					userRelation.setRelationType(64);
					userRelation.setCreatedBy(username);
					userRelation.setCreatedAt(date);
					userRelationService.insert(userRelation);
				}
				json.put("status", "success");
				json.put("msg", "操作成功");
			} else {// 修改
					// if(1 != user.getId() && !"1".equals(user.getId())){
				if (null == userId) {
					return Result.failure(json, "用户为空", "userId_null");
				}
				if (sessionUserId.intValue() != userId.intValue()) {
					return Result.failure(json, "您没有权限修改该用户数据", "authority_error");
				}

				if (null == user.getUpdatedAt()) {
					user.setUpdatedAt(date);
				}
				if (null != user.getPassword() && !"".equals(user.getPassword())) {
					String userPassword = Md5Util.encrypt(user.getPassword());
					user.setPassword(userPassword);
				}
				user.setUpdatedBy(username);
				userService.updateByPrimaryKeySelective(user);
				if (null != antiqueCityId && !"".equals(antiqueCityId)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("relationType", 64);
					map.put("userId", userId);
					List<BaseUserRelation> db_userRelationList = userRelationService.selectListByCondition(map);
					if (db_userRelationList.size() > 0) {
						BaseUserRelation db_userRelation = db_userRelationList.get(0);
						db_userRelation.setRelationId(antiqueCityId);
						db_userRelation.setUpdatedAt(date);
						db_userRelation.setUpdatedBy(username);
						userRelationService.updateByPrimaryKeySelective(db_userRelation);
					} else {
						BaseUserRelation userRelation = new BaseUserRelation();
						userRelation.setRelationId(antiqueCityId);
						userRelation.setUserId(userId);
						userRelation.setRelationType(64);
						userRelation.setCreatedAt(date);
						userRelation.setCreatedBy(username);
						userRelationService.insert(userRelation);
					}
				}
				if (null != user.getPassword() && !"".equals(user.getPassword())) {
					json.put("msg", "修改成功,请重新登录,3秒后将跳转到登录页面.");
				}
				Result.success(json, null, null);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改用户出现异常_", json);
		}
		return Result.success(json);
	}

	/**
	 * 上传头像
	 * 
	 * @param filePortrait
	 * @return
	 **/
	@ResponseBody
	@RequestMapping(value = "/uploadAvatar", method = RequestMethod.POST)
	public JSONObject uploadAvatar(HttpServletRequest request, @RequestParam MultipartFile filePortrait) {
		JSONObject json = new JSONObject();
		String avatar = "";
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String userportrait = formatter.format(new Date()) + "_userPortrait_";
			avatar = UploadUtil.uploadFile(request, filePortrait, userportrait, "/file/userPortrait/");// 执行上传图片
			// 更新用户
			json.put("filePath", avatar);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_上传头像出现异常_", json);
		}
		return Result.success(json);
	}

	/**
	 * 修改用户密码
	 **/
	@ResponseBody
	@RequestMapping(value = "/updateUserPassword")
	public JSONObject updateUserInfo(HttpServletRequest request, @RequestParam(required = false, value = "newPassword") String newPassword,
			@RequestParam(required = false, value = "oldPassword") String oldPassword) {
		JSONObject json = new JSONObject();
		try {
			if (null != oldPassword && !"".equals(oldPassword)) {
				BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
				if (null == sessionUser) {
					return Result.userSessionInvalid(json);// 返回session过期的json提示
				}
				if (1 != sessionUser.getId() && !"1".equals(sessionUser.getId())) {
					String password = Md5Util.encrypt(oldPassword.toString());
					BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
					if (password.equals(user.getPassword())) {
						user.setPassword(Md5Util.encrypt(newPassword.toString()));
						user.setId(sessionUser.getId());
						userService.updateByPrimaryKeySelective(user);
						json.put("status", "success");
						json.put("msg", "修改成功,请重新登录.");
						request.getSession().invalidate();
					} else {
						json.put("msg", "当前密码不正确,如果忘记当前密码,请到修改密码重新设置密码");
					}
				} else {
					json.put("status", "forbidden");
					json.put("msg", "默认管理员不能进行密码修改");
				}
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_修改用户密码出现异常_", json);
		}
		return Result.success(json);
	}

}
