package com.lhfeiyu.action.front.user;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.AuctionMicroOffers;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseSchedulers;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionMicroOffersService;
import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.base.BaseSchedulersService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
public class UserAuctionAction {
	private static Logger logger = Logger.getLogger("R");
	@Autowired
	BaseSchedulersService schedulersService;
	@Autowired
	AuctionProfessionService auctionProfessionService;
	@Autowired
	AuctionMicroService auctionMicroService;
	@Autowired
	AuctionMicroOffersService auctionMicroOffersService;
	@Autowired
	BaseUserService userService;
	
	
	@RequestMapping(value="/startAuction")
	public ModelAndView startAuction(ModelMap modelMap,HttpServletRequest request) {
		String path = Page.startAuction;
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				new ModelAndView("/index");
			}
			Map<String, Object> map = new HashMap<String,Object>();
//			modelMap.put("notice", notice);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionProfessionList",method=RequestMethod.POST)
	public JSONObject getAuctionProfessionList(ModelMap modelMap,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			int userId = sessionUser.getId();
	
			List<AuctionProfession> auctionProfessions = 
					auctionProfessionService.selectBySchedulers(userId);
			json.put("rows", auctionProfessions);
			//System.out.println(json);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteSchedulers", method=RequestMethod.POST)
	public JSONObject deleteSchedulers(HttpServletRequest request,
			@ModelAttribute BaseSchedulers schedulers) {
		JSONObject json = new JSONObject();
		//System.out.println("00");
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			schedulersService.deleteByCondition("1=1 AND task_type_id=1 AND notice_id = "
					+ ""+schedulers.getNoticeId()+" AND receiver_id = " + schedulers.getReceiverId());
			Result.success(json,"已删除");
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
		
	@RequestMapping(value="/myAuction")
	public ModelAndView myAuction(ModelMap modelMap,HttpServletRequest request) {
		String path = Page.myAuction;
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				new ModelAndView("/index");
			}
			Map<String, Object> map = new HashMap<String,Object>();
//			modelMap.put("notice", notice);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyAuctionMicroLists", method = RequestMethod.POST)
	public JSONObject getMyAuctionMicroLists(HttpServletRequest request, ModelMap modelMap) {
		JSONObject json = new JSONObject();
		//System.out.println("000");
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			int userId = sessionUser.getId();
			Map<String, Object> map = ActionUtil.getAllParam(request);
			
			List<AuctionMicro> auctionMicros = auctionMicroService.selectListByOfferUserId(map);
			json.put("rows", auctionMicros);
			//System.out.println(json);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addMicroOffers", method=RequestMethod.POST)
	public JSONObject addMicroOffers(HttpServletRequest request,
			@ModelAttribute AuctionMicroOffers auctionMicroOffers) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
			auctionMicroOffers.setOfferUserId(user.getId());
			auctionMicroOffers.setOfferUsername(user.getUsername());
			Date date = new Date();
			auctionMicroOffers.setOfferAt(date);
			auctionMicroOffersService.insert(auctionMicroOffers);
			
			AuctionMicro micro = auctionMicroService.selectByPrimaryKey(auctionMicroOffers.getAuctionId());
			if(sessionUser.getId() == micro.getOfferUserId()){
				Result.success(json,"您已经领先!");
			}else{
				micro.setOfferUsername(user.getUsername());
				micro.setOfferUserId(user.getId());
				micro.setOfferPrice(auctionMicroOffers.getOfferPrice());
				micro.setOfferAt(date);
				Integer offerTimes = micro.getOfferTimes();
				micro.setOfferTimes(++offerTimes);
				auctionMicroService.updateByPrimaryKey(micro);
				Result.success(json,"竞拍成功");
			}
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/addMicroOffers", json);
		}
		return Result.success(json,"竞拍成功");
	}
	@ResponseBody
	@RequestMapping(value="/deleteMicroOffers", method=RequestMethod.POST)
	public JSONObject deleteMicroOffers(HttpServletRequest request,
			@ModelAttribute AuctionMicroOffers auctionMicroOffers) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			AuctionMicro micro = auctionMicroService.selectByPrimaryKey(auctionMicroOffers.getAuctionId());
			if(sessionUser.getId() == micro.getOfferUserId()){
				Result.success(json,"当前出价最高是您，不能出局");
			}else{
				micro.setMainStatus(4);
				auctionMicroService.updateByPrimaryKey(micro);
				Result.success(json,"您已出局");
			}
			
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/addMicroOffers", json);
		}
		return Result.success(json,"竞拍成功");
	}
}

