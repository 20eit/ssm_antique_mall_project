package com.lhfeiyu.action.front.chat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.base.BaseChat;
import com.lhfeiyu.po.base.BaseNotice;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.BaseChatService;
import com.lhfeiyu.service.base.BaseNoticeService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/message")
public class MessageAction {
	private static Logger logger = Logger.getLogger("R");
	@Autowired
	private BaseChatService chatService;
	@Autowired
	private BaseNoticeService noticeSerrvice;
	@RequestMapping(value="/message")
	public ModelAndView message(ModelMap modelMap,HttpServletRequest request) {
		String path = Page.message;
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				new ModelAndView("/index");
			}
			Map<String, Object> map = new HashMap<String,Object>();
			/*map.put("sender_id", sessionUser.getId());
			map.put("notRead", 1);
			BaseNotice notice =  noticeSerrvice.selectByCondition(map);
			modelMap.put("notice", notice);*/
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	@RequestMapping(value="/systemMessage")
	public ModelAndView systemMessage(ModelMap modelMap,HttpServletRequest request) {
		String path = Page.systemMessage;
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				new ModelAndView("/index");
			}
			
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	@RequestMapping(value="/messageDetail/{id}")
	public ModelAndView messageDetail(ModelMap modelMap,HttpServletRequest request,
			@PathVariable Integer id) {
		String path = Page.messageDetail;
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				new ModelAndView("/index");
			}
			  BaseNotice notice = noticeSerrvice.selectByPrimaryKey(id);
			  //System.out.println(notice);
			  modelMap.put("notice", notice);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getSystemMessageList")
	public JSONObject getSystemMessageList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("receiverId", sessionUser.getId());
			List<BaseNotice> notice = noticeSerrvice.selectListByCondition(map);
			json.put("rows", notice);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMessageList")
	public JSONObject getMessageList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			int userId = sessionUser.getId();
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("receiverId", sessionUser.getId());
			map.put("notDeal", 1);
			map.put("orderBy", "send_time");
			map.put("ascOrdesc", "DESC");
			List<BaseChat> baseChat = chatService.selectListByCondition(map);
			json.put("rows", baseChat);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateMessage", method=RequestMethod.POST)
	public JSONObject updateMessage(HttpServletRequest request,
			@ModelAttribute BaseChat chat) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Date date = new Date();
			BaseChat chat2 = chatService.selectByPrimaryKey(chat.getId());
			chat2.setReadTime(date);
			chat2.setMainStatus(2);
			chatService.updateByPrimaryKey(chat2);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateSyatemMessage", method=RequestMethod.POST)
	public JSONObject updateSyatemMessage(HttpServletRequest request,
			@ModelAttribute BaseNotice chat) {
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Date date = new Date();
			BaseNotice chat2 = noticeSerrvice.selectByPrimaryKey(chat.getId());
			chat2.setReadTime(date);
			chat2.setReadStatus(2);
			noticeSerrvice.updateByPrimaryKey(chat2);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/message/getMessageList", json);
		}
		return Result.success(json);
	}
}

