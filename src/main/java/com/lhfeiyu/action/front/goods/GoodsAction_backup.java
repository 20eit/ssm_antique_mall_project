/*package com.lhfeiyu.action.front.goods;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.AuctionQuickInst;
import com.lhfeiyu.po.Wholesale;
import com.lhfeiyu.po.base.BaseComment;
import com.lhfeiyu.po.base.BaseDict;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseGoodsPicture;
import com.lhfeiyu.po.base.BaseShop;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.AuctionQuickInstService;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.WholesaleService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseCommentService;
import com.lhfeiyu.service.base.BaseDictService;
import com.lhfeiyu.service.base.BaseGoodsPictureService;
import com.lhfeiyu.service.base.BaseGoodsService;
import com.lhfeiyu.service.base.BaseShopService;
import com.lhfeiyu.thirdparty.wx.business.AuthAccess;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;

@Controller
public class GoodsAction_backup {
	@Autowired
	private BaseGoodsService service;
	@Autowired
	private BaseShopService shopService;
	@Autowired
	private BaseGoodsPictureService goodsPictureService;
	@Autowired
	private BaseCommentService commentService;
	@Autowired
	private BaseDictService dictService;
	@Autowired
	private WholesaleService  wholesaleService;
	@Autowired
	private BaseAA_UtilService  utilService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private AuctionQuickInstService auctionQuickInstService;
	@Autowired
	private AuctionInstService auctionInstService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/gm")
	public ModelAndView gm(ModelMap modelMap,HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.managementGoods;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == user){
				String jumpUrl = "/gm";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/{moduleId}")
	public ModelAndView goodsManage(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.managementGoods;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Integer userId = user.getId();
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			if(moduleId != 5){
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0);
					modelMap.put("shopId", shop.getId());
				}
			}else{
				List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
				if(wholesaleList.size() > 0){
					Wholesale wholesale = wholesaleList.get(0);
					modelMap.put("shopId", wholesale.getId());
				}
			}
			modelMap.put("moduleId", moduleId);
			modelMap.put("userId", userId);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/goodsManageType")
	public ModelAndView goodsManageType(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.goodsManageType;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/goodsManageType";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Integer userId = user.getId();
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
			if(wholesaleList.size() > 0){
				Wholesale wholesale = wholesaleList.get(0);
				modelMap.put("wholesaleId", wholesale.getId());
			}
			List<AuctionQuickInst> auctionQuickInstList = auctionQuickInstService.selectListByCondition(map);
			if(auctionQuickInstList.size() > 0){
				modelMap.put("auctionQuickInst", auctionQuickInstList.get(0));
			}
			List<AuctionInst> auctionInstList = auctionInstService.selectListByCondition(map);
			if(auctionInstList.size() > 0){
				modelMap.put("auctionInst", auctionInstList.get(0));
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品类型页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/saled/{moduleId}")
	public ModelAndView gMSaled(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmSaled;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/saled/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<Shop> shopList = shopService.selectListByCondition(map);
			if(shopList.size() > 0){
				Shop shop = shopList.get(0);
				modelMap.put("shopId", shop.getId());
			}
			modelMap.put("moduleId", moduleId);
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品已售页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	@RequestMapping(value="/gm/undercarriage/{moduleId}")
	public ModelAndView gmUndercarriage(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmUndercarriage;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/undercarriage/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			if(moduleId != 5){
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0);
					modelMap.put("shopId", shop.getId());
				}
			}else{
				List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
				if(wholesaleList.size() > 0){
					Wholesale wholesale = wholesaleList.get(0);
					modelMap.put("shopId", wholesale.getId());
				}
			}
			modelMap.put("moduleId", moduleId);
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品下架页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	@RequestMapping(value="/gm/auction/{moduleId}")
	public ModelAndView gmAuction(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmAuction;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/auction/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			if(moduleId != 5){
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0);
					modelMap.put("shopId", shop.getId());
				}
			}else{
				List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
				if(wholesaleList.size() > 0){
					Wholesale wholesale = wholesaleList.get(0);
					modelMap.put("shopId", wholesale.getId());
				}
			}
			modelMap.put("moduleId", moduleId);
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品已送拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	@RequestMapping(value="/gm/auctionning/{moduleId}")
	public ModelAndView gmAuctionning(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmAuctionning;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/auctionning/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			if(moduleId != 5){
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0);
					modelMap.put("shopId", shop.getId());
				}
			}else{
				List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
				if(wholesaleList.size() > 0){
					Wholesale wholesale = wholesaleList.get(0);
					modelMap.put("shopId", wholesale.getId());
				}
			}
			modelMap.put("moduleId", moduleId);
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品送拍中页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/like/{moduleId}")
	public ModelAndView gmLike(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmLike;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/like/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			modelMap.put("userId", user.getId());
			modelMap.put("moduleId", moduleId);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品送拍中页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/wholesale")
	public ModelAndView gmWholesale(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = PagePath.gmWholesale;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/wholesale";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
			if(wholesaleList.size() > 0){
				Wholesale wholesale = wholesaleList.get(0);
				modelMap.put("shopId", wholesale.getId());
			}
			modelMap.put("userId", user.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = PagePath.error;
			logger.error("LH_ERROR_加载管理商品送拍中页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/frozen/{moduleId}")
	public ModelAndView gmFrozen(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.gmFrozen;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/frozen/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
			if(wholesaleList.size() > 0){
				Wholesale wholesale = wholesaleList.get(0);
				modelMap.put("shopId", wholesale.getId());
			}
			modelMap.put("userId", user.getId());
			modelMap.put("moduleId", moduleId);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品冻结页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/gm/goodsSource/{moduleId}")
	public ModelAndView goodsSource(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@PathVariable Integer moduleId) {
		String path = Page.goodsSource;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/gm/goodsSource/"+moduleId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			if(moduleId != 5){
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0);
					modelMap.put("shopId", shop.getId());
				}
			}else{
				List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
				if(wholesaleList.size() > 0){
					Wholesale wholesale = wholesaleList.get(0);
					modelMap.put("shopId", wholesale.getId());
				}
			}
			modelMap.put("userId", user.getId());
			modelMap.put("moduleId", moduleId);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载管理商品冻结页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/pc")
	public ModelAndView addGoodsForPc(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.addGoodsForPc;
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("parentId", 120);
			List<BaseDict> goodsDidtList =  dictService.selectListByCondition(map);
			modelMap.put("goodsDidtList", goodsDidtList);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载电脑添加商品页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/releaseGoods")
	public ModelAndView releaseGoods(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@RequestParam(required=false) String from
			,@RequestParam(required=false) Integer goodsId) {
		String path = Page.releaseGoods;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/releaseGoods";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			modelMap.put("from", from);
			modelMap.put("goodsId", goodsId);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			//List<Shop> shopList = shopService.selectListByCondition(map);
			List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
			if(wholesaleList.size() > 0){
				modelMap.put("wholesaleId", wholesaleList.get(0).getId());
			}
			
			map.put("userId", user.getId());
			
			if("".equals("as")){
				
			}else if("".equals("ap")){
				List<AuctionInst> auctionInstList = auctionInstService.selectListByCondition(map);
				if(auctionInstList.size() > 0){
					modelMap.put("auctionInst", auctionInstList.get(0));
				}
			}
			
			
			List<AuctionQuickInst> auctionQuickInstList = auctionQuickInstService.selectListByCondition(map);
			if(auctionQuickInstList.size() > 0){
				modelMap.put("auctionQuickInst", auctionQuickInstList.get(0));
			}
			map.clear();
			map.put("parentId",120);
			List<Dict> dictList = dictService.selectListByCondition(map);//产品类型
			if(dictList.size() > 0){
				modelMap.put("dictList", dictList);
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载发布拍卖页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/addGoods")
	public ModelAndView addGoods(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request
			,@RequestParam(required=false) String from) {
		String path = Page.addGoods;
		try{
			BaseUser user = ActionUtil.checkSession4User(session);
			String url = "http://weipaike.net/addGoods";
			if(null == user){
				String jumpUrl = "/addGoods";
				if(Check.isNotNull(from)){
					jumpUrl += "?r="+r;
					url += "?r="+r;
					if(Check.isNotNull(from)){
						jumpUrl += "&from="+from;
						url += "&from="+from;
					}
				}else{
					if(Check.isNotNull(from)){
						jumpUrl += "?from="+from;
						url += "?from="+from;
					}
				}
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			
			String ticket = AuthAccess.getWxDataFromProperty("ticket");//从Property文件中获取ticket,如果文件中没有，则会远程获取
			modelMap = AuthAccess.getSign(modelMap, ticket, url);
			
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", user.getId());
			List<BaseShop> shopList = shopService.selectListByCondition(map);
			if(!Check.isNotNull(shopList)){
				return new ModelAndView(path, Result.failure(modelMap, "店铺不存在", "shop_null"));
			}
			BaseShop shop = shopList.get(0);
			Integer shopId = shop.getId();
			modelMap.put("shopId", shopId);
			map.put("relationType", "64");
			List<UserRelation> userRelationList = userRelationService.selectListByCondition(map);
			if(userRelationList.size() <= 0){
				modelMap.put("noAntiqueCity", "noAntiqueCity");
			}
			map.clear();
			map.put("parentId", 120);
			List<BaseDict> goodsDictList =  dictService.selectListByCondition(map);
			modelMap.put("goodsDictList", goodsDictList);
			modelMap.put("from", from);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载发布藏品页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/editGoods/{id}")
	public ModelAndView editGoods(ModelMap modelMap,HttpSession session,
			@PathVariable Integer id
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.editGoods;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			if(null == sessionUser){
				String jumpUrl = "/editGoods/"+id;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			BaseGoods goods = service.selectByPrimaryKey(id);
			modelMap.put("goods", goods);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载发布藏品页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/goods/{id}")
	public ModelAndView commodityDetail(ModelMap modelMap,HttpServletRequest request,
			@PathVariable Integer id,@RequestParam(required=false) String r
			,@RequestParam(required=false) String promoteUserSerial) {
		String path = Page.commodityGoodsDetail;
		try{
			BaseUser user = ActionUtil.checkSession4User(request.getSession());
			if(null == sessionUser){
				return Result.userSessionInvalid(modelMap, "/goods/"+id);
			}
			modelMap = authCheckService.checkWxLogin(request, modelMap, true, r);
			Map<String, Object> map = RequestUtil.getRequestParam(request);//自动获取所有参数（查询条件）
			String selled = request.getParameter("selled");
			if(null == selled){
				map.put("forAuction", 1);//只显示未卖的产品详情
			}
			if(null != user){
				map.put("collectGoods", 1);
				map.put("collectGoodsUserId", user.getId());
			}
			map.put("id", id);
			List<BaseGoods> GoodsList = service.selectListByCondition(map);
			if(GoodsList.size() > 0){
				BaseGoods goods= GoodsList.get(0);
				String shopId = request.getParameter("shopId");
				modelMap.put("shopId", shopId);
				Integer goodsId = goods.getId();
				map.clear();
				map.put("goodsId", goodsId);
				//map.put("isNOTCover", 2);
				List<BaseGoodsPicture> GoodsPictureList = goodsPictureService.selectListByCondition(map);
				modelMap.put("GoodsPictureList", GoodsPictureList);
				modelMap.put("goods", goods);
				map.clear();
				map.put("objectId", goodsId);
				map.put("commentTypeId", 3);
				map.put("start", 0);
				map.put("count", 6);
				List<BaseComment> commentList =  commentService.selectListByCondition(map);
				modelMap.put("commentList", commentList);
				map.clear();
				map.put("goodsId", goodsId);
				//List<GoodsOffers> goodsOffersList = goodsOffersService.selectListByCondition(map);
				//modelMap.put("goodsOffersList", goodsOffersList);
				if(null != user){
					modelMap.put("currentUserId", user.getId());
				}
				if(promoteUserSerial != null){
					modelMap.put("promoteUserSerial", promoteUserSerial);
					modelMap.put("r", promoteUserSerial);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载商品详情页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addBaseGoods", method=RequestMethod.POST)
	public JSONObject addBaseGoods(@ModelAttribute BaseGoods goods,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			String basePath = request.getServletContext().getRealPath("/");
			//TODO 修改
			//service.addGoods(json, user, goods, basePath);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addGoodsForPc", method=RequestMethod.POST)
	public JSONObject addGoodsForPc(@ModelAttribute BaseGoods goods,HttpServletRequest request
			,@RequestParam String phone){
		JSONObject json = new JSONObject();
		try {
			//TODO 修改
			//service.addGoodsForPC(json, goods, phone);
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_PC添加或修改商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getGoodsList", method=RequestMethod.POST)
	public JSONObject getGoodsList(HttpServletRequest request,@RequestParam(required=false) String from) {
		List<BaseGoods> goodsList = null;
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			if(Check.isNotNull(from) && from.equals("market")){
				map.put("orderBy", "bonus_type_id DESC,go.offer_price DESC,A.created_at");
				map.put("ascOrdesc", "DESC");
				map.put("forMarket", 1);
			}
			String wholesaleId = (String) map.get("wholesaleId");
			if(null != wholesaleId && !"".equals(wholesaleId)){
				if(null != user){
					map.put("shopUserId", user.getId());
				}
			}
			goodsList = service.selectListByCondition(map);
			Integer total = service.selectCountByCondition(map);
			json.put("rows", goodsList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取商品列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyGoodsList", method=RequestMethod.POST)
	public JSONObject getMyGoodsList(HttpServletRequest request,@RequestParam(required=false) String from) {
		List<BaseGoods> goodsList = null;
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = ActionUtil.getAllParam(request);
			if(Check.isNotNull(from) && from.equals("market")){
				map.put("orderBy", "bonus_type_id DESC,go.offer_price DESC,A.created_at");
				map.put("ascOrdesc", "DESC");
				map.put("forMarket", 1);
			}
			map.put("userId", userId);//自加载属于自己的藏品
			goodsList = service.selectListByCondition(map);
			Integer total = service.selectCountByCondition(map);
			json.put("rows", goodsList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取商品列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getGoods", method=RequestMethod.POST)
	public JSONObject getGoods(HttpServletRequest request) {
		List<BaseGoods> goodsList = null;
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			//TODO 修改
			//goodsList = service.getGoods(map);
			//Integer total = service.selectCountByCondition(map);
			json.put("rows", goodsList);
			//json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取商品列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/copyGoods", method=RequestMethod.POST)
	public JSONObject copyGoods(HttpServletRequest request,@RequestParam Integer id) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			String to = request.getParameter("to");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("goodsFrom", id);
			map.put("userId", userId);
			map.put("moduleId", to);
			List<BaseGoods> goodsList = service.selectListByCondition(map);
			if(null != goodsList){
				if(goodsList.size() >0 ){
					json.put("status","failure");
					json.put("msg","您已复制该商品,请不要重复复制.");
					return Result.success(json);
				}
			}
			map.remove("goodsFrom");
			BaseGoods goods = service.selectByPrimaryKey(id);
			if(null != to && !"".equals(to)){
				if(Integer.valueOf(to) == 4 || Integer.valueOf(to).equals(4)){
					List<BaseShop> shopList = shopService.selectListByCondition(map);
					goods.setGoodsPrivilege(2);
					goods.setModuleId(4);
					goods.setMainStatus(1);
					if(null != shopList){
						if(shopList.size() > 0){
							BaseShop shop = shopList.get(0);
							goods.setShopId(shop.getId());
						}
					}
				}else if(Integer.valueOf(to) == 5 || Integer.valueOf(to).equals(5)){
					List<Wholesale> wholesaleList = wholesaleService.selectListByCondition(map);
					goods.setGoodsPrivilege(2);
					goods.setModuleId(5);
					goods.setMainStatus(79);
					if(null != wholesaleList){
						if(wholesaleList.size() > 0){
							Wholesale wholesale = wholesaleList.get(0);
							goods.setShopId(wholesale.getId());
						}
					}
				}
			}
			goods.setGoodsFrom(id);
			service.insert(goods);
			json.put("status","success");
			json.put("msg","已成功复制该商品");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取复制商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/allowCopy", method=RequestMethod.POST)
	public JSONObject allowCopy(HttpServletRequest request,@RequestParam Integer id) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			BaseGoods goods = new BaseGoods();
			goods.setId(id);
			goods.setGoodsPrivilege(1);
			service.updateByPrimaryKeySelective(goods);
			json.put("status","success");
			json.put("msg","设置成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取允许复制商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/noAllowCopy", method=RequestMethod.POST)
	public JSONObject noAllowCopy(HttpServletRequest request,@RequestParam Integer id) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			BaseGoods goods = new BaseGoods();
			goods.setId(id);
			goods.setGoodsPrivilege(2);
			service.updateByPrimaryKeySelective(goods);
			json.put("status","success");
			json.put("msg","设置成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取允许复制商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteGoods", method=RequestMethod.POST)
	public JSONObject deleteFans(HttpServletRequest request,@RequestParam Integer id) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			int sessionUserId = user.getId();
			Integer db_id = null;
			if(null != id){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("id", id);
				map.put("userId", sessionUserId);
				List<BaseGoods> goodsList = service.selectListByCondition(map);
				if(goodsList.size() > 0){
					BaseGoods goods = goodsList.get(0);
					if(null != goods){
						int id1 = goods.getUserId();
						if(sessionUserId != id1){
							json.put("status","failure");
							json.put("msg","您没有该权限");
							return Result.success(json);
						}else{
							db_id = id;
							service.deleteByPrimaryKey(db_id);
							map.clear();
							map.put("ids", db_id);
							map.put("table", Table.goods_picture);
							map.put("username",user.getUsername());
							utilService.deleteByIds(map);
							json.put("status","success");
							json.put("msg","已成功删除该商品");
						}
					}
				}
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/updateGoods", method=RequestMethod.POST)
	public JSONObject updateGoods(HttpServletRequest request,@ModelAttribute BaseGoods goods) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			int sessionUserId = user.getId();
			Integer goodsId = goods.getId();
			if(null != goodsId){
				BaseGoods db_goods = service.selectByPrimaryKey(goodsId);
				if(null != db_goods){
					Integer moduleId = db_goods.getModuleId();
					Integer shopId = db_goods.getShopId();
					if(moduleId == null){
						BaseShop shop = shopService.selectByPrimaryKey(shopId);
						Integer db_userId = shop.getUserId();
						if(db_userId == sessionUserId){
							service.updateByPrimaryKeySelective(goods);
							json.put("status", "success");
							json.put("msg", "修改成功");
						}else{
							json.put("msg", "对不起,您没有修改商品信息的权限");
							json.put("status", "failure");
							return Result.success(json);
						}
					}else{
						Wholesale wholesale = wholesaleService.selectByPrimaryKey(shopId);
						Integer db_userId = wholesale.getUserId();
						if(db_userId == sessionUserId){
							service.updateByPrimaryKeySelective(goods);
							json.put("status", "success");
							json.put("msg", "修改成功");
						}else{
							json.put("msg", "对不起,您没有修改商品信息的权限");
							json.put("status", "failure");
							return Result.success(json);
						}
					}
				}else{
					json.put("msg", "对不起,无法查询到商品信息");
					json.put("status", "failure");
					return Result.success(json);
				}
			}else{
				json.put("msg", "对不起,无法查询到商品信息");
				json.put("status", "failure");
				return Result.success(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_修改商品出现异常_", json);
		}
		return Result.success(json);
	}
	
}
*/