/*package com.lhfeiyu.action.front.distribution;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.DistributionIncomeService;
import com.lhfeiyu.service.base.GoodsLinkService;
import com.lhfeiyu.service.base.GoodsService;
import com.lhfeiyu.service.base.PictureService;
import com.lhfeiyu.service.base.UserCustomerService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.DistributionIncome;
import com.lhfeiyu.po.base.Goods;
import com.lhfeiyu.po.base.GoodsLink;
import com.lhfeiyu.po.base.Picture;
import com.lhfeiyu.po.base.User;
import com.lhfeiyu.po.base.UserCustomer;

@Controller
public class DistributionAction {
	@Autowired
	private DistributionIncomeService diService;
	@Autowired
	private UserCustomerService ucService;
	@Autowired
	private PictureService picService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private GoodsLinkService glService;
	@Autowired
	private AuthCheckService authCheckService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/dist")
	public ModelAndView distIndex(ModelMap modelMap,HttpServletRequest request
			,@RequestParam(required=false) String r) {
		String path = Page.distributionIndex;
		path = Page.frontIndex;//TODO-tempHide-淘客
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			
			Map<String, Object> map = new HashMap<String,Object>();
			//map.put("overall", 1);//不传入任何参数：总榜单
			map.put("start", 0);
			map.put("count", 5);
			List<User> overallDIList = diService.selectIncomeListByCondition(map);
			map.put("month", 1);//月排行
			List<User> monthDIList = diService.selectIncomeListByCondition(map);
			map.remove("month");
			map.put("week", 1);//周排行
			List<User> weekDIList = diService.selectIncomeListByCondition(map);
			modelMap.put("overallDIList", overallDIList);
			modelMap.put("monthDIList", monthDIList);
			modelMap.put("weekDIList", weekDIList);
			
			map.clear();
			map.put("typeId", 87);//淘客Banner图片
			List<Picture> picList = picService.selectListByCondition(map);
			modelMap.put("picList", picList);
			
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载分销首页出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/distDesc")
	public ModelAndView distDesc(ModelMap modelMap,HttpServletRequest request,@RequestParam(required=false) String r) {
		modelMap = authCheckService.checkWxLogin(request, modelMap);
		
		return new ModelAndView(Page.distributionDesc);
	}
	
	@RequestMapping(value="/distGoods")
	public ModelAndView distGoods(ModelMap modelMap, HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		modelMap = authCheckService.checkWxLogin(request, modelMap);
		modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
		User user = ActionUtil.checkSession4User(session);
		if(null == user){
			String jumpUrl = "/distGoods";
			if(null != r)jumpUrl += "?r="+r;
			return Result.userSessionInvalid(modelMap, jumpUrl);
		}
		return new ModelAndView(Page.distributionGoods);
	}
	
	@RequestMapping(value="/myCustomer")
	public ModelAndView myCustomer(ModelMap modelMap, HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myCustomer;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/myCustomer";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载我的客户页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@RequestMapping(value="/myIncome")
	public ModelAndView myIncome(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myIncome;
		try{
			modelMap = authCheckService.checkWxLogin(request, modelMap);
			modelMap = Check.actionPagePromoterCheck(session, modelMap, r);
			User user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl = "/myIncome";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("start", 0);
			map.put("count", 1);
			map.put("userId", user.getId());
			List<User> overallDIList = diService.selectIncomeListByCondition(map);
			map.put("month", 1);//月收入
			List<User> monthDIList = diService.selectIncomeListByCondition(map);
			map.remove("month");
			map.put("today", 1);//今日收入
			List<User> todayDIList = diService.selectIncomeListByCondition(map);
			if(null != overallDIList && overallDIList.size()>0)modelMap.put("overallIncome", overallDIList.get(0));
			if(null != monthDIList && monthDIList.size()>0)modelMap.put("monthIncome", monthDIList.get(0));
			if(null != todayDIList && todayDIList.size()>0)modelMap.put("todayIncome", todayDIList.get(0));
			
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载我的收入列表页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyIncomeList", method=RequestMethod.POST)
	public JSONObject getMyIncomeList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			map.put("userId", user.getId());
			//map.put("groupBy", "A.user_id ");
			List<DistributionIncome> diList = diService.selectListByCondition(map);
			json.put("rows", diList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载我的收入列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getMyCustomerList", method=RequestMethod.POST)
	public JSONObject getMyCustomerList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			map.put("userId", user.getId());
			List<UserCustomer> ucList = ucService.selectListByCondition(map);
			Integer total = ucService.selectCountByCondition(map);
			json.put("rows", ucList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载我的客户列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getDistGoodsList", method=RequestMethod.POST)
	public JSONObject getDistGoodsList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = ActionUtil.getAllParam(request);
			map.put("exceptUserId", user.getId());//不加载自己的商品
			map.put("forAuction", 1);
			//Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			String goodsIds = glService.selectGoodsIdsOfUser(map);
			if(null != goodsIds && goodsIds.length() > 0){//过滤掉已经分享推广的藏品
				map.put("exceptGoodsIds", goodsIds);
			}
			List<Goods> goodsList = goodsService.selectListByCondition(map);
			json.put("rows", goodsList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载推广藏品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/shareGoods", method=RequestMethod.POST)
	public JSONObject shareGoods(HttpServletRequest request, @RequestParam Integer goodsId) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", userId);//
			map.put("goodsId", goodsId);
			List<GoodsLink> glList = glService.selectListByCondition(map);
			if(null != glList && glList.size()>0){
			}else{
				map.clear();
				map.put("id", goodsId);
				List<Goods> goodsList = goodsService.selectListByCondition(map);
				if(null == goodsList){
					json.put("msg", "该商品已经被删除");
					json.put("status", "failure");
					return Result.success(json);
				}else{
					Goods goods = goodsList.get(0);
					Integer status = goods.getMainStatus();
					if(null != status && status != 1){
						json.put("msg", "该藏品目前不能进行分享推广");
						json.put("status", "failure");
						return Result.success(json);
					}
					Integer hostId = goods.getUserId();
					if(null != hostId && hostId.intValue() == userId.intValue()){
						json.put("msg", "自己不能分享推广自己的藏品");
						json.put("status", "failure");
						return Result.success(json);
					}
				}
				GoodsLink gl = new GoodsLink();
				gl.setUserId(userId);
				gl.setGoodsId(goodsId);
				gl.setTypeId(2);//分享推广商品
				gl.setCreatedAt(new Date());
				gl.setCreatedBy(user.getUsername());
				glService.insertSelective(gl);
			}
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_添加推广藏品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/cancelShareGoods", method=RequestMethod.POST)
	public JSONObject cancelShareGoods(HttpServletRequest request, @RequestParam Integer goodsId) {
		JSONObject json = new JSONObject();
		try {
			User user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = new HashMap<String,Object>();
			map.put("userId", userId);//
			map.put("goodsId", goodsId);
			List<GoodsLink> glList = glService.selectListByCondition(map);
			if(null != glList && glList.size()>0){
				if(glList.size()<2){
					glService.deleteByPrimaryKey(glList.get(0).getId());
				}else{
					for(GoodsLink gl : glList){
						glService.deleteByPrimaryKey(gl.getId());
					}
				}
			}
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_取消推广藏品出现异常_", json);
		}
		return Result.success(json);
	}
	
}
*/