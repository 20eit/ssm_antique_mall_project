package com.lhfeiyu.action.front.forum;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.base.BaseForum;
import com.lhfeiyu.po.base.BaseForumArticle;
import com.lhfeiyu.po.base.BaseForumArticleCollect;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseForumArticleCollectService;
import com.lhfeiyu.service.base.BaseForumArticleService;
import com.lhfeiyu.service.base.BaseForumService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
public class ForumArticleCollectAction {
	@Autowired
	BaseForumArticleCollectService forumArticleCollectService;
	@Autowired
	BaseForumService forumService;
	@Autowired
	BaseForumArticleService forumArticleService;
	@Autowired
	BaseAA_UtilService utilService;
	@Autowired
	private AuthCheckService authCheckService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/fc/{forumId}")
	public ModelAndView myForumArticleCollect(ModelMap modelMap,HttpSession session,
			@PathVariable Integer forumId
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myForumArticleCollect;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			if(null == sessionUser){
				String jumpUrl = "/fc/"+forumId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			BaseForum forum = forumService.selectByPrimaryKey(forumId);
			modelMap.put("forum", forum);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("forumId", forumId);
			List<BaseForumArticle> forumArticleList = forumArticleService.selectListByCondition(map);
			if(forumArticleList.size() > 0){
				modelMap.put("count", forumArticleList.size());
			}
			modelMap.put("forumId", forumId);
			modelMap.put("user", sessionUser);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载我的收藏页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateForumArticleCollect", method=RequestMethod.POST)
	public JSONObject addOrUpdateForumArticle(@ModelAttribute BaseForumArticleCollect forumArticleCollect,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == user){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			if(null == forumArticleCollect.getId()){//添加
				forumArticleCollect.setUserId(user.getId());
				forumArticleCollect.setCreatedBy(user.getUsername());
				forumArticleCollect.setCreatedAt(new Date());
				forumArticleCollectService.insert(forumArticleCollect);
			}else{//修改
				forumArticleCollect.setUpdatedBy(user.getUsername());
				forumArticleCollect.setUpdatedAt(new Date());
				forumArticleCollectService.updateByPrimaryKeySelective(forumArticleCollect);
			}
			json.put("status", "success");
			json.put("id",forumArticleCollect.getId());
			json.put("msg", "操作成功");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改我的收藏出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getforumArticleCollectList", method=RequestMethod.POST)
	public JSONObject getForumList(HttpServletRequest request) {
		List<BaseForumArticleCollect> forumArticleCollectList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			forumArticleCollectList = forumArticleCollectService.selectListByCondition(map);
			Integer total = forumArticleCollectService.selectCountByCondition(map);
			json.put("rows", forumArticleCollectList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载论坛列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteForumArticleCollectThorough", method=RequestMethod.POST)
	public JSONObject deleteForumArticleCollectThorough(HttpServletRequest request,
			@RequestParam(value="ids") Integer ids) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			int sessionUserId = user.getId();
			Integer db_id = null;
			if(null != ids){
				BaseForumArticleCollect forumArticleCollect = forumArticleCollectService.selectByPrimaryKey(ids);
				if(null != forumArticleCollect){
					if(sessionUserId != forumArticleCollect.getUserId()){
						json.put("status","failure");
						json.put("msg","您没有该权限");
						return Result.success(json);
					}else{
						db_id = forumArticleCollect.getId();
					}
				}
			}
			if(null != db_id){
				forumArticleCollectService.deleteByPrimaryKey(db_id);
			}
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除公告出现异常_", json);
		}
		return Result.success(json);
	}
	
}
