package com.lhfeiyu.action.front.forum;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.base.BaseAssets;
import com.lhfeiyu.domain.base.AliyunOSS;
import com.lhfeiyu.po.base.BaseForum;
import com.lhfeiyu.po.base.BaseForumArticle;
import com.lhfeiyu.po.base.BaseOrderGoods;
import com.lhfeiyu.po.base.BasePicture;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseCommentService;
import com.lhfeiyu.service.base.BaseForumArticleService;
import com.lhfeiyu.service.base.BaseForumService;
import com.lhfeiyu.service.base.BaseOrderGoodsService;
import com.lhfeiyu.service.base.BasePictureService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.thirdparty.wx.business.AuthAccess;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
public class ForumArticleAction {

	@Autowired
	private BaseForumArticleService forumArticleService;
	@Autowired
	private BaseCommentService commentService;
	@Autowired
	private BaseForumService forumService;
	@Autowired
	private BasePictureService pictureService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private BaseOrderGoodsService orderGoodsService;
	@Autowired
	private BaseUserService userService;
	private static Logger logger = Logger.getLogger("R");

	@RequestMapping(value = "/forumUserArticle/{userSerial}")
	public ModelAndView forumUserArticle(ModelMap modelMap, HttpServletRequest request, @PathVariable String userSerial,
			@RequestParam(required = false) String r) {
		String path = Page.forumUserArticle;
		try {
			/*
			 * User sessionUser = ActionUtil.checkSession4User(session); if(null
			 * == sessionUser){ return Result.userSessionInvalid(modelMap,
			 * "/forumArticle?typeId="+typeId); }
			 */
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("serial", userSerial);
			BaseUser user = userService.selectByCondition(map);
			if (null != user) {
				modelMap.put("user", user);
				json.put("user", user);
				modelMap.put("userSerial", user.getSerial());
				json.put("userSerial", user.getSerial());
				/*
				 * map.clear(); map.put("userId", user.getId());
				 * List<BaseForumArticle> forumArticleList =
				 * forumArticleService.selectListByCondition(map);
				 * modelMap.put("forumArticleList", forumArticleList);
				 */
			}
			modelMap.put("paramJson", json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/forumUserArticle/" + userSerial, modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@RequestMapping(value = "/forumUserReply/{userSerial}")
	public ModelAndView forumUserReply(ModelMap modelMap, HttpServletRequest request, @PathVariable String userSerial,
			@RequestParam(required = false) String r) {
		String path = Page.forumUserReply;
		try {
			/*
			 * User sessionUser = ActionUtil.checkSession4User(session); if(null
			 * == sessionUser){ return Result.userSessionInvalid(modelMap,
			 * "/forumArticle?typeId="+typeId); }
			 */
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("serial", userSerial);
			BaseUser user = userService.selectByCondition(map);
			if (null != user) {
				modelMap.put("user", user);
				json.put("user", user);
				modelMap.put("userSerial", user.getSerial());
				json.put("userSerial", user.getSerial());
				/*
				 * map.clear(); map.put("userId", user.getId());
				 * List<BaseForumArticle> forumArticleList =
				 * forumArticleService.selectListByCondition(map);
				 * modelMap.put("forumArticleList", forumArticleList);
				 */
			}
			modelMap.put("paramJson", json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/forumUserArticle/" + userSerial, modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@RequestMapping(value = "/forumArticle/{typeId}")
	public ModelAndView ForumArticle(ModelMap modelMap, HttpSession session, @PathVariable Integer typeId,
			@RequestParam(required = false) String r, HttpServletRequest request) {
		String path = Page.forumArticle;
		try {
			/*
			 * User sessionUser = ActionUtil.checkSession4User(session); if(null
			 * == sessionUser){ return Result.userSessionInvalid(modelMap,
			 * "/forumArticle?typeId="+typeId); }
			 */
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseForum forumOld = forumService.selectByPrimaryKey(typeId);
			if (null != forumOld && !"".equals(forumOld)) {
				Integer num = forumOld.getVisitNum();
				if (num == null) {
					num = 0;
				} else {
					num++;
				}
				forumOld.setVisitNum(num);
				forumService.updateByPrimaryKeySelective(forumOld);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("forumId", typeId);
			List<BaseForumArticle> forumArticleList = forumArticleService.selectListByCondition(map);
			map.clear();
			map.put("id", typeId);
			List<BaseForum> forumNewList = forumService.selectListByCondition(map);
			/*
			 * if(forumArticleList.size() > 0){ String Ids = "";
			 * for(ForumArticle fa:forumArticleList){ Ids
			 * +=","+fa.getId().toString(); } modelMap.put("Ids", Ids); }
			 */
			if (forumNewList.size() > 0) {
				BaseForum forum = forumNewList.get(0);
				modelMap.put("forum", forum);
			}
			// modelMap.put("forumArticleList", forumArticleList);
			modelMap.put("count", forumArticleList.size());
			modelMap.put("typeId", typeId);

		} catch (Exception e) {
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载论坛文章页面出现异常_" + e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}

	@RequestMapping(value = "/fa/{forumArticleId}")
	public ModelAndView forumArticleDetail(ModelMap modelMap, HttpSession session, @PathVariable Integer forumArticleId,
			@RequestParam(required = false, value = "share") String share, @RequestParam(required = false) String r,
			HttpServletRequest request) {
		String path = Page.forumArticledetail;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			/*
			 * if(null == sessionUser){ return
			 * Result.userSessionInvalid(modelMap,
			 * "/forumArticleDetail?forumArticleId="+forumArticleId); }
			 */
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", forumArticleId);
			// List<ForumArticle> forumArticleList =
			// forumArticleService.getForumArticleList(map);
			// TODO 修改
			List<BaseForumArticle> forumArticleList = new ArrayList<BaseForumArticle>();

			if (forumArticleList.size() > 0) {
				BaseForumArticle forumArticle = forumArticleList.get(0);
				BaseForum forum = forumService.selectByPrimaryKey(forumArticle.getForumId());
				map.clear();
				map.put("forumId", forum.getId());
				List<BaseForumArticle> forumArticleCountList = forumArticleService.selectListByCondition(map);
				if (forumArticleCountList.size() > 0) {
					modelMap.put("count", forumArticleCountList.size());
				}
				modelMap.put("forumArticle", forumArticle);
				modelMap.put("forum", forum);
				modelMap.put("forumArticleId", forumArticleId);
				modelMap.put("user", sessionUser);
			}
			if (null != share && !"".equals(share)) {
				modelMap.put("share", share);
			}
		} catch (Exception e) {
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载论坛文章详细页面出现异常_" + e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}

	// TODO 晒好物
	@RequestMapping(value = "/forumArticleAdd/{forumId}/{goodsId}")
	public ModelAndView forumArticleAdd(ModelMap modelMap, HttpServletRequest request, @PathVariable Integer forumId,
			@PathVariable Integer goodsId, @RequestParam(required = false) String r) {
		String path = Page.forumArticleGoodsAddOrUpdate;
		JSONObject json = new JSONObject();
		try {
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/forumArticleAdd/" + forumId + "/" + goodsId;
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			BaseOrderGoods orderGoods = orderGoodsService.selectByPrimaryKeyAndOrder(goodsId);
			BaseForum forum = forumService.selectByPrimaryKey(forumId);

			// 微信验证
			String ticket = AuthAccess.getWxDataFromProperty("ticket");// 从Property文件中获取ticket,如果文件中没有，则会远程获取
			String url = "http://weipaike.net/forumArticleAdd/" + forumId + "/" + goodsId;
			url = ActionUtil.buildPromoterUrl(url, r);
			modelMap = AuthAccess.getSign(modelMap, ticket, url);

			modelMap.put("forumId", forumId);
			modelMap.put("orderGoods", orderGoods);
			modelMap.put("forum", forum);
			json.put("forum", forum);
			json.put("orderGoods", orderGoods);
			modelMap.put("paramJson", json);
			//System.out.println(json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/forumArticleAdd/" + forumId + "/" + goodsId,
					modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	@ResponseBody
	@RequestMapping(value = "/addActicle", method = RequestMethod.POST)
	public JSONObject addActicle(HttpServletRequest request, @ModelAttribute BaseForumArticle article,
			@RequestParam(required = false) String apSerial) {

		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			String basePath = request.getServletContext().getRealPath("/");
			String filePaths = article.getPicPaths();
			//System.out.println("图片地址filePaths： " + filePaths);

			String endpoint = Const.oss_endpoint;
			String accessKeyId = Const.oss_accessKeyId;
			String accessKeySecret = Const.oss_accessKeySecret;
			String bucketName = Const.oss_bucketName;
			String bucketEndpoint = Const.oss_bucketEndpoint;
			AliyunOSS oss = AliyunOSS.buildOSS(null, null, endpoint, accessKeyId, accessKeySecret, bucketName,
					bucketEndpoint);
			String mediaId = filePaths;
			if (Check.isNotNull(mediaId) && !mediaId.equalsIgnoreCase("undefined")) {// 此处为微信服务器获取用户上传的图片
				json = pictureService.getPicPathsByWxServerIds(json, mediaId, basePath, oss);
				String picPaths = json.getString("picPaths");
				if (Check.isNotNull(picPaths)) {
					if (picPaths.startsWith(","))
						picPaths = picPaths.substring(1);
					String localPath = "/file/wx/" + picPaths.substring(picPaths.lastIndexOf("/"), picPaths.length());
					article.setPicPaths(picPaths);
					// apply.setFile2(localPath);
				}
			}
			forumArticleService.addForumArticle(json, article, user.getUsername());
			// forumArticleService.addForumArticle(json, article,
			// user,apSerial,basePath);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/goods/addGoods", json);
		}
		return Result.success(json);
	}

	@RequestMapping(value = "/forumArticleUpdate/{articleId}")
	public ModelAndView forumArticleUpdate(ModelMap modelMap, HttpServletRequest request,
			@PathVariable Integer articleId, @RequestParam(required = false) String r) {
		String path = Page.forumArticleAddOrUpdate;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/forumArticleUpdate/" + articleId;
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (Check.isNull(articleId)) {
				BaseForumArticle forumArticle = forumArticleService.selectByPrimaryKey(articleId);
				modelMap.put("forumArticle", forumArticle);
				json.put("forumArticle", forumArticle);
			}
			modelMap.put("articleId", articleId);
			json.put("articleId", articleId);
			modelMap.put("paramJson", json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/forumArticleUpdate/" + articleId, modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	// TODO 置顶，精华
	@ResponseBody
	@RequestMapping(value = "/updateForumArticle")
	public JSONObject updateForumArticle(ModelMap modelMap, @ModelAttribute BaseForumArticle forumArticle,
			HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}

			forumArticleService.updateByPrimaryKeySelective(forumArticle);
			modelMap.put("paramJson", json);
			json.put("msg", "操作成功");
		} catch (Exception e) {
			String path = Page.error;
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改论坛文章出现异常_", json);
		}
		return Result.success(json);
	}

	// TODO 添加帖子
	@ResponseBody
	@RequestMapping(value = "/addOrUpdateForumArticle", method = RequestMethod.POST)
	public JSONObject addOrUpdateForumArticle(@ModelAttribute BaseForumArticle forumArticle,
			HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			/*
			 * BaseUser db_user = userService.selectByPrimaryKey(user.getId());
			 * if( Check.isNull(db_user.getPhone())){
			 * json.put("jumpToBindUserPhone", "jumpToBindUserPhone"); return
			 * Result.failure(json, "先绑定手机号码,再发布话题", "userPhone_null"); } String
			 * addForumArticle = request.getParameter("addForumArticle");
			 * if(addForumArticle == "yes"){ Map<String,Object> map = new
			 * HashMap<String,Object>(); map.put("userId", user.getId());
			 * map.put("controlTypeId", 53); map.put("controlDate", 1); int
			 * userControlList = userControlService.selectCountByCondition(map);
			 * if(userControlList > 0){ return Result.failure(json,
			 * "您被禁止发帖,请联系管理员解除限制", "userPhone_null"); } }
			 */
			Date date = new Date();
			Integer userId = user.getId();
			String username = user.getUsername();
			String basePath = request.getServletContext().getRealPath("/");

			if (null == forumArticle.getId()) {// 添加
				forumArticle.setUserId(userId);
				forumArticle.setVisitNum(0);
				forumArticle.setPraiseNum(0);
				forumArticle.setMainStatus(1);
				forumArticle.setSerial(CommonGenerator.getSerialByDate(BaseAssets.serial_prefix_forumArticle));
				forumArticle.setCreatedBy(username);
				forumArticle.setCreatedAt(date);

				String picPathsStr = forumArticle.getPicPaths();
				if (Check.isNotNull(picPathsStr)) {
					String filePaths[] = picPathsStr.split(",");
					List<BasePicture> pictureList = new ArrayList<BasePicture>(filePaths.length);

					String endpoint = Const.oss_endpoint;
					String accessKeyId = Const.oss_accessKeyId;
					String accessKeySecret = Const.oss_accessKeySecret;
					String bucketName = Const.oss_bucketName;
					String bucketEndpoint = Const.oss_bucketEndpoint;
					AliyunOSS oss = AliyunOSS.buildOSS(null, null, endpoint, accessKeyId, accessKeySecret, bucketName,
							bucketEndpoint);

					String picUrl = "";
					for (int j = 0; j < filePaths.length; j++) {
						String mediaId = filePaths[j];
						BasePicture picture = new BasePicture();
						picture.setSerial(
								CommonGenerator.getSerialByDate(BaseAssets.serial_prefix_forumArticlePicture));
						picture.setCreatedAt(date);
						picture.setCreatedBy(username);
						String picPaths = json.getString("picPaths");
						if (Check.isNotNull(mediaId) && !mediaId.equalsIgnoreCase("undefined")) {// 此处为微信服务器获取用户上传的图片
							json = pictureService.getPicPathsByWxServerIds(json, mediaId, basePath, oss);

							if (Check.isNotNull(picPaths)) {
								if (picPaths.startsWith(","))
									picPaths = picPaths.substring(1);
								picUrl += picPaths + ",";

								//System.out.println("addOrUpdateForumArticle_图片地址: " + picPaths);

								String localPath = "/file/wx/"
										+ picPaths.substring(picPaths.lastIndexOf("/"), picPaths.length());
								picture.setLocalPicPath(localPath);
								picture.setOssPicPath(picPaths);
								picture.setPicPath(picPaths);
								picture.setIsCover(0);
								if (j == 0) {
									picture.setIsCover(2);
								}
								pictureList.add(picture);

							}
						}
					}
					if (Check.isNotNull(pictureList)) {
						pictureService.insertBatch(pictureList);
					}
					if (picUrl.length() > 0)
						picUrl = picUrl.substring(0, picUrl.length() - 1);
					forumArticle.setPicPaths(picUrl);
				}
				forumArticleService.insert(forumArticle);
			} else {// 修改
				forumArticle.setUpdatedBy(username);
				forumArticle.setUpdatedAt(date);
				forumArticleService.updateByPrimaryKeySelective(forumArticle);
			}
			json.put("id", forumArticle.getId());
		} catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改论坛文章出现异常_", json);
		}
		return Result.success(json);
	}

	@ResponseBody
	@RequestMapping(value = "/getForumArticleList", method = RequestMethod.POST)
	public JSONObject getForumArticleList(HttpServletRequest request, ModelMap modelMap) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			forumArticleService.getForumArticleList(json, map);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/getForumArticleList", json);
		}
		return Result.success(json);
	}

	@ResponseBody
	@RequestMapping(value = "/getForumReplyList", method = RequestMethod.POST)
	public JSONObject getForumReplyList(HttpServletRequest request, ModelMap modelMap) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			// forumArticleService.getForumArticleList(json, map);
			commentService.getCommentList(json, map);
			//System.out.println(json);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/getForumReplyList", json);
		}
		return Result.success(json);
	}

	// 添加帖子
	@RequestMapping(value = "/forumArticleAddOrUpdate/{forumId}")
	public ModelAndView forumArticleAddOrUpdate(ModelMap modelMap, HttpServletRequest request,
			@PathVariable Integer forumId, @RequestParam(required = false) String r) {
		String path = Page.forumArticleAddOrUpdate;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/forumDetail/" + forumId;
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}

			modelMap.put("user", sessionUser);
			json.put("user", sessionUser);
			modelMap.put("forumId", forumId);
			json.put("forumId", forumId);

			// 微信验证
			String ticket = AuthAccess.getWxDataFromProperty("ticket");// 从Property文件中获取ticket,如果文件中没有，则会远程获取
			String url = "http://weipaike.net/forumArticleAddOrUpdate";
			if (Check.isNotNull(forumId)) {
				url += "/" + forumId;
			}
			url = ActionUtil.buildPromoterUrl(url, r);
			//System.out.println("url: " + url);
			modelMap = AuthAccess.getSign(modelMap, ticket, url);

			modelMap.put("paramJson", json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/forumDetail/" + forumId, modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	// 帖子详情
	@RequestMapping(value = "/forumDetail/{forumArticleId}")
	public ModelAndView forumDetail(ModelMap modelMap, HttpServletRequest request, @PathVariable Integer forumArticleId,
			@RequestParam(required = false) String r) {
		String path = Page.forumArticledetail;

		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, false, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/forumDetail/" + forumArticleId;
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			modelMap.put("user", sessionUser);
			json.put("user", sessionUser);
			modelMap.put("forumArticleId", forumArticleId);
			json.put("forumArticleId", forumArticleId);
			modelMap.put("paramJson", json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/forumDetail/" + forumArticleId, modelMap);
		}

		return new ModelAndView(path, modelMap);
	}

	// 获取帖子
	@ResponseBody
	@RequestMapping(value = "/getForumDetail", method = RequestMethod.POST)
	public JSONObject getForumDetail(HttpServletRequest request, ModelMap modelMap,
			@RequestParam Integer forumArticleId) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			if (Check.isNull(forumArticleId)) {
				return Result.failure(json, "123", "forumId_null");
			}
			;
			map.put("id", forumArticleId);

			BaseForumArticle forumArticle = forumArticleService.selectByCondition(map);
			if (null == forumArticle) {
				return Result.failure(json, "345", "forumArticle_null");
			}
			;
			json.put("forumArticle", forumArticle);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/getForumArticleList", json);
		}
		//System.out.println(json);

		return Result.success(json);
	}

	// 获取评论
	@ResponseBody
	@RequestMapping(value = "/getCommentList", method = RequestMethod.POST)
	public JSONObject getCommentList(HttpServletRequest request, ModelMap modelMap,
			@RequestParam Integer forumArticleId) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			/*
			 * if(Check.isNotNull(from) && from.equals("market")){
			 * map.put("orderBy",
			 * "bonus_type_id DESC,go.offer_price DESC,A.created_at");
			 * map.put("ascOrdesc", "DESC"); map.put("forMarket", 1); }
			 */
			
			map.put("objectId", forumArticleId);
			map.put("commentTypeCode", "comment_forumArticle");
			commentService.getCommentList(json, map);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName() + "/getForumArticleList", json);
		}
		//System.out.println(json);

		return Result.success(json);
	}
}
