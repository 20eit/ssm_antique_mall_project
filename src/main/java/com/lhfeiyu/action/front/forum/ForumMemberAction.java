package com.lhfeiyu.action.front.forum;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.base.BaseForumMember;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseForumMemberService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
public class ForumMemberAction {
	@Autowired
	private BaseForumMemberService forumMemberService;
	@Autowired
	private AuthCheckService authCheckService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/myForum")
	public ModelAndView ForumMemberDict(ModelMap modelMap,HttpSession session
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.myForum;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			if(null == sessionUser){
				String jumpUrl = "/myForum";
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
				modelMap.put("userId", sessionUser.getId());
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载我的论坛页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	
	
	
	@ResponseBody
	@RequestMapping(value="/getForumMemberList", method=RequestMethod.POST)
	public JSONObject getForumMemberList(HttpServletRequest request) {
		List<BaseForumMember> forumMemberList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			forumMemberList = forumMemberService.selectListByCondition(map);
			Integer total = forumMemberService.selectCountByCondition(map);
			json.put("rows", forumMemberList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载我的论坛列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/joinForum", method=RequestMethod.POST)
	public JSONObject joinForum(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		//System.out.println(request.getParameter("forumId")+"****"+request.getParameter("userId"));
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			BaseForumMember baseForumMember = new BaseForumMember();
//			baseForumMember.setForumId((Integer) map.get("forumId"));
//			baseForumMember.setUserId((Integer) map.get("userId"));
			baseForumMember.setForumId(Integer.valueOf(request.getParameter("forumId")));
			baseForumMember.setUserId(Integer.valueOf(request.getParameter("userId")));
			forumMemberService.insertSelective(baseForumMember);
			json.put("status", "success");
		} catch (Exception e) {
			json.put("status", "failure");
			Result.catchError(e, logger, "LH_ERROR_加载我的论坛列表出现异常_", json);
		}
		return Result.success(json);
	}
	@ResponseBody
	@RequestMapping(value=" /backForum", method=RequestMethod.POST)
	public JSONObject backForum(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		//System.out.println(request.getParameter("forumId")+"****"+request.getParameter("userId"));
		try {
			Integer userId = Integer.valueOf(request.getParameter("userId"));
			Integer forumId = Integer.valueOf(request.getParameter("forumId"));
			String Condition = "user_id = "+userId+" AND forum_id = "+forumId+"";
			forumMemberService.deleteByCondition(Condition);
			json.put("status", "success");
		} catch (Exception e) {
			json.put("status", "failure");
			Result.catchError(e, logger, "LH_ERROR_加载我的论坛列表出现异常_", json);
		}
		return Result.success(json);
	}
}
