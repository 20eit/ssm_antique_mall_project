package com.lhfeiyu.action.front.article;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.po.base.BaseComment;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.CommentService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/comment")
public class CommentAction {
	
	@Autowired
	private CommentService commentService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@ResponseBody
	@RequestMapping(value="/getCommentList", method=RequestMethod.POST)
	public JSONObject getCommentList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			commentService.getCommentList(json, map);
		} catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/comment/getArticleList", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateComment", method=RequestMethod.POST)
	public JSONObject addOrUpdateComment(@ModelAttribute BaseComment comment, HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());//验证session中的user，存在即返回
			if(null == sessionUser){
				return Result.userSessionInvalid(json);//返回session过期的json提示
			}
			String username = sessionUser.getUsername();
			int userId = sessionUser.getId();
			comment.setUserId(userId);
			if(null == comment.getId()){
				//comment.setCommentTypeCode("comment_forumArticle");
				commentService.addComment(json, comment, username);
				
			}else{
				commentService.updateComment(json, comment, username);
			}
			Result.success(json);
		}catch (Exception e) {
			Result.catchError(e, logger, this.getClass().getName()+"/comment/addOrUpdateComment", json);
		}
		//System.out.println("添加成功："+json);
		return Result.success(json);
	}
	
}
