package com.lhfeiyu.action.front.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.base.BaseConst;
import com.lhfeiyu.po.base.BaseOrderGoods;
import com.lhfeiyu.po.base.BaseOrderInfo;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseOrderGoodsService;
import com.lhfeiyu.service.base.BaseOrderInfoService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
public class OrderInfoAction {
	@Autowired
	private BaseOrderInfoService orderInfoService;
	@Autowired
	private BaseOrderGoodsService orderGoodsService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private BaseUserService userService;

	private static Logger logger = Logger.getLogger("R");
	
	/** 商品付款页面 */
	@RequestMapping(value = "/goodsPay")
	public ModelAndView goodsPay(ModelMap modelMap, HttpServletRequest request, 
			@RequestParam Integer orderGoodsId, @RequestParam(required = false) String r) {
		String path = Page.goodsPay;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/goodsPay";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
			Map<String,Object> map = CommonGenerator.getHashMap();
			map.put("id", orderGoodsId);
			BaseOrderGoods orderGoods = orderGoodsService.selectByCondition(map);
			json.put("orderGoods", orderGoods);
			json.put("orderGoodsId", orderGoods.getId());
			json.put("user", user);
			json.put("userId", sessionUser.getId());
			modelMap.put("orderGoods", orderGoods);
			modelMap.put("user", user);
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/goodsPay", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	/** 全部订单 */
	@RequestMapping(value = "/orderInfoManage")
	public ModelAndView orderInfoManage(ModelMap modelMap, HttpServletRequest request, 
			@RequestParam(required=false)String orderStatus, @RequestParam(required = false) String r) {
		String path = Page.orderInfoManage;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/orderInfoManage";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
			modelMap.put("user", user);
			modelMap.put("userId", sessionUser.getId());
			modelMap.put("orderStatus", orderStatus);
			json.put("userId", sessionUser.getId());
			json.put("orderStatus", orderStatus);
			modelMap.put("paramJson", json);
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/orderInfoManage", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	/** 待付款 */
	@RequestMapping(value = "/waitPayMoney")
	public ModelAndView waitPayMoney(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.waitPayMoney;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/waitPayMoney";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/waitPayMoney", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 待发货 */
	@RequestMapping(value = "/shipping")
	public ModelAndView shipping(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.shipping;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/shipping";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/shipping", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 待收货 */
	@RequestMapping(value = "/shipped")
	public ModelAndView shipped(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.shipped;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/shipped";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/shipped", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 待评价 */
	@RequestMapping(value = "/commenting")
	public ModelAndView commenting(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.commenting;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/commenting";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/commenting", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}

	/** 退货 */
	@RequestMapping(value = "/returnGoods")
	public ModelAndView returnGoods(ModelMap modelMap, HttpServletRequest request, @RequestParam(required = false) String r) {
		String path = Page.returnGoods;
		try {
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(request.getSession());
			if (null == sessionUser) {
				String jumpUrl = "/returnGoods";
				return Result.userSessionInvalid(modelMap, ActionUtil.buildPromoterUrl(jumpUrl, r));
			}
			if (null != sessionUser.getId() && !"".equals(sessionUser.getId())) {
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
			}
			modelMap.put("userId", sessionUser.getId());
		} catch (Exception e) {
			path = Page.error;
			Result.catchError(e, logger, this.getClass().getName() + "/returnGoods", modelMap);
		}
		return new ModelAndView(path, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getOrderCount")
	public JSONObject getOrderCount(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Integer userId = user.getId();
			Map<String, Object> map = new HashMap<String, Object>();
			/*
			 * map.put("offerStatus", 1); map.put("userId", userId); int
			 * goodsOffersCount =
			 * goodsOffersService.selectCountByCondition(map);//买家报价
			 * map.clear();
			 */
			map.put("orderStatusCode", BaseConst.order_status_todo);
			map.put("shippingStatusCode", BaseConst.shipping_status_todo);
			map.put("payStatusCode", BaseConst.pay_status_todo);
			map.put("buyerOrSeller", userId);
			/*map.put("mainStatus", 1);
			map.put("userId", userId);
			map.put("expressStateNotOver", 1);*/
			int waitPayMoneyCount = orderInfoService.selectCountByCondition(map);// 待付款
			map.clear();
			map.put("orderStatusCode", BaseConst.order_status_todo);
			map.put("shippingStatusCode", BaseConst.shipping_status_todo);
			map.put("payStatusCode", BaseConst.pay_status_done);
			map.put("buyerOrSeller", userId);
		/*	map.put("userId", userId);
			map.put("mainStatus", 1);
			map.put("expressStateNotOver", 1);*/
			int shipedCount = orderInfoService.selectCountByCondition(map);// 待收货
			//map.clear();
			//map.put("orderStatusCode", BaseConst.order_status_todo);
			//map.put("shippingStatusCode", BaseConst.shipping_status_done);
			//map.put("payStatusCode", BaseConst.pay_status_done);
			/*map.put("overComment", 1);
			map.put("orderGoodsStatus", 2);
			map.put("userId", user.getId());
			map.put("mainStatus", 1);*/
			// map.put("commentUserId", userId);
			//int shippedCount = orderInfoService.selectCountByCondition(map);// 待评价
			map.clear();
			// map.put("payStatus", 3);
			map.put("buyerOrSeller", userId);
			map.put("orderStatusCode", BaseConst.order_status_apply_return);
			map.put("payStatusCode", BaseConst.shipping_status_done);
		/*	map.put("orderStatus", 6);
			map.put("orderGoodsStatus", 5);
			map.put("sellerOrbuyer", userId);
			map.put("mainStatus", 1);*/
			int returnGoodsCount = orderInfoService.selectCountByCondition(map);// 退货
			map.clear();
			map.put("buyerOrSeller", userId);
			map.put("orderStatusCode", BaseConst.order_status_todo);
			map.put("shippingStatusCode", BaseConst.shipping_status_todo);
			map.put("payStatusCode", BaseConst.pay_status_done);
			/*map.put("orderGoodsStatus", 1);
			map.put("mainStatus", 1);
			map.put("expressStateNotOver", 1);*/
			int shippingCount = orderInfoService.selectCountByCondition(map);// 待发货
			json.put("returnGoodsCount", returnGoodsCount);
			json.put("shippingCount", shippingCount);
			json.put("shipedCount", shipedCount);
			json.put("waitPayMoneyCount", waitPayMoneyCount);
			// json.put("goodsOffersCount", goodsOffersCount);
			//json.put("commentingCount", commentingCount);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取订单数量异常_", json);
		}
		return Result.success(json);
	}


	@ResponseBody
	@RequestMapping(value = "/addOrderInfo", method = RequestMethod.POST)
	public JSONObject addOrUpdateOrderInfo(@ModelAttribute BaseOrderInfo orderInfo, HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Integer userId = user.getId();
			json = orderInfoService.addOrder(json, user, userId, orderInfo);
		} catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_新增订单信息出现异常_", json);
		}
		return Result.success(json);
	}

	@ResponseBody
	@RequestMapping(value = "/getOrderInfoList", method = RequestMethod.POST)
	public JSONObject getOrderInfoList(HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String pageFrom) {
		List<BaseOrderInfo> orderInfoList = null;
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Map<String, Object> map = ActionUtil.getAllParam(request);
			Integer userId = user.getId();
			if (Check.isNotNull(pageFrom)) {
				if (pageFrom.equals("shipping")) {
					map.put("orderStatusCode", BaseConst.order_status_todo);
					map.put("shippingStatusCode", BaseConst.shipping_status_todo);
					map.put("payStatusCode", BaseConst.pay_status_done);
					/*map.put("orderGoodsStatus", 1);
					map.put("shopUserId", userId);
					map.put("expressStateNotOver", userId);
					map.put("mainStatus", 1);*/
				} else if (pageFrom.equals("shipped")) {
					map.put("orderStatusCode", BaseConst.order_status_todo);
					map.put("shippingStatusCode", BaseConst.shipping_status_done);
					map.put("payStatusCode", BaseConst.pay_status_done);
					/*map.put("orderGoodsStatus", 1);
					map.put("userId", userId);
					map.put("expressStateNotOver", userId);
					map.put("mainStatus", 1);*/
				} else if (pageFrom.equals("waitPayMoney")) {
					map.put("orderStatusCode", BaseConst.order_status_todo);
					map.put("shippingStatusCode", BaseConst.shipping_status_todo);
					map.put("payStatusCode", BaseConst.pay_status_todo);
					/*map.put("orderGoodsStatus", 1);
					map.put("userId", userId);
					map.put("expressStateNotOver", userId);
					map.put("mainStatus", 1);*/
				} else if (pageFrom.equals("returnGoods")) {
					map.put("orderStatusCode", BaseConst.order_status_apply_return);
					map.put("payStatusCode", BaseConst.shipping_status_done);
					/*map.put("orderGoodsStatus", 5);
					map.put("sellerOrbuyer", userId);
					map.put("orderInfoMainStatus", 1);*/
				}else if (pageFrom.equals("done")) {
					map.put("orderStatusCode", BaseConst.order_status_done);
					map.put("shippingStatusCode", BaseConst.shipping_status_done);
					map.put("payStatusCode", BaseConst.pay_status_done);
					map.put("orderDoneStausCode", BaseConst.order_done_status_yes);
				}else if (pageFrom.equals("showdown")) {
					map.put("orderStatusShutdown", 1);
				}
			}
			orderInfoList = orderInfoService.selectListByCondition(map);
			Integer total = orderInfoService.selectCountByCondition(map);
			json.put("rows", orderInfoList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取订单信息出现异常_", json);
		}
		return Result.success(json);
	}

	/** 设置订单的收货地址 */
	@ResponseBody
	@RequestMapping(value = "/updateOrderAddress", method = RequestMethod.POST)
	public JSONObject updateOrderAddress(HttpServletRequest request, HttpSession session, 
			@RequestParam Integer orderGoodsId, @RequestParam Integer addressId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示 }
			}
			json =orderInfoService.updateOrderAddress(json, user, orderGoodsId,addressId);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_为订单商品付款出现异常_", json);
		}
		return Result.success(json);
	}

	/** 付款（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/payMoneyForOrderGoods", method = RequestMethod.POST)
	public JSONObject payMoneyForOrderGoods(HttpServletRequest request, HttpSession session, 
			@RequestParam Integer orderGoodsId, @RequestParam String payPass, @RequestParam Integer addressId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示 }
			}
			//Integer userId = user.getId();
			json = orderInfoService.payMoney(json, user, payPass,orderGoodsId);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_为订单商品付款出现异常_", json);
		}
		return Result.success(json);
	}

	/** 发货（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/sendGoods", method = RequestMethod.POST)
	public JSONObject sendGoods(HttpServletRequest request, HttpSession session, 
			@RequestParam Integer orderGoodsId, @RequestParam Integer expressId, @RequestParam String expressOrder) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示 }
			}
			json = orderInfoService.sendGoods(user, json, orderGoodsId, expressId, expressOrder);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_发货出现异常_", json);
		}
		return Result.success(json);
	}

	/** 确认收货（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/receiveGoods", method = RequestMethod.POST)
	public JSONObject receiveGoods(HttpServletRequest request, HttpSession session, @RequestParam Integer orderId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示 }
			}
			//Integer userId = user.getId();
			json = orderInfoService.receiveGoods(user, orderId, json);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_确认收货出现异常_", json);
		}
		return Result.success(json);
	}

	/** 取消订单（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/cancelOrder", method = RequestMethod.POST)
	public JSONObject cancelOrder(HttpServletRequest request, HttpSession session, @RequestParam Integer orderId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示 }
			}
			// Integer userId = user.getId();
			json = orderInfoService.cancelOrder(user, orderId, json);
			if (json.containsKey("error_desc")) {
				json.put("status", "failure");
				return Result.success(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_取消订单出现异常_", json);
		}
		return Result.success(json);
	}

	/** 申请退货（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/applyReturnOrderGoods", method = RequestMethod.POST)
	public JSONObject applyReturnOrderGoods(HttpServletRequest request, HttpSession session, @RequestParam Integer orderGoodsId) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			} // Integer userId = user.getId(); json =
			orderInfoService.applyReturnOrderGoods(user, orderGoodsId, json);
			if (json.containsKey("error_desc")) {
				json.put("status", "failure");
				return Result.success(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_申请退货出现异常_", json);
		}
		return Result.success(json);
	}

	/** 处理退货（订单商品） */
	@ResponseBody
	@RequestMapping(value = "/returnOrderGoods", method = RequestMethod.POST)
	public JSONObject returnOrderGoods(HttpServletRequest request, HttpSession session, @RequestParam Integer orderGoodsId, @RequestParam Integer opt) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			// Integer userId = user.getId();
			json = orderInfoService.returnOrderGoods(user, orderGoodsId, opt, json);
			if (json.containsKey("error_desc")) {
				json.put("status", "failure");
				return Result.success(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_处理退货出现异常_", json);
		}
		return Result.success(json);
	}

	@ResponseBody
	@RequestMapping(value = "/deleteOrderInfo", method = RequestMethod.POST)
	public JSONObject deleteOrderInfo(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		try {
			BaseUser user = ActionUtil.checkSession4User(request.getSession());// 验证session中的user，存在即返回
			if (null == user) {
				return Result.userSessionInvalid(json);// 返回session过期的json提示
			}
			Integer userId = user.getId();
			Integer orderId = Integer.valueOf(request.getParameter("orderId"));
			String overCommented = request.getParameter("overCommented");
			if (null != orderId) {
				BaseOrderInfo orderInfo = orderInfoService.selectByPrimaryKey(orderId);
				Integer db_userId = orderInfo.getUserId();
				if (db_userId != userId) {
					json.put("msg", "对不起,您没有删除该订单信息的权限");
					json.put("status", "failure");
					return Result.success(json);
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("orderId", orderId);
					map.put("deleteOrder", 1);
					if (overCommented != null) {
						map.put("mainStatus", 2);
					} else {
						map.put("mainStatus", 3);
					}
					List<BaseOrderGoods> orderGoodsList = orderGoodsService.selectListByCondition(map);
					if (orderGoodsList.size() > 0) {
						// OrderGoods orderGoods = orderGoodsList.get(0);
						// Integer orderGoodsId = orderGoods.getId();
						// orderGoodsService.deleteByPrimaryKey(orderGoodsId);//先删除订单商品信息
						orderInfo.setMainStatus(2);
						orderInfoService.updateByPrimaryKeySelective(orderInfo);// 再删除订单信息
						json.put("status", "success");
					} else {
						json.put("msg", "对不起,无法查询到您要删除的订单信息");
						json.put("status", "failure");
						return Result.success(json);
					}
				}
			} else {
				json.put("msg", "对不起,无法查询到您要删除的订单信息");
				json.put("status", "failure");
				return Result.success(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除用户订单信息出现异常_", json);
		}
		return Result.success(json);
	}

}
