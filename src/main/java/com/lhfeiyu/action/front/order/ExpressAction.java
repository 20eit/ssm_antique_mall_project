package com.lhfeiyu.action.front.order;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.base.BaseExpress;
import com.lhfeiyu.po.base.BaseOrderGoods;
import com.lhfeiyu.po.base.BaseOrderInfo;
import com.lhfeiyu.po.base.BaseProvinceCityArea;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserAddress;
import com.lhfeiyu.service.AuthCheckService;
import com.lhfeiyu.service.base.BaseExpressService;
import com.lhfeiyu.service.base.BaseOrderGoodsService;
import com.lhfeiyu.service.base.BaseOrderInfoService;
import com.lhfeiyu.service.base.BaseProvinceCityAreaService;
import com.lhfeiyu.service.base.BaseUserAddressService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.thirdparty.kuaidi.pojo.NoticeRequest;
import com.lhfeiyu.thirdparty.kuaidi.pojo.NoticeResponse;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;
@Controller
public class ExpressAction {
	@Autowired
	private BaseExpressService  expressService;
	@Autowired
	private BaseUserAddressService userAddressService;
	@Autowired
	private AuthCheckService authCheckService;
	@Autowired
	private BaseUserService userService;
	@Autowired
	private BaseProvinceCityAreaService provinceCityAreaService;
	@Autowired
	private BaseOrderGoodsService orderGoodsService;
	@Autowired
	private BaseOrderInfoService orderInfoService;
	
	private static Logger logger = Logger.getLogger("R");
	
	/**收货地址*/
	@RequestMapping(value="/addReceiveAddress")
	public ModelAndView addReceiveAddress(ModelMap modelMap,HttpSession session,
			@RequestParam(required = false,value="id") Integer id
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.addReceiveAddress;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser sessionUser = ActionUtil.checkSession4User(session);
			if(null == sessionUser){
				 String jumpUrl = "/addReceiveAddress?id="+id;
				if(null != r)jumpUrl += "&r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			if(null != sessionUser.getId() && !"".equals(sessionUser.getId())){
				BaseUser user = userService.selectByPrimaryKey(sessionUser.getId());
				modelMap.put("user", user);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("allProvince", 1);
				List<BaseProvinceCityArea> provinceList = provinceCityAreaService.selectListByCondition(map);
				modelMap.put("provinceList", provinceList);
			}
			if(null != id && !"".equals(id)){
				BaseUserAddress userAddress = userAddressService.selectByPrimaryKey(id);
				modelMap.put("userAddress", userAddress);
				modelMap.put("id", id);
			}
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载收货地址页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	/**选择发货地址**/
	@RequestMapping(value="/payOrder/{orderGoodsId}")
	public ModelAndView payOrder(ModelMap modelMap,HttpSession session,HttpServletRequest request,
			@PathVariable Integer orderGoodsId ,@RequestParam(required=false) String r) {
		String path = Page.payOrder;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl =  "/payOrder/"+orderGoodsId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			json.put("userId", user.getId());
			json.put("orderGoodsId", orderGoodsId);
			modelMap.put("userId", user.getId());
			modelMap.put("orderGoodsId", orderGoodsId);
			modelMap.put("paramJson", json);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载账户信息页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	/** 发货-添加快递单号  **/
	@RequestMapping(value="/goodsSend/{orderId}/{orderGoodsId}")
	public ModelAndView goodsSend(ModelMap modelMap,HttpSession session,
			@PathVariable Integer orderId, @PathVariable Integer orderGoodsId
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.goodsSend;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl =  "/goodsSend/"+orderId+"/"+orderGoodsId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			BaseOrderInfo orderInfo = orderInfoService.selectByPrimaryKey(orderId);
			//TODO 修改
			modelMap.put("orderId", orderId);
			modelMap.put("orderGoodsId", orderGoodsId);
			modelMap.put("orderInfo", orderInfo);
			
			//TODO 加载快递公司,省市区
			Map<String,Object> map = CommonGenerator.getHashMap();
			map.put("orderBy", "sequence");
			map.put("ascOrdesc", "asc");
			List<BaseExpress> expressList = expressService.selectListByCondition(map);
			modelMap.put("expressList", expressList);
			
			json.put("orderId", orderId);
			json.put("orderGoodsId", orderGoodsId);
			modelMap.put("paramJson", json);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载添加快递单号页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}
	
	/** 查看物流详细数据  **/
	@RequestMapping(value="/scanExpressJson/{orderGoodsId}")
	public ModelAndView scanExpressJson(ModelMap modelMap,HttpSession session,
			@PathVariable Integer orderGoodsId
			,@RequestParam(required=false) String r,HttpServletRequest request) {
		String path = Page.expressJsonScan;
		try{
			JSONObject json = new JSONObject();
			modelMap = authCheckService.checkWxLogin(request, modelMap, json, true, r);
			BaseUser user = ActionUtil.checkSession4User(session);
			if(null == user){
				String jumpUrl =  "/scanExpressJson/"+orderGoodsId;
				if(null != r)jumpUrl += "?r="+r;
				return Result.userSessionInvalid(modelMap, jumpUrl);
			}
			Map<String,Object> map = CommonGenerator.getHashMap();
			map.put("id", orderGoodsId);
			BaseOrderGoods orderGoods = orderGoodsService.selectByCondition(map);
			String expressJson = orderGoods.getExpressJson();
			expressJson = expressJson.replace("\\", "");
			//orderGoods.setKuaidiResult(JSONObject.parseObject(expressJson, com.lhfeiyu.thirdparty.kuaidi.pojo.Result.class));
			orderGoods.setExpressJson(expressJson);
			String status = orderGoods.getExpressState();
			String expressStateName = "";//快递单当前签收状态，包括0在途中、1已揽收、2疑难、3已签收、4退签、5同城派送中、6退回、7转单 -->
			if("0".equals(status)){
				expressStateName = "在途中";
			}else if("1".equals(status)){
				expressStateName = "已揽收";
			}else if("2".equals(status)){
				expressStateName = "疑难";
			}else if("3".equals(status)){
				expressStateName = "已签收";
			}else if("4".equals(status)){
				expressStateName = "退签";
			}else if("5".equals(status)){
				expressStateName = "同城派送中";
			}else if("6".equals(status)){
				expressStateName = "退回";
			}else if("7".equals(status)){
				expressStateName = "转单";
			}
			orderGoods.setExpressStateName(expressStateName);
			//TODO 修改
			modelMap.put("orderGoodsId", orderGoodsId);
			modelMap.put("orderGoods", orderGoods);
			
			json.put("orderId", orderGoodsId);
			json.put("orderGoods", orderGoods);
			modelMap.put("paramJson", json);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载添加快递单号页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path, modelMap);
	}

	/** 获取快递公司数组 **/
	@ResponseBody
	@RequestMapping(value="/getExpressArray")
	public JSONArray getExpressArray(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		try {
			Map<String,Object> map = CommonGenerator.getHashMap();
			map.put("orderBy", "sequence");
			map.put("ascOrdesc", "asc");
			List<BaseExpress> expressList = expressService.selectListByCondition(map);
			for(BaseExpress e : expressList){
				JSONObject json = new JSONObject();
				json.put("id", e.getId());
				json.put("name", e.getBriefName());
				json.put("code", e.getCode());//快递公司编码
				array.add(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取快递公司数组出现异常_", array);
		}
		return array;
	}
	
	/** 快递100推送回调接口 **/
	@ResponseBody
	@RequestMapping(value="/kuaidi/callback", method=RequestMethod.POST)
	public JSONObject kuaidiCallback(HttpServletRequest request, HttpServletResponse response
			,@RequestParam String param) {
		NoticeResponse resp = new NoticeResponse();
		resp.setResult(false);
		resp.setReturnCode("500");
		resp.setMessage("保存失败");
		try {
			NoticeRequest nReq = JSONObject.parseObject(param, NoticeRequest.class);
			//System.out.println("/kuaidi/callback-param: "+param);
			com.lhfeiyu.thirdparty.kuaidi.pojo.Result result = nReq.getLastResult();
			// 处理快递结果
			String expressOrder = result.getNu();
			if(!Check.isNotNull(expressOrder)){
				response.getWriter().print(JSONObject.toJSON(resp));return null;
			}
			boolean flag = expressService.kuaidiCallback(expressOrder, nReq);
			if(!flag){
				response.getWriter().print(JSONObject.toJSON(resp));return null;
			}
			resp.setResult(true);
			resp.setReturnCode("200");
			resp.setMessage("提交成功");
			response.getWriter().print(JSONObject.toJSON(resp)); //这里必须返回，否则认为失败，过30分钟又会重复推送。
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("LH_ERROR_快递100推送数据接口出现异常_"+e.getMessage());
			resp.setMessage("保存失败" + e.getMessage());
			try {
				response.getWriter().print(JSONObject.toJSON(resp));//保存失败，服务端等30分钟会重复推送。
			} catch (IOException e1) {
				e1.printStackTrace();
				logger.error("LH_ERROR_快递100推送数据接口返回失败状态出现异常_"+e.getMessage());
			}
		}
		return null;
	}

	
}
