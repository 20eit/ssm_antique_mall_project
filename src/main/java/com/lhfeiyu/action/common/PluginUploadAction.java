//package com.lhfeiyu.action.common;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.alibaba.fastjson.JSONObject;
//import com.lhfeiyu.config.Const; 
//import com.lhfeiyu.domain.base.AliyunOSS;
//import com.lhfeiyu.service.base.BasePictureService;
//import com.lhfeiyu.tools.base.Check;
//import com.lhfeiyu.tools.base.Result;
//
///**
// * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 基础库-控制层-通用：上传组件
// * <p>
// * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华
// * <p>
// * <strong> 编写时间：</strong> 2016年4月12日12:06:03
// * <p>
// * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com
// * <p>
// * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0
// * <p>
// * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong>
// * <p>
// */
//@Controller
//public class PluginUploadAction {
//
//	@Autowired
//	private BasePictureService pictureService;
//
//	private static Logger logger = Logger.getLogger("R");
//
//	@ResponseBody
//	@RequestMapping(value = "/uploadToOSSByWxServerId", method = RequestMethod.POST)
//	public JSONObject uploadToOSSByWxServerId(HttpServletRequest request, @RequestParam String serverId) {
//		JSONObject json = new JSONObject();
//		try {
//			String basePath = request.getServletContext().getRealPath("/");
//			String endpoint = Const.oss_endpoint;
//			String accessKeyId = Const.oss_accessKeyId;
//			String accessKeySecret = Const.oss_accessKeySecret;
//			String bucketName = Const.oss_bucketName;
//			String bucketEndpoint = Const.oss_bucketEndpoint;
//			AliyunOSS oss = AliyunOSS.buildOSS(null, null, endpoint, accessKeyId, accessKeySecret, bucketName, bucketEndpoint);
//
//			json = pictureService.getPicPathsByWxServerIds(json, serverId, basePath, oss);
//			// json.put("picPaths", picPaths);
//			String picPaths = json.getString("picPaths");
//			if (Check.isNotNull(picPaths)) {
//				if (picPaths.startsWith(",")) {
//					picPaths = picPaths.substring(1);
//				}
//				int index = picPaths.lastIndexOf(",");
//				if (index > 0) {
//					picPaths = picPaths.substring(index, picPaths.length());
//				}
//			}
//			json.put("picPaths", picPaths);
//		} catch (Exception e) {
//			Result.catchError(e, logger, this.getClass().getName() + "/uploadToOSSByWxServerId", json);
//		}
//		return Result.success(json);
//	}
//}
