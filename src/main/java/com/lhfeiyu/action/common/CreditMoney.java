package com.lhfeiyu.action.common;

import java.math.BigDecimal;

import org.springframework.ui.ModelMap;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.tools.base.Check;

public class CreditMoney {
	
	public static ModelMap checkCreditMoneyForAP(AuctionProfession profession, ModelMap modelMap, JSONObject json, BaseUserFund uf, String path){
		BigDecimal creditMoney = uf.getOtherFund();
		BigDecimal avaliableMoney = uf.getAvaliableMoney();
		if(null == creditMoney)creditMoney = new BigDecimal(0);
		if(null == avaliableMoney)avaliableMoney = new BigDecimal(0);
		double userBail = 0;
		if(null == profession.getBail()){
			profession.setBail(new BigDecimal(0));
		}
		userBail = profession.getBail().doubleValue()/10;
		profession.setUserBail(new BigDecimal(userBail));
		//double leftMoney = profession.getBail().doubleValue();
		double creditMoneyShow = creditMoney.doubleValue();
		modelMap.put("creditMoney", creditMoneyShow);
		json.put("creditMoney", creditMoneyShow);
		double leftMoney = userBail;
		if(creditMoney.doubleValue() == 0){
			modelMap.put("leftMoney", leftMoney);
			json.put("leftMoney", leftMoney);
			path = Page.creditMoney;
		}else if(creditMoney.doubleValue() < userBail){
			leftMoney = leftMoney - creditMoney.doubleValue();
			modelMap.put("leftMoney", leftMoney);
			json.put("leftMoney", leftMoney);
			path = Page.creditMoney;
		}
		if(Check.isNotNull(uf.getPayPassword())){
			modelMap.put("payPass", 1);
			json.put("payPass", 1);
			modelMap.put("avaliableMoney", avaliableMoney.doubleValue());
			json.put("avaliableMoney", avaliableMoney.doubleValue());
		}
		return modelMap;
	}
	
	
	
}
