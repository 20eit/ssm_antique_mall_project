/*package com.lhfeiyu.action.back.wholesale;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.Admin;
import com.lhfeiyu.po.Wholesale;
import com.lhfeiyu.service.WholesaleService;
import com.lhfeiyu.service.base.AA_UtilService;
import com.lhfeiyu.service.base.SysDictService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;

@Controller
@RequestMapping(value="/back")
public class BackWholesaleAction {
	@Autowired
	private WholesaleService  wholesaleService;
	@Autowired
	private SysDictService  sysDictService;
	@Autowired
	private AA_UtilService  utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/wholesale")
	public ModelAndView Wholesale(ModelMap modelMap,HttpSession session){
		String path = Page.backWholesale;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载批发城页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@RequestMapping(value="/wholesaleAddOrUpdate")
	public ModelAndView WholesaleAddOrUpdate(ModelMap modelMap,HttpSession session,
			@RequestParam(required=false,value="id") Integer id){
		String path = Page.backWholesaleAddOrUpdate;
		try{
			modelMap.put("id", id);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载添加或修改批发城页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateWholesale", method=RequestMethod.POST)
	public JSONObject addOrUpdateUser(@ModelAttribute Wholesale wholesale,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			wholesale.setMainStatus(1);
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			if(null == admin){
				return Result.adminSessionInvalid(json);//返回session过期的json提示
			}
			if(null == wholesale.getId()){//添加
				if(null == wholesale.getCreatedAt()){
					wholesale.setCreatedAt(new Date()); 
				}
				String serial = CommonGenerator.getSerialByDate("w");
				wholesale.setSerial(serial);
				wholesale.setGoodsPrivilege(1);
				wholesale.setCreatedBy(admin.getUsername());
				wholesaleService.insert(wholesale);
				json = sysDictService.incomeMoney(new BigDecimal(1000),6, json);
				if(json.equals(null)){
					return Result.success(json);
				}else{
					json.put("status", "success");
					json.put("msg", "操作成功");
				}
			}else{//修改
				if(null == wholesale.getUpdatedAt()){
					wholesale.setUpdatedAt(new Date()); 
				}
				wholesale.setUpdatedBy(admin.getUsername());
				wholesaleService.updateByPrimaryKeySelective(wholesale);
				json.put("status", "success");
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_添加或修改批发城数据出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getWholesaleList",method=RequestMethod.POST)
	public JSONObject getWholesaleList(HttpServletRequest request) {
		List<Wholesale> wholesaleList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			wholesaleList = wholesaleService.selectListByCondition(map);
			Integer total = wholesaleService.selectCountByCondition(map);
			json.put("rows", wholesaleList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载批发城列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateWholesaleDelete",method=RequestMethod.POST)
	public JSONObject updateWholesaleDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.wholesale);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除批发城出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteWholesaleThorough",method=RequestMethod.POST)
	public JSONObject deleteWholesaleThorough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.wholesale);
			map.put("username",admin.getUsername());
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除批发城出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateWholesaleRecover",method=RequestMethod.POST)
	public JSONObject updateWholesaleRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.wholesale);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复批发城出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getWholesale")
	public JSONArray getWholesale(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		try {
			List<Wholesale> WholesaleList = wholesaleService.selectListByCondition(null);
			for(Wholesale w:WholesaleList){
				JSONObject json = new JSONObject();
				json.put("id",w.getId());
				json.put("name",w.getName());
				array.add(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
}
*/