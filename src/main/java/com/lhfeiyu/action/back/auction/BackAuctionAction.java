package com.lhfeiyu.action.back.auction;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.po.Auction;
import com.lhfeiyu.service.AuctionService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackAuctionAction {
	@Autowired
	private AuctionService auctionService;
	private static Logger logger = Logger.getLogger("R");
	
	@ResponseBody
	@RequestMapping(value="/getAuctionList",method=RequestMethod.POST)
	public JSONObject getAuctionList(HttpServletRequest request) {
		List<Auction> auctionList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionList = auctionService.selectListByCondition(map);
			json.put("rows", auctionList);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载拍场列表出现异常_", json);
		}
		return Result.success(json);
	}
	
}
