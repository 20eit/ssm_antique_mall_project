package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.AuctionGoods;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.service.AuctionGoodsService;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.AuctionProfessionService;
import com.lhfeiyu.service.GoodsService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackAuctionGoods {
	@Autowired
	private AuctionGoodsService	auctionGoodsService;
	@Autowired
	private AuctionInstService auctionInstService;
	@Autowired
	private AuctionProfessionService auctionProfessionService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private BaseAA_UtilService utilService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/page/auctionGoods", method=RequestMethod.GET)
	public ModelAndView backAuctionGoodsInfo(ModelMap modelMap,HttpServletRequest request) {
		return new ModelAndView(Page.backAuctionGoods);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionGoods")
	public JSONArray getAuctionInst(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		
		try {
			List<BaseGoods> goodsList = goodsService.selectListByCondition(null);
			for(BaseGoods u : goodsList){
				JSONObject json = new JSONObject();
				json.put("id",u.getId());
				json.put("name",u.getGoodsName());
				array.add(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", array);
		}
		return array;
	}
	@ResponseBody
	@RequestMapping(value="/getAuctionProssion")
	public JSONArray getAuctionProssion(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		
		try {
			List<AuctionProfession> auctionProfessionList = auctionProfessionService.selectListByCondition(null);
			for(AuctionProfession u : auctionProfessionList){
				JSONObject json = new JSONObject();
				json.put("id",u.getId());
				json.put("name",u.getAuctionName());
				array.add(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", array);
		}
		return array;
	}
	
	
	
	@ResponseBody
	@RequestMapping(value="/getAuctionGoodsList",method=RequestMethod.POST)
	public JSONObject getAuctionGoodsList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionGoods> auctionGoodsList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionGoodsList = auctionGoodsService.selectListByCondition(map);
			Integer total = auctionGoodsService.selectCountByCondition(map);
			json.put("rows", auctionGoodsList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionGoods", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionGoods(@ModelAttribute AuctionGoods auctionGoods,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			//auctionGoods.setUpdatedBy(admin.getAuctionGoodsname());
			Date d = new Date();
			if(null != auctionGoods.getId()){//更新
				auctionGoods.setUpdatedAt(d); 
				auctionGoods.setUpdatedBy(admin.getUsername());
				auctionGoodsService.updateByPrimaryKeySelective(auctionGoods);
			}else{//新增
				auctionGoods.setCreatedAt(d); 
				auctionGoods.setGoodsSerial(CommonGenerator.getSerialByDate("ag"));
				auctionGoods.setCreatedBy(admin.getUsername());
				auctionGoodsService.insert(auctionGoods);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionGoodsDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionGoodsDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_goods);
			map.put("ids", ids);
			//map.put("auctionGoodsname",auctionGoods.getAuctionGoodsname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionGoodsThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionGoodsThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_goods);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionGoodsRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionGoodsRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_goods);
			map.put("ids", ids);
			//map.put("auctionGoodsname",auctionGoods.getAuctionGoodsname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
}
