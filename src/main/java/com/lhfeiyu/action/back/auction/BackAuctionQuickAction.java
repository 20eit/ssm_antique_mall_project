/*package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.Admin;
import com.lhfeiyu.po.AuctionQuick;
import com.lhfeiyu.service.AuctionQuickService;
import com.lhfeiyu.service.base.AA_UtilService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;

@Controller
@RequestMapping(value="/back")
public class BackAuctionQuickAction {
	@Autowired
	private AuctionQuickService auctionQuickService;
	@Autowired
	private AA_UtilService utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/auctionQuick")
	public ModelAndView accountLog(ModelMap modelMap,HttpServletRequest request){
		String path = Page.backAuctionQuick;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载即时拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickList",method=RequestMethod.POST)
	public JSONObject getAuctionQuickList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionQuick> auctionQuickList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionQuickList = auctionQuickService.selectListByCondition(map);
			Integer total = auctionQuickService.selectCountByCondition(map);
			json.put("rows", auctionQuickList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取即时拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionQuick", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionQuick(@ModelAttribute AuctionQuick auctionQuick,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			//auctionQuick.setUpdatedBy(admin.getAuctionQuickname());
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Date d = new Date();
			if(null != auctionQuick.getId()){//更新
				String auctionSerial = CommonGenerator.getSerialByDate("aq");
				auctionQuick.setAuctionSerial(auctionSerial);
				auctionQuick.setUpdatedAt(d); 
				auctionQuick.setUpdatedBy(admin.getUsername());
				auctionQuick.setMainStatus(1);//启用
				auctionQuickService.updateByPrimaryKeySelective(auctionQuick);
				BigDecimal moneyFlag = auctionQuick.getSpreadPackets();
				Integer userId = auctionQuick.getUserId();
				if(null != moneyFlag){//推广金额不为空 冻结推广金额
					json = cfService.freezeMoney(null, moneyFlag, userId, user.getUsername(), 151, null, "平台冻结", json, false);
					if(json.containsKey("error_desc")){ return Result.success(json); }
				}
				json = sysDictService.incomeMoney(new BigDecimal(1000),6, json);//扣除申请费用
				if(json.equals(null)){
					return Result.success(json);
				}else{
					json.put("status", "success");
					json.put("msg", "操作成功");
				}
			}else{//新增
				auctionQuick.setCreatedAt(d); 
				auctionQuick.setCreatedBy(admin.getUsername());
				auctionQuickService.insert(auctionQuick);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改即时拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_quick);
			map.put("ids", ids);
			//map.put("auctionQuickname",auctionQuick.getAuctionQuickname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除即时拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionQuickThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionQuickThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_quick);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除即时拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_quick);
			map.put("ids", ids);
			//map.put("auctionQuickname",auctionQuick.getAuctionQuickname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复即时拍出现异常_", json);
		}
		return Result.success(json);
	}
	
}
*/