/*package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.Admin;
import com.lhfeiyu.po.AuctionQuickGoods;
import com.lhfeiyu.service.AuctionQuickGoodsService;
import com.lhfeiyu.service.base.AA_UtilService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;
@Controller
@RequestMapping(value="/back")
public class BackAuctionQuickGoods {
	@Autowired
	private AuctionQuickGoodsService	auctionQuickGoodsService;
	@Autowired
	private AA_UtilService utilService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/auctionQuickGoods", method=RequestMethod.GET)
	public ModelAndView backAuctionQuickGoodsInfo(ModelMap modelMap,HttpServletRequest request) {
		return new ModelAndView(Page.backAuctionQuickGoods);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickGoodsList",method=RequestMethod.POST)
	public JSONObject getAuctionQuickGoodsList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionQuickGoods> auctionQuickGoodsList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionQuickGoodsList = auctionQuickGoodsService.selectListByCondition(map);
			Integer total = auctionQuickGoodsService.selectCountByCondition(map);
			json.put("rows", auctionQuickGoodsList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionQuickGoods", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionQuickGoods(@ModelAttribute AuctionQuickGoods auctionQuickGoods,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			//auctionQuickGoods.setUpdatedBy(admin.getAuctionQuickGoodsname());
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Date d = new Date();
			if(null != auctionQuickGoods.getId()){//更新
				auctionQuickGoods.setUpdatedAt(d); 
				auctionQuickGoods.setUpdatedBy(admin.getUsername());
				auctionQuickGoodsService.updateByPrimaryKeySelective(auctionQuickGoods);
			}else{//新增
				auctionQuickGoods.setCreatedAt(d); 
				auctionQuickGoods.setMainStatus(1);
				auctionQuickGoods.setCreatedBy(admin.getUsername());
				auctionQuickGoodsService.insert(auctionQuickGoods);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickGoodsDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickGoodsDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_shop_goods);
			map.put("ids", ids);
			//map.put("auctionQuickGoodsname",auctionQuickGoods.getAuctionQuickGoodsname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionQuickGoodsThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionQuickGoodsThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_shop_goods);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickGoodsRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickGoodsRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_shop_goods);
			map.put("ids", ids);
			//map.put("auctionQuickGoodsname",auctionQuickGoods.getAuctionQuickGoodsname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复拍卖商品出现异常_", json);
		}
		return Result.success(json);
	}
}
*/