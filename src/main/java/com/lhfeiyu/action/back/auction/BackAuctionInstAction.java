package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionInstService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;
@Controller
@RequestMapping(value="/back")
public class BackAuctionInstAction {
	@Autowired
	private AuctionInstService auctionInstService;
	@Autowired
	private BaseAA_UtilService utilService;
	@Autowired
	private BaseUserService userService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/page/auctionInst", method=RequestMethod.GET)
	public ModelAndView backAuctionInstInfo(ModelMap modelMap,HttpServletRequest request) {
		return new ModelAndView(Page.backAuctionInst);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionInstList",method=RequestMethod.POST)
	public JSONObject getAuctionInstList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionInst> auctionInstList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			 if(null!=map.get("userId")){
			BaseUser user= userService.selectByCondition(map);
			 map.put("userId", user.getId());
			 }
			auctionInstList = auctionInstService.selectListByCondition(map);
			Integer total = auctionInstService.selectCountByCondition(map);
			json.put("rows", auctionInstList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", json);
		}
		
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionInst")
	public JSONArray getAuctionInst(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		
		try {
			List<AuctionInst> auctionInstList = auctionInstService.selectListByCondition(null);
			for(AuctionInst u : auctionInstList){
				JSONObject json = new JSONObject();
				json.put("id",u.getId());
				json.put("name",u.getName());
				array.add(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取拍卖机构出现异常_", array);
		}
	
		return array;
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionInst", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionInst(@ModelAttribute AuctionInst auctionInst,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			//auctionInst.setUpdatedBy(admin.getAuctionInstname());
			Date d = new Date();
			if(null != auctionInst.getId()){//更新
				auctionInst.setUpdatedAt(d); 
				auctionInst.setUpdatedBy(admin.getUsername());
				auctionInstService.updateByPrimaryKeySelective(auctionInst);
			}else{//新增
				String picPaths = auctionInst.getPicPaths();//如果没有上传拍卖机构图片,则使用用户的头像
				if(null != picPaths && !"".equals(picPaths)){
					auctionInst.setPicPaths(picPaths);
				}else{
					int userId = auctionInst.getUserId();
					BaseUser user = userService.selectByPrimaryKey(userId);
					if(null != user){
						auctionInst.setPicPaths(user.getAvatar());
					}
				}
				auctionInst.setCreatedAt(d); 
				auctionInst.setSerial(CommonGenerator.getSerialByDate("ai"));
				auctionInst.setCreatedBy(admin.getUsername());
				auctionInstService.insert(auctionInst);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionInstDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionInstDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_inst);
			map.put("ids", ids);
			//map.put("auctionInstname",auctionInst.getAuctionInstname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionInstThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionInstThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_inst);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionInstRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionInstRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_inst);
			map.put("ids", ids);
			//map.put("auctionInstname",auctionInst.getAuctionInstname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复拍卖机构出现异常_", json);
		}
		return Result.success(json);
	}
	
}
