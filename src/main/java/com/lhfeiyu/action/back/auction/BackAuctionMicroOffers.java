package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.AuctionMicroOffers;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.AuctionMicroOffersService;
import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.GoodsService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseUserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackAuctionMicroOffers {
	@Autowired
	private AuctionMicroOffersService auctionMicroOffersService;
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private BaseAA_UtilService utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/page/auctionMicroOffers")
	public ModelAndView accountLog(ModelMap modelMap,HttpServletRequest request){
		String path = Page.backAuctionMicroOffers;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载微拍页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	@ResponseBody
	@RequestMapping(value="/getAuctionMicroOffersList",method=RequestMethod.POST)
	public JSONObject getAuctionMicroOffersList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionMicroOffers> auctionMicroOffers = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			
			auctionMicroOffers = auctionMicroOffersService.selectListByCondition(map);
			Integer total = auctionMicroOffersService.selectCountByCondition(map);
			json.put("rows", auctionMicroOffers);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取微拍列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionMicroOffers", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionMicroOffers(@ModelAttribute AuctionMicroOffers auctionMicroOffers,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			//auctionMicro.setUpdatedBy(admin.getAuctionMicroname());
			Date d = new Date();
			if(null != auctionMicroOffers.getId()){//更新
				auctionMicroOffers.setUpdatedAt(d); 
				auctionMicroOffers.setUpdatedBy(admin.getUsername());
				auctionMicroOffers.setMainStatus(1);//启用
				auctionMicroOffersService.updateByPrimaryKeySelective(auctionMicroOffers);
			}else{//新增
//				int id=auctionMicroOffers.getGoodsId();
//				BaseGoods goods=goodsService.selectByPrimaryKey(id);
				auctionMicroOffers.setSerial(CommonGenerator.getSerialByDate("aso"));
				auctionMicroOffers.setCreatedAt(d); 
				auctionMicroOffers.setCreatedBy(admin.getUsername());
			
				auctionMicroOffersService.insert(auctionMicroOffers);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionMicroOffersDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionMicroOffersDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_micro_offers);
			map.put("ids", ids);
			//map.put("auctionMicroname",auctionMicro.getAuctionMicroname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionMicroOffersThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionMicroOffersThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_micro_offers);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除微拍出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionMicroOffersRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionMicroOffersRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_micro_offers);
			map.put("ids", ids);
			//map.put("auctionMicroname",auctionMicro.getAuctionMicroname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复微拍出现异常_", json);
		}
		return Result.success(json);
	}
}
