/*package com.lhfeiyu.action.back.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.Admin;
import com.lhfeiyu.po.AuctionQuickInst;
import com.lhfeiyu.po.base.User;
import com.lhfeiyu.service.AuctionQuickInstService;
import com.lhfeiyu.service.base.AA_UtilService;
import com.lhfeiyu.service.base.UserService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;
import com.lhfeiyu.config.Page;
@Controller
@RequestMapping(value="/back")
public class BackAuctionQuickInst {
	@Autowired
	private AuctionQuickInstService auctionQuickInstService;
	@Autowired
	private AA_UtilService utilService;
	@Autowired
	private UserService userService;
	
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/auctionQuickInst", method=RequestMethod.GET)
	public ModelAndView backAuctionQuickInstInfo(ModelMap modelMap,HttpServletRequest request) {
		return new ModelAndView(Page.backAuctionQuickInst);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickInstList",method=RequestMethod.POST)
	public JSONObject getAuctionQuickInstList(HttpServletRequest request,HttpServletResponse response) {
		List<AuctionQuickInst> auctionQuickInstList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			auctionQuickInstList = auctionQuickInstService.selectListByCondition(map);
			Integer total = auctionQuickInstService.selectCountByCondition(map);
			json.put("rows", auctionQuickInstList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取即时拍机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAuctionQuickInst")
	public JSONArray getAuctionQuickInst(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		try {
			List<AuctionQuickInst> auctionQuickInstList = auctionQuickInstService.selectListByCondition(null);
			for(AuctionQuickInst u : auctionQuickInstList){
				JSONObject json = new JSONObject();
				json.put("id",u.getId());
				json.put("name",u.getName());
				array.add(json);
			}
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_获取即时拍机构出现异常_", array);
		}
		return array;
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAuctionQuickInst", method=RequestMethod.POST)
	public JSONObject addOrUpdateAuctionQuickInst(@ModelAttribute AuctionQuickInst auctionQuickInst,HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			//auctionQuickInst.setUpdatedBy(admin.getAuctionQuickInstname());
			Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Date d = new Date();
			if(null != auctionQuickInst.getId()){//更新
				auctionQuickInst.setUpdatedAt(d); 
				auctionQuickInst.setUpdatedBy(admin.getUsername());
				auctionQuickInstService.updateByPrimaryKeySelective(auctionQuickInst);
			}else{//新增
				String picPaths = auctionQuickInst.getPicPaths();//如果没有上传拍卖机构图片,则使用用户的头像
				if(null != picPaths && !"".equals(picPaths)){
					auctionQuickInst.setPicPaths(picPaths);
				}else{
					int userId = auctionQuickInst.getUserId();
					User user = userService.selectByPrimaryKey(userId);
					if(null != user){
						auctionQuickInst.setPicPaths(user.getAvatar());
					}
				}
				auctionQuickInst.setCreatedAt(d); 
				auctionQuickInst.setSerial(CommonGenerator.getSerialByDate("aqi"));
				auctionQuickInst.setCreatedBy(admin.getUsername());
				auctionQuickInstService.insert(auctionQuickInst);
			}
			json.put("status", "success");
			json.put("msg", "操作成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_新增修改即时拍机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickInstDelete",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickInstDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			//Table.admin
			map.put("table", Table.auction_quick_inst);
			map.put("ids", ids);
			//map.put("auctionQuickInstname",auctionQuickInst.getAuctionQuickInstname());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除即时拍机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteAuctionQuickInstThorough",method=RequestMethod.POST)
	public JSONObject deleteAuctionQuickInstThrough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_quick_inst);
			map.put("ids", ids);
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除即时拍机构出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAuctionQuickInstRecover",method=RequestMethod.POST)
	public JSONObject updateAuctionQuickInstRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = (Admin) request.getSession().getAttribute("admin");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("table", Table.auction_quick_inst);
			map.put("ids", ids);
			//map.put("auctionQuickInstname",auctionQuickInst.getAuctionQuickInstname());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复即时拍机构出现异常_", json);
		}
		return Result.success(json);
	}
	
}
*/