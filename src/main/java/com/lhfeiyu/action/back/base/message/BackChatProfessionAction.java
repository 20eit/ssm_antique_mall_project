package com.lhfeiyu.action.back.base.message;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.base.BasePage;
import com.lhfeiyu.po.ChatProfession;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.service.ChatProfessionService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Pagination;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.util.base.RequestUtil;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 基础库-控制层-后台：聊天记录 ChatProfession <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 包路径：com.lhfeiyu.action.back.message.BackChatProfessionAction <p>
 */
@Controller
@RequestMapping(value="/back")
public class BackChatProfessionAction {
	
	@Autowired
	private ChatProfessionService ChatProfessionService;
	
	private static Logger logger = Logger.getLogger("R");
	
	/**
	 * 加载后台聊天记录页面
	 * @param modelMap
	 * @return ModelAndView
	 */
	@RequestMapping(value="/page/backChatProfession")
	public ModelAndView ChatProfession(ModelMap modelMap){
		String path = BasePage.back_chatProfession;
		try{
			
		}catch(Exception e){
			path = BasePage.back_error;
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-PAGE-/back/page/ChatProfession-加载专场聊天记录页面出现异常", modelMap);
		}
		return new ModelAndView(path,modelMap);
	}
	
	/**
	 * 加载聊天记录列表数据
	 * @param request
	 * @return JSONObject(rows,total,status,success)
	 */
	@ResponseBody
	@RequestMapping(value = "/getChatProfessionList",method=RequestMethod.POST)
	public JSONObject getChatProfessionList(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = Pagination.getOrderByAndPage(RequestUtil.getRequestParam(request), request);
			ChatProfessionService.getChatProfessionList(json, map);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/getChatProfessionList-加载聊天记录列表出现异常", json);
		}
		return json;
	}
	
	/**
	 * 新增或修改聊天记录（新增和修改方法对应Serivce中的不同方法）
	 * @param ChatProfession ModelAttribute
	 * @param request
	 * @return JSONObject
	 */
	@ResponseBody
	@RequestMapping(value = "/addOrUpdateChatProfession", method = RequestMethod.POST)
	public JSONObject addUpdateChatProfession(@ModelAttribute ChatProfession ChatProfession,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			String username = admin.getUsername();
			ChatProfessionService.addOrUpdateChatProfession(json, ChatProfession, username);
		}catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/addUpdateChatProfession-添加或修改聊天记录出现异常", json);
		}
		return json;
	}
//	/**
//	 * 新增或修改聊天记录（新增和修改方法对应Serivce中的不同方法）
//	 * @param ChatProfession ModelAttribute
//	 * @param request
//	 * @return JSONObject
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/addOrUpdateChatProfession", method = RequestMethod.POST)
//	public JSONObject addOrUpdateChatProfession(@ModelAttribute ChatProfession ChatProfession,HttpServletRequest request){
//		JSONObject json = new JSONObject();
//		try {
//			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
//			String username = admin.getUsername();
//			ChatProfessionService.addOrUpdateChatProfession(json, ChatProfession, username);
//		}catch (Exception e) {
//			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/addOrUpdateChatProfession-添加或修改聊天记录出现异常", json);
//		}
//		return json;
//	}
	/**
	 * 逻辑删除聊天记录数据
	 * @param request
	 * @param ids ID串
	 * @return JSONObject
	 */
	@ResponseBody
	@RequestMapping(value = "/updateChatProfessionDelete",method=RequestMethod.POST)
	public JSONObject updateChatProfessionDelete(HttpServletRequest request, @RequestParam String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			ChatProfessionService.updateDeletedNowByIds(ids, admin.getUsername());
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/updateChatProfessionDelete-删除聊天记录出现异常", json);
		}
		return json;
	}
	
	/**
	 * 物理删除聊天记录
	 * @param request
	 * @param ids ID串
	 * @return JSONObject
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteChatProfessionThorough",method=RequestMethod.POST)
	public JSONObject deleteChatProfessionThorough(HttpServletRequest request, @RequestParam String ids) {
		JSONObject json = new JSONObject();
		try {
			//Admin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			ChatProfessionService.deleteByIds(ids);
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/deleteChatProfessionThorough-彻底删除聊天记录出现异常", json);
		}
		return json;
	}
	
	/**
	 * 恢复数字字典数据（去除逻辑删除状态）
	 * @param request
	 * @param ids ID串
	 * @return JSONObject
	 */
	@ResponseBody
	@RequestMapping(value = "/updateChatProfessionRecover",method=RequestMethod.POST)
	public JSONObject updateChatProfessionRecover(HttpServletRequest request, @RequestParam String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			ChatProfessionService.updateDeletedNullByIds(ids, admin.getUsername());
			Result.success(json);
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR-ChatProfession-AJAX-/back/updateChatProfessionRecover-恢复聊天记录出现异常", json);
		}
		return json;
	}
	
}
