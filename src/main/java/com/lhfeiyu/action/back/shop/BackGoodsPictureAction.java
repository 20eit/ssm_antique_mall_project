package com.lhfeiyu.action.back.shop;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.po.base.BaseGoodsPicture;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseGoodsPictureService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackGoodsPictureAction {
	@Autowired
	private BaseGoodsPictureService goodsPictureService;
	@Autowired
	private BaseAA_UtilService utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/goodsPictureLib")
	public ModelAndView goodsPictureLib(ModelMap modelMap,HttpServletRequest request){
		String path = Page.backGoodsPictureLib;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载藏品图片页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateGoodsPicture", method=RequestMethod.POST)
	public JSONObject addOrUpdateGoodsPicture(@ModelAttribute BaseGoodsPicture goodsPicture,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = (BaseAdmin) request.getSession().getAttribute("admin");
			if(null == goodsPicture.getId()){//添加
				String serial = CommonGenerator.getSerialByDate("gp");
				goodsPicture.setSerial(serial);
				goodsPicture.setCreatedBy(admin.getUsername());
				goodsPicture.setCreatedAt(new Date());
				if(null != goodsPicture.getPicPath() && !"".equals(goodsPicture.getPicPath())){//图片
					String filePaths[] = goodsPicture.getPicPath().split(",");
					for(int i = 0 ; i < filePaths.length ;i++){
						goodsPicture.setPicPath(filePaths[i]);
						if(i == 0){
							goodsPicture.setIsCover(2);
						}else{
							goodsPicture.setIsCover(0);
						}
						goodsPictureService.insert(goodsPicture);
					}
				}
			}else{//修改
				goodsPicture.setUpdatedBy(admin.getUsername());
				goodsPicture.setUpdatedAt(new Date());
				goodsPictureService.updateByPrimaryKeySelective(goodsPicture);
			}
			json.put("status", "success");
			json.put("id",goodsPicture.getId());
			json.put("msg", "操作成功");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改藏品出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getGoodsPictureList",method=RequestMethod.POST)
	public JSONObject getGoodsPictureList(HttpServletRequest request) {
		List<BaseGoodsPicture> goodsPictureList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			goodsPictureList = goodsPictureService.selectListByCondition(map);
			Integer total = goodsPictureService.selectCountByCondition(map);
			json.put("rows", goodsPictureList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载藏品图片列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateGoodsPictureDelete",method=RequestMethod.POST)
	public JSONObject updateGoodsPictureDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.goods_picture);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除藏品图片出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteGoodsPictureThorough",method=RequestMethod.POST)
	public JSONObject deleteGoodsPictureThorough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.goods_picture);
			map.put("username",admin.getUsername());
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除藏品图片出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateGoodsPictureRecover",method=RequestMethod.POST)
	public JSONObject updateGoodsPictureRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.goods_picture);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复藏品图片出现异常_", json);
		}
		return Result.success(json);
	}
	
}
