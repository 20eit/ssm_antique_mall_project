package com.lhfeiyu.action.back.shop;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.po.base.BaseShop;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.service.base.BaseShopService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackShopAction {
	@Autowired
	private BaseShopService shopService;
	@Autowired
	private BaseAA_UtilService utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/shop")
	public ModelAndView shop(ModelMap modelMap,HttpSession session){
		String path = Page.backShop;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载商店页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@RequestMapping(value="/agentProduct")
	public ModelAndView agentProduct(ModelMap modelMap,HttpSession session){
		String path = Page.backAgentProduct;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载产品库申请页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	
	@RequestMapping(value="/shopAddOrUpdate")
	public ModelAndView shopAddOrUpdate(ModelMap modelMap,HttpSession session,
			@RequestParam(required=false,value="id") Integer id){
		String path = Page.backShopAddOrUpdate;
		try{
			modelMap.put("id", id);
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载添加或修改店铺页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateShop", method=RequestMethod.POST)
	public JSONObject addOrUpdateShop(@ModelAttribute BaseShop shop,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			if(null == shop.getId()){//添加
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("userId", shop.getUserId());
				List<BaseShop> shopList = shopService.selectListByCondition(map);
				if(shopList.size() > 0){
					json.put("status", "failure");
					json.put("msg", "该商铺已经存在。");
					return Result.success(json);
				}
				shop.setCreatedBy(admin.getUsername());
				shop.setCreatedAt(new Date());
				String serial = CommonGenerator.getSerialByDate("s");
				shop.setMainStatus(1);
				shop.setSerial(serial);
				shopService.insert(shop);
			}else{//修改
				shop.setUpdatedBy(admin.getUsername());
				shop.setUpdatedAt(new Date());
				shopService.updateByPrimaryKeySelective(shop);
			}
			json.put("status", "success");
			json.put("id",shop.getId());
			json.put("msg", "操作成功");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改商店出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getShopList",method=RequestMethod.POST)
	public JSONObject getShopList(HttpServletRequest request) {
		List<BaseShop> shopList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			shopList = shopService.selectListByCondition(map);
			Integer total = shopService.selectCountByCondition(map);
			json.put("rows", shopList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载商店列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateShopDelete",method=RequestMethod.POST)
	public JSONObject updateShopDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.shop);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除商店出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteShopThorough",method=RequestMethod.POST)
	public JSONObject deleteShopThorough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.shop);
			map.put("username",admin.getUsername());
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除商店出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateShopRecover",method=RequestMethod.POST)
	public JSONObject updateShopRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.shop);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复商店出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getShop")
	public JSONArray getShop(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		try {
			List<BaseShop> shopList = shopService.selectListByCondition(null);
			for(BaseShop f:shopList){
				JSONObject json = new JSONObject();
				json.put("id",f.getId());
				json.put("name",f.getName());
				array.add(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
}
