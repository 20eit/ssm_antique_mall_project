package com.lhfeiyu.action.back.shop;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.po.base.BaseOrderGoods;
import com.lhfeiyu.service.base.BaseOrderGoodsService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackOrderGoodsAction {
	@Autowired
	private BaseOrderGoodsService  orderGoodsService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/promote")
	public ModelAndView promote(ModelMap modelMap,HttpServletRequest request){
		String path = Page.backPromote;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载推广记录页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/getOrderGoodsList",method=RequestMethod.POST)
	public JSONObject getOrderGoodsList(HttpServletRequest request) {
		List<BaseOrderGoods> orderGoodsList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			orderGoodsList = orderGoodsService.selectListByCondition(map);
			Integer total = orderGoodsService.selectCountByCondition(map);
			json.put("rows", orderGoodsList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载推广记录列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	
}
