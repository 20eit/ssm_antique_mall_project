package com.lhfeiyu.action.back.antiqueCity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.config.Table;
import com.lhfeiyu.po.AntiqueCity;
import com.lhfeiyu.po.base.BaseAdmin;
import com.lhfeiyu.service.AntiqueCityService;
import com.lhfeiyu.service.base.BaseAA_UtilService;
import com.lhfeiyu.tools.base.ActionUtil;
import com.lhfeiyu.tools.base.Result;

@Controller
@RequestMapping(value="/back")
public class BackAntiqueCityAction {
	@Autowired
	private AntiqueCityService antiqueCityService;
	@Autowired
	private BaseAA_UtilService utilService;
	private static Logger logger = Logger.getLogger("R");
	
	@RequestMapping(value="/antiqueCity")
	public ModelAndView antiqueCity(ModelMap modelMap,HttpSession session){
		String path = Page.backAntiqueCity;
		try{
		}catch(Exception e){
			e.printStackTrace();
			path = Page.error;
			logger.error("LH_ERROR_加载古玩城页面出现异常_"+e.getMessage());
		}
		return new ModelAndView(path,modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value="/addOrUpdateAntiqueCity", method=RequestMethod.POST)
	public JSONObject addOrUpdateAntiqueCity(@ModelAttribute AntiqueCity antiqueCity,HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			if(null == admin){
				return Result.adminSessionInvalid(json);//返回session过期的json提示
			}
			if(null == antiqueCity.getId()){//添加
				antiqueCity.setCreatedBy(admin.getUsername());
				antiqueCity.setCreatedAt(new Date());
				antiqueCityService.insert(antiqueCity);
			}else{//修改
				antiqueCity.setUpdatedBy(admin.getUsername());
				antiqueCity.setUpdatedAt(new Date());
				antiqueCityService.updateByPrimaryKeySelective(antiqueCity);
			}
			json.put("status", "success");
			json.put("id",antiqueCity.getId());
			json.put("msg", "操作成功");
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "操作失败");
			Result.catchError(e, logger, "LH_ERROR_添加或修改古玩城出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAntiqueCityList",method=RequestMethod.POST)
	public JSONObject getAntiqueCityList(HttpServletRequest request) {
		List<AntiqueCity> antiqueCityList = null;
		JSONObject json = new JSONObject();
		try {
			Map<String, Object> map = ActionUtil.getAllParam(request);
			antiqueCityList = antiqueCityService.selectListByCondition(map);
			Integer total = antiqueCityService.selectCountByCondition(map);
			json.put("rows", antiqueCityList);
			json.put("total", total);
			json.put("status", "success");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_加载古玩城列表出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAntiqueCityDelete",method=RequestMethod.POST)
	public JSONObject updateAntiqueCityDelete(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.antique_city);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNowByIds(map);
			json.put("status","success");
			json.put("msg","删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_删除古玩城出现异常_", json);
		}
		return Result.success(json);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/deleteAntiqueCityThorough",method=RequestMethod.POST)
	public JSONObject deleteAntiqueCityThorough(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.antique_city);
			map.put("username",admin.getUsername());
			utilService.deleteByIds(map);
			json.put("status","success");
			json.put("msg","彻底删除成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_彻底删除古玩城出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/updateAntiqueCityRecover",method=RequestMethod.POST)
	public JSONObject updateAntiqueCityRecover(HttpServletRequest request,
			@RequestParam(value="ids") String ids) {
		JSONObject json = new JSONObject();
		try {
			BaseAdmin admin = ActionUtil.checkSession4Admin(request.getSession());//验证session中的user，存在即返回
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ids", ids);
			map.put("table", Table.antique_city);
			map.put("username",admin.getUsername());
			utilService.updateDeletedNullByIds(map);
			json.put("status","success");
			json.put("msg","恢复成功");
		} catch (Exception e) {
			Result.catchError(e, logger, "LH_ERROR_恢复古玩城出现异常_", json);
		}
		return Result.success(json);
	}
	
	@ResponseBody
	@RequestMapping(value="/getAntiqueCity")
	public JSONArray getAntiqueCity(HttpServletRequest request) {
		JSONArray array = new JSONArray();
		try {
			List<AntiqueCity> antiqueCityList = antiqueCityService.selectListByCondition(null);
			for(AntiqueCity f:antiqueCityList){
				JSONObject json = new JSONObject();
				json.put("id",f.getId());
				json.put("name",f.getName());
				array.add(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}
	
}
