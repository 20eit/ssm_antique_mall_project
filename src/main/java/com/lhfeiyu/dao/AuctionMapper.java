package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.Auction;

public interface AuctionMapper extends CommonMapper<Auction>{

}
