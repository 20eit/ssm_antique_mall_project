package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionQuick;

public interface AuctionQuickMapper extends CommonMapper<AuctionQuick>{

	List<AuctionQuick> selectBeginApproach();
	AuctionQuick selectPrepareBegin(Map<String,Object> map);
    String selectAlreadyBegin();
    String selectTypeIdsAlreadyBegin();
    int updateHaveNoticed(Map<String,Object> map);
    int updateHaveBegun(Map<String,Object> map);
    int updateAuctionOver(Map<String,Object> map);
    String selectGroupToDelete();
    int updateChatGroupStatus();
    
}
