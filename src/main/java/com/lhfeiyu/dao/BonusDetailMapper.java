package com.lhfeiyu.dao;

import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.BonusDetail;

public interface BonusDetailMapper extends CommonMapper<BonusDetail>{

	String selectSumByCondition(Map<String,Object> map);
}
