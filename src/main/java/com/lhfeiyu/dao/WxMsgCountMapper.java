package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.WxMsgCount;

public interface WxMsgCountMapper extends CommonMapper<WxMsgCount>{
	
}
