package com.lhfeiyu.dao;

import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionQuickNotice;

public interface AuctionQuickNoticeMapper extends CommonMapper<AuctionQuickNotice>{

	String selectUsersPhonesByAuctionQuickNotice(Map<String,Object> map);
	
	int selectUsersCountByAuctionQuickNotice(Map<String,Object> map);
}
