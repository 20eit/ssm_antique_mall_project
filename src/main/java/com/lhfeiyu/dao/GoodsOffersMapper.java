package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.GoodsOffers;

public interface GoodsOffersMapper extends CommonMapper<GoodsOffers>{

}
