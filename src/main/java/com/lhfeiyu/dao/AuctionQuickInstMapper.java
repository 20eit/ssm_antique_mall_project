package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionQuickInst;

public interface AuctionQuickInstMapper extends CommonMapper<AuctionQuickInst>{

}
