package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AutoOffer;

public interface AutoOfferMapper extends CommonMapper<AutoOffer>{

	List<AutoOffer> selectByAuctionGoodsId(Map<String,Object> map);
}
