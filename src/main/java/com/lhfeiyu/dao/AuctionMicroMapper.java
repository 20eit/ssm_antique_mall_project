package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionMicro;

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层Mapper：AuctionMicro <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-微拍表 <p>
 */

public interface AuctionMicroMapper extends CommonMapper<AuctionMicro> {

	int deleteByAuctionIds(@Param(value="auctionIds") String auctionIds);
	
	int deleteByGoodsIds(@Param(value="goodsIds") String goodsIds);

	int updateOrderSn(@Param(value="orderSn") String orderSn, @Param(value="id") Integer id);
	
	String selectAuctionIdsDoneButNotDeal();

	List<AuctionMicro> selectListByOfferUserId(Map<String, Object> map);
	
	//String selectGoodsIdsOfShop(Map<String, Object> map);
	//String selectAuctionIdsOfShop(Map<String, Object> map);
}