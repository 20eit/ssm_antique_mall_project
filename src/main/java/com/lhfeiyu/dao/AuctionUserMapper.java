package com.lhfeiyu.dao;

import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionUser;

public interface AuctionUserMapper extends CommonMapper<AuctionUser>{

	 String selectAuctionInviteUserIds(Map<String,Object> map);
}
