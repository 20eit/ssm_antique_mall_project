package com.lhfeiyu.dao;

import java.util.List;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionMicroOffers;

public interface AuctionMicroOffersMapper extends CommonMapper<AuctionMicroOffers>{

	List<AuctionMicroOffers> selectListByNotCf(Integer id);
    
  
    
}
