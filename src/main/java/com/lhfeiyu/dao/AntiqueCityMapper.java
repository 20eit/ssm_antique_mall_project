package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AntiqueCity;

public interface AntiqueCityMapper extends CommonMapper<AntiqueCity>{

}
