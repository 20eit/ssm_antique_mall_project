package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.Credit;

public interface CreditMapper extends CommonMapper<Credit>{

}
