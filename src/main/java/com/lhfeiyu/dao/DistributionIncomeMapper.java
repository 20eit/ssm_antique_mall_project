package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.DistributionIncome;
import com.lhfeiyu.po.base.BaseUser;

public interface DistributionIncomeMapper extends CommonMapper<DistributionIncome>{

	List<BaseUser> selectIncomeListByCondition(Map<String,Object> map);
}
