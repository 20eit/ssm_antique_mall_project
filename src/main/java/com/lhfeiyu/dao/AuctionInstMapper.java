package com.lhfeiyu.dao;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionInst;

public interface AuctionInstMapper extends CommonMapper<AuctionInst>{

}
