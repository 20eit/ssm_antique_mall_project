package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionGoods;

public interface AuctionGoodsMapper extends CommonMapper<AuctionGoods>{
	
	int updateFirstRefresh(Map<String,Object> map);
	
	int updateStatusByRefresh(Map<String,Object> map);
	
	List<AuctionGoods> selectProfessionGoodsAutoOffer();
	
	int updateAutoOfferNULL(Map<String,Object> map);
	
	String selectGoodsIdsOfProfession(Map<String,Object> map);
	
	String selectGoodsIdsDoneButNotDeal(Map<String,Object> map);
	
	List<AuctionGoods> selectOrderSnNull();
	
	int updateOrderSn(Map<String,Object> map);
	
	int deleteByGoodsIds(Map<String,Object> map);
	
	
}
