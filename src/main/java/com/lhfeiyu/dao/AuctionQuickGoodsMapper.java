package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionQuickGoods;

public interface AuctionQuickGoodsMapper extends CommonMapper<AuctionQuickGoods>{
    
	int updateFirstRefresh(Map<String,Object> map);
	
	int updateStatusByRefresh(Map<String,Object> map);
	
	List<AuctionQuickGoods> selectQuickGoodsAutoOffer();
	
	int updateAutoOfferNULL(Map<String,Object> map);
	
	String selectGoodsIdsOfQuick(Map<String,Object> map);
	
	String selectGoodsIdsDoneButNotDeal(Map<String,Object> map);
	
	List<AuctionQuickGoods> selectOrderSnNull();
    
	int updateOrderSn(Map<String,Object> map);
	
	int deleteByGoodsIds(Map<String,Object> map);
	
}
