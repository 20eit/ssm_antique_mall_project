package com.lhfeiyu.dao;

import java.util.List;
import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.AuctionProfession;

public interface AuctionProfessionMapper extends CommonMapper<AuctionProfession>{
    
    List<AuctionProfession> selectBeginApproach();
    String selectPrepareBegin();
    String selectAlreadyBegin();
    int updateHaveNoticed(Map<String,Object> map);
    int updateHaveBegun(Map<String,Object> map);
    int updateAuctionOver(Map<String,Object> map);
    String selectGroupToDelete();
    int updateChatGroupStatus();
    
    int updateJoinNumAdd(Map<String,Object> map);
    int updateVisitNumAdd(Map<String,Object> map);
	List<AuctionProfession> selectBySchedulers(int userId);
}
