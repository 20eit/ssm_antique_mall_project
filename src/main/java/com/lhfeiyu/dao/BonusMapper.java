package com.lhfeiyu.dao;

import java.util.Map;

import com.lhfeiyu.dao.base.CommonMapper;
import com.lhfeiyu.po.Bonus;

public interface BonusMapper extends CommonMapper<Bonus>{

	String selectSumByCondition(Map<String,Object> map);
}
