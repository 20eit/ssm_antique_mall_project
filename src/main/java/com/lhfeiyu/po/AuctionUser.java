package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionUser <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-用户表 <p>
 */
public class AuctionUser extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	

	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 拍卖类型（1：专场，2：微拍，3：即时拍）  */
	private Integer auctionType;
	
	/** 拍卖ID  */
	private Integer auctionId;
	
	/** 拍场管理用户ID  */
	private Integer auctionManagerId;
	
	/** 拍场管理用户名称  */
	private String auctionManagerName;
	
	/** 用户ID  */
	private Integer userId;
	
	/** 用户名  */
	private String username;
	
	/** 是否缴纳保证金  */
	private Integer isAssure;
	
	/** 保证金-金额  */
	private BigDecimal assureMoney;
	
	/** 推广收益金额  */
	private BigDecimal promoteMoney;
	
	/** 邀请用户ID（点击该用户的推广链接地址进入拍场,计算推广分红）  */
	private Integer inviteUserId;
	
	/** 邀请人用户名  */
	private String inviteUsername;
	
	
	public Integer getAuctionType() {
		return auctionType;
	}
	public void setAuctionType(Integer auctionType) {
		this.auctionType = auctionType;
	}
	public Integer getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(Integer auctionId) {
		this.auctionId = auctionId;
	}
	public Integer getAuctionManagerId() {
		return auctionManagerId;
	}
	public void setAuctionManagerId(Integer auctionManagerId) {
		this.auctionManagerId = auctionManagerId;
	}
	public String getAuctionManagerName() {
		return auctionManagerName;
	}
	public void setAuctionManagerName(String auctionManagerName) {
		this.auctionManagerName = auctionManagerName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getIsAssure() {
		return isAssure;
	}
	public void setIsAssure(Integer isAssure) {
		this.isAssure = isAssure;
	}
	public BigDecimal getAssureMoney() {
		return assureMoney;
	}
	public void setAssureMoney(BigDecimal assureMoney) {
		this.assureMoney = assureMoney;
	}
	public BigDecimal getPromoteMoney() {
		return promoteMoney;
	}
	public void setPromoteMoney(BigDecimal promoteMoney) {
		this.promoteMoney = promoteMoney;
	}
	public Integer getInviteUserId() {
		return inviteUserId;
	}
	public void setInviteUserId(Integer inviteUserId) {
		this.inviteUserId = inviteUserId;
	}
	public String getInviteUsername() {
		return inviteUsername;
	}
	public void setInviteUsername(String inviteUsername) {
		this.inviteUsername = inviteUsername;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	
	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}