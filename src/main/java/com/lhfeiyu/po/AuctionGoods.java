package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionGoods <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-藏品表 <p>
 */
public class AuctionGoods extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String instSerial;
	private String instName;
	private String userSerial;
	private String username;
	private Integer remainNum;
	private Integer dealNum;
	private Integer totalNum;
	private String goodsName;
	private String picPath;
	private String picPaths;
	private Integer praiseNum;
	private String goodsSn;
	private String goodsDescription;
	private BigDecimal packFee;

	private Integer shopId;
	private String goodsIds;
	private String chatGroupId;
	private String subAccount;
	private String password;
	
	private String addressDetail;
	private String phone;
	private String receiverName;
	private String province;
	private String city;
	private String email;
	private String auctionInstName;
	private String auctionInstSerial;
	private String userOpenId;
	private String typeName;
	private String auctionName;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 用户ID  */
	private Integer userId;
	
	/** 拍卖类型（只有专场）  */
	private Integer auctionType;
	
	/** 拍卖ID  */
	private Integer auctionId;
	
	/** 拍品ID  */
	private Integer goodsId;
	
	/** 拍品编号  */
	private String goodsSerial;
	
	/** 起拍价  */
	private BigDecimal priceBegin;
	
	/** 最后出价用户  */
	private String offerUsername;
	
	/** 最后出价会员ID  */
	private Integer offerUserId;
	
	/** 最后出价  */
	private BigDecimal offerPrice;
	
	/** 最后出价时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date offerAt;
	
	/** 出价次数  */
	private Integer offerTimes;
	
	/** 刷新时间（用于倒计时）  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date refreshTime;
	
	/** 成交价  */
	private BigDecimal priceDeal;
	
	/** 成交时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dealTime;
	
	/** 运费（空值为包邮）  */
	private BigDecimal postageFee;
	
	/** 自动竞价的最高价  */
	private BigDecimal autoTopPrice;
	
	/** 自动竞价用户ID  */
	private Integer autoUserId;
	
	/** 自动竞价用户名  */
	private String autoUsername;
	
	/** 自动竞价表ID  */
	private Integer autoOfferId;
	
	/** 订单编号  */
	private String orderSn;
	
	/** 围观次数  */
	private Integer scansCount;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getAuctionType() {
		return auctionType;
	}
	public void setAuctionType(Integer auctionType) {
		this.auctionType = auctionType;
	}
	public Integer getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(Integer auctionId) {
		this.auctionId = auctionId;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public String getGoodsSerial() {
		return goodsSerial;
	}
	public void setGoodsSerial(String goodsSerial) {
		this.goodsSerial = goodsSerial;
	}
	public BigDecimal getPriceBegin() {
		return priceBegin;
	}
	public void setPriceBegin(BigDecimal priceBegin) {
		this.priceBegin = priceBegin;
	}
	public String getOfferUsername() {
		return offerUsername;
	}
	public void setOfferUsername(String offerUsername) {
		this.offerUsername = offerUsername;
	}
	public Integer getOfferUserId() {
		return offerUserId;
	}
	public void setOfferUserId(Integer offerUserId) {
		this.offerUserId = offerUserId;
	}
	public BigDecimal getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}
	public Date getOfferAt() {
		return offerAt;
	}
	public void setOfferAt(Date offerAt) {
		this.offerAt = offerAt;
	}
	public Integer getOfferTimes() {
		return offerTimes;
	}
	public void setOfferTimes(Integer offerTimes) {
		this.offerTimes = offerTimes;
	}
	public Date getRefreshTime() {
		return refreshTime;
	}
	public void setRefreshTime(Date refreshTime) {
		this.refreshTime = refreshTime;
	}
	public BigDecimal getPriceDeal() {
		return priceDeal;
	}
	public void setPriceDeal(BigDecimal priceDeal) {
		this.priceDeal = priceDeal;
	}
	public Date getDealTime() {
		return dealTime;
	}
	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}
	public BigDecimal getPostageFee() {
		return postageFee;
	}
	public void setPostageFee(BigDecimal postageFee) {
		this.postageFee = postageFee;
	}
	public BigDecimal getAutoTopPrice() {
		return autoTopPrice;
	}
	public void setAutoTopPrice(BigDecimal autoTopPrice) {
		this.autoTopPrice = autoTopPrice;
	}
	public Integer getAutoUserId() {
		return autoUserId;
	}
	public void setAutoUserId(Integer autoUserId) {
		this.autoUserId = autoUserId;
	}
	public String getAutoUsername() {
		return autoUsername;
	}
	public void setAutoUsername(String autoUsername) {
		this.autoUsername = autoUsername;
	}
	public Integer getAutoOfferId() {
		return autoOfferId;
	}
	public void setAutoOfferId(Integer autoOfferId) {
		this.autoOfferId = autoOfferId;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	public Integer getScansCount() {
		return scansCount;
	}
	public void setScansCount(Integer scansCount) {
		this.scansCount = scansCount;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/

	public String getAuctionName() {
		return auctionName;
	}
	public void setAuctionName(String auctionName) {
		this.auctionName = auctionName;
	}
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getUserOpenId() {
		return userOpenId;
	}

	public void setUserOpenId(String userOpenId) {
		this.userOpenId = userOpenId;
	}

	public String getAuctionInstSerial() {
		return auctionInstSerial;
	}

	public void setAuctionInstSerial(String auctionInstSerial) {
		this.auctionInstSerial = auctionInstSerial;
	}

	public String getAuctionInstName() {
		return auctionInstName;
	}

	public void setAuctionInstName(String auctionInstName) {
		this.auctionInstName = auctionInstName;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubAccount() {
		return subAccount;
	}

	public void setSubAccount(String subAccount) {
		this.subAccount = subAccount;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getChatGroupId() {
		return chatGroupId;
	}

	public void setChatGroupId(String chatGroupId) {
		this.chatGroupId = chatGroupId;
	}

	public String getGoodsIds() {
		return goodsIds;
	}

	public void setGoodsIds(String goodsIds) {
		this.goodsIds = goodsIds;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public String getGoodsDescription() {
		return goodsDescription;
	}

	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}

	public BigDecimal getPackFee() {
		return packFee;
	}

	public void setPackFee(BigDecimal packFee) {
		this.packFee = packFee;
	}

	public String getGoodsSn() {
		return goodsSn;
	}

	public void setGoodsSn(String goodsSn) {
		this.goodsSn = goodsSn;
	}

	public String getInstSerial() {
		return instSerial;
	}

	public void setInstSerial(String instSerial) {
		this.instSerial = instSerial;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getUserSerial() {
		return userSerial;
	}

	public void setUserSerial(String userSerial) {
		this.userSerial = userSerial;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getRemainNum() {
		return remainNum;
	}

	public void setRemainNum(Integer remainNum) {
		this.remainNum = remainNum;
	}

	public Integer getDealNum() {
		return dealNum;
	}

	public void setDealNum(Integer dealNum) {
		this.dealNum = dealNum;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Integer getPraiseNum() {
		return praiseNum;
	}

	public void setPraiseNum(Integer praiseNum) {
		this.praiseNum = praiseNum;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}

	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}