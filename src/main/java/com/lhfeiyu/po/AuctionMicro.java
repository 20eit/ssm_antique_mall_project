package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.lhfeiyu.po.base.BaseUserPraise;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionMicro <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-微拍表 <p>
 */
public class AuctionMicro extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	private String userSerial;
	private String username;
	private String realName;
	private String userAvatar;
	private String currentPrice;
	private Integer overFoucs;
	private Integer fansId;
	private String shopName;
	private String cityName;
	private String provinceName;
	private Integer creditMargin;
	private List<BaseUserPraise> userPraiseList;
	private Integer userPraiseCount;
	
	private String goodsName;
	private String goodsSn;
	private String goodsDescription;
	private BigDecimal postageFee;
	private BigDecimal packFee;
	private long remainSeconds;
	private List<AuctionMicroOffers> amoList;
	private String userOpenId;
	private String picPath;
	private String goodsPicPaths;
	private Integer isSevenReturn;
	private Integer userPraiseId;
	private Integer shopPrice;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 用户ID  */
	private Integer userId;
	
	/** 店铺ID  */
	private Integer shopId;
	
	/** 微拍ID  */
	private String auctionSerial;
	
	/** 拍场图片路径  */
	private String picPaths;
	
	/** 拍卖场次（编号）  */
	private Integer goodsId;
	
	/** 拍品编号  */
	private String goodsSerial;
	
	/** 保证金  */
	private BigDecimal bail;
	
	/** 开始时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	/** 结束时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	/** 加价幅度  */
	private BigDecimal increaseRangePrice;
	
	/** 起拍价  */
	private BigDecimal priceBegin;
	
	/** 一口价  */
	private BigDecimal buyoutPrice;
	
	/** 参拍人数  */
	private Integer joinNum;
	
	/** 围观次数（访问次数）  */
	private Integer visitNum;
	
	/** 单品红包  */
	private BigDecimal singlePackets;
	
	/** 最后出价用户  */
	private String offerUsername;
	
	/** 最后出价会员ID  */
	private Integer offerUserId;
	
	/** 最后出价  */
	private BigDecimal offerPrice;
	
	/** 最后出价时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date offerAt;
	
	/** 出价次数  */
	private Integer offerTimes;
	
	/** 成交价  */
	private BigDecimal priceDeal;
	
	/** 推广人ID  */
	private Integer promoteUserId;
	
	/** 推广人编号  */
	private String promoteUserSerial;
	
	/** 推广人名称  */
	private String promoteUsername;
	
	/** 订单编号  */
	private String orderSn;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getAuctionSerial() {
		return auctionSerial;
	}
	public void setAuctionSerial(String auctionSerial) {
		this.auctionSerial = auctionSerial;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public String getGoodsSerial() {
		return goodsSerial;
	}
	public void setGoodsSerial(String goodsSerial) {
		this.goodsSerial = goodsSerial;
	}
	public BigDecimal getBail() {
		return bail;
	}
	public void setBail(BigDecimal bail) {
		this.bail = bail;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public BigDecimal getIncreaseRangePrice() {
		return increaseRangePrice;
	}
	public void setIncreaseRangePrice(BigDecimal increaseRangePrice) {
		this.increaseRangePrice = increaseRangePrice;
	}
	public BigDecimal getPriceBegin() {
		return priceBegin;
	}
	public void setPriceBegin(BigDecimal priceBegin) {
		this.priceBegin = priceBegin;
	}
	public BigDecimal getBuyoutPrice() {
		return buyoutPrice;
	}
	public void setBuyoutPrice(BigDecimal buyoutPrice) {
		this.buyoutPrice = buyoutPrice;
	}
	public Integer getJoinNum() {
		return joinNum;
	}
	public void setJoinNum(Integer joinNum) {
		this.joinNum = joinNum;
	}
	public Integer getVisitNum() {
		return visitNum;
	}
	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}
	public BigDecimal getSinglePackets() {
		return singlePackets;
	}
	public void setSinglePackets(BigDecimal singlePackets) {
		this.singlePackets = singlePackets;
	}
	public String getOfferUsername() {
		return offerUsername;
	}
	public void setOfferUsername(String offerUsername) {
		this.offerUsername = offerUsername;
	}
	public Integer getOfferUserId() {
		return offerUserId;
	}
	public void setOfferUserId(Integer offerUserId) {
		this.offerUserId = offerUserId;
	}
	public BigDecimal getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}
	public Date getOfferAt() {
		return offerAt;
	}
	public void setOfferAt(Date offerAt) {
		this.offerAt = offerAt;
	}
	public Integer getOfferTimes() {
		return offerTimes;
	}
	public void setOfferTimes(Integer offerTimes) {
		this.offerTimes = offerTimes;
	}
	public BigDecimal getPriceDeal() {
		return priceDeal;
	}
	public void setPriceDeal(BigDecimal priceDeal) {
		this.priceDeal = priceDeal;
	}
	public Integer getPromoteUserId() {
		return promoteUserId;
	}
	public void setPromoteUserId(Integer promoteUserId) {
		this.promoteUserId = promoteUserId;
	}
	public String getPromoteUserSerial() {
		return promoteUserSerial;
	}
	public void setPromoteUserSerial(String promoteUserSerial) {
		this.promoteUserSerial = promoteUserSerial;
	}
	public String getPromoteUsername() {
		return promoteUsername;
	}
	public void setPromoteUsername(String promoteUsername) {
		this.promoteUsername = promoteUsername;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	public String getUserSerial() {
		return userSerial;
	}
	public void setUserSerial(String userSerial) {
		this.userSerial = userSerial;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getUserAvatar() {
		return userAvatar;
	}
	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}
	public String getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}
	public Integer getOverFoucs() {
		return overFoucs;
	}
	public void setOverFoucs(Integer overFoucs) {
		this.overFoucs = overFoucs;
	}
	public Integer getFansId() {
		return fansId;
	}
	public void setFansId(Integer fansId) {
		this.fansId = fansId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public Integer getCreditMargin() {
		return creditMargin;
	}
	public void setCreditMargin(Integer creditMargin) {
		this.creditMargin = creditMargin;
	}
	public List<BaseUserPraise> getUserPraiseList() {
		return userPraiseList;
	}
	public void setUserPraiseList(List<BaseUserPraise> userPraiseList) {
		this.userPraiseList = userPraiseList;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsSn() {
		return goodsSn;
	}
	public void setGoodsSn(String goodsSn) {
		this.goodsSn = goodsSn;
	}
	public String getGoodsDescription() {
		return goodsDescription;
	}
	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}
	public BigDecimal getPostageFee() {
		return postageFee;
	}
	public void setPostageFee(BigDecimal postageFee) {
		this.postageFee = postageFee;
	}
	public BigDecimal getPackFee() {
		return packFee;
	}
	public void setPackFee(BigDecimal packFee) {
		this.packFee = packFee;
	}
	public long getRemainSeconds() {
		return remainSeconds;
	}
	public void setRemainSeconds(long remainSeconds) {
		this.remainSeconds = remainSeconds;
	}
	public List<AuctionMicroOffers> getAmoList() {
		return amoList;
	}
	public void setAmoList(List<AuctionMicroOffers> amoList) {
		this.amoList = amoList;
	}
	public String getUserOpenId() {
		return userOpenId;
	}
	public void setUserOpenId(String userOpenId) {
		this.userOpenId = userOpenId;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public Integer getIsSevenReturn() {
		return isSevenReturn;
	}
	public void setIsSevenReturn(Integer isSevenReturn) {
		this.isSevenReturn = isSevenReturn;
	}
	public Integer getUserPraiseId() {
		return userPraiseId;
	}
	public void setUserPraiseId(Integer userPraiseId) {
		this.userPraiseId = userPraiseId;
	}
	public Integer getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(Integer shopPrice) {
		this.shopPrice = shopPrice;
	}
	public String getGoodsPicPaths() {
		return goodsPicPaths;
	}
	public void setGoodsPicPaths(String goodsPicPaths) {
		this.goodsPicPaths = goodsPicPaths;
	}
	public Integer getUserPraiseCount() {
		return userPraiseCount;
	}
	public void setUserPraiseCount(Integer userPraiseCount) {
		this.userPraiseCount = userPraiseCount;
	}
	
	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}