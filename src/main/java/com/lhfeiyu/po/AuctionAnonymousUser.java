package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/

/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionAnonymousUser <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-专场匿名用户表 <p>
 */
public class AuctionAnonymousUser extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 拍卖类型（1：专场，2：微拍，3：即时拍）  */
	private Integer auctionType;
	
	/** 拍卖ID  */
	private Integer auctionId;
	
	/** 用户ID  */
	private Integer userId;
	
	/** 用户名  */
	private String username;
	
	/** 号牌  */
	private String signNumber;
	
	/** 个人信息（1不公开，2公开）  */
	private Integer infoAuth;
	
	/** 显示设置（1显示号牌，2显示昵称）  */
	private Integer whichShow;
	
	
	public Integer getAuctionType() {
		return auctionType;
	}
	public void setAuctionType(Integer auctionType) {
		this.auctionType = auctionType;
	}
	public Integer getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(Integer auctionId) {
		this.auctionId = auctionId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSignNumber() {
		return signNumber;
	}
	public void setSignNumber(String signNumber) {
		this.signNumber = signNumber;
	}
	public Integer getInfoAuth() {
		return infoAuth;
	}
	public void setInfoAuth(Integer infoAuth) {
		this.infoAuth = infoAuth;
	}
	public Integer getWhichShow() {
		return whichShow;
	}
	public void setWhichShow(Integer whichShow) {
		this.whichShow = whichShow;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	
	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}