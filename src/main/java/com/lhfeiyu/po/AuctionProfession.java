package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionProfession <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖表 - 专场 <p>
 */
public class AuctionProfession extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
// @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String instSerial;
	private String instName;
	private String username;
	private String instTel;
	private Integer grade;
	private String professionIds;
	private String chatGroupIds;
	private String instLogo;
	private String picPath;
	private String userAvatar;
	private List<String> apPicPaths;
	private String typeName;
	private String goodsTypeName;
	private String picServerId;
	private Integer agCount;
	private String userSerial;
	private BigDecimal userBail;
	private String ossPicPath;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 机构ID  */
	private Integer instId;
	
	/** 用户ID  */
	private Integer userId;
	
	/** 店铺ID  */
	private Integer shopId;
	
	/** 拍卖场次（编号）  */
	private String auctionSerial;
	
	/** 拍卖名称  */
	private String auctionName;
	
	/** 数据字典代码：拍品类型  */
	private String goodsTypeCode;
	
	/** 参拍人数  */
	private Integer joinNum;
	
	/** 围观次数（访问次数）  */
	private Integer visitNum;
	
	/** 保证金  */
	private BigDecimal bail;
	
	/** 推广红包  */
	private BigDecimal spreadPackets;
	
	/** 单品红包  */
	private String singlePackets;
	
	/** 拍场图片路径  */
	private String picPaths;
	
	/** 拍卖开始时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	/** 拍卖结束时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	/** 是否已经进行通知（1：否，2：是）  */
	private Integer haveNoticed;
	
	/** 是否已经开始（1否，2是，3结束，4未提交，5提交审核中）  */
	private Integer haveBegun;
	
	/** 数据字典代码：拍卖状态  */
	private String statusCode;
	
	/** 聊天组ID  */
	private String chatGroupId;
	
	/** 群组状态（1：未创建，2：已创建，3：已删除）  */
	private Integer groupStatus;
	
	/** 匿名拍卖，拿号牌（1非匿名拍场，2匿名拍场）  */
	private Integer anonymous;
	
	
	public Integer getInstId() {
		return instId;
	}
	public void setInstId(Integer instId) {
		this.instId = instId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getAuctionSerial() {
		return auctionSerial;
	}
	public void setAuctionSerial(String auctionSerial) {
		this.auctionSerial = auctionSerial;
	}
	public String getAuctionName() {
		return auctionName;
	}
	public void setAuctionName(String auctionName) {
		this.auctionName = auctionName;
	}
	public String getGoodsTypeCode() {
		return goodsTypeCode;
	}
	public void setGoodsTypeCode(String goodsTypeCode) {
		this.goodsTypeCode = goodsTypeCode;
	}
	public Integer getJoinNum() {
		return joinNum;
	}
	public void setJoinNum(Integer joinNum) {
		this.joinNum = joinNum;
	}
	public Integer getVisitNum() {
		return visitNum;
	}
	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}
	public BigDecimal getBail() {
		return bail;
	}
	public void setBail(BigDecimal bail) {
		this.bail = bail;
	}
	public BigDecimal getSpreadPackets() {
		return spreadPackets;
	}
	public void setSpreadPackets(BigDecimal spreadPackets) {
		this.spreadPackets = spreadPackets;
	}
	public String getSinglePackets() {
		return singlePackets;
	}
	public void setSinglePackets(String singlePackets) {
		this.singlePackets = singlePackets;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getHaveNoticed() {
		return haveNoticed;
	}
	public void setHaveNoticed(Integer haveNoticed) {
		this.haveNoticed = haveNoticed;
	}
	public Integer getHaveBegun() {
		return haveBegun;
	}
	public void setHaveBegun(Integer haveBegun) {
		this.haveBegun = haveBegun;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getChatGroupId() {
		return chatGroupId;
	}
	public void setChatGroupId(String chatGroupId) {
		this.chatGroupId = chatGroupId;
	}
	public Integer getGroupStatus() {
		return groupStatus;
	}
	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}
	public Integer getAnonymous() {
		return anonymous;
	}
	public void setAnonymous(Integer anonymous) {
		this.anonymous = anonymous;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	public List<String> getApPicPaths() {
		return apPicPaths;
	}

	public void setApPicPaths(List<String> apPicPaths) {
		this.apPicPaths = apPicPaths;
	}

	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getGoodsTypeName() {
		return goodsTypeName;
	}
	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}
	public String getPicServerId() {
		return picServerId;
	}
	public void setPicServerId(String picServerId) {
		this.picServerId = picServerId;
	}
	public Integer getAgCount() {
		return agCount;
	}
	public void setAgCount(Integer agCount) {
		this.agCount = agCount;
	}
	public String getUserSerial() {
		return userSerial;
	}
	public void setUserSerial(String userSerial) {
		this.userSerial = userSerial;
	}
	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getInstLogo() {
		return instLogo;
	}

	public void setInstLogo(String instLogo) {
		this.instLogo = instLogo;
	}

	public String getChatGroupIds() {
		return chatGroupIds;
	}

	public void setChatGroupIds(String chatGroupIds) {
		this.chatGroupIds = chatGroupIds;
	}

	public String getProfessionIds() {
		return professionIds;
	}

	public void setProfessionIds(String professionIds) {
		this.professionIds = professionIds;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getInstTel() {
		return instTel;
	}

	public void setInstTel(String instTel) {
		this.instTel = instTel;
	}

	public String getInstSerial() {
		return instSerial;
	}

	public void setInstSerial(String instSerial) {
		this.instSerial = instSerial;
	}

	public String getInstName() {
		return instName;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public BigDecimal getUserBail() {
		return userBail;
	}
	public void setUserBail(BigDecimal userBail) {
		this.userBail = userBail;
	}
	public String getOssPicPath() {
		return ossPicPath;
	}
	public void setOssPicPath(String ossPicPath) {
		this.ossPicPath = ossPicPath;
	}

	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}