package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionMicroOffers <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-微拍出价记录表 <p>
 */
public class AuctionMicroOffers extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
	private String userSerial;
	private String username;
	private String realName;
	private String userAvatar;
	private String priceBegin;
	private String currentPrice;
	private String goodsName;
	private String auctionSerial;
	private String openId;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 微拍ID  */
	private Integer auctionId;
	
	/** 拍品ID  */
	private Integer goodsId;
	
	/** 出价用户  */
	private String offerUsername;
	
	/** 出价会员ID  */
	private Integer offerUserId;
	
	/** 出价金额  */
	private BigDecimal offerPrice;
	
	/** 出价时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date offerAt;
	
	/** 推广人ID  */
	private Integer promoteUserId;
	
	/** 推广人编号  */
	private String promoteUserSerial;
	
	/** 推广人名称  */
	private String promoteUsername;
	
	
	public Integer getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(Integer auctionId) {
		this.auctionId = auctionId;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public String getOfferUsername() {
		return offerUsername;
	}
	public void setOfferUsername(String offerUsername) {
		this.offerUsername = offerUsername;
	}
	public Integer getOfferUserId() {
		return offerUserId;
	}
	public void setOfferUserId(Integer offerUserId) {
		this.offerUserId = offerUserId;
	}
	public BigDecimal getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}
	public Date getOfferAt() {
		return offerAt;
	}
	public void setOfferAt(Date offerAt) {
		this.offerAt = offerAt;
	}
	public Integer getPromoteUserId() {
		return promoteUserId;
	}
	public void setPromoteUserId(Integer promoteUserId) {
		this.promoteUserId = promoteUserId;
	}
	public String getPromoteUserSerial() {
		return promoteUserSerial;
	}
	public void setPromoteUserSerial(String promoteUserSerial) {
		this.promoteUserSerial = promoteUserSerial;
	}
	public String getPromoteUsername() {
		return promoteUsername;
	}
	public void setPromoteUsername(String promoteUsername) {
		this.promoteUsername = promoteUsername;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	public String getUserSerial() {
		return userSerial;
	}

	public void setUserSerial(String userSerial) {
		this.userSerial = userSerial;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	public String getPriceBegin() {
		return priceBegin;
	}

	public void setPriceBegin(String priceBegin) {
		this.priceBegin = priceBegin;
	}

	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getAuctionSerial() {
		return auctionSerial;
	}
	public void setAuctionSerial(String auctionSerial) {
		this.auctionSerial = auctionSerial;
	}

	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}