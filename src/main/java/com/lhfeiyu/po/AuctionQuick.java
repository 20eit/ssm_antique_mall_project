package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionQuick <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖表 - 即时拍 <p>
 */
public class AuctionQuick extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
private String instSerial;
	private String instName;
	private String username;
	private String instTel;
	private Integer grade;
	private String professionIds;
	private String chatGroupIds;

	private Integer isFocus;
	private Integer isNotice;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 即时拍机构ID  */
	private Integer instId;
	
	/** 用户ID  */
	private Integer userId;
	
	/** 店铺ID  */
	private Integer shopId;
	
	/** 拍卖场次（编号）  */
	private String auctionSerial;
	
	/** 拍卖名称  */
	private String auctionName;
	
	/** 参拍人数  */
	private Integer joinNum;
	
	/** 围观次数（访问次数）  */
	private Integer visitNum;
	
	/** 保证金  */
	private BigDecimal bail;
	
	/** 推广红包  */
	private BigDecimal spreadPackets;
	
	/** 单品红包  */
	private String singlePackets;
	
	/** 拍场图片路径  */
	private String picPaths;
	
	/** 拍卖开始时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	
	/** 拍卖结束时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	/** 是否已经进行通知（1：否，2：是）  */
	private Integer haveNoticed;
	
	/** 是否已经开始（1：否，2：是,3:结束）  */
	private Integer haveBegun;
	
	/** 聊天组ID  */
	private String chatGroupId;
	
	/** 群组状态（1：未创建，2：已创建，3：已删除）  */
	private Integer groupStatus;
	
	
	public Integer getInstId() {
		return instId;
	}
	public void setInstId(Integer instId) {
		this.instId = instId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getAuctionSerial() {
		return auctionSerial;
	}
	public void setAuctionSerial(String auctionSerial) {
		this.auctionSerial = auctionSerial;
	}
	public String getAuctionName() {
		return auctionName;
	}
	public void setAuctionName(String auctionName) {
		this.auctionName = auctionName;
	}
	public Integer getJoinNum() {
		return joinNum;
	}
	public void setJoinNum(Integer joinNum) {
		this.joinNum = joinNum;
	}
	public Integer getVisitNum() {
		return visitNum;
	}
	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}
	public BigDecimal getBail() {
		return bail;
	}
	public void setBail(BigDecimal bail) {
		this.bail = bail;
	}
	public BigDecimal getSpreadPackets() {
		return spreadPackets;
	}
	public void setSpreadPackets(BigDecimal spreadPackets) {
		this.spreadPackets = spreadPackets;
	}
	public String getSinglePackets() {
		return singlePackets;
	}
	public void setSinglePackets(String singlePackets) {
		this.singlePackets = singlePackets;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getHaveNoticed() {
		return haveNoticed;
	}
	public void setHaveNoticed(Integer haveNoticed) {
		this.haveNoticed = haveNoticed;
	}
	public Integer getHaveBegun() {
		return haveBegun;
	}
	public void setHaveBegun(Integer haveBegun) {
		this.haveBegun = haveBegun;
	}
	public String getChatGroupId() {
		return chatGroupId;
	}
	public void setChatGroupId(String chatGroupId) {
		this.chatGroupId = chatGroupId;
	}
	public Integer getGroupStatus() {
		return groupStatus;
	}
	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	public Integer getIsFocus() {
		return isFocus;
	}

	public void setIsFocus(Integer isFocus) {
		this.isFocus = isFocus;
	}

	public Integer getIsNotice() {
		return isNotice;
	}

	public void setIsNotice(Integer isNotice) {
		this.isNotice = isNotice;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	private String userAvatar;

	public String getChatGroupIds() {
		return chatGroupIds;
	}

	public void setChatGroupIds(String chatGroupIds) {
		this.chatGroupIds = chatGroupIds;
	}

	public String getProfessionIds() {
		return professionIds;
	}

	public void setProfessionIds(String professionIds) {
		this.professionIds = professionIds;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getInstTel() {
		return instTel;
	}

	public void setInstTel(String instTel) {
		this.instTel = instTel;
	}

	public String getInstSerial() {
		return instSerial;
	}

	public void setInstSerial(String instSerial) {
		this.instSerial = instSerial;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}