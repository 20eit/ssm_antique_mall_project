package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/

/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：AuctionInst <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖机构（拍场）表 <p>
 */
public class AuctionInst extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	
private String userSerial;
	private String username;
	private String realName;
	private String shopName;
	private Integer professionIngCount;
	private Integer professionPreCount;
	private Integer professionEndCount;
	private Integer prefessionStatus;
	private Integer goodsCount;
	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 用户ID  */
	private Integer userId;
	
	/** 店铺ID  */
	private Integer shopId;
	
	/** 拍卖机构简介  */
	private String introduce;
	
	/** 联系地址  */
	private String address;
	
	/** 拍场图片路径  */
	private String picPaths;
	
	/** 机构大图  */
	private String mainPic;
	
	/** 拍场名称  */
	private String name;
	
	/** 拍场等级  */
	private Integer grade;
	
	/** 等级图片路径  */
	private String gradePic;
	
	/** 电话  */
	private String tel;
	
	/** 点赞数量  */
	private Integer praiseNum;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}
	public String getMainPic() {
		return mainPic;
	}
	public void setMainPic(String mainPic) {
		this.mainPic = mainPic;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public String getGradePic() {
		return gradePic;
	}
	public void setGradePic(String gradePic) {
		this.gradePic = gradePic;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Integer getPraiseNum() {
		return praiseNum;
	}
	public void setPraiseNum(Integer praiseNum) {
		this.praiseNum = praiseNum;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	public Integer getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}

	public Integer getPrefessionStatus() {
		return prefessionStatus;
	}

	public void setPrefessionStatus(Integer prefessionStatus) {
		this.prefessionStatus = prefessionStatus;
	}

	public Integer getProfessionIngCount() {
		return professionIngCount;
	}

	public void setProfessionIngCount(Integer professionIngCount) {
		this.professionIngCount = professionIngCount;
	}

	public Integer getProfessionPreCount() {
		return professionPreCount;
	}

	public void setProfessionPreCount(Integer professionPreCount) {
		this.professionPreCount = professionPreCount;
	}

	public Integer getProfessionEndCount() {
		return professionEndCount;
	}

	public void setProfessionEndCount(Integer professionEndCount) {
		this.professionEndCount = professionEndCount;
	}

	public String getUserSerial() {
		return userSerial;
	}

	public void setUserSerial(String userSerial) {
		this.userSerial = userSerial;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}