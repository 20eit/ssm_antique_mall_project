package com.lhfeiyu.po;

import com.lhfeiyu.po.base.Parent;

/**============================== 自定义导入 开始 _@CAUTION_SELF_IMPORT_BEGIN@_ ==============================*/
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
/**============================== 自定义导入 结束 _@CAUTION_SELF_IMPORT_FINISH@_ ==============================*/

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 持久层对象：Auction <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖表 <p>
 */
public class Auction extends Parent {

	/**============================== 自定义字段 开始 _@CAUTION_SELF_FIELD_BEGIN@_ ==============================*/
	

	/**============================== 自定义字段 结束 _@CAUTION_SELF_FIELD_FINISH@_ ==============================*/

	/** 用户ID  */
	private Integer userId;
	
	/** 店铺ID  */
	private Integer shopId;
	
	/** 拍卖类型（专场，微拍，即时拍）  */
	private Integer auctionType;
	
	/** 拍场图片路径  */
	private String picPaths;
	
	/** 参拍人数  */
	private BigDecimal joinNum;
	
	/** 围观次数（访问次数）  */
	private Integer visitNum;
	
	/** 保证金  */
	private String bail;
	
	/** 推广红包  */
	private String spreadPackets;
	
	/** 单品红包  */
	private String singlePackets;
	
	/** 开拍时间  */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date auctionTime;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public Integer getAuctionType() {
		return auctionType;
	}
	public void setAuctionType(Integer auctionType) {
		this.auctionType = auctionType;
	}
	public String getPicPaths() {
		return picPaths;
	}
	public void setPicPaths(String picPaths) {
		this.picPaths = picPaths;
	}
	public BigDecimal getJoinNum() {
		return joinNum;
	}
	public void setJoinNum(BigDecimal joinNum) {
		this.joinNum = joinNum;
	}
	public Integer getVisitNum() {
		return visitNum;
	}
	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}
	public String getBail() {
		return bail;
	}
	public void setBail(String bail) {
		this.bail = bail;
	}
	public String getSpreadPackets() {
		return spreadPackets;
	}
	public void setSpreadPackets(String spreadPackets) {
		this.spreadPackets = spreadPackets;
	}
	public String getSinglePackets() {
		return singlePackets;
	}
	public void setSinglePackets(String singlePackets) {
		this.singlePackets = singlePackets;
	}
	public Date getAuctionTime() {
		return auctionTime;
	}
	public void setAuctionTime(Date auctionTime) {
		this.auctionTime = auctionTime;
	}
	
	/**=========================== 自定义GETSET方法开始 _@CAUTION_SELF_GETSET_BEGIN@_ ===========================*/
	
	/**=========================== 自定义GETSET方法结束 _@CAUTION_SELF_GETSET_FINISH@_ ===========================*/
	
}