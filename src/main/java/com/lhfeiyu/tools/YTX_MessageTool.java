package com.lhfeiyu.tools;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lhfeiyu.config.Const;
import com.lhfeiyu.dao.base.BaseSchedulersMapper;
import com.lhfeiyu.domain.base.YtxMessage;
import com.lhfeiyu.util.base.YTX_MSG;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 群发短信 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong>  <p>
 */
@Component
public class YTX_MessageTool {
	
	@Autowired
	private BaseSchedulersMapper sMapper;

	public void sendMsgByCount(Map<String, Object> map, int count, String auctionName) {
		String phones = null;
		String[] params = new String[] { auctionName };// 通知消息只有一个参数：拍场名称
		// phones = "18702827609";
		String mobanId = Const.rl_ytx_msg_moban_notice_id;
		YtxMessage ytxs = new YtxMessage();
		ytxs.setParams(params);
		ytxs.setMobanId(mobanId);
		ytxs.setIp(Const.rl_ytx_ip);
		ytxs.setPort(Const.rl_ytx_port);
		ytxs.setSid(Const.rl_ytx_sid);
		ytxs.setAppId(Const.rl_ytx_appId);
		ytxs.setAuthToken(Const.rl_ytx_authToken);
		if (count <= 50) {
			phones = sMapper.selectNoticeUserPhones(map);
			System.out.println(phones);
			// TODO 发送短信
			ytxs.setPhones(phones);
			 YTX_MSG.send(ytxs);
			//System.out.println("发送短信：phones-" + phones + ",mobanId-" + mobanId + ",params-" + params.toString());
		} else {
			int length = count / 50;
			map.put("count", 50);
			for (int i = 0; i <= length; i++) {
				map.put("start", 50 * i);
				phones = sMapper.selectNoticeUserPhones(map);
				ytxs.setPhones(phones);
				// TODO 发送短信
				 //YTX_MSG.send(ytxs);
				//System.out.println("发送短信：phones-" + phones + ",mobanId-" + mobanId + ",params-" + params.toString());
			}
		}
	}

	
}
