package com.lhfeiyu.task;

import org.springframework.beans.factory.annotation.Autowired;

import com.lhfeiyu.service.AuctionProfessionService;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 任务调度：拍卖相关操作 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong>  <p>
 */
public class AuctionJob {

	@Autowired
	private AuctionProfessionService apService;
	//@Autowired
	//private AuctionQuickService aqService;
	
	public void run(){//40秒执行一次
		//apService.checkAutoOffer();//一期先不开发自动竞价
		apService.updateProfessionGoodsOneDone();
		apService.checkProfessionGoodsAddOrder();//检查是否有需要生成订单的拍品记录
		
		//aqServiceupdateAuctionQuickGoodsOneDone();//TODO-tempHide-即时拍
		//aqServicecheckQuickGoodsAddOrder();//TODO-tempHide-即时拍
	}
	
}
