package com.lhfeiyu.task;

import org.springframework.beans.factory.annotation.Autowired;

import com.lhfeiyu.service.AuctionMicroService;
import com.lhfeiyu.service.AuctionProfessionService;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 任务调度：通知提示相关操作 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong>  <p>
 */
public class NoticeJob {

	@Autowired
	private AuctionProfessionService apService;
	//@Autowired
	//private AuctionQuickService aqService;
	@Autowired
	private AuctionMicroService amService;
	
	public void run(){//
		apService.updateCheckProfessionBegin();//检查拍场是否到开始时间，若开始则刷新拍品的开拍时间
		apService.updateCheckProfessionNotice();//检查要开拍的专场，提前通知
		
		//aqService.updateCheckAuctionQuickBegin();//TODO-tempHide-即时拍
		//aqService.updateCheckAuctionQuickNotice();//TODO-tempHide-即时拍
		
		updateCheckAuctionMicroOver();//检查微拍是否结束
	}
	
	public void updateCheckAuctionMicroOver(){
		amService.updateCheckAuctionMicroOver(null);
	}
	
}
