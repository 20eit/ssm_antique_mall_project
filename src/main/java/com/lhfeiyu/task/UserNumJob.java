package com.lhfeiyu.task;

import org.springframework.beans.factory.annotation.Autowired;

import com.lhfeiyu.service.SysJobService;
import com.lhfeiyu.service.UserJobService;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 任务调度：系统相关操作 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong>  <p>
 */
public class UserNumJob {

	@Autowired
	private UserJobService userJobServcie;

	public void run() {// 
		userJobServcie.cleanUserNum();
	}
}
