package com.lhfeiyu.config;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import org.springframework.core.Ordered;

@Component
@Aspect
public class LogAspect implements Ordered{
	
	@Override  
    public int getOrder() {  
        return 1;  
    }

  @Pointcut("execution(* com.lhfeiyu.action..*.*(..))")
  public void anyMethod() {}// 定义一个切入点and args(method,args,target,ex)
  
  @AfterThrowing(pointcut="anyMethod()", throwing="ex")//  && args(Throwable)
  public void afterThrowing(JoinPoint joinPoint,Throwable ex) {
    addLog("AfterThrowing");
  }

  @Around("anyMethod()")
  public Object doBasicProfiling(ProceedingJoinPoint point) throws Throwable {
    addLog(point, null);
    Object[] args = point.getArgs();
    Object returnValue = point.proceed(args);
    addLog(point,returnValue);
    return returnValue;
  }

  private void addLog(String word) {
	  //System.out.println("----------addLog----------"+word);
  }

  private void addLog(ProceedingJoinPoint point, Object returnValue) {
    addLog("Around");
  }
}
