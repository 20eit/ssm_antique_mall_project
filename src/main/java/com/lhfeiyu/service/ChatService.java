package com.lhfeiyu.service;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.base.BaseTip;
import com.lhfeiyu.dao.base.BaseGoodsMapper;
import com.lhfeiyu.dao.base.BaseShopMapper;
import com.lhfeiyu.dao.base.BaseUserMapper;
import com.lhfeiyu.po.base.BaseChat;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.BaseChatService;
import com.lhfeiyu.service.base.BaseNoticeService;
import com.lhfeiyu.service.base.BasePushService;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.DateFormat;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.vo.base.ChatMsg;
import com.lhfeiyu.vo.base.ChatMsgDomain;

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：Chat <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 继承基础库中的ChatService <p>
 */
@Service
public class ChatService extends BaseChatService {
	
	@Autowired
	BaseUserMapper userMapper;
	@Autowired
	BaseGoodsMapper goodsMapper;
	@Autowired
	BaseShopMapper shopMapper;
	@Autowired
	BaseNoticeService noticeService;
	
	public Map<String, Object> initYtxChat(BaseUser sessionUser, String receiverSerial, String gSerial) {
		Map<String, Object> map = CommonGenerator.getHashMap();
		Date date = new Date();
		int userId = sessionUser.getId();
		String username = sessionUser.getUsername();
		String serial = sessionUser.getSerial();
		String timeStamp = DateFormat.format(date, DateFormat.stampSdf);
		Map<String, Object> newMap = CommonGenerator.getHashMap();
		newMap.put("serial", receiverSerial);
		BaseUser receiver = userMapper.selectByCondition(newMap);
		//String sig = ConstField.generateSubSig(user.getThirdName(),user.getThirdPassword(),timeStamp);//帐号密码登陆
		String sig = Const.generateSubSig(serial, timeStamp);//应用登陆
		map.put("senderId", userId);
		map.put("userTokenId", sessionUser.getThirdName());
		map.put("userTokenPswd", sessionUser.getThirdPassword());
		map.put("senderName", username);
		map.put("senderAvatar", sessionUser.getAvatar());
		map.put("sig", sig);
		map.put("timeStamp", timeStamp);
		map.put("receiverId", receiver.getId());
		map.put("receiverTokenId", receiver.getThirdName());
		map.put("receiverName", receiver.getUsername());
		map.put("receiverAvatar", receiver.getAvatar());
		
		map.put("senderSerial", serial);//应用登陆对应的账号：用户序号
		map.put("receiverSerial", receiverSerial);//应用登陆对应的账号：用户序号
		
		if(Check.isNotNull(gSerial)){//加载商品
			newMap.clear();
			newMap.put("serial", gSerial);
			BaseGoods goods = goodsMapper.selectByCondition(newMap);
			BaseGoods newGoods = new BaseGoods();
			if(null != goods){
				newGoods.setId(goods.getId());
				newGoods.setGoodsName(goods.getGoodsName());
				newGoods.setPicPath(goods.getPicPath());
				newGoods.setShopId(goods.getShopId());
				newGoods.setMainStatus(goods.getMainStatus());
				//goods.setWholesaleUserId(g.getWholesaleUserId());
			}
			map.put("goods", newGoods);
		}
		return map;
	}
	
	public JSONObject getChatList(JSONObject json, Map<String, Object> map) {
		return super.getChatList(json, map);
	}

	public JSONObject addChat(JSONObject json, BaseUser sessionUser, BaseChat chat){
		int userId = sessionUser.getId();
		String username = sessionUser.getUsername();
		BaseUser db_user = userMapper.selectByPrimaryKey(userId);
		if(null == db_user.getPhone() || "".equals(db_user.getPhone())){
			return Result.failure(json, BaseTip.msg_bindPhone, BaseTip.code_bindPhone);
		}
		//TODO 检查禁止权限
		/*String chatYes = request.getParameter("chat");
		if(chatYes == "yes"){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			map.put("controlTypeId", 52);
			map.put("controlDate", 1);
			int userControlList = userControlService.selectCountByCondition(map);
			if(userControlList > 0){
				json.put("status", "failure");
				json.put("msg", "您被禁止进行评论,请联系管理员解除限制.");
				return json;
			}
		}*/
		super.addChat(json, chat, username);
		return Result.success(json);
	}
	
	
	public JSONObject updateChat(JSONObject json, BaseChat chat, String username){
		return super.updateChat(json, chat, username);
	}
	
	public JSONObject doPush(JSONObject json, BaseUser user, String msgContent, String[] receiver, String pushType) {
		Date date = new Date();
		ChatMsg msg = new ChatMsg();
		//String[] receiver = {"8001977500000004"};//接收者账号，如果是个人，最大上限100人/次，如果是群组，仅支持1个。
		msg.setMsgContent(msgContent);
		msg.setMsgDomain(new ChatMsgDomain().toJSONString());
		msg.setPushType(pushType);//推送类型，1：个人，2：群组，默认为1
		msg.setReceiver(receiver);
		msg.setSender(user.getThirdName());//发送人ID
		String param = msg.toJSONString();
		String timeStamp = DateFormat.format(date, DateFormat.stampSdf);
		String pushUrl = Const.getPushMsgUrl(timeStamp);
		String authorization = Const.getAuthorization(timeStamp);
		BasePushService.doPush(pushUrl, param, authorization);
		return Result.success(json);
	}

}

