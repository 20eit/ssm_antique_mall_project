package com.lhfeiyu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.DistributionIncomeMapper;
import com.lhfeiyu.po.DistributionIncome;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.CommonService;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：管理员 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2015-7-4 10:08:21 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 1.0 <p>
 */
@Service
public class DistributionIncomeService extends CommonService<DistributionIncome> {

	@Autowired
	DistributionIncomeMapper mapper;
	
	public List<BaseUser> selectIncomeListByCondition(Map<String,Object> map){
		return mapper.selectIncomeListByCondition(map);
	}

}

