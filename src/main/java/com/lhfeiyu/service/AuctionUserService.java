package com.lhfeiyu.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.AuctionUserMapper;
import com.lhfeiyu.dao.base.BaseUserMapper;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.AuctionUser;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Check;

@Service
public class AuctionUserService extends CommonService<AuctionUser>{
	@Autowired
	AuctionUserMapper mappper;
	@Autowired
	BaseUserMapper userMappper;
	
	public void addNotRepeatAuctionUser(String promoterSerial,
			BaseUser user, AuctionProfession ap){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("serial", promoterSerial);
		List<BaseUser> userList = userMappper.selectListByCondition(map);
		if(Check.isNotNull(userList)){
			BaseUser promoter = userList.get(0);
			map.clear();
			map.put("auctoinType", 1);
			map.put("auctionId", ap.getId());
			map.put("userId", user.getId());
			List<AuctionUser> auList = mappper.selectListByCondition(map);
			if(Check.isNotNull(auList)){//当前用户已经被推荐了，不再新增
			}else{
				AuctionUser au = new AuctionUser();
				au.setAuctionType(1);
				au.setAuctionId(ap.getId());
				au.setAuctionManagerId(ap.getUserId());
				au.setAuctionManagerName(ap.getUsername());
				au.setInviteUserId(promoter.getId());
				au.setInviteUsername(promoter.getUsername());
				au.setUserId(user.getId());
				au.setUsername(user.getUsername());
				au.setMainStatus(1);
				au.setCreatedAt(new Date());
				au.setCreatedBy("-SYS-");
				mappper.insertSelective(au);
			}
			
		}
	}

}
