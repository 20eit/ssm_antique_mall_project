package com.lhfeiyu.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.base.BaseConst;
import com.lhfeiyu.config.base.BaseTip;
import com.lhfeiyu.dao.base.BaseGoodsMapper;
import com.lhfeiyu.dao.base.BaseShopMapper;
import com.lhfeiyu.dao.base.BaseUserMapper;
import com.lhfeiyu.po.base.BaseComment;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseNotice;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.BaseCommentService;
import com.lhfeiyu.service.base.BaseNoticeService;
import com.lhfeiyu.thirdparty.wx.business.Message;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.Result;

/**
* <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：Commont <p>
* <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  <p>
* <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
* <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
* <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
* <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 继承基础库中的CommentService <p>
 */
@Service
public class CommentService extends BaseCommentService {
	
	@Autowired
	BaseUserMapper userMapper;
	@Autowired
	BaseGoodsMapper goodsMapper;
	@Autowired
	BaseShopMapper shopMapper;
	@Autowired
	BaseNoticeService noticeService;
	
	public JSONObject getCommentList(JSONObject json, Map<String, Object> map) {
		return super.getCommentList(json, map);
	}

	public JSONObject addComment(JSONObject json, BaseUser sessionUser, BaseComment comment){
		int userId = sessionUser.getId();
		String username = sessionUser.getUsername();
		BaseUser db_user = userMapper.selectByPrimaryKey(userId);
		if(null == db_user.getPhone() || "".equals(db_user.getPhone())){
			return Result.failure(json, BaseTip.msg_bindPhone, BaseTip.code_bindPhone);
		}
		//TODO 检查禁止权限
		/*String commentYes = request.getParameter("comment");
		if(commentYes == "yes"){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			map.put("controlTypeId", 52);
			map.put("controlDate", 1);
			int userControlList = userControlService.selectCountByCondition(map);
			if(userControlList > 0){
				json.put("status", "failure");
				json.put("msg", "您被禁止进行评论,请联系管理员解除限制.");
				return json;
			}
		}*/
		String typeCode = comment.getCommentTypeCode();
		Integer goodsId = comment.getObjectId();
		String content 	= comment.getContent();
		if(Check.isNull(typeCode) || Check.isNull(goodsId)){
			return Result.failure(json, BaseTip.msg_comment_obj_null,  BaseTip.code_comment_obj_null);//评论对象不能为空
		}
		if(Check.isNull(content)){
			return Result.failure(json, BaseTip.msg_content_null,  BaseTip.code_content_null);//内容不能为空
		}
		comment.setUserId(userId);
		comment.setUsername(username);
		comment.setUserIp(sessionUser.getLastLoginIp());
		super.addComment(json, comment, username);
		if(Check.strEqual(typeCode, BaseConst.comment_type_goods)){//评论商品
			commentGoods(json, comment, goodsId, sessionUser);
		}
		return Result.success(json);
	}
	
	public JSONObject commentGoods(JSONObject json, BaseComment comment, Integer goodsId, BaseUser sessionUser){
		if(null == goodsId)return Result.failure(json, BaseTip.msg_goods_null, BaseTip.code_goods_null);
		String username = sessionUser.getUsername();
		BaseGoods goods = goodsMapper.selectByPrimaryKey(goodsId);
		Integer shopUserId = goods.getUserId();
		BaseUser user = userMapper.selectByPrimaryKey(shopUserId);//没有关联店家ID，即不发通知，直接返回
		if(null == user){
			return json;//
		}
		String goodsName = goods.getGoodsName();
		BaseNotice notice = new BaseNotice();//给用户发消息通知
		String title = BaseTip.tp_comment_goods_success;
		title = title.replace("-GOODS_NAME-", goodsName).replace("-USERNAME-", username);
		String content = comment.getContent();
		notice.setTitle(title);
		notice.setContent(content);
		notice.setReceiverId(shopUserId);
		String linkUrl = "/goods/comment/"+goods.getSerial();
		notice.setLinkUrl(linkUrl);
		notice.setContent(content);
		noticeService.addNotice(json, notice, username);	//添加通知消息
		
		String openId = user.getWxOpenid();
		if(Check.isNotNull(openId)){ //微信消息通知
			String firstValue = title;
			String attr1Value = content;
			String attr2Value = goodsName;
			String remark = Const.wx_message_remark;
			JSONObject messageJson = Message.buildMsg(openId, Const.wx_moban_3, Const.page_user_center, 
							 "first", firstValue, "Content1", attr1Value, "Good", attr2Value, null, null, null, null, "remark", remark);
			Message.sendMessage(Const.wx_moban_3, messageJson);
			//JSONObject resultJson = Message.sendMessage(ConstField.wx_moban_10, messageJson);
		}
		return Result.success(json);
	}
	
	public JSONObject updateComment(JSONObject json, BaseComment comment, String username){
		return super.updateComment(json, comment, username);
	}
	
	

}