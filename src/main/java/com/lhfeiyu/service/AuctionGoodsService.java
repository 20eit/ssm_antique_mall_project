package com.lhfeiyu.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.dao.AuctionGoodsMapper;
import com.lhfeiyu.dao.AuctionProfessionMapper;
import com.lhfeiyu.dao.base.BaseGoodsMapper;
import com.lhfeiyu.dao.base.BaseGoodsPictureMapper;
import com.lhfeiyu.po.AuctionGoods;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseGoodsPicture;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.service.base.BaseUserFundService;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;

@Service
public class AuctionGoodsService extends CommonService<AuctionGoods>{
	@Autowired
	AuctionGoodsMapper agMapper;
	@Autowired
	AuctionProfessionMapper apMapper;
	@Autowired
	BaseGoodsPictureMapper gpMapper;
	@Autowired
	BaseGoodsMapper goodsMapper;
	@Autowired
	BaseUserFundService ufService;
	
	public int updateAutoOfferNULL(Map<String,Object> map){
		return agMapper.updateAutoOfferNULL(map);
	}
	
	public String selectGoodsIdsOfProfession(Map<String,Object> map){
		return agMapper.selectGoodsIdsOfProfession(map);
	}
	
	public Map<String,Object> professionGoods(Map<String,Object> map, BaseUser user) {
			JSONObject json = new JSONObject();
			Integer userId = user.getId();
			Map<String,Object> resultMap = CommonGenerator.getHashMap();
			AuctionProfession profession = apMapper.selectByCondition(map);
			List<AuctionGoods> goodsList = agMapper.selectListByCondition(map);
			BaseUserFund uf = ufService.selectUserFundByUserId(userId);//诚信保证金
			if(null == uf){
				map.put("failure","failure");
				map.put("status","failure");
				return map;
			}
			BigDecimal creditMoney = uf.getOtherFund();
			BigDecimal avaliableMoney = uf.getAvaliableMoney();
			if(null == creditMoney)creditMoney = new BigDecimal(0);
			if(null == avaliableMoney)avaliableMoney = new BigDecimal(0);
			if(Check.isNotNull(goodsList) && null != profession){
				Integer auctionId = profession.getId();
				resultMap.put("goodsList", goodsList);
				resultMap.put("ap", profession);
				resultMap.put("auctionId", auctionId);
				resultMap.put("professionId", auctionId);
				resultMap.put("creditMoney", creditMoney);
				json.put("ap", profession);
				json.put("auctionId", auctionId);
				json.put("professionId", auctionId);
				json.put("creditMoney", creditMoney);
				apMapper.updateVisitNumAdd(map);//递增围观人数
			}else{
				resultMap.put("ap_ag_null", 1);
				json.put("ap_ag_null", 1);
			}
			resultMap.put("paramJson", json);
			return resultMap;
	}
	
	public Map<String,Object> professionGoodsDetail(Map<String,Object> map, BaseUser user, Integer auctionGoodsId) {
		JSONObject json = new JSONObject();
		Integer userId = user.getId();
		Map<String,Object> resultMap = CommonGenerator.getHashMap();
		map.put("auctionGoodsId", auctionGoodsId);
		AuctionGoods auctionGoods = agMapper.selectByCondition(map);
		Integer goodsId = auctionGoods.getGoodsId();
		Integer auctionId = auctionGoods.getAuctionId();
		map.clear();
		map.put("auctionId", auctionId);
		AuctionProfession profession = apMapper.selectByCondition(map);
		List<AuctionGoods> allGoodsList = agMapper.selectListByCondition(map);
		BaseUserFund uf = ufService.selectUserFundByUserId(userId);//诚信保证金
		if(null == uf){
			map.put("failure","failure");
			map.put("status","failure");
			return map;
		}
		BigDecimal creditMoney = uf.getOtherFund();
		BigDecimal avaliableMoney = uf.getAvaliableMoney();
		if(null == creditMoney)creditMoney = new BigDecimal(0);
		if(null == avaliableMoney)avaliableMoney = new BigDecimal(0);
		//检查本场拍卖的保证金，标明是否足够
		map.clear();
		map.put("baseGoodsPictureId", goodsId);
		List<BaseGoodsPicture> picList = gpMapper.selectListByCondition(map);
		BaseGoods goods = goodsMapper.selectByPrimaryKey(goodsId);
		if(null != auctionGoods && null != profession){
			resultMap.put("auctionGoods", auctionGoods);
			resultMap.put("goodsList", allGoodsList);
			resultMap.put("picList", picList);
			resultMap.put("ap", profession);
			resultMap.put("goods", goods);
			resultMap.put("creditMoney", creditMoney);
			json.put("ap", profession);
			json.put("auctionGoodsId", auctionGoodsId);
			json.put("creditMoney", creditMoney);
		}else{
			resultMap.put("ap_ag_null", 1);
			json.put("ap_ag_null", 1);
		}
		resultMap.put("paramJson", json);
		return resultMap;
	}
	
}
