package com.lhfeiyu.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.BonusDetailMapper;
import com.lhfeiyu.po.BonusDetail;
import com.lhfeiyu.service.base.CommonService;

@Service
public class BonusDetailService extends CommonService<BonusDetail>{
	@Autowired
	BonusDetailMapper mappper;

	public String selectSumByCondition(Map<String,Object> map){
		return mappper.selectSumByCondition(map);
	}
}
