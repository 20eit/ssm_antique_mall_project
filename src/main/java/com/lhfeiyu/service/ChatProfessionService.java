package com.lhfeiyu.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Assets;
import com.lhfeiyu.dao.ChatProfessionMapper;
import com.lhfeiyu.po.ChatProfession;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 基础库-业务层：专场聊天记录 chatProfession<p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日14:30:35 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 1.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 包路径：com.lhfeiyu.service.ChatProfessionService <p>
 */
@Service
public class ChatProfessionService extends CommonService<ChatProfession> {

	@Autowired
	ChatProfessionMapper chatProfessionMapper;
	
	public JSONObject getChatProfessionList(JSONObject json, Map<String, Object> map) {
		List<ChatProfession> chatProfessionList = chatProfessionMapper.selectListByCondition(map);
		Integer total = chatProfessionMapper.selectCountByCondition(map);
		return Result.gridData(chatProfessionList, total, json);
	}
	
	/**
	 * 新增或修改专场聊天记录
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatProfession 专场聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject addOrUpdateChatProfession(JSONObject json, ChatProfession chatProfession, String username){
		if(null == chatProfession.getId()){//添加
			return addChatProfession(json, chatProfession, username);
		}else{//修改
			return updateChatProfession(json, chatProfession, username);
		}
	}
	
	/**
	 * 新增专场聊天记录（代码若已经存在则提示失败）
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatProfession 专场聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject addChatProfession(JSONObject json, ChatProfession chatProfession, String username){
		Date date = new Date();
		chatProfession.setId(null);
		chatProfession.setSerial(CommonGenerator.getSerialByDate(Assets.serial_prefix_chatProfession));
		chatProfession.setMainStatus(1);
		chatProfession.setCreatedBy(username);
		chatProfession.setCreatedAt(date);
		chatProfessionMapper.insert(chatProfession);
		json.put("id", chatProfession.getId());
		return Result.success(json);
	}
	
	/**
	 * 修改专场聊天记录（ID不能为空，数据库中必须存在该ID的数据）
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatProfession 专场聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject updateChatProfession(JSONObject json, ChatProfession chatProfession, String username){
		Date date = new Date();
		Integer chatProfessionId = chatProfession.getId();
		if(null == chatProfessionId){//添加
			return Result.failure(json, "编号为空，无法进行执行修改", "chatProfessionId_null");
		}
		ChatProfession dbChatProfession = chatProfessionMapper.selectByPrimaryKey(chatProfessionId);
		if(null == dbChatProfession){
			return Result.failure(json, "该条数据不存在，无法进行执行修改", "chatProfession_null");
		}

		chatProfession.setUpdatedBy(username);
		chatProfession.setUpdatedAt(date);
		chatProfessionMapper.updateByPrimaryKeySelective(chatProfession);
		return Result.success(json);
	}
	
	
}

