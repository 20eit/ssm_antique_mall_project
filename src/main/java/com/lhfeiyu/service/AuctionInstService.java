package com.lhfeiyu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.dao.AuctionInstMapper;
import com.lhfeiyu.po.AuctionInst;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Result;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：拍卖机构 AuctionInst <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日12:06:03 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 拍卖机构相关操作：拍卖机构，机构公告，机构点赞 <p>
 */
@Service
public class AuctionInstService extends CommonService<AuctionInst>{
	
	@Autowired
	AuctionInstMapper auctionInstMapper;
	
	public JSONObject getAuctionInstList(JSONObject json, Map<String, Object> map) {
		map.put("orderBy", "prefessionStatus DESC, A.grade");
		map.put("ascOrdesc", "DESC");
		map.put("selfOrder", 1);
		map.put("prefessionStatus", 1);
		map.put("goodsCount", 1);
		List<AuctionInst> aiList = auctionInstMapper.selectListByCondition(map);
		Integer total = auctionInstMapper.selectCountByCondition(map);
		return Result.gridData(aiList, total, json);
	}
	
	
}
