package com.lhfeiyu.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.Page;
import com.lhfeiyu.dao.AuctionProfessionMapper;
import com.lhfeiyu.dao.base.BaseShopMapper;
import com.lhfeiyu.dao.base.BaseUserControlMapper;
import com.lhfeiyu.dao.base.BaseUserMapper;
import com.lhfeiyu.po.AuctionProfession;
import com.lhfeiyu.po.base.BaseShop;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.po.base.BaseUserFund;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.CommonGenerator;

@Service
public class AuctionHallService extends CommonService<AuctionProfession> {
	@Autowired
	AuctionProfessionMapper apMapper;
	@Autowired
	BaseUserMapper userMapper;
	@Autowired
	BaseShopMapper shopMapper;
	@Autowired
	BaseUserControlMapper userControlMapper;

	public ModelAndView professionHall(Map<String, Object> modelMap, Map<String, Object> paramMap, BaseUser user, Integer auctionId) {
		String path = Page.professionHall;
		Date date = new Date();
			int userId = user.getId();
			BaseUser db_user = userMapper.selectByPrimaryKey(userId);
			/*
			if(null == db_user.getPhone() || "".equals(db_user.getPhone())){
				modelMap.put("noPhone", "noPhone");//未绑定手机号码
			}
			map.put("userId", userId);
			BaseUserFund userFund = userFundService.selectUserFundByUserId(userId);
			if(null == userFund){
				BaseUserFund userFund = userFundList.get(0);
				modelMap.put("userFund", userFund);
				if(null == userFund.getPayPassword() || "".equals(userFund.getPayPassword())){
					modelMap.put("noPayPassword", "noPayPassword");//未设置支付密码
				}
			}
			*/
			Map<String,Object> map = CommonGenerator.getHashMap();
			map.put("userId", userId);
			BaseShop shop = shopMapper.selectByCondition(map);
			if(null != shop){
				modelMap.put("shop", shop);
			}
			map.clear();
			paramMap.put("auctionId", auctionId);//在URL路径中获取的参数不为被自动获取，需要手动存入paramMap
			AuctionProfession ap = apMapper.selectByCondition(paramMap);
			if(null == ap){
				
			}
				
			//TODO 更新拍场在线人数，同一用户只更新updated_at,通过该字段判断在线人数
		 	/*map.clear();
		 	map.put("auctionId", auctionId);
		 	map.put("auctionType", 1);//专场
		 	map.put("userId", userId);
		 	List<AuctionUser> auList = auService.selectListByCondition(map);
		 	if(Check.isNotNull(auList)){
		 		AuctionUser au = auList.get(0);
		 		au.setUpdatedAt(date);
		 		auService.updateByPrimaryKey(au);
		 	}else{
		 		AuctionUser au = new AuctionUser();
		 		au.setUpdatedAt(date);
		 		au.setAuctionId(auctionId);
		 		au.setAuctionType(1);
		 		au.setUserId(userId);
		 		au.setUsername(user.getUsername());
		 		au.setMainStatus(1);
		 		auService.insertSelective(au);
		 	}*/
		 	
			Integer hostId = ap.getUserId();
			if(null != hostId){//判断是否为主持人
				int hostUserId = hostId;
				if(hostUserId == userId){
					modelMap.put("host", 1);
					map.clear();
				 	map.put("auctionId", auctionId);
				 	map.put("auctionType", 1);//专场
				 	map.put("showRecentUserCount", 1);
					//int userCount = auService.selectCountByCondition(map);
					//modelMap.put("userCount", userCount);
				}
			}
			modelMap.put("ap", ap);
			BigDecimal bailBD = ap.getBail();
			BigDecimal creditMoney = db_user.getOtherFund();
			//信誉保证金:信誉保证金必须大于保证金
			if(null != bailBD && (null == creditMoney || bailBD.doubleValue() > creditMoney.doubleValue())){
				modelMap.put("creditMoneyLack", 1);//前台提示交纳的保证金不足
				modelMap.put("bail", bailBD.doubleValue());
				modelMap.put("creditMoney", creditMoney);
			}
			
			String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
			//String sig = ConstField.generateSubSig(user.getThirdName(),user.getThirdPassword(),timeStamp);
			String sig = Const.generateSubSig(user.getSerial(), timeStamp);//应用登陆
			modelMap.put("username", user.getUsername());
			modelMap.put("senderId", userId);
			modelMap.put("userTokenId", user.getThirdName());
			modelMap.put("userTokenPswd", user.getThirdPassword());
			modelMap.put("senderName", user.getUsername());
			modelMap.put("senderAvatar", user.getAvatar());
			modelMap.put("sig", sig);
			modelMap.put("timeStamp", timeStamp);
			/*modelMap.put("receiverId",receiver.getId());
			modelMap.put("receiverTokenId", receiver.getThirdName());
			modelMap.put("reveiverName", receiver.getUsername());
			modelMap.put("reveiverAvatar", receiver.getAvatar());*/
			
			modelMap.put("senderSerial", user.getSerial());//应用登陆对应的账号：用户序号
			
			// TODO 判断是否是推广进入,新增（auction_user）
			/*if(null != r){//有推广人
				String userSerial = user.getSerial();
				if(!r.equals(userSerial)){//推广人不是自己
					auService.addNotRepeatAuctionUser(r, user, ap);//新增推广记录，不会新增重复数据
				}
			}*/
			
			//TODO 检查用户的可用金额是否大于保证金金额
		 	BigDecimal bail = ap.getBail();
		 	if(null != bail && bail.doubleValue()>0){
		 		//UserFund uf = ufService.selectUserFundByUserId(userId);
		 		//DOTO 修改
		 		BaseUserFund uf = new BaseUserFund();
		 		if(null != uf && null != uf.getAvaliableMoney() && uf.getAvaliableMoney().doubleValue() >= bail.doubleValue()){
		 			map.clear();
					map.put("auctionId", auctionId);
		 			apMapper.updateJoinNumAdd(map);//递增参拍人数
		 		}else{
		 			path = Page.professionGoods;//
		 			modelMap.put("status", "bail_lack");
		 			modelMap.put("auctionId", auctionId);
		 		}
		 	}
		return new ModelAndView(path, modelMap);
	}



}
