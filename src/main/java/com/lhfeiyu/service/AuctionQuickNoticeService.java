package com.lhfeiyu.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.AuctionQuickNoticeMapper;
import com.lhfeiyu.po.AuctionQuickNotice;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Check;

@Service
public class AuctionQuickNoticeService extends CommonService<AuctionQuickNotice>{
	@Autowired
	AuctionQuickNoticeMapper aqnMappper;
	
	public String getMyAuctionQuickNotice(Integer userId) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", userId);
			List<AuctionQuickNotice> aqnList = aqnMappper.selectListByCondition(map);
			if(Check.isNotNull(aqnList)){
				AuctionQuickNotice aqn = aqnList.get(0);
				if(Check.isNotNull(aqn.getGoodsTypeIds())){
					return aqn.getGoodsTypeIds();
				}
			}
		return null;
	}
		
}
