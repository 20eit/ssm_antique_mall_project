package com.lhfeiyu.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.config.base.BaseConst;
import com.lhfeiyu.dao.AuctionProfessionMapper;
import com.lhfeiyu.dao.WholesaleMapper;
import com.lhfeiyu.dao.base.BaseGoodsMapper;
import com.lhfeiyu.dao.base.BaseNoticeMapper;
import com.lhfeiyu.dao.base.BaseOrderGoodsMapper;
import com.lhfeiyu.dao.base.BaseOrderInfoMapper;
import com.lhfeiyu.dao.base.BaseShopMapper;
import com.lhfeiyu.dao.base.BaseUserFundMapper;
import com.lhfeiyu.dao.base.BaseUserMapper;
import com.lhfeiyu.domain.base.Trade;
import com.lhfeiyu.po.AuctionMicro;
import com.lhfeiyu.po.Wholesale;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.po.base.BaseNotice;
import com.lhfeiyu.po.base.BaseOrderGoods;
import com.lhfeiyu.po.base.BaseOrderInfo;
import com.lhfeiyu.po.base.BaseShop;
import com.lhfeiyu.po.base.BaseUser;
import com.lhfeiyu.service.base.BaseCommonFundService;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;
import com.lhfeiyu.vo.base.ChatGroup;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：AuctionMicro </p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华  </p>
 * <strong> 编写时间：</strong>2016年3月20日22:22:22<p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com </p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 2.0 </p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 业务：拍卖-微拍表 </p>
 */
@Service
public class SysJobService extends CommonService<AuctionMicro> {

	@Autowired
	private AuctionProfessionMapper apMapper;
	@Autowired
	private BaseGoodsMapper goodsMapper;
	@Autowired
	private BaseOrderGoodsMapper orderGoodsMapper;
	@Autowired
	private BaseOrderInfoMapper orderInfoMapper;
	@Autowired
	private BaseNoticeMapper noticeMapper;
	@Autowired
	private CreditService creditService;
	@Autowired
	private BaseCommonFundService commonFundService;
	@Autowired
	private BaseUserMapper userMapper;
	@Autowired
	private BaseShopMapper shopMapper;
	@Autowired
	private WholesaleMapper wholesaleMapper;
	@Autowired
	private BaseUserFundMapper userFundMapper;

	public void deleteGroupWhichAuctionOver() {
		String groupIds = apMapper.selectGroupToDelete();
		if (null != groupIds && groupIds.length() > 0) {
			ChatGroup group = new ChatGroup();
			group.setThirdName(Const.rl_ytx_default_subAccount);
			group.setThirdPassword(Const.rl_ytx_default_password);
			String[] idAry = groupIds.split(",");
			for (String groupId : idAry) {
				group.setGroupId(groupId);
				ChatGroupService.deleteGroup(group);
			}
			apMapper.updateChatGroupStatus();
		}
	}

	public void checkGoodsNoPay() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mainStatus", 78);
		List<BaseGoods> goodsList = goodsMapper.selectListByCondition(map);
		if (Check.isNull(goodsList))return;
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -7);// 7天前
		Date deadLineTime = c.getTime();
		map.clear();
		for (BaseGoods goods : goodsList) {
			Integer goodsId = goods.getId();
			map.put("goodsId", goodsId);
			map.put("mainStatus", 1);//订单商品完成状态（1未完成，2已完成，3已退货或取消）
			List<BaseOrderGoods> orderGoodsList = orderGoodsMapper.selectListByCondition(map);
			if (!Check.isNotNull(orderGoodsList))
				continue;
			BaseOrderGoods orderGoods = orderGoodsList.get(0);
			Date createTime = orderGoods.getCreatedAt();// 订单创建时间
			if (!deadLineTime.after(createTime))
				continue;
			
			Integer orderId = orderGoods.getOrderId();
			BaseOrderInfo oi = orderInfoMapper.selectByPrimaryKey(orderId);
			if(null == oi || !Check.strEqual(BaseConst.pay_status_todo, oi.getPayStatusCode())){//数据字典代码：支付状态
				continue;//订单不存在或者商品不是未付款状态，就不往下执行
			}
			orderGoods.setMainStatus(3);//订单商品完成状态（1未完成，2已完成，3已退货或取消）
			orderGoodsMapper.updateByPrimaryKeySelective(orderGoods);// 更新订单商品状态
			BaseOrderInfo orderInfo = new BaseOrderInfo();
			orderInfo.setId(orderId);
			orderInfo.setOrderStatusCode(BaseConst.order_status_invalid);//数据字典代码：订单的状态
			orderInfo.setMainStatus(3);//状态（1未结束订单，2已结束订单，3无效订单）
			orderInfoMapper.updateByPrimaryKeySelective(orderInfo);// 更新订单状态
			// 给买家和卖家双方发送消息
			BaseNotice notice = new BaseNotice();// 买家
			String content = "【" + orderGoods.getGoodsName() + "】由于您超过七天未付款,系统取消了此次交易";
			notice.setSerial(CommonGenerator.getSerialByDate("n"));
			notice.setReceiverId(orderGoods.getOrderUserId());
			notice.setTitle(content);
			notice.setContent(content);
			notice.setReadStatus(1);// 未阅读
			notice.setTypeId(33);// 新订单生成
			notice.setCreatedAt(date);
			notice.setCreatedBy("-SYS-");
			noticeMapper.insert(notice);
			BaseNotice notice1 = new BaseNotice();// 卖家
			notice1.setSerial(CommonGenerator.getSerialByDate("n"));
			content = "【" + orderGoods.getGoodsName() + "】由于买家超过七天未付款,系统取消了此次交易";
			Integer goodsType = orderGoods.getGoodsType();
			Integer shopId = null;
			if (null != goodsType && goodsType == 2) {
				notice1.setReceiverId(orderGoods.getWholesaleUserId());
				shopId = orderGoods.getWholesaleUserId();
			} else {
				notice1.setReceiverId(orderGoods.getShopUserId());
				shopId = orderGoods.getShopUserId();
			}
			notice1.setTitle(content);
			notice1.setContent(content);
			notice1.setReadStatus(1);// 未阅读
			notice1.setTypeId(33);// 新订单生成
			notice1.setCreatedAt(date);
			notice1.setCreatedBy("-SYS-");
			noticeMapper.insert(notice1);
			// 生成违约记录
			// 更新商家信誉，个人信誉
			BaseOrderInfo db_orderInfo = orderInfoMapper.selectByPrimaryKey(orderId);
			Integer receiverId = db_orderInfo.getUserId();// 买家
			creditService.updateUserCredit(receiverId);// 更新用户的信誉
			//User user = userMapper.selectByPrimaryKey(receiverId);
			//String username = user.getUsername();
			//JSONObject json = new JSONObject();// 解除冻结买家的金额
			//commonFundService.activateMoney(null, orderGoods.getShopPrice(), receiverId, username, 141, 0, "-SYS-", json, false);//没有支付就不会有解冻
			if (null != shopId) {
				creditService.updateShopCredit(shopId, receiverId);// 更新商家信誉
			}
			goods.setMainStatus(76);
			goodsMapper.updateByPrimaryKeySelective(goods);// 更新商品状态
			//break;
		}
	}

	public void checkGoodsNoSendGoods() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("goodsAttrISNULL", 1);//占用：快递单号
		map.put("mainStatus", 1);//订单商品完成状态（1未完成，2已完成，3已退货或取消）
		map.put("payStatus", 3);//支付状态;1未付款;2付款中;3已付款
		map.put("shippingStatus", 1);//商品配送情况;1未发货,2已发货,3已收货,4退货
		map.put("orderStatus", 1);//订单的状态;1未完成,2已完成,3已取消,4无效,5退货
		List<BaseOrderGoods> orderGoodsList = orderGoodsMapper.selectListByCondition(map);
		if (!Check.isNotNull(orderGoodsList))
			return;
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -30);// 30天前
		Date deadLineTime = c.getTime();
		map.clear();
		for (BaseOrderGoods og : orderGoodsList) {
			Date d = og.getCreatedAt();// 订单创建时间
			if (!deadLineTime.after(d))continue;
			og.setMainStatus(2);
			Integer goodsId = og.getGoodsId();
			orderGoodsMapper.updateByPrimaryKeySelective(og);// 更新订单商品状态
			Integer orderId = og.getOrderId();
			BaseOrderInfo orderInfo = new BaseOrderInfo();
			orderInfo.setId(orderId);
			orderInfo.setOrderStatusCode(BaseConst.order_status_invalid);// 无效
			orderInfo.setMainStatus(3);//状态（1未结束订单，2已结束订单，3无效订单）
			orderInfoMapper.updateByPrimaryKeySelective(orderInfo);// 更新订单状态
			// 给买家和卖家双方发送消息
			BaseNotice notice = new BaseNotice();// 买家
			String content = "【" + og.getGoodsName() + "】由于卖家超过七天未发货,系统取消了此次交易";
			notice.setSerial(CommonGenerator.getSerialByDate("n"));
			notice.setReceiverId(og.getOrderUserId());
			notice.setTitle(content);
			notice.setContent(content);
			notice.setReadStatus(1);// 未阅读
			notice.setTypeId(33);// 新订单生成
			notice.setCreatedAt(date);
			notice.setCreatedBy("-SYS-");
			noticeMapper.insert(notice);
			BaseNotice notice1 = new BaseNotice();// 卖家
			notice1.setSerial(CommonGenerator.getSerialByDate("n"));
			content = "【" + og.getGoodsName() + "】由于您超过七天未发货,系统取消了此次交易";
			Integer goodsType = og.getGoodsType();
			Integer shopId = null;
			if (null != goodsType && goodsType == 2) {
				notice1.setReceiverId(og.getWholesaleUserId());
				shopId = og.getWholesaleUserId();
			} else {
				notice1.setReceiverId(og.getShopUserId());
				shopId = og.getShopUserId();
			}
			notice1.setTitle(content);
			notice1.setContent(content);
			notice1.setReadStatus(1);// 未阅读
			notice1.setTypeId(33);// 新订单生成
			notice1.setCreatedAt(date);
			notice1.setCreatedBy("-SYS-");
			noticeMapper.insert(notice1);
			// 生成违约记录
			// 更新商家信誉，个人信誉
			BaseOrderInfo db_orderInfo = orderInfoMapper.selectByPrimaryKey(orderId);
			Integer receiverId = db_orderInfo.getUserId();// 买家
			creditService.updateUserCredit(receiverId);// 更新用户的信誉
			BaseUser user = userMapper.selectByPrimaryKey(receiverId);
			String username = user.getUsername();
			JSONObject json = new JSONObject();// 解除冻结买家的金额
			//TODO 修改
			//commonFundService.activateMoney(null, og.getShopPrice(), receiverId, username, 141, 0, "-SYS-", json, false,getLinkUrl(goodsId));
			if (null != shopId) {
				creditService.updateShopCredit(shopId, receiverId);// 更新商家信誉
			}
			BaseGoods g = new BaseGoods();
			g.setId(og.getGoodsId());
			g.setMainStatus(1);
			goodsMapper.updateByPrimaryKeySelective(g);// 更新商品状态
			break;
		}
	}

	public void checkGoodsReceive() {
		JSONObject json = new JSONObject();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("payStatusCode", BaseConst.pay_status_done);//支付状态;1未付款;2付款中;3已付款
		map.put("shippingStatusCode", BaseConst.shipping_status_done);//商品配送情况;1未发货,2已发货,3已收货,4退货
		map.put("orderStatusCode", BaseConst.order_status_todo);//订单的状态;1未完成,2已完成,3已取消,4无效,5退货
		map.put("expressStateOver", 1);
		List<BaseOrderInfo> orderInfoList = orderInfoMapper.selectListByCondition(map);//查询出所有已发货但未收货的订单（商品配送情况;1未发货,2已发货,3已收货,4退货）
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -15);// 15天后,自动收货
		Date deadLineTime = c.getTime();
		if (Check.isNull(orderInfoList))return;
		for (BaseOrderInfo oi : orderInfoList) {
			map.clear();
			Integer orderId = oi.getId();
			Integer receiverId = oi.getUserId();
			String username = oi.getUsername();
			map.put("orderId", orderId);
			List<BaseOrderGoods> orderGoodsList = orderGoodsMapper.selectListByCondition(map);// 查询出该订单的所有商品
			if (Check.isNull(orderGoodsList)) continue;//过滤掉没有订单商品的订单
			int result = receiveGoods(oi, orderGoodsList, receiverId, username, map, deadLineTime, json);
			if(result == 1)continue;
			else if(result == 2)break;
		}
	}
	
	
	private int receiveGoods(BaseOrderInfo oi, List<BaseOrderGoods> orderGoodsList, Integer receiverId,String username, Map<String, Object> map, Date deadLineTime, JSONObject json){
		Date date = new Date();
		for (BaseOrderGoods og : orderGoodsList) {
			Date createTime = og.getCreatedAt();// 订单创建时间
			if (!deadLineTime.after(createTime) || (null != og.getMainStatus() && og.getMainStatus() != 1))continue;
			BigDecimal shopPrice = og.getShopPrice();
			if (null == shopPrice || shopPrice.doubleValue() <= 0)continue;
			Integer goodsType = null;
			//Integer wholesaleShopId = null;
			//Integer finalShopId = null;
			//String goodsName = null;
			goodsType = og.getGoodsType();// 判断goodsType为空的情况（continue）
			//goodsName = og.getGoodsName();
			Integer goodsId = og.getGoodsId();
			BaseGoods goods = goodsMapper.selectByPrimaryKey(goodsId);
			Integer sellerId = goods.getId();
			//String sellerName = goods.getUsername();
			if(null == goods || null == goodsId){
				continue;//当前藏品已经不存在，跳过
			}
			
			Integer customerId = oi.getUserId();// 订单客户ID，向客户发送通知消息
			String customerName = oi.getUsername();
			
			Integer shopUserId = og.getId();
			String shopUsername = og.getUsername();
			
			//Integer theOtherId = null;// 卖家id receiverId 买家id
			//String theOtherName = null;
			Integer shopId = og.getShopId();
			Integer goodsNum = og.getGoodsNumber();
			if (null == goodsNum || goodsNum <= 0) {
				goodsNum = 1;
			}
			BigDecimal totalMoney = shopPrice.multiply(new BigDecimal(goodsNum));// 判断goodsNumber为空的情况（为空则等于1）,判断totalMoney是否大于0
			if (null == totalMoney || totalMoney.doubleValue() <= 0)
				continue;
			map.clear();
			/*map.put("id", shopId);
			if (null != goodsType && goodsType == 2) {// 根据商品类型查处对应的商家姓名和id
				List<Wholesale> wholesaleList = wholesaleMapper.selectListByCondition(map);
				if (wholesaleList.size() > 0) {
					Wholesale wholesale = wholesaleList.get(0);
					theOtherId = wholesale.getUserId();
					theOtherName = wholesale.getUserName();
					//wholesaleShopId = wholesale.getWholesaleShopId();
					//finalShopId = wholesaleShopId;
				}
			} else {
				List<BaseShop> shopList = shopMapper.selectListByCondition(map);
				if (shopList.size() > 0) {
					BaseShop shop = shopList.get(0);
					theOtherId = shop.getUserId();
					theOtherName = shop.getUserName();
					//finalShopId = shopId;
				}
			}*/
			// 1.实际扣买家，卖家增加
			// 扣除商品的钱
			//BaseUserFund uf = userFundMapper.selectUserFundByUserId(receiverId);
			//TODO 修改
			Trade trade = new Trade();
			trade.setMoney(totalMoney);
			trade.setFundOptLevelCode(BaseConst.fund_opt_level_sys);
			trade.setPayPass("");
			trade.setUserId(customerId);
			trade.setInOrOut(2);
			trade.setPayTime(date);
			trade.setTradeUserId(shopUserId);
			trade.setTradeUsername(shopUsername);
			trade.setTradeStatusCode(BaseConst.trade_status_done);
			trade.setCreatedBy("SYS-JOB");
			
			json = commonFundService.outcomeFrozenMoney(json, trade, shopUsername);
			if (Result.hasError(json)) {
				commonSendnotice(receiverId, json.getString("msg"), json.getString("msg"));
				return 1;//扣钱出现异常，不再往下执行
			}
			trade.setMoney(totalMoney);
			trade.setFundOptLevelCode(BaseConst.fund_opt_level_sys);
			trade.setPayPass("");
			trade.setUserId(shopUserId);
			trade.setInOrOut(1);
			trade.setPayTime(date);
			trade.setTradeUserId(customerId);
			trade.setTradeUsername(customerName);
			trade.setTradeStatusCode(BaseConst.trade_status_done);
			trade.setCreatedBy("SYS-JOB");
			json = commonFundService.incomeMoney(json, trade, shopUsername);
			//json = commonFundService.outcomeFrozenMoneyToUserAvaliable(json, trade, customerName );
			if (Result.hasError(json)) {
				commonSendnotice(receiverId, json.getString("msg"), json.getString("msg"));
				return 1;//扣钱出现异常，不再往下执行
			}
			// 扣除卖家推广费用
			/*BigDecimal promoteBenifitMoney = og.getPromoteBenifitMoney();// 推广金额
			if (null != promoteBenifitMoney && promoteBenifitMoney.doubleValue() > 0) {
				//TODO 修改
				//json = commonFundService.outcomeFrozenMoney(null, promoteBenifitMoney, receiverId, username, 140, theOtherId, theOtherName, json, true,getLinkUrl(goodsId));
				if (json.containsKey("error_desc")) {
					commonSendnotice(sellerId, json.getString("msg"), json.getString("msg"));
					return 1;//扣钱出现异常，不再往下执行
				}
			}*/
			// 2.更新goods、order_info、order_goods状态
			BaseGoods goodsNew = new BaseGoods();
			goodsNew.setId(goodsId);
			goodsNew.setMainStatus(77);
			goodsNew.setUpdatedAt(date);
			goodsMapper.updateByPrimaryKeySelective(goodsNew);// 更新goods状态
			og.setMainStatus(2);
			og.setUpdatedAt(date);
			orderGoodsMapper.updateByPrimaryKeySelective(og);// 更新order_goods状态
			oi.setOrderStatusCode(BaseConst.order_status_done);
			oi.setShippingStatusCode(BaseConst.shipping_status_receive);
			oi.setMainStatus(2);
			oi.setUpdatedAt(date);
			orderInfoMapper.updateByPrimaryKeySelective(oi);
			// 3.更新商家信誉，个人信誉
			creditService.updateUserCredit(receiverId);// 更新用户的信誉
			creditService.updateShopCredit(shopId, receiverId);
			
			//TODO tempHide - 推广费用相关
			// 5.检查商品是否有推广人，如果有推广人，更新推广人的收入（检查所有淘客相关的收入）
			/*if (null == promoteBenifitMoney || promoteBenifitMoney.doubleValue() <= 0)continue;//没有推广费，直接跳过
			map.clear();
			String userSerial = og.getPromoteUserSerial();
			User db_user = null;
			Integer db_userId = null;// 推广人id
			String db_userName = null;// 推广人姓名
			if (!Check.isNotNull(userSerial))continue; // 没有推广人，直接跳过
			map.put("serial", userSerial);
			List<User> userList = userMapper.selectListByCondition(map);
			if (Check.isNotNull(userList)) {
				db_user = userList.get(0);
				db_userId = db_user.getId();
				db_userName = db_user.getUsername();
			}
			map.clear();
			map.put("customerId", receiverId);
			List<UserCustomer> userCustomerList = userCustomerMapper.selectListByCondition(map);
			UserCustomer db_userCustomer = null;
			Integer db_userCustomerId = null;// 介绍人id
			String db_userCustomerName = null;// 介绍人姓名
			if (Check.isNotNull(userCustomerList)) {
				db_userCustomer = userCustomerList.get(0);
				db_userCustomerId = db_userCustomer.getUserId();
				db_userCustomerName = db_userCustomer.getUsername();
			}
			if(null == db_userCustomer && (null == db_user || db_userId.intValue() == receiverId.intValue())){//没有推广人也没有介绍人，直接跳过
				json = commonFundService.activateMoney(null, promoteBenifitMoney, sellerId, sellerName, 150, 0,"-SYS-", json, false,getLinkUrl(goodsId));// 没有介绍人和推广人：不扣钱(推广费)从冻结的资金激活为可用
				if (json.containsKey("error_desc")) {
					commonSendnotice(sellerId, json.getString("msg"), json.getString("msg"));
					return 1;//扣钱出现异常，不再往下执行
				}
				continue;
			};
			boolean noPromoterFlag = false;
			if(null == db_user || db_userId.intValue() == receiverId.intValue()){
				noPromoterFlag = true;
			}
			// 1.判断是否为自己,自己不计算收益
			// 2.判断他的介绍人是谁，和推广人是否是同一个人，是：70% 不是：30% 推广人：40%
			if (null != db_userCustomerId && !noPromoterFlag && db_userCustomerId == db_userId) {// 介绍人和推广人是同一个人,收益70%
				BigDecimal promoteMoney = promoteBenifitMoney.multiply(new BigDecimal(0.7));// 应该是从卖家的冻结金额中转到淘客用户账户可用余额
				if(null == promoteMoney || promoteMoney.doubleValue() <= 0)continue;//收益金额异常，不计算收益
				json = commonFundService.incomeMoney(null, promoteMoney, db_userId, db_userName, 148, 0, "-SYS-", json, false,getLinkUrl(goodsId));
				if (json.containsKey("error_desc")) {
					commonSendnotice(receiverId, json.getString("msg"), json.getString("msg"));
					return 1;//扣钱出现异常，不再往下执行
				}
				commonDistributionIncome(db_userId, theOtherId, goodsName, promoteMoney);// 分销收入
			}else{
				if(null != db_userCustomerId){//计算介绍人收益
					BigDecimal promoteUserCustomerMoney = promoteBenifitMoney.multiply(new BigDecimal(0.3));// 应该是从卖家的冻结金额中转到淘客用户账户可用余额
					if (null == promoteUserCustomerMoney || promoteUserCustomerMoney.doubleValue() <= 0)continue;//没有推广费，直接跳过
					json = commonFundService.incomeMoney(null, promoteUserCustomerMoney, db_userCustomerId, db_userCustomerName, 148, 0, "-SYS-", json, false,getLinkUrl(goodsId));
					if (json.containsKey("error_desc")) {
						commonSendnotice(db_userCustomerId, json.getString("msg"), json.getString("msg"));
						return 1;//扣钱出现异常，不再往下执行
					}
					commonDistributionIncome(db_userCustomerId, theOtherId, goodsName, promoteUserCustomerMoney);// 分销收入
				}
				if(!noPromoterFlag){//计算推广人收益
					BigDecimal promoteMoney = promoteBenifitMoney.multiply(new BigDecimal(0.4));// 应该是从卖家的冻结金额中转到淘客用户账户可用余额
					if (null == promoteMoney || promoteMoney.doubleValue() <= 0)continue;//没有推广费，直接跳过
					json = commonFundService.incomeMoney(null, promoteMoney, db_userId, db_userName, 148, 0, "-SYS-", json, false,getLinkUrl(goodsId));
					if (json.containsKey("error_desc")) {
						commonSendnotice(db_userId, json.getString("msg"), json.getString("msg"));
						return 1;//扣钱出现异常，不再往下执行
					}
					commonDistributionIncome(db_userId, theOtherId, goodsName, promoteMoney);// 分销收入
				}
			}
			BigDecimal promoteUserCustomerMoney = promoteBenifitMoney.multiply(new BigDecimal(0.3));// 平台收取收益
			if (null == promoteUserCustomerMoney || promoteUserCustomerMoney.doubleValue() <= 0)continue;//没有推广费，直接跳过
			sysDictService.incomeMoney(promoteUserCustomerMoney, 148, json);*/
		}
		return 0;
	}
	
	/*//TODO tempHide
	 * private void commonDistributionIncome(Integer userId, Integer customerId, String goodsName,
			BigDecimal promoteMoney) {
		DistributionIncome di = new DistributionIncome();
		Date d = new Date();
		di.setUserId(userId);// 介绍人,推广人
		di.setCustomerId(customerId);// 卖家
		di.setTypeId(131);
		di.setObjectName(goodsName);
		di.setDealTime(d);
		di.setMoneyIncome(promoteMoney);
		di.setMainStatus(1);
		di.setCreatedAt(d);
		distributionIncomeMapper.insert(di);
	}*/

	private void commonSendnotice(Integer userId, String title, String content) {
		BaseNotice notice = new BaseNotice();// 给用户发消息通知
		notice.setReceiverId(userId);
		// String title = "对不起,您当前的资金不足无法进行自动收货";
		// String content = "对不起,您当前的资金不足无法进行自动收货";
		notice.setTitle(title);
		String serial = CommonGenerator.getSerialByDate("n");
		notice.setSerial(serial);
		notice.setContent(content);
		notice.setReadStatus(1);
		notice.setCreatedAt(new Date());
		notice.setCreatedBy("-SYS-");
		noticeMapper.insert(notice);
	}
	
	private String  getLinkUrl(Integer goodsId){
		String linkUrl = null;
		BaseGoods goods = goodsMapper.selectByPrimaryKey(goodsId);
		if(null != goods && !"".equals(goods)){
			Integer moduleId = goods.getModuleId();
			Integer shopId = goods.getShopId();
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("id", shopId);
			if(moduleId != null){
				List<Wholesale> wholesalesList = wholesaleMapper.selectListByCondition(map);
				if(wholesalesList.size() > 0){
					Wholesale wholesales = wholesalesList.get(0);
					Integer wholesalesUserId = wholesales.getUserId();
					String userSerial = wholesales.getUserSerial();
					linkUrl = "/wsg/"+goodsId+"/"+shopId+"/"+wholesalesUserId+"?moduleId="+moduleId+"&r="+userSerial;
				}
			}else{
				List<BaseShop> shopList = shopMapper.selectListByCondition(map);
				if(shopList.size() > 0){
					BaseShop shop = shopList.get(0); 
					String userSerial = shop.getUserSerial();
					linkUrl = "/goods/"+goodsId+"?shopId="+shopId+"&r="+userSerial;
				}
			}
		}
		return linkUrl;
	}
	
	


}