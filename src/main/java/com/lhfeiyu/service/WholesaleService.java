package com.lhfeiyu.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.WholesaleMapper;
import com.lhfeiyu.dao.base.BaseGoodsMapper;
import com.lhfeiyu.po.Wholesale;
import com.lhfeiyu.po.base.BaseGoods;
import com.lhfeiyu.service.base.CommonService;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 业务层：管理员 <p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2015-7-4 10:08:21 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 1.0 <p>
 */
@Service
public class WholesaleService extends CommonService<Wholesale> {

	@Autowired
	WholesaleMapper mapper;
	@Autowired
	BaseGoodsMapper goodsMapper;

	public List<Wholesale> getWholesaleList(HashMap<String, Object> map) {
		List<Wholesale> wholesaleList = mapper.selectListByCondition(map);
		map.put("start", 0);
		map.put("count", 6);
		//map.put("orderBy", A.id);
		for(Wholesale w : wholesaleList){
			map.put("shopId", w.getId());
			//map.put("goodsIds", w.getGoodsId());
			List<BaseGoods> goodsList = goodsMapper.selectListByCondition(map);
			List<String> picPathList = new ArrayList<String>(6);
			for(BaseGoods g : goodsList){
				picPathList.add(g.getPicPath());
			}
			w.setPicPathList(picPathList);
		}
		return wholesaleList;
	}

}

