package com.lhfeiyu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.AntiqueCityMapper;
import com.lhfeiyu.po.AntiqueCity;
import com.lhfeiyu.service.base.CommonService;

@Service
public class AntiqueCityService extends CommonService<AntiqueCity>{
	@Autowired
	AntiqueCityMapper mapper;
}
