package com.lhfeiyu.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhfeiyu.dao.AuctionQuickGoodsMapper;
import com.lhfeiyu.po.AuctionQuickGoods;
import com.lhfeiyu.service.base.CommonService;

@Service
public class AuctionQuickGoodsService extends CommonService<AuctionQuickGoods>{
	@Autowired
	AuctionQuickGoodsMapper mappper;
	
	public int updateAutoOfferNULL(Map<String,Object> map){
		return mappper.updateAutoOfferNULL(map);
	}
	
	public String selectGoodsIdsOfQuick(Map<String,Object> map){
		return mappper.selectGoodsIdsOfQuick(map);
	}
	
}
