package com.lhfeiyu.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.lhfeiyu.config.Assets;
import com.lhfeiyu.dao.ChatQuickMapper;
import com.lhfeiyu.po.ChatQuick;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.CommonGenerator;
import com.lhfeiyu.tools.base.Result;

/**
 * <strong> 描&nbsp;&nbsp;&nbsp;&nbsp;述：</strong> 基础库-业务层：即时拍聊天记录 chatQuick<p>
 * <strong> 作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong> 虞荣华 <p>
 * <strong> 编写时间：</strong> 2016年4月12日14:30:35 <p>
 * <strong> 公&nbsp;&nbsp;&nbsp;&nbsp;司：</strong> 成都蓝海飞鱼科技有限公司 http://lhfeiyu.com <p>
 * <strong> 版&nbsp;&nbsp;&nbsp;&nbsp;本：</strong> 1.0 <p>
 * <strong> 备&nbsp;&nbsp;&nbsp;&nbsp;注：</strong> 包路径：com.lhfeiyu.service.ChatQuickService <p>
 */
@Service
public class ChatQuickService extends CommonService<ChatQuick> {

	@Autowired
	ChatQuickMapper chatQuickMapper;
	
	public JSONObject getChatQuickList(JSONObject json, Map<String, Object> map) {
		List<ChatQuick> chatQuickList = chatQuickMapper.selectListByCondition(map);
		Integer total = chatQuickMapper.selectCountByCondition(map);
		return Result.gridData(chatQuickList, total, json);
	}
	
	/**
	 * 新增或修改即时拍聊天记录
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatQuick 即时拍聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject addOrUpdateChatQuick(JSONObject json, ChatQuick chatQuick, String username){
		if(null == chatQuick.getId()){//添加
			return addChatQuick(json, chatQuick, username);
		}else{//修改
			return updateChatQuick(json, chatQuick, username);
		}
	}
	
	/**
	 * 新增即时拍聊天记录（代码若已经存在则提示失败）
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatQuick 即时拍聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject addChatQuick(JSONObject json, ChatQuick chatQuick, String username){
		Date date = new Date();
		chatQuick.setId(null);
		chatQuick.setSerial(CommonGenerator.getSerialByDate(Assets.serial_prefix_chatQuick));
		chatQuick.setMainStatus(1);
		chatQuick.setCreatedBy(username);
		chatQuick.setCreatedAt(date);
		chatQuickMapper.insert(chatQuick);
		json.put("id", chatQuick.getId());
		return Result.success(json);
	}
	
	/**
	 * 修改即时拍聊天记录（ID不能为空，数据库中必须存在该ID的数据）
	 * @param json 消息数据容器对象（主要用于保存提示消息或数据）
	 * @param chatQuick 即时拍聊天记录对象
	 * @param username 操作人名称（数据库记录）
	 * @return JSONObject
	 */
	public JSONObject updateChatQuick(JSONObject json, ChatQuick chatQuick, String username){
		Date date = new Date();
		Integer chatQuickId = chatQuick.getId();
		if(null == chatQuickId){//添加
			return Result.failure(json, "编号为空，无法进行执行修改", "chatQuickId_null");
		}
		ChatQuick dbChatQuick = chatQuickMapper.selectByPrimaryKey(chatQuickId);
		if(null == dbChatQuick){
			return Result.failure(json, "该条数据不存在，无法进行执行修改", "chatQuick_null");
		}

		chatQuick.setUpdatedBy(username);
		chatQuick.setUpdatedAt(date);
		chatQuickMapper.updateByPrimaryKeySelective(chatQuick);
		return Result.success(json);
	}
	
	
}

