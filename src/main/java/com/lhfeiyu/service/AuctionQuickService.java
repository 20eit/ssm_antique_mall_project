/*package com.lhfeiyu.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.alibaba.fastjson.JSONArray;
import com.lhfeiyu.config.Const;
import com.lhfeiyu.dao.AuctionQuickGoodsMapper;
import com.lhfeiyu.dao.AuctionQuickMapper;
import com.lhfeiyu.dao.GoodsMapper;
import com.lhfeiyu.po.AuctionQuick;
import com.lhfeiyu.po.AuctionQuickGoods;
import com.lhfeiyu.po.Wholesale;
import com.lhfeiyu.po.base.Goods;
import com.lhfeiyu.po.base.Shop;
import com.lhfeiyu.po.base.User;
import com.lhfeiyu.service.base.CommonService;
import com.lhfeiyu.tools.base.Check;
import com.lhfeiyu.tools.base.CommonGenerator;

@Service
public class AuctionQuickService extends CommonService<AuctionQuick> {
	@Autowired
	AuctionQuickMapper mappper;
	@Autowired
	AuctionQuickGoodsMapper aqgMappper;
	@Autowired
	GoodsMapper goodsMappper;

	public ModelMap getAuctionQuickGoodsNumByType(Map<String, Object> map, ModelMap modelMap) {
		map.put("auctionStatus1", 1);
		map.put("auctionTypeId", 121);
		modelMap.put("goodsTypeNum121", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 122);
		modelMap.put("goodsTypeNum122", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 123);
		modelMap.put("goodsTypeNum123", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 124);
		modelMap.put("goodsTypeNum124", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 125);
		modelMap.put("goodsTypeNum125", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 126);
		modelMap.put("goodsTypeNum126", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 127);
		modelMap.put("goodsTypeNum127", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 128);
		modelMap.put("goodsTypeNum128", aqgMappper.selectCountByCondition(map));
		map.put("auctionTypeId", 129);
		modelMap.put("goodsTypeNum129", aqgMappper.selectCountByCondition(map));
		return modelMap;
	}

	public String addAuctionQuick(User user, AuctionQuick aq, String auctionGoodsAry) {
		Date date = new Date();
		Integer userId = user.getId();
		String username = user.getUsername();
		String msg = null;
		// TODO 每种产品类型对应一个即时拍群组，页面上选择所属类型
		// TODO 测试 - 先写死类型
		Integer typeId = 129;
		String groupId = Const.auction_quick_group_129_id;// gg8008953812:即时拍-杂珍
		
		 * if(null == typeId){ typeId = 129; }
		 
		if (typeId == 121) {
			groupId = Const.auction_quick_group_121_id;
		} else if (typeId == 122) {
			groupId = Const.auction_quick_group_122_id;
		} else if (typeId == 123) {
			groupId = Const.auction_quick_group_123_id;
		} else if (typeId == 124) {
			groupId = Const.auction_quick_group_124_id;
		} else if (typeId == 125) {
			groupId = Const.auction_quick_group_125_id;
		} else if (typeId == 126) {
			groupId = Const.auction_quick_group_126_id;
		} else if (typeId == 127) {
			groupId = Const.auction_quick_group_127_id;
		} else if (typeId == 128) {
			groupId = Const.auction_quick_group_128_id;
		}

		aq.setChatGroupId(groupId);// 即时拍每种类型使用一个固定群组
		String serial = CommonGenerator.getSerialByDate("aq");
		aq.setAuctionSerial(serial);
		aq.setEndTime(null);
		aq.setHaveBegun(1);
		aq.setHaveNoticed(1);
		aq.setMainStatus(1);
		aq.setDeletedAt(null);
		aq.setDeletedBy(null);
		aq.setCreatedAt(date);
		aq.setCreatedBy(username);
		aq.setUserId(userId);
		aq.setShopId(user.getShopId());
		mappper.insert(aq);
		Integer auctionId = aq.getId();
		if (null == auctionId) {
			mappper.insert(aq);
			auctionId = aq.getId();
		}
		if (null == auctionId) {
			msg = "本场拍卖添加失败";
			return msg;
		}
		addAuctionGoodsList(null, auctionGoodsAry, user, auctionId);
		return null;
	}

	public void addAuctionGoodsList(List<AuctionQuickGoods> aqgList, String auctionQuickGoodsAry, User user,
			Integer auctionId) {
		if (null == aqgList) {
			aqgList = JSONArray.parseArray(auctionQuickGoodsAry, AuctionQuickGoods.class);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("auctionId", auctionId);
		String ids = aqgMappper.selectGoodsIdsOfQuick(map);
		if (null != ids && !"".equals(ids)) {
			map.clear();
			map.put("goodsIds", ids);
			map.put("mainStatus", 1);
			map.put("updatdAt", new Date());
			map.put("updatedBy", user.getUsername());
			goodsMappper.updateStatusByIds(map);
			String condition = "";
			// TODO TODO TODO 修改删除条件
			aqgMappper.deleteByCondition(condition);
		}
		aqgList = resetList(aqgList, user, auctionId);
		aqgMappper.insertBatch(aqgList);
	}

	private List<AuctionQuickGoods> resetList(List<AuctionQuickGoods> aqgList, User user, Integer auctionId) {
		List<AuctionQuickGoods> newAQGList = new ArrayList<AuctionQuickGoods>();
		String goodsIds = "";
		Date date = new Date();
		Integer userId = user.getId();
		String username = user.getUsername();
		int index = 1;
		String serial = "";
		for (AuctionQuickGoods aqg : aqgList) {
			Integer goodsId = aqg.getGoodsId();
			if (null == goodsId)
				continue;
			Goods dbGoods = goodsMappper.selectByPrimaryKey(goodsId);
			Integer status = dbGoods.getMainStatus();
			if ((null != status && status != 1 && status != 76)
					|| (null != dbGoods.getModuleId() && dbGoods.getModuleId() == 5))
				continue;// 只能添加非批发城中正在出售的藏品
			// int dbUserId = dbGoods.getUserId();
			// int toUserId = userId;
			// if(dbUserId != toUserId)continue;//该商品不属于当前用户，不预添加
			AuctionQuickGoods newAQG = new AuctionQuickGoods();
			BigDecimal pb = aqg.getPriceBegin();
			if (null == pb) {
				newAQG.setPriceBegin(new BigDecimal(0));
			} else {
				newAQG.setPriceBegin(aqg.getPriceBegin());
			}
			newAQG.setUserId(userId);
			newAQG.setAuctionId(auctionId);
			newAQG.setGoodsId(aqg.getGoodsId());
			newAQG.setMainStatus(1);
			newAQG.setCreatedAt(date);
			newAQG.setCreatedBy(username);
			if (index < 10) {
				serial = "00" + index;
			} else if (index < 100) {
				serial = "0" + index;
			}
			newAQG.setGoodsSerial(serial);
			newAQGList.add(newAQG);
			goodsIds += "," + goodsId;
			index++;
		}
		if (goodsIds.length() > 0) {
			goodsIds = goodsIds.substring(1);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("goodsIds", goodsIds);
			map.put("mainStatus", 73);// 即时拍拍卖中
			map.put("updatdAt", date);
			map.put("updatedBy", username);
			goodsMappper.updateStatusByIds(map);// 更新商品拍卖状态
		}
		return newAQGList;
	}

	// TODO 即时拍的相关方法
	public void updateAuctionQuickGoodsOneDone() {
		String professionIds = aqMapper.selectAlreadyBegin();
		// //System.out.println("AuctionJob_selectAlreadyBegin_professionIds"+professionIds);
		if (null != professionIds && professionIds.length() > 0) {
			String[] idAry = professionIds.split(",");
			Map<String, Object> map = new HashMap<String, Object>();
			// map.put("auctionType", 1);//1:专场
			for (String apId : idAry) {
				int professionId = Integer.parseInt(apId);
				map.put("auctionId", professionId);
				int updateCount = aqgMapper.updateStatusByRefresh(map);// 返回更新的拍品状态的条数，如果有值，则更新下一个拍品的refresh_time
				// //System.out.println("AuctionJob_professionId"+professionId+"
				// ,updateCount-quick: "+updateCount);
				if (updateCount > 0) {
					int haveFirst = aqgMapper.updateFirstRefresh(map);// 更新下一个拍品的refresh_time，若没有值，则拍品已经拍卖完毕，更新专场表的拍卖状态
					// //System.out.println("AuctionJob_professionId"+professionId+"
					// ,haveFirst-quick: "+haveFirst);
					if (haveFirst <= 0) {
						// aqMapper.updateAuctionOver(map);//拍卖结束 -
						// 即时拍没有推广，不生成推广收益,NoticeJob中有自动开启下一场并通知用户
						updateAuctionQuickOver(map, professionId);
					}
				}
			}
		}
	}

	private void updateAuctionQuickOver(Map<String, Object> map, int professionId) {
		Date date = new Date();
		aqMapper.updateAuctionOver(map);// 拍卖结束
		// TODO 将流拍的拍品的状态改为普通状态（非拍卖状态）
		map.clear();
		map.put("auctionId", professionId);
		String goodsIds = aqgMapper.selectGoodsIdsDoneButNotDeal(map);
		if (null != goodsIds && !"".equals(goodsIds)) {
			map.put("mainStatus", 1);// 普通状态
			map.put("updatedAt", date);
			map.put("updatedBy", "-SYS-");
			map.put("goodsIds", goodsIds);
			goodsMapper.updateStatusByIds(map);
		}
	}

	
	 * /TODO-tempHide-即时拍 private void checkQuickGoodsAddOrder(){//新增订单
	 * JSONObject json = new JSONObject(); List<AuctionQuickGoods> aqgList =
	 * aqgMapper.selectOrderSnNull(); if(null != aqgList && aqgList.size() > 0
	 * ){ Map<String,Object> map = new HashMap<String,Object>();
	 * for(AuctionQuickGoods ag : aqgList){ json.clear(); Integer managerId =
	 * ag.getUserId(); String username = ag.getUsername(); Integer goodsId =
	 * ag.getGoodsId();//商品id if(null == managerId)continue; BigDecimal priceBD
	 * = ag.getOfferPrice(); if(null == priceBD)continue; priceBD =
	 * priceBD.setScale(2, BigDecimal.ROUND_HALF_UP);//设置保留两位小数 double price =
	 * priceBD.doubleValue(); if(price<=0 || price>= 1000000)continue;
	 * 
	 * //先扣除扣手续费 - 先从流动资金中扣除，如果不足则从冻解资金中扣除 double money =
	 * sysDictService.getProfessionDealFeeScale(); if(money > 0){ BigDecimal
	 * feeBD = new BigDecimal(money); feeBD = feeBD.setScale(2,
	 * BigDecimal.ROUND_HALF_UP);//设置保留两位小数 json = cfService.outcomeMoney("",
	 * feeBD, managerId, username, 155, null, "平台", json,
	 * true,getLinkUrl(goodsId)); if(json.containsKey("error_desc")){
	 * cfService.outcomeFrozenMoney("", feeBD, managerId, username, 155, null,
	 * "平台", json, true,getLinkUrl(goodsId));
	 * if(json.containsKey("error_desc")){ continue; } }
	 * sysDictService.incomeMoney(feeBD, 4, json); }
	 * 
	 * Integer userId = ag.getOfferUserId(); Date date = new Date(); OrderInfo
	 * order = new OrderInfo(); String serial =
	 * CommonGenerator.getSerialByDate("od"); order.setOrderSn(serial);
	 * order.setUserId(userId);
	 * order.setOrderStatus(1);//订单的状态;1未完成,2已完成,3已取消,4无效,5退货
	 * order.setShippingStatus(1);//商品配送情况;1未发货,2已发货,3已收货,4退货
	 * order.setPayStatus(1);//支付状态;1未付款;2付款中;3已付款
	 * order.setUsername(ag.getOfferUsername());
	 * order.setProvince(ag.getProvince()); order.setCity(ag.getCity());
	 * order.setAddress(ag.getAddressDetail()); order.setPhone(ag.getPhone());
	 * order.setEmail(ag.getEmail()); order.setCreatedAt(date);
	 * order.setCreatedBy("-SYS-"); order.setMainStatus(1);
	 * orderInfoMapper.insert(order);//新增订单 Integer orderId = order.getId();
	 * 
	 * 
	 * OrderGoods og = new OrderGoods();//新增orderGoods og.setOrderId(orderId);
	 * //og.setGoodsId(ag.getGoodsId()); og.setGoodsId(goodsId);
	 * og.setGoodsName(ag.getGoodsName()); og.setGoodsSn(ag.getGoodsSn());
	 * og.setGoodsType(1);//商品类型（1普通商品，2批发城商品）
	 * og.setGoodsFrom(5);//商品来源（1批发城，2店铺，3专场，4微拍，5即时拍） og.setGoodsNumber(1);
	 * og.setShopPrice(priceBD); og.setMainStatus(1); og.setCreatedAt(date);
	 * og.setCreatedBy("-SYS-"); orderGoodsMapper.insertSelective(og);
	 * 
	 * Goods goods = new Goods();//更新goods状态 //goods.setId(ag.getGoodsId());
	 * goods.setId(goodsId); goods.setMainStatus(78);//dict78:冻结中
	 * goods.setUpdatedAt(date); goods.setUpdatedBy("-SYS-");
	 * goodsMapper.updateByPrimaryKeySelective(goods);
	 * 
	 * Goods db_goods = goodsMapper.selectByPrimaryKey(goodsId); //double price
	 * = ag.getOfferPrice().doubleValue(); String title = "竞拍成功通知"; String
	 * content = "【"+db_goods.getGoodsName()+"】恭喜您竞拍成功（价格："+price+
	 * "元），系统已经为您生成订单，可以到个人中心查看订单并付款"; Dict dict =
	 * dictMapper.selectByPrimaryKey(20);//竞拍成功通知 if(null != dict){ content =
	 * dict.getDictValue(); title = dict.getDictName(); } map.clear();
	 * map.put("id", ag.getId()); map.put("orderSn", serial);
	 * aqgMapper.updateOrderSn(map);
	 * 
	 * Notice notice = new Notice();
	 * notice.setSerial(CommonGenerator.getSerialByDate("n"));
	 * notice.setReceiverId(userId); notice.setContent(content);
	 * notice.setReadStatus(1);//未阅读 notice.setTypeId(20);//竞拍成功通知
	 * notice.setTitle(title); notice.setCreatedAt(date);
	 * notice.setCreatedBy("-SYS-"); noticeMapper.insert(notice);//新增通知
	 * 
	 * String openId = ag.getUserOpenId(); //微信消息通知 if(Check.isNotNull(openId)){
	 * String firstValue = "恭喜！您已竞拍成功。"; String attr1Value =
	 * goods.getGoodsName(); String attr2Value = String.valueOf(price)+"元";
	 * String remarkValue = "系统已经为您生成订单，可以到个人中心查看订单并付款。"; JSONObject messageJson
	 * = Message.buildMsg(openId, ConstField.wx_moban_4,
	 * ConstField.page_user_center, "first", firstValue, "keyword1", attr1Value,
	 * "keyword2", attr2Value, null, null, null, null, "remark", remarkValue);
	 * Message.sendMessage(ConstField.wx_moban_4, messageJson);//JSONObject
	 * resultJson = Message.sendMessage(ConstField.wx_moban_10, messageJson); }
	 * 
	 * } } }
	 

	private String getLinkUrl(Integer goodsId) {
		String linkUrl = null;
		Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
		if (null != goods && !"".equals(goods)) {
			Integer moduleId = goods.getModuleId();
			Integer shopId = goods.getShopId();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", shopId);
			if (moduleId != null) {
				List<Wholesale> wholesalesList = wholesaleMapper.selectListByCondition(map);
				if (wholesalesList.size() > 0) {
					Wholesale wholesales = wholesalesList.get(0);
					Integer wholesalesUserId = wholesales.getUserId();
					String userSerial = wholesales.getUserSerial();
					linkUrl = "/wsg/" + goodsId + "/" + shopId + "/" + wholesalesUserId + "?moduleId=" + moduleId
							+ "&r=" + userSerial;
				}
			} else {
				List<Shop> shopList = shopMapper.selectListByCondition(map);
				if (shopList.size() > 0) {
					Shop shop = shopList.get(0);
					String userSerial = shop.getUserSerial();
					linkUrl = "/goods/" + goodsId + "?shopId=" + shopId + "&r=" + userSerial;
				}
			}
		}
		return linkUrl;
	}

	public void withholdFee(int typeId, double money) {

	}

	public void updateCheckAuctionQuickBegin() {
		// 23点- 10点可发布但是不进入拍卖,每个类型一个拍卖群组,按群组检查是否开启下一场拍卖
		Calendar now = Calendar.getInstance();
		int hour = now.get(Calendar.HOUR_OF_DAY);
		if (hour == 23 || hour < 10)
			return;

		String auctionTypeIds = aqMapper.selectTypeIdsAlreadyBegin();
		if (null == auctionTypeIds)
			auctionTypeIds = "";
		Map<String, Object> map = new HashMap<String, Object>();
		beginAuctionQuick(map, auctionTypeIds, "121", 121);
		beginAuctionQuick(map, auctionTypeIds, "122", 122);
		beginAuctionQuick(map, auctionTypeIds, "123", 123);
		beginAuctionQuick(map, auctionTypeIds, "124", 124);
		beginAuctionQuick(map, auctionTypeIds, "125", 125);
		beginAuctionQuick(map, auctionTypeIds, "126", 126);
		beginAuctionQuick(map, auctionTypeIds, "127", 127);
		beginAuctionQuick(map, auctionTypeIds, "128", 128);
		beginAuctionQuick(map, auctionTypeIds, "129", 129);

	}

	private void beginAuctionQuick(Map<String, Object> map, String auctionTypeIds, String typeId,
			Integer typeIdInteger) {
		if (!auctionTypeIds.contains(typeId)) {
			map.put("typeId", typeIdInteger);
			AuctionQuick aq = aqMapper.selectPrepareBegin(map);
			if (null != aq) {
				Integer id = aq.getId();
				updateCheckAuctionQuickNotice(map, aq);// 通知
				map.clear();
				map.put("haveBegun", 2);// haveBegun:1未开始，2已开始，3已结束
				map.put("ids", id);
				aqMapper.updateHaveBegun(map);// 拍卖结束后自动开始下一场拍卖
				map.clear();
				map.put("auctionId", id);
				aqgMapper.updateFirstRefresh(map);// 更新该次拍卖的第一个拍品的刷新时间
			}
		}
	}

	public void updateCheckAuctionQuickNotice(Map<String, Object> map, AuctionQuick aq) {
		Integer id = aq.getId();
		Integer isNoticed = aq.getHaveNoticed();
		if (null == isNoticed || isNoticed != 2) {// 已经通知了的不再通知
			String auctionName = aq.getAuctionName();
			auctionName = "即时拍-" + auctionName;
			//System.out.println(auctionName);
			int count = aqnMapper.selectUsersCountByAuctionQuickNotice(map);
			sendAuctionQuickNoticMsgByCount(map, count, auctionName);// 暂时不发短信
			// break;//只发送一场通知
		}

		if (Check.isNotNull(id)) {
			map.clear();
			map.put("ids", id);
			aqMapper.updateHaveNoticed(map);// 将拍场更新为已通知状态
			//System.out.println("NoticeJob_updateHaveNoticed_ids" + id);
		}
	}

	public void sendAuctionQuickNoticMsgByCount(Map<String, Object> map, int count, String auctionName) {
		String phones = null;
		String[] params = new String[] { auctionName };// 通知消息只有一个参数：拍场名称
		// phones = "18702827609";
		String mobanId = Const.rl_ytx_msg_moban_notice_id;
		// params = new String[] { "123456", "30" };
		//System.out.println("sendAuctionQuickNoticMsgByCount:count:" + count);
		if (count <= 50) {
			phones = aqnMapper.selectUsersPhonesByAuctionQuickNotice(map);
			// TODO 发送短信
			// YTX_MSG.send(phones, mobanId, params);
			//System.out.println("发送短信：phones-" + phones + ",mobanId-" + mobanId + ",params-" + params.toString());
		} else {
			int length = count / 50;
			map.put("count", 50);
			for (int i = 0; i <= length; i++) {
				map.put("start", 50 * i);
				phones = aqnMapper.selectUsersPhonesByAuctionQuickNotice(map);
				// TODO 发送短信
				// YTX_MSG.send(phones, mobanId, params);
				//System.out.println("发送短信：phones-" + phones + ",mobanId-" + mobanId + ",params-" + params.toString());
			}
		}
		//System.out.println("sendAuctionQuickNoticMsgByCount:phones:" + phones);
	}

	
}
*/