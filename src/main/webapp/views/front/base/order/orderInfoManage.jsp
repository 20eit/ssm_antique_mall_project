<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
<style>
.weui_dialog {
	z-index: 9999 !important;
}
h1 {
	margin: 0 !important;
}
.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}

.scrollbar_ul li {
	float: left;
	width: 33%; /**TODO图片尺寸自适应*/
	position: relative;
}

pre {
	display: block;
	padding: 0;
	margin: 0;
	font-size: 13px;
	line-height: 1.42857143;
	color: #333;
	word-break: break-all;
	word-wrap: break-word;
	background-color: white;
	border: none;
	border-radius: 4px;
}
#data-container .btn {
	margin: 0px 3px;
}
.fl{float:left;}
.fr{float:right;}
    
</style>
</head>
<body style="background-color: #f5f5f5;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-7 pt10 plr0 text-center">
				<span class="fs16">订单管理</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 plr0 jewel_title">
				<ul id="navUl">
					<li id="status_all" onclick="getOrderInfoList('all');" class="col5 active">全部</li>
					<li id="status_waitPayMoney" onclick="getOrderInfoList('waitPayMoney');" class="col5">待付款</li>
					<li id="status_shipping" onclick="getOrderInfoList('shipping');" class="col5">待发货</li>
					<li id="status_shipped" onclick="getOrderInfoList('shipped');" class="col5">已发货</li>
					<li class="col5">
						<div class="dropdown orderForm">
							<button class="btn btn-default dropdown-toggle bdn fs16 ptb0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="icon-reorder"></i>
							</button>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
								<!-- <li><a href="javascript:"> <img width="20" src="/images/front/order_form_01.png" class="dis-lin" /> 待晒好物</a></li> -->
								<li id="status_returnGoods" onclick="getOrderInfoList('returnGoods');">
									<a href="javascript:"> <img width="20" src="/images/front/order_form_02.png" class="dis-lin" /> 退款中</a>
								</li>
								<li id="status_done" onclick="getOrderInfoList('done');">
									<a href="javascript:"> <img width="20" src="/images/front/order_form_03.png" class="dis-lin" /> 已完成</a>
								</li>
								<li id="status_shutdown" onclick="getOrderInfoList('shutdown');">
									<a href="javascript:"> <img width="20" src="/images/front/order_form_04.png" class="dis-lin" /> 已关闭</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
		
		<div id="data-container">
		
		
		</div>
		
		<!-- <div class="row mt6">
			<div class="col-xs-12 white_bg ptb10">
				<div class="col-xs-6 plr0">
					<img width="30" src="/images/front/my_pic_02.png" class="dis-lin" /> 开心每一天
				</div>
				<div class="col-xs-6 plr0 text-right orange pt5">等待买家付款</div>
			</div>
			<div class="col-xs-12 ptb10">
				<div class="col-xs-3 plr0">
					<img src="/images/front/goods_02.png" class="img-responsive" />
				</div>
				<div class="col-xs-7 plr7 gray">特泊儿 和田玉白玉观音吊坠 高 端玉挂件男款 带证书 Y3041...</div>
				<div class="col-xs-2 plr0 fs16 fw">￥19.00</div>
			</div>
			<div class="col-xs-12 white_bg">
				<div class="col-xs-12 plr0 ptb10 text-right bdb">
					共一件商品&nbsp;&nbsp;总金额:(含运费10元) <span class="fs16 fw orange">￥29.00</span>
				</div>
			</div>
			<div class="col-xs-12 white_bg ptb5 text-right">
				<button type="button" class="btn btn-sm btn-default">联系买家</button>
				<button type="button" class="btn btn-sm btn-default">一键改价</button>
				<button type="button" class="btn btn-sm btn-default">取消订单</button>
				<button type="button" class="btn btn-sm btn-default">催付款</button>
			</div>
		</div>
		<div class="row mt6">
			<div class="col-xs-12 white_bg ptb10">
				<div class="col-xs-6 plr0">
					<img width="30" src="/images/front/my_pic_02.png" class="dis-lin" /> 开心每一天
				</div>
				<div class="col-xs-6 plr0 text-right orange pt5">买家已付款</div>
			</div>
			<div class="col-xs-12 ptb10">
				<div class="col-xs-3 plr0">
					<img src="/images/front/goods_02.png" class="img-responsive" />
				</div>
				<div class="col-xs-7 plr7 gray">特泊儿 和田玉白玉观音吊坠 高 端玉挂件男款 带证书 Y3041...</div>
				<div class="col-xs-2 plr0 fs16 fw">￥19.00</div>
			</div>
			<div class="col-xs-12 white_bg">
				<div class="col-xs-12 plr0 ptb10 text-right bdb">
					共一件商品&nbsp;&nbsp;总金额:(含运费10元) <span class="fs16 fw orange">￥29.00</span>
				</div>
			</div>
			<div class="col-xs-12 white_bg ptb5 text-right">
				<button type="button" class="btn btn-sm btn-default">联系买家</button>
				<button type="button" class="btn btn-sm btn-default">同意退货</button>
				<button type="button" class="btn btn-sm btn-default">查看物流</button>
				<button type="button" class="btn btn-sm btn-default">发货</button>
			</div>
		</div> -->
	</div>

	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>

	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/third-party/jquery-weui1/js/jquery-weui.js"></script>
	<script type="text/javascript" src="/third-party/jquery-weui1/js/swiper.js" charset='utf-8'></script>
	<script type="text/javascript" src="/js/front/wpk/myseller.js" title="v"></script>
	<script type="text/javascript" src="/js/front/order/orderInfoManage.js" title="v"></script>
	<!-- -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="row mt6">
			<div class="col-xs-12 white_bg ptb10">
				<div class="col-xs-6 plr0"><!-- @@_OSS_IMG_@@ -->
					<img width="30" src="{{buyerUserAvatar}}@40w_40h_4e_240-240-246bgc" class="dis-lin" /> {{buyerUsername}}
				</div>
				<div class="col-xs-6 plr0 text-right orange pt5">{{getStatusName}}</div>
			</div>
			<div class="col-xs-12 ptb10">
				<div class="col-xs-3 plr0"><!-- @@_OSS_IMG_@@ -->
					<img src="{{picPath}}@140w_140h_4e_240-240-246bgc" class="img-responsive" />
				</div>
				<div class="col-xs-7 plr7 gray">{{orderGoodsName}}</div>
				<div class="col-xs-2 plr0 fs16 fw">￥{{shopPrice}}</div>
			</div>
			<div class="col-xs-12 white_bg">
				<div class="col-xs-12 plr0 ptb10 text-right bdb">
					共{{goodsNumber}}件商品&nbsp;&nbsp;总金额:(含运费{{postageFee}}元) <span class="fs16 fw orange">￥{{totalMoney}}</span>
				</div>
			</div>
			<div class="col-xs-12 white_bg ptb5 text-right">
				<!--<button type="button" class="btn btn-sm btn-default">一键改价</button>-->
				<!--<button type="button" class="btn btn-sm btn-default">催付款</button>-->
				<!--<button onclick="lh.jumpR('/chat/{{buyerUserSerial}}');" type="button" class="btn btn-sm btn-default">联系买家</button>
					<button onclick="cancelOrder({{orderId}});" type="button" class="btn btn-sm btn-default">取消订单</button>
					<button onclick="sendGoods({{orderId}});" type="button" class="btn btn-sm btn-default">发货</button>
					<button onclick="agreeReturnGoods({{orderId}});" type="button" class="btn btn-sm btn-default">同意退货</button>
					<button onclick="disagreeReturnGoods({{orderId}});" type="button" class="btn btn-sm btn-default">拒绝退货</button>
					<button onclick="cancelReturnGoods({{orderId}});" type="button" class="btn btn-sm btn-default">取消申请退货</button>
				-->
				<!--
					<button onclick="lh.jumpR('/chat/{{sellerUserSerial}}');" type="button" class="btn btn-sm btn-default">联系卖家</button>
					<button onclick="cancelOrder({{orderId}});" type="button" class="btn btn-sm btn-default">取消订单</button>
					<button onclick="receiveGoods({{orderId}});" type="button" class="btn btn-sm btn-default">确认收货</button>
					<button onclick="applyReturnGoods({{orderId}});" type="button" class="btn btn-sm btn-default">申请退货</button>
					-->
					{{&getButtons}}
			</div>
		</div>	
	{{/rows}}
	</script>
</body>
</html>

