<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
</head>
<body class="bg_gray">
	<div class="container-fluid" style="    background-color: white;">
			<div class="row auction_title">
				<div class="col-xs-2" onclick="lh.back();">
					<i class="icon-angle-left icon-3x"></i>
				</div>
				<div class="col-xs-8 pt10 plr0 text-center">
					<span class="fs16">商品发货</span>
				</div>
			</div>
			<div class="weui_cell">
                <div class="weui_cell_hd"><label class="myLabel">快递公司：</label></div>
                <div class="weui_cell_bd weui_cell_primary">
                    <select id="express">
						<c:forEach var="e" items="${expressList}">
							<option value="${e.id}">${e.briefName}</option>
						</c:forEach>
					</select>
                </div>
            </div>
			<div class="weui_cell">
                <div class="weui_cell_hd"><label class="myLabel">快递单号：</label></div>
                <div class="weui_cell_bd weui_cell_primary">
                    <input class="weui_input" type="text" id="expressOrder" value="${orderInfo.expressOrder}" placeholder="(必填)">
                </div>
            </div>
            <div class="weui_cell">
                <div class="weui_cell_hd"><label class="myLabel">收货人：</label></div>
                <div class="weui_cell_bd weui_cell_primary">
                    <input class="weui_input" type="text" id="username" value="${orderInfo.username}" placeholder="(必填)">
                </div>
            </div>
            <div class="weui_cell">
                <div class="weui_cell_hd"><label class="myLabel">联系电话：</label></div>
                <div class="weui_cell_bd weui_cell_primary">
                    <input class="weui_input" type="text" id="phone" value="${orderInfo.phone}" placeholder="(必填)">
                </div>
            </div>
            <div class="weui_cell">
                <div class="weui_cell_hd"><label class="myLabel">收货人详细地址：</label></div>
                <div class="weui_cell_bd weui_cell_primary">
                    <input class="weui_input" type="text" id="address" value="${orderInfo.address}" placeholder="(必填)">
                </div>
            </div>
			
	  		<%-- <div style="font-size:14px;margin:5px 0px;">
				<nobr>快递单号:<br/><input type="text" id="goodsAttr" style="width:95%;"/></nobr>
			</div>
	  		<div style="font-size:14px;margin:5px 0px;">
				<nobr>收货人:<br/><input type="text" value="${orderInfo.username}" readonly="readonly" style="width:95%;"/></nobr>
			</div>
	  		<div style="font-size:14px;margin:5px 0px;">
				<nobr>联系电话:<br/><input type="text" value="${orderInfo.phone}" readonly="readonly" style="width:95%;"/></nobr>
			</div>
	  		<div style="font-size:14px;margin:5px 0px;">
				<nobr>收货人详细地址:<br/><input type="text" value="${orderInfo.address}" readonly="readonly" style="width:95%;"/></nobr>
			</div> --%>
			
			<div class="row ptb20" style="position: absolute;bottom: 5px;width: 100%;">
				<div class="col-xs-12">
					<a href="javascript:;" onclick="sendGoods(${orderGoodsId});return false;" class="weui_btn weui_btn_primary">确认发货</a>
				</div>
			</div>
	  </div>		
	  <input type="hidden" id="orderGoodsId" value="${orderGoodsId}">
	  <input type="hidden" id="orderId" value="${orderId}">
	  <input type="hidden" value="${r}" id="r"/> 	
	  <input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script>lh.param = ${paramJson}</script>
	<script type="text/javascript" src="/js/front/order/goodsSend.js" title="v"></script>
	
</body>
</html>
