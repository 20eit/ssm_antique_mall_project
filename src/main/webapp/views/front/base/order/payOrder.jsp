<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
</head>
<body class="bg_gray">
	<div class="container-fluid">
			<div class="row auction_title">
				<div class="col-xs-2" onclick="lh.back();">
					<i class="icon-angle-left icon-3x"></i>
				</div>
				<div class="col-xs-8 pt10 plr0 text-center">
					<span class="fs16">选择收货地址</span>
				</div>
			</div>
	 		<div id="receiveAddressList">
			</div>
			<div class="row ptb20" style="position: absolute;bottom: 5px;width: 100%;">
				<div class="col-xs-12">
					<a href="javascript:;" onclick="updateOrderAddressAndJumpToPay();return false;" class="weui_btn weui_btn_primary">安全支付</a>
				</div>
			</div>
	</div>
	
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script>lh.param = ${paramJson}</script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/order/payOrder.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<div class="row mt10 addressItems" id="check_{{id}}" style="background-color: #fff;border-radius:4px;" onClick="checkedAddress({{id}});">
				<div" class="col-xs-12">
					<div class="col-xs-12 plr0 ptb10 bdb pos-r">
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-12 plr0 pt10">收货人：{{receiverName}}</div>
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-12 plr0 pt10">地址：{{provinceName}}省&nbsp;{{cityName}}市</div>
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-12 plr0 pt10">详细地址：{{addressDetail}}</div>
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-12 plr0 pt10">联系电话：{{phone}}</div>
						</div>
					</div>
				</div>
			</div>
		{{/rows}}		 
	</script>
</body>
</html>
