<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
</head>
<body style="background-color: #f0f0f6;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">查看物流</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 plr7 white_bg">
				<div class="col-xs-12 plr7">
					<div class="col-xs-12 plr0 ptb15">
						<div class="col-xs-3 plr0"><!-- @@_OSS_IMG_@@ -->
							<img width="73" class="img-responsive" src="${orderGoods.picPath}@100w_100h_4e_240-240-246bgc" />
						</div>
						<div class="col-xs-9 pr0">
							<div class="col-xs-12 plr0 fs16">
								物流状态&nbsp;<span id="express_statusName" class="green">${orderGoods.expressStateName}</span>
							</div>
							<div class="col-xs-12 plr0 fs12 gray-init" style="font-size: 14px;">
								承运来源：<span id="express_company">${orderGoods.expressName}</span>
							</div>
							<div class="col-xs-12 plr0 fs12 gray-init" style="font-size: 14px;">
								运单编号：<span id="express_orderSn">${orderGoods.expressOrder}</span>
							</div>
							<!-- <div class="col-xs-12 plr0 fs12 gray-init">
								官方电话：<a href="tel:4009-566-565">4009-566-565</a>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt6 white_bg">
			<div class="col-xs-12 pb20">
				<div class="col-xs-12 plr0 ptb15 bdb">物流跟踪</div>
			</div>
			<div class="col-xs-12 mb10">
				<section id="cd-timeline" class="cd-container">
					<div id="data-container">

					</div>
					<!-- 
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
							<img width="20" src="/images/front/flag_01.png" class="center-block">
						</div>
						<div class="cd-timeline-content green">
							<p>【天津市】快件已签收，感谢您使用中通快递！感谢使用中通快递，期待再次为您服务</p>
							<span class="cd-date">2016-05-14 15:57:49</span>
						</div>
					</div>
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-movie">
							<img width="15" src="/images/front/flag_02.png" class="center-block">
						</div>
						<div class="cd-timeline-content">
							<p>【天津市】快件已经到达 天津西青分部 拷贝</p>
							<span class="cd-date">2016-05-14 07:57:49</span>
						</div>
					</div>
				    -->
				</section>
			</div>
		</div>
	</div>
	<input type="hidden" id="orderGoodsId" value="${orderGoodsId}">
	<input type="hidden" id="orderId" value="${orderId}">
	<input type="hidden" value="${r}" id="r" />
	<input type="hidden" value="${loginStatus}" id="loginStatus" />
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script>lh.param = ${paramJson}</script>
	<script type="text/javascript" src="/js/front/order/expressJsonScan.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-movie">
				<img width="15" src="/images/front/flag_02.png" class="center-block">
			</div>
			<div class="cd-timeline-content">
				<p>{{context}}</p>
				<span class="cd-date">{{ftime}}</span>
			</div>
		</div>
	{{/rows}}
	</script>

</body>
</html>
