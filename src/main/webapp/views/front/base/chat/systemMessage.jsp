<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>

<!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
<!-- <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
<script src="/js/front/user/myseller.js" title="v"></script>
<script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
<style>
.h90 {
	height: 90px;
}

.round_red {
	width: 15px;
	height: 15px;
	border-radius: 50%;
	background-color: #f00;
	color: #fff;
	float: right;
	text-align: center;
}

.message_icon {
	position: absolute;
	top: -3px;
	right: -3px;
}

.auction_title {
	position: relative;
}
</style>
</head>
<body style="background-color: #f0f0f6;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"
					style="position: relative; bottom: 2px;"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">系统消息</span>
			</div>
			<div class="col-xs-3 ptb10 pl0 text-right"></div>
		</div>
		<div class="row white_bg">
			<div class="col-xs-12 pl25">

				<div class="col-xs-12 plr0 gray ptb10 bdb"
					onclick="lh.jumpR('/fans');">
					<div class="col-xs-2 plr0">
						<img width="45" src="/images/front/message_02.png"
							class="center-block" />
					</div>
					<div class="col-xs-5 plr7 pt10 fs18">师友管理</div>
					<div class="col-xs-5 plr7 fs28 text-right">
						<i class="icon-angle-right"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row white_bg mt6">
			<!-- <div class="col-xs-12 pl25">
				<div class="col-xs-12 plr0 gray ptb10 bdb">
					 <div class="col-xs-2 plr0">
						<img width="45" src="/images/front/message_03.png" class="center-block" />
						<i class="red icon-circle message_icon"></i>
					</div> 
					<div class="col-xs-10 pl7 pr0">
						<div class="fs18">
							<span>微拍客消息</span>
							<span class="fs12 gray-init pull-right">11:02</span>
						</div>
						<div class="fs12 gray-init">
							<span>您好，您需要咨询什么业务</span>
							<span class="round_red">1</span>
						</div>
					</div> 
				</div>  -->
			<div id="data-container"></div>

			<!--  <div class="col-xs-12 plr0 gray ptb10 bdb">
					<div class="col-xs-2 plr0">
						<img width="45" src="/images/front/message_04.png" class="center-block" />
					</div>
					<div class="col-xs-10 pl7 pr0">
						<div class="fs18">
							<span>Martian</span>
							<span class="fs12 gray-init pull-right">昨天</span>
						</div>
						<div class="fs12 gray-init">
							<span>在么？</span>
						</div>
					</div>
				</div>  -->

		</div>
	</div>
	</div>

	

	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript"
		src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript"
		src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript" src="/js/front/chat/systemMessage.js"
		title="v"></script>

	<script id="template" type="x-tmpl-mustache">
		{{#rows}}	 	
			<div class="col-xs-12 plr0 gray ptb10 bdb" onclick="lh.jumpR('/message/messageDetail/{{id}}');">
					<div class="col-xs-2 plr0">
						<img width="45" src="/images/front/message_01.png" class="center-block" />
						{{&isRead}}
					</div>
					<div class="col-xs-10 pl7 pr0">
						<div class="fs18">
							<span>{{title}}</span>
							<span class="fs12 gray-init pull-right">{{&date}}</span>
						</div>
						<div class="fs12 gray-init">
							<span>{{content}}</span>
							{{&isReads}}
						</div>
					</div>
				</div>
		{{/rows}}
	</script>

</body>
</html>