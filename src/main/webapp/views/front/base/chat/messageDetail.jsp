﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场添加须知条款- AuctionProfessionAction:/ap/page/articleAgreement --%>
</head>
<body>
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-1">
				<i class="icon-angle-left icon-3x" onclick="lh.back();"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<span class="fs16">${notice.title}</span>
			</div>
		</div>
		<div class="row pt10">
			<div class="col-xs-12 article" style="line-height: 25px;">
				${notice.content}
			</div>
		</div>
		<div style="height: 75px;"></div>
		<div class="readButton">
			<div onclick="lh.jumpR('/ap/page/typeChoose');" class="col-xs-12">
				<a role="button" class="btn btn-orange col-xs-12" onclick="readMessage(${notice.id});">我已阅读</a>
			</div>
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/js/front/chat/messageDetail.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		
	{{/rows}}
	</script>
</body>
</html>
