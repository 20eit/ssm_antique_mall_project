<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>
<!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
<!-- <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
<script src="/js/front/user/myseller.js" title="v"></script>
<script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
<style>
.auction_title {
	position: relative;
}
</style>
</head>
<body style="background-color: #f5f5f5;">
	<input type="hidden" value="${wxMsgCount.id}" id="wxMsgCountId">
	<input type="hidden" value="${userId}" id="userId">
	 <div class="row auction_title">
            <div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x" style="margin-left:10px; relative;bottom: 2px;"></i>
			</div>
            <div class="col-xs-6 pt10 plr0 text-center">
                <span class="fs16">消息群发</span>
            </div>
           
        </div>
	<div class="pz_main">
		<div class="c_0100_14">
			<div class="slide_teacher" id="slide_teacher">
				<div class="bd">
					<div class="ul_out_box" id="ul_out_box1">
						<div class="inbd" style="border-bottom: #d9d7d7 solid 1px;">
							<div id="noticeSwith" class="fr" style="text-align: center; margin-top: 25px; padding-bottom: 5px;">
								<button type="button" class="weui_btn weui_btn_mini weui_btn_primary pointer" style="margin-right: 3px;" onclick="allMsg()">群发消息</button>
							</div>
							<!-- <div class="inbd_ul" id="goodsList">
								
							</div> -->
						</div>
						<div class="weui_article" style="border-bottom: #d9d7d7 solid 1px; padding: 10px">
							<span style="font-weight: 700;">今日剩余群发消息:</span>
							<h3 style="display: inline-block; color: #FF2400">
								<c:if test="${!empty wxMsgCount}">${wxMsgCount.maxCount -1}</c:if>
								<c:if test="${empty wxMsgCount}">3</c:if>
							</h3>
							<div>多品消息一次能发9个拍品(消息样式:单个/多个)</div>
						</div>
					</div>
					<div class="mainbox" id="iOther">
						<div class="mainsmall" id="auctionOnlineMain">
							<div id="main" role="main" style="border-bottom: 1px solid #CDCDCD; ">
								<div style="padding: 0 5px;">
									<ul id="goodsList" class="mainul mainul1" style="width: 100%">
										<%-- <c:forEach items="${goodsList}" var="goods" step="2">
										<li onclick="location.href='/professionGoodsDetail?goodsId=${goods.id}&auctionId=1'" class="mainulli goods_bg"><span
											style="background: url(${goods.picPath}) center center no-repeat #F2F2F2; background-position: 50% 50%; background-size: contain;"></span>
											<h3 style="color: #838381;">编号.${goods.goodsSn}</h3></li>
									</c:forEach> --%>
									</ul>
								</div>
								<div style="clear: both;"></div>
							</div>
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>

	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>

	<input type="hidden" value="${r}" id="r" />
	<input type="hidden" value="${loginStatus}" id="loginStatus" />



	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	 <%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<!-- <script src="/js/front/user/aution/myAuction.js" title="v"></script> -->
	<script type="text/javascript" src="/js/front/chat/wxMsgCount.js" title="v"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<li style="width:49%;float:left;margin: 2px;background:#fff;" 
				class="mainulli goods_bg pointer" onclick="checkedGoods({{id}});">
				<input type="checkbox" class="goodsChecked" onclick="checkedGoods({{id}});" id="goodsId_{{id}}" value="{{id}}" />
				<div class="col-xs-12 product plr0">
                <div class="col-xs-12 productMs">
                    <div class="col-xs-12 ptb3 gray text-center">
                        <div class="col-xs-12 plr0 bdb">
                            <span>结束:</span>
                            <span>{{&date}}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 plr0 ptb5 bdb">
                        <div class="col-xs-4">
                            <img class="img-responsive center-block" width="77" src="{{picPath}}@80w_80h_4e_240-240-246bgc_50Q" />
                        </div>
                        <div class="col-xs-5 plr0">
                            <div class="gray_deep fs18 elli">{{goodsName}}</div>
                            <div class="col-xs-12 plr0 pt5">
								<input type="hidden" value="{{offerPrice}}" id="offerPrice_{{id}}"/>
                                <span class="gray pull-left fs14">￥{{offerPrice}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</li>
		{{/rows}}		 
	</script>

</body>
</html>
