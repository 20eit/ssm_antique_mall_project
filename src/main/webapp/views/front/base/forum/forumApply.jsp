<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />


<style>
.scrollbar_ul li {
	float: left;
	width: 110px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
</style>
<%--from by ForumAction. ForumSquare--%>
</head>
<body style="background-color: #f0f0f6;">
	<input type="hidden" id="userId" value="${user.id}">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3">
				<i class="icon-angle-left icon-3x" onclick="lh.back();"></i>
			</div>
			<div class="col-xs-7 pt10 plr0 text-center">
				<span class="fs16">圈子申请</span>
			</div>
		</div>
		<div class="row">
			<div class="white_bg col-xs-12 bdb ptb5">
				<div class="col-xs-12 plr0">
					<input class="form-control bdn" id="attr1" type="text" value="" placeholder="圈子名称" />
				</div>
				<div class="col-xs-12 plr0">
					<input class="form-control bdn" id="attr2" type="text" value="" placeholder="每日上网时长" />
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5 mt6 ">
				<div class="col-xs-12 plr0 white_bg">
					<textarea rows="3" class="form-control bdn" id="attr3" placeholder="圈子有什么优势"></textarea>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5 mt6">
				<div class="col-xs-12 plr0 white_bg">
					<textarea rows="3" class="form-control bdn" id="attr4" placeholder="对圈子发展的简要想法"></textarea>
				</div>
			</div>
		</div>
		<div class="row white_bg">
			<div class="col-xs-12 pr7 ptb10">
				<div class="col-xs-3 pl0 pr7">
					<div class="col-xs-12 plr0 pos-r">
						<div class="col-xs-12 plr0" onclick="choosePic('showPic')" >
							<img src="images/front/picture.png" class="img-responsive">
						</div>
						<div id="fileUpload">
						</div>
					</div>
				</div>
				<div class="col-xs-9 gray" style="line-height: 60px;">为圈子选择一个标志</div>
			</div>
		</div>
		<div class="row pt20">
			<div class="col-xs-12 text-center gray ptb20">在社区发表5条以上才可以申请圈子协议</div>
			<div class="col-xs-12 text-center gray" onclick="addForum();">
				<a href="#" rel="button" class="col-xs-10 col-xs-offset-1 btn btn-lg btn-orange">提交申请</a>
			</div>
		</div>
	</div>

	<input type="hidden" value="${r}" id="r" />
	<input type="hidden" value="${loginStatus}" id="loginStatus" />
	<input type="hidden" id="shopId" value="${shopId}">
	<input type="hidden" id="from" value="${from}">

	<input type="hidden" id="openId" value="${openId}">
	<input id="appId" type="hidden" value="${appId}" />
	<input id="timeStamp2" type="hidden" value="${timeStamp}" />
	<input id="nonceStr2" type="hidden" value="${nonceStr}" />
	<input id="signature" type="hidden" value="${signature}" />

	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/forumApply.js" title="v"></script>


	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
     
    {{/rows}}	 		 
	</script>


</body>
</html>