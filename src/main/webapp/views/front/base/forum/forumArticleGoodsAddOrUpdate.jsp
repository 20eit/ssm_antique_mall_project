<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet"
	href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css"
	href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
<style>
.scrollbar_ul li {
	float: left;
	width: 110px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
</style>
</head>
<body style="background-color: #f8f8f8;">
	<input type="hidden" value="${forum.id}" id="forumId" />
	<input type="hidden" value="${user.id}" id="userId" />
	<input type="hidden" value="${orderGoods.id}" id="goodsId" />
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-8 pt10 plr0 text-center">
				<span class="fs16">晒好物</span>
			</div>
			<div class="col-xs-2 pt10 plr0 text-center" onclick="addGoods();">
				<span>发布</span>
			</div>
		</div>
		<div class="row white_bg">
			<div class="col-xs-12 ptb10 bdb">
				<div class="col-xs-3 plr0">
					<img width="73" src="${orderGoods.picPath}@73w_73h_4e_240-240-246bgc_50Q" class="img-responsive">
				</div>
				<div class="col-xs-7 pr0 pt10 fs16">
					<p>${orderGoods.goodsName}</p>
					<p class="orange">
						￥<span class="fs18">${orderGoods.shopPrice}</span>
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 pr7 ptb10 gray">
				<textarea name="title" maxlength="1000"
					onkeyup="javascript:setShowLength(this, 1000, 'goodsIntroduce_word');"
					rows="1" class="form-control gray_bg_init bdn" id="goodsIntroduce"
					placeholder="分享您的购物使用心得吧（8~1000字）"></textarea>
			</div>

			<div class="col-xs-12 pr7 pb10">
				<div class="col-xs-4 pl7 pt5">
					<div class="col-xs-12 plr0 pos-r">
						<input type="file" class="form-control fileUp" id="backdrop" />
						<div class="col-xs-12 plr0">
							<img src="/images/front/picture.png" class="img-responsive">
							<span class="weui_uploader_input" onclick="choosePic('showPic')"></span>
						</div>
					</div>
					</div>
						<div id="fileUpload">
							
						</div>
					
			</div>
		</div>
		<div class="col-xs-12 h50"></div>
	</div>
	<div class="fixed_topic pt15 pr7">
		<label for="sharecsbx" class="col-xs-10" style="display: block;"
			onclick="addGoods();">
		<div class="fs16"  align="center"><font  color="#FF5000" size="4">分享到${forum.name}</font></div>
		</label> 
	</div>
	<input type="hidden" id="openId" value="${openId}">
	<input id="appId" type="hidden" value="${appId}" />
	<input id="timeStamp2" type="hidden" value="${timeStamp}" />
	<input id="nonceStr2" type="hidden" value="${nonceStr}" />
	<input id="signature" type="hidden" value="${signature}" />
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript"
		src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript"
		src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<script src="/js/front/forum/forumArticleGoodsAddOrUpdate.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	{{/rows}}	 		 
	</script>


	<script>
		function setShowLength(obj, maxlength, id) {
			var rem = maxlength - obj.value.length;
			var wid = id;
			if (rem < 0) {
				rem = 0;
			}
			document.getElementById(wid).innerHTML = "还可以输入" + rem + "字数";
		}
	</script>
</body>
</html>