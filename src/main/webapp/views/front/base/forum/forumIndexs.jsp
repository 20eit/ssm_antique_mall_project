<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />


<style>
.scrollbar_ul li {
	float: left;
	width: 110px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}

.icon-heart-emptys:before {
	content: "\f08a";
}
.auction_title{
	position:relative;
}
</style>
</head>
<body style="background-color: #f0f0f6;">

	 <div id="weui-layer" class="weui-pull-to-refresh-layer" style="display: none;height:50px;">
		<div class="pull-to-refresh-arrow"></div>
		<!-- 上下拉动的时候显示的箭头 -->
		<div class="pull-to-refresh-preloader"></div>
		<!-- 正在刷新的菊花 -->
		<div class="down">下拉刷新</div>
		<!-- 下拉过程显示的文案 -->
		<div class="up">释放刷新</div>
		<!-- 下拉超过50px显示的文案 -->
		<div class="refresh">正在刷新...</div>
		<!-- 正在刷新时显示的文案 -->
	</div>


	<input type="hidden" value="${forum.id}" id="forumsId" />
	<input type="hidden" id="usersId" value="${user.id}">
	<input type="hidden" id="isJoin" value="${forum.isJoin}">
	<input type="hidden" id="isManager" value="${isManager}">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3">
				<i class="icon-angle-left icon-3x" onclick="lh.jumpR('/forumIndex')"></i>
			</div>
			<div class="col-xs-7 pt10 plr0 text-center">
				<span class="fs16">${forum.name}</span>
			</div>
		</div>
	
		<div class="row spreadCenter orange_bg_deep">
			<div class="col-xs-12 ptb10">
				<div class="col-xs-3 plr0">
					<a href="/forumIndexs/${forum.id}"><img src="${forum.logo}@70w_70h_4e_240-240-246bgc_50Q" class="img-responsive"> </a>
				</div>
				<div class="col-xs-6 pr0 pl7">
					<div class="col-xs-11 plr0">
						<div class="col-xs-12 plr0">${forum.name}</div>
						<div class="col-xs-12 plr0 fs12 pt3">
							<span>${forum.articleMemberCount}</span>个话题&nbsp;&nbsp;&nbsp;&nbsp; 成员<span>${forum.memberCount}</span>
						</div>
						<div class="col-xs-12 plr0 seller pt5">
							<!-- <a rel="button" class="btn btn-xs btn-orange">活跃用户</a> -->
							<a rel="button" onclick="getEssenceArticle(${forum.id});" class="btn btn-xs btn-orange">精华区</a>
						</div>
					</div>
				</div>

				<div id="joinForum" class="col-xs-3 plr0 text-center pt10">
					<div class="col-xs-12 plr0">
						<img id="isJionedImg" src="/images/front/icon_ok.png" />
					</div>
					<div id="isJionedText" class="col-xs-12 plr0 pt5">圈子首页</div>


				</div>
			</div>
		</div>
		<div class="row gray_bg_init">

			<div class="col-xs-2 pr0 ptb5">
				<a rel="button" class="btn btn-sm btn-orange"  onclick="gongGao();">公告</a>
			</div>
			<!-- 点击进入公告详情 -->
		
			<div id="gongGao" class="col-xs-10 plr7 pb5 pt10" onclick="GongGaoInfo(${forum.announcementId});">${forum.newGongGao}</div>

		</div>

		<div id="forumController" class="row mt6">
			<%-- <div class="col-xs-12 plr0 jewel_title">
				<ul>
					<li class="col4 gray_bg_init ptb0"><span>0</span><br>未审</li>
					<li class="col4 gray_bg_init ptb0"><span>0</span><br>已审</li>
					<li class="col4 gray_bg_init ptb0"><span>0</span><br>投诉</li>
					<li class="col4 gray_bg_init ptb0"><span>${forum.articleMemberCount}</span><br>话题</li>
				</ul>
			</div> --%>
		</div>
		<div id="data-container"></div>

		<div class="weui-infinite-scroll">
			<div class="infinite-preloader"></div>
			<!-- 菊花 -->
			正在加载...
			<!-- 文案，可以自行修改 -->
		</div>
		<br /><br /><br />
		<div class="myCircleIndex" style="position:fixed;">发布分享</div>
		<div class="releaseShare_bg"></div>
		<div class="releaseShare">
		
		
			<div class="col-xs-12 ptb10 bdb">
				<div class="col-xs-2 plr0 pt10">
					<img src="/images/front/present_01.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-10 pr0 fs16 pt3" onclick="lh.jumpR('/chooseOrderGoods/${forum.id}')">
					<div class="fw">晒好物</div>
					<div class="fs12 gray">晒出买过的好宝贝，与淘友们一期分享喜悦！</div>
				</div>
			</div>

			<div class="col-xs-12 ptb10 bdb" onclick="lh.jumpR('/forumArticleAddOrUpdate/${forum.id}')">
				<div class="col-xs-2 plr0 pt10">
					<img src="/images/front/theme_01.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-10 pr0 fs16 pt3">
					<div class="fw">发话题</div>
					<div class="fs12 gray">晒出买过的好宝贝，与淘友们一期分享喜悦！</div>
				</div>
			</div>

			<div class="col-xs-12 ptb10 bdb fs18 text-center" id="cirleCancel">取消</div>
		</div>

	</div>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/forumIndexs.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
{{#rows}}
		<div class="row white_bg myTeam mt6">
          <div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0" onclick="loadForumArticle('{{userSerial}}');">
					{{&userAvatars}}
				</div> 
				<div class="col-xs-9 plr0 fs16 pt5 blue_deep" 
				 onclick="loadForumArticle('{{userSerial}}');">{{username}}
				</div>
			</div>
            <div class="col-xs-12 ptb5 fw fs16" id="title_{{id}}">
                {{&titles}}
               <a href="/forumDetail/{{id}}"> <span>{{title}} </span></a>
            </div>
			<div class="col-xs-12 ptb5 fs16">{{subTitle}} {{&shai}}</div>
            <div class="col-xs-12">
                <div class="col-xs-12 plr0 ptb10">{{content}}</div>
                   <div class="made scrollbox" id="horizontal_{{id}}">
					<div class="madegame">
						<ul class="clearfix scrollbar_ul" id="scrollbar_{{id}}">
							{{&picsDom}}
						</ul>
					</div>
                </div>
                <div class="col-xs-12 plr0 ptb10 bdb">
                    <div class="col-xs-6 plr0">
                        <img width="20" src="/images/front/clock.png" class="img-responsive center-block dis-lin">
                        <span class="gray">{{date}}</span>
                    </div>
                    <div class="col-xs-6 plr0 gray text-right">
                        <i class="icon-comment-alt" onclick="lh.jumpR('/forumDetail/{{id}}');" ></i>&nbsp;<span>{{showArticleCount}}</span>&nbsp;&nbsp;&nbsp;
                       {{&isPraisesOrNot}}
                    </div>
                </div>
               {{&isManager}}
            </div>
        </div>
{{/rows}}
	</script>
	<script>
    (function($){
        $(window).load(function(){
            $.mCustomScrollbar.defaults.theme="light-2"; //set "light-2" as the default theme
            
            $("#ho1,#ho2").mCustomScrollbar({
                axis:"x",
                advanced:{autoExpandHorizontalScroll:true}
            });
        });
    })(jQuery);
</script>
</body>
</html>