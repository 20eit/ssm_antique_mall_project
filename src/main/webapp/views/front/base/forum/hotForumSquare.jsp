<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />


<style>
.scrollbar_ul li {
    float: left;
    width: 110px;
    position: relative;
}
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
.auction_title{
	position:relative;
}
</style>
</head>
<body style="background-color: #f0f0f6;">

<div id="weui-layer" class="weui-pull-to-refresh-layer" style="display: none;height:50px;">
    <div class="pull-to-refresh-arrow"></div> <!-- 上下拉动的时候显示的箭头 -->
    <div class="pull-to-refresh-preloader"></div> <!-- 正在刷新的菊花 -->
    <div class="down">下拉刷新</div><!-- 下拉过程显示的文案 -->
    <div class="up">释放刷新</div><!-- 下拉超过50px显示的文案 -->
    <div class="refresh">正在刷新...</div><!-- 正在刷新时显示的文案 -->
  </div>
  
<input type="hidden" id="usersId" value="${user.id}">
    <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-3">
                <i class="icon-angle-left icon-3x" onclick="lh.jumpR('/login');"></i>
            </div>
            <div class="col-xs-7 pt10 plr0 text-center">
                <span class="fs16">热门圈子</span>
            </div>
        </div>
        
        
        <div class="row">
			<div class="col-xs-12 ptb5 white_bg bdb">
				<div class="col-xs-6 plr0">
				<!-- @@_OSS_IMG_@@ -->
					<a href="forumUserArticle/${user.id}"><img class="dis-lin" src="${user.avatar}@50w_50h_4e_240-240-246bgc_50Q" width="45" /></a>&nbsp;&nbsp; <span class="gray">${user.username}</span>
				</div>
				<div class="col-xs-6 plr0 text-right pt10">
					<span class="orange fs16" onclick="lh.jumpR('/forumApply');">申请圈子</span> 
				</div>
			</div>
		</div>
		<div class="row">
			<div id="navigation" class="col-xs-12 plr0 jewel_title">
				<ul>
					<li class="col4">我的社区</li>
					<li class="col4 active">热门圈子</li>
					<li class="col4">我的圈子</li>
					<li class="col4">圈子广场</li>
				</ul>
			</div>
		</div>
		
		<div id="data-container">
		
		</div>
   
        <!-- <div class="row gray_bg_init">
            <div class="col-xs-12 ptb10">
                圈子推荐
            </div>
        </div> -->
       
       <!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/hotForumSquare.js" title="v"></script>
   <script id="template" type="x-tmpl-mustache">
	{{#rows}}
        <div class="row white_bg myTeam .mt6">
            <div class="col-xs-12 ptb10 bdb">
<a href="/forumIndexs/{{id}}">
                <div class="col-xs-3 plr0">
                    {{&logos}}
                </div>
                <div class="col-xs-7 pr0 fs16 pt10">
                    <p>{{name}}</p></a>
                    <p class="fs12 gray">{{typeDesc}}</p>
                </div>

                {{&isJoinOrBack}}
            </div>
</div>
         {{/rows}}	 		 
	</script>
          
          
        </div>
        <div class="modelAdd">加入成功</div>
	<div class="modelTc">退出成功</div>
    </div>
    <div class="weui-infinite-scroll">
  <div class="infinite-preloader"></div><!-- 菊花 -->
  正在加载... <!-- 文案，可以自行修改 -->
</div>
</body>
</html>