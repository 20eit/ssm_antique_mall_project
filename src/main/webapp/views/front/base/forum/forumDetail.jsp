<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />

<style>

.scrollbar_ul li {
	float: left;
	width: 100px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
#forumDetailInfo{
	margin-top: 15px;
}	
.col-xs-3{
	width: 14%;
}
#addComment{
	float: right;
}
.col-xs-7{width: 60%;}
.auction_title{
	position:relative;
}
body{
	width: 100%;
}
</style>	
<%--from by ForumAction. ForumSquare--%>
</head>
<body style="background-color: #f8f8f8;">
	 <div id="weui-layer" class="weui-pull-to-refresh-layer" style="display: none;height:35px;">
		<div class="pull-to-refresh-arrow"></div>
		<!-- 上下拉动的时候显示的箭头 -->
		<div class="pull-to-refresh-preloader"></div>
		<!-- 正在刷新的菊花 -->
		<div class="down">下拉刷新</div>
		<!-- 下拉过程显示的文案 -->
		<div class="up">释放刷新</div>
		<!-- 下拉超过50px显示的文案 -->
		<div class="refresh">正在刷新...</div>
		<!-- 正在刷新时显示的文案 -->
	</div>
 	
 	<div id="forumDetailInfo" style="width: 100%;margin-left: 10px;"></div>
	<input id="username" value="${user.username}" hidden="true" />
	<div class="container-fluid">
		
		<div class="col-xs-12 ptb15 fs16 bdb">
			<img style="float: left; width: 20px;"  src="/images/front/review_02.png" class="img-responsive dis-lin" />
			<div style="float: left; margin-left: 25px;" id="comenttotal"></div>
		</div>
		<div id="coment"></div>
		
		<div class="weui-infinite-scroll">
			<div class="infinite-preloader"></div>
			<!-- 菊花 -->
			正在加载...
			<!-- 文案，可以自行修改 -->
		</div>
		

		<div class="col-xs-12 h50"></div>
	</div>
	<div class="enter_scene_fixed bdt" >
		<div class="col-xs-3 pr0 pl7 pt5" >
			<img class="img-responsive dis-lin"  src="/images/front/review_03.png"/>
		</div>
		<div class="col-xs-7 plr0 pt5">
			<input type="text"  onclick="addPraise();" class="form-control" placeholder="楼主说的怎么样，点评一下" id="content" />
		</div>
		<div id="addComment" onclick="addComment()" class="col-xs-2 plr7 pt10">发送</div>
	</div>



	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/third-party/jquery-weui1/js/jquery-weui.js"></script>

	<script type="text/javascript" src="/third-party/jquery-weui1/js/swiper.js" charset='utf-8'></script>
	<link rel="stylesheet" href="/third-party/jquery-weui1/lib/weui.min.css">
	<link rel="stylesheet" href="/third-party/jquery-weui1/css/jquery-weui.css">


	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/forumDetail.js" title="v"></script>

	<!-- <script src="/js/front/comment/addComment.js" title="v"></script> -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
           <div class="row auction_title">
            <div class="col-xs-2">
                <i style="position: relative;bottom: 2px;" class="icon-angle-left icon-3x" onclick="lh.back();"></i>
            </div>
            <div class="col-xs-8 pt10 plr0 text-center">
                <span class="fs16"<font size="5px">{{&titles}}</font></span> 
            </div>
        	</div>
        <div class="row">
		  
            <div class="col-xs-12 ptb10" style="width:100%;">
            	<img src="{{userAvatar}}@35w_35h_4e_240-240-246bgc_50Q" onclick="loadForumArticle(${user.id})" width="30px" class="img-responsive dis-lin">
            	{{username}}
            	<a rel="button" class="btn btn-xs btn-primary ptb0 bdn">楼主</a>
            	<a rel="button" class="btn btn-sm btn-default" style="float:right;" onclick="lh.jumpR('/forumIndexs/{{forumId}}');">{{forumName}}</a>
           	</div>
        </div>
        <div class="row white_bg">
            <div class="col-xs-12 fs12 gray pt10">
            	创建于<span>{{&getDate}}</span>&nbsp;&nbsp;
            	<i class="icon-thumbs-up"></i><span>{{forums.praiseNum}}</span>&nbsp;&nbsp;&nbsp;&nbsp;
            	<span class="blue_deep">举报</span>
            </div>
            <div class="col-xs-12 fs12 gray pt10">
            	{{content}}
            </div>
		<div class="col-xs-5 pr0 pt5 fs10" onclick="showImgs('{{picPaths}}');">
   	    	{{&picsDom}}
		</div>
          <div class="col-xs-12 ptb15 blue_deep">
	            {{&shai}}
            </div>
        </div>
          {{/rows}}	 		 
	</script>
	<script id="template2" type="x-tmpl-mustache">
      <div class="row white_bg" >
 {{#rows}}	 	
            <div class="col-xs-12 ptb10 mt6">
            	<div class="col-xs-2 plr0">
            		<img src="{{userAvatar}}" width="40" class="img-responsive dis-lin">
           		</div>
            	<div class="col-xs-8 plr0 gray">
            		<span>{{username}}</span><br>
            		<span class="gray-init">{{&getDate}}</span>
           		</div>
           	</div>
            <div class="col-xs-12 ptb10 bdb">
            	<div class="col-xs-8 col-xs-offset-2 plr0">
            		{{content}}
           		</div>
           	</div>
 {{/rows}}	 	
        </div>
    </script>

</body>
</html>