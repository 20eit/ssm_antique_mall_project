<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet"
	href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css"
	href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />


<style>
.scrollbar_ul li {
	float: left;
	width: 110px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
.auction_title{
	position:relative;
}
</style>
<%--from by ForumAction. ForumSquare--%>
</head>
<body style="background-color: #f0f0f6;">
 <div id="weui-layer" class="weui-pull-to-refresh-layer" style="display: none;height:50px;">
    <div class="pull-to-refresh-arrow"></div> <!-- 上下拉动的时候显示的箭头 -->
    <div class="pull-to-refresh-preloader"></div> <!-- 正在刷新的菊花 -->
    <div class="down">下拉刷新</div><!-- 下拉过程显示的文案 -->
    <div class="up">释放刷新</div><!-- 下拉超过50px显示的文案 -->
    <div class="refresh">正在刷新...</div><!-- 正在刷新时显示的文案 -->
  </div> 


	<input type="hidden" id="userId" value="${user.id}" />
	<input type="hidden" id=cheatedAt value="${user.createdAt}" />
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="loadBackUrl();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">${user.username}的社区</span>
			</div>
		</div>
		<div class="row orange_bg white pb20">
			<div class="col-xs-12 ptb10">
				<img src="${user.avatar}@80w_80h_4e_240-240-246bgc"
					class="img-responsive center-block">
				<!-- @@_OSS_IMG_@@ -->
			</div>
			<div class="col-xs-12 fs24 text-center">
				<span>${user.username}</span> <img width="15"
					src="/images/front/pic_cheng.png" class="img-responsive dis-lin">
			</div>
			<div class="col-xs-12 pt5 text-center" id="days"></div>
		</div>
		<div class="row">
			<div class="col-xs-12 plr0 jewel_title">
				<ul>
					<li class="col3 active">发布的主题</li>
					<li class="col3">商家话题</li>
					<li class="col3"
						onclick="lh.jumpR('/forumUserReply/${user.serial}');">回复的评论</li>
				</ul>
			</div>
		</div>

		<div id="data-container"></div>

		<!-- <div class="row white_bg myTeam mt6">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0">
					<img width="30" src="/images/front/my_pic_03.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-9 plr0 fs16 pt5 blue_deep">索*****4</div>
			</div>
			<div class="col-xs-12 ptb5 fw fs16">木讷葫芦吊坠</div>
			<div class="col-xs-12 ptb5 fs16 gray">木讷葫芦美美哒！高级货！</div>
			<div class="col-xs-12">
				<div class="made scrollbox" id="horizontal">
					<div class="madegame">
						<ul class="clearfix" id="ho2">
							<li><img src="/images/front/pic_01.png" width="100" /></li>
							<li><img src="/images/front/index_products_02.png" width="100" /></li>
							<li><img src="/images/front/pic_01.png" width="100" /></li>
							<li><img src="/images/front/index_products_02.png" width="100" /></li>
							<li><img src="/images/front/pic_01.png" width="100" /></li>
							<li><img src="/images/front/index_products_02.png" width="100" /></li>
							<li><img src="/images/front/pic_01.png" width="100" /></li>
							<li><img src="/images/front/index_products_02.png" width="100" /></li>
							<li><img src="/images/front/pic_01.png" width="100" /></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 plr0 ptb10">
					<div class="col-xs-6 plr0">
						<img width="20" src="/images/front/clock.png" class="img-responsive center-block dis-lin"> <span class="gray">5分钟前</span>
					</div>
					<div class="col-xs-6 plr0 gray text-right">
						<i class="icon-comment-alt"></i>&nbsp;<span>275</span>&nbsp;&nbsp;&nbsp; <i class="icon-heart-empty"></i>&nbsp;<span>1660</span>
					</div>
				</div>
			</div>
		</div> -->

	</div>

	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript"
		src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript"
		src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/forumUserArticle.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="row white_bg myTeam mt6">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0" onclick="lh.jumpR('/forumUserArticle/{{userSerial}}');">
					{{&userAvatars}}
				</div>
				<div class="col-xs-9 plr0 fs16 pt5 blue_deep" onclick="lh.jumpR('/forumUserArticle/{{userSerial}}');">{{username}}</div>
			</div>
			<div class="col-xs-12">
				<div class="col-xs-12 plr0 ptb10">
					<a href="/forumDetail/{{id}}">{{&shai}}    {{title}}。</a>
				</div>
				<div class="made scrollbox" id="horizontal_{{id}}">
					<div class="madegame">
						<ul class="clearfix scrollbar_ul" id="scrollbar_{{id}}">
							{{&picsDom}}
						</ul>
					</div>
				</div>
				<div class="col-xs-12 plr0 ptb10">
					<div class="col-xs-6 plr0">
						<img width="20" src="/images/front/clock.png" class="img-responsive center-block dis-lin"> <span class="gray">{{date}}</span>
					</div>
					<div class="col-xs-6 plr0 gray text-right">
						<i class="icon-comment-alt" onclick="lh.jumpR('/forumDetail/{{id}}');"></i>&nbsp;<span>{{showArticleCount}}</span>&nbsp;&nbsp;&nbsp; 
{{&isPraisesOrNot}}
					</div>
				</div>
			</div>
		</div>
    {{/rows}}	 		 
	</script>

 <div class="weui-infinite-scroll">
  <div class="infinite-preloader"></div><!-- 菊花 -->
  正在加载... <!-- 文案，可以自行修改 -->
</div>

</body>
</html>