<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
<link rel="stylesheet" href="/css/front/wpk/weui.min.css" title="v" />
<style>
.scrollbar_ul li {
    float: left;
    width: 110px;
    position: relative;
}
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
 .weui_cells_checkbox .weui_check:checked+.weui_icon_checked:before{color: #ff5000}

</style>
    <%--from by ForumAction. ForumSquare--%>
</head>
<body style="background-color: #f8f8f8;">
<input type="hidden" id="usersId" value="${user.id}">
    <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-2">
                <i class="icon-angle-left icon-3x" onclick="lh.back();"></i>
            </div>
            <div class="col-xs-8 pt10 plr0 text-center">
                <span class="fs16">选择宝贝</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 plr0 ptb10">
                <div class="weui_cells weui_cells_checkbox gray_bg_init">
           
			        <div id="data-container">
			        </div>
			    </div>
            </div>
        </div>
    </div>
          
       <!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/chooseOrderGoods.js" title="v"></script>
   
      <script id="template" type="x-tmpl-mustache">
	{{#rows}} 
				<label class="weui_cell weui_check_label white_bg" onclick="lh.jump('/forumArticleAdd/${forumId}/{{id}}');">
			            <div class="weui_cell_bd weui_cell_primary">
				            <div class="col-xs-12 plr0">
				                <div class="col-xs-3 plr0">
				                    <img width="73" src="{{picPath}}" class="img-responsive">
				                </div>
				                <div class="col-xs-7 pr0 fs16">
				                    <p>{{goodsName}}</p>
				                    <p class="orange">￥<span class="fs18">{{shopPrice}}</span></p>
				                </div>
				            </div>
			            </div>
                        <div class="weui_cell_hd">
                            <input type="radio" class="weui_check" name="radio1">
                            <i class="weui_icon_checked"></i>
                        </div>
			        </label>
    {{/rows}}	 		 
	</script>
</body>
</html>