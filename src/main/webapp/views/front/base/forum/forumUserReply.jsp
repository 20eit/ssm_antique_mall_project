<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />


<style>
.scrollbar_ul li {
	float: left;
	width: 110px;
	position: relative;
}

.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
.auction_title{
	position:relative;
}
</style>
<%--from by ForumAction. ForumSquare--%>
</head>
<body style="background-color: #f0f0f6;">
 <div id="weui-layer" class="weui-pull-to-refresh-layer" style="display: none;height:50px;">
    <div class="pull-to-refresh-arrow"></div> <!-- 上下拉动的时候显示的箭头 -->
    <div class="pull-to-refresh-preloader"></div> <!-- 正在刷新的菊花 -->
    <div class="down">下拉刷新</div><!-- 下拉过程显示的文案 -->
    <div class="up">释放刷新</div><!-- 下拉超过50px显示的文案 -->
    <div class="refresh">正在刷新...</div><!-- 正在刷新时显示的文案 -->
  </div>

<input type="hidden" id="userId" value="${user.id}" />
<input type="hidden" id=cheatedAt value="${user.createdAt}" />
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="loadBackUrl();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">${user.username}的社区</span>
			</div>
		</div>
		<div class="row orange_bg white pb20">
			<div class="col-xs-12 ptb10">
				<img src="${user.avatar}@80w_80h_4e_240-240-246bgc" class="img-responsive center-block"><!-- @@_OSS_IMG_@@ -->
			</div>
			<div class="col-xs-12 fs24 text-center">
				<span>${user.username}</span> <img width="15" src="/images/front/pic_cheng.png" class="img-responsive dis-lin">
			</div>
			<div class="col-xs-12 pt5 text-center" id="days"></div>
		</div>
		<div class="row">
			<div class="col-xs-12 plr0 jewel_title">
				<ul>
					<li class="col3" onclick="lh.jumpR('/forumUserArticle/${user.serial}');">发布的主题</li>
					<li class="col3">商家话题</li>
					<li class="col3 active">回复的评论</li>
				</ul>
			</div>
		</div>
		
		<div id="data-container">
		
		</div>
		<div class="weui-infinite-scroll">
  <div class="infinite-preloader"></div>
  正在加载... <!-- 文案，可以自行修改 -->
</div>
		<!-- <div class="row white_bg myTeam mt6 pt15">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0">
					<img width="50" src="/images/front/my_pic_03.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-9 pr0 pl7 fs16 gray">
					<div>索*****4</div>
					<div class="pt5">
						<span>1天前</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue_deep">最爱甜美风</span>
					</div>
				</div>
			</div>
			<div class="col-xs-12 pt10 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10 fw">木讷葫芦吊坠</div>
			</div>
			<div class="col-xs-12 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10">
					<span class="blue_deep">h****哥</span>：谢谢
				</div>
			</div>
			<div class="col-xs-12 ptb10 gray text-right">
				<i class="icon-comment-alt"></i>&nbsp;<span>275</span>&nbsp;&nbsp;&nbsp; <i class="icon-heart-empty"></i>&nbsp;<span>1660</span>
			</div>
		</div>
		<div class="row white_bg myTeam mt6 pt15">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0">
					<img width="50" src="/images/front/my_pic_03.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-9 pr0 pl7 fs16 gray">
					<div>索*****4</div>
					<div class="pt5">
						<span>1天前</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue_deep">最爱甜美风</span>
					</div>
				</div>
			</div>
			<div class="col-xs-12 pt10 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10 fw">木讷葫芦吊坠</div>
			</div>
			<div class="col-xs-12 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10">
					<span class="blue_deep">h****哥</span>：谢谢
				</div>
			</div>
			<div class="col-xs-12 ptb10 gray text-right">
				<i class="icon-comment-alt"></i>&nbsp;<span>275</span>&nbsp;&nbsp;&nbsp; <i class="icon-heart-empty"></i>&nbsp;<span>1660</span>
			</div>
		</div>
	</div> -->
</div>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/forum/forumUserReply.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
     	<div class="row white_bg myTeam mt6 pt15">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0">
					{{&userAvatars}}
				</div>
				<div class="col-xs-9 pr0 pl7 fs16 gray">
					<div>{{receiverName}}</div>
					<div class="pt5">
						<span>{{&date}}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue_deep">{{thisTitle}}</span>
					</div>
				</div>
			</div>
			<!-- <div class="col-xs-12 pt10 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10 fw">[木讷葫芦吊坠]</div>
			</div>-->
			<div class="col-xs-12 fs16">
				<div class="col-xs-offset-2 col-xs-10 plr0 ptb10">
					<span class="blue_deep">{{username}}</span>:{{content}}
				</div>
			</div>
			
		</div>
		
    {{/rows}}	 		 
	</script>
	


</body>
</html>