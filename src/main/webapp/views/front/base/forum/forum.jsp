<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />

<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
    

<style>
.scrollbar_ul li {
    float: left;
    width: 110px;
    position: relative;
}
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
</style>
</head>
<body style="background-color: #f0f0f6;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-7 pt10 plr0 text-center">
				<span class="fs16">${forums.name}<a href="/myCommunity">我的社区</a></span>
			</div>
		</div>
		<div class="row spreadCenter orange_bg_deep">
			<div class="col-xs-12 ptb10">
				<div class="col-xs-3 plr0">
					<img src="${forums.logo}" width="71" class="img-responsive">
				</div>
				<div class="col-xs-7 pr0 pl7">
					<div class="col-xs-11 plr0">
						<div class="col-xs-12 plr0">${forums.name}</div>
						<div class="col-xs-12 plr0 fs12 pt3">
							<span>${forums.articleMemberCount}</span>个话题&nbsp;&nbsp;&nbsp;&nbsp; 成员<span>${forums.memberCount}</span>
						</div>
						<div class="col-xs-12 plr0 seller pt5">
							<a rel="button" class="btn btn-xs btn-orange">活跃用户</a> <a rel="button" class="btn btn-xs btn-orange">精华区</a>
						</div>
					</div>
				</div>
				<div class="col-xs-2 plr0 text-right pt10">
					<div class="col-xs-12 plr0">
						<img width="30" class="right-block" src="/images/front/join.png" />
					</div>
					<div class="col-xs-12 plr0 pt5">加入</div>
				</div>
			</div>
		</div>
		<div class="row gray_bg_init">
			<div class="col-xs-2 pr0 ptb5">
				<a rel="button" class="btn btn-sm btn-orange">公告</a>
			</div>
			<div class="col-xs-10 plr7 pb5 pt10">【发帖必读】珠宝DIY圈子发帖规范</div>
		</div>
		
			<div id="data-container">
				
		</div>
	
		<div class="row h50"></div>
	</div>


	<div class="myCircleIndex">发布分享</div>
	<div class="releaseShare_bg"></div>
	<div class="releaseShare">
		<div class="col-xs-12 ptb10 bdb">
			<div class="col-xs-2 plr0 pt10">
				<img src="/images/front/present_01.png" class="img-responsive center-block">
			</div>
			<div class="col-xs-10 pr0 fs16 pt3">
				<div class="fw">晒好物</div>
				<div class="fs12 gray">晒出买过的好宝贝，与淘友们一期分享喜悦！</div>
			</div>
		</div>
		<div class="col-xs-12 ptb10 bdb">
			<div class="col-xs-2 plr0 pt10">
				<img src="/images/front/theme_01.png" class="img-responsive center-block">
			</div>
			<div class="col-xs-10 pr0 fs16 pt3">
				<div class="fw">发话题</div>
				<div class="fs12 gray">晒出买过的好宝贝，与淘友们一期分享喜悦！</div>
			</div>
		</div>
		<div class="col-xs-12 ptb10 bdb fs18 text-center" id="cirleCancel">取消</div>
	</div> 		 
	</script>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>

	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<script src="/js/front/forum/forum.js" title="v"></script>
	<!-- -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="row white_bg myTeam mt6">
			<div class="col-xs-12 ptb5">
				<div class="col-xs-2 plr0">
					<img width="30" src="{{userAvatar}}" class="img-responsive center-block">
				</div>
				<div class="col-xs-10 plr0 fs16 pt5">
					<span class="blue_deep">{{userName}}</span>
					<button type="button" class="btn btn-xs btn-default pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...</button>
					<ul class="dropdown-menu" style="left: auto; right: 0; min-width: 100px;">
						<li class="text-center ptb0">举报话题</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12 ptb5 fw fs16">
				<img width="25" src="/images/front/ding.png" class="img-responsive center-block dis-lin"> <span>{{title}}</span>
			</div>
			<div class="col-xs-12 ptb5 fs16">{{subTitle}}</div>
			<div class="col-xs-12">
				<div class="made scrollbox" id="horizontal_{{id}}">
					<div class="madegame">	
						<ul class="clearfix scrollbar_ul" id="scrollbar_{{id}}">
							{{&picsDom}}
						</ul>
					</div>
				</div>
				<div class="col-xs-12 plr0 ptb10 bdb">
					<div class="col-xs-6 plr0">
						<img width="20" src="/images/front/clock.png" class="img-responsive center-block dis-lin"> <span class="gray">{{date}}</span>
					</div>
					<div class="col-xs-6 plr0 gray text-right">
						<i class="icon-comment-alt"></i>&nbsp;<span>[{{visitNum}}]</span>&nbsp;&nbsp;&nbsp; <i class="icon-heart-empty"></i>&nbsp;<span>[{{praiseNum}}]</span>
					</div>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>

	<script id="template_offers" type="x-tmpl-mustache">
	{{#rows}}	 	
		
	{{/rows}}
	</script>

</body>
</html>
