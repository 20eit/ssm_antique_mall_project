<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/call.css" title="v" />
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>

</head>
<body style="background-color: #f0f0f6;">
<input type="hidden" value="${userFUN.avaliableMoney}" id="avaliableMoney"/>
<input type="hidden" value="${userFUN.otherFund}" id="otherFund"/>
<input type="hidden" value="${isRealAuth}" id="isRealAuth"/>
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.back();">
				<i class="icon-angle-left icon-3x" style="position: relative; bottom: 2px;"></i>
			</div>
			<div class="col-xs-8 pt10 plr0 text-center">
				<span class="fs16">提现</span>
			</div>
		</div>
		<div class="row white_bg ptb10 bdb mt6">
			<div class="col-xs-4 pr0 pt5">到账银行卡</div>
			<div class="col-xs-8 pl0">
				<select class="form-control bdn blue" id="bankNameAndNum">
				<c:if test="${userFUN.bankCard1 != null }">
					<option value="${userFUN.bankCard1}">${userFUN.bankName1}(${bankCard1})</option>
				</c:if>
				<c:if test="${userFUN.bankCard2 != null }">
					<option value="${userFUN.bankCard2}">${userFUN.bankName2}(${bankCard2})</option>
				</c:if>
				<c:if test="${userFUN.bankCard3 != null }">
					<option value="${userFUN.bankCard3}">${userFUN.bankName3}(${bankCard3})</option>
				</c:if>
				<c:if test="${userFUN.bankCard4 != null }">
					<option value="${userFUN.bankCard4}">${userFUN.bankName4}(${bankCard4})</option>
				</c:if>
				<c:if test="${userFUN.bankCard5 != null }">
					<option value="${userFUN.bankCard5}">${userFUN.bankName5}(${bankCard5})</option>
				</c:if>
				
				</select>
			</div>
			
		</div>
		<div class="row white_bg ptb10 bdb mt6">
			<div class="col-xs-4 pr0 pt5">选择提现方式</div>
			<div class="col-xs-8 pl0">
				<select class="form-control bdn blue" onchange="selectAorO();" id="tixianFangShi">
					<option value="1">余额支付</option>
					<option value="2">保证金支付</option>
				</select>
			</div>
		</div>
		<div class="row white_bg ptb10 bdb mt6">
			<div class="col-xs-4 pr0 pt5">提现金额</div>
			<div class="col-xs-8 pl0">
				<input type="number" onkeyup="shouxufei();" id="withdrawDepositnumber" class="form-control bdn" placeholder="可提现的金额：${userFUN.avaliableMoney}元">
			</div>
		</div>
		<div class="row white_bg ptb10 bdb mt6">
			<div class="col-xs-4 pr0 pt5">提现手续费</div>
			<div class="col-xs-8 pl0">
				<input type="number" id="withdrawDepositshouxu" class="form-control bdn" placeholder="" readonly="readonly">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 gray-init ptb5">
				<p>提现时间：工作日24小时到帐！（节假日到帐时间顺延） 每日限额50000.00元</p>
				<p>提现事项：提现时银行扣除0.04的手续费。提现金额必 须大于100.00元</p>
			</div>
		</div>
		<div class="row pt30">
			<div class="col-xs-12 pt30" onclick="validate()">
				<a rel="button" class="col-xs-12 btn btn-lg btn-orange">提现</a>
			</div>
		</div>
	</div>


	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/js/front/user/myseller.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript" src="/js/common/call.js" title="v"></script>
	<script type="text/javascript" src="/js/front/user/propertyManage/withdraw.js" title="v"></script>


</body>
</html>