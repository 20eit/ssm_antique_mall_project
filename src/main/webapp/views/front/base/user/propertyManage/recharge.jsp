<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/call.css" title="v" />
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
</head>
<body style="background-color: #f0f0f6;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.jumpR('/user');">
				<i class="icon-angle-left icon-3x" style="position: relative; bottom: 2px;"></i>
			</div>
			<div class="col-xs-8 pt10 plr0 text-center">
				<span class="fs16">充值</span>
			</div>
		</div>
		<div class="row white_bg ptb10 bdb">
			<div class="col-xs-2 pr0 fs16 pt5">金额</div>
			<div class="col-xs-10 pl0">
				<span id="applyMoney" onclick="inputChargeMoney();" class="form-control bdn"></span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-right gray ptb5">单笔线上充值金额最多5000元</div>
		</div>
		<div class="row pt30">
			<div class="col-xs-12 pt30" onclick="doPay();return false;">
				<a href="javascript:;" rel="button" class="col-xs-12 btn btn-lg btn-orange">确认充值</a>
				<!-- disabled -->
			</div>
		</div>
	</div>

	<div id="chargeToast" class="weui_loading_toast" style="display: none;">
		<div class="weui_mask_transparent"></div>
		<div class="weui_toast">
			<div class="weui_loading">
				<div class="weui_loading_leaf weui_loading_leaf_0"></div>
				<div class="weui_loading_leaf weui_loading_leaf_1"></div>
				<div class="weui_loading_leaf weui_loading_leaf_2"></div>
				<div class="weui_loading_leaf weui_loading_leaf_3"></div>
				<div class="weui_loading_leaf weui_loading_leaf_4"></div>
				<div class="weui_loading_leaf weui_loading_leaf_5"></div>
				<div class="weui_loading_leaf weui_loading_leaf_6"></div>
				<div class="weui_loading_leaf weui_loading_leaf_7"></div>
				<div class="weui_loading_leaf weui_loading_leaf_8"></div>
				<div class="weui_loading_leaf weui_loading_leaf_9"></div>
				<div class="weui_loading_leaf weui_loading_leaf_10"></div>
				<div class="weui_loading_leaf weui_loading_leaf_11"></div>
			</div>
			<p class="weui_toast_content">正在提交数据....</p>
		</div>
	</div>

	<%@ include file="/views/front/common/z_div_call_charge.htm"%><!-- 出价面板 -->
	<%-- <%@ include file="/views/front/common/z_div_call.htm"%> --%>
	<!-- 出价面板 -->

	<input type="hidden" id="openId" value="${openId}">
	<input id="appId" type="hidden" value="${appId}" />
	<input id="timeStamp2" type="hidden" value="${timeStamp}" />
	<input id="nonceStr2" type="hidden" value="${nonceStr}" />
	<input id="signature" type="hidden" value="${signature}" />
	<input type="hidden" value="${r}" id="r" />
	<input type="hidden" value="${loginStatus}" id="loginStatus" />

	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/js/front/user/myseller.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript" src="/js/common/call.js" title="v"></script>
	<script type="text/javascript" src="/js/front/charge/recharge.js" title="v"></script>
	<!-- <script type="text/javascript" src="/js/front/user/propertyManage/recharge.js" title="v"></script> -->
</body>
</html>