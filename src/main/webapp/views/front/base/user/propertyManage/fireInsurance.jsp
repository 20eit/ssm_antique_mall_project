<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>

<link rel="stylesheet" type="text/css" href="/css/front/call.css" title="v" />
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
</head>
<body style="background-color: #f0f0f6;">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.back();">
				<i class="icon-angle-left icon-3x" style="position: relative; bottom: 2px;"></i>
			</div>
			<div class="col-xs-8 pt10 plr0 text-center">
				<span class="fs16">交纳诚信保证金</span>
			</div>
		</div>
		<div class="row white_bg ptb10 mt6">
			<div class="col-xs-5 pr0 pt3">已交纳保证金</div>
			<div class="col-xs-7 pl0 pt3 text-right red_deep">${userFund.otherFund}<c:if test="${null == userFund.otherFund}">0.00</c:if>
				元
			</div>
			<input type="hidden" value="${userFund.otherFund}" id="crediMoneyPayed">
		</div>
		<div class="row">
			<div class="col-xs-12 gray-init ptb5">增加交纳</div>
		</div>
		<div class="row white_bg ptb10 bdb">
			<div class="col-xs-3 pr0 pt5">金额</div>
			<div class="col-xs-9 pl0">
				<span onclick="inputChargeMoney();" id="money" class="form-control bdn qing"></span>
			</div>
		</div>
		<div class="row">
			<!-- <div class="col-xs-12 gray-init ptb5 text-right">至少缴纳1000元</div> -->
		</div>
		<div class="row pt30">
			<div class="col-xs-12 pt30" onclick="payCreditMoney();">
				<a class="weui_btn weui_btn_primary">下一步</a>
			</div>
			<div class="col-xs-12 gray-init fs12 ptb10 text-right">下一步即表示同意《微拍客消费者保障服务协议》</div>
		</div>
	</div>

	<%@ include file="/views/front/common/z_div_call_charge.htm"%><!-- 出价面板 -->

	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/js/front/user/myseller.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript" src="/js/common/call.js" title="v"></script>
	<script type="text/javascript" src="/js/front/user/safety/fireInsurance.js" title="v"></script>

</body>
</html>