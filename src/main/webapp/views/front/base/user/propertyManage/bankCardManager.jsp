<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
</head>
<body style="background-color: #f0f0f6;">
<input type="hidden" value="${notRealName }" id="notRealName"/> 
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.jumpR('/propertyManage');">
				<i class="icon-angle-left icon-3x" style="position: relative; bottom: 2px;"></i>
			</div>
			<div class="col-xs-8 pt10 plr0 text-center">
				<span class="fs16">我的银行卡</span>
			</div>
			<div class="col-xs-2 ptb10" onclick="lh.jumpR('/addCardManager');">
				<img src="/images/front/add_white.png" />
			</div>
		</div>
		<div class="row pb10">
			<c:if test="${userFund.bankCard1 != null }">
				<div class="col-xs-12 pt10 plr7" style="width: 400px">
					<div style="background-color: #D39049; width: 400px; height: 122px; border-radius: 10px;"></div>
					<div class="cardType" style="font-size: 18px;">${userFund.bankName1 }</div>
					<div class="cardNo" style="font-size: 18px;">卡号:${userFund.bankCard1 }</div>
				</div> 
			</c:if>
			<c:if test="${userFund.bankCard2 != null }">
				<div class="col-xs-12 pt10 plr7" style="width: 400px">
					<div style="background-color: #D39049; width: 400px; height: 122px; border-radius: 10px;"></div>
					<div class="cardType" style="font-size: 18px;">${userFund.bankName2 }</div>
					<div class="cardNo" style="font-size: 18px;">卡号:${userFund.bankCard2 }</div>
				</div>
			</c:if>
			<c:if test="${userFund.bankCard3 != null }">
				<div class="col-xs-12 pt10 plr7" style="width: 400px">
					<div style="background-color: #D39049; width: 400px; height: 122px; border-radius: 10px;"></div>
					<div class="cardType" style="font-size: 18px;">${userFund.bankName3 }</div>
					<div class="cardNo" style="font-size: 18px;">卡号:${userFund.bankCard3 }</div>
				</div>
			</c:if>
			<c:if test="${userFund.bankCard4 != null }">
				<div class="col-xs-12 pt10 plr7" style="width: 400px">
					<div style="background-color: #D39049; width: 400px; height: 122px; border-radius: 10px;"></div>
					<div class="cardType" style="font-size: 18px;">${userFund.bankName4 }</div>
					<div class="cardNo" style="font-size: 18px;">卡号:${userFund.bankCard4 }</div>
				</div>
			</c:if>
			<c:if test="${userFund.bankCard5 != null }">
				<div class="col-xs-12 pt10 plr7" style="width: 400px">
					<div style="background-color: #D39049; width: 400px; height: 122px; border-radius: 10px;"></div>
					<div class="cardType" style="font-size: 18px;">${userFund.bankName5 }</div>
					<div class="cardNo" style="font-size: 18px;">卡号:${userFund.bankCard5 }</div>
				</div>
			</c:if>
		</div>
	</div>

	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/js/front/user/myseller.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript" src="/js/common/call.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">
		$(function(){
			var notRealName = $("#notRealName").val();
			if(notRealName == "notRealName"){
				lh.alert("您没有通过实名认证，请先实名认证",jumpToUser);
			}
		});
		function jumpToUser(){
			lh.jumpR("/user/safety/realNameAuthentication");
		}
	</script>

</body>
</html>