<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>

<style>
.scrollbar_ul li {
    float: left;
    width: 110px;
    position: relative;
}
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}
.icon-heart-emptys:before {
    content: "\f08a";
}
.weui-pull-to-refresh-layer{
	display: none;
}
.auction_title{
	position:relative;
}
.bg_grays{
color: #FF5000;
background-color: #FF5000;
}
.shareImg{
	float:right;
}
.back{
	 position:relative;
} 
.icon-3x{
 position:static;
  top:-50%;
  vertical-align:middle
}
</style>
</head>
<body style="background-color: #f0f0f6;">
<input type="hidden" value="${user.id}" id="userId"/>
    <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x" style="position: relative;bottom: 2px;"></i>
			</div>
            <div class="col-xs-6 pt10 plr0 text-center">
                <span class="fs16">我的参拍</span>
            </div>
            <div class="col-xs-3 ptb10 pl0 text-right">
                <!-- <img class='shareImg' src="/images/front/share_white.png" onclick="showShare();" />&nbsp;&nbsp; -->
                <!-- <img src="/images/front/add_white.png" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                 <ul class="dropdown-menu" style="left:auto;right:0;min-width: 100px;">
                    <li><a href="#"><img src="/images/front/partake_add_01.png" >&nbsp;&nbsp;参与-微拍</a></li>
                    <li><a href="#"><img src="/images/front/partake_add_02.png" >&nbsp;&nbsp;参与-市场</a></li>
                    <li><a href="#"><img src="/images/front/partake_add_03.png" >&nbsp;&nbsp;参与-鬼市</a></li>
                </ul>  -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 plr0 jewel_title">
                <ul>
                    <li id="isJoin" class="active">参拍的</li>
                    <li id="isEnd" >已结束</li>
                </ul>
            </div>
        </div>
        <div class="row">
        	<div id="data-container"></div>
            <!-- <div class="col-xs-12 product plr0 pt6">
                <div class="col-xs-12 productMs">
                    <div class="col-xs-12 ptb3 gray text-center">
                        <div class="col-xs-12 plr0 bdb">
                            <span>4月19日</span>&nbsp;&nbsp;
                            <span>22:00</span>&nbsp;&nbsp;
                            <span>结束</span>
                        </div>
                    </div>
                    <div class="col-xs-12 plr0 ptb5 bdb">
                        <div class="col-xs-4">
                            <img class="img-responsive center-block" width="77" src="/images/front/pic_01.png" />
                        </div>
                        <div class="col-xs-5 plr0">
                            <div class="gray_deep fs18 elli">翡翠A货小吊坠</div>
                            <div class="col-xs-12 plr0 pt5">
                                <span class="gray pull-left fs14">￥1</span>
                            </div>
                            <div class="col-xs-12 plr0 gray pt5">
                                <div class="col-xs-2 plr0 pt3 partakeSub">
                                    <img class="center-block" src="/images/front/partake_sub.png" >
                                </div>
                                <div class="col-xs-6 plr0 text-center partakePrice">
                                    800
                                </div>
                                <div class="col-xs-2 plr0 pt3 partakeAdd">
                                    <img class="center-block" src="/images/front/partake_add.png" >
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="partake_bonus">
                                <div class="top">
                            		        出局
                                </div>
                                <div class="bottom">
                                  	  出价
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
          
        </div>
    </div>
    
      <%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%>  --%>
	<!-- 底部菜单 -->
	<%@ include file="/views/common/common_share.htm"%><!-- 分享DIV -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	 <!-- <script type="text/javascript" src="/cssjs_stable/js/bootstrap.min.js"></script>  -->
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	 <script src="/js/front/user/aution/myAuction.js" title="v"></script>
	<!-- -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="col-xs-12 product plr0 pt6">
                <div class="col-xs-12 productMs">
                    <div class="col-xs-12 ptb3 gray text-center">
                        <div class="col-xs-12 plr0 bdb">
                            <span>{{&date}}</span>&nbsp;&nbsp;
                            <span>结束</span>
                        </div>
                    </div>
                    <div class="col-xs-12 plr0 ptb5 bdb">
                        <div class="col-xs-4" onclick="lh.jumpR('/am?amSerial={{serial}}');">
                            <img class="img-responsive center-block" width="77" src="{{picPath}}@80w_80h_4e_240-240-246bgc_50Q" />
                        </div>
                        <div class="col-xs-5 plr0">
                            <div class="gray_deep fs18 elli">{{goodsName}}</div>
                            <div class="col-xs-12 plr0 pt5">
								<input type="hidden" value="{{offerPrice}}" id="offerPrice_{{id}}"/>
                                <span class="gray pull-left fs14">￥{{offerPrice}}</span>
                            </div>
                                {{&offerPrices}}
                        </div>
                        <div class="col-xs-3">
                            <div class="partake_bonus">
                                <div id="top" class="top" onclick="deleteMicroOffers({{id}},{{goodsId}});">
                            		        出局
                                </div>
                                <div id="bottom" class="bottom" onclick="addMicroOffers({{id}},{{goodsId}});">
                                  	  出价
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	{{/rows}}
	</script>
    
</body>
</html>