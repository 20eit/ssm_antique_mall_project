<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
</head>
<body style="background-color: #f0f0f6;">

<input type="hidden" id="userId" value="${user.id}" />

    <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-2" onclick="lh.back();">
                <i class="icon-angle-left icon-3x"></i>
            </div>
            <div class="col-xs-8 pt10 plr0 text-center">
                <span class="fs16">地址管理</span>
            </div>
        </div>
        <div id="data-container"></div>
       <!--  <div class="row white_bg mt6">
        	<div class="col-xs-12 pt10">
                <div class="col-xs-6 plr0">张丽丽</div>
                <div class="col-xs-6 plr0 text-right">15000000000</div>
        	</div>
        	<div class="col-xs-12 ptb10 bdb">
                天津市河西区******
        	</div>
        	<div class="col-xs-12 ptb10">
                <div class="col-xs-6 plr0">
	                <div class="weui_cells weui_cells_checkbox">
				        <label class="weui_cell weui_check_label pa0 orange" for="x12">
				            <div class="weui_cell_hd">
	                            <input type="radio" class="weui_check" name="radio1" id="x12" >
	                            <i class="weui_icon_checked"></i>
	                        </div>
				            <div class="weui_cell_bd weui_cell_primary">
					            默认地址
				            </div>
				        </label>
                	</div>
                </div>
                <div class="col-xs-3 plr0">
	                <img src="/images/front/managerMenu_01.png" class="img-responsive dis-lin">&nbsp;
	                修改
                </div>
                <div class="col-xs-3 plr0 text-right removeAddress">
	                <img src="/images/front/managerMenu_04.png" class="img-responsive dis-lin">&nbsp;
	                删除
                </div>
        	</div>
        </div> -->
    </div>
    <br /><br />
    <div class="addAddress" onclick="lh.jumpR('/addOrUpdateUserAddress');">
    	<img src="/images/front/add_white.png" class="dis-lin" />&nbsp;
    	<a>
    	新增收货地址
    	</a>
    </div>
    <div class="spmc_bg"></div>
    <div class="alert_madol" >
        <div>确认要删除该地址吗？</div>
        <div class="pt10 text-right">
        	<button type="button" id="cancleBtn" class="btn btn-sm btn-orange-remove">取消</button>&nbsp;&nbsp;&nbsp;
        	<button type="button" id="removeBtn" class="btn btn-sm btn-orange-remove">删除</button>
        </div>
    </div>
    <div class="modelAddress">
    	删除成功
    </div>
    <div class="defaultAddress">
    	设置默认地址成功
    </div>
    
    
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	
	<script type="text/javascript" src="/js/front/user/addReceiveAddress.js" title="v"></script>
    <script id="template" type="x-tmpl-mustache">
	{{#rows}}
		 <div class="row white_bg mt6">
        	<div class="col-xs-12 pt10">
                <div class="col-xs-6 plr0">
                {{receiverName}}
             </div>
                <div class="col-xs-6 plr0 text-right">
                 {{phone}}
            </div>
        	</div>
        	<div class="col-xs-12 ptb10 bdb">
                {{provinceName}}{{cityName}}{{addressDetail}}
        	</div>
        	<div class="col-xs-12 ptb10">
                <div class="col-xs-6 plr0">
	                <div class="weui_cells weui_cells_checkbox">
				        <label class="weui_cell weui_check_label pa0 orange">
				            <div class="weui_cell_hd">
								{{&isDefult}}
	                           
	                            <i class="weui_icon_checked"></i>
	                        </div>
				            <div class="weui_cell_bd weui_cell_primary">
					            默认地址
				            </div>
				        </label>
                	</div>
                </div>
                <div class="col-xs-3 plr0" onclick="lh.jumpR('/addOrUpdateAddress/{{id}}');">
	                <img src="/images/front/managerMenu_01.png" class="img-responsive dis-lin">&nbsp;
	                修改
                </div>
                <div class="col-xs-3 plr0 text-right removeAddress" onclick="deleteAddress({{id}});">
	                <img src="/images/front/managerMenu_04.png" class="img-responsive dis-lin">&nbsp;
	                删除
                </div>
        	</div>
        </div>
	{{/rows}}	
    </script>
</body>
</html>