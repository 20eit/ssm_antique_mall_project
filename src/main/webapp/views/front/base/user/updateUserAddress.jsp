<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/style_v1.css" title="v">
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v">
<link rel="stylesheet" href="/css/front/my_seller.css" title="v">
</head>
<body style="background-color: #f0f0f6;">

<input type="hidden" id="userId" value="${user.id}" />

    <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-2" onclick="lh.back();">
                <i class="icon-angle-left icon-3x"></i>
            </div>
            <div class="col-xs-8 pt10 plr0 text-center">
                <span class="fs16">修改地址</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 ptb15 set_selectRow" style="line-height:34px;">
            	联系人
            		<input  class="form-control bdn" type="text" id="receiverName" value="${userAddress.receiverName}" style="width:80%;float:right">
            </div>
            <div class="col-xs-12 ptb15 set_selectRow" style="line-height:34px;">
            	联系电话
            	<input  class="form-control bdn" value="${userAddress.phone}"  type="number" id="phone" style="width:80%;float:right">
            </div>
            <div class="col-xs-12 ptb10 set_selectRow">
				<div class="col-xs-4 plr0 pt5">
            		所在省
				</div>
				<div class="col-xs-8 plr0" id="provinceDiv">
	        		<!-- <select class="form-control bdn"  id="province">
					  	<option>北京</option>
					</select> -->
				</div>
            </div>
            <div class="col-xs-12 ptb10 set_selectRow">
				<div class="col-xs-4 plr0 pt5">
            		所在地区
				</div>
				<div class="col-xs-8 plr0" id="cityDiv">
	        	<select class="form-control bdn">
					  	<option>请选择</option>
					</select>
				</div>
            </div>
            <div class="col-xs-12 ptb15 set_selectRow bdn">
            	<p class="gray">详细地址</p>
            	<textarea class="form-control bdn" rows="5" id="addressDetail">${userAddress.addressDetail}</textarea>
            </div>
        </div>
    </div>
    <div class="addAddress pa0">
    	<button class="btn col-xs-12 btn-orange btn-lg bdn" onclick="addUserAddress();">保存</button>
    </div>
    
    <input type="hidden" id="id" value="${id}">
	<input type="hidden" id="userId" value="${user.id}">
	<input type="hidden" id="userAddressId" value="${userAddress.id}">
	<input type="hidden" id="userAddressCity" value="${userAddress.city}">
	<input type="hidden" id="userAddressProvince" value="${userAddress.province}">
    
    <%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/js/front/user/addAddress.js" title="v"></script>
</body>
</html>