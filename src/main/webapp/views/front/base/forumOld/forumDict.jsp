<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<div class="pz_main">
		<div class="c_0100_19">
			 <div class="slide_sale slide_teacher" id="slide_sale">
				<div class="bd">
					<div class="slide_sale slide_teacher c_0100_3" style="padding:0;text-align: center;" id="slide_sale">
						<div class="cytopdiv">
							<!-- <ul>
								<li style="margin:0px;position: relative;border-radius: 5px 0 0 5px;border-right: none;" class="pointer on" >展会活动</li>
								<li style="margin:0px;border-radius: 5px 0 0 5px;" class="pointer" onclick="location.href='/news'">新闻资讯</li>
							</ul> -->
							<span class="cytopdiv1 pointer" style="background: white;color:black" onclick="location.href='/goods'">商城</span>
							<span class="cytopdiv1 pointer" style="background: #5F6367;color:white;border-left: none;border-radius: 0px;" onclick="location.href='/forums'">论坛</span>
							<span class="cytopdiv2 pointer" style="background: white;color:black;" onclick="location.href='/allShop'">商铺</span>
						</div>
					</div>
				</div>
				<div class="bd">
					<div class="slide_sale slide_teacher c_0100_3" style="padding:0;text-align: center;" id="slide_sale">
						<div class="cytopul">
							 <ul>
								<li id="allNews" class="pointer" style="width:33%" onclick="location.href='/forums'">热门论坛</li>
								<li id="antiqueNews" style="width:33%;border-bottom:1px solid red;color:red;" onclick="location.href='/forumDict'" >论坛分类</li>
								<li id="antiqueKnowloge" style="width:33%" onclick="location.href='/myForum'">我的论坛</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="slide_sale slide_teacher c_0100_3" style="padding:0;margin:0;text-align: center;" id="slide_sale">
					<div class="addActivies">
						<span class="pointer" onclick="location.href='/forumRule'">我要开论坛</span>
					</div>
				</div>
				<div class="bd">
					<div class="ul_show">
						<div id="forumDictList" class="sale_show">
							
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/util/util.js" title="v"></script>
	<script src="/js/front/forum/forumDict.js" title="v"></script>
	
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="li_sale pointer" onclick="location.href='/forums?typeId={{id}}'" >
             	<div class="img_112" style="height:60px;width:60px;overflow: hidden;"><img src="{{dictValue}}" width="100%" /></div>
            	<div class="tit_210" style="border-bottom: none;"><a href="/forums?typeId={{id}}"><nobr>{{dictName}}</nobr></a></div>
        </div>
	{{/rows}}	 		 
	</script>
</body>
</html>
