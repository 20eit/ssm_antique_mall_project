<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/tiaokuan.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
<input type="hidden" id="object" value="${forumArticle.id}">
<input type="hidden" id="share" value="${share}">
	<div class="c_0100_22">
		<div class="t_0100_24_box">
			<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
			<div class="new_list_down">
				<img src="/images/front/forum_img15.png" width="31" height="13" />
			</div>
		</div>
		<div class="t_0100_25 bodyWidth" style="left: inherit;z-index: 770;">
			<div class="l_117" onclick="location.href='/forumArticle/${forumArticle.forumId}'">
				<img src="${forum.logo}" width="100%" />
			</div>
			<div class="r_537">
				<div class="t_537_1">
					<a href="/forumArticle/${forumArticle.forumId}" style="text-decoration: none;">${forum.name}</a>
				</div>
				<div class="t_537_2">话题&nbsp;${count}&nbsp;|&nbsp;访问&nbsp;${forum.visitNum}</div>
			</div>
		</div>
		<div class="slide_forum" id="slide_forum">
			<div class="bd">
				<!--切换循环forum_show-->
				<div class="forum_show">
					<%-- <div class="t_0100_26">
						<div class="l_590">
							<!--公告开始-->
							<div class="slide_fortxt" id="slide_fortxt">
								<div class="inbd">
									<ul>
										<li><marquee id="mq" behavior="scroll" scrollamount="3" style="width: 100%;">
												<a href="javascript:;"><nobr>公告：${forum.name}论坛规章</nobr></a>
											</marquee></li>
									</ul>
								</div>
								<div class="inhd">
									<ul></ul>
								</div>
							</div>
							<!--公告结束-->
						</div>
						<div class="r_14">
							<a href="/forumRuleDetail?forumId=${forum.id}"><img src="/images/front/shop_img9.png" width="14" height="22" /></a>
						</div>
					</div> --%>
					<div class="t_0100_27">
						<div class="tt_0100_8">
							<div class="li_rum">
								<div class="tt_0100_8 " style="border:none;">
									<div class="l_81" style="border-radius: 0px;width:10%">
										<a href="/shop/${forumArticle.userId}"><img src="${forumArticle.userAvatar}" width="40px;" /></a>
									</div>
									<div class="r_575" style="float:left;">
										<div class="t_575_1">
											<a href="/shop/${forumArticle.userId}"><nobr>${forumArticle.userName}</nobr></a>
										</div>
										<div class="t_575_1">
											<a href="/shop/${forumArticle.userId}"><nobr><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${forumArticle.createdAt}"></fmt:formatDate></nobr></a>
										</div>
									 </div>
									<div class="tt_0100_9">
										<div class="tit_frum">${forumArticle.content}</div>
									</div>
									<c:forEach var="pic" items="${forumArticle.picPathList}">
										<div  style="margin:0.5%;float: left;" >
											<a href="javascript:void(0);"><img src="${pic}" style="height:101px;" /></a> 
										</div>
									</c:forEach>
								</div>
								<div class="">
									<!-- <span class="cytopdiv1 pointer" style="margin-left: 20px;padding: 6px 14px;font-size: 12px;line-height: 1.5;border-radius: 3px;" onclick="location.href='/forumRule'">申请论坛</span> -->
									<c:if test="${user.id != forumArticle.userId && empty forumArticle.overCollect}">
										<span class="cytopdiv2 pointer collect" onclick="myForumArticleCollect('${forum.id}','${forumArticle.id}');return false;">收藏</span>
									</c:if>
									<c:if test="${!empty forumArticle.overCollect}">
										<span class="cytopdiv2 pointer collect" onclick="cancelForumArticleCollect(${forumArticle.overCollect});return false;">取消收藏</span>
									</c:if>
								</div>
							</div>
						</div>
						<div id="commentList" class="c_0100_3" style="display:none;">
						</div>
						<div id="resultTip" class="resultTip frontHide"></div>
						<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<div class="t_0100_28" style="position: fixed;bottom: 0;max-width: 640px;min-width: 320px;">
       	<div class="pf_romretu" ><a href="/forumArticle/${forum.id}" >返回</a></div>
       	<div class="m_500 pointer" ><a href="javascript:;" onclick="reply('${forumArticle.id}','','','');return false;" class="a_sbumit">回复</a></div>
        <div class="pf_point" ><a  href="javascript:;" onclick="showActionSheet()">···</a></div>
    </div>
    <div class="zhezhao frontHide"></div>
    <div class="pfreturnforum frontHide" id="share" style="top:0%;margin-top:0px;" onclick="hideShare();return false;">
    	<div class="share">
		 	 喜欢这个帖子，就点击右上角图标分享吧
		</div>
    </div>
	<%-- <div class="pfreturnforum frontHide" id="pfreturnforum">
	    <div class="m_90">
	    	<div class="c_648">
	        	<div class="li_ret"><a href="/forums" >论坛首页</a></div>
	        	<div class="li_ret"><a href="/userInfo" >个人信息</a></div>
	        	<div class="li_ret"><a href="/fc/${forum.id}" >我的收藏</a></div>
	            <div class="li_ret" style="border:0;"><a href="javascript:;" onclick="alertauto()">关闭菜单</a></div>
	    	</div>
	    </div>
	</div> --%>
	<div id="actionSheet_wrap">
        <div class="weui_mask_transition" id="myMask" style="display: none;"></div>
        <div class="weui_actionsheet" id="weui_actionsheet">
            <div class="weui_actionsheet_menu">
                <div class="weui_actionsheet_cell pointer" onclick="location.href='/forums'">论坛首页</div>
                <div class="weui_actionsheet_cell pointer" onclick="location.href='/userInfo'">个人信息</div>
                <div class="weui_actionsheet_cell pointer" onclick="location.href='/fc/${forum.id}'">我的收藏</div>
            </div>
            <div class="weui_actionsheet_action">
                <div class="weui_actionsheet_cell pointer" id="actionsheet_cancel">关闭菜单</div>
            </div>
        </div>
    </div>	
	<!-- <div class="reply pfreturnforum frontHide">
	    <div class="m_90">
	    	<div class="c_648" style="height: 150px;">
	    		<input type="hidden" id="objectId" />
	    		<input type="hidden" id="parentId" />
	    		<input type="hidden" id="receiverId" />
	    		<input type="hidden" id="receiverName" />
	    		<div style="width:100%;text-align: center;"><span id="tips" style="color:red;"></span></div>
	        	<div class="li_ret" style="border-bottom:none;"><textarea id="content" style="width:95%;margin:10px;"></textarea></div>
	        	<div id="dialog_btn">
					<a id="dialog_no" onclick="cancel();return false;" class="button pdR10 pointer">取消</a>
					<a id="dialog_yes" onclick="addComment();return false;" class="button pointer" style="">确定</a>
				</div>
	    	</div>
	    </div>
	</div> -->
	<div class="weui_dialog_confirm" id="dialog" style="display: none;">
        <div class="weui_mask"></div>
        <div class="weui_dialog">
        	<input type="hidden" id="objectId" />
    		<input type="hidden" id="parentId" />
    		<input type="hidden" id="receiverId" />
    		<input type="hidden" id="receiverName" />
    		<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>
            <div class="weui_dialog_bd">
                <div class="weui_cell_bd weui_cell_primary">
                    <textarea class="weui_textarea" placeholder="请输入内容" rows="3" id="content"></textarea>
                    <div class="weui_textarea_counter"><span>0</span>/200</div>
                </div>
	        </div>
            <div class="weui_dialog_ft">
                <a href="javascript:;" onclick="cancel();return false;" class="weui_btn_dialog default">取消</a>
                <a href="javascript:;" onclick="addComment();return false;" class="weui_btn_dialog primary">确定</a>
            </div>
        </div>
    </div>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/util/util.js" title="v"></script>
	<script src="/js/front/forum/forumArticleDetail.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<table class="pointer" width="100%" border="0" cellspacing="0" cellpadding="0" >
			{{^parentId}}			
				<tr>
					<td width="30"><img src="{{userAvatar}}" width="20"  /><span>{{userName}}：{{content}}</span></td>
				</tr>
				<tr onclick="reply('{{objectId}}','{{id}}','{{userId}}','{{userName}}');return false;">
					<td class="span_11 fr"><img src="/images/front/replay.png" style="height:16px;"/>&nbsp;&nbsp;<span>回复</span></td>
				</tr>
			{{/parentId}}
			{{#parentId}}
				<tr>
					<td width="30"><img src="{{receiverAvatar}}" width="20"  /><span>{{receiverName}}&nbsp;回复:&nbsp;{{userName}}</span>{{content}}</td>
				</tr>
				<tr onclick="reply('{{objectId}}','{{id}}','{{receiverId}}','{{receiverName}}');return false;">
					<td class="span_11 fr"><img src="/images/front/replay.png" style="height:16px;"/>&nbsp;&nbsp;<span >回复</span></td>
				</tr>
			{{/parentId}}
		</table>
		<br/>
	{{/rows}}		 
	</script>
</body>
</html>