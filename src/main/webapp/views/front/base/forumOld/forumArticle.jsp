<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<div class="c_0100_22">
		<div class="t_0100_24_box">
			<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
			<div class="new_list_down">
				<img src="/images/front/forum_img15.png" width="31" height="13" />
			</div>
		</div>
		<div class="t_0100_25 bodyWidth" style="left: inherit;z-index: 770;">
			<div class="l_117">
				<img src="${forum.logo}" width="100%" />
			</div>
			<div class="r_537">
				<div class="t_537_1">
					<a href="javascript:void(0);" style="text-decoration: none;">${forum.name}</a>
				</div>
				<div class="t_537_2">话题&nbsp;${count}&nbsp;|&nbsp;访问&nbsp;${forum.visitNum}</div>
			</div>
		</div>
		<div class="slide_forum" id="slide_forum">
			<div class="hd">
				<ul>
					<li class="on"><a style="padding:8px 0;">首页</a></li>
					<li><a style="padding:8px 0;" href="javascript:lh.jumpR('/forumRuleAndAnnouncementList/${forum.id}');">公告</a></li>
					<li><a style="padding:8px 0;" href="javascript:lh.jumpR('/shop/${forum.forumUserId}');">店铺</a></li>
					<li><a style="padding:8px 0;" href="javascript:lh.jumpR('/auctions?instId=${forum.instId}');">拍场</a></li>
				</ul>
			</div>
			<div class="bd">
				<!--切换循环forum_show-->
				<div class="forum_show">
						<c:if test="${!empty forum.ruleDesc}">
							<div class="t_0100_26">
									<div class="l_590">
										<!--公告开始-->
											<div class="slide_fortxt" id="slide_fortxt">
												<div class="inbd">
													<ul>
														<li><marquee id="mq" behavior="scroll" scrollamount="3" style="width: 100%;">
																<a href="javascript:;"><nobr>公告：${forum.name}论坛规章</nobr></a>
															</marquee></li>
													</ul>
												</div>
												<div class="inhd">
													<ul></ul>
												</div>
											</div>
										<!--公告结束-->
									</div>
									<div class="r_14">
										<a href="javascript:lh.jumpR('/forumRuleDetail?forumId=${forum.id}');"><img src="/images/front/shop_img9.png" width="14" height="22" /></a>
									</div>
							</div>
						</c:if>
					<div class="t_0100_27" id="forumArticle">
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>

	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="t_0100_28" style="position: fixed;bottom: 0;max-width: 640px;min-width: 320px;">
       	<div class="pf_romretu" ><a href="javascript:lh.jumpR('/forum');>" >返回</a></div>
       	<div class="m_500 pointer" ><a href="javascript:lh.jumpR('/addForumArticle/${forum.id}');" class="a_sbumit"><img src="/images/front/forum_img12.png" width="25" height="25" />&nbsp;发帖</a></div>
        <div class="pf_point" ><a  href="javascript:;" onclick="showActionSheet()">···</a></div>
    </div>
    <%-- <div class="zhezhao frontHide"></div>
	<div class="pfreturnforum frontHide" id="pfreturnforum">
	    <div class="m_90">
	    	<div class="c_648">
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/forum');" onclick="alertauto()">论坛首页</a></div>
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/userInfo');" >个人信息</a></div>
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/fc/${forum.id}');" >我的收藏</a></div>
	            <div class="li_ret" style="border:0;"><a href="javascript:;" onclick="alertauto()">关闭菜单</a></div>
	    	</div>
	    </div>
	</div> --%>
	<div id="actionSheet_wrap">
        <div class="weui_mask_transition" id="myMask" style="display: none;"></div>
        <div class="weui_actionsheet" id="weui_actionsheet">
            <div class="weui_actionsheet_menu">
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/forums');">论坛首页</div>
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/userInfo');">个人信息</div>
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/fc/${typeId}');'">我的收藏</div>
            </div>
            <div class="weui_actionsheet_action">
                <div class="weui_actionsheet_cell pointer" id="actionsheet_cancel">关闭菜单</div>
            </div>
        </div>
    </div>
	<!-- <div class="reply pfreturnforum frontHide">
	    <div class="m_90">
	    	<div class="c_648" style="height: 150px;">
	    		<input type="hidden" id="objectId" />
	    		<input type="hidden" id="parentId" />
	    		<input type="hidden" id="receiverId" />
	    		<input type="hidden" id="receiverName" />
	    		<div style="width:100%;text-align: center;"><span id="tips" style="color:red;"></span></div>
	        	<div class="li_ret" style="border-bottom:none;"><textarea id="content" style="width:95%;margin:10px;"></textarea></div>
	        	<div class="li_ret" style="border-bottom:none;">
	        		<a class="pointer" style="width:49%;background-color: #f5f5f5;" >取消</a>
	        		<a class="pointer" style="width:49%;background-color: #f5f5f5;" >确定</a>
	        	</div>
	        	<div id="dialog_btn">
					<a id="dialog_no" onclick="cancel();return false;" class="button pdR10 pointer">取消</a>
					<a id="dialog_yes" onclick="addComment();return false;" class="button pointer" style="">确定</a>
				</div>
	    	</div>
	    </div>
	</div> -->
	<div class="weui_dialog_confirm" id="dialog" style="display: none;">
        <div class="weui_mask"></div>
        <div class="weui_dialog">
        	<input type="hidden" id="objectId" />
    		<input type="hidden" id="parentId" />
    		<input type="hidden" id="receiverId" />
    		<input type="hidden" id="receiverName" />
    		<div class="weui_dialog_hd"><strong class="weui_dialog_title"></strong></div>
            <div class="weui_dialog_bd">
                <div class="weui_cell_bd weui_cell_primary">
                    <textarea class="weui_textarea" placeholder="请输入内容" rows="3" id="content"></textarea>
                    <div class="weui_textarea_counter"><span>0</span>/200</div>
                </div>
	        </div>
            <div class="weui_dialog_ft">
                <a href="javascript:;" onclick="cancel();return false;" class="weui_btn_dialog default">取消</a>
                <a href="javascript:;" onclick="addComment();return false;" class="weui_btn_dialog primary">确定</a>
            </div>
        </div>
    </div>
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	
	<input type="hidden" id="typeId" value="${typeId}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
		<!-- 图片展示 开始 -->
	<%-- <div id="imagePreview" onclick="closeImgView();" class="yipu-device" style="height:0px;">
		<div class="pre_title">
			<p class="pre_subhead"></p>
			<p class="pre_subhead"></p>
		</div>
		<span id="pre_closepic" style="color: #6CB4E5; position: absolute; top: 10px; z-index: 9999; right: 10px; cursor: pointer;">关闭</span>
		<div id="pre_container" class="swiper-container" style="height:100%;width:100%;">
			<div class="swiper-wrapper">
				<c:forEach var="pic" items="${GoodsPictureList}">
				 	<div class="swiper-slide yipu-middle" style="text-align: center;"><img src="${pic.picPath}"/></div>
				</c:forEach>
		    </div>
		    <!-- 如果需要分页器 -->
		    <div class="swiper-pagination"></div>
    		<!-- 如果需要滚动条 -->
    		<div class="swiper-scrollbar"></div>
		</div>
	</div> --%>
	<!-- 图片展示 结束 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/util/util.js" title="v"></script>
	<script src="/js/front/forum/forumArticle.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		{{^parentId}}
			<table class="pointer" style="margin: 5px;" width="100%" border="0" cellspacing="0" cellpadding="0" onclick="reply('{{objectId}}','{{id}}','{{userId}}','{{userName}}');return false;">
				<tr>
					<td width="30"><img src="{{userAvatar}}" width="20" height="20" /><span>{{userName}}：</span>&nbsp;{{content}}</td>
				</tr>
			</table>
		{{/parentId}}
		{{#parentId}}
		  <table class="pointer" style="margin: 5px;" width="100%" border="0" cellspacing="0" cellpadding="0" onclick="reply('{{objectId}}','{{id}}','{{receiverId}}','{{receiverName}}');return false;">
			<tr>
				<td width="30"><img src="{{receiverAvatar}}" width="20" height="20" /><span>{{receiverName}}&nbsp;回复&nbsp;{{userName}}</span>&nbsp;:&nbsp;{{content}}</td>
			</tr>
		   </table>
		{{/parentId}}
	{{/rows}}		 
	</script>
	<script id="template1" type="x-tmpl-mustache">
	{{#rows}}
		<div class="li_rum" style="box-shadow: 2px 2px 2px #E0E0E0;margin:5px 0 10px 0;">
			<div class="tt_0100_8" style="padding:5px 3px;">
				<div class="r_575" style="width: 100%;">
				<div style="float:left;width:auto;display:inline-block;">
					<a href="javascript:lh.jumpR('/shop/{{userId}}');"><img src="{{userAvatar}}" width="100%" style="width: 40px;" /></a>
				</div>
					<div class="t_575_1" style="display:inline-block;width:auto;position: relative;top: 0px;padding-left:5px;">
						<a href="javascript:lh.jumpR('/shop/{{userId}}');"><nobr>{{userName}}</nobr></a>
								{{#compareNow}}
									<span class="span_9" style="font-size:12px;color:gray;">{{compareNow}}</span>
								{{/compareNow}}
					</div>
					<div class="t_575_2" style="display:inline-block;width:auto;position: relative;top: 8px;padding-left:5px;float:right;padding-right:10px;">
								{{#shopId}}
									<span class="span_9 pointer" style="color: #4585D2;" onclick="lh.jumpR('/shop/{{userId}}');">店铺：{{shopName}}</span>
								{{/shopId}}
								{{^shopId}}
									<span class="span_9 pointer" style="color: #4585D2;">店铺：未开设店铺</span>
								{{/shopId}}
					</div>
				</div>
			</div>
			<div class="tt_0100_9" style="padding:5px 3px;">
				<div class="tit_frum pointer" style="color: #428bca;" onclick="lh.jumpR('/fa/{{id}}');">{{content}}</div>
			</div>
			<div class="slide_cp clear-both" id="slide_cp_1" style="margin: auto;width:99%">
				<div class="obd" style="margin-bottom:2%">
					<ul class="ul_gg" id="forumArticleImg{{id}}">
						{{#picPathList}}
							<div class="div_gg" style="margin:0.5% 0.5% 1% 1%;overflow:hidden;  width: initial;" >
								<a href="javascript:void(0);"><img src="{{.}}" height="101px" /></a> 
							</div>
						{{/picPathList}}
					</ul>
				<div class="ohd">
					<ul></ul>
				</div>
			</div>
			<div class="tt_0100_10" style="padding:0px;border: none;float: right;width: 70%;">
				<div class="li_fohome pointer" onclick="addPraise('{{id}}');return false;">
					<div class="img_fhom">
						<a href="javascript:;" style="color: rgb(137, 171, 210);"  class="ombg1"></a>
					</div>
					{{^praiseNum}}
						<span class="tit_fhom">赞</span>
					{{/praiseNum}}
					{{#praiseNum}}
						<span class="tit_fhom" id="praiseNum{{id}}">{{praiseNum}}</span>
					{{/praiseNum}}
				</div>
				<div class="li_fohome share pointer" onclick="lh.jumpR('/fa/{{id}}?share=share');">
					<div class="img_fhom">
						<div class="bshare-custom">
						<div class="bsPromo bsPromo2"></div>
							<a title="更多平台" href="javascript:;"  class="bshare-more"></a>
						</div>
					</div>
					<span class="tit_fhom">分享</span>
				</div>
				<div class="li_fohome pointer" onclick="reply('{{id}}','','','');return false;">
					<div class="img_fhom">
						<a href="javascript:;" class="ombg2"></a>
					</div>
					<span class="tit_fhom">回复</span>
				</div>
			</div>
			<div class="tt_0100_13" style="padding: 0 3px 5px 3px;">
				<div class="li_hf" id="comment{{id}}">
					<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="30"><img src="${comment.userAvatar}" width="20" height="20" /></td>
							<td class="span_11"><span>${comment.userName}：</span>${comment.content}</td>
						</tr>
					</table> --%>
					<div class="tt_0100_14" id="commentMore{{id}}" style="border-bottom: none;display:none;">
						<a href="javascript:lh.jumpR('/fa/{{id}}');" style="text-decoration: none;border-top: none;">查看更多回复</a>
					</div>
				</div>
				<!-- <div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div> -->
			</div>
		</div>
	</div>	
	{{/rows}}		 
	</script>
</body>
</html>