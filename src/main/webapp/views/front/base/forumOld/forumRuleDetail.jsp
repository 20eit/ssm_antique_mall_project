<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<div class="c_0100_22">
		<div class="t_0100_24_box">
			<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
			<div class="new_list_down">
				<img src="/images/front/forum_img15.png" width="31" height="13" />
			</div>
		</div>
		<div class="t_0100_25" style="z-index: 770;">
			<div class="l_117">
				<img src="${forum.logo}" width="100%" />
			</div>
			<div class="r_537">
				<div class="t_537_1">
					<a href="javascript:void(0);">${forum.name}</a>
				</div>
				<div class="t_537_2">话题&nbsp;${count}&nbsp;|&nbsp;访问&nbsp;${forum.visitNum}</div>
			</div>
		</div>
		<div class="slide_forum" id="slide_forum">
			<div class="hd">
				<ul>
					<li ><a style="padding:8px 0;" href="/forumArticle/${forum.id}">首页</a></li>
					<li class="on"><a style="padding:8px 0;">公告</a></li>
					<li><a style="padding:8px 0;" href="/sale/${forum.forumUserId}">店铺</a></li>
					<li><a style="padding:8px 0;" href="/auctions?instId=${forum.instId}">拍场</a></li>
				</ul>
			</div>
			<div class="bd">
				<div class="forum_show">
					<div class="t_0100_27">
						<div class="li_rum" style="margin:5px 0 10px 0;">
							<div class="tt_0100_9">
								<c:if test="${!empty forum.ruleDesc}">
									<div class="text_frum" style="height: auto;">${forum.ruleDesc}</div>
								</c:if>
								<c:if test="${!empty announcement.content}">
									<div class="text_frum" style="height: auto;">${announcement.content}</div>
								</c:if>
								<c:if test="${empty forum.ruleDesc}">
									<div class="text_frum" style="height: auto;">暂无论坛公告内容</div>
								</c:if>
								<%-- <c:if test="${empty announcement.content}">
									<div class="text_frum" style="height: auto;">暂无论坛公告内容</div>
								</c:if> --%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script src="/js/front/common.js" title="v"></script>
</body>
</html>