<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
<input type="hidden" id="forumId" value="${forumId}">
<input type="hidden" id="userId" value="${user.id}">
	<div class="c_0100_22">
		<div class="t_0100_24_box">
			<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
			<div class="new_list_down">
				<img src="/images/front/forum_img15.png" width="31" height="13" />
			</div>
		</div>
		<div class="t_0100_25 bodyWidth" style="left: inherit;z-index: 770;">
			<div class="l_117">
				<img src="${forum.logo}" width="100%" />
			</div>
			<div class="r_537">
				<div class="t_537_1">
					<a href="javascript:void(0);" style="text-decoration: none;">${forum.name}</a>
				</div>
				<div class="t_537_2">话题&nbsp;${count}&nbsp;|&nbsp;访问&nbsp;${forum.visitNum}</div>
			</div>
		</div>
		<div class="slide_forum" id="slide_forum">
			<div class="bd">
				<!--切换循环forum_show-->
				<div class="forum_show">
					<span class="c_0100_3" style="font-size:16px;border-bottom: #d9d7d7 solid 1px;padding:10px;">我收藏的帖子</span>
					<div class="t_0100_27" id="myForumArticleCollectList">
						<div class="teacher_list  c_0100_3" id="notMyCollect" style="margin:0px;display:none;padding:10px;">
							<span style="font-size:16px;">暂无收藏的帖子</span>
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>

	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/util/util.js" title="v"></script>
	<script src="/js/front/forum/myForumArticleCollect.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="teacher_list  c_0100_3" style="margin:0px;">
			<div class="l_60 pointer" onclick="javascript:location.href='/shop/{{collectedUserId}}'">
				<img src="{{userAvatar}}" width="100%" />
			</div>
			<div style="position: relative;top: 10px;padding-left:50px;" class="pointer" onclick="javascript:location.href='/shop/{{collectedUserId}}'">
				<span  style="margin-right:5px;">{{userName}}</span>
				<span  style="margin-right:5px;">{{forumArticleCreatedAt}}</span>
			</div>			
			<div class="r_595 pointer " onclick="javascript:location.href='/fa/{{articleId}}'">
				{{forumArticleTitle}}
			</div>
			<div class="r_gz">
				<a href="javascript:;" onclick="cancelForumArticleCollect('{{id}}');return false;" class="a_gz pointer" >取消收藏</a>
			</div>
		</div>
	{{/rows}}		 
	</script>
</body>
</html>