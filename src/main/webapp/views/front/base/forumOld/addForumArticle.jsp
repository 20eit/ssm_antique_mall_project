<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<style type="text/css">
	a:hover{color:white;}
</style>
</head>
<body style="background-color: #f5f5f5;">
	
	  <div class="page c_0100_3 information_border" >
	  		<input type="hidden" value="${typeId}" id="typeId">
	  		<div id="tips"  style="color:red;"></div>
			<!-- 标题:<input type="text" id="title" style="margin:15px;width:90%"/> -->
	  		<!-- <textarea id="content" placeholder="说两句吧..." style="width:100%;height:80px;"></textarea> -->
	  		<div class="weui_cells weui_cells_form">
		  		<div class="weui_cell">
	                <div class="weui_cell_bd weui_cell_primary">
	                    <textarea class="weui_textarea" placeholder="请输入内容" rows="3" id="content"></textarea>
	                    <div class="weui_textarea_counter"><span>0</span>/200</div>
	                </div>
	            </div>
            </div>
	  		<a id="cancel" href="javascript:;" onclick="goBack();return false;" class="weui_btn weui_btn_mini weui_btn_primary">取消</a>
	  		<a href="javascript:;" onclick="addForumArticle();return false;" class="weui_btn weui_btn_mini weui_btn_primary">发表</a>
	  		<!-- <a onclick="addForumArticle();return false;" style="margin:5px;cursor:pointer;background: -webkit-linear-gradient(top, #90B9ED, #316FB9);border: 1px solid #2C64B2;float: right;padding: 4px 15px;color: white;font-size: 14px;border-radius: 3px;">发表</a>
	  		<a onclick="goBack();return false;"style="margin:5px;cursor:pointer;background: -webkit-linear-gradient(top, #FCFCFC, #F0EFED);margin-right: 10px;border: 1px solid #C8C9C6;color: black;float: right;padding: 4px 15px;font-size: 14px;border-radius: 3px;">取消</a> -->
	  </div>
	  
	  <div class="c_0100_3 information_border" >
	  	<div class="ipt_item_div">
					<label>上传图片：</label>
					<input type="hidden" id="instLogo" value="${ap.picPaths}">
					<img src="${ap.picPaths}" class="picurl">
					<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
				    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
					<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传图片</button>
			</div>
			<div id="upload_outer_div"><!-- 上传文件进度展示 --></div>
	  </div>	
	  <div class="pz_down">
		<div class="c_0100_9"></div>
	  </div>
	  <div class="t_0100_28" style="position: fixed;bottom: 0;max-width: 640px;min-width: 320px;">
       	<div class="pf_romretu" ><a href="/forumArticle/${typeId}" >返回</a></div>
       	<div class="m_500 pointer" style="height: 40px;" ><a href="javascript:;" onclick="addForumArticle();return false;" class="a_sbumit">发表</a></div>
        <div class="pf_point" ><a  href="javascript:;" onclick="showActionSheet()" >···</a></div><!-- onclick="alertauto()" -->
      </div>
     <%--  <div class="zhezhao frontHide"></div>
	  <div class="pfreturnforum frontHide" id="pfreturnforum">
	    <div class="m_90">
	    	<div class="c_648">
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/forum');" onclick="alertauto()">论坛首页</a></div>
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/userInfo');" >个人信息</a></div>
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/fc/${typeId}');" >我的收藏</a></div>
	            <div class="li_ret" style="border:0;"><a href="javascript:;" onclick="alertauto()">关闭菜单</a></div>
	    	</div>
	    </div>
	  </div>	 --%>	
	  <div id="actionSheet_wrap">
        <div class="weui_mask_transition" id="myMask" style="display: none;"></div>
        <div class="weui_actionsheet" id="weui_actionsheet">
            <div class="weui_actionsheet_menu">
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/forum');">论坛首页</div>
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/userInfo');">个人信息</div>
                <div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/fc/${typeId}');">我的收藏</div>
            </div>
            <div class="weui_actionsheet_action">
                <div class="weui_actionsheet_cell pointer" id="actionsheet_cancel">关闭菜单</div>
            </div>
        </div>
    </div>	
	  <input type="hidden" value="${r}" id="r"/> 	
	  <input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/front/forum/addForumArticle.js" title="v"></script>
	<script type="text/javascript">


	</script>
</body>
</html>
