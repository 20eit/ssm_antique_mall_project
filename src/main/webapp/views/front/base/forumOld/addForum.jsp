<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
<style type="text/css">
	.c_0100_3 li{
		float: none;
  		width: 100%;
	}
</style>
</head>
<body style="background-color: #f5f5f5;">
	
	  <div class="page c_0100_3 information_border" >
	  		<div id="tips"  style="color:red;"></div>
	  		<div class="ipt_item_div">
					<label>上传论坛logo：</label>
					<input type="hidden" id="instLogo" value="${ap.picPaths}">
					<img src="${ap.picPaths}" class="picurl">
					<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
				    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
					<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传论坛logo</button>
			</div>
			<div id="upload_outer_div"><!-- 上传文件进度展示 --></div>
			 <!-- 论坛名称:<input type="text" id="name" style="margin:15px;width:65%"/> -->
			<div class="weui_cells weui_cells_form" style="margin-top:50px;">
				 <div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">论坛名称:</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                    <input class="weui_input" type="text" id="name" placeholder="请输入论坛名称">
	                </div>
	            </div>
			<%--  论坛类型:<select id="type" style="margin:15px;width:30%">
			 		<option value="">请选择</option>
				 <c:forEach var="forum" items="${dictForumList}">
				 	<option value="${forum.id}">${forum.dictName}</option>
				 </c:forEach>
			 </select> --%>
			 <div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">论坛类型:</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                    <select id="type" class="selectpicker">
							<c:forEach var="forum" items="${dictForumList}">
							 	<option value="${forum.id}">${forum.dictName}</option>
							 </c:forEach>
						</select> 
	                </div>
	          </div>
			 <!-- <textarea id="message" placeholder="其他附加说明..." style="width:100%;height:80px;"></textarea> -->
			  <div class="weui_cell">
                <div class="weui_cell_bd weui_cell_primary">
                    <textarea class="weui_textarea" placeholder="请输入其他附加说明..." rows="3" id="message"></textarea>
                    <div class="weui_textarea_counter"><span>0</span>/200</div>
                </div>
            </div>
	  		<a onclick="addForum();return false;" style="margin: 5px;cursor:pointer;background: -webkit-linear-gradient(top, #90B9ED, #316FB9);border: 1px solid #2C64B2;float: right;padding: 4px 15px;color: white;font-size: 14px;border-radius: 3px;">提交申请</a>
	  		<a onclick="goBack();return false;"style="margin: 5px;cursor:pointer;background: -webkit-linear-gradient(top, #FCFCFC, #F0EFED);margin-right: 10px;border: 1px solid #C8C9C6;color: black;float: right;padding: 4px 15px;font-size: 14px;border-radius: 3px;">取消</a>
	  		</div>
	  </div>
	  <div class="pz_down">
		<div class="c_0100_9"></div>
	  </div>
	  <div class="t_0100_28" style="position: fixed;bottom: 0;max-width: 640px;min-width: 320px;">
       	<div class="pf_romretu" ><a href="javascript:lh.jumpR('/forums');" >返回</a></div>
       	<div class="m_500 pointer" style="height: 40px;" ><a href="javascript:;" class="a_sbumit"></a></div>
        <div class="pf_point" ><a  href="javascript:;" onclick="alertauto()">···</a></div>
      </div>
      <input type="hidden" value="${r}" id="r"/> 	
      <input type="hidden" value="${loginStatus}" id="loginStatus"/>
      <div class="zhezhao frontHide"></div>
	<div class="pfreturnforum frontHide" id="pfreturnforum">
	    <div class="m_90">
	    	<div class="c_648">
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/forums');" onclick="alertauto()">论坛首页</a></div>
	        	<div class="li_ret"><a href="javascript:lh.jumpR('/userInfo');">个人信息</a></div>
	        	<%-- <div class="li_ret"><a href="javascript:lh.jumpR('/fc/${forum.id}');" >我的收藏</a></div> --%>
	            <div class="li_ret" style="border:0;"><a href="javascript:;" onclick="alertauto()">关闭菜单</a></div>
	    	</div>
	    </div>
	</div>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/front/forum/addForum.js" title="v"></script>
	<script type="text/javascript">

	</script>
</body>
</html>
