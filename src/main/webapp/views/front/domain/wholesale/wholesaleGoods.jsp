<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<input type="hidden" id="shopId" value="${wholesale.id}">
	<input type="hidden" id="userId" value="${userId}">
	<div class="pz_main">
		<div class="c_0100_16">
			<div class="pf_return_home">
				<span class="pointer" style="color:white;font-size: 18px;" onclick="history.back(-1);return false;">返回</span>
				<div class="threepoint" style="margin:0"><!-- threepoint -->
					<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
					<div class="pf_point" style="display: none;">
						<div class="img_point">
							<img src="/images/front/top_img2.png" width="8" height="7" />
						</div>
						<div class="ulist_point">
							<c:if test="${userId == currentUserId}">
								<div class="ist_point">
									<a href="javascript:lh.jumpR('/goodsManageType');">产品管理</a>
								</div>
							</c:if>
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/creditShop/${shop.id}');">店铺信誉</a>
							</div>
							<c:if test="${userId == currentUserId}">
								<c:if test="${!empty wholesale}">
									<div class="ist_point">
										<a href="javascript:lh.jumpR('/editWholesale/${wholesale.id}');">批发城设置</a>
									</div>
								</c:if>
							</c:if>
							<c:if test="${userId == currentUserId}">
								<c:if test="${!empty forumMember}">
									<div class="ist_point">
										<a href="javascript:lh.jumpR('/forumArticle/${forumMember.forumId}');">我的论坛</a>
									</div>
								</c:if>
							</c:if>
							<c:if test="${userId != currentUserId}">
								<c:if test="${!empty forumMember}">
									<div class="ist_point">
										<a href="javascript:lh.jumpR('/forumArticle/${forumMember.forumId}');">他的论坛</a>
									</div>
								</c:if>
							</c:if>
						</div>
					</div>
				</div>
				<div class="ewm" onclick="showQRCode('show', '${url}', '${shop.logo}');">
					<a href="javascript:void(0);"><img src="/images/front/shop_img6.png" width="23" height="23" /></a>
				</div>
				<div class="home_pic">
					<a href="javascript:lh.jumpR('/');"><img src="/images/front/shop_img7.png" width="27" height="23" /></a>
				</div>
			</div>
			<div class="t_0100_10">
				<div class="m_100">
					<a href="javascript:void(0);" class="a_people"><img src="${shop.logo}" width="80" height="80" /></a>
				</div>
				<div class="tt_0100_1">
					<a href="javascript:void(0);">${shop.name}</a>
					<c:if test="${userId == currentUserId}">
						<a href="javascript:lh.jumpR('/editShop');">修改</a>
					</c:if>
				</div>
				<div class="tt_0100_2">
					<a href="javascript:void(0);">关注</a>&nbsp;${focusMeCount}<a href="javascript:void(0);">&nbsp;|&nbsp;粉丝&nbsp;${meFocusCount}</a>&nbsp;|&nbsp;<a href="javascript:lh.jumpR('/shopComment/${userId}');">评价&nbsp;${commentCount}</a>
				</div>
				<div class="tt_0100_3">
					<c:if test="${shop.isSevenReturn == 2}">
						<img src="/images/front/shop_img3.png" width="24" height="24" />&nbsp;七天退换&nbsp;&nbsp;&nbsp;
					</c:if>
					<c:if test="${shop.isRealAuth == 2}">
						<img src="/images/front/shop_img4.png" width="24" height="24" />&nbsp;&nbsp;实名认证&nbsp;&nbsp;&nbsp;
					</c:if>
					<c:if test="${shop.isWarrant == 2}">
						<img src="/images/front/shop_img5.png" width="24" height="24" />&nbsp;担保交易
					</c:if>
					<c:if test="${shop.creditMargin > 0}">
						<img src="/images/front/shop_img4.png" width="24" height="24" />&nbsp;已缴纳信誉保证金:${shop.creditMargin}元
						<c:if test="${userId == currentUserId}">
							<button id="add_38" class="weui_btn weui_btn_mini weui_btn_primary  pointer" style="margin:2px;color: rgb(255, 255, 255);" onclick="showCreditMarginWin(${shop.id})">增加保证金</button>
						</c:if>
					</c:if>
					<c:if test="${shop.creditMargin <= 0}">
						<img src="/images/front/shop_img4.png" width="24" height="24" />&nbsp;未缴纳信誉保证金
						<c:if test="${userId == currentUserId}">
							<button id="add_38" class="weui_btn weui_btn_mini weui_btn_primary  pointer" style="margin:2px;color: rgb(255, 255, 255);" onclick="showCreditMarginWin(${shop.id})">增加保证金</button>
						</c:if>
					</c:if>
				</div>
			</div>
		</div>
		<div class="c_0100_17">
			<div class="slide_shop" id="slide_shop">
				<div class="hd">
					<ul>
						<c:if test="${empty wholesale}">
							<li class="pointer" onclick="lh.jumpR('/shop/${userId}');">主页</li>
							<li class="pointer" onclick="lh.jumpR('/sale/${userId}');">在售</li>
							<li class="on pointer" onclick="lh.jumpR('/like/${userId}');">喜欢</li>
						</c:if>
						<c:if test="${!empty wholesale}">
							<li class="pointer" style="width:25%" onclick="lh.jumpR('shop/${userId}');">主页</li>
							<li class="pointer" style="width:25%" onclick="lh.jumpR('/sale/${userId}');">在售</li>
							<li class="pointer" style="width:25%" onclick="lh.jumpR('/like/${userId}');">喜欢</li>
							<li class="on pointer" style="width:25%" onclick="lh.jumpR('/ws/${userId}');">批发</li>
						</c:if>
					</ul>
					<div class="bd">
						<div class="shop_show ">
							<div class="t_0100_11">
									<div class="title_3">
										<div class="tit_3">基本信息</div>
									</div>
									<div class="t_0100_12 botline">
										<div class="l_110" style="width:20%">联系电话</div>
										<div class="r_568" style="width:80%">
											<span  class="bfom5">${wholesale.tel}</span> 
										</div>
									</div>
									<div class="t_0100_12 botline">
										<div class="l_110" style="width:20%">店铺介绍</div>
										<div class="r_568" style="width:80%">
											<!-- <span  class="bfom5"></span>  -->
											<span class="weui_textarea" onclick="frontBaseAlert('${wholesale.description}')">${wholesale.description}</span>
										</div>
									</div>
									<div class="t_0100_12">
										<div class="l_110" style="width:20%">店铺地址</div>
										<div class="r_568" style="width:80%">
											<span  class="bfom5">${wholesale.address}</span>
										</div>
									</div>
							</div>
							<div class="t_0100_11">
								<div style="border-bottom: 1px solid #CDCDCD; background: white;">
									<div style="padding: 0 5px;">
										<ul id="wholesaleSale" class="mainul mainul1" style="width:100%">
											<%-- <c:forEach items="${goodsList}" var="goods" step="2">
												<li onclick="location.href='/professionGoodsDetail?goodsId=${goods.id}&auctionId=1'" class="mainulli goods_bg"><span
													style="background: url(${goods.picPath}) center center no-repeat #F2F2F2; background-position: 50% 50%; background-size: contain;"></span>
													<h3 style="color: #838381;">编号.${goods.goodsSn}</h3></li>
											</c:forEach> --%>
										</ul>
									</div>
								</div>
								<div id="resultTip" class="resultTip frontHide"></div>
								<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<div id="actionSheet_wrap" >
        <div class="weui_mask_transition" id="myMask" style="display:none;"></div>
        <div class="weui_actionsheet" id="weui_actionsheet">
            <div class="weui_actionsheet_menu">
            	<input type="hidden" id="goodsId">
            	<c:if test="${!empty hasWholesale}">
                	<div class="weui_actionsheet_cell pointer" onclick="towholesale()">批发城</div>
                </c:if>
            	<c:if test="${empty hasWholesale}">
                	<div class="weui_actionsheet_cell pointer" onclick="lh.jumpR('/applyWholesale');">申请开通批发城</div>
                </c:if>
                <c:if test="${!empty hasShop}">
                	<div class="weui_actionsheet_cell pointer" onclick="toShop()">店铺</div>
                </c:if>
                <c:if test="${empty hasShop}">
                	<div class="weui_actionsheet_cell pointer" onclick="">申请开通店铺</div>
                </c:if>
            </div>
            <div class="weui_actionsheet_action">
                <div class="weui_actionsheet_cell pointer" id="actionsheet_cancel">取消</div>
            </div>
        </div>
    </div>
    
    <div class="weui_dialog_confirm" id="creditMarginShow" style="display:none;">
        <div class="weui_mask"></div>
        <div class="weui_dialog">
        	<input type="hidden" id="shopId" >
            <div class="weui_dialog_hd"><strong class="weui_dialog_title">请输入需要增加的保证金</strong></div>
            <div class="weui_dialog_bd">
            	<div style="display:none;color:red;" id="tip"></div>
            	<div class="weui_cell_bd weui_cell_primary">
            		  <label >保证金:</label>
            		  <input class="weui_input" style="width:80%" type="text" id="creditMargin"  placeholder="请输入需要增加的保证金">
                </div>
            	<div class="weui_cell_bd weui_cell_primary">
            		<label >支付密码:</label>
            		<input class="weui_input" style="width:80%" type="password" id="payPassword"  placeholder="请输入支付密码">
                </div>
            </div>
            <div class="weui_dialog_ft">
                <a href="javascript:;" onclick="closeCreditMarginWin()" class="weui_btn_dialog default">取消</a>
                <a href="javascript:;" onclick="addCreditMargin()" class="weui_btn_dialog primary">确定</a>
            </div>
        </div>
    </div>
    
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/js/front/wholesale/wholesaleGoods.js" title="v"></script>
	<script type="text/javascript" src="/js/front/shop/creditMargin.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<li style="width:45%;float:left;margin: 5px;"  class="mainulli goods_bg pointer">
				<div onclick="lh.jumpR('/wsg/{{id}}/{{shopId}}/{{wholesaleUserId}}?moduleId={{moduleId}}')">
					<span  style="background: url({{picPath}}) center center no-repeat #F2F2F2; background-position: 50% 50%; background-size: contain;"></span>
					<h3 style="color:black;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">{{goodsName}}</h3>
					{{#shopPrice}}
						<h3 style="color:red;">￥ {{shopPrice}}</h3>
					{{/shopPrice}}
					{{^shopPrice}}
						<h3 style="color:red;">议价</h3>
					{{/shopPrice}}
				</div>
				{{#moduleId}}
					<c:if test="${wholesale.goodsPrivilege == 1}">
						<c:if test="${userId != currentUserId}">
							<button  type="button" class="btn btn-info fl" onclick="copyGoods('{{id}}');return false;">复制</button>
						</c:if>
					</c:if>
				{{/moduleId}}
			</li>
		{{/rows}}		 
	</script>
</body>
</html>
