<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;margin-top:50px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	  <div class="page c_0100_3 information_border" >
	  		<div id="tips"  style="color:red;"></div>
	  		<div class="ipt_item_div">
					<label>上传批发城logo：</label>
					<input type="hidden" id="instLogo" value="${ap.picPaths}">
					<img src="${ap.picPaths}" class="picurl">
					<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
				    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
					<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传批发城logo</button>
			</div>
			<div id="upload_outer_div"><!-- 上传文件进度展示 --></div>
			 批发城名称:<input type="text" id="name" style="margin:15px;width:70%"/>
			 <textarea id="message" placeholder="其他附加说明..." style="width:100%;height:80px;"></textarea>
	  		<button onclick="addApplyWholesale();return false;" class="btn weui_btn_primary fr pointer" style="margin:5px;color: #FFFFFF;">提交</button>
	  		<button onclick="goWholesale();return false;" class="btn weui_btn_primary fr pointer" style="margin:5px;color: #FFFFFF;">取消</button>
	  </div>
	  <div class="pz_down">
		<div class="c_0100_9"></div>
	  </div>
	  <div id="offerWin" class="szweituo" style="display: none;">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<div class="szwtinput">
					<div id="offerIptDiv" class="szwtinpk">
						<form id="entrustFrom">
							<span> <input type="password" id="payPass" placeholder="请在此输入支付密码" value="">
							</span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="offerCancel" onclick="realPost('cancel')" class="szwtbutr entrusta2 pointer">取消</span> 
						<span id="offerSave" onclick="realPost('save')" class="szwtbutl entrusta1 pointer">确定</span>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wholesale/applyWholesale.js" title="v"></script>
</body>
</html>
