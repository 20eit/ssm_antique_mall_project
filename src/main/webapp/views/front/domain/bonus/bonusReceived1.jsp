<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/dust.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/mobiscroll/css/mobiscroll.custom-2.6.2.min.css"/> 
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">

			<ul class="zghongbao">
				<li><span style="background: url(${user.avatar}) center center no-repeat; background-size: cover;"></span></li>
				<li class="zghongbaoli2">${user.username}共收到</li>
				<li class="zghongbaoli3">${totalBonusMoney} <strong>元</strong></li>
			</ul>
			<div class="myztconttitle" style="border-bottom: 1px solid #EEEDEB; background: #F5F5F5; padding: 8px;">
				<span style="float: left; width: 100%;"> 
					<!-- 
					<select id="ddlDay" onchange="searchBonusReceived();">
						<option value="today">今日</option>
						<option value="week">本周</option>
						<option value="month" selected="selected">本月</option>
						<option>全部</option>
					</select> -->
					<input id="startDate" value="" style="padding: 8px 0; border: none; text-align: center; width: 28%;"> 至
					<input id="endDate" value="" readonly="readonly" style="padding: 8px 0; border: none; text-align: center; width: 28%;"> 
					<button type="button" class="btn btn-success fr" style="padding:6px 35px;margin-bottom:3px;" onclick="getBonusList(true);">查询</button>
					<!-- <a href="javascript:void(0);" onclick="searchBonusReceived();" class="bonus_search">查询</a> -->
				</span>
				<div style="clear: both;"></div>
			</div>

			<div id="myOpen">
				<ul id="dataUl" class="" rows="1" onclick="jumpToDetail();">
					<!-- 数据容器  -->
					<!-- 
					<li class="receivehbli Receivedhb">
						<a>
							<span class="hblispan hblispan1">1.88元<br><strong></strong></span>
							<span>2015-10-09<br>鹭草山房的红包</span>
						</a>
					</li> -->
				</ul>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>

		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script type="text/javascript">IS_SEND = false;</script>
	<script src="/js/front/bonus/bonusList.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<li class="receivehbli Receivedhb" onclick="lh.jumpR('/bonusDetail/{{bonusId}}');">
				<a>
					<span class="hblispan hblispan1" style="line-height:40px;"><abbr style="color:#ff4000;">{{money}}</abbr> 元<br><strong></strong></span>
					<span>2015-10-09<br>{{name}}的红包</span>
				</a>
			</li>
		{{/rows}}	 		 
	</script>

</body>
</html>
