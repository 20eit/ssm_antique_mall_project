<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
  <link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
  
    <link rel="stylesheet" href="/css/front/bonus/bonus.css" title="v">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
    <!-- <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
   <script src="/js/front/bonus/index.js" title="v"></script>
    <script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
</head>
<body style="background:url(images/bonus_index_bg.png) no-repeat; background-size: 100% 100%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 bonus-head plr0">
                <div class="col-xs-2 ptb5 head-left" onclick="lh.back();">
                    <img src="/images/icon-left.png" width="22" height="21" class="img-responsive center-block">
                </div>
                <div class="col-xs-7 mt-5">
                    <span class="fs18">微信红包</span><br>
                    <span>微信安全支付</span>
                </div>
                <div class="col-xs-3 mt-5 plr0" style="line-height: 40px;">
                    <div id="dropdown open" class="dropdown">
                        <div class="dropdown-toggle fs16" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            我的红包
                        </div>
                        <ul class="dropdown-menu" style="min-width: 112px;left: auto;right: 0;" aria-labelledby="dropdownMenu1">
                            <li><a href="javascript:lh.jumpR('/bonusReceived');">收到的红包</a></li>
                            <li><a href="javascript:lh.jumpR('/bonusSend');">发出的红包</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 mt40p">
                    <a href="javascript:lh.jumpR('/randomBonus');" rel="button" class="col-xs-12 btn-lg btn btn-yellow-tint">拼手气群红包</a>
                    <a href="javascript:lh.jumpR('/commonBonus');" rel="button" class="col-xs-12 btn-lg btn btn-yellow-deep mt20">普通红包</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_word">
        可直接使用收到的零钱发红包
    </div>
    
 <%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
    <script type="text/javascript" src="/js/front/bonus/bonusIndex.js" title="v"></script> 
    
    <script>
        $(function(){
            var width = $(document).width();//当前的浏览器的宽度
            var height = $(document).height();//当前浏览器的高度
            $(".container-fluid").height(height);
        });
    </script>
</body>
</html>