<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
  <link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
  
    <link rel="stylesheet" href="/css/front/bonus/bonus.css" title="v">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
   <!--  <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
   <script src="/js/front/bonus/index.js" title="v"></script>
    <script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
</head>
<body style="background-color: #fffaf5;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 bonus-head plr0">
                <div class="col-xs-2 ptb5 head-left" onclick="lh.back();">
                    <img src="/images/front/icon-left.png" width="22" height="21" class="img-responsive center-block">
                </div>
                <div class="col-xs-7 mt-5">
                    <span class="fs18">拼手气群红包</span><br>
                    <span>微信安全支付</span>
                </div>
                <div class="col-xs-3 mt-5 plr0" style="line-height: 40px;">
                    <div id="dropdown" class="dropdown">
                        <div class="dropdown-toggle fs16" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            我的红包
                        </div>
                        <ul class="dropdown-menu" style="min-width: 112px;left: auto;right: 0;" aria-labelledby="dropdownMenu1">
                               <li><a href="javascript:lh.jumpR('/bonusReceived');">收到的红包</a></li>
                            <li><a href="javascript:lh.jumpR('/bonusSend');">发出的红包</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 bg-lily mt20 ptb10">
                    <div class="col-xs-4 plr0 fs18 pt3">
                        红包个数
                    </div>
                    <div class="col-xs-7 plr0"><!--  bdr -->
                        <input type="number" id="bonusCount"  class="form-control col-xs-12 bdn text-right" name="txtCount" placeholder="" onchange="checkCount();">
                    </div>
                    <div class="col-xs-1 pr0 fs18 pt3 pl7">个</div>
                </div>
                <div class="col-xs-12 bg-lily mt20 ptb10">
                    <div class="col-xs-4 plr0 fs18 pt3">
                        总金额 <img src="/images/front/pin.png" width="18" style="line-block" >
                    </div>
                    <div class="col-xs-7 plr0">
                        <input type="number" id="totalMoney" onchange="calculateTotalMoney();" class="form-control col-xs-12 bdn text-right" placeholder="">
                    </div>
                    <div class="col-xs-1 plr0 fs18 pt3">元</div>
                </div>
                
                <div class="col-xs-12 gray enveluli">每人抽到的金额随机，最大不超过200元</div>
                
                <div class="col-xs-12 bg-lily mt20 ptb10">
                    <div class="col-xs-4 plr0 fs18 pt3">
                       		 留言
                    </div>
                    <div class="col-xs-8 plr0">
                        <input type="text" class="form-control col-xs-12 bdn" id="leaveWord" placeholder="恭喜发财，大吉大利！">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 bg-lily mt20 ptb10">
                    <div class="col-xs-4 plr0 fs18 pt3">
                       		支付密码
                    </div>
                    <div class="col-xs-8 plr0">
                        <input class="form-control col-xs-12 bdn" name="payPass" type="password" id="payPass" placeholder="支付密码" need="need">
                    </div>
                </div>
        <div class="row mt20">
            <div class="col-xs-12 fs50 text-center mt20" id="tatalMoneys">
                ￥0.00
            </div>
        </div>
 	
        <div class="row mt20">
            <div class="col-xs-12 text-center mt20">
                <button type="button" onclick="addRandomBonus();" class="col-xs-12 btn btn-danger btn-lg disabled" id="btnPay">塞钱进红包</button>
            </div>
            <div class="col-xs-12 gray text-center mt20">对方可领取的红包金额为0.01~200元</div>
        </div>
    </div>
    
    <div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/bonus/bonus.js" title="v"></script>
    
    
    
    <script>
        $(function(){
            $("input").keyup(function(){
                var num = $("#num").val();
                var money = $("#money").val();
                var leaveWord = $("#leaveWord").val();
                if (num != "" && money != "" && leaveWord != "" && num != "0" && money != "0") {
                    $("#btnPay").removeClass("disabled");
                }else{
                   $("#btnPay").addClass("disabled"); 
                }
            });
        });
    </script>
</body>
</html>