<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/dust.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
		
			<div class="envelopes">
		        <div class="enveldiv">
		            <img src="/images/front/bonus.jpg" style="width:82%;">
		            <div class="envelbox" style="padding-top:20px;">
		                <a href="javascript:lh.jumpR('/randomBonus');">拼手气红包</a>
		                <a href="javascript:lh.jumpR('/commonBonus');">普通红包</a>
		            </div>
		            <div class="envelhb">
		                <a href="javascript:lh.jumpR('/bonusReceived');">收到的红包</a>|
				 		<a href="javascript:lh.jumpR('/bonusSend');">我发的红包</a>|
				 		<a href="javascript:lh.jumpR('/fans');">我的好友</a>
		            </div>
		        </div>
		        <div class="envelfoot" style="background-color:#920808">
		            <span class="enfoots1" onclick="lh.jumpR('/withdraw');" style="background-color:#5cb85c;padding:0 30px;">提现</span>
		            <!-- <button type="button" class="btn btn-success fr" style="width:14%" onclick="location.href='/withdraw'">提现</button> -->
		            <span class="enfoots2">可提现金额：<strong>${avaliableMoney}</strong> 元</span>
		        </div>
			</div>
			
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/bonus/bonus.js" title="v"></script>

</body>
</html>
