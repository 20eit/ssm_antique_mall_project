<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/dust.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">

			<div class="envelopes" style="background: #F0F0F0;">
				<div class="enveldiv1">
					<ul class="envelul">
						<li><input name="txtCount" type="number" id="bonusCount" placeholder="发多少红包（个）" need="need" onchange="checkCount();"></li>
						<li><input name="txtSum" type="number" id="totalMoney" placeholder="共发多少钱（元）" need="need" onchange="checkRandomPayTotal();"></li>
						<li class="enveluli">每人抽到的金额随机，最大不超过200元</li>
						<li><textarea name="msg" id="msg" placeholder="恭喜发财，大吉大利" style="margin: 0px; height: 72px;"></textarea></li>
						<li class="enveluli enveluli1">
							<div>支付金额：<span id="payTotal">0</span> 元</div>
							<div>发红包免费，支持系统余额、微信支付</div>
						</li>
						<li><input name="payPass" type="password" id="payPass" placeholder="支付密码" need="need"></li>
						<li id="ASubmit" onclick="addRandomBonus();" class="envelusq" style="border: 1px solid #920808;background: #920808"><a>塞钱进红包</a></li>
						<li class="enveluli enveluli1" style="text-align: center;">① 填写红包信息 - ② 系统支付 - ③ 发给好友</li>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/bonus/bonus.js" title="v"></script>

</body>
</html>
