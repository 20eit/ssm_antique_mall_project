<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
  <link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
  
    <link rel="stylesheet" href="/css/front/bonus/bonus.css" title="v">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
<!--     <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
   <script src="/js/front/bonus/index.js" title="v"></script>
    <script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
</head>
<body style="background-color: #faf6f1;">
    <div class="container-fluid" style="background:url(/images/front/bonus_bg.png) no-repeat; background-size: 100% 100%;background-position: 0 30px;">
        <div class="row">
            <div class="col-xs-12 bonus-head plr0">
                <div class="col-xs-2 ptb5 head-left" onclick="lh.back();">
                    <img src="/images/front/icon-left.png" width="22" height="21" class="img-responsive center-block">
                </div>
                <div class="col-xs-10 fs18 mt-5">
                    微信红包
                </div>
                <div class="col-xs-10">
                    微信安全支付
                </div>
            </div>
        </div>
        <div class="row fs16">
            <div class="col-xs-4 col-xs-offset-4 pt70">
                <img src="${bonus.bossAvatar}" class="img-responsive center-block">
            </div>
            <div class="col-xs-12 text-center pt10">
                <span>${bonus.bossName}的红包</span> <img src="/images/pin.png" width="18" style="line-block" >
            </div>
            <div class="col-xs-12 text-center mt20 fs26 pt20"><span class="fs50">${bonus.money}</span>元</div>
            <div class="col-xs-12 mt20 blue text-center fs14">
               		 已存入零钱，可用于发红包
            </div>
        </div>
        <div class="row ptb10 bg_gray gray">
            <div class="col-xs-12">${bonus.count}个红包共${bonus.totalMoney}元</div>
        </div>
        <div class="row">
        <div id="dataContainer" class="drawcontc">
	                    	<!-- 数据容器 -->
		                    <!-- 
		                    <div class="drawcontcdiv">
		                    	<a href="/user/{{userId}}">
			                        <span class="drawtspan1">领到<strong>0.18</strong>元<br>
			                            10月09日 20:42:52</span>
			                        <span class="drawtspan" style="background: url({{userAvatar}}); background-position: 50%,50%; background-size: 100%;"></span>
			                        <span class="drawtspan2">{{username}}<br>
			                            <strong>{{msg}}</strong>
			                        </span>
			                        <div style="clear: both;"></div>
		                        </a>
		                    </div> -->
	
	                    </div>
            <!-- <div class="col-xs-12 bg-white">
                <div class="col-xs-12 plr0 bdb">
                    <div class="col-xs-2 plr0 pt10">
                        <img src="/images/front/portrait_bonus_02.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-10 pr0 fs16 pt10">
                        <div class="col-xs-6 plr0">
                            朋友
                        </div>
                        <div class="col-xs-6 plr0 text-right">
                            0.01元
                        </div>
                    </div>
                    <div class="col-xs-10 pr0 gray pt5 pb10">
                        <div class="col-xs-6 plr0 blue">
                            <a href="#">留言</a>
                        </div>
                    </div>
                </div>
            </div> -->
            
                </div>
            </div>
        </div>
        <div class="row mt20">
            <div class="col-xs-12 text-center blue">
                <a href="#">查看我的红包记录</a>
            </div>
            <div class="col-xs-12 text-center gray ptb20">
                收到的钱可直接用来发红包
            </div>
        </div>
    </div>
    
    
     <%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
    <script src="/js/front/bonus/bonusDetail.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			 <div class="col-xs-12 bg-white" onclick="lh.jumpR('/user/{{userSerial}}');">
                <div class="col-xs-12 plr0 bdb">
                    <div class="col-xs-2 plr0 pt10">
                        <img src="{{userAvatar}}" class="img-responsive">
                    </div>
                    <div class="col-xs-10 pr0 fs16 pt10">
                        <div class="col-xs-6 plr0">
                       		     {{username}}
                        </div>
                        <div class="col-xs-6 plr0 text-right">
                            {{money}}元
                        </div>
                    </div>
                    <div class="col-xs-10 pr0 gray pt5 pb10">
                        <div class="col-xs-6 plr0">
                            {{dealTime}}
                        </div>
                        <div class="col-xs-6 plr0 orange text-right">
                            <img src="/images/front/king.png" width="18" style="line-block" > 手气最佳
                        </div>
                    </div>
                </div>
            </div>
		{{/rows}}	 		 
	</script> 
    
</body>
</html>