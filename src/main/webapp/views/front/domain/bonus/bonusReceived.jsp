<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css_new.htm"%>
  <link rel="stylesheet" href="/cssjs_stable/css/font-awesome.min.css">
  
    <link rel="stylesheet" href="/css/front/bonus/bonus.css"  title="v">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/css/front/wpknew/font-awesome-ie7.min.css" title="v">
    <![endif]-->
  <!--   <script src="/cssjs_stable/js/jquery-1.11.2.js"></script>
   <script src="/js/front/bonus/index.js" title="v"></script>
    <script src="/cssjs_stable/js/bootstrap.min.js"></script> -->
</head>
<body style="background-color: #faf6f1;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 bonus-head plr0">
                <div class="col-xs-2 ptb5 head-left" onclick="lh.back();">
                    <img src="images/icon-left.png" width="22" height="21" class="img-responsive center-block">
                </div>
                <div class="col-xs-10 fs18 mt-5">
                    拼手气群红包
                </div>
                <div class="col-xs-10">
                    微信安全支付
                </div>
            </div>
        </div>
        <div class="row fs16">
            <div class="col-xs-4 col-xs-offset-4 pt50">
                <img src="${user.avatar}" class="img-responsive center-block">
            </div>
            <div class="col-xs-4 pt20 text-center yellow">
                <select class="weui_select fs18"  onchange="getBonusList(true);" style="line-height: 24px;height: 24px;padding-left: 30%;" name="select1">
                    <option selected="" value="1">2016</option>
                    <option value="2">2015</option>
                    <option value="3">2014</option>
                </select>
                <i class="icon-angle-down"></i>
            </div>
            
     
            <div class="col-xs-12 text-center pt10"><span>${user.username}</span>共收到</div>
            <div class="col-xs-12 text-center">
            <span class="fs28">
            <c:if test="${totalBonusMoney == null}">0</c:if>
           	 ${totalBonusMoney}
            </span>元</div>
            <div class="col-xs-12 mt20 gray text-center">
	            <div class="col-xs-6 plr0 text-center">
	                <span class="fs28">${totalBonusNum}</span><br>收到红包
	            </div>
	            <div class="col-xs-6 plr0 text-center">
	                <span class="fs28">${totalSendBonusNum }</span><br>发出的红包
	            </div>
            </div>
        </div>
        <div class="row mt20">
           
           <!--  <div class="col-xs-12 bg-white">
                <div class="col-xs-12 plr0 bdb">
                    <div class="col-xs-12 plr0 fs16 pt10">
                        <div class="col-xs-6 plr0">
                            小康康
                        </div>
                        <div class="col-xs-6 plr0 text-right">
                            0.01元
                        </div>
                    </div>
                    <div class="col-xs-12 plr0 gray pt5 pb10">
                        <div class="col-xs-6 plr0">
                            04/22
                        </div>
                    </div>
                </div>
            </div> -->
           
        </div>
    </div>
    <div id="myOpen">
				<ul id="dataUl" class="" rows="1" onclick="jumpToDetail();">
					<!-- 数据容器  -->
					<!-- 
					<li class="receivehbli Receivedhb">
						<a>
							<span class="hblispan hblispan1">1.88元<br><strong></strong></span>
							<span>2015-10-09<br>鹭草山房的红包</span>
						</a>
					</li> -->
				</ul>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
    <div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 
		<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script type="text/javascript">IS_SEND = false;</script>
	<script src="/js/front/bonus/bonusList.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			 <div class="col-xs-12 bg-white" onclick="lh.jumpR('/bonusDetail/{{bonusId}}');">
                <div class="col-xs-12 plr0 bdb">
                    <div class="col-xs-12 plr0 fs16 pt10">
                        <div class="col-xs-6 plr0">
                            {{name}}
                        </div>
                        <div class="col-xs-6 plr0 text-right">
                            {{money}}元
                        </div>
                    </div>
                    <div class="col-xs-12 plr0 gray pt5 pb10">
                        <div class="col-xs-6 plr0">
                            04/22
                        </div>
                    </div>
                </div>
            </div>
		{{/rows}}	 		 
	</script>
    
</body>
</html>