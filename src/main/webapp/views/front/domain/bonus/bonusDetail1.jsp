<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/dust.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/mobiscroll/css/mobiscroll.custom-2.6.2.min.css"/> 
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">

			<div class="envelopes">
	            <div class="enveldiv1">
	                <%-- 
	                <div style="margin-bottom: 12px;">
	                    <span id="spnOver" class="drawover" style="display: none">◤全部领完◢</span>
	                    <span id="labSname">${bonus.bossName}</span>
	                </div> --%>
	                <div class="drawtop">
	                    <span class="drawtspan1"><span id="labDate"><fmt:formatDate value="${bonus.dealTime}" pattern="YYYY年MM月dd日"></fmt:formatDate></span></span>
	                    <span id="labBless"><span style="color:green;">${bonus.bossName}：</span><c:if test="${bonus.msg}">恭喜发财，大吉大利</c:if>${bonus.msg}</span>
	                    <span id="spnIcon" class="drawtspan" style="background-position:50%,50%;background-size:100%;background:url(${bonus.bossAvatar}) no-repeat scroll 50% 50% / cover  rgba(0, 0, 0, 0);"></span>
	                </div>
	                
	                <c:if test="${empty myBonus.msg}">
	                 	<div style="clear: both;"></div>
		                <div id="divBlessing" class="drawinput">
	                        <input name="msg" type="text" id="msg" placeholder="请输入祝福语">
	                        <button type="button" class="btn btn-success fr" style="width:14%" onclick="sendBonusMsg(${bonus.id});">发送</button>
	                        <!-- <span id="ASubmit">发送</span> -->
		                </div>
                   	</c:if>
	
	                <div style="clear: both;"></div>
	                <div class="drawcont">
	                    <div class="drawconttop">
	                    	<c:if test="${!empty boss}">
		                        <span id="labAll" class="drawyilq">
		                            <span id="bonusCount">${bonus.count}</span>个红包，共<span id="totalMoney">${bonus.totalMoney}</span>元
		                        </span>
	                    	</c:if>
	                        <span class="drawyilq1">
	                        	已领取<strong><span id="dealNum"> ${dealCount} </span></strong>个，共<strong><span id="dealMoney"> ${dealMoney} </span></strong>元
	                        </span>
	                    </div>
	                    <div id="dataContainer" class="drawcontc">
	                    	<!-- 数据容器 -->
		                    <!-- 
		                    <div class="drawcontcdiv">
		                    	<a href="/user/{{userId}}">
			                        <span class="drawtspan1">领到<strong>0.18</strong>元<br>
			                            10月09日 20:42:52</span>
			                        <span class="drawtspan" style="background: url({{userAvatar}}); background-position: 50%,50%; background-size: 100%;"></span>
			                        <span class="drawtspan2">{{username}}<br>
			                            <strong>{{msg}}</strong>
			                        </span>
			                        <div style="clear: both;"></div>
		                        </a>
		                    </div> -->
	
	                    </div>
	                </div>
	            </div>
	        </div>

		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<div class="save bottomFix pointer" id="antiqueCityShop">
		<div onclick="lh.jumpR('/bonus');" style="width:45%;padding-top: 10px;height: 45px;color: #f0f0f2;font-size: 20px;text-align: center;display: inline-block;background-color: #525244;">我也要发红包</div>
		<div onclick="lh.jumpR('/shop/${bonus.userId}');" style="width:53%;padding-top: 10px;height: 45px;color: #f0f0f2;font-size: 20px;text-align: center;display: inline-block;">进他的店铺</div>
		<!-- 
		<ul>
			<li><a href="javascript:;" class="a_say" onclick="saveProfession();return false;">保存</a></li>
		</ul> -->
	</div>
	
	<input type="hidden" id="bonusId" value="${bonusId}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script src="/js/front/bonus/bonusDetail.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<div class="drawcontcdiv">
            	<a href="javascript:lh.jumpR('/shop/{{userId}}');">
                   <span class="drawtspan1">领到<strong> {{money}} </strong>元<br>
                      {{dealTime}}</span>
                   <span class="drawtspan" style="background: url({{userAvatar}}); background-position: 50%,50%; background-size: 100%;"></span>
                   <span class="drawtspan2">{{username}}<br>
                      <strong>{{msg}}</strong>
                   </span>
                   <div style="clear: both;"></div>
                </a>
            </div>
		{{/rows}}	 		 
	</script>

</body>
</html>
