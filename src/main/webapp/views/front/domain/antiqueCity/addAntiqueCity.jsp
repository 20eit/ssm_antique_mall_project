<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
</head>
<body style="background-color: #f5f5f5;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<%-- <input type="hidden" value="${antiqueCity.id}" id="antiqueCityId"> --%>
	<div class="c_0100_22 li_rum" style="margin-top:50px;">
		<!-- <div class="text_frum " style="height: auto;"> -->
		<div class="weui_cells weui_cells_form">
			<div> 
				<!-- <div class="c_0100_3 information_border" style="padding:10px;">
					<div class="titleFontSzie">
						<nobr>商圈名称:<input style="border:0;width:85%" type="text" id="name" /></nobr>
					</div>
				</div> -->
				<div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">商圈名称:</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                    <input class="weui_input" type="text" id="name"  placeholder="请输入商圈名称">
	                </div>
	            </div>
				<!-- <div class="c_0100_3 information_border" style="padding:10px;">
					<div class="titleFontSzie">
						<nobr>商圈地址:<input style="border:0;width:85%"  type="text" id="address" /></nobr>
					</div>
				</div> -->
				<div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">所在省：</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                	<select id="province" onChange="getCity();return false;">
	                		<option value="">请选择</option>
							<c:forEach var="province" items="${provinceList}">
								<option value="${province.id}" <c:if test="${province.id == antiqueCity.province}">selected="selected"</c:if>>${province.areaName}</option>
							</c:forEach>
						</select>
	                </div>
	            </div>
	            <div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">所在市：</label></div>
	                <div id="cityDiv" class="weui_cell_bd weui_cell_primary">
	                	<select id="city"></select>
	                </div>
	            </div>
				<div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">商圈地址:</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                    <input class="weui_input" type="text" id="address" placeholder="请输入商圈地址">
	                </div>
	            </div>
				<div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">联系电话:</label></div>
	                <div class="weui_cell_bd weui_cell_primary">
	                    <input class="weui_input" type="text" id="tel" placeholder="请输入联系电话">
	                </div>
	            </div>
			</div>
			<!-- <div class="c_0100_3 information_border" style="padding:0px;">
				<nobr style="font-size:14px;padding:10px;">商圈介绍:</nobr><textarea id="introduce" style="width:100%;height:300px;border:0;"></textarea>
			</div> -->
			<div class="weui_cell">
	                <div class="weui_cell_hd"><label class="myLabel">内容</label></div>
	          </div>
			<div class="weui_cell">
                <div class="weui_cell_bd weui_cell_primary">
                    <textarea class="weui_textarea" placeholder="请输入商圈介绍内容" rows="3" id="introduce"></textarea>
                    <div class="weui_textarea_counter"><span>0</span>/200</div>
                </div>
            </div>
			<div class="c_0100_3 information_border" >
			  	<div class="ipt_item_div">
							<label class="myLabel">上传商圈logo：</label>
							<input type="hidden" id="instLogo" value="${ap.picPaths}">
							<img src="${ap.picPaths}" class="picurl">
							<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
						    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
							<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传商圈logo</button>
					</div>
					<div id="upload_outer_div"><!-- 上传文件进度展示 --></div>
			  </div>	
		</div>
	</div>
	<div class="c_0100_18 bottomFix">
		<ul>
			<li><a href="javascript:;" class="a_say"   onclick="javascript:history.back(-1)">取消</a></li>
			<li><a href="javascript:;" class="a_say"   onclick="commitAntiqueCity();return false;">保存</a></li>
		</ul>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<input type="hidden" id="antiqueCityCity" value="${antiqueCity.city}">
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="/js/front/antiqueCity/addAntiqueCity.js" title="v"></script>
</body>
</html>
