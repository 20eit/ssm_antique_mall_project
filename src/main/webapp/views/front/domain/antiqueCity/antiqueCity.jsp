<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
</head>
<body style="background-color:#f5f5f5;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="pz_main" style="margin-top:50px;">
		<div class="c_0100_15">
			<div class="c_0100_15">
		    	<ul id="antiqueCity">
		        </ul>
	        </div>
	    </div>
	    <div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="save bottomFix" id="antiqueCityShop">
		<ul>
			<li><a href="javascript:;" class="a_say" id="" onclick="javascript:lh.jumpR('/addAntiqueCity');">添加商圈</a></li>
		</ul>
	</div>

	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/js/front/antiqueCity/antiqueCity.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
		<li>
			<div class="teacher_list pointer" onclick="lh.jumpR('/antiqueCityDetail/{{id}}');">
				<div class="l_60" style="height:60px;width:120px;overflow:hidden;border-radius: 0;">
					<img src="{{picPath}}" height="60" />
				</div>
				<div class="r_595 pointer" style="width:60%">
					<nobr>
						&nbsp;&nbsp;<span>{{name}}</span>
					</nobr>
				</div>
				<div class="r_595 pointer" style="width:60%">
					<nobr>
						&nbsp;&nbsp;<span>{{address}}</span>
					</nobr>
				</div>
			</div>
		</li>
		{{/rows}}		 		 
	</script>

</body>
</html>
