<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
<style type="text/css">
	.c_0100_3 li{
		float: none;
  		width: 100%;
	}
</style>
</head>
<body style="background-color: #f5f5f5;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<input type="hidden" value="${antiqueCityId}" id="antiqueCityId">
	<div class="c_0100_22 li_rum" style="margin-top:50px;">
		<div class="text_frum " style="height: auto;">
			<div> 
				<div class="c_0100_3" style="padding: 0px;text-align: center;">
					<img src="${antiqueCity.picPath}" width="50%" />
				</div>
				<div class="c_0100_3" style="padding:10px;">
					<c:if test="${!empty userId}">
						<c:if test="${currentUserId == userId}">
							<div class="r_gz" style="margin-right:10px;">
								<a href="javascript:;" onclick="editData();return false;" class="a_gz">修改</a>
							</div>
						</c:if>
					</c:if>
					<div class="titleFontSzie">
						<nobr>商圈名称:<input style="border:0;width:85%" readonly="readonly" type="text" id="name" value="${antiqueCity.name}"/></nobr>
					</div>
					<div class="titleFontSzie">
						<nobr>商圈所在省:
							<select id="province" onChange="getCity();return false;" disabled="disabled">
		                		<option value="">请选择</option>
									<c:forEach var="province" items="${provinceList}">
										<option value="${province.id}" <c:if test="${province.id == antiqueCity.province}">selected="selected"</c:if>>${province.areaName}</option>
									</c:forEach>
								</select>
						</nobr>
					</div>
					<div class="titleFontSzie">
						<nobr>商圈所在城市: 
							<span id="cityDiv">
			                	<select id="city" disabled="disabled"></select>
			                </span>
		                </nobr>
					</div>
					<div class="titleFontSzie">
						<nobr>商圈地址:<input style="border:0;width:85%" readonly="readonly" type="text" id="address" value="${antiqueCity.address}"/></nobr>
					</div>
					<div class="titleFontSzie">
						<nobr>联系电话:<input style="border:0;width:85%" readonly="readonly" type="text" id="tel" value="${antiqueCity.tel}"/></nobr>
					</div>
					<div class="titleFontSzie">
						<nobr>创建日期：<fmt:formatDate value="${antiqueCity.createdAt}" pattern="yyyy-MM-dd"></fmt:formatDate></nobr>
					</div>
				</div>
			</div>
			<div class="c_0100_3" style="padding:10px;">
				<div class="titleFontSzie">
					<span>商圈介绍:</span><textarea id="introduce" readonly="readonly" style="width:100%;height:300px;border:0;">${antiqueCity.introduce}</textarea>
				</div>
			</div>
		</div>
		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
		<div class="save bottomFix c_0100_18" id="antiqueCityShop">
			<ul>
				<li><a href="javascript:;" class="a_say" onclick="lh.jumpR('/antiqueCityShop/${antiqueCity.id}');">商家列表</a></li>
				<li><a href="javascript:;" class="a_say" onclick="lh.jumpR('/addNews/${antiqueCity.id}');">发布新闻</a></li>
			</ul>
		</div>
		<div class="c_0100_18 frontHide bottomFix" id="edit" >
			<ul>
				<li><a href="javascript:;" class="a_say" onclick="cancelData();return false;">取消</a></li>
				<li><a href="javascript:;" class="a_say" onclick="commitAntiqueCity();return false;">保存</a></li>
			</ul>
		</div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<input type="hidden" id="antiqueCityCity" value="${antiqueCity.city}">
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="/js/front/antiqueCity/antiqueCityDetail.js" title="v"></script>
</body>
</html>
