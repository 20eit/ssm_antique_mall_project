﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/dust.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="myzhangtao">
			<div class="extension">
				<div style="text-align: center;padding-top: 5px;">
					<!-- <span style="width:48%;border:1px solid #DA1919">推广分类：全部 ∨</span> -->
					<span id="goodsTypes" onclick="$('.navcdbox').show();" style="width:98%;border:1px solid #DA1919">藏品分类：全部 ∨</span><!-- width:48% -->
				</div>
			</div>
			<div>
				<div class="ztchoice" style="margin: 0 0 6px 0;text-align: center;">
					<span class="ztchoicespan">点击藏品右下角图标，勾选藏品即分享到我的店铺</span> 
				</div>
				<div style="clear: both;"></div>
				<!--我要推广藏品 begin-->
				<div class="tarticlecont" id="leftRight2">
					<div style="padding: 0 8px;">
						<div class="tarticlecontleft" id="leftGoodsList">
							<!-- 
							<div class="tarticlebox" style="position: relative;" bid="330973">
								<img src="/file/default/1449043351922__20151202142715465_thumb.jpg"/>
								<div class="tarticlename">百艺堂藏品 尼泊尔...</div>
								<div class="tarticlejg" style="color: #797979;">￥ 8800</div>
								<div class="tarticlejg">红包 ￥100</div>
								<span id="330973" class="tarzan tarzan2 tarzan3"></span>
							</div> -->
							
						</div>
						<div class="tarticlecontright" id="rightGoodsList">

						</div>
					</div>
					<div style="clear: both;"></div>
				</div>
				<!--我要推广藏品 end-->
			</div>
		</div>
		
		<div class="extensiondiv" style="display: none;">
            <ul>
                <li dl="1,2">推广每日一拍拍品</li>
                <li dl="3">推广掌拍拍品</li>
                <li dl="0">推广普通藏品</li>
                <li dl="99" style="border-bottom: none;">关闭</li>
            </ul>
        </div>
        
        <div class="navcdbox" style="display: none;">
            <div class="menu-listbox">
                <div id="firstpane" class="menu_list fr">
                    <p class="menu_head dbbstype">全部</p>
                    <p class="menu_head dbbstype" typeId="129">杂珍</p>
                    <p class="menu_head dbbstype" typeId="128">陶瓷</p>
                    <p class="menu_head dbbstype" typeId="127">金石</p>
                    <p class="menu_head dbbstype" typeId="126">竹木</p>
                    <p class="menu_head dbbstype" typeId="125">玉器</p>
                    <p class="menu_head dbbstype" typeId="124">珠宝</p>
                    <p class="menu_head dbbstype" typeId="123">书画</p>
                    <p class="menu_head dbbstype" typeId="122">钱证</p>
                    <p class="menu_head dbbstype" typeId="121">文玩</p>
                    <p class="menu_head current" onclick="$('.navcdbox').hide();">关闭</p>
                </div>
            </div>
        </div>

		<div style="height: 40px"></div>

	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<div id="resultTip" class="resultTip frontHide"></div>
	<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%>
	<!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/distribution/distributionGoods.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="tarticlebox" style="position: relative;" bid="330973">
			<img src="{{picPath}}"/>
			<div class="tarticlename">{{goodsName}}</div>
			<div class="tarticlejg" style="color: #797979;"> {{shopPrice}}</div>
			<div class="tarticlejg">红包 {{spreadPackets}}</div>
			<span id="goodsId_{{id}}" onclick="selectGoods({{id}});" class="tarzan tarzan2 tarzan3"></span>
		</div>
	{{/rows}}	 		 
	</script>

</body>
</html>
