﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/taokeContent.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="content read_recom">
			<div class="myzttitle">
		        <div class="taoketext" style="padding-top: 5px;">
		            <div class="taokewenh" id="divOpen"><span class="tkwenhsp pointer" onclick="toggleTip('show');"></span><span>终身客户：由你推广进来并注册的新用户</span></div>
		            <div style="clear: both;"></div>
		        </div>
		    </div>
			<div class="list_wrap">
					<div style="text-align: center;">共  <span id="customerCount"></span> 位客户</div>
				<div class="list list1">
					<ul id="customerUl">
							<li>
								<span class="rank" style="width:54px;">序号</span> 
								<span class="shop_name" style="width:auto;padding-left:16px;">客户名称</span> 
								<span class="link" style="width:auto;float:right;padding-right:40px;">注册日期</span>
							</li>
							<!-- 我的客户列表容器 -->
					</ul>
				</div>
			</div>
		</div>
		
		<div id="divInst" class="szweituo" style="display:none;">
	        <div class="szweituodiv">
	            <div class="szwtcontent">
	                <div class="szwtinput">
	                    <div class="szwtwhat">终身客户有什么用？</div>
	                    <div class="szwtmiaosu">① 终身客户通过您的链接购买佣金藏品，您能获得佣金的70%奖励</div>
	                    <div class="szwtmiaosu" style="margin-top: 8px;">② 终身客户通过别人链接购买佣金藏品，您能获得佣金30%奖励，别人可获佣金40%奖励</div>
	                    <div class="szwtbutton">
	                        <div id="divClose" onclick="toggleTip('close');" class="szwtbutl" style="background: red;cursor:pointer; padding: 12px 0; min-width: 255px; margin-top: 15px;">关闭</div>
	                        <div style="clear: both;"></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    
		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
		<div style="height: 40px"></div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/distribution/myCustomer.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<li>
			<span class="rank" style="width:50px;padding-top:2px;"><em>{{index}}</em></span> 
			<span class="shop_name" style="width:auto;padding-left:20px;" onclick="lh.jumpR('/sale/{{customerId}}');">{{customerName}}</span> 
			<span class="link" style="width:auto;float:right;padding-right:10px;">{{regDate}}</span>
		</li>
	{{/rows}}	 		 
	</script>
</body>
</html>
