﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div class="tkbanner" onclick="">
				<img src="/images/zy/pic/tkbanner.png">
			</div>
		</div>
		<div class="content read_recom">
			<div id="j_tab_menu" class="tab_menu">
				<ul id="rankUl" class="clearfix">
					<li id="rankLi1" class="cur"><a href="javascript:switchRank(1);">总榜单</a></li>
					<li id="rankLi2" class=""><a href="javascript:switchRank(2);">月排行</a></li>
					<li id="rankLi3" class=""><a href="javascript:switchRank(3);">周排行</a></li>
					<div style="clear: both;"></div>
				</ul>
			</div>
			<div class="list_wrap">
				<div id="rankList1" style="display: block;" class="list list1">
					<ul>
						<c:forEach items="${overallDIList}" var="user" varStatus="status">
							<li><span class="rank"><em>0${status.index+1}</em></span> <span class="shop_name">${user.username}</span> <span class="link">￥${user.totalIncome}</span></li>
						</c:forEach>
					</ul>
				</div>
				<div id="rankList2" style="display: none;" class="list list2">
					<ul>
						<c:forEach items="${monthDIList}" var="user" varStatus="status">
							<li><span class="rank"><em>0${status.index+1}</em></span> <span class="shop_name">${user.username}</span> <span class="link">￥${user.totalIncome}</span></li>
						</c:forEach>
					</ul>
				</div>
				<div id="rankList3" style="display: none;" class="list list3">
					<ul>
						<c:forEach items="${weekDIList}" var="user" varStatus="status">
							<li><span class="rank"><em>0${status.index+1}</em></span> <span class="shop_name">${user.username}</span> <span class="link">￥${user.totalIncome}</span></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>

		<ul class="tknavul" style="height: 200px;">
			<li class="tknavulli1" onclick="lh.jumpR('/myIncome');"><span></span><br>收入报表</li>
			<li class="tknavulli2" onclick="lh.jumpR('/myCustomer');"><span></span><br>我的客户</li>
			<li class="tknavulli5" onclick="lh.jumpR('/myShop');"><span></span><br>我的店铺</li>
			<li class="tknavulli3" onclick="lh.jumpR('/auctions');"><span></span><br>推广拍场<!-- <strong>14</strong> --></li>
			<li class="tknavulli4" onclick="lh.jumpR('/distGoods');"><span></span><br>推广藏品</li>
			<div class="clear"></div>
		</ul>

		<div style="height: 40px"></div>

	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<div class="save bottomFix" style="background-color: #A4A8B3;">
		<ul>
			<li><a href="javascript:lh.jumpR('/distDesc');" class="a_say">淘客说明</a></li>
		</ul>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/distribution/distribution.js" title="v"></script>
	
</body>
</html>
