﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/taokeContent.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="myzhangtao">
			<div class="myzttitle">
				<ul>
					<li class="myztli1">今日收入</li>
					<li class="myztli2">
						<span id="today_income_int" style="color: #E42626;">
							<c:if test="${!empty todayIncome}">${todayIncome.totalIncome}<%-- <fmt:formatNumber value="${todayIncome.totalIncome}" pattern="#."></fmt:formatNumber> --%></c:if>
							<c:if test="${empty todayIncome}">0.00</c:if>
						</span>
						<!-- <nobr id="today_income_decimal">00</nobr> -->元
					</li>
				</ul>
				<ul>
					<li class="myztli1">本月收入</li>
					<li class="myztli2">
						<span id="month_income_int" style="color: #E42626;">
							<c:if test="${!empty monthIncome}">${monthIncome.totalIncome}<%-- <fmt:formatNumber value="${monthIncome.totalIncome}" pattern="#."></fmt:formatNumber> --%></c:if>
							<c:if test="${empty monthIncome}">0.00</c:if>
						</span>
						<!-- <nobr id="month_income_decimal">00</nobr> -->元
					</li>
				</ul>
				<ul>
					<li class="myztli1">总收入</li>
					<li class="myztli2">
						<span id="total_income_int" style="color: #E42626;">
							<c:if test="${!empty overallIncome}">${overallIncome.totalIncome}<%-- <fmt:formatNumber value="${overallIncome.totalIncome}"></fmt:formatNumber> --%></c:if>
							<c:if test="${empty overallIncome}">0.00</c:if>
						</span>
						<%-- <nobr id="total_income_decimal"><fmt:formatNumber value="${overallIncome.totalIncome}" pattern=".##"></fmt:formatNumber></nobr> --%>元
					</li>
				</ul>

				<div style="clear: both;"></div>
			</div>

			<div class="myztcont" style="padding:2px;">
				<div class="myztconttitle">
					<span style="float: right;"> 
						<select id="ddlDay" onchange="loadMyIncome(1);">
								<option value="today">今日</option>
								<option value="week">本周</option>
								<option value="month" selected="selected">本月</option>
								<option>全部</option>
						</select>
					</span> 
					<span>账户近期概况</span>
				</div>

				<table id="tabMain" class="myzttable">
					<tbody id="incomeTable">
						<tr class="myzttabt" style="border-bottom: 1px solid #F1EEEE;">
							<td class="myzttabtd1" style="width: 18%;text-align: center;">类型</td>
							<td class="myzttabtd2"style="width: 24%;">对象</td>
							<td class="myzttabtd2"style="width: 18%;">金额</td>
							<td class="myzttabtd3"style="width: 20%;">日期</td>
							<td class="myzttabtd4"style="width: 15%;">状态</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
		<div style="height: 40px"></div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>

	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%>
	<!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script src="/js/front/distribution/myIncome.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<tr class="myzttabt">
			<td class="myzttabtd1" style="width: 18%;text-align: center;">{{typeName}}</td>
			<td class="myzttabtd2"style="width: 24%;">{{objectName}}</td>
			<td class="myzttabtd2"style="width: 18%;"><span style="color:green">{{moneyIncome}}</span> 元</td>
			<td class="myzttabtd3"style="width: 20%;">{{dealTime}}</td>
			<td class="myzttabtd4"style="width: 15%;">{{statusName}}</td>
		</tr>
	{{/rows}}	 		 
	</script>
</body>
</html>
