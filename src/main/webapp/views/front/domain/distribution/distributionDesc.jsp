﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/taoke.css" title="v" />
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div class="rich_media">
				<div class="rich_media_inner">
					<h2 class="rich_media_title" id="activity-name">什么是淘客</h2>
					<div class="rich_media_meta_list">
						<em class="rich_media_meta text" id="post-date">2015-12-04</em> <a id="post-user" href="javascript:void(0);" class="rich_media_meta link nickname">【微拍客】</a>
					</div>
					<div id="page-content">
						<div id="img-content">
							<div class="rich_media_content" id="js_content">
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">
													<span style="font-size: 16px;"><span style="line-height: 1.5em; text-decoration: inherit; font-family: inherit;">什么是淘客？</span></span>
												</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
									<p style="margin: 5px 0px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px;">
										<span style="font-size: 16px;"><span style="font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">淘客的推广是一种按成交或效果计费的推广模式，淘客就是指帮助卖家推广商品或拍场并获取佣金的人，每个人都可申请成为淘客，通过微信群、微信朋友圈、QQ、微博、空间、贴吧、论坛等推广获取佣金。</span></span><br>
										&nbsp;
									</p>
									<div>
										<fieldset class="tn-Powered-by-wxqq"
											style="color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
												style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
												<section class="tn-Powered-by-wxqq wxqq-bg"
													style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
													<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">淘客优势</section>
												</section>
												<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
													style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
													&nbsp;</section>
											</section>
										</fieldset>
										<p style="margin: 5px 0px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px;">
											<span style="font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">1. 即时结算佣金</span>
										</p>
									</div>
								</div>
								<div style="text-align: center;">
									<div style="text-align: left;">
										<div style="word-wrap: break-word;">
											<div style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
												<span style="font-size: 16px;">2. 由您推广进来并注册的新用户将成为您的终身客户
													<span style="color: #4EB952;font-size: 13px;">（终身客户有什么用？1.终身客户通过您的链接购买佣金藏品，您能获得佣金的70%奖励。2.终身客户通过别人链接购买佣金藏品，您能获得佣金30%奖励，别人可获得佣金40%奖励）</span>
												</span>
											</div>
											<!-- <div
												style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px; text-align: center;">
												<img alt="" src="/images/zy/pic/1-15061R2160Ib.jpg"><br> &nbsp;
											</div> -->
											<div style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
												<span style="font-size: 16px;">3. 佣金高于其他行业</span>
											</div>
											<div style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
												<span style="font-size: 16px;">4. 推广拍场，推广的用户进入拍卖大厅就能获得佣金</span>
											</div>
											<!-- <div style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
												<span style="font-size: 16px;">5. 推广每日一拍，成交后获官方5元奖励，若拍品有佣金，还可获佣金，</span>
											</div> -->
											<div style="word-wrap: break-word; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
												<span style="font-size: 16px;">5. 您推广的用户有注册，您还可获得官方1元奖励哦</span>
											</div>
											<span style="font-size: 16px;"><span
												style="color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">&#8203;</span></span><br> <br>
											<div>
												<fieldset class="tn-Powered-by-wxqq"
													style="color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
													<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
														style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
														<section class="tn-Powered-by-wxqq wxqq-bg"
															style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
															<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">淘客如何盈利</section>
														</section>
														<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
															style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
															&nbsp;</section>
													</section>
												</fieldset>
											</div>
										</div>
									</div>
								</div>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1. 推广拍卖专场</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">2. 推广藏品</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<!-- <span style="font-size: 16px;">3. 推广天天拍拍品<br> --> 3. 分销-选择佣金藏品加入自己店铺中，推广店铺<!-- <br> 5. 推广掌拍拍场拍品 -->
									</span>
								</p>
								<br>
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">淘客佣金结算时间</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
								</div>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">淘客佣金即时结算，藏品成功交易至确认收货后结算，<!-- 每日一拍拍卖结束后， -->拍场拍卖结束至确认收货后结算。</span>
								</p>
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">淘客佣金提现规则</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
									<p>
										<span style="font-size: 16px;">淘客账户大于100元方可提现</span>
									</p>
								</div>
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">藏品推广</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
								</div>
								<!-- <div style="text-align: center;">
									<img alt="" src="/images/zy/pic/1-15061R21G3G2.jpg"
										style="color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; font-size: 16px; line-height: 18px;">
								</div> -->
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<br> <span style="font-size: 16px;">推广古玩交易商城上的藏品，当有用户通过您分享的链接进来成功交易至确认收货后，您可以获得相应藏品的佣金，每件藏品的佣金由卖家设定，每件藏品佣金不同。计费方式：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1. 终身客户通过您链接<br> ① 例如：设置藏品佣金为100元，则：
									</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">藏品佣金 = 100元 * 70%</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">② 例如：设置藏品佣金为1%，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">藏品佣金 = 成交金额 * 1% * 70%<br> 2. 终身客户通过其他人链接<br> 计算方式如上，佣金获得比例为30%
									</span><br> <br> &nbsp;
								</p>
								<!-- <div>
									<fieldset class="tn-Powered-by-wxqq"
										style="color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">天天拍拍品推广</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
									<p style="margin: 5px 0px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px;">
										<span style="font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">推广每日一拍栏目上的拍品，当有用户通过您分享的链接进来拍中拍品至确认收货后，您可以获得相应拍品的佣金并获得官方的20元推广奖励，每件拍品的佣金由卖家设定，每件拍品佣金不同。计费方式：</span>
									</p>
								</div>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1. 终身客户通过您链接</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">① 例如：设置拍品佣金为50元，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">拍品佣金 = 50元 * 70% + 5元</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">② 例如：设置拍品佣金为1%，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">拍品佣金 = （成交金额 * 1%） * 70% + 5元</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">2. 终身客户通过其他人链接<br> 计算方式如上，佣金获得比例为30%
									</span>
								</p>
								<br> -->
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">拍场推广</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
								</div>
								<!-- <p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px; text-align: center;">
									<span style="font-size: 16px;"><img alt="" src="/images/zy/pic/1-15061R2194K92.jpg"></span><br> &nbsp;
								</p> -->
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">推广栏目上的各个拍场，当有用户通过您分享的链接进入拍场，您就可以与其他淘客按比例分享拍场总佣金。计算方式：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1.例如：拍场总佣金为1000元，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">根据用户数、用户点击数等综合计算，按比例分配1000元佣金</span><br> <br> &nbsp;
								</p>
								<!-- <div>
									<fieldset class="tn-Powered-by-wxqq"
										style="color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">掌拍拍品推广</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
									<p style="margin: 5px 0px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: 16px;">
										<span style="font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">推广栏目上的各个拍场，当有用户通过您分享的链接领取拍场号牌后，并在拍场进场后有进入拍场，您就可以与其他淘客按比例分享拍场总佣金。计算方式：</span>
									</p>
								</div>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1. 终身客户通过您链接</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">① 例如：拍品佣金为1000元，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">所得佣金 = 1000元 * 70%&nbsp;</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">1. 终身客户通过其他人链接</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">① 例如：拍品佣金为1000元，则：</span>
								</p>
								<p
									style="word-wrap: break-word; margin: 5px 0px; color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">
									<span style="font-size: 16px;">所得佣金 = 1000元 * 30%&nbsp;</span>
								</p>
								<h4 style="color: rgb(0, 0, 0); font-family: 'sans serif', tahoma, verdana, helvetica; line-height: 18px;">&nbsp;</h4> -->
								<div>
									<fieldset class="tn-Powered-by-wxqq"
										style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
										<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
											style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
											<section class="tn-Powered-by-wxqq wxqq-bg"
												style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236); background-position: initial initial; background-repeat: initial initial;">
												<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">平台推广</section>
											</section>
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
												style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
												&nbsp;</section>
										</section>
									</fieldset>
									<p style="margin: 5px 0px; font-family: sans-serif; font-size: 16px;">
										<span style="font-size: 16px;"><span style="font-family: 'sans serif', tahoma, verdana, helvetica;">成为淘客后，推广任意一个页面，当有用户通过您分享的链接进入并注册成功，您将获得官方1元推广奖励，每人每天限额30元。此外，官方将不定时发放淘客红包奖励，淘客积分越高，获得的红包奖励也越多哦！</span></span><br>
										&nbsp;
									</p>
									<div>
										<fieldset class="tn-Powered-by-wxqq"
											style="font-family: sans-serif; font-size: 16px; border: none; margin: 0.5em 0px 0px; box-sizing: border-box; padding: 0px;">
											<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor wxqq-borderLeftColor wxqq-borderRightColor"
												style="font-size: 1.2em; font-family: inherit; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(0, 187, 236); box-sizing: border-box;">
												<section class="tn-Powered-by-wxqq wxqq-bg"
													style="padding: 0.5em; line-height: 1.5em; box-sizing: border-box; background-color: rgb(0, 187, 236);">
													<section class="tn-Powered-by-wxqq" style="box-sizing: border-box;">作弊处罚</section>
												</section>
												<section class="tn-Powered-by-wxqq wxqq-borderTopColor wxqq-borderBottomColor "
													style="margin: auto; width: 0px; border-top-width: 0.6em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-color: rgb(0, 187, 236); box-sizing: border-box; border-left-width: 0.5em !important; border-left-style: solid !important; border-left-color: transparent !important; border-right-width: 0.5em !important; border-right-style: solid !important; border-right-color: transparent !important;">
													&nbsp;</section>
											</section>
										</fieldset>
										<p style="margin: 5px 0px; font-size: 16px;">
											<font face="sans serif, tahoma, verdana, helvetica">对不正当手段（包括但不限于恶意刷点击，恶意领取红包，扰乱系统及拍场秩序），微拍客坚决进行处理，视情节对其及其作弊帮助者进行封号、冻结账户资金、收回作弊奖励，实名列入黑名单等处罚，请正确进行推广</font>
										</p>
									</div>
								</div>
								<br>

							</div>
						</div>
					</div>
					<!-- 
					<div id="js_pc_qr_code" class="qr_code_pc_outer" style="display: block;">
						<div class="qr_code_pc_inner">
							<div class="qr_code_pc">
								<img src="http://qr.liantu.com/api.php?w=520&amp;text=http%3A%2F%2Fzy.guwanch.com%2Fsolution%2Fguwan%2Fzhangyanindex.aspx"
									id="js_pc_qr_code_img" class="qr_code_pc_img">
								<p>
									微信扫一扫<br> 微拍客古玩平台
								</p>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/distribution/distribution.js" title="v"></script>

</body>
</html>
