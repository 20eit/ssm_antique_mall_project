﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖机构 - AuctionInstAction:/ap/inst --%>
</head>
<body style="background-color: #f5f5f5;">
	<div class="pz_main">
		<div class="c_0100_19">
			<div class="slide_sale" id="slide_sale">
				<div class="t_0100_14">
					<div class="hd">
						<ul>
							<li class="on pointer" onclick="lh.jumpR('/ap/inst')">专场</li>
							<li class="pointer" onclick="lh.jumpR('/as')">微拍</li>
							<!-- //TODO-tempHide <li class="pointer" onclick="lh.jumpR('/aqIndex');">即时拍</li>-->
						</ul>
					</div>
					<div class="sr_255">
						<div class="threepoint">
							<a href="javascript:void(0);" class="a_po" onclick="toggleOptBtn();">
								<img src="/images/front/top_img1.png" width="25" height="25" />
							</a>
							<div id="pf_point" class="pf_point" style="display: none;">
								<div class="img_point">
									<img src="/images/front/top_img2.png" width="8" height="7" />
								</div>
								<div class="ulist_point">
									<div id="instApply" class="ist_point">
										<a href="javascript:lh.jumpR('/ap/instApplyDesc')">入驻专场</a>
									</div>
									<div id="myInst" class="ist_point">
										<a href="javascript:lh.jumpR('/ap/myInst')">我的专场</a>
									</div>
									<div class="ist_point">
										<a href="javascript:lh.jumpR('/goodsManageType')">产品管理</a><!-- /goodsSend -->
									</div>
								</div>
							</div>
						</div>
						<div class="class_sale">
							<a href="javascript:;" onclick="$('.pf_menu,.class').show(200);">分类</a>
						</div>
						<div class="class_sale">
							<a id="switchLaout" href="lh.jumpR('/ap/auctions')">大图</a>
						</div>
					</div>
				</div>
				<div class="bd">
					<div class="ul_show">
						<div id="container" class="sale_show" style="padding: 10px 0px 5px 5px;text-align: center;">
							<!-- 主数据加载区域 -->
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>

	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/auction/profession/auctionInst.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="li_sale">
			<div class="pf_lt">
				{{ &statusDom }}
			</div>
			<div class="img_112">
				<a href="javascript:lh.jumpR('/ap/auctions?instId={{ id }}');"><img src="{{ picPaths }}" width="100%" /></a>
			</div>
			<div class="tit_210">
				<a href="javascript:lh.jumpR('/ap/auctions?instId={{ id }}');"><nobr>{{ name }}</nobr></a>
			</div>
			<div class="t_210_2">
				<div class="fl"><!--  class="c_l_52"-->
					<div><!-- class="l_10"-->
						{{ &gradeDom }}
					</div>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>
	<!-- <div class="r_price">{{ goodsCount }}件</div> -->
</body>
</html>
