﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<input type="hidden" id="instId" value="${inst.id}">
			<div class="ipt_item_div">
				<label>专场图标：</label>
				<input type="hidden" id="instLogo" value="${inst.picPaths}">
				<img src="${inst.picPaths}" class="picurl">
				<input type="hidden" name="filePaths" id="filePaths" value="${inst.picPaths}"/>
			    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
				<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传图标</button>
				<!-- 上传文件进度展示 开始  id="filelist" -->
				<div id="upload_outer_div" class="fieldset"></div>
			</div>
			<div class="ipt_item_div">
				<label>专场名称：</label>
				<input type="text" id="instName" value="${inst.name}" class="ipt_item">
			</div>
			<div class="ipt_item_div">
				<label>联系电话：</label>
				<input type="text" id="instTel" value="${inst.tel}" class="ipt_item">
			</div>
			<div class="ipt_item_div">
				<label>联系地址：</label>
				<input type="text" id="instAddress" value="${inst.address}" class="ipt_item">
			</div>
			<div class="ipt_item_div">
				<label>专场简介：</label>
				<textarea id="instIntroduce" rows="15" class="ipt_item" >${inst.introduce}</textarea>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="save bottomFix" id="antiqueCityShop">
		<ul>
			<li><a href="javascript:;" class="a_say" onclick="saveInstDesc();return false;">保存</a></li>
		</ul>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/util/upload.js" title="v"></script>
	<script type="text/javascript" src="/js/common/file_upload.js" title="v"></script>
	<script src="/js/front/auction/profession/inst.js" title="v"></script>
</body>
</html>
