﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/mobiscroll/css/mobiscroll.custom-2.6.2.min.css"/> 
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div id="goodsList" class="clear-both" style="text-align: center;padding:5px;">
				<!-- 数据列表容器 -->
			</div>
			<div id="resultTip" class="resultTip frontHide"></div>
			<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="save bottomFix pointer" id="antiqueCityShop">
		<div onclick="lh.jumpR('/addGoods');" style="width:40%;padding-top: 10px;height: 45px;color: #f0f0f2;font-size: 20px;text-align: center;display: inline-block;background-color: #525244;">发布藏品</div>
		<div style="width:58%;padding-top: 10px;height: 45px;color: #f0f0f2;font-size: 20px;text-align: center;display: inline-block;" onclick="lh.jumpR('/professionAddOrUpdate?instId=${instId}');">新增拍卖</div>
		<!-- 
		<ul>
			<li><a href="javaspt:;" class="a_say" onclick="saveProfession();return false;">保存</a></li>
		</ul> -->
	</div>
	
	<input type="hidden" id="instId" value="${inst.id}">
	<input type="hidden" id="professionId" value="${ap.id}">
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<!-- <div id="mask"></div> -->

	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/auction/profession/professionSendGoods.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
			<div class="goodsDiv" style="height:255px;" id="img_{{ id }}">
				<img src="{{ picPath }}" class="goods_pic" onclick="lh.jumpR('/goods/{{id}}?shopId={{shopId}}');">
				<div class="goods_pic_txt_div">
					<span class="auc_pic_txt_span">&nbsp;起拍价：
						<span id="priceBegin_{{ id }}">{{auctionPrice}}</span>
						<span style="color:orange;"> 送拍拍品</span><br>&nbsp;名称：{{ goodsName }}
					</span>
					<div style="text-align:left;padding:3px 0px;">送拍时间：{{promoteStartDate}}</div>
					<div style="text-align:left;padding:1px 0 3px 0">送拍人：{{username}}</div>
					<div style="text-align:left">
						<button type="button" class="btn btn-sm btn-danger" style="" onclick="rejectSendGoods({{id}});">退 回</button>
						<button type="button" class="btn btn-sm btn-success" style="" onclick="lh.jumpR('/goods/{{id}}?shopId={{shopId}}');">查 看</button>
					</div>
				</div>
			</div>
		{{/rows}}	 		 
	</script>
</body>
</html>
