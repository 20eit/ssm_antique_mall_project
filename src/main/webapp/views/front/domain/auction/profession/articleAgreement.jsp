﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场添加须知条款- AuctionProfessionAction:/ap/page/articleAgreement --%>
</head>
<body>
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-1">
				<i class="icon-angle-left icon-3x" onclick="lh.back();"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<span class="fs16">阅读专场文章</span>
			</div>
		</div>
		<div class="row pt10">
			<div class="col-xs-12 article" style="line-height: 25px;">
				1、卖家不得在发布专场、拍卖过程中出现联系方式，引导或者要求买家不通过微拍客支付，而是通过线下支付或者转账，一经发现，永禁专场； <br/>
				2、拍品必须属于陶瓷、玉石、书画、邮品钱币、木器、铜器、珠宝翡翠、文玩收藏、古玩收藏、杂项；不得出售违禁品，比如博物馆文物、枪支弹药、刀具、毒品、动物制品等； <br/>
				3、每个专场，拍品数量最少10件。最多300件； <br/>
				4、同一个专场，同一件拍品只能上传一次； <br/>
				5、拍品必须图片清晰、品相完好无损、多图且不同角度展示、每个拍品最少三张图片； <br/>
				6、如果拍品有小的缺陷，请单独拍照并作说明，否则交易因此出现责任由卖方承担；<br/>
				7、拍品一经发现是假货、与描述不符，视情节对卖家进行警告、曝光、扣减保证金、封号处理。假货一律从严处理。同时，对买家造成的损失，责任由卖家承担；<br/>
				8、务必记住自己的专场开拍时间，必须到场主持； <br/>
				9、拍而不卖，视情节进行：警告、曝光、扣减保证金处理，3次以上拍而不卖一律封号处理；<br/>
				10、平台支持七天无理由退款退货，卖家必须遵守该原则； <br/>
				11、卖家需严格遵守微拍客交易及拍卖制度。<br/>
			</div>
		</div>
		<div style="height: 75px;"></div>
		<div class="readButton">
			<div onclick="lh.jumpR('/ap/page/typeChoose');" class="col-xs-12">
				<a role="button" class="btn btn-orange col-xs-12">我已阅读，同意</a>
			</div>
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>

</body>
</html>
