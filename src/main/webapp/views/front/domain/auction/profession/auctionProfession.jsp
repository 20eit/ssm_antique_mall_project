<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖机构 - AuctionInstAction:/ap/inst --%>

<style>body{font-size: 16px;}</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 plr0 jewel_title" style="background-color: white;">
				<ul>
					<li style="width:50%" class="active" onclick="lh.jumpR('/ap');">专场</li>
					<li style="width:50%" onclick="lh.jumpR('/am');">微拍</li>
					<!-- <li onclick="lh.jumpR('/ap');">大厅</li>
					<li onclick="lh.jumpR('/ap');">鬼市</li>
					<li onclick="lh.jumpR('/ap');">微现场</li> -->
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 bg_gray ptb10">
				<div class="col-xs-10 plr0 classification">
					<ul id="auctionSwitchUl">
						<li id="ap_senior" class="active" onclick="loadAuctionProfession('senior');">精品专场</li>
						<li id="ap_primary" onclick="loadAuctionProfession('primary');">普通专场</li>
						<li id="ap_focus" onclick="loadAuctionProfession('focus');">我的关注</li>
					</ul>
				</div>
				<div class="col-xs-2 plr0"><!--  onclick="lh.jumpR('/ap/professionSearch');" -->
					<img class="img-responsive center-block" src="/images/front/search.png" />
				</div>
			</div>
		</div>
		
		<div id="data-container">
		
		</div>
		
		<div class="row h85 bg_gray"></div>
	</div>
	
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script src="/js/front/auction/profession/auctionProfession.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 plr0 ptb10 bdb">
					<div onclick="lh.jumpR('/ag/page/agList/{{ serial }}')" class="col-xs-11 plr0 elli">【第{{auctionSerial}}场】{{auctionName}}</div>
					<div class="col-xs-1 plr0" onclick="lh.jumpR('/ag/page/agList/{{ serial }}')">
						<img width="20" height="20" class="img-responsive center-block" src="/images/front/pic_06.png" />
					</div>
				</div>
			</div>
			<div class="col-xs-12 ptb10">
				<img onclick="lh.jumpR('/ag/page/agList/{{ serial }}')" class="img-responsive" src="{{getPicPath}}" />
			</div>
			<div class="col-xs-12 pb10">
				<div class="col-xs-8 plr0">
					<div class="col-xs-4 plr0">
						<span class="orange">30</span>件拍品
					</div>
					<div class="col-xs-4 plr0">
						<span class="orange">38</span>人出价
					</div>
					<div class="col-xs-4 plr0">
						<span class="orange">{{visitNum}}</span>围观
					</div>
				</div>
				<div class="col-xs-4 plr0">
					{{&statusDom}}
				</div>
			</div>
			<div class="col-xs-12 h10 bg_gray"></div>
		</div>
	{{/rows}}	 		 
	</script>
</body>

</html>