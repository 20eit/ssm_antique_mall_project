﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<%-- 拍卖机构申请 - AuctionInstAction:/ap/instApply --%>
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			 
			<div class="ipt_item_div">
				<label>机构图标(如果不上传机构图标,那么您的头像将做为机构图标)：</label>
				<input type="hidden" id="instLogo" value="${ap.picPaths}">
				<img src="${ap.picPaths}" class="picurl">
				<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
			    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
				<button id="browse" type="button" class="weui_btn weui_btn_mini weui_btn_primary" >上传图标</button>
				<!-- 上传文件进度展示 开始  id="filelist" -->
				<!-- <div id="upload_outer_div" class="fieldset"></div> -->
			</div>
			<div id="upload_outer_div"><!-- 上传文件进度展示 --></div>
			<div class="ipt_item_div">
				<label>机构名称：</label>
				<input type="text" id="instName" class="ipt_item" value="${username}">
			</div>
			<div class="ipt_item_div">
				<label>联系电话：</label>
				<input type="text" id="tel" class="ipt_item" value="${phone}">
			</div>
			<div class="ipt_item_div">
				<label>备注信息：</label>
				<input type="text" id="message" class="ipt_item"/>
			</div>
			<div class="ipt_item_div" style="margin-top: 10px;text-align: center;color: #5D5858;font-size: 14px;">
				<span>提示：<span id="tip"></span></span>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="save bottomFix">
		<ul>
			<li><a href="javascript:void(0);" class="a_say" onclick="submitInstApply();return false;">提交申请</a></li>
		</ul>
	</div>
	 <div id="offerWin" class="szweituo" style="display: none;">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<div class="szwtinput">
					<div id="offerTip" class="szwtzuigao">输入支付密码:</div>
					<div id="offerIptDiv" class="szwtinpk">
						<form id="entrustFrom">
							<span> <input type="password" id="payPass" placeholder="请在此输入输入支付密码" >
							</span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="offerCancel" onclick="doAutoOffer('cancel')" class="szwtbutr entrusta2 pointer">取消</span> 
						<span id="offerSave" onclick="doAutoOffer('save')" class="szwtbutl entrusta1 pointer">确定</span>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/auction/profession/auctionInstApply.js" title="v"></script>
</body>
</html>
