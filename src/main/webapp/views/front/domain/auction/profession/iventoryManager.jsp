﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场拍品列表 - AuctionHallAction:/ap/agList/{auctionId}--%>
</head>
<body class="bg_gray">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">拍品库管理</span>
			</div>
			<div class="col-xs-3 pt10 plr7">
				<span>上传新品</span>
			</div>
		</div>
		<div class="row ptb10 goodsSearch">
			<div class="col-xs-12 plr7">
				<input type="text" class="form-control"/>
				<div class="col-xs-12 plr0 img">
					<div class="col-xs-1 plr0">
						<img src="images/search.png" class="img-responsive" />
					</div>
					<div class="col-xs-11 fs16 plr0 gray">
						输入商品关键词
					</div>
				</div>
			</div>
		</div>
		<div class="row mt10">
			<div class="col-xs-3 plr0 text-center bg_white fs16 bdso99">
				<div class="col-xs-12 plr0 bdb ptb10">
					陶瓷
				</div>
				<div class="col-xs-12 plr0 bdb ptb10">
					玉石
				</div>
				<div class="col-xs-12 plr0 bdb ptb10">
					书法
				</div>
				<div class="col-xs-12 plr0 bdb ptb10">
					字画
				</div>
				<div class="col-xs-12 plr0 bdb ptb10">
					珠宝/翡翠
				</div>
				<div class="col-xs-12 plr0 bdb ptb10">
					金银铜器
				</div>
			</div>
			<div class="col-xs-9 plr7">
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_01.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">冰糯种翡翠霸王貔貅吊坠</div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_02.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">白玉平安扣玉吊坠 </div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_01.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">冰糯种翡翠霸王貔貅吊坠</div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_02.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">白玉平安扣玉吊坠 </div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_01.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">冰糯种翡翠霸王貔貅吊坠</div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 plr7 bg_white">
					<div class="col-xs-12 plr0 ptb10 bdb">
						<div class="col-xs-4 plr0">
							<img class="img-responsive" src="images/goods_02.png" />
						</div>
						<div class="col-xs-8 pl7 pr0">
							<div class="col-xs-11 plr0">
								<div class="gray">白玉平安扣玉吊坠 </div>
								<div class="col-xs-12 plr0 mt10 gray">
									价格：￥158.00
								</div>
							</div>
							<div class="col-xs-1 plr0 pt20 mt10">
								<img class="img-responsive" src="images/plus.png" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <input type="hidden" value="${r}" id="r"/> 	
    <input type="hidden" value="${loginStatus}" id="loginStatus"/>
    <input type="hidden" value="${status}" id="status"/>
    <input type="hidden" value="${auctionId}" id="auctionId"/>
    <input type="hidden" value="${forbidden}" id="forbidden"/>
    
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/js/front/auction/profession/professionGoods.js" title="v"></script>

</body>
</html>
