﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div style="position: relative;text-align: center;" class="header">
				<h1 id="activity-name">${inst.name}</h1>
			</div>
			<div class="showpic">
				<img src="${inst.picPaths}" id="shopPic" style="width:100%;margin-top:5px;">
			</div>
			<div style="margin: 10px 0;">
				<span id="labAuctionComProd">${inst.introduce}</span>
			</div>
			<div class="storecontact">
				<span class="storename">${inst.name}</span>
			</div>
			<div style="clear: both;"></div>
			<div class="contactsto">
				<ul>
					<li>联系电话：<a href="tel:${inst.tel}"><span id="labPhone">${inst.tel}</span>（拨打）</a></li>
					<li>联系地址：<span id="labAuctionAddr">${inst.address}</span></li>
					<li></li>
				</ul>
			</div>
			<br/>
			<!-- <div><img id="imgCode" src="" style="width: 100%;"></div> -->
			<div class="page-content"></div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<c:if test="${!empty manager}">
		<div class="save bottomFix" id="instDescUpdate">
			<ul>
				<li><a href="javascript:lh.jumpR('/instDescUpdate/${inst.id}');" class="a_say">更新简介</a></li>
			</ul>
		</div>
	</c:if>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
</body>
</html>
