﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="pz_main" style="margin-top: 48px;">
		<div class="c_0100_19">
			<div class="slide_sale" id="slide_sale">
				<div class="bd">
					<div class="ul_show">
						<div id="professionList" class="sale_show">
							<div style="text-align: center; line-height: 50px; font-size: 20px; font-weight: bold; color: #FFFFFF;">
								<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="jumpToAddAuction();">新增拍卖</button>
								<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="lh.jumpR('/professionSendGoods');">查看送拍</button>
								<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="lh.jumpR('/instAnno/${inst.id}');">拍场公告</button>
								<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="lh.jumpR('/instDesc/${inst.id}');">拍场简介</button>
								<%-- <button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="location.href='/instSet?instId=${inst.id}'">拍场设置</button> --%>
							</div>
							<!-- 主数据加载区域 -->
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" id="instId" value="${inst.id}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/auction/profession/myAuctionInst.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="listday" style="padding:15px 5px;">
			<div class="pointer">{{&picPaths}}</div>
			<div class="listdaytit" style="padding:10px 0px;">
				{{ &gradeDom }}
				<span class="fl">{{ auctionName }}</span>
				<div class="clear-both"></div>
			</div>
			<table class="widthMax">
				<tbody>
					<tr><td class="fr">保证金：{{ #bail }}{{bail}}{{/bail}}{{^bail}}0 {{/bail}}元</td><td class="fl">电话：{{ instTel }}</td></tr>
				</tbody>
			</table>
			<div class="listdaytime">
				<span class="listdaysp1" style="text-indent: 0px;">
				<img style="margin: 0 5px 4px 0;" src="/images/front/day_img8.png" height="15">开拍时间：{{ getStartTime }} {{ &statusDom }} &nbsp;</span>
				{{ &updateDom }}
			</div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
