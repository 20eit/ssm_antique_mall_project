﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场拍品列表 - AuctionHallAction:/ap/agList/{auctionId}--%>
</head>
<body>
	<div class="container-fluid">
		<div class="row auction_title" style="position:absolute;top:0px;background:rgba(0,0,0,0.4);    width: 100%;">
			<div class="col-xs-1" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<span class="fs16">开始时间：<fmt:formatDate pattern="yyyy-MM-dd hh:mm" value="${ap.startTime}"/></span>
			</div>
		</div>
		<div class="row">
			<img class="img-responsive" src="${ap.picPaths}" /><!-- /images/front/auctionBanner.png -->
		</div>
		<div class="row mt10 mb10">
			<div class="col-xs-9">
				<div class="auction fs16">【第${ap.auctionSerial}场】</div>
				<div class="auction fs16"> ${ap.auctionName}</div>
				<div class="gray">${ap.description}</div>
			</div>
			<div class="col-xs-3 plr7 mt5 bdl" onclick="toggleNotice();">
				<img class="img-responsive center-block" src="/images/front/clock.png" />
				<div class="text-center" id="beginNotice2" state="1">开拍提醒</div>
			</div>
		</div>
		<div class="row">
			<div id="data-container" class="col-xs-12 product">
			
			</div>
		</div>
		<div class="row h50"></div>
	</div>
	<div class="auction_fixed">
		<div class="col-xs-2" onclick="lh.jumpR('/chat/${ap.userSerial}');">
			<img class="img-responsive center-block" src="/images/front/pic_04.png" />咨询
		</div>
		<div class="col-xs-2" onclick="toggleNotice();">
			<img class="img-responsive center-block" src="/images/front/pic_05.png" /><span id="beginNotice" state="1">提醒</span>
		</div>
		<div class="col-xs-2" onclick="showShare();">
			<img class="img-responsive center-block" src="/images/front/pic_06.png" />分享
		</div>
		<c:choose>
			<c:when test="${leftMoney > 0 }">
				<div onclick="jumpToPayCreditMoney('${ap.serial}');" class="col-xs-6">去支付 <br/>(保证金${ap.userBail}元)</div>
			</c:when>
			<c:otherwise>
				<div onclick="lh.jumpR('/ap/hall/${ap.serial}');" class="col-xs-6">进入现场 <br/>
					<c:if test="${empty ap.userBail}">(无保证金)</c:if>
					<c:if test="${!empty ap.userBail}">(保证金${ap.userBail})</c:if>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	
    <%@ include file="/views/common/common_share.htm"%><!-- 分享dom -->
    
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script> lh.param = ${paramJson}; </script>
	<script src="/js/front/auction/profession/common_auction_goods.js" title="v"></script>
	<script type="text/javascript" src="/js/front/auction/profession/professionGoods.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div onclick="lh.jumpR('/ag/page/detail/{{id}}');" class="col-xs-12 productMs" style="margin-bottom:5px;">
			<div class="col-xs-4 plr0">
				<img class="img-responsive" src="{{getPicPath}}" />
			</div>
			<div class="col-xs-8 pl7">
				<div class="gray_deep">{{goodsName}} </div>
				<div class="col-xs-12 plr0 mt5">
					<span class="orange pull-left fs20">￥<span>{{priceBegin}}</span></span>
					<span class="pull-right"><span class="orange">{{scansCount}}</span>次围观</span>
				</div>
				<div class="col-xs-12 plr0 mt10">
					<!--<span class="time_active fs12">距离结束：19分09秒1</span>-->
					<span class="orange fs12">开始时间：{{getStartTime}}</span>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
