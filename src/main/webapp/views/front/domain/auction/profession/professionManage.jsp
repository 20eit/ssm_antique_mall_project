﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场管理- AuctionProfessionAction:/ap/professionAddOrUpdate --%>
</head>
<body class="bg_gray">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="lh.jumpR('/userMerchantsIndex');">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">专场管理</span>
			</div>
			<div class="col-xs-3 pt10 plr7" onclick="lh.jumpR('/ap/page/articleAgreement');">
				<span>发布专场</span>
			</div>
		</div>
		<div class="row goodsSearch">
            <div class="col-xs-12 text-center">
                <div id="auction_pre" onclick="getMyProfessionList('pre');" class="col-xs-4 plr7 ptb10 active">未开始</div>
                <div id="auction_ing" onclick="getMyProfessionList('ing');" class="col-xs-4 plr7 ptb10"> 拍卖中</div>
                <div id="auction_done" onclick="getMyProfessionList('done');" class="col-xs-4 plr7 ptb10">已结束</div>
            </div>
		</div>
		<div id="data-container">
		
		</div>
	</div>
	<!-- 点击后弹出的菜单 -->
	<div class="spmc_bg">
		
	</div>
	<div class="spmc">
		<div class="spmc_menu">
			<div id="ap_update" onclick="updateAuction();" class="col-xs-3 plr0 text-center">
				<div>
					<img src="/images/front/managerMenu_01.png" class="img-responsive center-block">
				</div>
				<div class="pt5">修改</div>
			</div>
			<div id="ap_submit" onclick="submitAuction();" class="col-xs-3 plr0 text-center">
				<div>
					<img src="/images/front/managerMenu_02.png" class="img-responsive center-block">
				</div>
				<div class="pt5">提交</div>
			</div>
			<div id="ap_preview" onclick="previewAuction();" class="col-xs-3 plr0 text-center">
				<div>
					<img src="/images/front/managerMenu_03.png" class="img-responsive center-block">
				</div>
				<div class="pt5">预览</div>
			</div>
			<div id="ap_delete" onclick="confirmDelete();" class="col-xs-3 plr0 text-center">
				<div>
					<img src="/images/front/managerMenu_04.png" class="img-responsive center-block">
				</div>
				<div class="pt5">删除</div>
			</div>
		</div>
		<div class="spmc_quit fs16">
			取消
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<!-- <script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script> -->
	<!-- <script type="text/javascript" src="/base/js/common/common_upload.js" title="bv"></script> -->
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script src="/js/front/auction/profession/professionManage.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div onclick="switchCurrent('{{serial}}', '{{haveBegun}}');" class="row mt10 bg_white goodsClick">
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-10 plr0 pt5">
					<div class="gray pt5">【第{{auctionSerial}}场】</div>
					<div class="pt5">{{auctionName}}</div>
					<div class="gray pt5">开拍时间 : {{startTimeStr}}</div>
					<div class="gray pt5">{{#agCount}}共{{agCount}}件{{/agCount}}{{^agCount}}暂无拍品{{/agCount}}</div>
				</div>
				<div class="col-xs-2 plr0">
					{{&statusDom}}
					<div class="col-xs-12 plr0 gray text-right pt25">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
