﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场管理- AuctionProfessionAction:/ap/page/update --%>
</head>
<body>
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-2" onclick="lh.jumpR('/ap/page/addOrUpdate?apSerial=${apSerial}');">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-8 pt10 text-center">
				<span class="fs16">修改拍卖专场</span>
			</div>
			<div onclick="lh.jumpR('/ap/page/addOrUpdate?apSerial=${apSerial}');" class="col-xs-2 pt10 plr0">
				<span class="fs16">编辑</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场场次</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">【第${ap.auctionSerial}场】</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('auctionSerial');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场标题</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">${ap.auctionName}</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('auctionName');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场介绍</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">${ap.description}</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('description');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">开拍时间</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">
						<fmt:formatDate value="${ap.startTime}" pattern="yyyy-MM-dd HH:mm"/> 
					</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('startTime');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场类型</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">${ap.typeName}</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('typeName');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场类别</div>
				<div class="col-xs-9 plr0">
					<div class="col-xs-11 plr0 elli pt5 text-right gray">${ap.goodsTypeName}</div>
					<div class="col-xs-1 plr7 gray" onclick="promptUpdateItem('goodsTypeName');">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row h10 bg_gray"></div>
		<div id="data-container" class="row">

		</div>
		<div class="row">
			<div id="agNum" class="col-xs-12 gray ptb5">尚未添加拍品</div>
			<div class="col-xs-12 mt10">
				<div class="col-xs-12 plr0 pos-r bdda66">
					<div class="col-xs-12 ptb10" id="addGoodsBg">
						<div class="col-xs-2 pl7">
							<img src="/images/front/add.png" class="img-responsive">
						</div>
						<div class="col-xs-10 plr0 gray fs16">添加商品</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt20">
			<div onclick="doSubmit();" class="col-xs-12">
				<a role="button" class="btn btn-orange col-xs-12">提交审核</a>
			</div>
		</div>
	</div>
	<!-- 20160510 -->
	<div class="addGoodsFixedBg">
		
	</div>
	<div class="addGoodsFixedButton">
		<div onclick="lh.jumpR('/goods/page/add?from=ap-${apSerial}');" class="addGoodsNew">上传新拍品</div>
		<div onclick="lh.jumpR('/goods/page/store?from=ap-${apSerial}');" class="addGoodsChoose">从拍品库中选择</div>
		<div class="addGoodsquit">取消</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/base/js/common/common_upload.js" title="bv"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script> lh.param = ${paramJson}; </script>
	<script src="/js/front/auction/profession/professionGoodsAddOrUpdate.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="col-xs-12 ptb10 bdb" id="auction_goods_{{id}}">
			<div class="col-xs-4 plr0">
				<img class="img-responsive" src="{{getPicPath}}" /><!--/images/front/pic_01.png-->
			</div>
			<div class="col-xs-8 pl7 pr0">
				<div class="col-xs-11 plr0">
					<div class="blue_tint pt10">{{goodsName}}</div>
					<div class="col-xs-12 plr0 mt10 gray">
						起拍价：{{priceBegin}}元
					</div>
					<div class="col-xs-12 plr0 mt10 gray">拍品编号：{{id}}</div>
				</div>
				<div onclick="confirmRemove({{id}});" class="col-xs-1 plr0 pt20 gray">
					<i class="icon-angle-right icon-2x"></i>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
