﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>
	${ap.auctionName}
	（
	<c:if test="${ap.haveBegun == 2}"> 拍卖中</c:if>
	<c:if test="${ null ==  ap.haveBegun || ap.haveBegun == 1}"> 预展</c:if>
	）
</title>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
 <link rel="stylesheet" href="/css/front/wpk/bonus.css">
<%-- 拍卖大厅 - AuctionHallAction:/ap/hall/{auctionId} --%>
</head>
<body id="body">
	<div class="container-fluid">
		<%-- <div class="row auction_title">
			<div class="col-xs-1"onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<span class="fs16">${ap.auctionName}
				（
					<span id="have_begun_text">
						<c:if test="${ap.haveBegun == 2}"> 拍卖中</c:if>
						<c:if test="${ null ==  ap.haveBegun || ap.haveBegun == 1}"> 预展</c:if>
					</span>
				  ）
				</span>
			</div>
		</div> --%>
		<div class="row ptb5 orange">
			<div class="col-xs-12 text-center plr0 enter_scene">
				<div class="col-xs-1" onclick="lh.back();">
					<i class="icon-angle-left icon-3x" style="font-size:20px;"></i>
				</div>
				<div class="col-xs-3">
					<i class="icon-suitcase"></i>仅剩<span id="goodsRemain"></span>件
				</div>
				<div class="col-xs-4">
					<i class="icon-legal"></i>成交<span id="goodsDeal"></span>件
				</div>
				<div class="col-xs-4">
					<i class="icon-file-alt"></i>共计<span id="goodsTotal"></span>件
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 product plr0">
				<div class="col-xs-12 productMs">
					<div class="col-xs-4 plr0">
						<img id="picPath" class="img-responsive" src="${goods.picPath}" />
					</div>
					<div class="col-xs-8 pl7">
						<div id="goodsName" class="gray_deep orange"> ${goods.goodsName} </div>
						<div class="col-xs-12 plr0 mt5">
							<span class="pull-left">当前第<span id="currentNum"></span>件拍品</span><br/>
							<span class="pull-left">编号：<span id="goodsSerial"></span></span><!-- pull-right -->
						</div>
						<div class="col-xs-12 plr0 mt10 gray">
							<span class="pull-left">起拍价：<span id="priceBegin"></span>元</span>
							<span class="pull-right">保证金：<span id="creditMargin"></span>无</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row ptb5 orange">
			<div class="col-xs-12 text-center plr0 enter_scene_noborder">
				<div class="col-xs-4" style="padding-left:1px;padding-right:5px;">
					当前:<span id="currentPrice"></span>
				</div>
				<div class="col-xs-4 plr0" style="padding-left:0px;padding-right:0px;">
					领先:<span id="currentUser"></span>
				</div>
				<div class="col-xs-4" style="padding-left:5px;padding-right:1px;">
					出价:<span id="offerTimes"></span>次
				</div>
			</div>
		</div>
		<div class="row h10 bg_gray"></div>
		<div class="row fs16">
			<div class="col-xs-12">
				<div class="col-xs-12 bdb plr0 pt5">
					<div class="col-xs-6 pt5 plr0 fs14">
						<div class="col-xs-5 plr0">主持人：</div>
						<div class="col-xs-2 plr0"><!-- /images/front/pic_09.png -->
							<img class="img-responsive center-block" width="23" height="23" src="${ap.userAvatar}" />
						</div>
						<div class="col-xs-5 plr0">${ap.username}</div>
					</div>
					<div class="col-xs-6 text-right gray plr0 pb5">
						<!-- <div class="col-xs-10 pt5 plr0">在线100人</div>-->
						<div class="col-xs-2 plr0 fr" onclick="jumpToUserHome('${ap.userSerial}');"><i class="icon-angle-right icon-2x"></i></div> 
					</div>
				</div>
			</div>
		</div>
		
		<div id="msg_container" style="overflow:visible;">
			<!-- <div class="row">
				<div class="col-xs-12 fs16 pb10">08:53：...主持人</div>
				<div class="col-xs-12">
					<div class="col-xs-12">
						<p>第7件拍品“<span class="orange"> 日本Akoya阿古屋稀有珍珠吊坠</span>”，起拍价1元,运费15元。</p>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="col-xs-12"><p>七天无理由退换货</p></div>
				</div>
				<div class="col-xs-12">
					<div class="col-xs-12"><p>现在开始出价</p></div>
				</div>
			</div>
			
			<div class="row pb10">
				<div class="col-xs-12 fs16 pb10">08:53：...主持人</div>
				<div class="col-xs-12">
					<div class="col-xs-12">
						<img src="/images/front/pic_02.png" class="img-responsive" />
					</div>
				</div>
			</div> -->
		</div>
		<div class="row" style="margin-bottom:52px;"></div>
		<div class="row h50"></div>
	</div>
	
	<c:if test="${!empty isHost}">
		<div class="enter_scene_fixed">
			<div class="col-xs-2 plr0 pt5" id="keyboard">
				<img class="img-responsive center-block" src="/images/front/menu_07.png" />
			</div>
			<div class="col-xs-7 plr0 pt5">
				<input type="text" class="form-control" id="msgContent" />
			</div>
			<div class="col-xs-3 plr7 pt5" id="msgSend" onclick="sendMsg();">
				<a role="button" class="btn btn-orange col-xs-12">发送</a>
			</div>
		</div>
		<div class="enter_scene_fixed_change" style="height:auto;">
			<div class="col-xs-2 plr0 pt5" style="margin-top:4px;" id="keyboard">
				<img class="img-responsive center-block" src="/images/front/keyboard.png" />
			</div>
			<div class="col-xs-7 plr0 pt5">
				<input type="text" class="form-control" id="msgContent2" />
			</div>
			<div class="col-xs-3 plr7 pt5" id="msgSend2" onclick="transToMsgSend();">
				<a role="button" class="btn btn-orange col-xs-12">发送</a>
			</div>
			<div class="col-xs-12 plr0 pt10 text-center">
				<div onclick="sendGoodsPic();" class="col-xs-3">
					<img src="/images/front/menu_01.png" class="img-responsive center-block" />图片
				</div>
				<div onclick="sendGoodsDesc();" class="col-xs-3">
					<img src="/images/front/menu_02.png" class="img-responsive center-block" />描述
				</div>
				<div onclick="callCuation();" class="col-xs-3">
					<img src="/images/front/menu_03.png" class="img-responsive center-block" />叫场
				</div>
				<div onclick="sendBonus();" class="col-xs-3">
					<img src="/images/front/menu_04.png" class="img-responsive center-block" />红包
				</div>
			</div>
			<div class="col-xs-12 plr0 pt10 text-center">
				<div onclick="auctionfallThrough();" class="col-xs-3">
					<img src="/images/front/menu_05.png" class="img-responsive center-block" />流拍
				</div>
				<div onclick="auctionCountdown();" class="col-xs-3">
					<img src="/images/front/menu_06.png" class="img-responsive center-block" />计时
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${empty isHost}">
		<div class="enter_scene_fixed" id="sayWord" style="display:none;">
			<div class="col-xs-2 plr0 pt5" id="sayWordBtn" onclick="$('#sayWord').hide();$('#doOffer').show();">
				<img class="img-responsive center-block" src="/images/front/my_img4.png" width="40" />
			</div>
			<div class="col-xs-7 plr0 pt5">
				<input type="text" class="form-control" id="msgContent" />
			</div>
			<div class="col-xs-3 plr7 pt5" id="msgSend" onclick="sendMsg();">
				<a role="button" class="btn btn-orange col-xs-12">发送</a>
			</div>
		</div>
		
		<div class="enter_scene_fixed" id="doOffer">
			<div class="col-xs-2 plr0 pt5" style="margin-top: 4px;" id="doOfferBtn" onclick="$('#doOffer').hide();$('#sayWord').show();">
				<img class="img-responsive center-block" src="/images/front/keyboard.png" />
			</div>
			<div class="col-xs-7 plr0 pt10">
				<div class="col-xs-3" id="count_less" onclick="changePrice('del');">
					<i class="icon-minus"></i>
				</div>
				<div class="col-xs-6 fs16" id="amount"><!-- 800.54 -->
					<input id="offerPrice" onblur="changePrice();" type="number" readonly="readonly" class="count_input" style="width: 100%; border: none;text-align: center;" />
				</div>
				<div class="col-xs-3" id="count_add" onclick="changePrice('add');">
					<i class="icon-plus"></i>
				</div>
			</div>
			<div class="col-xs-3 plr7" id="offerSend" onclick="sendOffer();">
				<a role="button" class="btn btn-orange col-xs-12" style="margin-top: 5px;">出价</a>
			</div>
		</div>
	</c:if>
	<div class="allBg"></div>
	<div id="timmer_number" class="timmer_number frontHide" style=" position:fixed">10</div>
	
	<script src="http://app.cloopen.com/im50/ytx-web-im.min.js"></script>
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript">
		lh.param = ${paramJson}
	</script>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/js/front/chat/common_base_chat.js" title="v"></script>
	<script type="text/javascript" src="/js/front/chat/common_group_chat.js" title="v"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/js/front/auction/profession/professionHall.js" title="v"></script>
	<script id="template_other" type="x-tmpl-mustache">
	<div class="row">
		<div class="col-xs-12 fs16 pb10  {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.sendHour}}：{{chat.senderName}}</div>
		<div class="col-xs-12">
			<div class="col-xs-12">
				{{chat.content}}{{&chat.goodsPic}}{{&chat.price}}
			</div>
		</div>
	</div>
	</script>
	
	<script id="template_self" type="x-tmpl-mustache">
	<div class="row">
		<div class="col-xs-12 fs16 pb10  {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.sendHour}}：{{chat.senderName}}</div>
		<div class="col-xs-12">
			<div class="col-xs-12">
				{{chat.content}}{{&chat.goodsPic}}{{&chat.price}}
			</div>
		</div>
	</div>
	</script>
	
	<script id="template_bonus" type="x-tmpl-mustache">
	<div class="row">
		<div class="col-xs-12 fs16 pb10  {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.sendHour}}：{{chat.senderName}}</div>
		<div class="col-xs-12">
			<div class="col-xs-12">{{chat.content}}</div>
		</div>
	</div>
	<div class="col-xs-12 plr0 mt10 mb10" onclick="receiveBonus({{chat.senderId}});">
          <div class="col-xs-9 plr0 col-xs-offset-1">
              <div class="col-xs-12 plr0 ptb10 bonusMsg-top">
                  <div class="col-xs-4">
                      <img src="/images/front/bonus_01.png" class="img-responsive">
                  </div>
                  <div class="col-xs-8 plr0">
                      <div class="col-xs-12 plr0 white">恭喜发财，大吉大利！</div>
                      <div class="col-xs-12 plr0 mt10 white"> 领取红包</div>
                  </div>
              </div>
              <div class="col-xs-12 plr0 bonusMsg-bottom ptb5 gray">
                  <div class="col-xs-6"> 微信红包</div>
                  <div class="col-xs-6">
                      <img src="/images/front/bonus_02.png" width="18" class="img-responsive pull-right">
                  </div>
              </div>
          </div>
     </div>
	</script>
	
	
</body>
</html>
