﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="pz_main" style="margin-top: 48px;">
		<div class="c_0100_19">
			<div class="slide_sale" id="slide_sale">
				<div class="bd">
					<div class="ul_show">
						<div style="text-align: center;line-height: 50px;font-size: 20px;font-weight: bold;color: #FFFFFF;">
							<a href="tel:${ap.instTel}">
								<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;">打电话</button>
							</a>
							<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;margin-left:20px;" onclick="lh.jumpR('/chat/${ap.userId}');">文字聊天</button>
							<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;margin-left:20px;" onclick="lh.jumpR('/user');">订单管理</button>
						</div>
						<div id="goodsList" class="sale_show">
							<!-- 主数据加载区域 -->
						</div>
					</div>
				</div>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<input type="hidden" id="auctionId" value="${ap.id}">
	<input type="hidden" id="instTel" value="${ap.instTel}">
	<input type="hidden" id="auctionName" value="${ap.auctionName}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/auction/profession/myAuctionGoods.js" title="v"></script>

	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="listday" style="padding:15px 5px;">
			<div class="pointer" style="max-height:600px;overflow: hidden;margin-bottom:5px;text-align: center;">
				<img src="{{ picPath }}" style="max-width:96%;min-width: 30%" />
			</div>
			<div class="listdaytime" style="padding:8px;">
				<table class="widthMax">
					<tbody>
						<tr style="height: 25px;"><td class="fr">藏品名称：{{ goodsName }}</td><td class="fl">拍场场次：{{ getAuctionName }}</td></tr>
						<tr style="height: 25px;"><td class="fr">金额：<strong>{{ offerPrice }}元</strong></td><td class="fl">拍下时间：{{ getDealTime }}</td></tr>
						<tr style="height: 25px;"><td class="fr">状态：{{ getStatus }}</td><td class="fl">电话：{{ getInstTel }}</td></tr>
					</tbody>
				</table>
			</div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
