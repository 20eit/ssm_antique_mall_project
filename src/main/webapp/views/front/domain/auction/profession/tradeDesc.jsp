﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/tiaokuan.css" title="v" />
</head>
<body style="margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div style="background:white;min-width:320px;max-width:480px;margin:0 auto;">
		<ul class="zctiaok">
			<li>1. 藏品图片必须保证其清晰度，需上传多个角度的照片；</li>
			<li>2. 藏品必须属于古玩艺术品范畴，不得出售违禁品，出售违禁品后果自负；</li>
			<li>3. 藏品非全品时需在描述中说明，否则交易因此出现责任由卖方承担；</li>
			<li>4. 卖家在发货时请让快递员与货物合照，藏品可保价；买家请当着快递员面亲自核对藏品无损坏，尽量不要托人代签收，避免纠纷；</li>
			<li>5. 购买及退货运费默认由买家支付，买卖双方亦可沟通协商，如所发藏品与描述不相符（仅限品相），则退货运费由卖家支付；</li>
			<li>6. 买家多次报价并且卖家同意报价后不购买，将依情节进行警告、封号等处理，并记不良记录；</li>
			<li>7. 买家付款后，卖家如在七天内未发货，将依情节进行警告、封号等处理，并记不良记录；</li>
			<li>8. 成交后，请在微拍客上进行免费担保交易，支持七天无理由退换货，避免受骗；如线下交易微拍客不承担任何责任，并记不良记录；</li>
			<li>9. 微拍客平台支持七天无理由退换货，卖家必须遵循此条款；</li>
			<li>10. 以上条款对买卖双方皆有效，买卖双方保证严格遵守微拍客已发布及未发布的交易条款。</li>
		</ul>
		<div style="height: 80px;"></div>
		<div class="save bottomFix" >
			<ul>
				<li><a href="javascript:;" class="a_say" id="" onclick="history.back();">关闭</a></li>
			</ul>
		</div>
	</div>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
</body>
</html>
