﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 新增拍卖专场前选择专场类型- AuctionProfessionAction:/ap/page/typeChoose --%>
</head>
<body class="bg_gray">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-1" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<span class="fs16">请选择专场类型</span>
			</div>
		</div>


		<div class="row pt10">
			<div class="col-xs-12">
				<div onclick="lh.jumpR('/ap/page/addOrUpdate?typeId=1');" class="col-xs-12 bg_white ptb10 bdrs4">
					<div class="col-xs-12 plr0 fs16">普通专场</div>
					<div class="col-xs-12 plr0 gray pt10">无需保证金</div>
				</div>
			</div>
		</div>

		<!-- 这里需求从后台加载出来  -->
		<div class="row mt10 pt10">
			<div class="col-xs-12">
				<div onclick="lh.jumpR('/ap/page/addOrUpdate?typeId=2');" class="col-xs-12 bg_white ptb10 bdrs4">
					<div class="col-xs-12 plr0 fs16">精品专场</div>
					<div class="col-xs-12 plr0 gray pt10">卖家预展前：专场保证金缴足至1000元<br> 买家参拍时：专场保证金缴足至100元</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/auction/profession/auctionTypeChoose.js" title="v"></script>

</body>
</html>
