﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<style type="text/css">
h1 {
	margin: auto;
}

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
	background-color: #fff;
	opacity: 1;
}

.weui-picker-modal {
	min-height: 220px;
}

.weui-picker-modal .picker-modal-inner {
	min-height: 220px;
}

.weui-picker-modal .picker-item {
	font-size: 20px;
}

.weui-picker-modal .toolbar-inner {
	height: 40px;
}

.weui-picker-modal .title {
	font-size: 20px;
	line-height: 40px;
}

.weui-picker-modal .picker-button {
	font-size: 18px;
	line-height: 40px;
}
.weui-picker-container p span{padding-left:10px;color:gray;font-size:smaller;}
/**weui,bootstrap冲突*/
</style>
<%-- 新增拍卖专场- AuctionProfessionAction:/ap/page/dd --%>
</head>
<body>
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-1" onclick="lh.jumpR('/ap/page/manage');">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-10 pt10 text-center">
				<c:choose>
					<c:when test="${empty ap}">
						<span class="fs16">发起拍卖专场</span>
					</c:when>
					<c:otherwise>
						<span class="fs16">修改拍卖专场</span>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 ptb5"><!-- bdb -->
				<!-- <div class="col-xs-3 plr0 pt5">专场标题：</div> -->
				<div class="col-xs-12 plr0">
					<input id="auctionName" type="text" class="form-control" placeholder="请输入专场标题" style="border: none; text-align: center;" />
				</div>
			</div>
			<div class="col-xs-12 ptb5"><!-- bdb -->
				<!-- <div class="col-xs-3 plr0 pt5">专场介绍：</div> -->
				<div class="col-xs-12 plr0">
					<!-- <input id="description" type="text" class="form-control" placeholder="请输入专场介绍 " style="border: none; text-align: center;" /> -->
					<div class="weui_cells weui_cells_form">
						<div class="weui_cell">
							<div class="weui_cell_bd weui_cell_primary">
								<textarea id="description" class="weui_textarea" style="border: none; text-align: center;" placeholder="请输入专场介绍" rows="3"></textarea>
								<div class="weui_textarea_counter">
									<!-- <span>0</span>/200 -->
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div class="col-xs-12 ptb5"><!-- bdb -->
				<!-- <div class="col-xs-3 plr0 pt5">开拍时间：</div> -->
				<div class="col-xs-12 plr0">
					<input id="startTime" type="text" class="form-control" placeholder="请选择开拍时间 " style="border: none; text-align: center;" />
				</div>
			</div>
			<div id="datePlugin"></div>
			<div class="col-xs-12 ptb5"><!-- bdb -->
				<!-- <div class="col-xs-3 plr0 pt5">拍品类别：</div> -->
				<div class="col-xs-12 plr0">
					<input id="goodsType" type="text" class="form-control" placeholder="请选择拍品类别 " style="border: none; text-align: center;" />
				</div>
			</div>
			<div class="col-xs-12 h10 bg_gray"></div>
			<div class="col-xs-12 bdb ptb5">
				<div class="col-xs-3 plr0 pt5">专场背景：</div>
				<div class="col-xs-9 plr0 pos-r bdda66">
					<input onclick="choosePic({showPic:true,count:1})" accept="image/jpg,image/jpeg,image/png,image/gif" type="text" class="form-control fileUp" id="picPaths" />
					<div class="col-xs-12 plr0 fileAdd">
						<div class="col-xs-2 ptb10 plr7">
							<img src="/images/front/add.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>

			<div id="fileUpload">
				<!-- 展示已上传图片 -->
			</div>

		</div>
		<div class="row pt20">
			<div onclick="addOrUpdateAuctionProfession();" class="col-xs-12">
				<a role="button" class="btn btn-orange col-xs-12">下一步</a>
			</div>
		</div>
	</div>

	<input type="hidden" value="${r}" id="r" />
	<input type="hidden" value="${loginStatus}" id="loginStatus" />
	<input type="hidden" id="shopId" value="${shopId}">
	<input type="hidden" id="from" value="${from}">

	<input type="hidden" id="openId" value="${openId}">
	<input id="appId" type="hidden" value="${appId}" />
	<input id="timeStamp2" type="hidden" value="${timeStamp}" />
	<input id="nonceStr2" type="hidden" value="${nonceStr}" />
	<input id="signature" type="hidden" value="${signature}" />

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<!-- <script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script> -->
	<!-- <script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script> -->
	<!-- <script type="text/javascript" src="/base/js/common/common_upload.js" title="bv"></script> -->
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script>lh.param = ${paramJson};</script>
	<!-- <script>lh.param = ${paramJson};</script> -->
	<script src="/js/front/auction/profession/professionAddOrUpdate.js" title="v"></script>
</body>
</html>
