﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="announcetop">公告列表</div>
	<div id="announcont" class="announcont">
		<ul>
			<c:forEach items="${annoList}" var="anno">
				<li>
					<a href="javascript:lh.jumpR('/instAnnoDetail?id=${anno.id}');">
						<span class="announrq"><fmt:formatDate value="${anno.createdAt}" pattern="yyyy年MM-dd"/></span>
						<div class="announdiv">
							<span class="annound1">${fn:substring(anno.title, 0,14)}<c:if test="${fn:length(anno.title)>14}">...</c:if></span><br/> 
							<span class="annound2">${fn:substring(anno.content, 0,14)}<c:if test="${fn:length(anno.content)>14}">...</c:if></span>
						</div>
					</a>
					<c:if test="${!empty manager}">
						<span style="float: right;position: relative;bottom: 48px;right: 40px;">
							<button type="button" class="btn weui_btn_primary" style="color:#920808;" onclick="deleteInstAnno(${anno.id});">删除</button>
							<button type="button" class="btn weui_btn_primary" style="color:#920808;" onclick="lh.jumpR('/instAnnoAddOrUpdate?instId=${inst.id}&annoId=${anno.id}');">修改</button>
						</span>
					</c:if>
				</li>
			</c:forEach>
		</ul>
	</div>
	<c:if test="${!empty manager}">
		<div class="save bottomFix" id="annoAdd">
			<ul>
				<li><a href="javascript:lh.jumpR('/instAnnoAddOrUpdate?instId=${inst.id}');" class="a_say">发布公告</a></li>
			</ul>
		</div>
	</c:if>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%>
	<!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%>
	<!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script src="/js/front/auction/profession/inst.js" title="v"></script>
</body>
</html>
