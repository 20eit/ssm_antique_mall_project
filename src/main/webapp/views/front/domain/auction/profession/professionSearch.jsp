﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场拍品列表 - AuctionHallAction:/ap/agList/{auctionId}--%>
</head>
<body>
	 <div class="container-fluid">
        <div class="row auction_title">
            <div class="col-xs-1">
                <i class="icon-angle-left icon-3x"></i>
            </div>
            <div class="col-xs-8 pt5 text-center">
                <input type="text" class="form-control" id="Search" />
            </div>
            <div class="col-xs-2 plr0 pt5">
                <a role="button" class="btn btn-default">搜索</a>
            </div>
        </div>
        <div class="row ptb10">
            <div class="col-xs-12 text-center plr0 search_menu">
                <div class="col-xs-10">
                    <div class="col-xs-4 plr7">
                        <div class="col-xs-12 plr0">
                            翡翠
                        </div>
                    </div>
                    <div class="col-xs-4 plr7">
                        <div class="col-xs-12 plr0">
                            珍珠
                        </div>
                    </div>
                    <div class="col-xs-4 plr7">
                        <div class="col-xs-12 plr0">
                            和田玉
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 gray pt5">
                    <i class="icon-list icon-large"></i>
                </div>
            </div>
        </div>
        <div class="row bg_gray pt5">
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_01.png" />
                <div class="col-xs-12 plr0 bg_white red fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_02.png" />
                <div class="col-xs-12 plr0 bg_white fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_03.png" />
                <div class="col-xs-12 plr0 bg_white fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_04.png" />
                <div class="col-xs-12 plr0 bg_white fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_04.png" />
                <div class="col-xs-12 plr0 bg_white fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 pl0 pr7 pb5">
                <img class="img-responsive" src="/images/front/goods_04.png" />
                <div class="col-xs-12 plr0 bg_white fs13">
                    碧玉尊 冰糯种翡翠霸王貔貅 吊坠
                </div>
                <div class="col-xs-12 plr0 bg_white">
                    <div class="col-xs-4 plr0 red">
                        ￥<span class="fs18">899</span>
                    </div>
                    <div class="col-xs-8 plr0">
                        <span class="fs13">倒计时</span><span class="red">00:07:56</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <input type="hidden" value="${r}" id="r"/> 	
    <input type="hidden" value="${loginStatus}" id="loginStatus"/>
    <input type="hidden" value="${status}" id="status"/>
    <input type="hidden" value="${auctionId}" id="auctionId"/>
    <input type="hidden" value="${forbidden}" id="forbidden"/>
    
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/js/front/auction/profession/professionGoods.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache"></script>

</body>
</html>
