﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%-- <%@ include file="/views/common/common_css.htm"%> --%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<%-- 拍卖专场拍品详情 - AuctionHallAction://ag/{auctionId}/{goodsId}--%>
</head>
<div class="container-fluid">
	<div class="row auction_title">
		<div class="col-xs-1" onclick="lh.back();">
			<i class="icon-angle-left icon-3x"></i>
		</div>
		<div class="col-xs-10 pt10 text-center">
			<span class="fs16">开始时间：<fmt:formatDate pattern="yyyy-MM-dd hh:mm" value="${ap.startTime}" /></span>
		</div>
	</div>
	<div class="row">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<c:forEach items="${picList}" var="pic" varStatus="s">
					<c:choose>
						<c:when test="${s.index == 0}">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						</c:when>
						<c:otherwise>
							<li data-target="#carousel-example-generic" data-slide-to="${s.index}"></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ol>
			<div class="carousel-inner" role="listbox">
				<c:forEach items="${picList}" var="pic" varStatus="s">
					<c:choose>
						<c:when test="${s.index == 0}">
							<div class="item active">
								<img class="img-responsive" src="${pic.picPath}" />
							</div>
						</c:when>
						<c:otherwise>
							<div class="item">
								<img class="img-responsive" src="${pic.picPath}" />
							</div>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
		</div>
	</div>
	<div class="row mt10 mb10 fs16">
		<div class="col-xs-12">${goods.goodsName}</div>
		<div class="col-xs-12" style="font-size: 15px;">${goods.goodsBrief}<c:if test="${empty goods.goodsBrief}">${goods.goodsDescription}</c:if></div>
		<div class="col-xs-12 plr0 ptb10">
			<div class="col-xs-6">
				当前价<span class="orange fs18">￥${auctionGoods.offerPrice}<c:if test="${empty auctionGoods.offerPrice}">${auctionGoods.priceBegin}</c:if></span>
			</div>
			<div class="col-xs-6">
				<span class="gray">起拍价￥${auctionGoods.priceBegin}<c:if test="${null == auctionGoods.priceBegin}">0</c:if></span>
			</div>
		</div>
		<div class="col-xs-12 text-center plr0 sale_commodity_detail">
			<div class="col-xs-4">
				围观<span>${auctionGoods.scansCount}<c:if test="${null == auctionGoods.scansCount}">1</c:if></span>次
			</div>
			<div class="col-xs-4">
				出价<span>${auctionGoods.offerTimes}<c:if test="${null == auctionGoods.offerTimes}">0</c:if></span>次
			</div>
			<div class="col-xs-4">
				运费<span>${goods.postageFee}<c:if test="${null == auctionGoods.postageFee}">0.00</c:if></span>元
			</div>
		</div>
	</div>
	<div class="row fs16">
		<div class="col-xs-12 h10 bg_gray"></div>
		<div class="col-xs-12">
			<div class="col-xs-12 bdb plr0">
				<div class="col-xs-6 pt5 plr0">
					拍卖纪录|<span>${apoListSize}</span>条
				</div>
				<div class="col-xs-6 text-right gray plr0">
					<i class="icon-angle-right icon-2x"></i>
				</div>
			</div>
		</div>
	</div>
	<div id="apoContainer" class="row">
		<c:forEach items="${apoList}" var="apo">
			<div class="col-xs-12 text-center fs14 ptb5"><!-- 第一条添加类：orange -->
				<div class="col-xs-5 plr0">
					<div class="col-xs-2 plr0">
						<i class="icon-thumbs-down"></i>
					</div>
					<div class="col-xs-4 plr0">领先</div>
					<div class="col-xs-6 plr0">${apo.offerUsername}<c:if test="${empty apo.offerUsername}">匿名</c:if></div>
				</div>
				<div class="col-xs-7 plr0">
					<div class="col-xs-6 plr0">
						￥<span>${apo.offerPrice}</span>
					</div>
					<div class="col-xs-6 plr0"><fmt:formatDate value="${apo.offerAt}" pattern="yyyy-MM-dd"/>  </div>
				</div>
			</div>
		</c:forEach>
		<div class="col-xs-12 h10 bg_gray"></div>
	</div>
	<div class="row fs16">
		<div class="col-xs-12">
			<div class="col-xs-12 plr0 ptb5 bdb elli">【第${ap.auctionSerial}场】${ap.auctionName}</div>
		</div>
		<div class="col-xs-12">
			<div class="col-xs-8 text-center ptb5 plr0" onclick="lh.alert('跳转到用户页面');return;lh.jumpR('/user/${ap.userSerial}');">
				<div class="col-xs-4 plr0">
					<img class="img-responsive center-block" src="${ap.userAvatar}" />
					<!-- /images/front/pic_09.png -->
				</div>
				<div class="col-xs-4 plr0 pt20">${ap.username}</div>
				<div class="col-xs-2 plr0 pt20">
					<img class="img-responsive" src="/images/front/pic_10.png" />
				</div>
			</div>
			<div class="col-xs-4 pt15 plr0" onclick="toggleFocus('${ap.userSerial}');">
				<a id="focusBtn" role="button" class="btn btn-orange col-xs-12" state="1">关注</a>
			</div>
		</div>
	</div>

	<div class="row bg_tint_gray ptb10">
		<div class="col-xs-12">————— 拖动，查看商品详情 —————</div>
		<div class="col-xs-12 pt10">
			<img class="img-responsive center-block" src="/images/front/pic_11.png" />
		</div>
	</div>

	<div class="row h50"></div>
</div>
<div class="auction_fixed">
	<div class="col-xs-2" onclick="lh.jumpR('/chat/${ap.userSerial}?gSerial=${goods.serial}');">
		<img class="img-responsive center-block" src="/images/front/pic_04.png" />咨询
	</div>
	<div class="col-xs-2" onclick="toggleNotice();">
		<img class="img-responsive center-block" src="/images/front/pic_05.png" /><span id="beginNotice" state="1">提醒</span>
	</div>
	<div class="col-xs-2" onclick="showShare();">
		<img class="img-responsive center-block" src="/images/front/pic_06.png" />分享
	</div>
	<c:choose>
		<c:when test="${leftMoney > 0 }">
			<div onclick="jumpToPayCreditMoney('${ap.serial}');" class="col-xs-6">去支付 <br/>(保证金${ap.userBail})</div>
		</c:when>
		<c:otherwise>
			<div onclick="lh.jumpR('/ap/hall/${ap.serial}');" class="col-xs-6">进入现场 <br/>
			<c:if test="${empty ap.userBail}">(无保证金)</c:if>
			<c:if test="${!empty ap.userBail}">(保证金${ap.userBail})</c:if>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<!-- 图片展示 结束 -->
<%@ include file="/views/common/common_share.htm"%><!-- 分享dom -->

<%@ include file="/views/common/common_js.htm"%>
<%@ include file="/views/common/common_front_js.htm"%>
<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
<%@ include file="/views/common/common_front_wpk_js.htm"%>
<script> lh.param = ${paramJson}; </script>
<script src="/js/front/auction/profession/common_auction_goods.js" title="v"></script>
<script src="/js/front/auction/profession/professionGoodsDetail.js" title="v"></script>
</body>
</html>
