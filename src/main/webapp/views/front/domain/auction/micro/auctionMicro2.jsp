﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/tiaokuan.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/call.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<div class="pz_top">
		<div class="c_0100_1">
			<div class="t_0100_1">
				<div class="tt_one">
					<div class="l_118">
						<a href="javascript:;" id="ss1" class="sstext">藏品&nbsp;<img src="/images/front/top_img5.png" width="5" height="4" /></a>
					</div>
					<div class="r_305">
						<div class="l_205">
							<input name="" type="text" placeholder="输入藏品名称" class="bfom" id="ss1_input" />
						</div>
						<div class="r_18">
							<a href="javascript:void(0);"><input name="" type="image" src="/images/front/top_img6.png" width="18" height="18" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="threepoint">
				<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
				<div class="pf_point" style="display: none;">
					<div class="img_point">
						<img src="/images/front/top_img2.png" width="8" height="7" />
					</div>
					<div class="ulist_point">
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/addGoods?from=am');">发布拍品</a>
						</div>
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/myAuctionMicro');">拍品管理</a>
						</div>
					</div>
				</div>

			</div>
			<div class="class">
				<a href="javascript:;" onclick="$('.pf_menu,.class').show(200);"><img src="/images/front/top_img3.png" width="18" height="13" />&nbsp;&nbsp;分类</a>
			</div>
		</div>
	</div>
	<div class="pz_main">
		<div id="auctionMicroList" class="c_0100_23">
			<!-- 藏品容器 -->
		</div>
		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
		
	<div id="offerWin" class="szweituo" style="display: none;z-index:1;">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<h4>微拍出价</h4>
				<div class="szwtinput">
					<div id="offerTip" class="szwtzuigao">当前出价：<span id="currentOfferPrice" class="colorRed"></span></div>
					<div id="offerIptDiv" class="szwtinpk">
						<form id="entrustFrom">
							<span> <input type="number" id="topPrice" placeholder="请在此输入您的出价" value="">
							</span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="offerCancel" onclick="doOffer('cancel')" class="szwtbutr entrusta2 pointer">取消</span> 
						<span id="offerSave" onclick="doOffer('save')" class="szwtbutl entrusta1 pointer">确定</span>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtwhat">提示：</div>
					<div class="szwtmiaosu">您的出价不能低于当前出价</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- <div class="weui_dialog_alert" id="payWayAlertDialog" style="display: block;">
        <div class="weui_mask"></div>
        <div class="weui_dialog">
            <div id="payWay_title" class="weui_dialog_hd"><strong class="weui_dialog_title">选择支付方式</strong></div>
            <div id="payWay_msg" class="weui_dialog_bd">
				<a href="javascript:;" onclick="" class="weui_btn weui_btn_primary">余额支付</a>
				<a href="javascript:;" onclick="" class="weui_btn weui_btn_primary">微信支付</a>
			</div>
            <div class="weui_dialog_ft">
                <a href="javascript:;" onclick="$('#payWayAlertDialog').hide();" class="weui_btn_dialog default">取消</a>
            </div>
        </div>
    </div> -->
	
	<div class="zhezhao frontHide" id="shareMask" onclick="hideShare();"></div>
    <div class="pfreturnforum frontHide" id="share" onclick="hideShare();" style="top:0%;margin-top:0px;">
    	<div class="share">
		 	 喜欢就点击右上角图标分享吧
		</div>
    </div>
    
    <a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
    
    <%@ include file="/views/front/common/z_div_call_ambid.htm"%><!-- 出价面板 -->
    
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
    <input type="hidden" value="${creditMoney}" id="creditMoney"/> 	
    <input type="hidden" id="asId" value="${asId}">
	<input type="hidden" id="auctionId" value="${auctionId}">
	<input type="hidden" value="${noPhone}" id="noPhone">
	<input type="hidden" value="${noPayPassword}" id="noPayPassword">

	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript">
		lh.param = ${paramJson}
	</script>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/js/common/call.js" title="v"></script>
	<script type="text/javascript" src="/js/front/auction/micro/auctionMicro.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	<div id="am_{{id}}" class="li_day">
		<input type="hidden" id="increaseRangePrice_{{id}}" value="{{increaseRangePrice}}">
		<input type="hidden" id="shopPrice_{{id}}" value="{{shopPrice}}">
		<div class="t_0100_29" style="border:none;">
			<div class="l_81">
				<a href="javascript:lh.jumpR('/sale/{{userId}}');"><img src="{{userAvatar}}" width="100%" onerror="this.src='/images/front/default_avatar.jpg'" /></a>
			</div>
				
			<div class="l_380">
				<div class="t_380_1">
					<a href="javascript:lh.jumpR('/sale/{{userId}}');"><nobr>{{username}}
					{{#creditMargin}}
						<img src="/images/front/shop_img4.png" width="20" height="20" style="background-color: rgba(146, 8, 8, 1);"/>&nbsp;<span style="color: #c42905;">已缴纳信誉保证金:{{creditMargin}}元</span>
					{{/creditMargin}}
					{{^creditMargin}}
						<span style="color: #747474;">未缴纳信誉保证金</span>
					{{/creditMargin}}
					</nobr></a>
				</div>
				<div class="t_380_2">{{provinceName}}{{cityName}} {{date}}</div>
			</div>
			<div class="r_gzbox">
				<div class="r_gz">
					{{&focusDom}}
				</div>
			</div>
		</div>
		<div class="t_0100_30">
			<div class="tit_day">{{goodsName}}</div>
			<div id="desc_{{id}}" role="1" class="text_day" style="max-height: 300px;height:auto">
				<!--<a style="float:right;color:#c42905" href="javascript:void(0);" onclick="toggleDescription({{id}});">展开</a>-->
				<p style="white-space: pre;">{{goodsDescription}}</p>
			</div>
<!--
			<div class="all_day">
				<a style="float:right;color:" href="javascript:void(0);" onclick="toggleDescription({{id}});">全文</a>
			</div>
-->
		</div>
<!--
		<div class="t_0100_31">
			<div class="price_box">
				<div class="l_456">
					起拍价：<span class="span_10">￥{{priceBegin}}</span>
				</div>
				<div class="r_220">保证金：{{#bail}}￥{{bail}}{{/bail}}{{^bail}}无{{/bail}} </div>
			</div>
			<div class="price_box">
				<div class="l_456">
					当前价：<span id="top_offer_{{id}}" class="span_10" style="color: #c42905;">￥
							{{^offerPrice}}0{{/offerPrice}}
							{{#offerPrice}}{{offerPrice}}{{/offerPrice}}
						  </span>
				</div>
				<div class="r_220" >运&nbsp;&nbsp;&nbsp;&nbsp;费：
						<span style="color: #c42905;font-size: 18px;"> 
							{{^postageFee}}买家支付{{/postageFee}}
							{{#postageFee}}
							<img src="/images/front/shop_img4.png" width="20" height="20" style="background-color: rgba(146, 8, 8, 1);"/>&nbsp;包邮&nbsp;&nbsp;&nbsp;
							{{/postageFee}}
						</span>
				</div>
			</div>
			<div class="price_box">
				<div class="l_456">
					一口价：<span id="top_offer_{{id}}" class="span_10" style="color: #c42905;">￥
							{{^bugoutPrice}}无{{/bugoutPrice}}
							{{#bugoutPrice}}{{bugoutPrice}}{{/bugoutPrice}}
						  </span>
				</div>
				<div class="r_220">
					{{#isSevenReturn}}
					<span class="span_10" style="color: #c42905;">
						<img src="/images/front/shop_img3.png" width="20" height="20" style="background-color: rgba(146, 8, 8, 1);"/>&nbsp;七天包退换&nbsp;&nbsp;&nbsp;
					</span>
					{{/isSevenReturn}}
				</div>
			</div>
		</div>
-->
		<div class="t_0100_32" style="padding:0px 0px 0px 3px;">
			<ul {{&picsUrlsDom}}>
				{{&picsDom}}
			</ul>
		</div>
		<div class="t_0100_33">
			<div class="l_sheet"><!--共{{picNum}}张-->
				<div class="r_220" >
						<span style="color: #c42905;"> 
							{{^postageFee}}买家支付运费{{/postageFee}}
							{{#postageFee}}
							<img src="/images/front/shop_img4.png" width="20" height="20" style="background-color: rgba(146, 8, 8, 1);margin-bottom:3px;"/>包邮
							{{/postageFee}}
						</span>
						{{^postageFee}}不支持退换货{{/postageFee}}
						{{#isSevenReturn}}
							<span class="span_10" style="color: #c42905;">
								<img src="/images/front/shop_img3.png" width="20" height="20" style="background-color: rgba(146, 8, 8, 1);margin-bottom:3px;"/>七天包退换
							</span>
						{{/isSevenReturn}}
				</div>
			</div>
			<div class="r_480">
				{{^userPraiseId}}
				<div id="praise_btn_{{id}}" role="1" class="li_rday pointer" style="width:35px;" onclick="togglePraise({{id}},${userId});">
					<div class="img_rady" >
						<a href="javacript:;" class="ombg3"></a>
					</div>
					<!--<span id="praise_text_{{id}}" class="tit_rday">赞</span>-->
				</div>
				{{/userPraiseId}}
				{{#userPraiseId}}
				<div id="praise_btn_{{id}}" role="2" class="li_rday pointer" style="width:35px;" onclick="togglePraise({{id}},${userId});">
					<div class="img_rady" style="margin-left:0px;">
						<a href="javacript:;" class="ombg3"></a>
					</div>
					<!--<span id="praise_text_{{id}}" class="tit_rday">取消点赞</span>-->
				</div>
				{{/userPraiseId}}
				<div class="li_rday" onclick="showShare();">
					<div class="img_rady">
						<div class="bshare-custom">
							<div class="bsPromo bsPromo2"></div>
							<a title="更多平台" href="javascript:void(0);" class="ombg4 bshare-more"></a>
						</div>
					</div>
					<span class="tit_rday">分享</span>
				</div>
				{{!
				<div class="li_rday">
					<div class="img_rady">
						<a href="javascript:void(0);" class="ombg5"></a>
					</div>
					<span class="tit_rday">群发</span>
				</div>
				}}
			</div>
		</div>
		<!--点赞用户头像-->
		<div id="praise_container">
		{{#userPraiseList}}
			<div id="praise_{{id}}" class="li_day" style="height:60px;overflow:hidden;width:54px;margin:0px;">
				<div class="t_0100_29" style="border:none;padding:2px;">
					<div class="l_81">
						<a href="javascript:jumptToUrl('/sale/{{userId}}');"><img src="{{&userAvatar}}" width="100%" /></a>
					</div>
				</div>
			</div>
		{{/userPraiseList}}
		</div>
		<div class="t_0100_35">
			<div class="l_24_1">
				<img src="/images/front/day_img8.png" width="24" height="24" />
			</div>
			<div class="l_24_2">倒计时：</div>
			<div class="l_24_3" id="ctdown_hour_{{id}}"></div>
			<div class="l_24_4">:</div>
			<div class="l_24_3" id="ctdown_minute_{{id}}"></div>
			<div class="l_24_4">:</div>
			<div class="l_24_3" id="ctdown_second_{{id}}"></div>
		</div>
		
		<div class="t_0100_36">
			<div class="l_326">
				<a href="javascript:void(0);" onclick="doOffer('show',{{id}},'{{bail}}');" class="a_outprice">
					<img src="/images/front/day_img9.png" width="22" height="22" />&nbsp;出价
				</a>
			</div>
			<div class="r_326">
				<a href="javascript:void(0);" onclick="lh.jumpR('/chat/{{userId}}?gId={{goodsId}}');" class="a_outprice">
					<img src="/images/front/day_img10.png" width="22" height="22" />&nbsp;咨询
				</a>
			</div>
		</div>
		<div>	
			
		<div class="t_0100_31">
			<div class="price_box">
				<div class="l_456">
					起拍价 <span class="span_10">￥{{priceBegin}}</span>
				</div>
				<div class="r_220">
					<span style="color: #343333;">保证金 </span>
						<span id="credit_{{id}}" class="span_10" style="color: #55ae05;font-size: 18px;font-weight: bold;">
							{{^bail}}无{{/bail}}
							{{#bail}}￥{{bail}}{{/bail}}
						  </span>
				</div>
			</div>
			<div class="price_box">
				<div class="l_456">
					当前价 <span id="top_offer_{{id}}" class="span_10" style="color: #c42905;">{{^offerPrice}}￥0{{/offerPrice}}{{#offerPrice}}￥{{offerPrice}}{{/offerPrice}}</span>
				</div>
				<div class="r_220">
					<span style="color: #343333;">一口价 </span>
					<span id="top_offer_{{id}}" class="span_10" style="color: #c42905;font-size: 18px;font-weight: bold;">
							{{^buyoutPrice}}￥无{{/buyoutPrice}}
							{{#buyoutPrice}}￥{{buyoutPrice}}{{/buyoutPrice}}
						  </span>
				</div>
			</div>
		</div>
			

		</div>

		<div id="offer_container_{{id}}" class="t_0100_37">
			{{#asoList}}
			<div id="offer_{{id}}" class="li_prlist" style="font-size:16px;">
				<div class="l_60_4">
					<img src="{{userAvatar}}" onclick="location.href='lh.jumpR('/sale/{{userId}}');" width="100%" />
				</div>
				<div class="l_410" style="width:auto;">
					<div class="t_410_1" style="margin-top:10px;font-size:16px;">
						<a href="javascript:lh.jumpR('/sale/{{userId}}');">
							<span style="font-size:16px;">{{username}}</span>
						</a>
					</div>
				</div>
				<div class="r_169" style="margin:12px 0px 0px 5px;width:auto;font-size:16px;">{{offerAt}}</div>
				<div style="float:right;margin:8px 5px 0px 0px;">
					<span>￥<span style="color:#c42905;font-size:16px;">{{offerPrice}}&nbsp;&nbsp;</span></span>
					{{#priceLead}}<span style="color:green;font-size:16px;">领先</span>{{/priceLead}}
					{{^priceLead}}<span style="color:gray;font-size:16px;">淘汰</span>{{/priceLead}}
				</div>
			</div>
			{{/asoList}}

		</div>
	</div>
	{{/rows}}	 		
	</script>
	
	<script id="template_offers" type="x-tmpl-mustache">
	{{#rows}}	 	
		<div id="offer_{{id}}" class="li_prlist" style="font-size:16px;">
				<div class="l_60_4">
					<img src="{{userAvatar}}" onclick="lh.jumpR('/sale/{{userId}}');" width="100%" />
				</div>
				<div class="l_410" style="width:auto;">
					<div class="t_410_1" style="margin-top:10px;font-size:16px;">
						<a href="javascript:lh.jumpR('/sale/{{userId}}');">
							<span style="font-size:16px;">{{username}}</span>
						</a>
					</div>
				</div>
				<div class="r_169" style="margin:12px 0px 0px 5px;width:auto;font-size:16px;">{{offerAt}}</div>
				<div style="float:right;margin:8px 5px 0px 0px;">
					<span>￥<span style="color:#c42905;font-size:16px;">{{offerPrice}}&nbsp;&nbsp;</span></span>
					{{#priceLead}}<span style="color:green;font-size:16px;">领先</span>{{/priceLead}}
					{{^priceLead}}<span style="color:gray;font-size:16px;">淘汰</span>{{/priceLead}}
				</div>
			</div>
	{{/rows}}
	</script>
</body>
</html>
