﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css" />
<link rel="stylesheet" href="/css/front/wpk/style_v1.css" title="v">
<link rel="stylesheet" href="/css/front/wpk/bid.css" title="v">
<link rel="stylesheet" href="/third-party/jquery-weui1/lib/weui.min.css">
<link rel="stylesheet" href="/third-party/jquery-weui1/css/jquery-weui.css">
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
<style>
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}

.swiper-pagination-bullet {
	background: white;
}

.tempWrap {
	max-height: 300px;
}

.scrollbar_ul li {
	float: left;
	width: 33%; /**TODO图片尺寸自适应*/
	position: relative;
}

pre {
	display: block;
	padding: 0;
	margin: 0;
	font-size: 13px;
	line-height: 1.42857143;
	color: #333;
	word-break: break-all;
	word-wrap: break-word;
	background-color: white;
	border: none;
	border-radius: 4px;
}
</style>
</head>
<body style="background-color: #f5f5f5;">
	<div id="weui-layer" class="weui-pull-to-refresh-layer"style="display: none;height:50px;">
	    <div class="pull-to-refresh-arrow"></div> <!-- 上下拉动的时候显示的箭头 -->
	    <div class="pull-to-refresh-preloader"></div> <!-- 正在刷新的菊花 -->
	    <div class="down">下拉刷新</div><!-- 下拉过程显示的文案 -->
	    <div class="up">释放刷新</div><!-- 下拉超过50px显示的文案 -->
	    <div class="refresh">正在刷新...</div><!-- 正在刷新时显示的文案 -->
	</div>
	<input type="hidden" value="${user.id }" id="userId"/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 plr0 jewel_title" style="background-color: white;padding:0px;">
				<ul>
					<li style="width:50%" onclick="lh.jumpR('/ap');">专场</li>
					<li style="width:50%" class="active" onclick="lh.jumpR('/am');">微拍</li>
					<!-- <li onclick="lh.jumpR('/am');">大厅</li>
					<li onclick="lh.jumpR('/am');">鬼市</li>
					<li onclick="lh.jumpR('/am');">微现场</li> -->
				</ul>
			</div>
		</div>
		<div class="row bdb">
			<div class="col-xs-12 bg_gray_tint ptb10">
				<div class="col-xs-10 plr0 classification">
					<ul id="auctionSwitchUl">
						<li id="am_focus" class="active" onclick="loadAuctionMicro('focus');">我的关注</li>
						<li id="am_newest" onclick="loadAuctionMicro('newest');">最新拍品</li>
						<li id="am_nearend" onclick="loadAuctionMicro('nearend');">即将结束</li>
					</ul>
				</div>
				<div class="col-xs-2 plr0" onclick="promptSearch();">
					<!--  -->
					<img class="img-responsive center-block" src="/images/front/search.png" />
				</div>
			</div>
		</div>

		<div class="col-xs-12 pt10" id="showAllMyDiv" style="display: none;">
			<a href="javascript:;" class="weui_btn weui_btn_primary text-center" onclick="loadMyAllAuctionMicro();"> 
				<span style="display: inline-block;">显示所有我的微拍</span>
			</a>
		</div>
		<div class="col-xs-12 pt10" id="showAllDiv" style="display: none;">
			<a href="javascript:;" class="weui_btn weui_btn_primary text-center" onclick="loadAllAuctionMicro();"> 
				<span style="display: inline-block;">显示所有微拍</span>
			</a>
		</div>
		<div id="data-container"></div>

		<%@ include file="/views/front/common/z_div_keyboard.htm"%>

		<div class="row h60"></div>
	</div>

	<!-- 图片展示 开始 -->
	<!-- <div id="imagePreview" onclick="closeImgView();" class="yipu-device" style="height:100%;width:100%;position: absolute;top: 1px;left: 1px;background: rgba(0,0,0,.9);">
		<div class="pre_title">
			<p class="pre_subhead"></p>
			<p class="pre_subhead"></p>
		</div>
		<span id="pre_closepic" style="color: #6CB4E5; position: absolute; top: 10px; z-index: 9999; right: 10px; cursor: pointer;display: none;">关闭</span>
		<div id="pre_container" class="swiper-container" style="height:100%;width:100%;">
			<div class="swiper-wrapper">
				<div class="swiper-slide yipu-middle" style="text-align: center;"><img src="http://weipaike.img-cn-qingdao.aliyuncs.com/2016061509122705.jpeg@180w_180h_4e_240-240-246bgc_10Q"/></div>
				<div class="swiper-slide yipu-middle" style="text-align: center;"><img src="http://weipaike.img-cn-qingdao.aliyuncs.com/2016061509122705.jpeg@180w_180h_4e_240-240-246bgc_10Q"/></div>
				<div class="swiper-slide yipu-middle" style="text-align: center;"><img src="http://weipaike.img-cn-qingdao.aliyuncs.com/2016061509122705.jpeg@180w_180h_4e_240-240-246bgc_10Q"/></div>
		    </div>
		    如果需要分页器
		    <div class="swiper-pagination"></div>
    		如果需要滚动条
    		<div class="swiper-scrollbar"></div>
		</div>
	</div> -->
	
	<%@ include file="/views/common/common_share.htm"%><!-- 分享DIV -->
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>

	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/bid.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/third-party/jquery-weui1/js/jquery-weui.js"></script>
	<script type="text/javascript" src="/third-party/jquery-weui1/js/swiper.js" charset='utf-8'></script>
	<script type="text/javascript" src="/js/front/auction/micro/auctionMicro.js" title="v"></script>
	
	<!-- -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="row ">
			<div class="col-xs-12 bg_gray plr7">
				<div class="col-xs-12 plr0 bdr10 mt10">
					<div class="col-xs-12 bg_white plr0 bdb bdtlr10">
						<div class="col-xs-12 plr7">
							<div class="col-xs-12 ptb10 plr0">
								<div class="col-xs-2 plr0" onclick="lh.jumpR('/user/{{userSerial}}');">
									<img class="img-responsive center-block" width="48" height="48" src="{{userAvatar}}" onerror="this.src='/images/front/default_avatar.jpg'" />
								</div>
								<div class="col-xs-10 pr0">
									<div class="pa5 pull-left orange" onclick="lh.jumpR('/user/{{userSerial}}');">{{shopName}}</div>
									<div class="pull-left pa5">
										<img src="/images/front/pic_12.png" class="img-responsive center-block">
									</div>
									<div class="pull-left pa5">
										<img src="/images/front/pic_13.png" class="img-responsive center-block">
									</div>
									<div class="fr">{{&focusDom}}</div>
								</div>
								<!--分享赚钱：二期开发
								<div class="col-xs-5 pr0 pt5 fs10" onclick="showImgs('{{picPathAry}}');">
									{{&focusDom}}
								</div>
								<div class="col-xs-5 plr0 text-right">
									<button class="btn btn-sm btn-orange">
										<div class="pull-left">
											<img src="/images/front/pic_14.png" class="img-responsive center-block pull-left">
										</div>
										<div class="pull-left">分享赚钱</div>
									</button>
								</div>
								-->
							</div>
						</div>
					</div>
					<div class="col-xs-12 bg_white plr0 bdb">
						<div class="col-xs-12 plr7 blue_deep pt5" id="desc_div_{{id}}">
							{{goodsName}}
							<a style="float:right;color:" href="javascript:void(0);" onclick="toggleDescription({{id}});">展开</a>
						</div>
						<div class="col-xs-12 plr7 pt5" id="desc_{{id}}" role="1" style="max-height:94px;overflow:hidden;"><pre>{{goodsDescription}}</pre></div>
						<div class="col-xs-12 plr7 ptb5">
							<div class="made scrollbox" id="horizontal_{{id}}">
								<div class="madegame">
									<ul  onclick="showImgs('{{picPathAry}}');" class="clearfix scrollbar_ul" id="scrollbar_ul_{{id}}">
										{{&picsDom}}<!--拍品图片dom-->
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 plr7 bidder_pic" style="max-height:40px;height:auto;">
							<div class="col-xs-12 plr0" id="praise_container_{{id}}">
								{{#userPraiseList}}
									<div id="praise_{{id}}" class="col-xs-2 pl0 pr7 pb5">
										<img onclick="lh.jumpR('/user/{{userSerial}}');" src="{{userAvatar}}" width="36" class="img-responsive">
									</div>
								{{/userPraiseList}}
								<!--
								<div class="col-xs-2 pl0 pr7 pb5 bidder_pic_next">
									<img src="/images/front/portrait_07.png" width="36" class="img-responsive">
								</div>-->
							</div>
							<!--
							<div class="col-xs-12 plr0">
								<div class="col-xs-2 pl0 pr7 pb5">
									<img src="/images/front/portrait_05.png" width="36" class="img-responsive">
								</div>
							</div>
							-->
						</div>
					</div>
					<div class="col-xs-12 bg_white plr0 bdblr10">
						<div class="col-xs-7 pl7 pr0 pt5">
							<div class="col-xs-5 plr0">
								<button class="btn btn-xs btn-orange pull-left">包邮</button>
								<img src="/images/front/pic_15.png" class="img-responsive pull-left ml5">
							</div>
							<div class="col-xs-7 gray plr0">{{date}}</div>
						</div>
						<div class="col-xs-5 pr7 pt5 text-right gray">
							<!--<i class="icon-comment-alt"></i> day_img4_h&nbsp;<span>275</span>&nbsp;&nbsp;&nbsp;--> 
							<span onclick="addPraise({{id}}, {{userId}});" style="line-height:21px;font-size:20px;">
								<i class="icon-heart-empty"></i>&nbsp;<span id="praiseNum_{{id}}">{{userPraiseCount}}</span>
							</span>
								<img src="/images/front/day_img4_h.png" onclick="showShare();" style="height:22px;padding-left:20px;margin-right:10px;" class="img-responsive fr ml5">
						</div>
						<div class="col-xs-12 pt10">
							<a href="javascript:;" class="weui_btn weui_btn_primary text-center" onclick="doOffer('show',{{id}},'{{bail}}');"> 
								<img src="/images/front/pic_16.png" style="display: inline-block;" /> 
								<span style="display: inline-block;">我要出价</span>
							</a>
						</div>
						<div class="col-xs-12 plr7 pt10">
							<div class="col-xs-4 pt5 plr0">
								<div class="col-xs-12 plr0">
									<button class="btn btn-xs btn-orange" onclick="lh.jumpR('/chat/{{userSerial}}');">
										<img src="/images/front/pic_04_white.png" style="display: inline-block;" /> <span style="display: inline-block;">在线咨询</span>
									</button>
								</div>
							</div>
							<div class="col-xs-8 plr0 text-right">
								<div class="col-xs-12 plr0 fs10">
									距离结束：<span id="ctdown_hour_{{id}}" class="red_deep1 fs16">00</span>小时<span id="ctdown_minute_{{id}}" class="red_deep1 fs16">00</span>分<span id="ctdown_second_{{id}}" class="red_deep1 fs16">00</span>秒
								</div>
								<div class="col-xs-12 gray plr0 text-right">
									保证金￥<span id="bail_{{id}}">{{^bail}}0{{/bail}}{{#bail}}{{bail}}{{/bail}}</span>&nbsp;
									一口价：￥<span id="buyoutPrice_{{id}}">{{^buyoutPrice}}无{{/buyoutPrice}}{{#buyoutPrice}}{{buyoutPrice}}{{/buyoutPrice}}</span>
								</div>
							</div>
						</div>
						<div class="col-xs-12 text-center plr7 enter_scene ptb10">
							<div class="col-xs-4 plr0">起拍价￥<span id="priceBegin_{{id}}">{{priceBegin}}</span></div>
							<div class="col-xs-4 plr0">当前价￥<span id="top_offer_{{id}}">{{^offerPrice}}0{{/offerPrice}}{{#offerPrice}}{{offerPrice}}{{/offerPrice}}</span></div>
							<div class="col-xs-4 plr0">加价幅度￥<span id="increaseRangePrice_{{id}}">{{increaseRangePrice}}</span></div>
							<!--<div class="col-xs-4 plr0">
								保证金￥{{^bail}}0{{/bail}}{{#bail}}{{bail}}{{/bail}}
							</div>-->
						</div>
						<div class="col-xs-12 plr7 bdblr10 pb5">
							<div id="offer_container_{{id}}" class="col-xs-12 bg_gray ptb10 plr7 bdblr10">

								{{#amoList}}
								<div class="col-xs-12 plr0" style="border-bottom: 1px solid #c6c5c5;padding:5px 0px;">
									<div class="col-xs-2 plr0" onclick="lh.jumpR('/user/{{userSerial}}');">
										<img src="{{userAvatar}}" onerror="this.src='/images/front/default_avatar.jpg'" width="50" class="img-responsive">
									</div>
									<div class="col-xs-6 plr0">
										<div class="col-xs-12 plr0 blue_deep" onclick="lh.jumpR('/user/{{userSerial}}');">{{username}}</div>
										<div class="col-xs-12 plr0 fs18 red_deep1">￥{{offerPrice}}</div>
									</div>
									<div class="col-xs-4 plr0">
										<div class="col-xs-12 plr0 red_deep text-right">
											{{#priceLead}}
											<div class="col-xs-12 pr5" style="font-size:16px;">领先</div>
											{{/priceLead}}
											{{^priceLead}}
											<div class="col-xs-12 pl0 pr5" style="font-size:16px;color:gray;">出局</div>
											{{/priceLead}}
										</div>
										<div class="col-xs-12 fs12 plr0 gray text-right" style="padding-top:5px;">{{offerAt}}</div>
									</div>
								</div>
								{{/amoList}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>

	<script id="template_offers" type="x-tmpl-mustache">
	{{#rows}}	 	
		<!-- <div id="offer_{{id}}" class="li_prlist" style="font-size:16px;">
			<div class="l_60_4">
				<img src="{{userAvatar}}" onclick="lh.jumpR('/sale/{{userId}}');" width="100%" />
			</div>
			<div class="l_410" style="width:auto;">
				<div class="t_410_1" style="margin-top:10px;font-size:16px;">
					<a href="javascript:lh.jumpR('/sale/{{userId}}');">
						<span style="font-size:16px;">{{username}}</span>
					</a>
				</div>
			</div>
			<div class="r_169" style="margin:12px 0px 0px 5px;width:auto;font-size:16px;">{{offerAt}}</div>
			<div style="float:right;margin:8px 5px 0px 0px;">
				<span>￥<span style="color:#c42905;font-size:16px;">{{offerPrice}}&nbsp;&nbsp;</span></span>
				{{#priceLead}}<span style="color:green;font-size:16px;">领先</span>{{/priceLead}}
				{{^priceLead}}<span style="color:gray;font-size:16px;">淘汰</span>{{/priceLead}}
			</div>
		</div> -->

		<div class="col-xs-12 plr0" style="border-bottom: 1px solid #c6c5c5;padding:5px 0px;">
			<div class="col-xs-2 plr0" onclick="lh.jumpR('/user/{{userSerial}}');">
				<img src="{{userAvatar}}" onerror="this.src='/images/front/default_avatar.jpg'" width="50" class="img-responsive">
			</div>
			<div class="col-xs-6 plr0">
				<div class="col-xs-12 plr0 blue_deep" onclick="lh.jumpR('/user/{{userSerial}}');">{{username}}</div>
				<div class="col-xs-12 plr0 fs18 red_deep1">￥{{offerPrice}}</div>
			</div>
			<div class="col-xs-4 plr0">
				<div class="col-xs-12 plr0 red_deep text-right">
					{{#priceLead}}
					<div class="col-xs-12 pr5" style="font-size:16px;">领先</div>
					{{/priceLead}}
					{{^priceLead}}
					<div class="col-xs-12 pl0 pr5" style="font-size:16px;color:gray;">出局</div>
					{{/priceLead}}
				</div>
				<div class="col-xs-12 fs12 plr0 gray text-right" style="padding-top:5px;">{{offerAt}}</div>
			</div>
		</div>

	{{/rows}}
	</script>

</body>
</html>
