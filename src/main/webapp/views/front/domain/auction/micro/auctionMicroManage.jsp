﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="/css/front/wpk/style_v1.css" title="v">
<link rel="stylesheet" href="/css/front/wpk/bid.css" title="v">
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>
<style>
.weui_dialog {
	z-index: 9999 !important;
}

h1 {
	margin: 0 !important;
}
</style>
</head>
<body class="bg_gray">
	<div class="container-fluid">
		<div class="row auction_title">
			<div class="col-xs-3" onclick="lh.back();">
				<i class="icon-angle-left icon-3x"></i>
			</div>
			<div class="col-xs-6 pt10 plr0 text-center">
				<span class="fs16">微拍管理</span>
			</div>
			<div class="col-xs-3 pt10 plr7" onclick="$('.addGoodsFixedBg,.addGoodsFixedButton').show();">
				<span>发布微拍</span>
			</div>
		</div>
		<div class="row goodsSearch">
			<div class="col-xs-12 text-center">
				<!-- <div onclick="lh.jumpR('/am/page/manage');" class="col-xs-4 plr7 ptb10 active">微拍产品</div>
				<div onclick="lh.jumpR('/am/page/manage');" class="col-xs-4 plr7 ptb10">已截拍</div>
				<div onclick="lh.jumpR('/goods/page/store?from=am');" class="col-xs-4 plr7 ptb10">我的产品库</div> -->
				
				<div id="auction_ing" onclick="getMyMicroList('ing');" class="col-xs-4 plr7 ptb10 active">微拍产品</div>
                <div id="auction_done" onclick="getMyMicroList('done');" class="col-xs-4 plr7 ptb10"> 已截拍</div>
                <div id="auction_goods" onclick="lh.jumpR('/goods/page/store?from=am');" class="col-xs-4 plr7 ptb10">我的产品库</div>
			</div>
		</div>
		<div class="row goodsClick" style="margin-bottom: 100px;">
			<div id="data-container">
		
			</div>
			<!-- <div class="col-xs-12 bg_white mt10 ptb10 bdb">
				<div class="col-xs-3 plr0">
					<img width="73" src="/images/front/pic_01.png" class="img-responsive center-block">
				</div>
				<div class="col-xs-8 pr0 fs16 pt10">
					<div>翡翠A货小吊坠</div>
					<div class="fs12 gray pt10">
						<span>起拍价：<span class="red">￥10</span></span>&nbsp;&nbsp;&nbsp;&nbsp; <span>当前价：<span class="red">000</span></span><br> <span><span class="red">000</span>人围观</span>
					</div>
				</div>
				<div class="col-xs-1 plr0">
					<div class="col-xs-12 plr0 gray text-right pt25">
						<i class="icon-angle-right icon-2x"></i>
					</div>
				</div>
				-->
		</div>
	</div>
	<!-- 点击后弹出的菜单 -->
	<div class="spmc_bg"></div>
	<div class="spmc">
		<div class="spmc_menu">
			<div class="col-xs-3 plr0 text-center" id="am_update" onclick="updateAuction();">
				<div>
					<img src="/images/front/managerMenu_01.png" class="img-responsive center-block">
				</div>
				<div class="pt5">修改</div>
			</div>
			<!-- <div class="col-xs-3 plr0 text-center">
				<div>
					<img src="/images/front/managerMenu_02.png" class="img-responsive center-block">
				</div>
				<div class="pt5">提交</div>
			</div> -->
			<div class="col-xs-3 plr0 text-center" id="am_preview" onclick="previewAuction();">
				<div>
					<img src="/images/front/managerMenu_03.png" class="img-responsive center-block">
				</div>
				<div class="pt5">查看</div>
			</div>
			<div class="col-xs-3 plr0 text-center" id="am_submit" onclick="submitAuction();">
				<div>
					<img src="/images/front/managerMenu_02.png" class="img-responsive center-block">
				</div>
				<div class="pt5">上架</div>
			</div>
			<div class="col-xs-3 plr0 text-center" id="am_delete" onclick="confirmDelete();">
				<div>
					<img src="/images/front/managerMenu_04.png" class="img-responsive center-block">
				</div>
				<div class="pt5">下架</div>
			</div>
		</div>
		<div class="spmc_quit fs16">取消</div>
	</div>
	
	<!-- 
	<div class="addGoodsFixedBg"></div>
	<div class="addGoodsFixedButton">
		<div onclick="lh.jumpR('/goods/page/add?from=am');" class="addGoodsNew">上传新拍品</div>
		<div onclick="lh.jumpR('/goods/page/store?from=am');" class="addGoodsChoose">从拍品库中选择</div>
		<div class="addGoodsquit">取消</div>
	</div> -->

	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/bid.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<script type="text/javascript" src="/js/front/auction/micro/auctionMicroManage.js" title="v"></script>
	<!-- -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div onclick="switchCurrent('{{serial}}', '{{userSerial}}');" class="col-xs-12 bg_white mt10 ptb10 bdb">
			<div class="col-xs-3 plr0">
				<img width="73" src="{{firstPic}}" class="img-responsive center-block">
			</div>
			<div class="col-xs-8 pr0 fs16 pt10">
				<div>{{goodsName}}</div>
				<div class="fs12 gray pt10">
					<span>起拍价：<span class="red">{{priceBegin}}</span></span>&nbsp;&nbsp;&nbsp;&nbsp; 
					<span>当前价：<span class="red" id="top_offer_{{id}}">{{^offerPrice}}0{{/offerPrice}}{{#offerPrice}}{{offerPrice}}{{/offerPrice}}</span></span><br> 
					<span>围观数：<span class="red" id="visitNum_{{id}}">{{visitNum}}</span></span>&nbsp;&nbsp;&nbsp;&nbsp;
					<span>出价数：<span class="red" id="offerTimes_{{id}}">{{^offerTimes}}0{{/offerTimes}}{{#offerTimes}}{{offerTimes}}{{/offerTimes}}</span></span>
				</div>
			</div>
			<div class="col-xs-1 plr0" onclick="lh.jumpR('/am?amSerial={{serial}}&userSerial={{userSerial}}');">
				<div class="col-xs-12 plr0 gray text-right pt25">
					<i class="icon-angle-right icon-2x"></i>
				</div>
			</div>
		</div>
	{{/rows}}	 		 
	</script>

</body>
</html>
