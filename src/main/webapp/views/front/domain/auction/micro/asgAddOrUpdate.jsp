﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div class="mainbox" id="iOther">
				<div class="mainsmall" id="auctionOnlineMain">
					<div id="main" role="main" style="border-bottom: 1px solid #CDCDCD; background: white;">
						<div>
							<div class="weui_cell">
					    		<div class="weui_cell_bd weui_cell_primary">
					    			<input type="text" id="searchGoodsNames" style="width: 80%;" class="ipt_item" placeholder="请输入要查询的商品">
					    			<button class="weui_btn weui_btn_mini weui_btn_primary pointer fr"  style="color: #FFFFFF;" onclick="searchGoods()">查询</button>
					    		</div>
							</div>
							<div class="weui_cell">
				        		<!-- <div class="weui_cell_hd"><label class="myLabel" style="font-size:14px">分类:</label></div> -->
					         	<div class="weui_cell_bd weui_cell_primary" style="margin-right: 10px;">
					    		 	<button onclick="catValue(null,'全部','first')" id="TypeName" class="weui_btn weui_btn_primary pointer" style="float:left;color: #FFFFFF;">全部</button>
					    		</div>
					    		<div class="weui_cell_bd weui_cell_primary">
					    			<button class="weui_btn weui_btn_primary fr pointer" id="nextBtn" style="color: #FFFFFF;" onclick="location.href='/addGoods<c:if test="${!empty r}">?r=${r}&auctionInst=1</c:if><c:if test="${empty r}">?auctionInst=1</c:if>'">发布藏品</button>
					    		</div>
					    	</div>	
					    	<div class="weui_cell">
				        		<div class="weui_cell_hd">
				        			<label class="myLabel" style="font-size:14px">已选择商品数量:</label>
				        			<span id="selectNum" style="color:red">0</span>
				        			<span id="scanSelected" style="color:green;" class="pointer" onclick="catGoods()">点击查看</span>
				        			<span id="back" style="display:none;color:green;" class="pointer" onclick="catValue(null,'全部')">继续选择藏品</span>
				        			<button id="back" class="weui_btn weui_btn_mini weui_btn_primary pointer fr"  style="color: #FFFFFF;display:none;" onclick="catValue(null,'全部')">返回</button>
				        		</div>
				        	</div>
						</div>
						<div style="padding: 0 5px;">
							<ul id="goodsList" class="mainul mainul1" style="width:100%">
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div id="priceWin" class="szweituo" style="display:none;z-index:1">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<h4 id="priceTitle">设置藏品起拍价格</h4>
				<div class="szwtinput">
					<div id="offerIptDiv" class="szwtinpk" style="">
						<input type="hidden" id="goodsId">
						<label id="tips" style="display:none;color:red;"></label>
						<form id="entrustFrom">
							<span><input type="number" id="priceBegin" placeholder="请在此输入该藏品的起拍价格" value="0"></span>
							<span><input type="number" id="increaseRangePrice" placeholder="请在此输入该藏品的加价幅度" value="0"></span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="priceCancel" onclick="doPriceBegin('cancel')" class="szwtbutr entrusta2 pointer">取消</span> 
						<span id="priceSave" onclick="doPriceBegin('save')" class="szwtbutl entrusta1 pointer">确定</span>
						<span id="priceEdit" style="display:none;" onclick="doPriceBegin('edit')" class="szwtbutl entrusta1 pointer">确定</span>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="weui_dialog_alert" id="dialog" style="display:none">
	        <div class="weui_mask"></div>
	        <div class="weui_dialog">
	            <div class="weui_dialog_hd"><strong class="weui_dialog_title">输入支付密码</strong></div>
	            <div class="weui_dialog_bd">
	            	<div><span id="tips" style="display:none"></span></div>
	            	<div class="weui_cell">
		                <div class="weui_cell_hd"><label>输入支付密码</label></div>
		                <div class="weui_cell_bd weui_cell_primary">
		                    <input class="weui_input" type="password" id="payPassword" placeholder="请输入支付密码">
		                </div>
		            </div>
	            </div>
	            <div class="weui_dialog_ft">
	                <a href="javascript:;" onclick="addAuctionProfession()" class="weui_btn_dialog primary">确定</a>
	            </div>
	        </div>
	    </div>
	
	<div id="actionSheet_wrap">
        <div class="weui_mask_transition" id="myMask" style="display: none;"></div>
        <div class="weui_actionsheet" id="weui_actionsheet">
            <div class="weui_actionsheet_menu">
            	<div class="weui_actionsheet_cell pointer" onclick="catValue(null,'全部')">全部</div>
            	<c:forEach items="${dictList}" var="dict" >
                	<div class="weui_actionsheet_cell pointer" onclick="catValue('${dict.id}','${dict.dictName}')">${dict.dictName}</div>
               </c:forEach>
            </div>
            <div class="weui_actionsheet_action">
                <div class="weui_actionsheet_cell pointer" id="actionsheet_cancel">关闭菜单</div>
            </div>
        </div>
    </div>

	<div class="save bottomFix" id="antiqueCityShop">
		<ul>
			<li><a href="javascript:;" class="a_say" onclick="saveProfession();return false;">保存</a></li>
		</ul>
	</div>
	
	<input type="hidden" id="asId" value="${am.id}">
	<%-- <input type="hidden" id="asgId" value="${asg.id}"> --%>
	<input type="hidden" id="aqId" value="${auctionQuickInst.id}">
	<%-- <input type="hidden" id="comeFrom" value="${comeFrom}">
	<input type="hidden" id="bounsNum" value="${bounsNum}">
	<input type="hidden" id="bounsSinglePrice" value="${bounsSinglePrice}">
	<input type="hidden" id="bonusTypeId" value="${bonusTypeId}">
	<input type="hidden" id="bonus" value="${bonus}">
	<input type="hidden" id="bail" value="${bail}">
	<input type="hidden" id="startTime" value="${startTime}">
	<input type="hidden" id="day" value="${day}">
	<input type="hidden" id="auctionName" value="${auctionName}"> --%>
	<input type="hidden" id="aiId" value="${auctionInst.id}">
	<%-- <input type="hidden" id="typeId" value="${typeId}"> --%>
	<input type="hidden" id="userId" value="${userId}">
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<!-- <div id="mask"></div> -->

	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/auction/micro/asg.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<li style="width: 100%;" class=" pointer" id="img_{{ id }}">
			<span style="background: url({{picPath}}) center center no-repeat #C4C5C7; background-position: 50% 50%; background-size: contain;width:45%"></span>
			<div style="width:54%;float:right;height:155px;">
				<div class="tit_cp">
					<a href="javascript:void(0);" style="font-size:16px;">{{goodsName}}</a>
				</div>
				<div class="price_cp" style="font-size:14px;">类别:{{typeName}}</div>
				<div class="price_cp" style="font-size:14px;display:none;" id="priceBegin_{{id}}">
				</div>
				<div class="price_cp_{{id}}" style="height: 35px;position: absolute;bottom: 1px;">
					<button class="btn weui_btn_primary fl pointer" style="margin:2px;color: rgb(255, 255, 255);"  onclick="lh.jumpR('/gm/1');">修改</button>
					<button class="btn weui_btn_primary fl pointer" style="margin:2px;color: rgb(255, 255, 255);"  onclick="lh.jumpR('/gm/1');">删除</button>
					<!--<button id="add_{{id}}" class="btn weui_btn_primary fl pointer" style="margin:2px;color: rgb(255, 255, 255);"  onclick="showPriceBegin('{{ id }}','','','')">添加</button>-->
					<button id="add_{{id}}" class="btn weui_btn_primary fl pointer" style="margin:2px;color: rgb(255, 255, 255);"  onclick="lh.jumpR('/releaseGoods?goodsId={{id}}&from=am'">添加</button>
				</div>
			</div>
		</li>
	{{/rows}}	 		 
	</script>
</body>
</html>
