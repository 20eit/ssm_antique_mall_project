﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="/css/front/wpk/style_v1.css" title="v">
<link rel="stylesheet" href="/css/front/wpk/bid.css" title="v">
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>
<style>
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{background-color: #fff;opacity: 1;}
.weui_dialog {z-index: 9999 !important;}
h1 {margin: 0 !important;}
.weui-picker-modal {min-height:220px;}
.weui-picker-modal .picker-modal-inner {min-height:220px;}
.weui-picker-modal .picker-item {font-size:20px;}
.weui-picker-modal .toolbar-inner {height:40px;}
.weui-picker-modal .title {font-size:20px;line-height: 40px;}
.weui-picker-modal .picker-button {font-size:18px;line-height: 40px;}
</style>
</head>
<div class="container-fluid">
	<div class="row auction_title">
		<div class="col-xs-1" onclick="lh.back();">
			<i class="icon-angle-left icon-3x"></i>
		</div>
		<div class="col-xs-10 pt10 text-center">
			<span class="fs16">发布微拍</span>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 bdb ptb5">
			<div class="col-xs-3 plr0 pt5">拍品：</div>
			<div class="col-xs-9 plr0">
				<input type="text" class="form-control" readonly="readonly" style="background-color:white;border: none; text-align: center;" value="${goods.goodsName}"/>
			</div>
		</div>
		<div class="col-xs-12 bdb ptb5">
			<div class="col-xs-3 plr0 pt5">截止时间：</div>
			<div class="col-xs-9 plr0">
				<input type="text" class="form-control" style="z-index: 2;background-color:white;border: none; text-align: center;" placeholder="请选择截止时间" id="endTime" />
			</div>
		</div>
		<div id="datePlugin"></div>
		<!-- <div class="col-xs-12 bdb ptb5">
			<div class="col-xs-3 plr0 pt5">分类：</div>
			<div class="col-xs-9 plr0">
				<select class="form-control" name="classification" id="classification">
					<option value="请选择拍品分类">请选择拍品分类</option>
					<option value="请选择拍品分类1">请选择拍品分类1</option>
					<option value="请选择拍品分类2">请选择拍品分类2</option>
					<option value="请选择拍品分类3">请选择拍品分类3</option>
				</select>
			</div>
		</div> -->
		<div class="col-xs-12 bdb ptb5">
			<div class="col-xs-3 plr0 pt5">起拍价：</div>
			<div class="col-xs-9 plr0">
				<input type="number" class="form-control" id="priceBegin" value="0" style="background-color:white;border: none; text-align: center;"/>
			</div>
		</div>
		<div class="col-xs-12 bdb ptb5">
			<div class="col-xs-3 plr0 pt5">加价幅度：</div>
			<div class="col-xs-9 plr0">
				<input type="number" class="form-control" placeholder="请填写每次加价的最小金额" id="increaseRangePrice" style="background-color:white;border: none; text-align: center;"/>
			</div>
		</div>
		<div class="col-xs-12 ptb5 bg_gray">可选设置</div>
		<div class="col-xs-12 ptb5">
			<div class="col-xs-3 plr0 pt5">运费：</div>
			<div class="col-xs-9 plr0">
				<input type="number" class="form-control" placeholder="0" id="postageFee" style="background-color:white;border: none; text-align: center;"/>
			</div>
		</div>
		<div class="col-xs-12 ptb5">
			<div class="col-xs-3 plr0 pt5">保证金：</div>
			<div class="col-xs-9 plr0">
				<input type="text" readonly="readonly" placeholder="请选择保证金"  class="form-control" id="bail" style="background-color:white;border: none; text-align: center;"/>
			</div>
		</div>
		<div class="col-xs-12 plr0">
			<div class="weui_cells weui_cells_form mt0">
				<div class="weui_cell weui_cell_switch">
					<div class="weui_cell_hd weui_cell_primary fs14">七天包退</div>
					<div class="weui_cell_ft">
						<input class="weui_switch" type="checkbox" checked="checked" id="isSevenReturn">
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="col-xs-12 plr0">
			<div class="weui_cells weui_cells_form mt0" style="border-top: none;">
				<div class="weui_cell weui_cell_switch">
					<div class="weui_cell_hd weui_cell_primary fs14">允许分享</div>
					<div class="weui_cell_ft">
						<input class="weui_switch" type="checkbox">
					</div>
				</div>
			</div>
		</div> -->
	</div>
	<div class="row pt20">
		<div class="col-xs-12" onclick="addOrUpdateAuctionMicro();">
			<a role="button" class="btn btn-orange col-xs-12">发布</a>
		</div>
	</div>
</div>

<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
<%@ include file="/views/common/common_js.htm"%>
<%@ include file="/views/common/common_front_js.htm"%>
<%@ include file="/views/common/common_front_wpk_js.htm"%>
<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/js/front/wpk/bid.js" title="v"></script>
<script type="text/javascript"> lh.param = ${paramJson} </script>
<script type="text/javascript" src="/js/front/auction/micro/auctionMicroAddOrUpdate.js" title="v"></script>
<!-- -->
<script id="template" type="x-tmpl-mustache"></script>

</body>
</html>
