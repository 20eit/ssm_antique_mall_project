﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/mobiscroll/css/mobiscroll.custom-2.6.2.min.css"/> 
<link rel="stylesheet" type="text/css" href="/third-party/bootstrap-select-1.10.0/css/bootstrap-select.min.css"/>
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	 <div class="weui_cells weui_cells_form" style="margin-top:50px;min-height:500px;">
	 	<ul class="information">
 			<li class="realName" style="background:#DDDAD6">
 				<button class="btn weui_btn_primary fl pointer" id="draft" style="color: #FFFFFF;" onclick="lh.jumpR('/goodsManageType');">产品管理</button>
 				<button class="btn weui_btn_primary fr pointer" id="add" style="color: #FFFFFF;" onclick="addAuction();return false;">选择商品</button>
 			</li>
	 	</ul>
         <div>
         	<c:if test="${!empty ap}">
	         	<div class="weui_cell">
		            <div class="weui_cell_hd"><label>拍卖场次:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		                <input class="weui_input" type="text" id="auctionName" value="${ap.auctionName}" placeholder="请填写拍卖场次">
		            </div>
		        </div>
	         	<div class="weui_cell" id="start">
		            <div class="weui_cell_hd"><label>开拍时间:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		                <input class="weui_input" type="text" id="startTime" value="<fmt:formatDate value="${ap.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" placeholder="请选择拍卖开始时间">
		            </div>
		        </div>
	        </c:if>
	        <c:if test="${!empty as}">
	         	<div class="weui_cell" id="end">
		            <div class="weui_cell_hd"><label>结束时间:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		            	 <input class="weui_input" type="text" id="endTime" value="<fmt:formatDate value="${asg.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" placeholder="请选择拍卖结束时间">
		            </div>
		        </div>
	        </c:if>
	        <c:if test="${!empty aq}">
	         	<div class="weui_cell">
		            <div class="weui_cell_hd"><label>即时拍拍卖类型:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		            	<select id="typeId" class="selectpicker">
		            		<option value="">请选择类型</option>
			                <c:forEach var="dict" items="${dictList}">
			                	<option value="${dict.id}">${dict.dictName}</option>
			                </c:forEach>
		                </select>
		            </div>
		        </div>
	        </c:if>
	        <c:if test="${!empty ap}">
	         	<div class="weui_cell" id="bailValue">
		            <div class="weui_cell_hd"><label>保证金:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		                <input class="weui_input" type="text" id="bail" value="${ap.bail}" placeholder="请填写保证金">
		            </div>
		        </div>
	        </c:if>
	        <c:if test="${!empty aq}">
	         	<div class="weui_cell" id="bailValue">
		            <div class="weui_cell_hd"><label>保证金:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		                <input class="weui_input" type="text" id="bail" value="${aq.bail}" placeholder="请填写保证金">
		            </div>
		        </div>
	        </c:if>
	        <c:if test="${!empty as}">
	         	<div class="weui_cell" id="bailValue">
		            <div class="weui_cell_hd"><label>保证金:</label></div>
		            <div class="weui_cell_bd weui_cell_primary">
		                <input class="weui_input" type="text" id="bail" value="${as.bail}" placeholder="请填写保证金">
		            </div>
		        </div>
	        </c:if>
	     </div>
      </div>
	  <div class="pz_down">
		<div class="c_0100_9"></div>
	 </div>
	 <input type="hidden" id="comeFrom" value="${comeFrom}">
	 <input type="hidden" id="apTypeId" value="${aq.typeId}">
	 <input type="hidden" id="apId" value="${ap.id}">
	 <input type="hidden" id="asId" value="${as.id}">
	 <input type="hidden" id="aqId" value="${aq.id}">
	 <input type="hidden" id="professionId" value="${professionId}">
	 <input type="hidden" id="auctionQuickId" value="${auctionQuickId}">
	 <input type="hidden" value="${loginStatus}" id="loginStatus"/>
	 <%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script src="/js/front/auction/quick/aqAddOrUpdate.js" title="v"></script>
</body>
</html>
