﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/swiper/swiper.3.1.7.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="/third-party/wookmark/main.css"/> -->
<style>
.swiper-pagination-bullet {background: white;}
.tempWrap{max-height: 300px;}
.banner {
    background: -webkit-gradient(linear, left top, left bottom, from(#999), to(#333333));
    background: -webkit-linear-gradient(top, #999, #333333);
    background: -moz-linear-gradient(top, #999, #333333);
    background: -o-linear-gradient(top, #999, #333333);
    background: -ms-linear-gradient(top, #999, #333333);
    background: linear-gradient(top, #999, #333333);
}
</style>
</head>
<body style="background-color: #f5f5f5; margin-top: 48px;">
	<input type="hidden" id="professionId" value="${ap.id}" />

	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<!-- Baner 开始 -->
	<div class="banner" style="float: none;">
		<div id="slideBox" class="slideBox">
			<!-- Banner图片 -->
			<div id="banner" class="bd frontHide">
				<ul>
					<c:forEach var="pic" items="${picList}">
						<li><a class="pic" href="javascript:void(0)"><img src="${pic.picPath}" style="height:280px;width: auto;" onclick="showImgView(${status.index});"/></a></li>
					</c:forEach>
				</ul>
			</div>
			<div class="hd">
				<ul></ul>
			</div>
		</div>
	</div>
	<!-- Banner 结束 -->

	<!--  开始 -->
	<div class="jydtitle">
		<div class="jydtitdiv">
			<ul class="jydtitul">
				<li><span id="labBtitle">${goods.goodsSn}.${goods.goodsName}</span></li>
				<li class="jydtitulli" style="border-bottom: none;">
					<div class="jydtiyuldiv" id="iSPrice">
						<span class="yikou">一口价：无</span> <span id="lStartPrice" style="font-size: 14px;">起拍：${goods.priceBegin} 元</span>
					</div>
					<div class="jydtiyuldiv" id="iGPrice" style="display: none;">RMB：無底價</div>
				</li>
				<li class="jydtitulli1" id="iChange">
					<div class="jydtitli jydtitlima"></div>
					<div style="clear: both;"></div>
				</li>
				<li id="details" class="jydtitulli1">
					<div class="jydtitli" style="color: red" id="hide4">
						<span id="labBbear">运费： <c:if test="${empty goods.postageFee || goods.postageFee<=0}">无</c:if> <c:if test="${goods.postageFee > 0}">到付</c:if>
						</span>
					</div>
					<div class="jydtitli" style="color: red" id="hide5">
						<span id="labBpacking">包装费： <c:if test="${empty goods.packFee || goods.packFee<=0}">无</c:if> <c:if test="${goods.packFee > 0}">${goods.packFee}</c:if>
						</span>
					</div>
					<div class="jydtitli">
						<span id="labBcontent">描述：${goods.goodsDescription}</span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="jydtitle" id="iTime">
		<div class="jydtitdiv">
			<div class="endtime">
				<strong id="remainTime">开拍时间：</strong>
				<span class="timer"><fmt:formatDate value="${ap.startTime}" pattern="yyyy-MM-dd hh:mm"></fmt:formatDate> </span>
			</div>
		</div>
	</div>

	<div class="jydtitle" id="iPrice" style="display: none;">
		<div class="jydtitdiv">
			<ul class="jydsubmit">
				<li class="jyddabao"><span class="jyddbspan">进行担保交易,七天可退换货</span></li>
				<li>
					<div style="clear: both;"></div>
				</li>
			</ul>
		</div>
	</div>

	<div class="jydtitle">
		<div class="jydtitdiv">
			<a>
				<ul class="jydshop">
					<li onclick=""><div id="ShopLogo" onclick="lh.jumpR('/shop/${ap.userId}');" class="shopslimg shop_logo pointer" style="background: url('${ap.userAvatar}');background-size: 100%;"></div></li>
					<li class="comeinshop pointer" onclick="lh.jumpR('/shop/${ap.userId}');">进入店铺</li>
					<li onclick="lh.jumpR('/shop/${ap.userId}');" class="shoptittle pointer"><span>${ap.instName}</span><br></li>
					<li class="jyddabao"><span class="jyddbspan">进行微拍客担保交易,七天可退换货</span></li>
					<div style="clear: both;"></div>
				</ul>
			</a>
		</div>
	</div>

	<div style="padding: 0 8px; cursor: pointer;" onclick="lh.jumpR('/tradeDesc');">
		<div class="trade_tip_bar">买卖交易规则说明</div>
	</div>

	<div class="mainbox" id="iOther">
		<div class="mainsmall" id="auctionOnlineMain">
			<div id="main" role="main" style="border-bottom: 1px solid #CDCDCD; background: white;">
				<div class="news" style="margin-bottom: 8px; font-size: 14px; padding: 0 8px;">
					<div class="newsdiv">
						<span class="newsfk"></span> <span>其他拍品</span>
					</div>
				</div>
				<div style="padding: 0 5px;">
					<ul id="AuctionCollectionLeft" class="mainul mainul1">
						<c:forEach items="${goodsList}" var="goods" step="2">
							<li onclick="lh.jumpR('/aqg/${goods.goodsId}/${goods.auctionId}');" class="mainulli goods_bg"><span
								style="background: url(${goods.picPath}) center center no-repeat #F2F2F2; background-position: 50% 50%; background-size: contain;"></span>
								<h3 style="color: #838381;">编号.${goods.goodsSn}</h3></li>
						</c:forEach>
					</ul>
					<ul id="AuctionCollectionRight" class="mainul mainul2">
						<c:forEach items="${goodsList}" var="goods" begin="1" step="2">
							<li onclick="lh.jumpR('/aqg/${goods.goodsId}/${goods.auctionId}');" class="mainulli goods_bg"><span
								style="background: url(${goods.picPath}) center center no-repeat #F2F2F2; background-position: 50% 50%; background-size: contain;"></span>
								<h3 style="color: #838381;">编号.${goods.goodsSn}</h3></li>
						</c:forEach>
					</ul>
				</div>
				<div style="clear: both;"></div>
			</div>
			<div class="loading">对不起，已经加载完啦！</div>
		</div>
	</div>

	<!--  结束 -->

	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>

	<div class="pz_menu">
		<ul id="top_header" class="jydfoot">
			<!-- <li id="beginNotice" onclick="toggleNotice();" class="jydfootli4 jydfootlip2 jydfootli8 remind pointer" state="1">开拍提醒</li> -->
			<li id="beginNotice"  class=" remind pointer" state="1"></li>
			<li id="chat" onclick="lh.jumpR('/chat/${ap.userId}?gId=${goods.id}');" class="jydfootli3 pointer">咨询卖家</li><!-- class="jydfootli3" -->
			<li id="offerShow" class="pointer" style=" text-indent: 0;" state="1"></li> 
			<!-- <li id="offerShow" onclick="showAutoOffer();" class="jydfootli1 jydfootlip1" style=" text-indent: 0;" state="1">委托出价</li> -->
		</ul>
	</div>

	<%-- <div id="offerWin" class="szweituo" style="display: none;">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<h4>设置委托出价</h4>
				<div class="szwtinput">
					<div id="offerTip" class="szwtzuigao">输入最高委托金额(元):</div>
					<div id="offerIptDiv" class="szwtinpk">
						<form id="entrustFrom">
							<span> <input type="number" id="topPrice" placeholder="请在此输入委托金额" value="">
							</span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="offerCancel" onclick="doAutoOffer('cancel')" class="szwtbutr entrusta2">取消</span> 
						<span id="offerSave" onclick="doAutoOffer('save',{auctionId:${ap.id},goodsId:${goods.id}})" class="szwtbutl entrusta1">确定</span>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtwhat">什么是委托出价？</div>
					<div class="szwtmiaosu">设置后系统会自动帮您出价，超过最高委托金额后停止</div>
				</div>
			</div>
		</div>
	</div> --%>

	<div id="maskDiv" class="">&nbsp;</div>
	<!-- 图片展示 开始 -->
	<div id="imagePreview" onclick="closeImgView();" class="yipu-device" style="height:0px;">
		<div class="pre_title">
			<p class="pre_subhead"></p>
			<p class="pre_subhead"></p>
		</div>
		<span id="pre_closepic" style="color: #6CB4E5; position: absolute; top: 10px; z-index: 9999; right: 10px; cursor: pointer;display: none;">关闭</span>
		<div id="pre_container" class="swiper-container" style="height:100%;width:100%;">
			<div class="swiper-wrapper">
				<c:forEach var="pic" items="${picList}">
				 	<div class="swiper-slide yipu-middle" style="text-align: center;"><img src="${pic.picPath}"/></div>
				</c:forEach>
		    </div>
		    <!-- 如果需要分页器 -->
		    <div class="swiper-pagination"></div>
    		<!-- 如果需要滚动条 -->
    		<div class="swiper-scrollbar"></div>
		</div>
	</div>
	<!-- 图片展示 结束 -->
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script src="/js/front/auction/quick/quickGoodsDetail.js" title="v"></script>
</body>
</html>
