﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/tiaokuan.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<div class="pz_top">
		<div class="c_0100_1">
			<div class="t_0100_1">
				<div class="tt_one">
					<div class="l_118">
						<a href="javascript:;" id="ss1" class="sstext">藏品&nbsp;<img src="/images/front/top_img5.png" width="5" height="4" /></a>
					</div>
					<div class="r_305">
						<div class="l_205">
							<input name="" type="text" placeholder="输入藏品名称" class="bfom" id="ss1_input" />
						</div>
						<div class="r_18">
							<a href="javascript:void(0);"><input name="" type="image" src="/images/front/top_img6.png" width="18" height="18" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="threepoint">
				<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
				<div class="pf_point" style="display: none;">
					<div class="img_point">
						<img src="/images/front/top_img2.png" width="8" height="7" />
					</div>
					<div class="ulist_point">
						<c:if test="${empty aqi}">
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/auctionQuickInstApplyDesc');">我要入驻</a>
							</div>
						</c:if>
						<c:if test="${!empty aqi}">
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/releaseGoods');">发布拍品</a>
							</div>
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/myAuctionQuick');">拍品管理</a>
							</div>
						</c:if>
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/user');">交易管理</a><!-- 交易管理（付款和发货在这里都可以查看到） -->
						</div>
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/auctionQuickNotice');">提醒设置</a><!--	提醒设置（可以按类别打扣，有拍品就提醒）  -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pz_main">
		<div id="auctionQuickTypeDiv" style="width:100%;height:100%;">
			<c:if test="${!empty goodsTypeNum129 && goodsTypeNum129 > 0}">
				<div id="goodsNum_div_129" onclick="lh.jumpR('/aq?typeId=129');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/129.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_129" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum129}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum128 && goodsTypeNum128 > 0}">
				<div id="goodsNum_div_128" onclick="lh.jumpR('/aq?typeId=128');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/128.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_128" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum128}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum127 && goodsTypeNum127 > 0}">
				<div id="goodsNum_div_127" onclick="lh.jumpR('/aq?typeId=127');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/127.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_127" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum127}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum126 && goodsTypeNum126 > 0}">
				<div id="goodsNum_div_126" onclick="lh.jumpR('/aq?typeId=126');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/126.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_126" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum126}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum125 && goodsTypeNum125 > 0}">
				<div id="goodsNum_div_125" onclick="lh.jumpR('/aq?typeId=125');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/125.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_124" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum125}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum124 && goodsTypeNum124 > 0}">
				<div id="goodsNum_div_124" onclick="lh.jumpR('/aq?typeId=124');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/124.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_124" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum124}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum123 && goodsTypeNum123 > 0}">
				<div id="goodsNum_div_123" onclick="lh.jumpR('/aq?typeId=123');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/123.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_123" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum123}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum122 && goodsTypeNum122 > 0}">
				<div id="goodsNum_div_122" onclick="lh.jumpR('/aq?typeId=122');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/122.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_122" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum122}件</span>
				</div>
			</c:if>
			<c:if test="${!empty goodsTypeNum121 && goodsTypeNum121 > 0}">
				<div id="goodsNum_div_121" onclick="lh.jumpR('/aq?typeId=121');" style="height:180px;width:49%;display:inline-block;background-image:url('/images/front/goods_type/121.jpg');background-size: cover;background-repeat: no-repeat;">
					<span id="goodsNum_span_121" style="margin-top: 1px;padding-right:8px;float: right;color:#900E0D;">${goodsTypeNum121}件</span>
				</div>
			</c:if>
		</div>
		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
		
	<div class="zhezhao frontHide" id="shareMask" onclick="hideShare();"></div>
    <div class="pfreturnforum frontHide" id="share" onclick="hideShare();" style="top:0%;margin-top:0px;">
    	<div class="share">
		 	 喜欢就点击右上角图标分享吧
		</div>
    </div>
    
    <a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
    <input type="hidden" value="${status}" id="status"/>
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/auction/quick/auctionQuickIndex.js" title="v"></script>

	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	<div id="as_aqgid_{{id}}" class="li_day">
		<div class="t_0100_29">
			<div class="l_81">
				<a href="javascript:lh.jumpR('/sale/{{userId}}');"><img src="{{userAvatar}}" width="100%" /></a>
			</div>
			<div class="l_380">
				<div class="t_380_1">
					<a href="javascript:lh.jumpR('/sale/{{userId}}');"><nobr>{{username}}</nobr></a>
				</div>
				<div class="t_380_2">{{date}}</div>
			</div>
			<div class="r_gzbox">
				<div class="r_gz">
					{{&focusDom}}
				</div>
			</div>
		</div>
		<div class="t_0100_32">
			<ul>
				{{&picsDom}}
			</ul>
		</div>
		<div class="t_0100_33">
			<div class="l_sheet">共{{picNum}}张</div>
			<!--<div class="r_480">
				<div class="li_rday">
					<div class="img_rady" onclick="addPraise({{aqg.id}});">
						<a href="javacript:void(0);" class="ombg3"></a>
					</div>
					<span class="tit_rday">赞</span>
				</div>
				<div class="li_rday" onclick="showShare();">
					<div class="img_rady">
						<div class="bshare-custom">
							<div class="bsPromo bsPromo2"></div>
							<a title="更多平台" href="javascript:void(0);" class="ombg4 bshare-more"></a>
						</div>
					</div>
					<span class="tit_rday">分享</span>
				</div>
			</div>-->
		</div>
		<!--点赞用户头像-->
		<div class="t_0100_36">
			<div class="r_326">
				<a href="javascript:void(0);" style="background-color:#383737;" onclick="lh.jumpR('/chat/{{userId}}');" class="a_outprice">
					<img src="/images/front/day_img10.png" width="22" height="22" />&nbsp;咨询
				</a>
			</div>
			<div class="l_326">
				<!-- <a href="javascript:void(0);" style="color:#B93415;background-color:#383737;" onclick="toggleNotice('show',{{id}});" class="a_outprice">
					<img src="/images/front/day_img8.png" width="22" height="22" />&nbsp;开拍提醒
				</a> -->
				{{&noticeDom}}
			</div>
		</div>
	</div>
	{{/rows}}	 		
	</script>
	
</body>
</html>
