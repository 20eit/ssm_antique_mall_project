﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div class="pz_main" style="margin-top:48px;">
		<div style="text-align: center; line-height: 50px; font-size: 20px; font-weight: bold; color: #FFFFFF;">
			<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;width:100%;" onclick="jumpToAddAuction();">新增拍卖</button>
		</div>
		<div id="auctionQuickList" class="c_0100_23">
			<!-- 藏品容器 -->
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<input type="hidden" id="instId" value="${inst.id}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="/js/front/auction/quick/myAuctionQuick.js" title="v"></script>

	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	<div id="aq_{{id}}" class="li_day">
		<div class="t_0100_29">
			<div class="l_380">
				<div class="t_380_1">
					<a href="javacript:void(0);"><nobr>{{auctionName}} ({{&statusDom}})</nobr></a>
				</div>
				<div class="t_380_2">结束时间：{{getEndTime}}</div>
			</div>
			<div class="r_gzbox">
				<div class="r_gz">
					{{&updateDom}}
				</div>
			</div>
		</div>
	</div>
	{{/rows}}	 		
	</script>
	
</body>
</html>
