﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<!-- <link rel="stylesheet" type="text/css" href="/third-party/wookmark/main.css"/> -->
</head>
<body style="background-color: #f5f5f5;">
	<%-- <input type="hidden" id="goodsId" value="${goods.id}"/>
	<input type="hidden" id="professionId" value="${ap.id}"/> --%>
	<div class="pz_main">
		<div class="c_0100_20">
			<div class="c_0100_1">
				<div class="l_510">
					<a id="auctionSerialShow" href="javascript:history.back(-1)" style="width:35%;">返回</a>
				</div>
				<div class="threepoint">
					<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
					<div class="pf_point" style="display: none;">
						<div class="img_point">
							<img src="/images/front/top_img2.png" width="8" height="7" />
						</div>
						<div class="ulist_point">
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/releaseGoods');">发布拍品</a>
							</div>
							<div class="ist_point">
								<a href="javascript:void(0);">拍品管理</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%-- <div style="text-align: center;line-height: 50px;font-size: 20px;font-weight: bold;color: #FFFFFF;">
				<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="location.href='/instDesc/${ap.instId}'">机构简介</button>
				<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="location.href='/instAnno/${ap.instId}'">拍场公告</button>
				<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="location.href='/myAGList/${ap.id}'">我拍到的</button>
				<button type="button" class="btn weui_btn_primary" style="color: #FFFFFF;" onclick="location.href='/shop/${ap.userId}'">进他店铺</button>
				
				<button type="button" class="btn btn-primary" onclick="location.href='/instDesc?instId=${ap.instId}'">机构简介</button>
				<button type="button" class="btn btn-success" onclick="location.href='/instAnno?instId=${ap.instId}'">拍场公告</button>
				<button type="button" class="btn btn-warning" onclick="location.href='/instAnno?instId=${ap.instId}'">我拍到的</button>
				<button type="button" class="btn btn-info" onclick="location.href='/instAnno?instId=${ap.instId}'">进他店铺</button>
			</div> --%>
			
			<div id="aucPicContent" style="position:relative;margin-bottom:100px;  margin-top: 50px;">
				<c:forEach items="${goodsList}" var="goods">
					<div class="pointer" onclick="lh.jumpR('/aqg/${goods.goodsId}/${auctionId}');">
						<img src="${goods.picPath}" style="width:320px;max-height:156px;overflow: hidden;"/>
						<div class="auc_pic_txt_div"><span class="auc_pic_txt_span">&nbsp;编号：${goods.goodsSn}<br/>&nbsp;起拍价：${goods.priceBegin}</span></div>
					</div>
				</c:forEach>
				
			</div>

			<div class="t_0100_18">
				<div class="tt_0100_5">
					<div id="host_write" class="host_write pointer" onclick="lh.jumpR('/auctionQuickHall?typeId=${aq.typeId}');">
							<div style="text-align: center;line-height: 50px;font-size: 20px;font-weight: bold;color: white;background: rgb(146, 8, 8) none repeat scroll !important;">进入拍场</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <button type="button" class="btn btn-primary" style="position: fixed;right: 10px;top: 45%" onclick="showQRCode('show', null, '${ap.instLogo}', '${refer}');">二维码</button>
    <input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%--  <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%-- <%@ include file="/views/front/common/z_div_monkey_nav.htm"%> --%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/common/common_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/wookmark/wookmark.min.js"></script>
	<script type="text/javascript" src="/third-party/qrcode/utf.js"></script>
	<script type="text/javascript" src="/third-party/qrcode/jquery.qrcode.js"></script>
	<script type="text/javascript" src="/js/front/common.js" title="v"></script>
	<script type="text/javascript" src="/js/front/auction/quick/quickGoods.js" title="v"></script>
	<script type="text/javascript">
		$('#aucPicContent').wookmark({
			container: $('#aucPicContent'),
			//offset: 11,
			//outerOffset : 50,
			//direction:'right',
			align:'left',
			flexibleWidth:'40%',
			itemWidth: 130
		});
	</script>
	<script id="template" type="x-tmpl-mustache"></script>

</body>
</html>
