﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/mobiscroll/css/mobiscroll.custom-2.6.2.min.css"/> 
</head>
<body style="background-color: #f5f5f5;margin-top: 48px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->	
	<div class="Listpage">
		<div class="top46"></div>
		<div class="page-bizinfo">
			<div class="ipt_item_div">
				<label>场次名称：</label>
				<input type="text" id="auctionName" value="${aq.auctionName}" class="ipt_item">
			</div>
			<%-- <div class="ipt_item_div">
				<label>推广红包：</label>
				<input type="number" id="spreadPackets" value="${aq.spreadPackets}" class="ipt_item">
			</div> --%>
			<div class="clear-both" style="margin:5px 0px;">
				<button type="button" class="btn weui_btn_primary" style="color:green;width: 100%;">已选拍品</button>
			</div>
			
			<div id="auctionList" class="clear-both" style="text-align: center;padding:5px;">
			</div>
			<div class="clear-both" style="margin:5px 0px;">
				<button type="button" class="btn weui_btn_primary" onclick="lh.jumpR('/addGoods?auctionInst=2')" style="color:#920808;width: 100%;">发布新的藏品</button>
			</div>
			<div class="clear-both" style="margin:5px 0px;">
				<button type="button" class="btn weui_btn_primary" style="color:#920808;width: 100%;">备选拍品</button>
			</div>
			<div id="goodsList" class="clear-both" style="text-align: center;padding:5px;">
					
			</div>
		</div>
	</div>
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	<div class="save bottomFix" id="antiqueCityShop">
		<ul>
			<li><a href="javascript:;" class="a_say" onclick="saveProfession();return false;">保存</a></li>
		</ul>
	</div>
	
	<div id="priceWin" class="szweituo" style="display:none;">
		<div class="szweituodiv">
			<div class="szwtcontent">
				<h4>设置藏品起拍价格</h4>
				<div class="szwtinput">
					<div id="offerIptDiv" class="szwtinpk" style="">
						<form id="entrustFrom">
							<span> <input type="number" id="priceBegin" placeholder="请在此输入该藏品的起拍价格" value="">
							</span>
						</form>
						<div style="clear: both;"></div>
					</div>
					<div class="szwtbutton">
						<span id="priceCancel" onclick="doPriceBegin('cancel')" class="szwtbutr entrusta2">取消</span> 
						<span id="priceSave" onclick="doPriceBegin('save')" class="szwtbutl entrusta1">确定</span>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="auctionId" value="${aq.id}">
	<input type="hidden" id="instId" value="${inst.id}">
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<!-- <div id="mask"></div> -->

	<%-- <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%-- <%@ include file="/views/front/common/z_div_type_slide.htm"%> --%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/js/mobiscroll.custom-2.6.2.min.js"></script>
	<script type="text/javascript" src="/third-party/mobiscroll/dev/js/mobiscroll.core-2.6.2-zh.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script src="/js/front/auction/quick/aqAddOrUpdate.js" title="v"></script>
	
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<div class="goodsDiv" id="img_{{ id }}">
			<img src="{{ picPath }}" class="goods_pic" onclick="selectGoods({{ id }});">
			<div class="goods_pic_txt_div"><span onclick="showPriceBegin({{ id }});" class="auc_pic_txt_span">&nbsp;起拍价：<span id="priceBegin_{{ id }}">0</span><span style="color:green;"> 点击输入或修改</span><br>&nbsp;名称：{{ goodsName }}</span></div>
		</div>
	{{/rows}}	 		 
	</script>
</body>
</html>
