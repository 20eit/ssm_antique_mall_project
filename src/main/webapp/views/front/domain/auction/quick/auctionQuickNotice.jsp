﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/userCenter.css" title="v" />
<link rel="stylesheet" type="text/css" href="/third-party/select2/css/select2.min.css"/>

</head>
<body style="background-color: #f5f5f5;">
<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<input type="hidden" id="noticeTypeIds" value="${noticeTypeIds}">
	 <div class="weui_cells weui_cells_form" style="margin-top:50px;">
	 	
	 	<div style="padding: 10px 15px;text-align: center;font-size: 18px;font-weight: 600;color: #FFFFFF;">即时拍开拍提醒设置</div>
	 	
	 		<!-- <li id="sld_type_000" onclick="switchSldSrhType();" class="l1 on"><a href="javascript:void(0);" class="l1_a">全部</a></li>
			<li id="sld_type_129" onclick="switchSldSrhType(129);" class="l1"><a href="javascript:void(0);" class="l1_a">杂珍</a></li>
			<li id="sld_type_128" onclick="switchSldSrhType(128);" class="l1"><a href="javascript:void(0);" class="l1_a">陶瓷</a></li>
			<li id="sld_type_127" onclick="switchSldSrhType(127);" class="l1"><a href="javascript:void(0);" class="l1_a">金石</a></li>
			<li id="sld_type_126" onclick="switchSldSrhType(126);" class="l1"><a href="javascript:void(0);" class="l1_a">竹木</a></li>
			<li id="sld_type_125" onclick="switchSldSrhType(125);" class="l1"><a href="javascript:void(0);" class="l1_a">玉器</a></li>
			<li id="sld_type_124" onclick="switchSldSrhType(124);" class="l1"><a href="javascript:void(0);" class="l1_a">珠宝</a></li>
			<li id="sld_type_123" onclick="switchSldSrhType(123);" class="l1"><a href="javascript:void(0);" class="l1_a">书画</a></li>
			<li id="sld_type_122" onclick="switchSldSrhType(122);" class="l1"><a href="javascript:void(0);" class="l1_a">钱证</a></li>
			<li id="sld_type_121" onclick="switchSldSrhType(121);" class="l1"><a href="javascript:void(0);" class="l1_a" style="border: 0;">文玩</a></li> -->
    	
		<div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">杂珍</div>
            <div class="weui_cell_ft">
                <input id="goodsType_129" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">陶瓷</div>
            <div class="weui_cell_ft">
                <input id="goodsType_128" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">金石</div>
            <div class="weui_cell_ft">
                <input id="goodsType_127" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">竹木</div>
            <div class="weui_cell_ft">
                <input id="goodsType_126" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">玉器</div>
            <div class="weui_cell_ft">
                <input id="goodsType_125" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">珠宝</div>
            <div class="weui_cell_ft">
                <input id="goodsType_124" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">书画</div>
            <div class="weui_cell_ft">
                <input id="goodsType_123" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">钱证</div>
            <div class="weui_cell_ft">
                <input id="goodsType_122" class="weui_switch" type="checkbox">
            </div>
        </div>
        <div class="weui_cell weui_cell_switch">
            <div class="weui_cell_hd weui_cell_primary">文玩</div>
            <div class="weui_cell_ft">
                <input id="goodsType_121" class="weui_switch" type="checkbox">
            </div>
        </div>
        
	 </div>
	  <div class="pz_down">
		<div class="c_0100_9"></div>
	 </div>
	 <div class="save bottomFix" style="z-index: 1;">
		<ul>
			<li><a href="javascript:;" class="a_say" id="" onclick="aqNoticeAddOrUpdate();return false;">保存</a>
	  			<!-- <a href="javascript:;" class="weui_btn weui_btn_primary" onclick="addGoods();return false;">发布</a> -->
			</li>
		</ul>
	  </div>
    
	  <input type="hidden" value="${r}" id="r"/> 	
	  <input type="hidden" value="${loginStatus}" id="loginStatus"/>
	  
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/select2/js/select2.min.js"></script>
	<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
	<script type="text/javascript" src="/js/front/auction/quick/auctionQuickNotice.js" title="v"></script>
	
</body>
</html>
