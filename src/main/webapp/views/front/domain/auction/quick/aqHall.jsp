﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
	<input type="hidden" value="${noPhone}" id="noPhone">
	<input type="hidden" value="${noPayPassword}" id="noPayPassword">
	<input type="hidden" value="${ap.bail}" id="bail">
	<input type="hidden" value="${shop.creditMargin}" id="shopBail">
	<input type="hidden" value="${shop.id}" id="shopId">
	<div id="pzMain" class="pz_main">
		<div class="c_0100_20">
			<div id="t_0100_11" class="c_0100_1">
				<div class="l_510">
					<a id="auctionSerialShow" href="javascript:history.back(-1)">
						<%-- 第${ap.auctionSerial}场 --%>
						${aq.auctionName}
						（
							<span id="have_begun_text">
								<c:if test="${aq.haveBegun == 2}"> 拍卖中</c:if>
								<c:if test="${ null ==  aq.haveBegun || aq.haveBegun == 1}"> 预展</c:if>
							</span>
						  ）
					</a>
				</div>
				<div class="threepoint">
					<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
					<div class="pf_point" style="display: none;">
						<div class="img_point">
							<img src="/images/front/top_img2.png" width="8" height="7" />
						</div>
						<div class="ulist_point">
							<div class="ist_point">
								<a href="javascript:lh.jumpR('/releaseGoods');">发布拍品</a>
								<a href="javascript:lh.jumpR('/aqIndex');">返回即时拍</a>
							</div>
							<div class="ist_point">
								<a href="javascript:javascript:lh.jumpR('/');">返回首页</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="t_0100_16" class="t_0100_16">
				<div class="li_sheng">
					仅剩&nbsp;<span id="goodsRemain" class="span_1"></span>&nbsp;件
				</div>
				<div class="li_sheng">
					成交&nbsp;<span id="goodsDeal" class="span_1"></span>&nbsp;件
				</div>
				<div class="li_sheng">
					共计&nbsp;<span id="goodsTotal" class="span_1"></span>&nbsp;件
				</div>
			</div>
			<div id="t_0100_17" class="t_0100_17">
				<div class="tt_0100_4">
					<div class="l_170">
						<img id="picPath" onclick="lh.jumpR('/aqgList/${aq.id}');" width="100%" />
						<div class="pf_tin">
							<a href="javascript:lh.jumpR('/aqgList/${aq.id}');">查看大图</a>
						</div>
					</div>
					<div class="r_488">
						<div class="t_488_1">
							<a href="javascript:"><nobr id="goodsName">${goods.goodsName}</nobr></a>
						</div>
						<div id="serialShow" class="t_488_2 mgV5">当前第<span id="currentNum"></span>件<span id="serialSpan"> 编号<span id="goodsSerial"></span>号</span></div>
						<div class="t_488_3">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="47%" class="span_2">起拍价：<span id="priceBegin" class="span_3"></span> 元
									</td>
									<td width="3%">&nbsp;</td>
									<!-- <td id="bail" width="50%" class="span_2">保证金：无</td> -->
								</tr>
							</table>
						</div>
						<!-- <div class="t_488_4" style="padding:3px 0px;">
							<div id="grade" style="width:auto;" class="c_l_52">拍场等级：</div>
							<a href="javascript:void(0);" class="r_zan">赞(136)</a>
						</div> -->
					</div>
				</div>
			</div>
			<div id="t_0100_18" class="t_0100_18">
				<div id="t_0100_5" class="tt_0100_5">
					<div class="title_3">
						<div id="hostName" class="tit_4">主持人：${aq.username}</div>
						<div class="more_3" onclick="javascript:lh.jumpR('/sale/${aq.userId}');">
							<a href="javascript:void(0);"><img src="/images/front/shop_img9.png" width="14" height="22" /></a>
						</div>
					</div>
				</div>
				<div class="tt_0100_6">
					<div id="host_show" class="host_show">
						<div class="li_host">
							<div class="up_host">当前 (元)</div>
							<div class="down_host">
								<span id="currentPrice"></span>
							</div>
						</div>
						<div class="li_host">
							<div class="up_host">领先</div>
							<div class="down_host">
								<span id="currentUser"></span>
							</div>
						</div>
						<div class="li_host">
							<div class="up_host">出价 (次)</div>
							<div class="down_host">
								<span id="offerTimes"></span>
							</div>
						</div>
						<span id="pf_host_cha" class="pf_host_cha">
							<a href="javascript:;"><img src="/images/front/sale_img11.jpg" width="20" height="20" /></a>
						</span>
						<span id="pf_host_cha2" class="pf_host_cha fr" style="display: none;">
							<a href="javascript:;"><img src="/images/front/sale_img15.png" width="20" height="20" /></a>
						</span>
					</div>
					<div id="msg_container" class="host_sale_box">
						<!-- 聊天内容容器 -->
					</div>
					<div style="position: fixed;bottom: 75px;text-align: right;width: 100%;max-width: 640px;padding-right:28px;">
						<button id="hostCountdown" type="button" class="btn btn-sm btn-warning" style="display:none;" onclick="sendGroupMsgCountDown();">倒计时</button>
						<button type="button" class="btn btn-sm btn-danger" style="" onclick="sendBonus();">发红包</button>
					</div>
					<div id="timmer_number" class="timmer_number frontHide">10</div>
					<div id="host_write" class="host_write">
						<div class="l_110_2" onclick="switchMsgOffer('msg');">
							<img src="/images/front/sale_img12.png" width="35" height="24" style="margin: 5px 0 0 0;" />
						</div>
						<div id="count_box" class="count_box">
							<div id="count_less" class="count_less" onclick="changePrice('del');">
								<a class="a_less">-</a>
							</div>
							<div id="amount" class="amount">
								<input id="offerPrice" type="number" class="count_input" onblur="changePrice();" />
							</div>
							<div id="count_add" class="count_add" onclick="changePrice('add');">
								<a class="a_less" style="float: right;">+</a>
							</div>
						</div>

						<div id="offerSend" class="r_outprice" onclick="sendOffer();">
							<a href="javascript:void(0);">出价</a>
						</div>
					</div>
					<div id="host_sub" class="host_write" style="display: none;">
						<div class="l_110_3" onclick="switchMsgOffer('offer');">
							<img src="/images/front/sale_img14.png" width="26" height="26" style="margin-top: 4px;" />
						</div>
						<div class="submib_box">
							<input type="text" name="textfield" id="msgContent" class="inputbox" />
						</div>
						<div id="msgSend" class="r_outprice" onclick="sendMsg();">
							<a href="javascript:void(0);">发送</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<input type="hidden" id="auctionId" value="${aq.id}"/>
	<input type="hidden" id="username" value="${username}"/>
	<input type="hidden" id="chatGroupId" value="${aq.chatGroupId}"/>
	<input type="hidden" id="sig" value="${sig}"/>
	<input type="hidden" id="timeStamp" value="${timeStamp}"/>
	<input type="hidden" id="senderId" value="${senderId}"/>
	<input type="hidden" id="userTokenId" value="${userTokenId}"/>
	<input type="hidden" id="userTokenPswd" value="${userTokenPswd}"/>
	<input type="hidden" id="senderAvatar" value="${senderAvatar}"/>
	<input type="hidden" id="senderName" value="${senderName}"/>
	<%-- <input type="hidden" id="receiverId" value="${receiverId}"/>
	<input type="hidden" id="receiverTokenId" value="${receiverTokenId}"/>
	<input type="hidden" id="receiverAvatar" value="${receiverAvatar}"/>
	<input type="hidden" id="receiverName" value="${receiverName}"/> --%>
	<input type="hidden" id="senderSerial" value="${senderSerial}"/>
	
	<input type="hidden" id="haveBegun" value="${aq.haveBegun}"/>
	<input type="hidden" id="instGrade" value="${aq.grade}"/>
	<input type="hidden" id="host" value="${host}"/>
	<input type="hidden" value="${r}" id="r"/> 	
	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	
	<input type="hidden" value="${creditMoneyLack}" id="creditMoneyLack"/> 	
	<input type="hidden" value="${bail}" id="bail"/> 	
	<input type="hidden" value="${creditMoney}" id="creditMoney"/> 	
	
	<%--  <%@ include file="/views/front/common/z_div_menu_bottom.htm"%> --%><!-- 底部菜单 -->
	<%-- <%@ include file="/views/front/common/z_div_monkey_nav.htm"%> --%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript">
		function initHallHeight(){
			var h0 = window.innerHeight;
			//var h0 = document.documentElement.clientHeight;
			//var h0 = document.body.clientHeight;
			//h0 = parseInt(h0) - 250;
			//alert(h0);
			document.getElementById('pzMain').style.height = h0+'px';
			var h1 = document.getElementById('t_0100_11').clientHeight;
			var h2 = document.getElementById('t_0100_16').clientHeight;
			var h3 = document.getElementById('t_0100_17').clientHeight;
			var h4 = document.getElementById('t_0100_5').clientHeight;
			var h5 = document.getElementById('host_show').clientHeight;
			var h = h1+h2+h3+h4+h5;
			var finalH = h0 - h - 45;//54:底部栏高度
			document.getElementById('msg_container').style.height = finalH+'px';
			//document.getElementById('msg_container').style.height = '640px';
		}
		initHallHeight();
		//document.getElementById('msg_container').style.height = '640px';
	</script>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script src="http://app.cloopen.com/im50/ytx-web-im.min.js"></script>
	<script type="text/javascript" src="/js/front/chat/common_base_chat.js" title="v"></script>
	<script type="text/javascript" src="/js/front/chat/common_group_chat.js" title="v"></script>
	<script type="text/javascript" src="/js/front/auction/quick/aqHall.js" title="v"></script>
	
	<script id="template_other" type="x-tmpl-mustache">
	<div class="host_say">
		<div class="host_tit titcolor">
			<div class="hosttit_l {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.sendHour}}</div>
			<div class="hosttit_r {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.senderName}}</div>
		</div>
		<div class="host_text">
			<div {{#chat.bonus}}onclick="receiveBonus({{chat.bonus}});"{{/chat.bonus}} class="hosttext_r {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.content}}{{&chat.price}}</div>
		</div>
	</div>
	</script>
	
	<script id="template_self" type="x-tmpl-mustache">
	<div class="host_say">
		<div class="host_tit titcolor">
			<div class="hosttit_l {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.sendHour}}</div>
			<div class="hosttit_r {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.senderName}}</div>
		</div>
		<div class="host_text">
			<div {{#chat.bonus}}onclick="receiveBonus({{chat.bonus}});"{{/chat.bonus}} class="hosttext_r {{&chat.msgGroup}} {{&chat.myMsg}}">{{chat.content}}{{&chat.price}}</div>
		</div>
	</div>
	</script>

</body>
</html>
