<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
</head>
<body style="background-color: #920808;background-image: url('/images/front/yao_img14.png');background-repeat: no-repeat;background-position:50% 160px;">
	<div class="pz_main" style="height: 100%;">
		<div id="goods_main" class="c_566" style="position: relative;top: -1500px;">
	    	<div class="c_566_1">
	        	<div class="t_525">
	        	    <div class="img_525" style="max-height:600px;overflow: hidden;">
	        	    	<a id="a_goodsPic" href=""><img id="goodsPic" src="" width="100%" /></a>
	        	    </div>
	                <div class="tit_525">
	                    <div class="label_box" style="margin-left:1px;float:right;">
	                    	<div class="label_cell cbd1">七天退换</div>
	                        <div class="label_cell cbd2">实名认证</div>
	                        <div class="label_cell cbd3">担保交易</div>
	                        <!-- <div class="label_cell_2">包邮</div> -->
	                    </div>
	                </div>
	       	    </div>
	            <div class="t_525_2">
	                <div class="l_262">
	                	<a id="goodsShop" href="javascript:void(0);">
	                		<span id="shopName"></span>
	                	</a>
	                </div>
	                <!-- <div class="r_262">店铺评分：<span>5分</span></div> -->
	            </div>
	            <div class="t_525_3">
	            	<span id="goodsName"></span><br/>
	            	<div id="goodsDesc"></div>
	            </div>
	            <div class="t_525_4">当前价格：￥<span id="goodsPrice"></span></div>
	    	</div>
	    </div>
    	<div id="shake_bottom" class="shake_bottom widthControl" style="background-color: #920808;">
    		<div class="c_0100_11 widthControl">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%" height="30">&nbsp;</td>
						<td width="56%" align="center" class="m_56">选择您感兴趣的品类</td>
						<td width="22%">&nbsp;</td>
					</tr>
				</table>
			</div>
			<div class="c_0100_12">
				<ul id="ul_dom">
					<li id="item_129" style="border: 2px solid #920808;" onclick="switchGoodsType(129);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg1"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">杂珍</a></div>
					</li>
					<li id="item_128" style="border: 2px solid #920808;" onclick="switchGoodsType(128);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg2"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">陶瓷</a></div>
					</li>
					<li id="item_127" style="border: 2px solid #920808;" onclick="switchGoodsType(127);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg3"></a></div>
						<div class="tit_yao">	<a href="javascript:void(0);">金石</a></div>
					</li>
					<li id="item_126" style="border: 2px solid #920808;" onclick="switchGoodsType(126);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg4"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">竹木</a></div>
					</li>
					<li id="item_125" style="border: 2px solid #920808;" onclick="switchGoodsType(125);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg5"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">玉器</a></div>
					</li>
					<li id="item_124" style="border: 2px solid #920808;" onclick="switchGoodsType(124);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg6"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">珠宝</a></div>
					</li>
					<li id="item_123" style="border: 2px solid #920808;" onclick="switchGoodsType(123);">
						<div class="img_56"><a href="javascript:void(0);" class="ayaobg7"></a></div>
						<div class="tit_yao"><a href="javascript:void(0);">书画</a></div>
					</li>
				</ul>
			</div>
	    	<div>
				<button id="shake_btn" onclick="loadGoodsAfterShake();" type="button" class="btn btn-success" style="position: absolute;right: 10px;bottom: 110px;"> 摇一摇 </button>
				<button id="audio_btn" onclick="toggleAudio();" role="2" type="button" class="btn btn-success" style="position: absolute;left: 10px;bottom: 110px;"> 关闭声音 </button>
			</div>
    	</div>
	</div>
	<div id="voice">
	</div>
	<audio id="shaking" src="/third-party/shake/shaking.ogg" preload="auto"></audio>
	<audio id="shaked" src="/third-party/shake/shaked.ogg" preload="auto"></audio>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->

	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/shake/shake.js"></script>
	<script src="/js/front/shake/shake_main.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	{{/rows}}	 		 
	</script>

</body>
</html>
