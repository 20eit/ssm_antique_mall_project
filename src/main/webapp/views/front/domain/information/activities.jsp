<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/news.css" title="v" />
</head>
<body style="background-color: #f5f5f5;">
<input type="hidden" id="userId" value="${userId}">
<div class="pz_main">
		<div class="c_0100_19">
			 <div class="slide_sale slide_teacher c_0100_3" style="padding:0;text-align: center;" id="slide_sale">
				<div class="cytopdiv">
					<!-- <ul>
						<li style="margin:0px;position: relative;border-radius: 5px 0 0 5px;border-right: none;" class="pointer on" >展会活动</li>
						<li style="margin:0px;border-radius: 5px 0 0 5px;" class="pointer" onclick="location.href='/news'">新闻资讯</li>
					</ul> -->
					<span class="cytopdiv1 pointer" style="color:white;" onclick="lh.jumpR('/activity');">展会活动</span>
					<span class="cytopdiv2 pointer" onclick="lh.jumpR('/news');">新闻资讯</span>
				</div>
			</div>
			<div class="slide_sale slide_teacher c_0100_3" style="padding:0;text-align: center;" id="slide_sale">
				<div class="cytopul">
					 <ul>
						<li id="allActivities" class="pointer" style="border-bottom:1px solid red;color:red;" onclick="allActivities();return false;">全部</li>
						<li id="sevenDay" onclick="sevenDay();return false;">一周内</li>
						<li id="myComment" onclick="myComment(); return false;">我评论的</li>
						<li id="myActivities"  class="pointer" onclick="myActivities();return false;">我发起的</li>
					</ul>
				</div>
			</div>
			 <div class="slide_sale slide_teacher c_0100_3" style="padding:0;text-align: center;" id="slide_sale">
				<div class="addActivies">
					<span class="pointer" onclick="lh.jumpR(''/addActivity');">发起活动</span>
				</div>
			</div>
		</div>
		<div id="activities" class="t_0100_3">
		</div>
		<div id="resultTip" class="resultTip frontHide"></div>
		<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
		<div class="pz_down">
			<div class="c_0100_9"></div>
		</div>
	</div>

	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	<input type="hidden" value="${r}" id="r"/> 	<input type="hidden" value="${loginStatus}" id="loginStatus"/>
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/js/front/information/activities.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
		{{#rows}}
		<ul class="c_0100_3" style="padding:0">
			<li onclick="lh.jumpR('/activity/{{id}}');" class="pointer">
				<div class="r_500" style="float:left;">
					<div class="t_500_1">
						<a href="javascript:void(0);"><nobr>{{title}}</nobr></a>
					</div>
					<div class="t_500_2">
						<nobr>日期：{{startDate}}&nbsp;-&nbsp;{{endDate}}</nobr>
					</div>
					<div class="t_500_3">
						<nobr>发起方：{{institution}}</nobr>
					</div>
					<div class="t_500_3">
						<nobr>地址：{{address}}</nobr>
					</div>
				</div>
				<div class="l_155" style="float:right;">
					<img src="{{picPath}}" width="100%" />
				</div>
			</li>
		</ul>
		{{/rows}}		 		 
	</script>
</body>
</html>
