<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/auction.css" title="v" />
<link rel="stylesheet" type="text/css" href="/css/front/login.css" title="v" />
</head>
<body id="zyidcard" style="margin-top: 48px; max-width: 480px; background: #F2F2F2; min-width: 320px;">
	<%@ include file="/views/front/common/z_div_top_nav.htm"%><!-- 顶部导航 -->
	<div id="ulMobile" class="newregist">
		<div style="position: absolute; right: 0; opacity: 0.2; top: 10px">
			<img src="/images/front/Upload_Close.png" style="width: 40px" onclick="$('#mobile').val('').focus();">
		</div>
		<input id="mobile" name="mobile" type="tel" placeholder="请输入手机号码" onfocus="this.focused=true;this.select();"
			onmouseup="if(this.focused){this.focused=false;return false;}">

		<div id="btnNext" class="newregbut" onclick="afterMobile();">
			<span>下一步</span>
		</div>
	</div>
	<div id="divSended" class="newreged" style="display: none; z-index: 100;">
		<div style="text-align: center;">
			<span style="background: #D6D6D6;">重发验证码</span>
		</div>
	</div>
	<div id="ulCode" class="newregist" style="display: none; position: relative;">
		<div style="padding: 10px 0 4px 8px; font-size: 14px; color: #7c8e9d;">请查收手机短信验证码，填入下方</div>
		<div>
			<span id="btnMobile" onclick="getVerifycode();" class="newregistsp">获取验证码</span> 
			<input type="tel" id="verifycode" placeholder="输入手机接收到的短信验证码" style="float: left; width: 60%;" onfocus="this.focused=true;this.select();" onmouseup="if(this.focused){this.focused=false;return false;}" name="verifycode">
		</div>
		<div style="clear: both;"></div>
		<!-- <div id="btnNext2" class="newregbut" style="margin-top: 10px;" onclick="afterCode();">
			<span>下一步</span>
		</div> -->
	</div>
	<div id="ulPassword" class="newregist" style="display: none; position: relative;">
		<div id="nicknameLable" class="settingTips" style="padding: 10px 0px 4px 8px; font-size: 14px; color: rgb(124, 142, 157); display: none;">请设置昵称</div>
		<input type="text" id="nickname" placeholder="请输入昵称" value="" name="nickname" style="display: none;">
		<!-- 请设置密码、昵称 -->
		<div id="passwordLable" class="settingTips" style="padding: 10px 0px 4px 8px; font-size: 14px; color: rgb(124, 142, 157); display: none;">请设置登陆密码</div>
		<input type="password" id="password" placeholder="请输入密码（6-20位字符）" name="password">
		<div id="iforGet" style="margin: 2px 0px; text-align: right;">
			<a href="javascript:lh.jumpR('/passwordFind');"
				style="display: inline-block; height: 40px; line-height: 40px; color: #25a431; margin-right: 8px; font-size: 15px;">不知道密码怎么办？点我找回</a>
		</div>
		<div id="msgTip" class="newregwz">
			收不到短信请拨 <a href="tel:40000000" style="color: #25a431; font-size: 18px;">40000000</a> 热线进行注册
		</div>
		<div id="btnSubmit" class="newregbut" onclick="login();" style="margin-top: 10px;">
			<span>完成</span>
		</div>
	</div>

	<input type="hidden" id="r" value="${r}"/>
	<%-- <input type="hidden" value="${loginStatus}" id="loginStatus"/> --%>
	<input type="hidden" id="jumpUrl" value="${jumpUrl}"/>
	<div id="maskDiv" class="">&nbsp;</div>
	
	<div id="loginToast" class="weui_loading_toast" style="display: none;">
        <div class="weui_mask_transparent"></div>
        <div class="weui_toast">
            <div class="weui_loading">
                <div class="weui_loading_leaf weui_loading_leaf_0"></div>
                <div class="weui_loading_leaf weui_loading_leaf_1"></div>
                <div class="weui_loading_leaf weui_loading_leaf_2"></div>
                <div class="weui_loading_leaf weui_loading_leaf_3"></div>
                <div class="weui_loading_leaf weui_loading_leaf_4"></div>
                <div class="weui_loading_leaf weui_loading_leaf_5"></div>
                <div class="weui_loading_leaf weui_loading_leaf_6"></div>
                <div class="weui_loading_leaf weui_loading_leaf_7"></div>
                <div class="weui_loading_leaf weui_loading_leaf_8"></div>
                <div class="weui_loading_leaf weui_loading_leaf_9"></div>
                <div class="weui_loading_leaf weui_loading_leaf_10"></div>
                <div class="weui_loading_leaf weui_loading_leaf_11"></div>
            </div>
            <p class="weui_toast_content">正在登录中....</p>
        </div>
    </div>
	
	<%@ include file="/views/front/common/z_div_monkey_nav.htm"%><!-- 猴子导航 -->
	<%@ include file="/views/front/common/z_div_type_slide.htm"%><!-- 右侧分类查询 -->
	<%@ include file="/views/front/common/z_div_qrcode.htm"%><!-- 二维码弹出框 -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<script type="text/javascript" src="/js/front/login/login.js" title="v"></script>
</body>
</html>



