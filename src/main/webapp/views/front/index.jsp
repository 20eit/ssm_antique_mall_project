﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>微拍客</title>
<%@ include file="/views/common/common_front_wpk_css.htm"%>
<link rel="stylesheet" href="/cssjs_stable/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="/css/front/wpk/style_v1.css" title="v">
<link rel="stylesheet" href="/css/front/wpk/my_v1.css" title="v" />
<style type="text/css">
	.body{
		background: #FFFFFF;
		background-color: #FFFFFF;
	}
	.mt6{
		background: #FFFFFF;
		background-color: #FFFFFF;
	}
	#data-container{
		background: #FFFFFF;
		background-color: #FFFFFF;
	}
	
</style>
</head>
<body>
	<input type="hidden" id="usersId" value="${user.id}">
	<div class="container-fluid">
		<div class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<c:forEach var="banner" items="${bannerList }" varStatus="status">	
						<c:if test="${status.index == 0}">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						</c:if>
						<c:if test="${status.index != 0}">
							<li data-target="#carousel-example-generic" data-slide-to="${status.index}"></li>
						</c:if>
					</c:forEach>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<c:forEach var="banner" items="${bannerList }" varStatus="status">	
						<div class="item <c:if test="${status.index == 0}">active</c:if>" >
							<img src="${banner.picPath }@414w_166h_4e_240-240-246bgc" class="img-img-responsive">
						</div> 
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="row pb10">
			<div class="col-xs-12 pt10 plr0 text-center menuMsg">

				<!-- <a id="tests" onclick="lh.jumpR('/ap/page/manage');" href="javascript:;" class="weui_btn weui_btn_primary">专场管理</a><br/>
				<a id="tests" onclick="lh.jumpR('/am/page/manage');" href="javascript:;" class="weui_btn weui_btn_primary">微拍管理</a><br/> -->
				<!-- <a id="tests" onclick="lh.jumpR('/goods/page/add');" href="javascript:;" class="weui_btn weui_btn_primary">发布藏品</a><br /> 
				<a id="tests" onclick="lh.jumpR('/login?flag=1');" href="javascript:;" class="weui_btn weui_btn_primary">快捷登陆</a> 
				<a id="tests" onclick="lh.jumpR('/forumApply');" href="javascript:;" class="weui_btn weui_btn_primary">圈子申请</a> <br /> -->

				<div class="col-xs-3 plr0" onclick="lh.jumpR('/am');">
					<img src="/images/front/index_menu_01.png" class="img-responsive center-block">
					<div class="pt8">微拍</div>
					<!-- <i class="red icon-circle"></i> -->
				</div>
				<div class="col-xs-3 plr0" onclick="lh.jumpR('/ap');">
					<img src="/images/front/index_menu_02.png" class="img-responsive center-block">
					<div class="pt8">专场</div>
				</div>
				<div class="col-xs-3 plr0" onclick="lh.jumpR('/forumIndex');">
					<img src="/images/front/index_menu_06.png" class="img-responsive center-block">
					<div class="pt8">社区</div>
				</div>
				<div class="col-xs-3 plr0" onclick="lh.jumpR('/shangjia');">
					<img src="/images/front/index_menu_05.png" class="img-responsive center-block">
					<div class="pt8">商家</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 plr0 bdt ptb10">
					<div class="col-xs-3 bdr">
						<img src="/images/front/top_line.png" width="42" height="42" class="img-responsive center-block">
					</div>
					<div class="col-xs-9 pl7 pr0 fs10">
					<c:forEach var="article" items="${articleList }">
						<div class="col-xs-12 plr0">
							<div class="col-xs-1 plr0 text-center">
								<i class="red_deep icon-circle"></i>
							</div>
							<div class="col-xs-11 pr0 elli pl7"  onclick="lh.jumpR('/forumDetail/${article.id}');">${article.content }</div>
						</div>
					</c:forEach>
					</div>
				</div>
			</div>
		</div>

		<div class="row h10 bg_gray"></div>
		 
		<!-- 专场 开始 -->
		<c:forEach var="ap"  items="${apList}">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 ptb5 plr0 bdb"  onclick="lh.jumpR('/ag/page/agList/${ap.serial }');">
					<div class="col-xs-2 plr0">
						<img class="img-responsive center-block" width="48" height="48" src="${ap.instLogo }@50w_50h_4e_240-240-246bgc_50Q" />
					</div>
					<div class="col-xs-8 pr0">
						<div class="col-xs-12 plr0" >
							${ap.auctionName }<i class="weui_icon_safe weui_icon_safe_success fs14"></i>
						</div>
						<div class="col-xs-12 plr0 pt5 fs10">
							<div class="authentication">七天无理由退款</div>
							<div class="authentication ml7">质量保证</div>	
						</div>
					</div>
					<!-- <div class="col-xs-2 plr0 text-right" id="more">
						<div class="dropdown">
							<button class="btn btn-default index-dropdown-button dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">...</button>
							 <ul class="dropdown-menu index-dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="#">关注</a></li>
								<li><a href="#">咨询</a></li>
								<li><a href="#">打电话</a></li>
							</ul> 
						</div>
					</div> -->
				</div>
			</div>
			<div class="col-xs-12">
				<div class="col-xs-12 plr0 ptb10">${ap.description}</div>
				<div class="made scrollbox" id="horizontal">
					<div class="madegame"  onclick="lh.jumpR('/ag/page/agList/${ap.serial }');">	
						<img src="${ap.picPaths }@420w_150h_4e_240-240-246bgc" />
					</div>
				</div>
				<div class="col-xs-12 plr0 ptb10">
					<div class="col-xs-6 plr0 red_deep1 fw">即将开始：<fmt:formatDate value="${ap.startTime}"  pattern="MM月dd日 HH:mm"/> </div>
					<div class="col-xs-6 plr0 gray text-right">
						已加入：&nbsp;<span>${ap.joinNum}</span>&nbsp;&nbsp;&nbsp;
						<i>围观：</i>&nbsp;<span>${ap.visitNum}</span>
					</div>
				</div>
			</div>
		</div>
		<hr style="height: 2px; background: #F0F0F6"/>
		</c:forEach>
		<!-- 专场结束 -->
		
		<div class="row h10 bg_gray"></div>
		<div class="row fs16 ptb10">
			<div class="col-xs-6">拍品推荐</div>
			<div class="col-xs-6 text-right gray" onclick="lh.jumpR('/am');">更多</div>
		</div>
		<div class="row bg_gray pt5">
			
			<div class="col-xs-12 pl7 pr0">
			<div id="data-container" >
			
			</div>
			</div>
		</div>
	</div>

	<div class="index_search_fixed" onclick="jumpToAM();">
		<input type="text" class="form-control" id="indexSearch">
	</div>
	<div class="index_search_fixed pt8 white" id="indexSearchWord">
		<i class="icon-search"></i> 请输入您想搜索的商品名称
	</div>
	<div class="indexBgOp"></div>
	
	<div class="weui-infinite-scroll">
	 <div class="infinite-preloader" ></div><!-- 菊花 -->
	  正在加载... <!-- 文案，可以自行修改 -->
	</div>
	
	<br />
	<br />
	<br />
	<br />
	<br />
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<%@ include file="/views/front/common/z_div_common.htm"%><!-- 通用DIV -->
	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_front_js.htm"%>
	<%@ include file="/views/common/common_front_wpk_js.htm"%>
	<script type="text/javascript" src="/cssjs_stable/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/third-party/swiper/swiper.3.1.7.jquery.min.js"></script>
	<script type="text/javascript" src="/js/front/wpk/my.js" title="v"></script>
	<script type="text/javascript">lh.param = ${paramJson}</script>
	<!-- <script type="text/javascript">lh.param = ${paramJson}</script> -->
	<script src="/js/front/index/index.js" title="v"></script>
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
	<div id="am_{{id}}" class="col-xs-6 pl0 pr7 pb5 .mt6" style="background-color: #ffffff;width:203.5px;height:264.5px;">
		<img class="img-responsive" src="{{picPath}}@197w_197h_4e_240-240-240bgc"  onclick="lh.jumpR('/am?amSerial={{auctionSerial}}');"/>
		<div class="col-xs-12 plr0 bg_white fs13">{{goodsName}}&nbsp;</div>
		<div class="col-xs-12 plr0 bg_white">
			<div class="col-xs-4 plr0 red">
				￥<span class="fs18">{{priceBegin}}</span>
			</div>
			
		</div>
			<div class="col-xs-8 plr0 text-left" style="float:left;width:100%;">
				<i class="icon-time fs16"></i> <span class="red">
					<span id="ctdown_hour_{{id}}" class="red_deep1 fs16">00</span>小时<span id="ctdown_minute_{{id}}" class="red_deep1 fs16">00</span>分<span id="ctdown_second_{{id}}" class="red_deep1 fs16">00</span>秒
				</span>
			</div>
	</div>
	{{/rows}}	 		 
	</script>
</body>
</html>