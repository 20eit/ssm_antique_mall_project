﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<link type="text/css" rel="stylesheet" href="/css/common.css" title="v"/>
<link rel="stylesheet" type="text/css" href="/css/front/front.css" title="v" />
<link type="text/css" rel="stylesheet" href="/third-party/jquery-weui/css/weui.min.css"/>
<link type="text/css" rel="stylesheet" href="/third-party/jquery-weui/css/jquery-weui.min.css"/>
<link rel="stylesheet" type="text/css" href="/cssjs_stable/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/cssjs_stable/css/font-awesome.min.css"/>
<!--[if IE 7]><link rel="stylesheet" href="/cssjs_stable/css/font-awesome-ie7.min.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/front/wpk/common.css" title="v"/>
<link rel="stylesheet" type="text/css" href="/css/front/wpk/style.css" title="v"/>

<%-- 首页- sys/IndexAction:/,/index --%>
</head>
<body style="background-color: #f5f5f5;">
	<div class="pz_top"> <!-- 顶部栏 开始 -->
		<div class="c_0100_1">
			<div class="t_0100_1">
				<div class="tt_one">
					<div class="l_118">
						<a href="javascript:void(0);" id="ss1" class="sstext">
							<span id="searchLabel" role="1">商铺</span>&nbsp;
							<img src="/images/front/top_img5.png" width="5" height="4" />
						</a>
					</div>
					<div class="r_305">
						<div class="l_205">
							<input name="" type="text" placeholder="输入商铺名称" class="bfom" id="ss1_input" />
						</div>
						<div class="r_18" onclick="search(1);">
							<a href="javascript:void(0);"><img src="/images/front/top_img6.png" width="18" height="18" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="threepoint">
				<a href="javascript:;" class="a_po" onclick="$('.pf_point').slideToggle(0)"><img src="/images/front/top_img1.png" width="25" height="25" /></a>
				<div class="pf_point" style="display: none;">
					<div class="img_point">
						<img src="/images/front/top_img2.png" width="8" height="7" />
					</div>
					<div class="ulist_point">
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/fansRanking');">会员列表</a>
						</div>
						<div class="ist_point">
							<a href="javascript:lh.jumpR('/market');">店铺列表</a>
						</div>
					</div>
				</div>

			</div>
			<div class="class">
				<a href="javascript:;" onclick="$('.pf_menu,.class').show(200);"><img src="/images/front/top_img3.png" width="18" height="13" />&nbsp;&nbsp;分类</a>
			</div>
		</div>
	</div> <!-- 顶部栏 结束 -->
	
	<div class="pz_main"> <!-- Baner 开始 -->
		<div class="banner">
			<div id="slideBox" class="slideBox"><!-- Banner图片 -->
				<div id="banner" class="bd frontHide">
					<ul>
						<c:forEach var="banner" items="${bannerList}">
							<li><a class="pic" href="${banner.linkPath}"><img alt="${banner.title}" src="${banner.picPath}" /></a></li>
						</c:forEach>
					</ul>
				</div>
				<div class="hd">
					<ul></ul>
				</div>
			</div>
		</div> <!-- Banner 结束 -->
		
		<!-- 分类图片 开始 -->
		<%-- <div class="c_0100_2"> 
			<ul class="ul_mtig">
				<li class="li_mtig">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="pointer" onclick="lh.jumpR('/market');">
							<td width="36%" align="center" class="l_122">杂珍<br /> <span style="font-weight: bold;">金石</span></td>
							<td width="4%">&nbsp;</td>
							<td width="60%"><img src="/images/front/main_img1.jpg" width="100%" /></td>
						</tr>
					</table>
				</li>
				<li class="li_mtig">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="pointer" onclick="lh.jumpR('/market');">
							<td width="36%" align="center" class="l_122">文玩<br /> <span style="font-weight: bold;">杂项</span></td>
							<td width="4%">&nbsp;</td>
							<td width="60%"><img src="/images/front/main_img2.jpg" width="100%" /></td>
						</tr>
					</table>
				</li>
				<li class="li_mtig">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="pointer" onclick="lh.jumpR('/market');">
							<td width="36%" align="center" class="l_122">字画<br /> <span style="font-weight: bold;">篆刻</span></td>
							<td width="4%">&nbsp;</td>
							<td width="60%"><img src="/images/front/main_img3.jpg" width="100%" /></td>
						</tr>
					</table>
				</li>
				<li class="li_mtig">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="pointer" onclick="lh.jumpR('/market');">
							<td width="36%" align="center" class="l_122">玉翠<br /> <span style="font-weight: bold;">珠宝</span></td>
							<td width="4%">&nbsp;</td>
							<td width="60%"><img src="/images/front/main_img4.jpg" width="100%" /></td>
						</tr>
					</table>
				</li>
				<div class="pf_clock pointer">
					<img src="/images/front/top_img8.gif" width="58" height="58" onclick="lh.jumpR('/inst');" />
				</div>
			</ul>
		</div> --%> <!-- 分类图片 结束 -->
		
		<div class="c_0100_3"> <!-- 8个导航按钮 开始 -->
			<ul>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/news');"><img src="/images/front/main_img5.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/news');"><nobr>活动资讯</nobr></a>
					</div>
				</li>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/forums');"><img src="/images/front/main_img6.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/forums');"><nobr>论坛</nobr></a>
					</div>
				</li>
				<li><%--  //TODO tempHide
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/fansRanking');"><img src="/images/front/main_img7.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/fansRanking');"><nobr>粉丝榜</nobr></a>
					</div> --%>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/fansRanking');"><img src="/images/front/main_img7.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/market');"><nobr>店铺</nobr></a>
					</div>
					
				</li>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/antiqueCity');"><img src="/images/front/main_img8.png" /></a>
						</div>
					</div>
					<div class="tit_main" >
						<a href="javascript:lh.jumpR('/antiqueCity');"><nobr>文玩市场</nobr></a>
					</div>
				</li>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/ap')"><img src="/images/front/main_img9.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/ap')"><nobr>专场</nobr></a>
					</div>
				</li>
				<li>
					<%-- //TODO tempHide
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/aq')"><img src="/images/front/main_img10.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/aq')/aq"><nobr>即时拍</nobr></a>
					</div> --%>
					<div class="img_main">
						<div class="div">
							<a  href="javascript:lh.jumpR('/aq');"><img src="/images/front/main_img10.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/goods');"><nobr>鬼市</nobr></a>
					</div>
				</li>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/am')"><img src="/images/front/main_img11.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/am')"><nobr>微拍卖</nobr></a>
					</div>
				</li>
				<li>
					<div class="img_main">
						<div class="div">
							<a href="javascript:lh.jumpR('/wholesale');"><img src="/images/front/main_img12.png" /></a>
						</div>
					</div>
					<div class="tit_main">
						<a href="javascript:lh.jumpR('/wholesale');"><nobr>批发城</nobr></a>
					</div>
				</li>
			</ul>
			
			<br/>
			<a id="tests" onclick="lh.jumpR('/ap');" href="javascript:;" class="weui_btn weui_btn_primary">专场入口</a><br/>
			<a id="tests" onclick="lh.jumpR('/ap/page/manage');" href="javascript:;" class="weui_btn weui_btn_primary">专场管理</a><br/>
			<a id="tests" onclick="lh.jumpR('/goods/page/add');" href="javascript:;" class="weui_btn weui_btn_primary">发布藏品</a><br/>
			<a id="tests" onclick="lh.jumpR('/index');" href="javascript:;" class="weui_btn weui_btn_primary">快捷登陆</a>
			<!-- <a id="tests" onclick="lh.jumpR('/login');" href="javascript:;" class="weui_btn weui_btn_primary">快捷登陆</a> -->
			<br/>
		</div> <!-- 8个导航按钮 结束 -->
		
		<!-- 推荐拍场 开始 -->
		<!-- 
		<div class="c_0100_4"> 
			<div class="t_0100_2">
				<div class="img_chass">
					<a href="javascript:void(0);"><img src="/images/front/main_img13.jpg" width="100%" /></a>
				</div>
				<div class="tit_chass">
					<a href="/auctionQuick"><nobr>微拍堂在线即时拍 </nobr></a>
				</div>
			</div>
		</div>  --><!-- 推荐拍场 结束 -->
		
		<%-- //TODO tempHide
		<div class="c_0100_5"> <!-- 展会活动 开始 -->
			<div class="title_1">
				<div class="tit_1">展会活动</div>
				<div class="more_1">
					<a href="javascript:lh.jumpR('/activity');"><img src="/images/front/main_img15.png" width="24" height="24" /></a>
				</div>
			</div>
			<div class="t_0100_3">
				<ul>
					<c:forEach var="article" items="${articleList}">
						<li onclick="lh.jumpR('/activity/${article.id}');" class="pointer">
							<div class="l_155" style="max-height:138px;overflow: hidden;">
								<img src="${article.picPath}" width="100%" />
							</div>
							<div class="r_500">
								<div class="t_500_1">
									<nobr>${article.title}</nobr>
								</div>
								<div class="t_500_2">
									<nobr>日期：<fmt:formatDate value="${article.startDate}" pattern="yyyy-MM-dd"></fmt:formatDate>&nbsp;-&nbsp;<fmt:formatDate value="${article.endDate}" pattern="yyyy-MM-dd"></fmt:formatDate></nobr>
								</div>
								<div class="t_500_3">
									<nobr>发起方：${article.institution}</nobr>
								</div>
								<div class="t_500_3">
									<nobr>地址：${article.address}</nobr>
								</div>
							</div>
						</li>
					</c:forEach>
					
					<c:if test="${empty articleList}">
						<li class="pointer" style="width:100%;">
							<div style="height:40px;text-align: center;margin-top:15px;color: gray">
								暂无展会活动
							</div>
						</li>
					</c:if>
				</ul>
			</div>
		</div> --%> <!-- 展会活动 结束 -->
		
		<!-- 推荐店铺 开始 -->
		<%-- //TODO tempHide
		<div class="c_0100_6"> 
			<div class="title_1">
				<div class="tit_1">推荐店铺</div>
				<div class="more_1">
					<a href="lh.jumpR('/allShop');"><img src="/images/front/main_img15.png" width="24" height="24" /></a>
				</div>
			</div>
			<div class="t_0100_4">
				<ul>
					<c:forEach var="shop" items="${shopList}">
						<li>
							<div class="l_60 pointer" style="height:30px;width:30px;" onclick="lh.jumpR('/sale/${shop.userId}');">
								<img src="${shop.logo}"  />
							</div>
							<div class="r_110">
								<a href="javascript:lh.jumpR('/sale/${shop.userId}');"><nobr>${shop.name}</nobr></a>
							</div>
						</li>
					</c:forEach>
					
				</ul>
				<c:if test="${empty shopList}">
					<div class="pointer" style="width:100%;">
						<div style="width:100%;height:40px;text-align: center;margin-top:15px;color: gray">
							暂无推荐店铺
						</div>
					</div>
				</c:if>
			</div>
		</div> --%> <!-- 推荐店铺 结束 -->
		
		<%-- <div class="c_0100_6">//TODO tempHide <!-- 推荐论坛 开始 -->
			<div class="title_1">
				<div class="tit_1">推荐论坛</div>
				<div class="more_1">
					<a href="javascript:lh.jumpR('/forums');"><img src="/images/front/main_img15.png" width="24" height="24" /></a>
				</div>
			</div>
			<div class="t_0100_4">
				<ul>
					<c:forEach var="forum" items="${forumList}">
						<li>
							<div class="l_60 pointer" style="height:30px;width:30px;" onclick="lh.jumpR('/forumArticle/${forum.id}');">
								<img src="${forum.logo}" width="100%" />
							</div>
							<div class="r_110">
								<a href="javascript:lh.jumpR('/forumArticle/${forum.id}');"><nobr>${forum.name}</nobr></a>
							</div>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${empty forumList}">
					<div class="pointer" style="width:100%;">
						<div style="width:100%;height:40px;text-align: center;margin-top:15px;color: gray">
							暂无推荐论坛
						</div>
					</div>
				</c:if>
			</div>
		</div> --%> <!-- 推荐论坛 结束 -->
		
		<%-- //TODO tempHide
		<div class="c_0100_7"> <!-- 精品专场 开始 -->
			<div class="title_1">
				<div class="tit_1">精品专场</div>
				<div class="more_1">
					<a href="javascript:lh.jumpR('/inst');"><img src="/images/front/main_img15.png" width="24" height="24" /></a>
				</div>
			</div>
			<div class="t_0100_5">
				<ul>
					<c:forEach var="ap" items="${apList}">
						<li>
							<div class="img_jp" style="max-height:400px;overflow: hidden;">
								<div class="bd">
									<c:forEach var="picPath" items="${ap.apPicPaths}">
											<div class="div_gg fl">
						  						<a href="javascript:lh.jumpR('/auctions?instId=${ap.instId}');"><img src="${picPath}" style="max-height:90px;margin:5px;"/></a>
				  							</div>
									</c:forEach>
								</div>
								<div class="pf_tit">
									<a href="javascript:lh.jumpR('/auctions?instId=${ap.instId}');"><nobr>${ap.auctionName}</nobr></a>
								</div>
							</div>
							<div class="tit_jp">
								<div class="l_338" style="width:64%">
									<span><fmt:formatDate value="${ap.startTime}" pattern="MM"/></span>月
									<span><fmt:formatDate value="${ap.startTime}" pattern="dd"/></span>日&nbsp;
									<span><fmt:formatDate value="${ap.startTime}" pattern="hh:mm"/></span>&nbsp;开始
								</div>
								<div class="r_338" style="width:35%">
									<a href="javascript:void(0);">
										<img src="/images/front/main_img19.png" width="20" height="20" />
										<c:if test="${!empty ap.visitNum}">
											&nbsp;${ap.visitNum}次围观
										</c:if>
									</a>
								</div>
							</div>
							<!-- <div class="time_jp">
							</div> -->
						</li>
					</c:forEach>
					<c:if test="${empty apList}">
						<li class="pointer" style="width:100%;">
							<div style="width:100%;height:40px;text-align: center;margin-top:15px;color: gray">
								暂无精品专场
							</div>
						</li>
					</c:if>
				</ul>
			</div>
		</div> --%> <!-- 精品专场 结束 -->
		
		<div class="c_0100_8"> <!-- 推荐藏品 开始 -->
			<div class="title_2">
				<div class="tit_2">推荐藏品</div>
				<div class="more_2" onclick="lh.jumpR('/goods');">
					<a href="javascript:void(0);"><img src="/images/front/main_img15.png" width="24" height="24" /></a>
				</div>
			</div>
			<div class="t_0100_6">
				<ul id="collectGoods">
					<li id="noCollectGoods" class="pointer frontHide" style="width:100%;">
						<div style="width:100%;height:40px;text-align: center;margin-top:15px;color: gray">
							暂无推荐藏品
						</div>
					</li>
				</ul>
				<div id="resultTip" class="resultTip frontHide"></div>
				<div id="loadingTip" class="loadingTip  frontHide">正在加载，请稍候...</div>
			</div>
		</div>
	</div> <!-- 推荐藏品 结束 -->
	<!--  
	<a id="tests" onclick="tests();" href="javascript:;" class="weui_btn weui_btn_primary">按钮</a>
	<br/><br/><br/>
	-->
	<div class="pz_down">
		<div class="c_0100_9"></div>
	</div>
	
	<a id="gotop" onclick="goTop();" href="javascript:void(0);">︿</a>
	
	<%@ include file="/views/front/common/z_div_menu_bottom.htm"%><!-- 底部菜单 -->
	<script type="text/javascript" src="/third-party/jquery/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="/third-party/clipboard/clipboard.min.js"></script>
	<script type="text/javascript" src="/third-party/jquery-ajax-cache/locache.min.js"></script>
	<script type="text/javascript" src="/base/js/common/common_base.js" title="bv"></script>
	<script type="text/javascript" src="/base/js/common/common_front.js" title="bv"></script>
	<%--
	<script type="text/javascript" src="/third-party/lodash/lodash.min.js"></script>
	<script type="text/javascript" src="/third-party/json2/json2.js"></script>
	<script type="text/javascript" src="/third-party/jquery.event.drag/jquery.event.drag-2.2.js"></script>
	<script type="text/javascript" src="/third-party/jquery.event.drag/jquery.event.drag.live-2.2.js"></script>
	
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>
	<script type="text/javascript" src="/third-party/is/is.min.js"></script>
	<script type="text/javascript" src="/third-party/moment/moment.min.js"></script>
	<script type="text/javascript" src="/third-party/moment/locale/zh-cn.js"></script>
	
	<script type="text/javascript" src="/base/js/common/common_tools.js" title="bv"></script>
	
	<script type="text/javascript" src="/third-party/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript" src="/third-party/jquery-weui/js/jquery-weui.min.js"></script>
	<script type="text/javascript" src="/third-party/pagination/query.js"></script>
	<script type="text/javascript" src="/third-party/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="/base/js/common/common_front_ui_weui.js" title="bv"></script>
	<script type="text/javascript" src="/js/front/common.js" title="v"></script>
	
	<script type="text/javascript" src="/third-party/other/TouchSlide.1.1.js"></script>
	<script type="text/javascript" src="/third-party/other/jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="/third-party/mustache/mustache.min.js"></script>

	<script type="text/javascript" src="/js/front/wpk/index.js" title="v"></script>
	<script type="text/javascript" src="/js/front/wpk/iscroll.js" title="v"></script>
	<script> lh.param = ${paramJson}; </script>
	--%>
	
	<!-- <script type="text/javascript">
	$(function(){
		frontBaseLoadingOpen();
	});
	</script> -->
	<script id="template" type="x-tmpl-mustache">
	{{#rows}}
		<li>
			<div class="img_cp" style="max-height:200px;overflow:hidden;">
				<a href="javascript:lh.jumpR('/goods/{{goodsId}}?shopId={{shopId}}');"><img src="{{picPath}}" width="100%" /></a>
			</div>
			<div class="tit_cp">
				<a href="javascript:void(0);">{{goodsName}}</a>
			</div>
			<div class="price_cp">￥{{shopPrice}}{{^shopPrice}}议价{{/shopPrice}}</div>
		</li>
	{{/rows}}	 		 
	</script>
</body>
</html>
