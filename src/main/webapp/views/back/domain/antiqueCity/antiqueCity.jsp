<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="STYLESHEET" type="text/css" href="/css/back/back.css" title="v"/>
</head>
<body>
    <!-- 查询条件  开始 -->
   <table>
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>商圈名称:</span><input id="name" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>介绍:</span><input id="introduce" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>地址:</span><input id="address" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>类型:</span><input id="type" class="easyui-combobox width120" /></td>
				<td class="td_pad"><span>状态:</span><input id="mainStatus" class="easyui-combobox width120" /></td>
				<td class="td_pad"><button id="searchLoanBusiness" onclick="doSearch();return false;" class="button button-primary button-rounded button-small">查 询</button></td>
				<td class="td_pad"><button id="clearsSearchLoanBusiness" onclick="clearSearch();return false;" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
		</tbody>
	</table>
	<div class="clear-both height10"></div>
	<!-- 查询条件 结束 -->
	<div class="fl_opt_div">
		<button id="batchDelete" onclick="batchDelete();return false;"  class="button button-primary button-rounded button-small" >批量删除</button>
		<button id="addAntiqueCity" onclick="addAntiqueCity();return false;" class="button button-primary button-rounded button-small">添加</button>
		<button id="batchRecover"  onclick="batchRecover();return false;" class="hide button button-primary button-rounded button-small">批量恢复</button>
		<button id="batchThoroughDelete" onclick="batchThoroughDelete();return false;" class="hide button button-primary button-rounded button-small">批量彻底删除</button>
		<button id="userInfoLink" onclick="jumpToUserInfo()" class="button button-royal button-rounded button-small">用户信息</button>
	</div>
	<div class="fr_opt_div">
		<button id="showTrash" onclick="showTrash();return false;" class="button button-primary button-rounded button-small">回收站</button>
		<button id="returnBack" onclick="returnBack();return false;" class="hide button button-primary button-rounded button-small">返回</button>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->
    <div id="antiqueCityDetailWindiv" style="display:none;">
	     <div id='antiqueCityDetailWin' class="easyui-window" title="商圈" style="width: 700px;height:500px" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
	         <form id="antiqueCityDetailForm"><br/>
	       		 <table id="antiqueCityDetailTable" class="padL5">
					<tbody>
						<tr>
							<td class="td_pad"> <input type="hidden" id="f_antiqueCityId"></td>
						</tr>	
						<tr class="tr_ht">
							<td class="td_pad"><span>商圈名称：</span><input id="f_name" data-options="readonly:true,required:true" class="easyui-textbox width140"/></td>
						</tr>
						<tr class="tr_ht">
							<td class="td_pad"><span>状态：&nbsp;&nbsp;</span><input id="f_mainStatus" data-options="readonly:true,required:true" class="easyui-combobox width140"/></td>
						</tr>
						<tr class="tr_ht">
							<td ><span>商圈地址：</span><input id="f_address" data-options="readonly:true,required:true" class="easyui-textbox width380"/></td>
						</tr>
						<!-- <tr class="tr_ht" id="file">
						</tr> -->
						<tr class="tr_ht" >
							<td class="td_pad" colspan="3"><span>商圈介绍:</span>
								<input id="f_introduce" data-options="readonly:true,multiline:true,height:130,width:600,prompt:'请填写商圈介绍',required:true" class="easyui-textbox "/>
							</td>
						</tr>
					</tbody>
				 </table>
			 </form>  
			 <div>
				<img src="${ap.picPaths}" class="picurl">
				<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
    			<input type="hidden" name="fileDBIds" id="fileDBIds"/>
				<!-- 上传文件进度展示 开始 -->
				<!-- id="filelist" -->
				<div id="upload_outer_div" >
				</div>
				<!-- 上传文件进度展示 结束 -->
				<div style="min-height:25px;margin-top:5px;">
					<a id="browse"  class="button button-primary button-rounded button-small" value="上传商圈logo">
						<span>上传商圈logo</span>
					</a>
				</div> 
			</div>						
			 <div class="inline-center mgV40">
			     <button id="antiqueCityDetailSave" onclick="submitAntiqueCityDetail();return false;"  class="button button-primary button-rounded button-small" >保存</button>
			     <button id="antiqueCityDetailBack" onclick="closeAntiqueCityDetailWin();return false;"  class="button button-primary button-rounded button-small" >返回</button>
			 </div>
	     </div>
    </div>
<%@ include file="/views/common/common_back_js.htm"%>
<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
<script type="text/javascript" src="/js/back/antiqueCity/antiqueCity.js" title="v"></script>
</body>
</html>