<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="STYLESHEET" type="text/css" href="/css/back/back.css" title="v"/>
</head>
<body>
    <!-- 查询条件  开始 -->
	<table>
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>拍卖场次：</span><input id="sc_auctionSerial" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>机构名称：</span><input id="sc_instName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>状态：</span><input id="sc_mainStatus" data-options="editable:false" class="easyui-combobox width120" /></td>
				<td class="td_pad"><span>拍卖时间-从：</span><input id="sc_startTimeFrom" data-options="editable:false" class="easyui-datebox width120" /></td>
				<td class="td_pad"><span>至：</span><input id="sc_startTimeTo" data-options="editable:false" class="easyui-datebox width120" /></td>
				<td class="td_pad"><button id="search" onclick="doSearch()" class="button button-primary button-rounded button-small">查 询</button></td>
				<td class="td_pad"><button id="clearsSearch" onclick="clearSearch()" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
			<!-- <tr class="tr_ht" align="right"> </tr> -->
		</tbody>
	</table>
	<!-- 查询条件 结束 -->
	
	<div class="clear-both height10"></div>
	<div class="fl_opt_div">
		<button id="batchDelete" onclick="batchDelete()"  class="button button-primary button-rounded button-small" >批量删除</button>
		<button id="addAuctionQuick" onclick="addAuctionQuick()" class="button button-primary button-rounded button-small">添加场次</button>
		<button id="userInfoLink" onclick="jumpToUserInfo()" class="button button-royal button-rounded button-small">用户信息</button>
		<button id="auctionQuickInstLink" onclick="jumpToAuctionQuickInst()" class="button button-royal button-rounded button-small">即时拍机构</button>
		<button id="goodsLink" onclick="jumpToGoods()" class="button button-royal button-rounded button-small">藏品</button>
		
		<button id="batchRecover" onclick="batchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
		<button id="batchThoroughDelete" onclick="batchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>
		
	</div>
	<div class="fr_opt_div">
		<button id="showTrash" onclick="showTrach()" class="button button-primary button-rounded button-small">回收站</button>
		<button id="returnBack" onclick="returnBack()" class="hide button button-primary button-rounded button-small">返回</button>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->
    
     <div id="auctionQuickWindiv" style="display:none;">
	     <div id='auctionQuickWin' class="easyui-window" title="即时拍" style="width: 750px;height:460px" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
	         <form id="auctionQuickForm"><br/>
	       		 <table id="auctionQuickTable" class="padL5">
					<tbody>
						<tr id="winSearchTr" class="tr_ht" align="left">
							<td class="td_pad"><span>机构查询：</span><input id="f_serialSearch" class="easyui-textbox width140"/></td>
							<td class="td_pad" colspan="2">
								<input type="button" onclick="searchInst();" class="button button-primary button-rounded button-small" value="查询"/>
								<input type="button" style="width: 105px;" onclick="jumpToAuctionQuickInst()" class="button button-royal button-rounded button-small" value="即时拍机构"/>
								<span class="colorGray"> 请输入机构编号（可从拍卖机构表格中复制）</span>
							</td>
						</tr>
						<tr id="winSearchDivisionTr" class="tr_ht" align="left"></tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad">
								<span class="colorGray">机构编号：</span>
								<input id="f_serial" data-options="readonly:true" class="easyui-textbox width140"/>
								<input id="f_userId" type="hidden"/>
								<input id="f_instId" type="hidden"/>
							</td>
							<td class="td_pad"><span class="colorGray">机构名称：</span><input id="f_instName" data-options="readonly:true" class="easyui-textbox width140"/></td>
							<td class="td_pad"><span class="colorGray">负责人：</span><input id="f_username" data-options="readonly:true" class="easyui-textbox width140"/></td>
						</tr>
						<!-- <tr class="tr_ht" align="right">
							<td class="td_pad"><span>拍卖场次：</span><input id="f_auctionSerial" class="easyui-textbox width140" /></td> 
							<td class="td_pad"><span>保证金(元)：</span><input id="f_bail" class="easyui-numberbox width140"/></td>
							<td class="td_pad"><span>推广红包(元)：</span><input id="f_spreadPackets" class="easyui-numberbox width140"/></td> 
						</tr> -->
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>开始时间：</span><input id="f_startTime" data-options="editable:false,required:true,onChange:function(){refreshStatus()}" class="easyui-datetimebox width140"/></td>
							<td class="td_pad"><span>参拍人数：</span><input id="f_joinNum" class="easyui-numberbox width140"/></td>
							<td class="td_pad"><span>围观次数：</span><input id="f_visitNum" class="easyui-numberbox width140"/></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad">
								<span>结束时间：</span>
								<input id="f_endTime" data-options="editable:false,onChange:function(){refreshStatus()}" class="easyui-datetimebox width140" />
							</td>
							<td align="left" class="td_pad">
								<input type="button" onclick="$('#f_endTime').datetimebox('clear');" class="button button-rounded button-tiny" value="清除"/>
								<span class="colorGray">一般不用手动设置结束时间</span>
							</td>
							<td class="td_pad"><span class="colorGray">状态：</span><input id="f_mainStatus" data-options="editable:false,readonly:true" class="easyui-combobox width140"/></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>拍卖场次：</span><input id="f_auctionSerial" class="easyui-textbox width140" /></td>
							<td class="td_pad"><span>即时拍名称：</span><input id="f_auctionName" class="easyui-textbox width140" /></td>
						</tr>
					</tbody>
				 </table>
			 </form>  
			 <div class="ipt_item_div" >
				<label>上传专场封面：</label>
				<input type="hidden" id="instLogo" value="${ap.picPaths}">
				<img src="${ap.picPaths}" class="picurl">
				<input type="hidden" name="filePaths" id="filePaths" value="${ap.picPaths}"/>
			    <input type="hidden" name="fileDBIds" id="fileDBIds"/>
				<button id="browse" type="button" class="button button-primary button-rounded button-small">上传专场封面</button>
			</div>
			<!-- 上传文件进度展示 开始   -->
			<div id="upload_outer_div" class="fieldset"></div>
			 <div class="inline-center mgV40">
			     <button id="auctionQuickSave" onclick="submitAuctionQuick()"  class="button button-primary button-rounded button-small" >保存</button>
			     <button id="auctionQuickBack" onclick="closeAuctionQuickWin()"  class="button button-primary button-rounded button-small" >返回</button>
			 </div>
	     </div>
    </div>
    
<%@ include file="/views/common/common_back_js.htm"%>
<script type="text/javascript" src="/third-party/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="/js/common/common_upload.js" title="v"></script>
<script type="text/javascript" src="/js/back/auction/auctionQuick.js" title="v"></script>
</body>
</html>