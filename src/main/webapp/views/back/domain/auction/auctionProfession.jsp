<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/back/back.css" title="v" />
</head>
<body>
	<!-- 查询条件  开始 -->
	<table id="mainQueryTable">
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>拍卖场次：</span><input role="textbox" id="sc_auctionSerial" class="domain-input easyui-textbox width120" /></td>
				<td class="td_pad"><span>机构名称：</span><input role="textbox" id="sc_instName" class="domain-input easyui-textbox width120" /></td>
				<td class="td_pad"><span>状态：</span><input role="combobox" id="sc_mainStatus" data-options="editable:false" class="domain-input easyui-combobox width120" /></td>
				<td class="td_pad"><span>拍卖时间-从：</span><input role="datebox" id="sc_startTimeFrom" data-options="editable:false" class="domain-input easyui-datebox width120" /></td>
				<td class="td_pad"><span>至：</span><input id="sc_endTimeTo" role="datebox" data-options="editable:false" class="domain-input easyui-datebox width120" /></td>
				<td class="td_pad"><button id="search" onclick="doSearch();return false;" class="button button-primary button-rounded button-small">查 询</button></td>
				<td class="td_pad"><button id="clearsSearch" onclick="clearSearch();return false;" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
			<!-- <tr class="tr_ht" align="right"> </tr> -->
		</tbody>
	</table>
	<!-- 查询条件 结束 -->

	<div class="clear-both height10"></div>
	<div id="opt_outer_div">
		<div class="fl_opt_div">

			<button role="opt_1" id="btn_batchDelete" onclick="lh.commonBatchDelete()" class="button button-primary button-rounded button-small">批量删除</button>
			<button role="opt_1" onclick="addMainObj()" class="button button-primary button-rounded button-small">添加场次</button>
			
			<button id="userInfoLink" onclick="jumpToUserInfo()" class="button button-royal button-rounded button-small">用户信息</button>
			<button id="auctionInstLink" onclick="jumpToAuctionInst()" class="button button-royal button-rounded button-small">拍卖机构</button>
			<!-- <button id="goodsLink" onclick="jumpToGoods()" class="button button-royal button-rounded button-small">藏品</button> -->

			<button role="opt_2" id="btn_batchRecover" onclick="lh.commonBatchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
			<button role="opt_2" id="btn_throughDelete" onclick="lh.commonBatchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>

		</div>

		<div class="fr_opt_div">
			<button role="opt_1" id="btn_trash" onclick="lh.commonShowTrash()" class="button button-primary button-rounded button-small">回收站</button>
			<button role="opt_2" id="btn_trashBack" onclick="lh.commonReturnBack()" class="hide button button-primary button-rounded button-small">返回</button>
		</div>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->

	<div id="mainObjWindiv" style="display:none;">
	     <div id='mainObjWin' class="easyui-window" title="专场" style="width: 720px;" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
			<div id="mainObjTip">数据字典对系统的正常运行至关重要，请谨慎操作！</div>
	         <form id="mainObjForm"><br/>
	       		 <table id="mainObjTable" class="padL5">
					<tbody>
						<tr id="winSearchTr" class="tr_ht" align="left">
							<td class="td_pad"><span>机构查询：</span><input role="textbox" id="f_serialSearch" class="domain-input easyui-textbox width140" /></td>
							<td class="td_pad" colspan="2"><input type="button" onclick="searchInst();" class="button button-primary button-rounded button-small" value="查询" /> <input type="button" onclick="jumpToAuctionInst()"
								class="button button-royal button-rounded button-small" value="拍卖机构" /> <span class="colorGray"> 请输入机构编号（可从拍卖机构表格中复制）</span></td>
						</tr>
						<tr id="winSearchDivisionTr" class="tr_ht" align="left"></tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span class="colorGray">机构编号：</span> <input id="f_serial" role="textbox" data-options="readonly:true" class="domain-input easyui-textbox width140" />
							 <input  id="f_userId" " type="hidden" />
							 <input id="f_instId"  " type="hidden" />
							<td class="td_pad"><span class="colorGray">机构名称：</span><input id="f_instName" role="textbox" data-options="readonly:true" class="domain-input easyui-textbox width140" /></td>
							<td class="td_pad"><span class="colorGray">负责人：</span><input id="f_username" role="textbox" data-options="readonly:true" class="domain-input easyui-textbox width140" /></td>
						</tr> 
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>拍卖场次：</span><input role="textbox" id="f_auctionSerial" class="domain-input  easyui-textbox width140" /></td>
							<td class="td_pad"><span>保证金(元)：</span><input role="numberbox" id="f_bail" class="domain-input easyui-numberbox width140" min="0.01" data-options="prompt:'请输入正确的金额'"/></td>
							<td class="td_pad"><span>推广红包(元)：</span><input role="numberbox" id="f_spreadPackets" data-options="required:true,prompt:'请输入正确的金额'" class="domain-input easyui-numberbox width140" min="0.01" /></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>开始时间：</span><input role="datetimebox"  id="f_startTime" data-options="editable:false,required:true" class="domain-input easyui-datetimebox width140" /></td>
							<td class="td_pad"><span>参拍人数：</span><input role="textbox"  id="f_joinNum" class="domain-input easyui-textbox width140" /></td>
							<td class="td_pad"><span>围观次数：</span><input role="textbox" id="f_visitNum" class="domain-input easyui-textbox width140" /></td>
						</tr>
						 <tr class="tr_ht" align="right">
							<td class="td_pad"><span>结束时间：</span> <input role="datetimebox"  id="f_endTime" data-options="editable:false" class="domain-input easyui-datetimebox width140" /></td>
							<td align="left" class="td_pad"><input type="button" onclick="$('#f_endTime').datetimebox('clear');" class="button button-rounded button-tiny" value="清除" /> <span class="colorGray">一般不用手动设置结束时间</span></td>
							<td class="td_pad"><span class="colorGray">状态：</span><input role="combobox" id="f_mainStatus" data-options="editable:false,readonly:true" class="domain-input easyui-combobox width140" /></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>专场名称：</span><input role="textbox" id="f_auctionName" class="domain-input easyui-textbox width140" /></td>
						</tr> 
					</tbody>
				</table>
			</form>
			<span>头&nbsp;&nbsp;像：</span>
			<button id="browse" type="button" class="button button-primary button-rounded button-small">选择图片</button>
			<span>(建议图片长宽均为120像素)</span>
			<div style="display: inline-block; float: left;">
				<img id="pic" class="picurl" src="${pic.picPath}" style="height: 60px; max-width: 100px; overflow: hidden; padding: 5px;" />
				 <input type="hidden" name="filePaths" id="filePaths" value="${filePath}" />
				  <input type="hidden" name="fileDBIds" id="fileDBIds" />
			</div>
			<div id="upload_outer_div" style="margin-top: 30px;">
				<!-- 上传文件进度展示 -->
			</div>
			<div class="inline-center mgV40">
					<button id="mainObjSave" onclick="saveMainObj()" class="button button-primary button-rounded button-small">保存</button>
				<button id="mainObjBack" onclick="closeMainObjWin()" class="button button-primary button-rounded button-small">返回</button>
			</div>
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_back_js.htm"%>
	<%@ include file="/views/common/common_upload_js.htm"%>
	<script type="text/javascript">
		lh.paramJsonStr = '${paramJson}';
	</script>
	<script type="text/javascript" src="/js/common/back_template.js" title="v"></script>
	<script type="text/javascript" src="/js/back/domain/auction/auctionProfession.js" title="v"></script>
</body>
</html>