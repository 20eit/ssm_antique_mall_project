<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="stylesheet" type="text/css" href="/css/back/back.css" title="v" />
</head>
<body>
	<!-- 查询条件  开始 -->
	<table id="mainQueryTable">
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>拍品名称：</span><input role="textbox" id="sc_goodsName" class="domain-input easyui-textbox width120" /></td>
				<td class="td_pad"><span>用户名：</span><input role="textbox" id="sc_username" class="domain-input easyui-textbox width120" /></td>
				<td class="td_pad"><span>出价金额：</span><input role="textbox" id="sc_offerPrice" class="domain-input easyui-numberbox width120" /></td>
				<td class="td_pad"><span>出价时间：</span><input role="datebox" id="sc_offerAt" data-options="editable:false" class="domain-input easyui-datebox width120" /></td>
				<td class="td_pad"><button id="searchClear" onclick="clearSearch();return false;" class="button button-primary button-rounded button-small">重 置</button></td>
				<td class="td_pad"><button id="searchYes" onclick="doSearch();return false;" class="button button-primary button-rounded button-small">查 询</button></td>
			</tr>
		</tbody>
	</table>
	<!-- 查询条件 结束 -->

	<div class="clear-both height10"></div>
	<div id="opt_outer_div">
		<div class="fl_opt_div">
			<button role="opt_1" id="btn_batchDelete" onclick="lh.commonBatchDelete()" class="button button-primary button-rounded button-small">批量删除</button>
			<button role="opt_1" onclick="addMainObj()" class="button button-primary button-rounded button-small">添加微拍</button>
			<button id="userInfoLink" onclick="jumpToUserInfo()" class="button button-royal button-rounded button-small">用户信息</button>
			<!-- <button id="shopLink" onclick="jumpToShop()" class="button button-royal button-rounded button-small">拍品信息</button> -->
			<button role="opt_2" id="btn_batchRecover" onclick="lh.commonBatchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
			<button role="opt_2" id="btn_throughDelete" onclick="lh.commonBatchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>
		</div>
		<div class="fr_opt_div">
			<button role="opt_1" id="btn_trash" onclick="lh.commonShowTrash()" class="button button-primary button-rounded button-small">回收站</button>
			<button role="opt_2" id="btn_trashBack" onclick="lh.commonReturnBack()" class="hide button button-primary button-rounded button-small">返回</button>
		</div>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->

	<div id="mainObjWindiv" style="display: none;">
		<div id='mainObjWin' class="easyui-window" title="微拍信息" style="width: 710px;" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
			<div id="mainObjTip"></div>
			<form id="mainObjForm">
				<br />
				<table id="mainObjTable" class="padL5">
					<tbody>
						<tr id="winSearchTr" class="tr_ht" align="left">
							<td class="td_pad"><span>用户查询：</span><input id="f_serialSearch" class="easyui-textbox width140" /></td>
							<td class="td_pad" colspan="2"><input type="button" onclick="searchUser();" class="button button-primary button-rounded button-small" value="查询" /> <input type="button" onclick="jumpToUserInfo()"
								class="button button-royal button-rounded button-small" value="用户信息" /> <span class="colorGray"> 请输入用户编号（可从用户信息表格中复制）</span></td>
						</tr>
						<tr id="winSearchDivisionTr" class="tr_ht" align="left"></tr>
						  <tr class="tr_ht" align="right">
							<td class="td_pad"><span class="colorGray">用户编号：</span> <input id="f_serial" role="textbox" data-options="readonly:true" class=" domain-input easyui-textbox  width140" /> 
							<input  id="f_userId"  type="hidden" />
							<td class="td_pad"><span class="colorGray">用户名：</span><input id="f_username" role="textbox" data-options="readonly:true" class="domain-input easyui-textbox width140" /></td>
							<td class="td_pad"><span class="colorGray">真实姓名：</span><input id="f_realName" role="textbox" data-options="readonly:true" class="domain-input easyui-textbox width140" /></td>
						</tr>
						<tr class="tr_ht" align="right">
						<td class="td_pad"><span class="colorGray">商品名称：</span><input role="combobox" id="f_goodsId" class="domain-input easyui-combobox width140" /></td>
						<td class="td_pad"><span class="colorGray">出价金额：</span><input role="textbox" id="f_offerPrice" class="easyui-textbox width140" /></td>	
						<td class="td_pad"><span>出价时间：</span> <input role="datetimebox"  id="f_offerAt" data-options="editable:false" class="domain-input easyui-datetimebox width140" /></td>
						</tr>
					</tbody>
				</table>
			</form>
			<div class="inline-center mgV40">
				<button id="mainObjSave" onclick="saveMainObj()" class="button button-primary button-rounded button-small">保存</button>
				<button id="mainObjBack" onclick="closeMainObjWin()" class="button button-primary button-rounded button-small">返回</button>
			</div>
		</div>
	</div>

	<%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_back_js.htm"%>
	<script type="text/javascript">
		lh.paramJsonStr = '${paramJson}';
	</script>
<script type="text/javascript" src="/js/common/back_template.js" title="v"></script>
	<script type="text/javascript" src="/js/back/domain/auction/auctionMicroOffers.js" title="v"></script>
</body>
</html>