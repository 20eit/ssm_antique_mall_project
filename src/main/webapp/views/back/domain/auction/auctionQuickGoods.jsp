<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="STYLESHEET" type="text/css" href="/css/back/back.css" title="v"/>
</head>
<body>
    <!-- 查询条件  开始 -->
	<table>
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span class="colorGray">用户：</span><input id="sc_user"  class="easyui-combobox width140"/></td>
				<td class="td_pad"><span class="colorGray">机构名称：</span><input id="sc_auctionInst"  class="easyui-textbox width140"/></td>
				<td class="td_pad"><span class="colorGray">商品名称：</span><input id="sc_goods" class="easyui-textbox width140"/></td>
				<td class="td_pad"><button id="search" onclick="doSearch()" class="button button-primary button-rounded button-small">查 询</button></td>
				<td class="td_pad"><button id="clearsSearch" onclick="clearSearch()" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
			<!-- <tr class="tr_ht" align="right"> </tr> -->
		</tbody>
	</table>
	<!-- 查询条件 结束 -->
	
	<div class="clear-both height10"></div>
	<div class="fl_opt_div">
		<button id="batchDelete" onclick="batchDelete()"  class="button button-primary button-rounded button-small" >批量删除</button>
		<button id="addAuctionQuickGoods" onclick="addAuctionQuickGoods()" class="button button-primary button-rounded button-small">添加商品</button>
		<button id="batchRecover" onclick="batchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
		<button id="batchThoroughDelete" onclick="batchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>
		
	</div>
	<div class="fr_opt_div">
		<button id="showTrash" onclick="showTrach()" class="button button-primary button-rounded button-small">回收站</button>
		<button id="returnBack" onclick="returnBack()" class="hide button button-primary button-rounded button-small">返回</button>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->
    
     <div id="auctionQuickGoodsWindiv" style="display:none;">
	     <div id='auctionQuickGoodsWin' class="easyui-window" title="添加商品" style="width: 300px;height:300px" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
	         <form id="auctionQuickGoodsForm"><br/>
	       		 <table id="auctionQuickGoodsTable" class="padL5">
					<tbody>
						<tr><td><input type="hidden" id="f_auctionQuickGoodsId"></td></tr>
						<tr class="tr_ht">
							<td><td class="td_pad"><span class="colorGray">用户：</span><input id="f_userId" data-options="readonly:true" class="easyui-combobox width140"/></td></td>
						</tr>
						<tr class="tr_ht">
							<td><td class="td_pad"><span class="colorGray">机构名称：</span><input id="f_auctionInstId" data-options="readonly:true" class="easyui-combobox width140"/></td></td>
						</tr>
						<tr class="tr_ht">
							<td><td class="td_pad"><span class="colorGray">商品名称：</span><input id="f_goodsId" data-options="readonly:true" class="easyui-combobox width140"/></td></td>
						</tr>
						<tr class="tr_ht">
							<td><td class="td_pad"><span class="colorGray">开始价格：</span><input id="f_priceBegin" data-options="readonly:true" class="easyui-textbox width140"/></td></td>
						</tr>
					</tbody>
				 </table>
			 </form>  
			 <div class="inline-center mgV40">
			     <button id="auctionQuickGoodsSave" onclick="submitAuctionQuickGoods()"  class="button button-primary button-rounded button-small" >保存</button>
			     <button id="auctionQuickGoodsBack" onclick="closeAuctionQuickGoodsWin()"  class="button button-primary button-rounded button-small" >返回</button>
			 </div>
	     </div>
    </div>
    
<%@ include file="/views/common/common_back_js.htm"%>
<script type="text/javascript" src="/js/common/back_template.js" title="v"></script>
<script type="text/javascript" src="/js/back/auction/auctionQuickGoods.js" title="v"></script>
</body>
</html>