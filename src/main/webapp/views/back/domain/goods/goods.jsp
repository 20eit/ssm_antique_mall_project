<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="STYLESHEET" type="text/css" href="/css/back/back.css" title="v"/>
</head>
<body>
    <!-- 查询条件  开始 -->
	<table>
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>店铺名称：</span><input id="sc_shopName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>店铺编号：</span><input id="sc_shopSerial" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>用户名：</span><input id="sc_username" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>用户编号：</span><input id="sc_userSerial" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>真实姓名：</span><input id="sc_realName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><button id="search" onclick="doSearch()" class="button button-primary button-rounded button-small">查 询</button></td>
			</tr>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>商品状态：</span><input id="sc_mainStatus" class="easyui-combobox width120" /></td>
				<td class="td_pad"><span>商品名称：</span><input id="sc_goodsName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>商品编号：</span><input id="sc_goodsSn" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>价格-从：</span><input id="sc_priceFrom" class="easyui-numberbox width120" /></td>
				<td class="td_pad"><span>至：</span><input id="sc_priceTo" class="easyui-numberbox width120" /></td>
				<td class="td_pad"><button id="clearsSearch" onclick="clearSearch()" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
		</tbody>
	</table>
	<!-- 查询条件 结束 -->
	
	<div class="clear-both height10"></div>
	<div class="fl_opt_div">
		<button id="batchDelete" onclick="batchDelete()"  class="button button-primary button-rounded button-small" >批量删除</button>
		<button id="addGoods" onclick="addGoods()" class="button button-primary button-rounded button-small">添加商品</button>
		<button id="userInfoLink" onclick="jumpToUserInfo()" class="button button-royal button-rounded button-small">用户信息</button>
		<button id="shopLink" onclick="jumpToShop()" class="button button-royal button-rounded button-small">店铺信息</button>
		
		<button id="batchRecover" onclick="batchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
		<button id="batchThoroughDelete" onclick="batchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>
		
	</div>
	<div class="fr_opt_div">
		<button id="showTrash" onclick="showTrach()" class="button button-primary button-rounded button-small">回收站</button>
		<button id="returnBack" onclick="returnBack()" class="hide button button-primary button-rounded button-small">返回</button>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->
    
     <div id="goodsWindiv" style="display:none;">
	     <div id='goodsWin' class="easyui-window" title="商品" style="width: 800px;height:500px" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
	         <form id="goodsForm"><br/>
	       		 <table id="goodsTable" class="padL5">
					<tbody>
						<tr id="winSearchTr" class="tr_ht" align="left">
							<td class="td_pad"><span>店铺查询：</span><input id="f_serialSearch" class="easyui-textbox width140"/></td>
							<td class="td_pad" colspan="2">
								<input type="button" onclick="searchShop();" class="button button-primary button-rounded button-small" value="查询"/>
								<input type="button" onclick="jumpToShop()" class="button button-royal button-rounded button-small" value="店铺信息"/>
								<span class="colorGray"> 请输入店铺编号（可从店铺表格中复制）</span>
							</td>
						</tr>
						<tr id="winSearchDivisionTr" class="tr_ht" align="left"></tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad">
								<span class="colorGray">店铺编号：</span>
								<input id="f_serial" data-options="readonly:true" class="easyui-textbox width140"/>
								<input id="f_userId" type="hidden"/>
								<input id="f_shopId" type="hidden"/>
							</td>
							<td class="td_pad"><span class="colorGray">店铺名：</span><input id="f_shopName" data-options="readonly:true" class="easyui-textbox width140" /></td>
							<td class="td_pad"><span class="colorGray">用户名：</span><input id="f_userName" data-options="readonly:true" class="easyui-textbox width140"/></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>商品名称：</span><input id="f_goodsName" data-options="required:true" class="easyui-textbox width140"/></td>
							<td class="td_pad"><span>商品编号：</span><input id="f_goodsSn" data-options="prompt:'可以不填'" class="easyui-textbox width140" /></td>
							<td class="td_pad"><span>商品价格：</span><input id="f_price" data-options="prompt:'不填为议价'" class="easyui-textbox width140"/></td>
						</tr>
						<tr class="tr_ht" align="right">
							<td class="td_pad"><span>商品状态：</span><input id="f_mainStatus" data-options="required:true" class="easyui-textbox width140"/></td>
							<td class="td_pad"><span>商品类型：</span><input id="f_catId" data-options="required:true" class="easyui-textbox width140"/></td>
							<!-- <td class="td_pad"><span>商品图片：</span>
							<button onclick="catGoods()"  class="button button-primary button-rounded button-small width140" >查看图片</button>
							<input id="f_goodsImg" class="easyui-textbox width140"/></td> -->
							<!-- <td class="td_pad"><span>相册图片：</span><input id="f_goodsThumb" class="easyui-textbox width140"/></td> -->
						</tr>
						<tr id="winSearchDivisionTr" class="tr_ht" align="left"></tr>
						<tr class="tr_ht" align="right">
							<td ><span>商品描述：</span><input id="f_goodsDescription" data-options="required:true,multiline:true,prompt:'商品介绍',width:200,height:100," class="easyui-textbox width140"/></td>
						</tr>
					</tbody>
				 </table>
			 </form>  
			 <div class="inline-center mgV40">
			     <button id="goodsSave" onclick="submitGoods()"  class="button button-primary button-rounded button-small" >保存</button>
			     <button id="goodsBack" onclick="closeGoodsWin()"  class="button button-primary button-rounded button-small" >返回</button>
			 </div>
	     </div>
    </div>
    
<%@ include file="/views/common/common_back_js.htm"%>
<script type="text/javascript" src="/js/back/goods/goods.js" title="v"></script>
</body>
</html>