<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/views/common/meta_info.htm"%>
<%@ include file="/views/common/common_back_css.htm"%>
<link rel="STYLESHEET" type="text/css" href="/css/back/back.css" title="v"/>
<!-- <link rel="STYLESHEET" type="text/css" href="/css/back/upload.css" title="v"/> -->
</head>
<body>
    <!-- 查询条件  开始 -->
	<table>
		<tbody>
			<tr class="tr_ht" align="right">
				<td class="td_pad"><span>图片类型：</span><input id="sc_typeId" class="easyui-combobox width120" /></td>
				<td class="td_pad"><span>图片编号：</span><input id="sc_serial" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>图片名称：</span><input id="sc_title" class="easyui-textbox width120" /></td>
				<!-- <td class="td_pad"><span>相册名称：</span><input id="sc_albumName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>相册编号：</span><input id="sc_albumSerial" class="easyui-textbox width120" /></td> -->
				<td class="td_pad"><button id="search" onclick="doSearch();" class="button button-primary button-rounded button-small">查 询</button></td>
			</tr>
			<tr class="tr_ht" align="right">
				<!-- <td class="td_pad"><span>商品名称：</span><input id="sc_goodsName" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>店铺名称：</span><input id="sc_shopName" class="easyui-textbox width120" /></td> 
				<td class="td_pad"><span>用户名：</span><input id="sc_username" class="easyui-textbox width120" /></td>-->
				<td class="td_pad"><span>用户编号：</span><input id="sc_userSerial" class="easyui-textbox width120" /></td>
				<!-- <td class="td_pad"><span>商品编号：</span><input id="sc_goodsSn" class="easyui-textbox width120" /></td>
				<td class="td_pad"><span>店铺编号：</span><input id="sc_shopSerial" class="easyui-textbox width120" /></td> -->
				<td class="td_pad"><span>上传时间-从：</span><input id="sc_startTimeFrom" data-options="editable:false" class="easyui-datebox width120" /></td>
				<td class="td_pad"><span>至：</span><input id="sc_startTimeTo" data-options="editable:false" class="easyui-datebox width120" /></td>
				<td class="td_pad"><button id="clearsSearch" onclick="clearSearch()" class="button button-primary button-rounded button-small">重 置</button></td>
			</tr>
		</tbody>
	</table>
	<!-- 查询条件 结束 -->
	
	<div id="opt_outer_div">
		<div class="fl_opt_div">
			<button role="opt_1" id="btn_batchDelete" onclick="lh.commonBatchDelete()"  class="button button-primary button-rounded button-small">批量删除</button>
			 <!--<button id="addPicture" onclick="addPicture()" class="button button-primary button-rounded button-small">添加图片</button> -->
	          <button role="opt_1" onclick="addMainObj()" class="button button-primary button-rounded button-small">添加</button>
			<button role="opt_2" id="btn_batchRecover" onclick="lh.commonBatchRecover()" class="hide button button-primary button-rounded button-small">批量恢复</button>
			<button role="opt_2" id="btn_throughDelete" onclick="lh.commonBatchThoroughDelete()" class="hide button button-primary button-rounded button-small">彻底删除</button>
			
		</div>
		<div class="fr_opt_div">
			<button role="opt_1" id="btn_trash" onclick="lh.commonShowTrash()" class="button button-primary button-rounded button-small">回收站</button>
			<button role="opt_2" id="btn_trashBack" onclick="lh.commonReturnBack()" class="hide button button-primary button-rounded button-small">返回</button>
		</div>
	</div>
	<!-- 表格  开始 -->
	<div id='datagrid_div'>
		<table id="datagrid"></table>
	</div>
	<!-- 表格  结束 -->
   	<div id="mainObjWindiv" style="display: none;">
		<div id='mainObjWin' class="easyui-window" title="论坛信息" style="width: 740px;" data-options="modal:true,closed:true,maximizable:false,collapsible:false,minimizable:false">
			<div id="mainObjTip"></div>
			<form id="mainObjForm">
				<br />
				<table id="mainObjTable" class="padL5">
					<tbody>
						<tr class="tr_ht" align="right">
						<td class="td_pad"><span><span style="color: red; font-weight: bolder;">*</span>图片描述：</span><input role="textbox" id="f_title" class="domain-input easyui-textbox width140" data-options="required:true" /></td>
						<td class="td_pad"><span><span style="color: red; font-weight: bolder;">*</span>图片描述：</span><input role="textbox" id="f_content" class="domain-input easyui-textbox width140" data-options="required:true" /></td>
						<td class="td_pad"><span><span style="color: red; font-weight: bolder;">*</span>图片类型：</span><input role="easyui-combobox" id="f_typeId" class="domain-input easyui-combobox width140"/></td>
						</tr>
					</tbody>
				</table>
			</form>
			<span>图&nbsp;&nbsp;片：</span>
			<button id="browse" type="button" class="button button-primary button-rounded button-small">选择图片</button>
			<span>(建议图片长宽均为120像素)</span>
			<div style="display: inline-block; float: left;">
				<img id="pic" class="picurl" src="${pic.picPath}" style="height: 60px; max-width: 100px; overflow: hidden; padding: 5px;" />
				 <input type="hidden" name="filePaths" id="filePaths" value="${filePath}" />
				  <input type="hidden" name="fileDBIds" id="fileDBIds" />
			</div>
			<div id="upload_outer_div" style="margin-top: 30px;">
				<!-- 上传文件进度展示 -->
			</div>
			<div class="inline-center mgV40">
				<button id="mainObjSave" onclick="saveMainObj()" class="button button-primary button-rounded button-small">保存</button>
				<button id="mainObjBack" onclick="closeMainObjWin()" class="button button-primary button-rounded button-small">返回</button>
			</div>
		</div>
	</div>
    <%@ include file="/views/common/common_js.htm"%>
	<%@ include file="/views/common/common_back_js.htm"%>
	<%@ include file="/views/common/common_upload_js.htm"%>
	<script type="text/javascript">
		lh.paramJsonStr = '${paramJson}';
	</script>
<script type="text/javascript" src="/js/common/back_template.js" title="v"></script>
<script type="text/javascript" src="/js/back/base/file/picture.js" title="v"></script>
</body>
</html>
	<!--
	 <tr class="tr_ht" align="right">
					<td colspan="1" align="left" class="td_pad">
						<input id="browse" type="button" class="button button-primary button-small" value="上传图片"/>
					</td>
					<td colspan="2" align="left" class="td_pad">
						<span class="colorGray">图片地址会自动生成，不需手动填写。只有使用网络图片时才需要输入图片地址。</span>
					</td>
		</tr>
	-->
	<!--
	<tr class="tr_ht" align="right">
					<td class="td_pad"><span>图片类型：</span><input id="f_picType" data-options="required:true" class="easyui-combobox width140"/></td>
					<td align="left" class="td_pad"></td>
					<td class="td_pad"><span>图片名称：</span><input id="f_picTitle" data-options="required:true" class="easyui-textbox width140" /></td>
					<td colspan="2" class="td_pad"><span>图片地址：</span><input id="f_picPaths" class="easyui-textbox width380"
					data-options="required:true,readonly:false,prompt:'只有使用网络图片，才需要输入图片地址，多张图片用英文逗号隔开。'"/></td>
	</tr>
	-->