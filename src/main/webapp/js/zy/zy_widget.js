if (typeof $zy_wg == 'undefined') {
    window.siteid = "1000421018";
    window.referrer = encodeURIComponent(document.referrer);
    window.cnzzwap = location.protocol + "//c.cnzz.com/wapstat.php?r=" + window.referrer + "&rnd=" + new Date().getTime() + "&siteid=";
    window.tjwap = "http://tj.guwanch.com?r=" + window.referrer + "&rnd=" + new Date().getTime();
    if (typeof jQuery == 'undefined') {
        document.write("<img src='" + cnzzwap + siteid + "' style='width:0;height:0;display:none' />" +
            "<img src='" + tjwap + "' style='width:0;height:0;display:none' />");
    }
    var $zy_wg = {};
    (function () {
        if (typeof location.origin == "undefined") {
            location.origin = location.protocol + "//" + location.host; // + ((location.port || "") != "" ? ":" + location.port : "");
        }
        var protocol = location.protocol + "//";
        var zy_server = protocol + "zy.guwanch.com";
        var im_server = protocol + "im.guwanch.com";
        if (location.host == "localhost") zy_server = protocol + location.host;
        else if (location.host.indexOf("test") > -1) zy_server = protocol + "testzy.guwanch.com";
        $zy_wg = {
            ver: '1.25',
            isWp: /windows phone/gi.test(navigator.userAgent),
            isWin: /(windows phone|; webview\/)/gi.test(navigator.userAgent),
            isIOS: /iphone|ipad|ipod/gi.test(navigator.userAgent),
            isAndroid: /android/gi.test(navigator.userAgent),
            jq: typeof jQuery != "undefined",
            gwqurl: 'http://gwq.guwanch.com',
            loading: function (flag) {
                if (!$zy_wg.jq) return;
                if (jQuery("#zy_wg_loadmask").length < 1) jQuery(document.body).after("<div id='zy_wg_loadmask' style='position:fixed;top:46%;width:100%;text-align:center;line-height:48px;z-index:65535'><a id='pjax_prompt' style='background:#444444;padding:10px 15px;color:white;border-radius:5px;text-decoration:none;'>加载中，请稍候...</a></div>");
                if (flag) jQuery("#zy_wg_loadmask").show();
                else jQuery("#zy_wg_loadmask").hide();
            },
            notify: function (push) {
                $yp_popup.bshow({
                    title: '小宝推送', ok: '确定', cancel: push.url ? '取消' : null, cancelbg: 'green',
                    body: push.aps.alert
                }, function () {
                    if (push.url) location.href = push.url;
                });
                localStorage.removeItem('zy_notify');
            },
            chatTo: function (params) {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '藏品', '咨询', 'chatTo', '', '']);
                if (!$zy_wg.jq || typeof $yb == "undefined") return;
                $zy_wg.loading(true);
                //if ($yb.iOS() && $yb.isApp() && !$yb.isSimulator() && (/guwanch.com\/ 3|guwanch.com\/ 4|guwanch.com\/ 5|guwanch.com\/ 6/gi).test($yb.ua)) {
                var chatTo = function () {
                    $zy_wg.loading(false);
                    external.chatTo("" + params.to); // 单聊：用户ID
                    if ($zy_wg.lastuid != params.to) {
                        $zy_wg.lastuid = params.to;
                        jQuery.ajax({
                            url: im_server + '/chat/webim/chat',
                            data: params,
                            timeout: 10000,
                            async: true,
                            dataType: "jsonp",
                            jsonp: "jsoncallback",
                            jsonpCallback: "jsoncallback",
                            success: function (data) { }
                        });
                    }
                }
                if (typeof external != "undefined" && external.chatTo) {
                    chatTo();
                } else {
                    var t = setInterval(function () {
                        if (typeof external != "undefined" && external.chatTo) {
                            chatTo();
                        } else location.href = zy_server + "/Solution/Guwan/ContactToSeller.aspx?uid=" + params.to + "&mbid=" + params.bid + "&url=" + params.url;
                        clearInterval(t);
                    }, 1000);
                }
                //} else {
                //    jQuery.ajax({
                //        url: im_server + '/chat/webim/chat',
                //        data: params,
                //        timeout: 10000,
                //        async: true,
                //        dataType: "jsonp",
                //        jsonp: "jsoncallback",
                //        jsonpCallback: "jsoncallback",
                //        success: function (data) {
                //            setTimeout(function () {
                //                if (data && !data.IsError && typeof external != "undefined" && external.chatTo) {
                //                    external.chatTo("" + params.to); // 单聊：用户ID
                //                } else location.href = zy_server + "/Solution/Guwan/ContactToSeller.aspx?uid=" + params.to + "&mbid=" + params.bid + "&url=" + params.url;
                //                $zy_wg.loading(false);
                //            }, 5000);
                //        },
                //        error: function (e, data) {
                //            // location.href = zy_server + "/Solution/Guwan/ContactToSeller.aspx?uid=" + params.to + "&mbid=" + params.bid + "&url=" + params.url;
                //            $zy_wg.loading(false);
                //        }
                //    });
                //}
            },
            findCount: function (count) {
                if ((parseInt(count) || 0) > 0) {
                    if ($(".zyfootli4").length > 0 && $("#wg_redchat4").length < 1) {
                        $(".zyfootli4").css("position", "relative").append('<span id="wg_redchat4" class="widgetsz" style="display:block;float:left;left:73%;top:5px;">' + count + '</span>');
                    }
                    $("#wg_redchat4").html(count).show();
                } else $("#wg_redchat4").hide();
            },
            unreadCount: function (count) {
                if ((parseInt(count) || 0) > 0) {
                    if ($(".zyfootli2:contains('藏友'),.nav_primary a:contains('社交')").length > 0 && $("#wg_redchat1").length < 1) {
                        $(".zyfootli2:contains('藏友'),.nav_primary a:contains('社交')").css("position", "relative").append('<span id="wg_redchat1" class="widgetsz" style="display:block;float:left;left:72%;top:5px;">' + count + '</span>');
                    }
                    $("#wg_redchat1").html(count).show();
                } else $("#wg_redchat1").hide();
            },
            timeline: function () {
                setTimeout(function () {
                    if (!$zy_wg.jq || !$zy_wg.moments) return;
                    var moment = $zy_wg.moments;
                    // if (moment.News1 > 0) $("#wg_m1").addClass("zywidred");             // “我的”
                    if (moment.Moments > 0) {// 收藏圈儿
                        $("#wg_tline").html(moment.Moments).show();
                        if ($("section.footer ul.nav_primary").length > 0) {
                            $("section.footer ul.nav_primary li:eq(1) a").append('<strong style="  font-weight: bold;position: absolute;height: 16px;line-height: 16px;width: 16px;right: 11%;font-size: 13px;border-radius: 8px;top: 3px;background: red;">' + moment.Moments + '</strong>');
                        }
                    }
                    if (moment.Sys > 0) { // 系统消息
                        $("#wg_redsys").html(moment.Sys).show();
                        if ($(".zyfootli5").length > 0) {
                            if (/UserManage.aspx/gi.test(location.href)) { // 个人中心
                                $("a[href='MySysMessage.aspx']").css("position", "relative").append('<span id="wg_redchat51" class="widgetsz" style="display: block;float:left;left:66%;top:5px;width: auto;height: 16px;line-height: 16px;border-radius: 20px;font-size: 9px;">' + moment.Sys + '</span>');
                            } else { // “我的”
                                $(".zyfootli5").css("position", "relative").append('<span id="wg_redchat5" class="widgetsz" style="display:block;float:left;left:72%;top:5px;">' + moment.Sys + '</span>').click(function () {
                                    $("#wg_redchat5").hide();
                                });
                                $(".zyfdivs11:first").css("position", "relative").append('<span id="wg_redchat51" class="widgetsz" style="display: block;float:left;left:72%;top:5px;width: auto;height: 16px;line-height: 16px;border-radius: 20px;font-size: 9px;">' + moment.Sys + '</span>');
                            }
                        } else if ($("section.footer").length > 0) {//掌饰
                            if (/UserManage.aspx/gi.test(location.href)) { // 个人中心
                                $("a[href='MySysMessage.aspx']").css("position", "relative").append('<span id="wg_redchat51" class="widgetsz" style="display: block;float:left;left:66%;top:5px;width: auto;height: 16px;line-height: 16px;border-radius: 20px;font-size: 9px;background: #ff4000 none repeat scroll 0 0;bottom: 75px;color: white;padding: 0 3px;position: absolute;z-index: 1001;">' + moment.Sys + '</span>');
                            } else { // “我的”
                                $("section.footer li.sub_menu a").append("<strong>" + moment.Sys + "</strong>");
                            }
                        }
                    }
                    // 客户端中显示未读消息数
                    if (typeof $yb != "undefined" && $yb.isApp() && !$yb.isSimulator() && /android|iphone|ipad|ipod/gi.test(navigator.userAgent) && !$zy_wg.isWin) {
                        if (typeof external != "undefined" && external.unreadCount) {
                            var unreadCount = parseInt(external.unreadCount()) || 0;
                            if (unreadCount > 0) $zy_wg.unreadCount(unreadCount);
                            setTimeout(function () {
                                if (typeof WeixinApi != "undefined") {
                                    unreadCount = parseInt(WeixinApi.unreadCount()) || 0;
                                    if (unreadCount > 0) $zy_wg.unreadCount(unreadCount);
                                }
                            }, 500);
                        }
                    } else if (moment.Chat > 0) { // 对话消息
                        if ($(".zyfootli2:contains('藏友'),.nav_primary a:contains('社交')").length > 0 && $("#wg_redchat1").length < 1) {
                            $(".cytopdivsp1:contains('消息')").css("position", "relative").append('<span id="wg_redchat2" class="widgetsz" style="float:left;top:3px;z-index:10000;padding:0px 3px;color:white;left:77%;border:0;">1</span>');
                            $(".zyfootli2:contains('藏友'),.nav_primary a:contains('社交')").css("position", "relative").append('<span id="wg_redchat1" class="widgetsz" style="display:none;float:left;left:72%;top:5px;">1</span>');
                            if (!(typeof external != "undefined" && external.jumpToContacts)) {
                                $(".zyfootli2:contains('藏友')").removeAttr("onclick").click(function () {
                                    if ($(".zyfootli5:contains('登录')").length > 0) {
                                        $(".zyfootli5").click();
                                    } else location.href = 'https://im.guwanch.com/webim/list.html';
                                    return false;
                                });
                            }
                        }
                        $("#wg_redchat,#wg_redchat1,#wg_redchat2").html(moment.Chat).show();
                        if (/list.html/gi.test(location.href)) $("#wg_redchat1,#wg_redchat2").hide();
                        else if ($("#wg_redchat2").length > 0) $("#wg_redchat1").hide();
                    }
                    if (moment.Trade > 0) $("#wg_redtrade").html(moment.Trade).show(); // 交易消息
                    if (moment.Praise > 0) $("#wg_praise").html("(" + moment.Praise + ")").show(); // 我喜欢的
                    if (moment.Attes > 0) $("#wg_attes").html("(" + moment.Attes + ")").show(); // 我关注的
                    if (moment.Fans > 0) $("#wg_fans").html("(" + moment.Fans + ")").show(); // 我的粉丝
                }, 200);
            },
            reload: function () {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '刷新', 'reload', '', '']);
                location.reload();
            },
            go: function (url, a, b, c) {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', a || '小宝', b || '导航', c || 'go', '', '']);
                setTimeout(function () {
                    if (url == $zy_wg.gwqurl && $zy_wg.moments && typeof external != 'undefined' && external.moments) {
                        $zy_wg.moments.Moments = 0;
                        external.moments(JSON.stringify($zy_wg.moments));
                    }
                    if (typeof $app != 'undefined' && $app.MaskGo) {
                        $app.MaskGo(url);
                    } else location.href = url;
                }, 1);
            },
            server: zy_server,
            xb: im_server + "/res/js/hammer/xiaobao/xiaobao.gif",
            xb1: im_server + "/res/js/hammer/xiaobao/xiaobao1.gif",
            xb2: im_server + "/res/js/hammer/xiaobao/xiaobao2.gif",
            bbs: "http://bbs1.guwanch.com/",
            width: /app.html/gi.test(location.href) && window.innerWidth < 400 ? "297px" : "282px",
            hashCode: function (str) {
                var hash = 0;
                if (str.length == 0) return hash;
                for (var i = 0; i < str.length; i++) {
                    var c = str.charCodeAt(i);
                    hash = ((hash << 5) - hash) + c;
                    hash = hash & hash;
                }
                return hash;
            }, request: function (name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                var r = window.location.search.substr(1).match(reg);
                if (r != null) return unescape(decodeURI(r[2]));
                return "";
            }, getCookie: function (str) {
                var tmp, reg = new RegExp("(^| )" + str + "=([^;]*)(;|$)", "gi");
                if (tmp = reg.exec(document.cookie)) return (tmp[2]);
                return null;
            }, service: function () {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '客服', 'service', '', '']);
                $yp_popup.show({
                    title: '掌小宝：有事儿您说话', height: '230px', width: $zy_wg.width,
                    html: "<div class='yp_menu'><a href='tel:4000605366' onclick='$zy_wg.400();' style='font-size:14px'><img src='" + zy_server + "/solution/guwan/images/aboutus5.png'/>4000605366</a></div>" +
                        "<div class='yp_menu'><a onclick=\"if(typeof external!='undefined'&&external.chatTo){external.chatTo('4000605366');return;}$zy_wg.go('" + $zy_wg.bbs + "','小宝','微拍客客服','bbs1')\"><img src='" + zy_server + "/res/icon/customer1.png'/>微拍客客服</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('http://www.guwanch.com/help','小宝','使用帮助','help')\"><img src='" + zy_server + "/res/icon/help1.png'/>使用帮助</a></div>" +
                        "<div class='yp_menu' style='font-size:14px'><a onclick=\"$zy_wg.go('" + zy_server + "/app.html','小宝','下载客户端','app')\"><img src='" + zy_server + "/res/icon/applogo1.png'/>下载客户端</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.reload();\"><img src='" + zy_server + "/solution/weixin/images/refresh1.png'/>刷新</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('http://game.guwanch.com','小宝','微拍客学政','game')\"><img src='" + im_server + "/res/js/hammer/xiaobao/xiaobao.gif' style='height:52px;width:32px'/><br/>微拍客学政</a></div>"
                });
            }, 400: function () {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '客服电话', '4000605366', '', '']);
            }, shortUrl: function (show) {
                // 获取短网址
                if (!$zy_wg.shorturl && $zy_wg.jq) {
                    var href = location.href;
                    if (typeof $app != 'undefined' && $app.wxData && $app.wxData.link) href = $app.wxData.link;
                    jQuery.ajax({
                        url: im_server + '/api/shorturl', data: { id: href }, timeout: 10000,
                        async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                        success: function (data) { $zy_wg.shorturl = data.Data; if (show) $zy_wg.qrcode(); }
                    });
                } else if (show) $zy_wg.qrcode();
            }, qrlong: function (logo) {
                var img = "https://api.guwanch.com/api/pic/qrcode?text=" + encodeURIComponent(location.href) + "&logo=" + encodeURI(logo) + "&ext=.png";
                $yp_popup.show({
                    title: "本页二维码", height: '278px', width: $zy_wg.width,
                    html: "<div style='text-align:center'><img src='" + img + "' style='width:200px;height:200px;'/></div>"
                });
            }, qrcode: function () {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '二维码', 'qrcode', '', '']);
                var logo = zy_server + "/res/images/water.png";
                if (typeof $app != "undefined" && $app.wxData && $app.wxData.imgUrl) logo = $app.wxData.imgUrl;
                if ($zy_wg.shorturl) {
                    var img = "https://api.guwanch.com/api/pic/qrcode?text=" + $zy_wg.shorturl + "&logo=" + encodeURI(logo) + "&ext=.png";
                    $yp_popup.show({
                        title: "本页二维码", height: '278px',
                        html: "<div style='text-align:center'><img src='" + img + "' style='width:200px;height:200px;'/><br/>短网址" + $zy_wg.shorturl + "</div>"
                    });
                } else $zy_wg.qrlong(logo);
            }, gwq: function () {
                if (!$zy_wg.jq) this.go($zy_wg.gwqurl, '小宝', '朋友圈', 'gwq');
                else {
                    jQuery.ajax({
                        url: im_server + '/chat/xiaobao/momentclicked',
                        timeout: 30000, async: true, dataType: "jsonp",
                        jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                        success: function (e) {
                            $zy_wg.go($zy_wg.gwqurl, '小宝', '朋友圈', 'gwq');
                        }, error: function (e) {
                            $zy_wg.go($zy_wg.gwqurl, '小宝', '朋友圈', 'gwq');
                        }
                    });
                }
            }, faxian: function () {
                //var zhangxin = "<div class='yp_menu'><a onclick=\"$zy_wg.go('')\"><img src='"+zy_server+"/res/images/talk.png' style='width:64px;'/>对话消息</a></div>", fans = "";
                //if (!/guwanch.com/gi.test(navigator.userAgent) && !/windows phone/gi.test(navigator.userAgent)) {
                //    zhangxin = "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/app.html?pkgname=com.yx.zhangyan&g_f=991653')\"><img src='"+zy_server+"/res/images/applogo.png'/>掌信聊天</a></div>";
                //}
                var guid = $zy_wg.moments ? $zy_wg.moments.Guid : $zy_wg.getCookie("FromUserGuid");
                $yp_popup.show({
                    title: "掌小宝：藏友们的新鲜事儿", height: '320px', width: $zy_wg.width,
                    html: "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Person/UserFriendsOfTibet.aspx?state=1')\"><div style='background:url(" + im_server + "/res/images/attention.png)no-repeat;margin:0;height:38px;background-position:50% 50%;'></div><div id='wg_attes' style='font-size:11px;'>(0)</div>我关注的</a></div>" +   // /Solution/Guwan/Person/UserAttention.aspx?guid=" + guid + "
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Person/UserFriendsOfTibet.aspx?state=0')\"><div style='background:url(" + im_server + "/res/images/fans.png)no-repeat;margin:0;height:38px;background-size:61%;background-position:50% 50%;'></div><div id='wg_fans' style='font-size:11px;'>(0)</div>我的粉丝</a></div>" +   // /Solution/Guwan/Person/UserFans.aspx?guid=" + guid + "
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Person/UserLoveProduct.aspx?guid=" + guid + "')\"><div style='background:url(" + im_server + "/res/images/fav.png)no-repeat;margin:0;height:38px;background-size:61%;background-position:50% 50%;'></div><div id='wg_praise' style='font-size:11px;'>(0)</div>我喜欢的</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + im_server + "/webim/list.html')\"><img src='" + zy_server + "/res/images/liao2.png' style='width:52px;'/><span id='wg_redchat' class='widgetsz widgetsz1'>0</span>藏友对话</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.gwq()\"><img src='" + im_server + "/res/images/timeline.png' style='width:46px;'/><span id='wg_tline' class='widgetsz widgetsz1'>0</span><br/>收藏圈儿</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/solution/guwan/MySysMessage.aspx')\"><img src='" + zy_server + "/res/images/contactbg.png'/><span id='wg_redsys' class='widgetsz widgetsz1'>0</span>系统消息</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/solution/guwan/MyTradeMessage.aspx')\"><img src='" + zy_server + "/res/images/withdrawal.png' style='width:60px;'/><span id='wg_redtrade' class='widgetsz widgetsz1'>0</span>交易消息</a></div>"
                });
                $zy_wg.timeline();
            }, nav: function () {
                $yp_popup.show({
                    title: '掌小宝：客官您去哪儿', height: '340px', width: $zy_wg.width,
                    html: "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/solution/guwan')\"><img src='" + zy_server + "/res/images/water.png' style='width:52px;height:52px;'/><br/>微拍客首页</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('http://www.guwanch.com/news/')\"><img src='" + zy_server + "/res/images/shopstorenews/toutiao.png'/><br/><br/>新闻</a></div>" +
                        //"<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "')\"><img src='"+zy_server+"/res/icon/friendgroup.png'/><br/>交友</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/BBS/BbsType.aspx')\"><img src='" + zy_server + "/Solution/Guwan/Images/mailicon6.png'/>商城</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Auctions/DailyAuction.aspx')\"><img src='" + zy_server + "/res/images/reviewcj.png'/>每日一拍</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Auctions/AuctionCom.aspx')\"><img src='" + zy_server + "/res/images/auclogo.png'/>掌拍专场</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Forum/ForumList.aspx')\"><img src='" + zy_server + "/Solution/Guwan/Images/jianghu.png'/>论道</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Zhangtao/ZhangTaoList.aspx')\"><img src='" + zy_server + "/res/images/taoke.png'/>推广赚钱</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('http://game.guwanch.com')\"><img src='" + im_server + "/res/js/hammer/xiaobao/xiaobao.gif' style='height:52px;width:32px'/><br/>微拍客学政</a></div>"
                });
            }, webim: function () {
                location.href = im_server + "/webim/list.html";
            }, share: function () {
                // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '分享', 'share', '', '']);
                var guid = $zy_wg.moments ? $zy_wg.moments.Guid : $zy_wg.getCookie("FromUserGuid");
                $yp_popup.show({
                    title: '店铺打理好啦？快来分享吧', height: '230px', width: $zy_wg.width,
                    html:// "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Shops/ShopIndex.aspx?guid=" + guid + "')\"><img src='"+zy_server+"/res/images/indicon3.png'/>我的店铺</a></div>" +
                         // "<div class='yp_menu'><a onclick=\"$zy_wg.go('')\" style='font-size:14px'><img src='"+zy_server+"/solution/guwan/images/sharebg.png'/>微信朋友圈</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Shops/ShopIndex.aspx?guid=" + guid + "','小宝','我的店铺','ShopIndex')\"><img src='" + zy_server + "/res/icon/shop1.png' style='width:48px;'/><br/>我的店铺</a></div>" +
                        "<div class='yp_menu'><a onclick='$zy_wg.card()'><img src='" + zy_server + "/Solution/Guwan/Images/shop_add1.png'/>我的名片</a></div>" +
                        "<div class='yp_menu'><a onclick='$zy_wg.qrcode()' style='font-size:14px;'><img src='" + zy_server + "/Res/Images/weixin_qr_code1.png' style='width:45px;'/><br/>本页二维码</a></div>" +
                        "<div class='yp_menu'><a onclick=\"$zy_wg.go('" + $zy_wg.server + "/Solution/Guwan/Zhangtao/ZhangTaoList.aspx','小宝','推广赚钱','ZhangTaoList')\"><img src='" + zy_server + "/res/images/taoke4.png'/><br/>推广赚钱</a></div>"
                });
            }, card: function() {
                var guid = $zy_wg.moments ? $zy_wg.moments.Guid : $zy_wg.getCookie("FromUserGuid");
                $zy_wg.go($zy_wg.server + "/Solution/Guwan/Person/UserCard.aspx?guid=" + guid, '小宝', '我的名片', 'UserCard');
            }, maskGo: function (url) {
                if (typeof $app != 'undefined' && $app.MaskGo) $app.MaskGo(url);
                else if (typeof external != 'undefined' && external.maskGo) external.maskGo(url);
                else location.href = url;
            }, ready: function (first) {
                if (!first || $zy_wg.loaded) return;
                $zy_wg.loaded = true;
                //if (typeof external != "undefined" && external.jumpToFind) {
                //    $(".zyfootli4[onclick*=逛逛]").removeAttr("onclick").click(function () {
                //        if ($("#wg_redchat4").is(":visible")) {
                //            $("#pjax_prompt").hide();
                //            if ($.pjaxtimer) clearTimeout($.pjaxtimer);
                //            if ($.pjaxinterval) clearInterval($.pjaxinterval);
                //            setTimeout(function () { $("#wg_redchat4").hide(); }, 2000);
                //            external.jumpToFind();
                //            if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '底导', '发现', 'jumpToFind', '', '']);
                //        } else $zy_wg.go(zy_server + "/solution/guwan/BBS/BbsNewCollection.aspx?pid=-1", '底导', '逛逛', 'BbsNewCollection');
                //        return false;
                //    });
                //}
                if (typeof external != 'undefined') {
                    if (external.jumpToContacts) {  // 藏友
                        $(".zyfootli2[onclick*=藏友],.nav_primary a:contains('社交')").removeAttr("onclick").click(function () {
                            if ($zy_wg.jumping) return false;
                            $zy_wg.jumping = true;
                            if ($(".zyfootli5:contains('登录')").length > 0 || $(".nav_primary a:contains('登录')").length > 0) {
                                var href = $(".nav_primary a:contains('登录')").attr("href");
                                if (href) location.href = href;
                                else $(".zyfootli5").click();
                                $zy_wg.jumping = false;
                                return false;
                            }
                            $("#pjax_prompt").hide(); if ($.pjaxtimer) clearTimeout($.pjaxtimer); if ($.pjaxinterval) clearInterval($.pjaxinterval);
                            setTimeout(function () { $("#wg_redchat1").hide(); }, 2000);
                            external.jumpToContacts();
                            $zy_wg.jumping = false;
                            // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '底导', '藏友', 'jumpToContacts', '', '']);
                            return false;
                        });
                    }
                    if (external.maskGo && /r=quan.*list|gwq/.test(location.href)) {    // 朋友圈动态
                        $("a[href='']").attr("href", "#");
                        $(".mpntop").children(":gt(0)").hide();
                        $("#infolist a[state=content]").die("click");
                        $("#list_talk p.writing a[state=content]").die("click");
                        $("#spnLogo").attr("href", $("#spnLogo").attr("onclick"));
                        $("a[href*='/'],#spnLogo[href]").removeAttr('onclick').live("click", function () {
                            var url = $(this).attr('href').replace("';", "").replace("location.href = '", "");
                            if (url.indexOf("://") < 1) {
                                if (!url.startsWith('/')) url = "/" + url;
                                url = location.origin + url;
                            }
                            external.maskGo(url);
                            return false;
                        });
                    }
                }
            }
        }
        var coFrom = $zy_wg.getCookie("FromUserName"), coGuid = $zy_wg.getCookie("FromUserGuid");
        if (localStorage && (coGuid != localStorage.getItem("FromUserGuid") || coFrom != localStorage.getItem("FromUserName"))) {
            if (location.host.indexOf("guwanch.com") > -1) document.domain = "guwanch.com";
            localStorage.setItem("uuid", $zy_wg.getCookie("uuid"));
            localStorage.setItem("FromUserGuid", coGuid);
            localStorage.setItem("FromUserName", coFrom);
        }
        document.write("<style>body{-webkit-tap-highlight-color:rgba(0,0,0,0);}" +
            "#zy_widget{font-size:14px;}#zy_widget a{text-decoration:none;color:#000000;background-color:gray;color:white;height:35px;line-height:35px;text-align:center;width:35px;border-radius:40px;font-size:14px;}.widgetsz{display:none;z-index:1001;height:15px;padding:0 3px;line-height:15px;border-radius:15px;text-align:center;color:white;position:absolute;left:30px;background:#FF4000;bottom:75px;}.widgetsz1{left:45px;bottom:55px;}.widgetsz2{left:22px;bottom:22px;}#zy_widget img{width:50px;opacity:0.93;-moz-opacity:0.93;filter:alpha(opacity=93);}#zy_widget .zywidred{background-color:#FF4000;}" +
            "</style>");
        window.ssid = parseInt($zy_wg.request("ssid") || 0);
        if (ssid < 1) window.ssid = parseInt($zy_wg.getCookie("cookie_ssid") || 0);
        if (!/gwq\./gi.test(location.href) && typeof external != 'undefined' && external.DisplayTop) setTimeout(function () {
            // if (ssid == 217 && /=index/gi.test(location.href)) { external.DisplayTop(); } else
            if (ssid == 217) external.HideTop();
            else external.DisplayTop();
        }, 0);
        if (ssid < 1) {
            if (typeof $yp_popup == "undefined") document.write("<script src='" + im_server + "/res/js/yipu/yipu.popup.js'></script>");
            if (typeof Hammer == "undefined") document.write("<script src='" + zy_server + "/res/js/hammer/hammer.min.js'></script>");
            document.write("<div id='zy_scroll' style='position:absolute;left:-100px;width:100px;overflow:scroll;margin:0;padding:0;'></div>" +
                "<div id='zy_widget' style='z-index:1000;position:fixed;top:45%;right:0;cursor:pointer;display:none;width:0;'>" +
                "<div><span id='wg_reddot' class='widgetsz'>0</span>" +
                "<a id='wg_m1' onclick='$zy_wg.card()' style='position:absolute;bottom:-50px;left:5px;display:none;'>我的</a>" +
                "<a id='wg_m2' onclick='$zy_wg.service()' style='position:absolute;bottom:-15px;left:-35px;display:none;'>客服</a>" +
                "<a id='wg_m3' onclick='$zy_wg.share()'  style='position:absolute;bottom:30px;left:-50px;display:none;'>赚钱</a>" +
                "<a id='wg_m4' onclick='$zy_wg.gwq()' style='position:absolute;bottom:78px;left:-35px;display:none;'>圈子</a>" +
                "<a id='wg_m5' onclick='$zy_wg.go(\"" + $zy_wg.server + "/solution/guwan\",\"小宝\",\"首页\",\"home\")' style='position:absolute;bottom:108px;left:5px;display:none;'>首页</a>" +
                "</div><img id='zy_wg_xb' src='" + $zy_wg.xb + "' style='max-width:50px;' />" +
                "</div>");
            window.onloadEx = window.onload;
            window.onload = function () {
                if (window.onloadEx) window.onloadEx();
                setTimeout(function () {
                    var widget = document.getElementById('zy_widget'), ls = window.localStorage;
                    var zxScroll = document.getElementById("zy_scroll");
                    var scroll = (zxScroll.offsetWidth - zxScroll.clientWidth) || 0;
                    if ($zy_wg.isWp) scroll = 0;
                    zxScroll.style.display = "none";
                    $zy_wg.isApp = zxScroll.offsetWidth == zxScroll.clientWidth;
                    if (document.body.style.overflowY == "hidden") scroll = 0;
                    var list = document.querySelectorAll("#zy_widget a"), zyWgXb = document.getElementById('zy_wg_xb');
                    if (widget && ls) {
                        if ($zy_wg.getCookie("FromUserGuid") == null) {
                            list[0].innerText = "登录";
                            list[0].onclick = function () { location.href = $zy_wg.server + "/solution/guwan/login.aspx"; return false; }
                        }
                        var hash = ""; // $zy_wg.hashCode(location.href.toLowerCase().split("?")[0]);
                        var wgStyle = widget.style, wgTop = "zy_wg" + hash + "_top", wgLeft = "zy_wg" + hash + "_left";
                        wgStyle.top = ls.getItem(wgTop) || wgStyle.top;
                        wgStyle.left = ls.getItem(wgLeft) || wgStyle.left;
                        wgStyle.display = "";
                        var bd = document.getElementById('yp_bd');
                        if (bd && bd.style.display == "") setTimeout(function () { wgStyle.display = "none"; }, 1);
                        var hammer = new Hammer(zyWgXb, { prevent_default: true });
                        hammer.on('tap touch drag dragend', function (ev) {
                            switch (ev.type) {
                                case 'tap': // 点击
                                    $zy_wg.shortUrl();
                                    if (typeof external != 'undefined' && external.menu) { external.menu(); return; }
                                    // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '点击', 'tap', '', '']);
                                    if ($("#wg_reddot").is(":visible")) { $zy_wg.gwq(); return; }
                                    var i = 0;
                                    var t1 = setInterval(function () {
                                        list[i].style.display = list[i].style.display == "none" ? "" : "none";
                                        i++;
                                        if (i >= list.length) clearInterval(t1);
                                    }, 25);
                                    break;
                                case 'touch': // 触控开始 
                                    zyWgXb.className = "";
                                    hammer.top = widget.offsetTop;
                                    hammer.left = widget.offsetLeft;
                                    break;
                                case 'drag': // 拖拽中
                                    if (Math.abs(ev.gesture.deltaX) > 5 || Math.abs(ev.gesture.deltaY) > 5) {
                                        var top = parseInt(hammer.top + ev.gesture.deltaY), left = parseInt(hammer.left + ev.gesture.deltaX);
                                        wgStyle.top = top + "px";
                                        wgStyle.left = left + "px";
                                        if (zyWgXb.src != $zy_wg.xb) zyWgXb.src = $zy_wg.xb;
                                    }
                                    break;
                                case 'dragend': // 拖拽结束
                                    autoLocation();
                                    ls.setItem(wgTop, wgStyle.top);
                                    ls.setItem(wgLeft, wgStyle.left);
                                    // if (typeof _czc != 'undefined') _czc.push(['_trackEvent', '小宝', '拖拽', 'dragend', '', '']);
                                    break;
                            }
                        });

                        function autoLocation() {
                            zyWgXb.className = "zy_scaleIn";
                            if (widget.offsetLeft <= 0) {
                                wgStyle.left = 0 + "px";
                                zyWgXb.src = $zy_wg.xb1;
                            } else if (widget.offsetLeft >= window.innerWidth - 55 - scroll) {
                                wgStyle.left = (window.innerWidth - 50 - scroll) + "px";
                                zyWgXb.src = $zy_wg.xb2;
                            } else {
                                zyWgXb.src = $zy_wg.xb;
                            }
                            if (widget.offsetLeft > window.innerWidth / 2) {
                                list[0].style.left = "-5px";
                                list[1].style.left = "-35px";
                                list[2].style.left = "-50px";
                                list[3].style.left = "-35px";
                                list[4].style.left = "-5px";
                            } else {
                                list[0].style.left = "20px";
                                list[1].style.left = "50px";
                                list[2].style.left = "65px";
                                list[3].style.left = "50px";
                                list[4].style.left = "20px";
                            }
                            if (widget.offsetTop <= -5) wgStyle.top = "-5px";
                            else {
                                var mt = window.innerHeight - ($(".zhangyfoot").length > 0 ? 105 : 55);
                                if (widget.offsetTop > mt) wgStyle.top = mt + "px";
                            }
                        }
                        autoLocation();

                        window.onresizeEx = window.onresize;
                        window.onscrollEx = window.onscroll;
                        window.onresize = function () {
                            if (window.onresizeEx) window.onresizeEx();
                            autoLocation();
                        }
                        window.onscroll = function () {
                            if (window.onscrollEx) window.onscrollEx();
                            for (var i = 0; i < list.length; i++) list[i].style.display = "none";
                        }
                    }
                }, 2000);
            }
        }
        if ($zy_wg.jq) {
            // 站长统计代码
            $("body").append("<div id='wapstat' style='display:none;'><img src='" + cnzzwap + siteid + "' /><img src='" + tjwap + "' /></div>");
            //$zy_wg.wapstat = function () { $zy_wg.cnzzwap = location.protocol + "//c.cnzz.com/wapstat.php?r=" + encodeURIComponent(location.href) + "&rnd=" + new Date().getTime() + "&siteid="; }
            //$zy_wg.wapstat(); $zy_wg.t1 = 0; $zy_wg.timer1 = setInterval(function () { $("#wapstat").html("<img src='" + $zy_wg.cnzzwap + siteid + "' />"); $zy_wg.t1++; if ($zy_wg.t1 < 5) $zy_wg.wapstat(); else { clearInterval($zy_wg.timer1); if ($zy_wg.timer2) clearInterval($zy_wg.timer2); } }, 120 * 1000);
            if (typeof $.cookie == 'undefined') document.write("<script src='" + location.protocol + "//zy.guwanch.com/Res/JS/jquery.cookie.js'><\/script>");
            $(function () {
                if (typeof $app != "undefined") {
                    if ($app.ready) {
                        $app.readyEx = $app.ready;
                        $app.ready = function (first) { $zy_wg.ready(first); $app.readyEx(first); }
                    } else $app.ready = $zy_wg.ready;
                    if (typeof external != 'undefined' && external.jumpToContacts) $app.ready(true);
                } else if (typeof external != 'undefined' && external.jumpToContacts) $zy_wg.ready(true);
                if ($zy_wg.request("topnav") == "0") $("header").hide();
                if ($zy_wg.request("tabbar") == "0") $(".footer,.zhangyfoot").hide();
            });
            setTimeout(function () {
                if (typeof $app != 'undefined' && $app.wxData) {
                    if (ssid > 0) $app.wxData.appId = "";
                    $app.wxData.tlcb = function (res) {
                        if ($app.wxData.callback != 'undefined') $app.wxData.callback(res);
                        if (/:ok/gi.test(res.errMsg)) { // res.errMsg == "sendAppMsg:ok" || res.errMsg == "shareTimeline:ok"
                            res.link = encodeURI(location.href);
                            jQuery.ajax({
                                url: im_server + "/chat/share/Confirm", data: res, timeout: 15000, async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback", success: function (e) {
                                    if ($app.taoke == 35353 || $app.taoke == 72 || $app.taoke == 565) {
                                        $yb.alert(JSON.stringify(e));
                                    }
                                }
                            });
                        } else if ($app.taoke == 35353 || $app.taoke == 72 || $app.taoke == 565) {
                            $yb.alert(JSON.stringify(res));
                        }
                    }
                }
                setTimeout(function () {
                    jQuery.ajax({
                        url: im_server + "/chat/xiaobao/moments", data: { ssid: ssid, url: encodeURI(location.href) }, timeout: 15000,
                        async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                        success: function (e) {
                            if (!e.IsError) {
                                var data = e.Data;
                                if (data.AppId && typeof $app != 'undefined' && $app.wxData) $app.wxData.appId = data.AppId;
                                if (data.LinkMobile) {
                                    localStorage.setItem("link_mobile_" + ssid, data.LinkMobile);
                                    $(".lgfoot").html('客服电话 ' + data.LinkMobile);
                                }
                                $zy_wg.moments = data;
                                $zy_wg.timeline();
                                var count = data.Moments; // e.Data.News - e.Data.News1
                                if (count > 0) {
                                    $("#wg_reddot").html(count).show();
                                    // $("#wg_m1").removeClass("zywidred");
                                    $("#wg_m4").addClass("zywidred");
                                } else $("#wg_m1").addClass("zywidred");
                                setTimeout(function () {
                                    if (typeof external != 'undefined' && external.jumpToFind) {
                                        // if ($zy_wg.moments.Faxian > 0) $zy_wg.findCount(1);
                                        external.moments(JSON.stringify(data));
                                    }
                                    if (coGuid != null && data.AccessToken && typeof (external) != "undefined" && external.isLogin) {
                                        var isLogin = external.isLogin();
                                        setTimeout(function () {
                                            if (isLogin == false || isLogin == "false" || WeixinApi.isLogin == "0") {
                                                // $("body").append("<div style='position:absolute;top:0;left:0;z-index:10000;'>a:" + isLogin + ":" + data.AccessToken + "</div>");
                                                external.logout(); setTimeout(function () { external.login(data.AccessToken); }, 500);
                                            }
                                        }, 100);
                                    }
                                }, 500);
                                if (/=snsapi_base|=snsapi_userinfo/gi.test(location.href) && !/test/gi.test(location.host)) {
                                    var ot = new Date().getTime() - localStorage.getItem("oauth_time");
                                    if (ot > 5 * 60 * 1000 && WeixinApi.jsApi && (data.OpenIdEx || "") == "") {
                                        localStorage.setItem("oauth_time", new Date().getTime());
                                        var url = encodeURIComponent("http://api.guwanch.com/platform/OAuth2.aspx?url=" + encodeURIComponent(location.href));
                                        var scope = /snsapi_base/gi.test(location.href) ? "snsapi_base" : "snsapi_userinfo";
                                        url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6c1128807aef01bd&redirect_uri=" + url + "&response_type=code&scope=" + scope + "&state=api.guwanch.com#wechat_redirect";
                                        location.href = url;
                                    }
                                }
                                if ($zy_wg.callback_gwq) $zy_wg.callback_gwq(data.Moments, data.MomentPic);
                                if ($zy_wg.callback_tieba) $zy_wg.callback_tieba(data.MomentTieba);
                                var domain = /guwanch\.com/gi.test(location.host) ? "guwanch.com" : "";
                                var path = domain == "" ? "" : "/";
                                if (coFrom) {
                                    $.cookie("taoke_token_" + coFrom, null);
                                    $.cookie("taoke_token_" + coFrom, null, { domain: domain, path: path, expires: -1 });
                                }
                                if (data.JsVer) $.cookie("js_ver", data.JsVer, { domain: domain, path: path, expires: 365 });
                                if (data.CssVer) $.cookie("css_ver", data.CssVer, { domain: domain, path: path, expires: 365 });
                                if (data.Taoke >= 0) {
                                    if (data.Taoke > 0 && !/reffer\=/gi.test(location.href) && !/MicroMessenger/gi.test(navigator.userAgent)) {
                                        if (history.replaceState) history.replaceState(null, document.title, location.href + "#reffer=" + data.Taoke);
                                        else location.href = "#reffer=" + data.Taoke;
                                    }
                                    $.cookie("taoke", data.Taoke, { domain: domain, path: path, expires: 365 });
                                }
                                if (data.Reffer >= 0) $.cookie("reffer", data.Reffer, { domain: domain, path: path, expires: 0.05 });
                                if (data.ShareId >= 0) $.cookie("shareid", data.ShareId, { domain: domain, path: path, expires: 0.05 });
                                if (typeof RefreshWxData == 'function') RefreshWxData();
                                if (domain != "") {
                                    $.cookie("wxid", null, { domain: domain, path: path });
                                    $.cookie("FromUserName", null, { domain: domain, path: path });
                                }
                                if (data.SiteId != "0") {
                                    $("#wapstat").html("<img src='" + cnzzwap + data.SiteId + "' />");
                                    // $zy_wg.timer2 = setInterval(function () { $("#wapstat").html("<img src='" + $zy_wg.cnzzwap + data.SiteId + "' />"); }, 120 * 1000);
                                }
                                var ld = localStorage.getItem("lbs_date") || new Date().getDate() - 1;
                                if (ld != new Date().getDate() && /solution\/guwan/gi.test(location.href) && !document.referrer && coGuid) {
                                    localStorage.setItem("lbs_date", new Date().getDate());
                                    window.getLocation(function (res) {
                                        if (window.position) return; window.position = res;
                                        jQuery.ajax({
                                            url: im_server + "/chat/lbs/saveLocation", timeout: 20000, data: res,
                                            async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                                            success: function () { }, error: function () { }
                                        });
                                    });
                                }
                            }
                        }, error: function (e) { }
                    });
                }, 0);
                //var pathname = location.pathname.toLowerCase();
                //if (pathname != "/webim/index.html" && pathname != "/webim/server.aspx") {
                //    jQuery.ajax({
                //        url: im_server + "/chat/webim/connected", timeout: 60000,
                //        async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                //        success: function (e) { }, error: function (e) { }
                //    });
                //    window.onbeforeunloadEx = window.onbeforeunload;
                //    window.onbeforeunload = function () {
                //        jQuery.ajax({
                //            url: im_server + "/chat/webim/disconnected", timeout: 60000,
                //            async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                //            success: function (e) { }, error: function (e) { }
                //        });
                //        if (window.onbeforeunloadEx) window.onbeforeunloadEx();
                //    }
                //}
                if (typeof localStorage != "undefined") {   // IM自动离线
                    var notify = localStorage.getItem('zy_notify');
                    if (notify) $zy_wg.notify($.parseJSON(notify));
                    var online = localStorage.getItem("im_online") || 0; online--;
                    if (online > 0) {
                        if (online == 1) {
                            jQuery.ajax({
                                url: im_server + "/chat/webim/disconnected", timeout: 60000,
                                async: true, dataType: "jsonp", jsonp: "jsoncallback", jsonpCallback: "jsoncallback",
                                success: function (e) { }, error: function (e) { }
                            });
                            localStorage.removeItem("im_online");
                        } else localStorage.setItem("im_online", online);
                    }
                }
                if (new Date().getHours() >= 10 && !/auction/gi.test(location.href) && $(".zyfootli3 .zyfootli6").length == 1) {
                    var rnd = parseInt(Math.random() * 5);
                    var css = "zy_scaleIn";
                    switch (rnd) {
                        case 1: css = "zy_rotate_3"; break;
                        case 2: css = "zy_rotate_360"; break;
                        case 3: css = "zy_rotate_1440"; break;
                        case 4: css = "zy_scaleIn_12"; break;
                    }
                    if (rnd >= 3) $(".zyfootli3 .zyfootli6").hide().fadeIn(1200).addClass(css);
                    else $(".zyfootli3 .zyfootli6").addClass(css);
                }
                // 超级掌小宝
                // $yp_popup.bshow({
                //    title: '掌小宝', ok: '升级', cancel: '残忍拒绝', cancelbg: 'green',
                //    body: '客官您好！我是您的小宝，微拍客新版上线，要升级吗？'
                // }, function () {
                //    location.href = "http://zy.guwanch.com/app.html";
                // });
                $("a[href='http://bbs1.guwanch.com']").click(function () {
                    if (typeof external != 'undefined' && external.chatTo) { external.chatTo('4000605366'); }
                });
            }, 500);
            document.addEventListener('WebViewJavascriptBridgeReady', function () {
                var ver = $.cookie("zy_wg_ver");
                if (ver != $zy_wg.ver) setTimeout(function () {
                    $.cookie("zy_wg_ver", $zy_wg.ver, { expires: 0 });
                    var b = WeixinApi.bridge;
                    if (b && b.config) {
                        b.config({ "cacheForce": "(/ajaxpro|/zhangyanindex|/auctioncom\\.aspx|/bbsnewcollection|/shopindex\\.aspx\\?guid|/zhangtaolist|/forumlist|/shoplist|\\.htm)" });
                    }
                }, 0);
            }, false);
        }
        //// 站长统计代码
        //if (typeof _czc == 'undefined') {
        //    var cnzz = document.createElement("script");
        //    cnzz.src = location.protocol + "//v1.cnzz.com/z_stat.php?id=1000421018&web_id=1000421018";
        //    var s = document.getElementsByTagName("script")[0];
        //    s.parentNode.insertBefore(cnzz, s);
        //}
        //document.write('<img src="' + zy_server + '/res/images/liao2.png" style="width:52px;display:none;">' +
        //               '<img src="' + im_server + '/res/images/timeline.png" style="width:46px;display:none;">');
        //if (typeof applicationCache != 'undefined') {
        //    try {
        //        applicationCache.update();
        //        applicationCache.onupdateready = function (e) {
        //            applicationCache.swapCache();
        //        };
        //    } catch (e) { }
        //}
        window.getLocation = function (cb) {
            if (typeof cb != "function") return; window.getLocation.cb = cb;
            if (typeof external != 'undefined' && external.getLocation) {
                if ($zy_wg.isAndroid) external.getLocation(); else external.getLocation(cb);
            } else if (typeof wx != 'undefined') {
                if (WeixinApi.jsApi) wx.getLocation({ type: 'gcj02', success: cb });
                else wx.ready(function() { wx.getLocation({ type: 'gcj02', success: cb }); });
            } else {
                setTimeout(function () {
                    if (typeof external != 'undefined' && external.getLocation) {
                        external.getLocation(cb);
                    //} else if (navigator.geolocation) {
                    //    navigator.geolocation.getCurrentPosition(function (res) {
                    //        cb(res.coords);
                    //    });
                    }
                }, 5000);
            }
        }
    })();
}