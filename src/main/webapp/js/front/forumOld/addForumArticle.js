SAVING_FLAG = false;
/**初始化*/
$(function(){
	initUploadSimple();//初始化上传组件
})

function showActionSheet(){
	
	 var mask = $('#myMask');
     var weuiActionsheet = $('#weui_actionsheet');
     weuiActionsheet.addClass('weui_actionsheet_toggle');
     mask.show().addClass('weui_fade_toggle').click(function () {
         hideActionSheet(weuiActionsheet, mask);
     });
     $('#actionsheet_cancel').click(function () {
         hideActionSheet(weuiActionsheet, mask);
     });
     weuiActionsheet.unbind('transitionend').unbind('webkitTransitionEnd');

     function hideActionSheet(weuiActionsheet, mask) {
         weuiActionsheet.removeClass('weui_actionsheet_toggle');
         mask.removeClass('weui_fade_toggle');
         weuiActionsheet.on('transitionend', function () {
             mask.hide();
         }).on('webkitTransitionEnd', function () {
             mask.hide();
         })
     }
 
}


/**发表帖子*/
function addForumArticle(){
	//var title = $("#title").val();
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var content = $("#content").val();
	var typeId =  $("#typeId").val();
	var filePaths =  $("#filePaths").val();
	var fileDBIds =  $("#fileDBIds").val();
	//filePaths = filePaths.substring(1);
	var obj = {};
	/*if(!title){
		lh.alert('请填写标题');return ;
	}*/
	if(!content){
		lh.alert('请填写内容');SAVING_FLAG = false;return ;
	}
	var filePathArr = new Array();
	if(filePaths.indexOf(',') >= 0){
		filePaths = filePaths.substring(1);
	}
	filePathArr = filePaths.split(",");
	if(!filePaths){lh.alert('请上传图片');SAVING_FLAG = false;return;}
	if(filePathArr.length >= 9){
		lh.alert('只能上传8张图片');
		SAVING_FLAG = false;
		return;
	}
	obj.forumId = typeId;
	//obj.title = title;
	obj.content = content;
	obj.description = content;
	obj.picPaths = fileDBIds;
	obj.addForumArticle = 'yes';
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateForumArticle',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				var url = "/forumArticle/"+typeId;
				var r = $("#r").val();
				if(r) url += "?r="+r;
				window.location.href = url;
			}else{
				if(rsp.status == 'jumpToBindUserPhone'){
					lh.alert(rsp.msg,"jumpToBindUserPhone()");
				}else{
					lh.alert(rsp.msg);
				}
			}
		}
	},'json');
}

/**返回上一级*/
function goBack(){
	var typeId = $("#typeId").val();
	var url = "/forumArticle/"+typeId;
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}