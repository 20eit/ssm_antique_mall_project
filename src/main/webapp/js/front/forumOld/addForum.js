SAVING_FLAG = false;
/**初始化*/
$(function(){
	initUploadSimple({showEdBtns:true});//初始化上传组件
	/*$("#type").select2({
	    tags: "true",
	    placeholder: "",
	    allowClear: true
	 });*/
	$("#type").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    size: 10
	});
})

/**发表帖子*/
function addForum(){
	//var title = $("#title").val();
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var typeId =  $("#type").val();
	var name =  $("#name").val();
	var message =  $("#message").val();
	var filePaths =  $("#filePaths").val();
	var fileDBIds =  $("#fileDBIds").val();
	var obj = {};
	if(!name){
		lh.alert('请填写论坛名称');SAVING_FLAG = false;return ;
	}
	if(!typeId){
		lh.alert('请选择论坛类型');SAVING_FLAG = false;return ;
	}
	if(!filePaths){
		lh.alert('请上传论坛logo');SAVING_FLAG = false;return ;
	}
	filePaths = filePaths.substring(1);
	var filePathArr = new Array();
	if(filePaths.indexOf(',') >= 0){
		filePaths = filePaths.substring(1);
		fileDBIds = fileDBIds.substring(1);
	}
	if(filePathArr.length >= 2){
		lh.alert('只能上传1张logo图片');
		SAVING_FLAG = false;
		return;
	}
	obj.file1 = filePaths;
	obj.message = message;
	obj.attr1 = name;
	obj.attr2 = typeId;
	obj.applyType = '95';
	obj.fileDBIds = fileDBIds;
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateApply',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				frontBaseConfirm('你的论坛申请已经提交,管理员审核通过后即可使用.','goBack()');
				//window.location.href="/forum";
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function goBack(){
	var r = $("#r").val();
	var url = "/forums";
	if(r) url += "?r="+r;
	window.location.href = url;
}

function alertauto(){
    $('.zhezhao,#pfreturnforum').slideToggle(0);
}


function cancel(){
	$('.zhezhao,.reply').slideToggle(0);
}

