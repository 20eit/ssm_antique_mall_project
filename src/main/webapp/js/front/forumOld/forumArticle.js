CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
SAVING_FLAG = false;
$(function(){
	loadForumArticle();
	lh.scrollBottom(loadForumArticle);
	/*setTimeout(function(){
		lh.scrollBottom(loadComment);
	},'1000');*/
});

function showActionSheet(){
	
	 var mask = $('#myMask');
    var weuiActionsheet = $('#weui_actionsheet');
    weuiActionsheet.addClass('weui_actionsheet_toggle');
    mask.show().addClass('weui_fade_toggle').click(function () {
        hideActionSheet(weuiActionsheet, mask);
    });
    $('#actionsheet_cancel').click(function () {
        hideActionSheet(weuiActionsheet, mask);
    });
    weuiActionsheet.unbind('transitionend').unbind('webkitTransitionEnd');

    function hideActionSheet(weuiActionsheet, mask) {
        weuiActionsheet.removeClass('weui_actionsheet_toggle');
        mask.removeClass('weui_fade_toggle');
        weuiActionsheet.on('transitionend', function () {
            mask.hide();
        }).on('webkitTransitionEnd', function () {
            mask.hide();
        })
    }

}


function loadComment(articleIdsAry){
	//var objectIds = $("#object").val();
	//var objectId = objectIds.substring(1).split(",");
	for(var i in articleIdsAry){
		var param = {};
		param.objectId = articleIdsAry[i];
		param.commentTypeId = 2;
		param.start = 0;
		param.count = 3;
		$.post('/getCommentList',param,function(rsp){
			if(rsp){
				if(rsp.status == 'success'){
					var rows = rsp.rows;
					var count = rsp.total;
					if(count && count > 0){
						makeCommentListDom(rows,1,rows[0].objectId);
					}else{
						$('#resultTip').text('没有更多数据').show();
					}
				}else{
					lh.alert(rsp.msg);
				}
			}
			SCROLL_LOADING = false;//设置为加载完毕
		},'json');
	}
}


function makeCommentListDom(commentList,isAppend,objectId){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:commentList});
	if(isAppend){
		if(commentList.length == 3){$("#commentMore"+objectId).show()};
		if(commentList){$('#comment'+objectId).css('border-top','1px solid #F1EBF6');}
		$('#comment'+objectId).prepend(rendered);
	}else{
		$('#comment'+objectId).html(rendered);
	}
}

function loadForumArticle(){
		var typeId = $("#typeId").val();
		var param = {};
		param.forumId = typeId;
		param.rows = PAGE_COUNT;
		param.page = CURRENT_PAGE;
		$.post('/getForumArticleList',param,function(rsp){
			if(rsp){
				if(rsp.status == 'success'){
					var rows = rsp.rows;
					var count =  rsp.total;
					if(count && count > 0){
						makeForumArticleListDom(rows,1);
						var articleIdsAry = [];
						if(rows){
							for(var i=0; i<rows.length;i++){
								var row = rows[i];
								articleIdsAry.push(row.id);
								CURRENT_PAGE++;
							}
							loadComment(articleIdsAry);//加载评论
						}
					}else{
						$('#resultTip').text('没有更多数据').show();
					}
				}else{
					lh.alert(rsp.msg);
				}
			}
			SCROLL_LOADING = false;//设置为加载完毕
		},'json');
}


function makeForumArticleListDom(forumArticleList,isAppend){

	var template1 = $('#template1').html();
	Mustache.parse(template1);   // optional, speeds up future uses
	var rendered = Mustache.render(template1, {rows:forumArticleList});
		if(isAppend){
			$('#forumArticle').prepend(rendered);
		}else{
			$('#forumArticle').html(rendered);
		}
	
}

function alertauto(){
    $('.zhezhao,#pfreturnforum').slideToggle(0);
}

function reply(id,parentId,receiverId,receiverName){
    //$('.zhezhao').slideToggle(0);
    $('#dialog').show();
    $("#objectId").val(id);
    $("#parentId").val(parentId);
     $("#receiverId").val(receiverId);
    $("#receiverName").val(receiverName);
    $("#tips").text('');
}

function cancel(){
	//$('.zhezhao').slideToggle(0);
	$('#dialog').hide();
}

function addComment(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var content = $("#content").val();
	var objectId = $("#objectId").val();
	var parentId = $("#parentId").val();
	var receiverId = $("#receiverId").val();
	var receiverName = $("#receiverName").val();
	if(!content){
		lh.alert('请输入评论的内容');
		SAVING_FLAG = false;
		return ;
	}
	var obj = {};
	obj.content = content;
	obj.commentTypeId = 2;
	obj.objectId = objectId;
	obj.parentId = parentId;
	obj.receiverId = receiverId;
	obj.receiverName = receiverName;
	obj.comment = 'yes';
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateComment',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				reply();
				window.location.reload();
			}else{
				if(rsp.noPhone == 'noPhone'){
					lh.alert(rsp.msg,"jumpToBindUserPhone()");
				}else{
					lh.alert(rsp.msg);
				}
			}
		}
	},'json');
}

function addPraise(id){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var praiseNum = $("#praiseNum"+id).text();
	praiseNum++;
	var obj = {};
	obj.praiseNum = praiseNum;
	obj.id = id;
	obj.addForumArticle = 'yes';
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateForumArticle',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				//location.reload();
				$("#praiseNum"+id).text(praiseNum);
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}