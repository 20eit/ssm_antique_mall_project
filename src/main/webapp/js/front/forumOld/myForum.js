CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadForumMemberList();
	lh.scrollBottom(loadForumMemberList);
});

function loadForumMemberList(){
	var userId = $("#userId").val();
	var param = {};
	param.userId = userId;
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	$.post('/getForumMemberList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count =  rsp.total;
				if(count && count > 0){
					makeForumMemberListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}


function makeForumMemberListDom(forumMemberList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:forumMemberList});
	if(isAppend){
		$('#myForumList').append(rendered);
	}else{
		$('#myForumList').html(rendered);
	}
}

