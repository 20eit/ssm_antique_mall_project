CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadMyForumArticleCollect();
	lh.scrollBottom(loadMyForumArticleCollect);
});

function loadMyForumArticleCollect(){
	var forumId = $("#forumId").val();
	var userId = $("#userId").val();
	var param = {};
	param.forumId = forumId;
	param.page = CURRENT_PAGE;
	param.rows = PAGE_COUNT;
	$.post('/getforumArticleCollectList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count = rsp.total;
				if(count && count  > 0){
					makeMyForumArticleCollectListDom(rsp.rows,1);
					CURRENT_PAGE++;
					$("#commentList").show();
				}else{
					if(CURRENT_PAGE == 0){
						$('#resultTip').text('没有更多数据').hide();
						$("#notMyCollect").show();
					}else{
						$('#resultTip').text('没有更多数据').show();
						$("#notMyCollect").hide();
					}
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeMyForumArticleCollectListDom(myForumArticleCollectList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:myForumArticleCollectList});
	if(isAppend){
		$('#myForumArticleCollectList').prepend(rendered);
	}else{
		$('#myForumArticleCollectList').html(rendered);
	}
}

function cancelForumArticleCollect(id){
	$.post('/deleteForumArticleCollectThorough',{ids:id},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				lh.alert(rsp.msg);
				window.location.reload();
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

