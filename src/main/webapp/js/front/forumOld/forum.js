CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadForumList();
	lh.scrollBottom(loadForumList);
});

function loadForumList(){
	var typeId = $("#typeId").val();
	var param = {};
	if(typeId){
		param.typeId = typeId;
	}
	param.rows = PAGE_COUNT;
	param.page  = CURRENT_PAGE;
	param.orderBy = "visit_num";
	param.ascOrdesc = "DESC";
	$.post('/getForumList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count =  rsp.total;
				if(count && count > 0){
					makeForumListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}


function makeForumListDom(forumList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:forumList});
	if(isAppend){
		$('#forumList').append(rendered);
	}else{
		$('#forumList').html(rendered);
	}
}

