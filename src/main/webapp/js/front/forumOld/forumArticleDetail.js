CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
SAVING_FLAG = false;
$(function(){
	var share = $("#share").val();
	if(share){
		 $('.zhezhao,#share').slideToggle(0);
	}
	loadComment();
	lh.scrollBottom(loadComment);
});

function showActionSheet(){
	
	 var mask = $('#myMask');
   var weuiActionsheet = $('#weui_actionsheet');
   weuiActionsheet.addClass('weui_actionsheet_toggle');
   mask.show().addClass('weui_fade_toggle').click(function () {
       hideActionSheet(weuiActionsheet, mask);
   });
   $('#actionsheet_cancel').click(function () {
       hideActionSheet(weuiActionsheet, mask);
   });
   weuiActionsheet.unbind('transitionend').unbind('webkitTransitionEnd');

   function hideActionSheet(weuiActionsheet, mask) {
       weuiActionsheet.removeClass('weui_actionsheet_toggle');
       mask.removeClass('weui_fade_toggle');
       weuiActionsheet.on('transitionend', function () {
           mask.hide();
       }).on('webkitTransitionEnd', function () {
           mask.hide();
       })
   }

}

function loadComment(){
	var objectId = $("#object").val();
	var param = {};
	param.objectId = objectId;
	param.commentTypeId = 2;
	param.page = CURRENT_PAGE;
	param.rows = PAGE_COUNT;
	$.post('/getCommentList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count = rsp.total;
				if( count && count > 0){
					makeCommentListDom(rsp.rows,1);
					CURRENT_PAGE++;
					$("#commentList").show();
				}else{
					if(CURRENT_PAGE == 0){
						$('#resultTip').text('没有更多数据').hide();
					}else{
						$('#resultTip').text('没有更多数据').show();
					}
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}


function makeCommentListDom(commentList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:commentList});
	if(isAppend){
		$('#commentList').prepend(rendered);
	}else{
		$('#commentList').html(rendered);
	}
}

function alertauto(){
    $('.zhezhao,#pfreturnforum').slideToggle(0);
}

function reply(id,parentId,receiverId,receiverName){
    //$('.zhezhao').slideToggle(0);
    $('#dialog').show();
    $("#objectId").val(id);
    $("#parentId").val(parentId);
    $("#receiverId").val(receiverId);
    $("#receiverName").val(receiverName);
    $("#tips").text('');
}

function cancel(){
	//$('.zhezhao').slideToggle(0);
	$('#dialog').hide();
}

function hideShare(){
	$('.zhezhao,#share').slideToggle(0);
}

function addComment(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var content = $("#content").val();
	var objectId = $("#objectId").val();
	var parentId = $("#parentId").val();
	var receiverId = $("#receiverId").val();
	var receiverName = $("#receiverName").val();
	if(!content){
		lh.alert('请输入评论的内容');
		SAVING_FLAG = false;
		return ;
	}
	var obj = {};
	obj.content = content;
	obj.commentTypeId = 2;
	obj.objectId = objectId;
	obj.parentId = parentId;
	obj.receiverId = receiverId;
	obj.receiverName = receiverName;
	obj.comment = 'yes';
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateComment',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				reply();
				location.reload();
			}else{
				if(rsp.noPhone == 'noPhone'){
					lh.alert(rsp.msg,"jumpToBindUserPhone()");
				}else{
					lh.alert(rsp.msg);
				}
			}
		}
	},'json');
}

function myForumArticleCollect(forumId,forumArticleId){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var obj = {};
	obj.forumId = forumId;
	obj.articleId = forumArticleId;
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateForumArticleCollect',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				lh.alert('已收藏该帖子');
				setTimeout(function(){
					window.location.reload();
				},1000);
			}else{
				lh.alert(rsp.msg);
			}
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}

function cancelForumArticleCollect(id){
	$.post('/deleteForumArticleCollectThorough',{ids:id},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				lh.alert(rsp.msg);
				window.location.reload();
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

