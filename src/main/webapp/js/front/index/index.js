/**初始化数据*/
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 4;//取多少条数据

//------
var fromUrl = localStorage.getItem("fromUrl");//点击进入公众号的链接地址
var haveJumped = localStorage.getItem("haveJumped");
if(fromUrl && !haveJumped){
	localStorage.removeItem("fromUrl");
	localStorage.setItem('haveJumped', 1);
	var currentUrl = location.href;
	var idx = currentUrl.indexOf('?');
	if(idx>5){
		currentUrl = currentUrl.substring(0,idx);
	}
	var fromUrlBase = fromUrl;
	var idx2 = fromUrlBase.indexOf('?');
	if(idx2>5){
		fromUrlBase = fromUrlBase.substring(0,idx2);
	}
	//lh.alert("currentUrl:"+currentUrl+"---fromUrlBase:"+fromUrlBase);
	if(currentUrl != fromUrlBase)window.location.href = fromUrl;//TODO 后期取消注释
}
//------

$(function(){
	initPage();
	//loadUser();
	//loadCollectGoods();
	loadCollectGoods();
//	lh.scrollBottom(loadCollectGoods);
	rollingLoad();
});

function tests(){
	$('#tests').animates('flash');//bounce,
}

function initPage(){
	/*$('#banner').show();
	initMenu();
	initTopSearch();
	initBanner();
	switchBottomMenu('menu_index');*/
	
	$.mCustomScrollbar.defaults.theme = "light-2"; //set "light-2" as the default theme
	$("#ho1,#ho2").mCustomScrollbar({
		axis : "x",
		advanced : {
			autoExpandHorizontalScroll : true
		}
	});
	
}

/**加载藏品*/
function loadCollectGoods(){
	var param = {};
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
/*	param.forAuction = 1;
	param.orderBy = "g.bonus_type_id DESC,go.offer_price DESC,g.created_at";
	param.ascOrdesc = "DESC";
	param.orderSellerPrice = 1;*/
	$.post('/getMyAuctionMicroList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length > 0){
					makeCollectGoodsListDom(data,true);
					CURRENT_PAGE++;
				}else{
					$(".weui-infinite-scroll").text('没有更多数据');
					if(CURRENT_PAGE == 0){
						$('#noCollectGoods').show();
					}else{
						$('#noCollectGoods').hide();
						$('#resultTip').text('没有更多数据').show();
					}
				}
			}else{
				$('#resultTip').text(rsp.msg).show();
			}
		}
	},'json');
}

function countDown(auctionId, seconds, hour_elem, minute_elem, second_elem) {
    var timer = setInterval(function () {
        if (seconds > 0) {
            seconds --;
            /*var day = Math.floor((sys_second / 3600) / 24);*/
            var hour = Math.floor(seconds / 3600);
            var minute = Math.floor((seconds / 60) % 60);
            var second = Math.floor(seconds % 60);
            $(hour_elem).text(hour < 10 ? "0" + hour : hour);//计算小时
            $(minute_elem).text(minute < 10 ? "0" + minute : minute);//计算分
            $(second_elem).text(second < 10 ? "0" + second : second);// 计算秒
        } else {
            clearInterval(timer);
//        	auctionOver(auctionId);
        }
    if(seconds <= 0){
    	$('#am_'+auctionId).remove();//时间结束时移除
    }
    }, 1000);
}


function makeCollectGoodsListDom(collectGoodsList,isAppend){
	var template = $('#template').html();
	for(var i = 0; i<collectGoodsList.length; i++){
		var am = collectGoodsList[i];
		var auctionId = am.id;
		countDown(auctionId, am.remainSeconds, '#ctdown_hour_'+auctionId, '#ctdown_minute_'+auctionId, '#ctdown_second_'+auctionId);
	}
	
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, 
		{rows:collectGoodsList
		/*date:function(){
			var createdAt = this.endTime;
			if(createdAt){
				createdAt = moment(createdAt).format('MM月DD日 HH:mm');
			}
			return createdAt;
		}*/}
		
		);
	if(isAppend){
		$('#data-container').append(rendered);
	}else{
		$('#data-container').html(rendered);
	}
}
function checkDom(){
	var domNum = $(".mt6").size();
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<3){
		$(".weui-infinite-scroll").text('没有更多数据');
	}
}
//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadCollectGoods();
	  	isFirst = true;
	    loading = false;
	  }, 100);   //模拟延迟
});
}

function jumpToAM(){
	var indexSearch = $("#indexSearch").val();
	lh.jumpR("/am?goodsName="+indexSearch);
}
