
$(function(){
	initPage();
});

function initPage(){
	var param = lh.param;
}

function togglePayPass(){
	var payWay = $("input[name='checkbox1']:checked").val();  
	if(!payWay){
	}else if(payWay == 'payWay_pay'){
		$('#payPassDiv').hide();
	}else if(payWay == 'payWay_account'){
		$('#payPassDiv').show();
	}else{
		$('#payPassDiv').hide();
	}
}

function payGoodsMoney(){
	var payWay = $("input[name='checkbox1']:checked").val();  
	if(!payWay){
		lh.alert('请选择支付方式');
	}else if(payWay == 'payWay_pay'){
		jumpToPay();
	}else if(payWay == 'payWay_account'){
		$('#payPassDiv').show();
		payGoodsMoneyByAccount();
	}else{
		lh.alert('请选择正确的支付方式');
	}
}

function payGoodsMoneyByAccount(){
	if(lh.param.moneyLack){
		lh.alert('您的可用余额不足，无法使用余额支付');return;
	}
	var payPass = $("#payPass").val();  
	if(!payPass){
		lh.alert('请输入您的支付密码');return;
	}
	if(!lh.param.leftMoney){
		lh.alert('无法确定您支付的金额，无法完成支付');return;
	}
	lh.loading('正在支付...');
	lh.post('front', '/payMoneyForOrderGoods', {orderGoodsId:lh.param.orderGoodsId, payPass:payPass}, function(rsp){
		if(rsp.success){
			lh.alert('您已成功付款，卖家即将开始发货，请注意查收', '提示', jumpToOrderInfoManage);
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}

function jumpToPay(){
	//money, payType, fromUrl
	var fromUrl = '/orderInfoManage';
	var param = {orderGoodsId:lh.param.orderGoodsId, payType:2, fromUrl:fromUrl};
	localStorage.removeItem("fromUrl");
	redirectToPay(param);
}

function jumpToPaypass(){
	lh.jumpR('/payPasswordSet');
}

function jumpToCharge(){
	lh.jumpR('/fund/charge/recharge');
}
function jumpToOrderInfoManage(){
	lh.jumpR('/orderInfoManage');
}

