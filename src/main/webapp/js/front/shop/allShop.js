CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadallShopList();
	lh.scrollBottom(loadallShopList);
});

function loadallShopList(){
	var param = {rows:PAGE_COUNT,page:CURRENT_PAGE,allShop:1};
	$.post('/getShopList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count =  rsp.total;
				if( count && count > 0){
					makeAllShopListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}


function makeAllShopListDom(shopList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:shopList});
	if(isAppend){
		$('#shopList').append(rendered);
	}else{
		$('#shopList').html(rendered);
	}
}

function myShop(){
	var userId = $("#userId").val();
	if(userId){
		var url = "/myShop";
		var r = $("#r").val();
		if(r) url += "?r="+r;
		window.location.href = url;
	}else{
		window.location.href="/login"
	}
}



