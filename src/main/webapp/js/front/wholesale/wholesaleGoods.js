/**初始化数据*/
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadShopGoods();
	lh.scrollBottom(loadShopGoods);
});

/**加载藏品*/
function loadShopGoods(){
	var shopId = $("#shopId").val();
	var userId = $("#userId").val();
	var param = {};
	//param.userId = userId;
	param.shopId = shopId;
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	//param.shopId = shopId;
	//param.currentUserId = userId;
	//param.wholesaleId = 1;
	//param.noCopy = 1;
	//param.noCopyShopId = shopId;
	param.allWholesaleGoods = 1;
	param.moduleId = 5;
	$.post('/getGoodsList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count =  rsp.total;
				if(count && count > 0){
					makeShopGoodsListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeShopGoodsListDom(shopGoodsList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	for(var a in shopGoodsList){
		if(shopGoodsList[a].goodsPrivilege){
			if(shopGoodsList[a].goodsPrivilege == 1){
				shopGoodsList[a].goodsPrivilege = shopGoodsList[a].goodsPrivilege;
			}else if(shopGoodsList[a].goodsPrivilege == 2){
				shopGoodsList[a].goodsPrivilege = "";
			}
		}
		if(shopGoodsList[a].moduleId == 5){
			shopGoodsList[a].moduleId = shopGoodsList[a].moduleId;
		}else{
			shopGoodsList[a].moduleId = "";
		}
	}
	var rendered = Mustache.render(template, {rows:shopGoodsList});
	if(isAppend){
		$('#wholesaleSale').append(rendered);
	}else{
		$('#wholesaleSale').html(rendered);
	}
}

function copyGoods(id){
	$("#goodsId").val(id);
	var mask = $('#myMask');
    var weuiActionsheet = $('#weui_actionsheet');
    weuiActionsheet.addClass('weui_actionsheet_toggle');
    mask.show().addClass('weui_fade_toggle').click(function () {
        hideActionSheet(weuiActionsheet, mask);
    });
    $('#actionsheet_cancel').click(function () {
        hideActionSheet(weuiActionsheet, mask);
    });
    weuiActionsheet.unbind('transitionend').unbind('webkitTransitionEnd');

    function hideActionSheet(weuiActionsheet, mask) {
        weuiActionsheet.removeClass('weui_actionsheet_toggle');
        mask.removeClass('weui_fade_toggle');
        weuiActionsheet.on('transitionend', function () {
            mask.hide();
        }).on('webkitTransitionEnd', function () {
            mask.hide();
        })
        $("#goodsId").val('');
    }
}

function towholesale(){
	var goodsId = $("#goodsId").val();
	$.post('/copyGoods',{id:goodsId,to:5},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				copyGoods(null);
				lh.alert(rsp.msg);
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function toShop(){
	var goodsId = $("#goodsId").val();
	$.post('/copyGoods',{id:goodsId,to:4},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				copyGoods(null);
				lh.alert(rsp.msg);
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

