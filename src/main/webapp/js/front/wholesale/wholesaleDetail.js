//TOP_PRICE = null;
//AUTO_OFFER_ID = null;
//NOTICE_ID = null;
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
SAVING_FLAG = false;
MY_SWIPER = null;
$(function(){
	initBanner();//common.js
	initData();
	lh.scrollBottom(initData);
});

/** 初始化数据 - 是否开拍提醒，是否委托出价 */
function initData(){
	 $('#bannerUl').show();//先隐藏图片，页面初始化完成后再加载图片，避免加载时出现图片跳动
	 MY_SWIPER = new Swiper ('.swiper-container', {
	    direction: 'horizontal',
	    loop: true,
	    // 如果需要分页器
	    pagination: '.swiper-pagination',
	    // 如果需要前进后退按钮
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    // 如果需要滚动条
	    scrollbar: '.swiper-scrollbar'
	  })        
	
	var paramObj = {}
	var wholesaleId = $("#wholesaleId").val();
	paramObj.rows = PAGE_COUNT;
	paramObj.page = CURRENT_PAGE;
	paramObj.moduleId = 5;
	paramObj.wholesaleId = wholesaleId;
	paramObj.allWholesaleGoods = 1;
	$.post('/getGoodsList',paramObj,function(rsp){
		if(rsp){
			if(rsp.success){
				 var count =  rsp.total;
				if( count && count > 0){
					makeCommodityGoodsListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
	
}

function makeCommodityGoodsListDom(GoodsList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:GoodsList});
	if(isAppend){
		$('#wholesaleList').append(rendered);
	}else{
		$('#wholesaleList').html(rendered);
	}
}

function addGoodsComment(objectId){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var content = $("#content").val();
	if(!content){
		lh.alert('请填写评论内容');
		SAVING_FLAG = false;
		return ;
	}
	var obj = {};
	obj.objectId = objectId;
	obj.content = content;
	obj.commentTypeId = 3;
	obj.comment = 'yes';
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateComment',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);
			if(rsp.status == 'success'){
				window.location.reload();
			}else{
				if(rsp.noPhone == 'noPhone'){
					lh.alert(rsp.msg,"jumpToBindUserPhone()");
				}else{
					lh.alert(rsp.msg);
				}
			}
		}
	},'json');
}

function addCart(goodsId,shopId){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var Obj = {goodsId:goodsId,shopId:shopId};
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateCart',Obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);
			if(rsp.status == 'success'){
				lh.alert("已加入购物车");
			}else{
				lh.alert(rsp.msg);
			}	
		}
	},'json')
}
