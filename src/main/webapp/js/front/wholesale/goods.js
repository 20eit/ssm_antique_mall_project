/** 鬼市（逛逛）主JS  */
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	initPage();
	loadGoodsList();//加载店铺列表（包含每个店铺的部分商品）
	lh.scrollBottom(loadGoodsList);
});

function initPage(){
	initTopSearch();
	switchBottomMenu('menu_market');
}

function loadGoodsList(){
	var param = {start:CURRENT_PAGE, count:PAGE_COUNT};
	var searchName = $("#searchName").val();
	if(searchName)param.nameLike = searchName;
	param.withGoods = 1;
	param.page = CURRENT_PAGE;
	param.rows = PAGE_COUNT;
	//param.noCopy = 1;
	param.moduleId = 5;
	param.allWholesaleGoods = 1;
	//param.statusIn = 1;
	//param.status = 79;
	//param.wholesale = 1;
	/*param.shopId = shopId;
	param.currentUserId = userId;
	param.wholesaleId = 1;
	param.noCopy = 1;
	param.noCopyShopId = shopId;
	param.moduleId = moduleId;*/
	$.post('/getGoodsList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count =  rsp.total;
				if(count && count  > 0){
					makeGoodsListDom(rsp.rows,1);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeGoodsListDom(goodsList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:goodsList});
	if(isAppend){
		$('#shopGoods').append(rendered);
	}else{
		$('#shopGoods').html(rendered);
	}
}

function search(loc){
	if(!loc)loc == 1;
	var searchName = null;
	if(loc == 1){//页面顶部搜索栏
		searchName = $('#ss1_input').val();
	}else if(loc == 2){//页面侧面滑动显示搜索栏
		searchName = $('#ss2_input').val();
	}
	var url = '';
	if(SEARCH_TYPE == 1){
		url = '/wsGoods';
	}else if(SEARCH_TYPE == 2){
		url = '/wholesale';
	}
	if(searchName){
		url += '?searchName='+searchName;
		if(SEARCH_GOODS_TYPE)url += '&searchType='+SEARCH_GOODS_TYPE;
	}else{
		if(SEARCH_GOODS_TYPE)url += '?searchType='+SEARCH_GOODS_TYPE;
	}
	location.href = url;
}

/*
function search(){
	var ss1_input = $('#ss1_input').val();
	var searchType = SEARCH_TYPE;//搜索类型，1：藏品，2：店铺
	if(searchType == 2){
		if(ss1_input){
			window.location.href="/market?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/market?searchType="+searchType;
		}
	}else{
		if(ss1_input){
			window.location.href="/goods?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/goods?searchType="+searchType;
		}
	}
}
*/
function jumpToShop(){
	var url = "/shop";
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}