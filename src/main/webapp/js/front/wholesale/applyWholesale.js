SAVING_FLAG = false;
OBJ = null;
/**初始化*/
$(function(){
	initUploadSimple();//初始化上传组件
})

function addApplyWholesale(){
	//var title = $("#title").val();
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var name =  $("#name").val();
	var message =  $("#message").val();
	var filePaths =  $("#filePaths").val();
	var fileDBIds =  $("#fileDBIds").val();
	var obj = {};
	if(!name){
		lh.alert('请填写批发城名称');SAVING_FLAG = false;return ;
	}
	if(!filePaths){
		lh.alert('请上传批发城logo');SAVING_FLAG = false;return ;
	}
	filePaths = filePaths.substring(1);
	var filePathArr = new Array();
	if(filePaths.indexOf(',') >= 0){
		filePaths = filePaths.substring(1);
	}
	if(filePathArr.length >= 2){
		lh.alert('只能上传1张logo图片');
		SAVING_FLAG = false;
		return;
	}
	obj.file1 = filePaths;
	obj.message = message;
	obj.attr1 = name;
	obj.applyType = '94';
	OBJ = obj;
	frontBaseConfirm('申请开通批发城需要交纳1000元保证金','showPayMoneyWin()');
	SAVING_FLAG = false;
}

function realPost(flag){
	if(flag == 'cancel'){
		$("#offerWin").hide();
	}else{
		var payPass = $("#payPass").val();
		if(!payPass){
			lh.alert('请输入支付密码');
			SAVING_FLAG = false;
			return;
		}
		OBJ.payPass = payPass;
		frontBaseLoadingOpen('正在保存数据...');//加载遮罩
		$.post('/addOrUpdateApply',OBJ,function(rsp){
			SAVING_FLAG = false;
			frontBaseLoadingClose();//解除遮罩
			$("#offerWin").hide();
			if(rsp){
				frontLoginCheck(rsp);//登陆检查
				if(rsp.status == 'success'){
					lh.alert('你的申请已经提交,管理员审核通过后即可使用.','goWholesale()');
					//window.location.href="/wholesale";
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}
}

function showPayMoneyWin(){
	$("#offerWin").show();
}

function goWholesale(){
	var url = '/wholesale';
	var r = $("#r").val();
	if(r) url += "?r="+r;
	location.href = url;
}

