/** 批发城JS  */
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	initPage();
	loadWholesaleList();//加载店铺列表（包含每个店铺的部分商品）
	lh.scrollBottom(loadWholesaleList);
});

function initPage(){
	initTopSearch();
	switchBottomMenu('menu_market');
}

function loadWholesaleList(){
	var param = {page:CURRENT_PAGE, rows:PAGE_COUNT,moduleId:5,allWholesaleGoods:1};
	var searchName = $("#searchName").val();
	if(searchName)param.nameLike = searchName;
	$.post('/getWholesaleList',param,function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				//var wholesaleList = rsp.wholesaleList;
				var count =  rsp.total;
				if( count && count > 0){
					makeWholesaleListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeWholesaleListDom(wholesaleList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var data = {
		rows:wholesaleList,
		picDom:function(){
			var picPathList = this.picPathList;
			var picDom = '';
			if(picPathList.length > 0){
				for(var i=0; i<picPathList.length; i++){
					var picPath = picPathList[i];
					if(!picPath)continue;
					picPath = picPath.replace(',','');
				   picDom += 
				   '<div class="div_gg fl">'
						  +'<a href="javascript:void(0);"><img src="'+picPath+'" style="max-height:60px;margin:5px;"/></a>'
				  +'</div>'
				}
			}
			return picDom;
		}
   }
	var rendered = Mustache.render(template, data);
	
	if(isAppend){
		$('#wholesaleItems').append(rendered);
	}else{
		$('#wholesaleItems').html(rendered);
	}
}

function search(loc){
	if(!loc)loc == 1;
	var searchName = null;
	if(loc == 1){//页面顶部搜索栏
		searchName = $('#ss1_input').val();
	}else if(loc == 2){//页面侧面滑动显示搜索栏
		searchName = $('#ss2_input').val();
	}
	var url = '';
	if(SEARCH_TYPE == 1){
		url = '/wsGoods';
	}else if(SEARCH_TYPE == 2){
		url = '/wholesale';
	}
	if(searchName){
		url += '?searchName='+searchName;
		if(SEARCH_GOODS_TYPE)url += '&searchType='+SEARCH_GOODS_TYPE;
	}else{
		if(SEARCH_GOODS_TYPE)url += '?searchType='+SEARCH_GOODS_TYPE;
	}
	location.href = url;
	//loadWholesaleList();
}

/*
function search(){
	var ss1_input = $('#ss1_input').val();
	var searchType = SEARCH_TYPE;//搜索类型，1：藏品，2：店铺
	if(searchType == 1){
		if(ss1_input){
			window.location.href="/goods?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/goods?searchType="+searchType;
		}
	}else{
		if(ss1_input){
			window.location.href="/market?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/market?searchType="+searchType;
		}
	}
}
*/
function jumpToWholesale(){
	var url = "/wholesale";
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}