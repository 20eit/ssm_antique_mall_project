CURRENT_PAGE = 1;
LOADING_FLAG = false;//正在加载标识
CURRENT_ROW_INDEX = 0;
$(function(){
	loadMyCustomer();
	lh.scrollBottom(loadMyCustomer);//下拉加载更多数据
});

function loadMyCustomer(){
	if(LOADING_FLAG)return;
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {page:CURRENT_PAGE,rows:20};
	LOADING_FLAG = true;//正在加载
	$.post('/getMyCustomerList',param,function(rsp){
		LOADING_FLAG = false;//取消正在加载
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				var total = rsp.total;
				if(total){
					$('#customerCount').text(total);
				}
				if(data && data.length>0){
					makeAuctionInstDom(data,true);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg).show();
			}
		}
	},'json');
}

function makeAuctionInstDom(dataList,isAppend){
	var template = $('#template').html();
	//if(CURRENT_PAGE == 1)isAppend = false;
	Mustache.parse(template);   // optional, speeds up future uses
	for(var i = 0; i<dataList.length; i++){
		var d = dataList[i];
		var idx = ++CURRENT_ROW_INDEX;
		if(idx < 10)idx = '0'+idx;
		d.index = idx;
		d.regDate = formatDate(d.createdAt,1);
	}
	var data = {
			rows:dataList
	   }
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#customerUl').append(rendered);
	}else{
		$('#customerUl').html(rendered);
	}
}

function toggleTip(opt){
	$('#divInst').toggle();
	
}

