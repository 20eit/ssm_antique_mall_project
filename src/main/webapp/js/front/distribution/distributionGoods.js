CURRENT_PAGE = 1;
LOADING_FLAG = false;//正在加载标识
SAVING_FLAG = false;//正在保存标识
CURRENT_ROW_INDEX = 0;
GOODS_TYPE_ID = null;
$(function(){
	initEvent();
	loadDistributionGoods();
	lh.scrollBottom(loadDistributionGoods);//下拉加载更多数据
});

function initEvent(){
	$('#firstpane p').click(function(){
		var $p = $(this);
		var typeName = $p.text();
		if(typeName == '关闭'){
			$('.navcdbox').hide();return;
		}
		$('#goodsTypes').text('藏品分类：'+typeName+' ∨');
		$('#firstpane p').css('color','black');
		$p.css('color','#D81717');
		var typeId = $p.attr('typeId');
		if(typeId){
			GOODS_TYPE_ID = typeId;
		}else{
			GOODS_TYPE_ID = null;
		}
		loadDistributionGoods(true);
		$('.navcdbox').hide();
	});
}

function loadDistributionGoods(isNotAppend){
	if(LOADING_FLAG)return;
	$('#resultTip').hide();
	$('#loadingTip').show();
	if(isNotAppend){
		CURRENT_PAGE = 1;
	}
	var param = {page:CURRENT_PAGE,rows:20};
	if(GOODS_TYPE_ID)param.catId = GOODS_TYPE_ID;
	LOADING_FLAG = true;//正在加载
	$.post('/getDistGoodsList',param,function(rsp){
		LOADING_FLAG = false;//取消正在加载
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeDistributionGoodsDom(data, isNotAppend);
					CURRENT_PAGE ++;
				}else{
					if(isNotAppend){
						$('#leftGoodsList,#rightGoodsList').empty();
					}
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg).show();
			}
		}
	},'json');
}

function makeDistributionGoodsDom(dataList, isNotAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var dataList1 = [];
	var dataList2 = [];
	var which = 1;
	for(var i = 0; i<dataList.length; i++){
		var d = dataList[i];
		if(which == 1){
			dataList1.push(d);
			which = 2;
		}else{
			dataList2.push(d);
			which = 1;
		}
		var price = d.shopPrice;
		if(!price || price <= 0){
			price = '议价';
		}else{
			price = '￥ '+price;
		}
		d.shopPrice = price;
		var bonusType = d.bonusTypeId;
		if(bonusType == 1){
			d.spreadPackets = ' ￥'+d.bonus;
		}else if(bonusType == 2){
			d.spreadPackets = ' '+d.bonus+'%';
		}else{
			continue;
		}
	}
	var rendered1 = Mustache.render(template, {rows:dataList1});
	var rendered2 = Mustache.render(template, {rows:dataList2});
	if(isNotAppend){
		$('#leftGoodsList').html(rendered1);
		$('#rightGoodsList').html(rendered2);
	}else{
		$('#leftGoodsList').append(rendered1);
		$('#rightGoodsList').append(rendered2);
	}
}

function selectGoods(goodsId){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var $goodsCheck = $('#goodsId_'+goodsId);
	if($goodsCheck.hasClass('tarzan3')){//选择
		$.post('/shareGoods', {goodsId:goodsId}, function(rsp){//同步数据
			SAVING_FLAG = false;
			if(rsp){
				if(rsp.success){
					$goodsCheck.removeClass('tarzan3').addClass('tarzan4');
					lh.alert('已成功将该藏品上架到您的店铺');
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
		
	}else{//取消选择
		$.post('/cancelShareGoods', {goodsId:goodsId}, function(rsp){//同步数据
			SAVING_FLAG = false;
			if(rsp){
				if(rsp.success){
					$goodsCheck.removeClass('tarzan4').addClass('tarzan3');
					lh.alert('已成功将该藏品从您的店铺下架');
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
		
	}
}

function selectGoodsType(){
	
}

