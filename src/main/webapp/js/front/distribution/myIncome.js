CURRENT_PAGE = 1;
LOADING_FLAG = false;//正在加载标识
CURRENT_ROW_INDEX = 0;
$(function(){
	loadMyIncome();
	lh.scrollBottom(loadMyIncome);//下拉加载更多数据
	/*$("#ddlDay").select2({
	    tags: "true",
	    placeholder: "",
	    allowClear: true
	 });*/
	$("#ddlDay").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    size: 10
	});
});

function loadMyIncome(isNotAppend){
	if(LOADING_FLAG)return;
	$('#resultTip').hide();
	$('#loadingTip').show();
	if(isNotAppend){
		CURRENT_PAGE = 1;
		var tbody = 
			'<tr class="myzttabt" style="border-bottom: 1px solid #F1EEEE;">'+
				'<td class="myzttabtd1" style="width: 18%;text-align: center;">类型</td>'+
				'<td class="myzttabtd2"style="width: 24%;">对象</td>'+
				'<td class="myzttabtd2"style="width: 18%;">金额</td>'+
				'<td class="myzttabtd3"style="width: 20%;">日期</td>'+
				'<td class="myzttabtd4"style="width: 15%;">状态</td>'+
			'</tr>';
		$('#incomeTable').html(tbody);
	}
	var param = {page:CURRENT_PAGE,rows:20};
	var timeVal = $('#ddlDay').val();
	if(timeVal)param[timeVal] = 1;
	LOADING_FLAG = true;//正在加载
	$.post('/getMyIncomeList',param,function(rsp){
		LOADING_FLAG = false;//取消正在加载
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				var total = rsp.total;
				if(total){
					$('#customerCount').text(total);
				}
				if(data && data.length>0){
					makeMyIncomeDom(data, isNotAppend);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg).show();
			}
		}
	},'json');
}

function makeMyIncomeDom(dataList, isNotAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	for(var i = 0; i<dataList.length; i++){
		var d = dataList[i];
		var idx = ++CURRENT_ROW_INDEX;
		if(idx < 10)idx = '0'+idx;
		d.index = idx;
		d.dealTime = formatDate(d.dealTime,false);
		var status = d.mainStatus;
		d.statusName = '已到账';
		//if(status==2){}其他状态
		var name = d.objectName;
		if(name && name.length>6)name = name.substring(0,5)+'...';
		d.objectName = name;
	}
	var data = {
			rows:dataList
	   }
	var rendered = Mustache.render(template, data);
	if(isNotAppend){
		$('#incomeTable').append(rendered);
	}else{
		$('#incomeTable').append(rendered);
	}
}

function toggleTip(opt){
	$('#divInst').toggle();
	
}

