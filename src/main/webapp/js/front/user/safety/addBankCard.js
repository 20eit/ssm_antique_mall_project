$(function(){
	//getAllBankCard();
});

function checkBankCard(){
	var bankCardName = $("#bankCardName").val();
	var bankCardNum = $("#bankCardNum").val();
	if(!bankCardName){lh.alert("请输入持卡人姓名");return;}
	if(!bankCardNum){lh.alert("请输入卡号");return;}
	var param = {bankCardName:bankCardName,bankCardNum:bankCardNum};
	lh.post('front','/checkBankCard',param,function(rsp){
		if(rsp.success){
			if(rsp.msg == '银行卡添加成功'){
				lh.alert(rsp.msg,jumpTobankCardMsg);
			}else{
				lh.alert(rsp.msg);
			}
		}else{
			lh.alert(rsp.msg);
		}
	});
}
function jumpTobankCardMsg(){
	lh.jumpR('/bankCardManager');
}