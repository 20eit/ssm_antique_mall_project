$(function() {
	initPage();
	var userId = $("#fansId").val();
	var param = {userId : userId};
	loadgetGoodsMicro('/getGoodsMicro', param, 1);
	loadgetGoodsMicro('/getGoodsAuction', param, 2);
//	loadisFans(param);
});

//function loadisFans(param, temple) {
//	lh.post('front', '/isFans', param, function(rsp) {
//		if (rsp.success) {
//			var data = rsp.rows;
//				makeMainListDom(data, true, 3);
//		} else {
//			$('#resultTip').text(rsp.msg).show();
//			lh.alert(rsp.msg);
//		}
//	}, 'json');
//}
function loadgetGoodsMicro(url, param, temple) {
	lh.post('front', url, param, function(rsp) {
		if (rsp.success) {
			var data = rsp.rows;
				makeMainListDom(data, true, temple);
		} else {
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function makeMainListDom(mainList, isAppend, temple) {
	var template = '';
	if (temple == 1) {
		template = $('#template').html();
	} else if (temple == 2) {
		template = $('#template2').html();
	}
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
			rows : mainList,
			picsDom : function() {
				var picPaths = this.picPaths;
				if (!picPaths)
					return '';
				var pics = picPaths.split(',');
				var length = pics.length;
				// if(length > 9)length = 9;
				var picsDom = '';
				for (var i = 0; i < length; i++) {
					var pic = pics[i];
					pic += buildOSSZoom(100, 100); // @@_OSS_IMG_@@
					picsDom += '<img width="55" src="'+pic+'" class="mr7" />';
				}
				return picsDom;
			}
		});	
	if (temple == 1) {
		if (isAppend) {
			$('#data-container').append(rendered);
		} else {
			$('#data-container').html(rendered);
		}
	} else if (temple == 2) {
		if (isAppend) {
			$('#data-container2').append(rendered);
		} else {
			$('#data-container2').html(rendered);
		}
	} 
}
function initPage() {
	
	var isFans = $("#isFans").val();
	var fansId = $("#fansId").val();
	var param = {fansId:fansId};
	if(isFans == 1){
		$("#fansManager").text("取消关注");
		$("#fansManager").click(function(){
			cancleFans(param);
		});
	}else if(isFans == -1){
		$("#fansManager").text("不能关注你自己");
	}else{
		$("#fansManager").text("关注");
		$("#fansManager").click(function(){
			joinFans(param);
		});
	}
}
//取消关注
function cancleFans(param){
		lh.post('front', '/cancelFans', param, function(rsp) {
		if (rsp.success) {
			var data = rsp.rows;
			lh.alert(rsp.msg,lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}
//添加关注
function joinFans(param){
		lh.post('front', '/joinFans', param, function(rsp) {
		if (rsp.success) {
		    lh.alert(rsp.msg,lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}

