$(function(){
	initPage();
	
});
function initPage(){
	var isRealAuth = $("#isRealAuth").val();
	if(isRealAuth == "notRealAuth"){
		lh.alert("您没有通过实名认证，不能提现!",jumpToAuthen);
	}
}
function jumpToAuthen(){
	lh.jumpR("/user/safety/realNameAuthentication");
}
function selectAorO(){
	var code = $("#tixianFangShi  option:selected").val();
	var avaliableMoney = $("#avaliableMoney").val();
	var otherFund = $("#otherFund").val();
	if(code == 1){
		$("#withdrawDepositnumber").attr("placeholder","可提现的金额："+avaliableMoney+"元");
		$("#withdrawDepositnumber").val('');
	}else if(code == 2){
		$("#withdrawDepositnumber").attr("placeholder","可提现的金额："+otherFund+"元");
		$("#withdrawDepositnumber").val('');
	}
}
function shouxufei(){
	var withdrawDepositshouxu = $("#withdrawDepositnumber").val();
	$("#withdrawDepositshouxu").val(withdrawDepositshouxu*0.004);
}
function validate() {
	var withdrawDepositnumber = $("#withdrawDepositnumber").val();
	if (!withdrawDepositnumber) {
		lh.alert('请输入正确提现金额!');
		return;
	}
	if (parseFloat(withdrawDepositnumber) > parseFloat(5000)) {
		lh.alert('js:前台判断：单笔线上提现金额上限5000.00元');
		return;
	}
	if (parseFloat(withdrawDepositnumber) <= parseFloat(100)) {
		lh.alert('js:前台判断：提现金额不能小于100.00元');
		return;
	}
	var withdrawDepositshouxu = $("#withdrawDepositshouxu").val();
	if (!withdrawDepositshouxu) {
		lh.alert('警告：手续费异常!');
		return;
	}
	/*if (parseFloat(withdrawDepositshouxu) <= parseFloat(4)) {
		lh.alert('js:前台判断：异常提交手续费不能小于4.00元');
		return;
	}*/
	var bankNum = $("#bankNameAndNum  option:selected").val();
	var bankName = $("#bankNameAndNum  option:selected").text();
	var code = $("#tixianFangShi  option:selected").val();

	var applyrealMoney = withdrawDepositnumber - withdrawDepositshouxu;
	bankName = bankName.substring(0,bankName.length-6);
	var params = {typeId:code,applyMoney:withdrawDepositnumber,applyRealMoney:applyrealMoney,fee:withdrawDepositshouxu
					,applyMsg:"申请提现",attr1:bankName,attr2:bankNum};
	lh.post('front','/addWithdrawApply',params,function (rsp){
		if(rsp.success){
			lh.alert(rsp.msg,lh.back);
		}else{
			lh.alert(rsp.msg);
		}
	});
	
}