
$(function(){
	initPage();
	var userId = $("#userId").val();
	var param = {userId:userId,ing:"1",mainStatusd:"1"};
	loadmyAutionList(param,true,1);
});

function loadmyAutionList(param,isAppend,isEnd){
	lh.post('front', '/getMyAuctionMicroLists', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, isAppend,isEnd);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
				$(".weui-infinite-scroll").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function makeMainListDom(mainList, isAppend,isEnd) {
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var data = getData(mainList);
	var rendered = Mustache.render(template, data);
	if(isEnd == 2){
		if(isAppend){//第一页时为html
			$('#data-container').html(rendered);
		}else{
			$('#data-container').append(rendered);
		}
		$(".partake_bonus").empty();
	}else{
	if(isAppend){//第一页时为html
		$('#data-container').html(rendered);
	}else{
		$('#data-container').append(rendered);
	}
	}
	
}
function getData(auctionProfessionList){
	var data = {
		rows:auctionProfessionList,
		getPicPath:function(){
			var picPaths = this.picPaths;
			if(picPaths){
				picPaths += buildOSSZoom(420,150); //@@_OSS_IMG_@@ //330,120
			}
			return picPaths;
		},
		date:function(){
			var createdAt = this.endTime;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		},
		offerPrices:function(){
			var price = this.increaseRangePrice;
			var offerPrices = '';
			if(price <= 0){
				offerPrices = '<div class="col-xs-12 plr0 gray pt5">' +
				'<div class="col-xs-2 plr0 pt3 partakeSub"  onclick="subPrice('+0+','+this.id+');">' +
				'<img class="center-block" src="/images/front/partake_sub.png"></div>' +
				'<div class="col-xs-6 plr0 text-center partakePrice_'+this.id+'" id="newofferPrice_'+this.id+'">'+0+'</div>' +
				'<div class="col-xs-2 plr0 pt3 partakeAdd" onclick="addPrice('+0+','+this.id+');">' +
				'<img class="center-block" src="/images/front/partake_add.png"></div></div>';
			}else{
				offerPrices = '<div class="col-xs-12 plr0 gray pt5">' +
					'<div class="col-xs-2 plr0 pt3 partakeSub"  onclick="subPrice('+price+','+this.id+');">' +
					'<img class="center-block" src="/images/front/partake_sub.png"></div>' +
					'<div class="col-xs-6 plr0 text-center partakePrice_'+this.id+'" id="newofferPrice_'+this.id+'">'+this.offerPrice+'</div>' +
					'<div class="col-xs-2 plr0 pt3 partakeAdd" onclick="addPrice('+price+','+this.id+');">' +
					'<img class="center-block" src="/images/front/partake_add.png"></div></div>';
			}
			return offerPrices;
		}
    }
	return data;
}

function addMicroOffers(id,goodsId){
	var offerPrice = $('#offerPrice_'+id).val();
	var newofferPrice = $('#newofferPrice_'+id).html();
	if(parseFloat(newofferPrice)<=parseFloat(offerPrice)){
		lh.alert("您的出价小于当前最高价");	
	}else{
		var param = {auctionId:id,offerPrice:newofferPrice,goodsId:goodsId};
		lh.post('front','/addMicroOffers',param,function(rsp){
			if(rsp.success){
				if(rsp.msg == '竞拍成功'){
					lh.alert(rsp.msg,lh.reload);
				}else{
					lh.alert(rsp.msg);
				}
			}
		});
	}	
}
function deleteMicroOffers(id,goodsId){
	var offerPrice = $('#offerPrice_'+id).val();
	var newofferPrice = $('#newofferPrice_'+id).html();
		var param = {auctionId:id,offerPrice:newofferPrice,goodsId:goodsId,mainStatus:2};
		lh.post('front','/deleteMicroOffers',param,function(rsp){
			if(rsp.success){
				if(rsp.msg == '当前出价最高是您，不能出局'){
					lh.alert(rsp.msg);
				}else{
					lh.alert(rsp.msg,lh.reload);
				}
			}
		});
}

function addPrice(addNum,id){
//出价位置的加价按钮
     var price = $('.partakePrice_'+id).html();
   	 $('.partakePrice_'+id).html((Digit.round(price, 2)*1000000 + Digit.round(addNum, 2)*1000000)/1000000);
}
function subPrice(subNum,id){
     var price = $('.partakePrice_'+id).html();
     $('.partakePrice_'+id).html((Digit.round(price, 2)*1000000 - Digit.round(subNum, 2)*1000000)/1000000);
}

function initPage(){
	var userId = $("#userId").val();
	$("#isJoin").click(function (){
		$("#data-container").empty();
		var param = {userId:userId,ing:"1",mainStatusd:"1"};
		$("#isJoin").attr("class","active");
		$("#isEnd").attr("class","");	
		loadmyAutionList(param,true,1);
	});
	$("#isEnd").click(function (){
		$("#data-container").empty();
		var param = {userId:userId,ending:"1",mainStatusd:"1"};
		$("#isJoin").attr("class","");
		$("#isEnd").attr("class","active");
		loadmyAutionList(param,true,2);
	});
}

function showShare(){
	$('#shareMask,#share').show();
}
function hideShare(){
	$('#shareMask,#share').hide();
}

//保留两位小数
var Digit = {};
Digit.round = function(digit, length) {
    length = length ? parseInt(length) : 0;
    if (length <= 0) return Math.round(digit);
    digit = Math.round(digit * Math.pow(10, length)) / Math.pow(10, length);
    return digit;
};