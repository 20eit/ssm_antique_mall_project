var URL = '';
$(function(){	
	init();
});

function init(){
	/*var noPhone = $("#noPhone").val();
	if(noPhone){
		frontBaseConfirm("您还未绑定手机号码，是否前往绑定手机号码","jumpToBindUserPhone()",null);
	}
	var noEditPassword = $("#noEditPassword").val();
	if(noEditPassword){
		frontBaseConfirm("您还未修改默认密码，是否前往修改默认密码","jumpToEditPasswd()",null);
	}
	var noPayPassword = $("#noPayPassword").val();
	if(noPayPassword){
		frontBaseConfirm("您还未设置支付密码，是否前往设置支付密码","jumpToPayPasswordFind()",null);
	}*/
	/*setTimeout(function(){
		loadNotice(null,renderNoticeCount);
	},1000);*/
	loadOrderInterval();
}


function loadOrderInterval(timeGap, func){
	getOrderCount();//加载完页面时先加载一次再开始计时
	if(!timeGap)timeGap = 120000;//默认120秒
	 var timer = setInterval(function () {
		 getOrderCount(timeGap, func);
    }, timeGap);//默认120秒
}


function getOrderCount(timeGap, func){
	lh.post('front', '/getOrderCount',null,function(rsp){
		if(rsp.success){
			var returnGoodsCount = rsp.returnGoodsCount;
			var shippingCount = rsp.shippingCount;
			var shipedCount = rsp.shipedCount;
			var waitPayMoneyCount = rsp.waitPayMoneyCount;
			//var goodsOffersCount = rsp.goodsOffersCount;
			//var commentingCount = rsp.commentingCount;
			if(returnGoodsCount > 0){
				//$("#returnGoodsCount").text(returnGoodsCount);
				$("#order_returnGoodsCount").show();
			}
			if(shippingCount > 0){
				//$("#shippingCount").text(shippingCount);
				$("#order_shippingCount").show();
			}
			if(shipedCount > 0){
				//$("#shipedCount").text(shipedCount);
				$("#order_shipedCount").show();
			}
			if(waitPayMoneyCount > 0){
				//$("#waitPayMoneyCount").text(waitPayMoneyCount);
				$("#order_waitPayMoneyCount").show();
			}
			/*
			 if(goodsOffersCount > 0){
				$("#goodsOffersCount").text(goodsOffersCount);
				$("#goodsOffersCount").show();
			 }
			 if(commentingCount > 0){
				$("#commentingCount").text(commentingCount);
				$("#commentingCount").show();
			}*/
		}
	},'json');
}

function jumpToOrderList(status){
	localStorage.setItem('orderInfoManage_orderStatus', status);
	lh.jumpR('/orderInfoManage');
}

function logout(){
	$.post('/logout',function(rsp){
		if(rsp.status == 'success'){
			localStorage.clear();
			window.location.href='/login';
		}else{
			lh.alert(rsp.msg);
		}
	});
}

function renderNoticeCount(count){
	$('#noticeCt_userCtr').text(count);
}

function jumpToRecharge(){
	redirectToCharge();
}
