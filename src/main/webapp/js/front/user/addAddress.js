SAVING_FLAG = false;
$(function(){
	getUserAddress();
	init();
});
function init(){
	var userAddressProvince = $("#userAddressProvince").val();
	$.post('/getProvince',function(rsp){
		if(rsp){
			var dom = '<select class="form-control bdn" id="province"><option value="">请选择</option>';
			$("#provinceDiv").empty();
			for(var i = 0;i < rsp.length;i++){
				dom +=' <option';
				dom += ' value="'+rsp[i].id+'">'+rsp[i].name+'</option>';
				if(userAddressProvince == rsp[i].id){
					getCity(rsp[i].id);
					dom +=' <option';
				dom += ' value="'+rsp[i].id+'" selected="selected">'+rsp[i].name+'</option>';
				}
			}
			dom += '</select>';
			$("#provinceDiv").append(dom);
//			$("#province").selectpicker({
//				title:'',
//			    showTick:true,
//			    size: 30
//			});
//			$('#province').on('changed.bs.select', function (e) {
//				getCity($("#province").val());
//			});
			$('#province').change(function(){
				getCity($("#province").val());
			});
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}


function getUserAddress(){
	var userId = $("#userId").val();
	var param = {userId:userId};
		$.post('/getUserAddressList',param,function(rsp){
			frontLoginCheck(rsp);
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
//					makeMainListDom(data, true);
				}else{
					$('#resultTip').text('没有更多数据').show();
					$(".weui-infinite-scroll").text('没有更多数据');
				}
			}else{
				lh.page.currentPage = 1;
				$('#resultTip').text(rsp.msg).show();
				lh.alert(rsp.msg);
			}
		}, 'json');
	
}

function getCity(city){
	var userAddressCity = $("#userAddressCity").val();
	//var provinceId = $("#province").val();
	$.post('/getCity',{provinceId:city},function(rsp){
		if(rsp){
			var dom = '<select id="city"" class="form-control bdn" ><option value="">请选择</option>';
			$("#cityDiv").empty();
			for(var i = 0;i < rsp.length;i++){
				dom +=' <option';
				if(userAddressCity == rsp[i].id)dom += ' selected="selected" ';
				dom += ' value="'+rsp[i].id+'">'+rsp[i].name+'</option>';
			}
			dom += '</select>';
			$("#cityDiv").append(dom);
//			$("#city").selectpicker({
//				title:'',
//			    showTick:true,
//			    size: 30
//			});
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}
//function makeMainListDom(mainList, isAppend) {
//	var template = $('#template').html();
//	Mustache.parse(template); // optional, speeds up future uses
//	var rendered = Mustache.render(template, {
//		rows : mainList,
//		picsDom:function(){
//			var picPaths = this.picPaths;
//			if(!picPaths)return '';
//			var pics = picPaths.split(',');
//			var length = pics.length;
//			//if(length > 9)length = 9;
//			var picsDom = '';
//			for(var i=0;i<length;i++){
//				var pic = pics[i];
//				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
//				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
//			}
//			return picsDom;
//		},
//		date:function(){
//			var createdAt = this.createdAt;
//			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
//			return createdAt;
//		},
//		isDefult:function(){
//			var isDefult = '';
//			var defult = this.isDefault;
//			if(defult == 1){
//				isDefult = '<input type="radio" class="weui_check" onclick="setDefult('+this.id+')" checked="true" name="radio1" id="x12" >';
//			}else{
//				isDefult = '<input type="radio" class="weui_check" onclick="setDefult('+this.id+')" name="radio1" id="x12" >';
//			}
//			return isDefult;
//		}	
//	});
//	if (isAppend) {
//		$('#data-container').append(rendered);
//	} else {
//		$('#data-container').html(rendered);
//	}
//}
//设置默认地址
function setDefult(addressId){
	var userId = $("#userId").val();
	var param = {id:addressId,userId:userId};
	$.post('/setDefultAddress',param,function(rsp){
		if(rsp.success){
			
		}
	}, 'json');
}


function addUserAddress(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var userId = $("#userId").val();
	var userAddressId = $("#userAddressId").val();
	var receiverName = $("#receiverName").val();
	var addressDetail = $("#addressDetail").val();
	var phone = $("#phone").val();
	var province = $("#province").val();
	var city = $("#city").val();
	if(!receiverName){lh.alert('请输入收货人的姓名');SAVING_FLAG = false;return;}
	if(!province){lh.alert('请输入收货人所在的省或直辖市');SAVING_FLAG = false;return;}
	if(!city){lh.alert('请输入收货人所在的城市');SAVING_FLAG = false;return;}
	if(!addressDetail){lh.alert('请输入收货人的详细地址');SAVING_FLAG = false;return;}
	if(!phone){lh.alert('请输入收货人的联系电话');SAVING_FLAG = false;return;}
	var obj = {};
	obj.userId = userId;
	obj.id = userAddressId;
	obj.receiverName = receiverName;
	obj.addressDetail = addressDetail;
	obj.phone = phone;
	obj.province = province;
	obj.city = city;
//	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateUserAddress',obj,function(rsp){
		SAVING_FLAG = false;
		if(rsp.status == 'success'){
			lh.alert(rsp.msg);
			lh.jumpR('/address');
			//frontBaseConfirm('收货地址已经成功保存，是否跳转到个人中心？',"lh.jumpR('/user')");
		}else{
			lh.alert(rsp.msg);
		}
	},'json')
}

