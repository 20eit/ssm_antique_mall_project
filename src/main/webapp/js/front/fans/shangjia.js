/**初始化数据*/
PAGE_COUNT = 10;//取多少条数据
CURRENT_PAGE = 1;//当前页数
CURRENT_FANS_PAGE = 1;//当前页数
CURRENT_RECOMMEND_PAGE = 1;
CURRENT_NOTICE_PAGE = 1;
CURRENT_CHAT_PAGE = 1;
var userId = $("#userId").val();
$(function(){
	var u = navigator.userAgent;
	if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
		var dom ="<audio id='msgAudio' src='/file/audio/newMsg.ogg' preload='auto'></audio>";
		$("#voice").append(dom);
	} else if (u.indexOf('iPhone') > -1 || u.indexOf("Safari") > -1) {//苹果手机
		var dom ="<audio id='msgAudio' src='/file/audio/newMsg.mp3' preload='auto'></audio>";
		$("#voice").append(dom);
	} /*else if (u.indexOf('Windows Phone') > -1) {//winphone手机
		alert("winphone手机");
	}*/
	loadRecommend();
	rollingLoad();//滚动加载
	downRefresh();//下拉刷新
});
//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}
//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadRecommend();
	    loading = false;
	  }, 1000);   //模拟延迟
});
}
function loadRecommend(){
	var param = {rows:PAGE_COUNT,page:CURRENT_RECOMMEND_PAGE};
	$.post('/getAllUserList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count = rsp.total
				if(count && count > 0){
					makeRecommendListDom(rsp.rows,true);
					CURRENT_RECOMMEND_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();			
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeRecommendListDom(fansList,isAppend){
	
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {
	rows:fansList,
	isFansOrNot:function(){
		var fans = this.isFans;
		var isFans = '';
		if(fans > 0){
			isFans = '<button type="button" class="btn btn-red mr5 pull-right" onclick="cancleFans('+this.id+');">-已关注</button>';
		}else if(fans <= 0){
			isFans = '<button type="button" class="btn btn-red mr5 pull-right" onclick="joinFans('+this.id+');">+关注他</button>';
		}
		return isFans;
	}
	});
	if(isAppend){
		$('#teacherAndFriend').append(rendered);
	}else{
		$('#teacherAndFriend').html(rendered);
	}
	checkDom();
}
function checkDom(){
	var domNum = $(".mt6").size();
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<10){
		$(".weui-infinite-scroll").text('没有更多数据');
	}
}


//取消关注
function cancleFans(fansId){
	var param = {fansId:fansId}
		lh.post('front', '/cancelFans', param, function(rsp) {
		if (rsp.success) {
			var data = rsp.rows;
			lh.alert(rsp.msg,lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}
//添加关注
function joinFans(fansId){
	var userId = $("#userId").val();
	if(userId == fansId){lh.alert('不能关注你自己');return;}
	var param = {fansId:fansId}
		lh.post('front', '/joinFans', param, function(rsp) {
		if (rsp.success) {
		    lh.alert(rsp.msg,lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}





