$(function(){
    //页面滚动时隐藏导航菜单
    $(window).bind('scrollstart', function(){
        $('.performance_fixed').addClass('hide');
    });
    $(window).bind('scrollstop', function(e){
        $('.performance_fixed').removeClass('hide');
    });
    //出价位置的加价按钮
    $("#add").on("click",function(){
        var price = $(this).prev().html();
        var addNum = 0.231;
        $(this).prev().html((Digit.round(price, 2)*1000000 + Digit.round(addNum, 2)*1000000)/1000000);//调用了bootstrap页面添加的保留两位小数的方法
    });
    $("#sub").on("click",function(){
        var price = $(this).next().html();
        var subNum = 0.231;
        $(this).next().html((Digit.round(price, 2)*1000000 - Digit.round(subNum, 2)*1000000)/1000000);//调用了bootstrap页面添加的保留两位小数的方法
    });
    //点击键盘后更改DOM为聊天输入框
    $("#keyboard").on("click",function(){
        $(".enter_scene_fixed").hide();
        $(".enter_scene_fixed_change").show();
        $(".allBg").show();
    });
    $(".allBg").on("click",function(){
        $(".enter_scene_fixed").show();
        $(".enter_scene_fixed_change").hide();
        $(".allBg").hide();
    });
    //点击发布按钮后的操作
    $("#release").on("click",function(){
        $(".release_click,.closeX").show();
    });
    $("#closeX").on("click",function(){
        $(".release_click,.closeX").hide();
    });
    //专场管理点击后处理
    $(".goodsClick").on("click",function(){
        $(".spmc_bg,.spmc").show();
    });
    $(".spmc_quit,.spmc_bg").on("click",function(){
        $(".spmc_bg,.spmc").hide();
    });
});




