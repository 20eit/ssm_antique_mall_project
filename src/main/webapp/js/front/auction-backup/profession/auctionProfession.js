
$(function(){
	/*TouchSlide({
		slideCell : "#slide_sale",
		effect : "leftLoop"
	});*/
	loadAuctionProfession();//加载专场
	lh.scrollBottom(loadAuctionProfession);//下拉加载更多数据
});

function loadAuctionProfession(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {sc_order:'mainStatus___DESC',orderByNotA:1,page:lh.page.currentPage,rows:lh.page.rows};
	var instId = $('#instId').val();
	if(instId)param.instId = instId;
	lh.post('front', '/ap/getAuctionProfessionList', param, function(rsp){
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionProfessionDom(data,1);
					lh.page.currentPage ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				lh.page.currentPage = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}

function makeAuctionProfessionDom(auctionProfessionList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	for(var i = 0; i<auctionProfessionList.length;i++){
		var ap = auctionProfessionList[i];
		var picPaths =  ap.picPath ? ap.picPath.split(",") : '';
		for(var j = 0;j<picPaths.length;j++){
			if(j == 0){
				var dom ="";
				dom ='<ul style="float:left;width:100%;">';
			}
				dom +='<li>'
					+'<div class="img_jp">'
					+'<div class="div_gg fl">'
						+'<a href="javascript:void(0);"><img src="'+picPaths[j]+'" style="max-height:90px;margin:5px;"/></a>'
					+'</div>'
				+'</div></li>';
			if(j == picPaths.length){
				dom +='</ul>';
			}
		}
		auctionProfessionList[i].picPaths = dom;
	}
	var data = getData(auctionProfessionList);
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#professionList').append(rendered);
	}else{
		$('#professionList').html(rendered);
	}
}

function getData(auctionProfessionList){
	var data = {
				rows:auctionProfessionList,
				statusDom:function(){
					var haveBegun = this.haveBegun || 1;
					//var startTime = this.startTime;
					//startTime = new Date(startTime);
					//var now = new Date().getTime();
					//if(startTime < now)haveBegun = 3;
					var statusDom = '';
					if(haveBegun == 2){
						statusDom = '<span class="colorRed">拍卖中</span>';
					}else if(haveBegun == 1){
						statusDom = '<span class="colorGreen">预展</span>';
					}else if(haveBegun == 3){
						statusDom = '<span class="colorGray">结束</span>';
					}
					return statusDom;
				},
				gradeDom:function(){
					var grade = this.grade;
					var gradeDom = '';
					if(grade && grade > 100){
						var grade = grade.toString();
						var diamond = grade[1];
						var star = grade[2];
						for(var j = 0;j<star;j++){
							gradeDom += '<img src="/images/front/sale_img2.png" width="12" height:"12" class="mgH1 fr"/>';
						}
						for(var i = 0;i<diamond;i++){
							gradeDom += '<img src="/images/front/diamond_blue.png" width="15" height:"15" class="fr"/>';
						}
					}
					return gradeDom;
				},
				getStartTime:function(){
					var startTime = this.startTime;
					return lh.formatDate(startTime,1);
				}
		   }
	return data;
}

//loadPraise:可把是否已经点赞标识存入本地缓存,添加时验证

function addPraise(instId){
	//var praiseNum = $("#praiseNum").val();
	//praiseNum++;
	//obj.praiseNum = praiseNum;
	var obj = {instId:instId};
	$.post('/addPraiseForInst',obj,function(rsp){
		if(rsp){
			$("#praiseSpan_"+instId).html('<img onclick="return false;" style="margin: 0 2px 5px 0;" src="/images/front/forum_img9_h.png" height="15">已点赞');
			frontLoginCheck(rsp);//登陆检查 
			if(rsp.status == 'success'){}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}
