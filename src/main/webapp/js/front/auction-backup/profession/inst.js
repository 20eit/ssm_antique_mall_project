$(function(){
	
});

function saveInstDesc(){
	var instId = $('#instId').val();
	var instLogo = $('#filePaths').val();
	var instName = $('#instName').val();
	var instTel = $('#instTel').val();
	var instAddress = $('#instAddress').val();
	var instIntroduce = $('#instIntroduce').val();
	if(!instId){
		location.href="/";return;
	}
	if(!instName){
		lh.alert('专场名称不能为空');return;
	}
	if(!instTel){
		lh.alert('联系电话不能为空');return;
	}
	if(!instAddress){
		lh.alert('联系地址不能为空');return;
	}
	if(!instIntroduce){
		lh.alert('机构简介不能为空');return;
	}
	if(!instLogo){
		lh.alert('机构图标不能为空');return;
	}
	var param = {id:instId,name:instName,tel:instTel,address:instAddress,introduce:instIntroduce,picPaths:instLogo};
	
	$.post('/updateAuctionInst',param,function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查 
			if(rsp.success){
				history.back(-1);
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}


function saveInstAnno(){
	var instId = $('#instId').val();
	var annoId = $('#annoId').val();
	var annoTitle = $('#annoTitle').val();
	var annoContent = $('#annoContent').val();
	if(!instId){
		location.href="/";return;
	}
	if(!annoTitle){
		lh.alert('公告标题不能为空');return;
	}
	if(!annoContent){
		lh.alert('公告内容不能为空');return;
	}

	var param = {linkId:instId,title:annoTitle,content:annoContent};
	if(annoId){
		param.id = annoId;
	}
	$.post('/addOrUpdateInstAnno',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var instId = $('#instId').val();
				var url = "/instAnno/"+instId;
				var r = $("#r").val();
				if(r) url += "?r="+r;
				window.location.href = url;
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function deleteInstAnno(annoId){
	if(!annoId){
		lh.alert('请选择要删除的公告');return;
	}
	if(confirm("确定要删除此条公告吗？")){
		var param = {annoId:annoId};
		$.post('/deleteInstAnno',param,function(rsp){
			if(rsp){
				frontLoginCheck(rsp);//登陆检查 
				if(rsp.success){
					location.reload();
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}
}

