CURRENT_PAGE = 1;
ADD_FLAG = true;
$(function(){
	getMyProfessionList();//加载专场
	//lh.scrollBottom(loadAuctionProfession);//下拉加载更多数据
});

function getMyProfessionList(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {sc_order:'mainStatus___DESC',orderByNotA:1,page:CURRENT_PAGE,rows:15};
	var instId = $('#instId').val();
	if(instId)param.instId = instId;
	$.post('/getMyProfessionList',param,function(rsp){
		$('#loadingTip').hide();
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionProfessionDom(data,1);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}


function makeAuctionProfessionDom(auctionProfessionList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	for(var i = 0; i<auctionProfessionList.length;i++){
		var picPaths =  auctionProfessionList[i].picPath.split(",");
		for(var j = 0;j<picPaths.length;j++){
			if(j == 0){
				var dom ="";
				dom ='<ul style="float:left;width:100%;">';
			}
				dom +='<li>'
					+'<div class="img_jp">'
					+'<div class="div_gg fl">'
						+'<a href="javascript:void(0);"><img src="'+picPaths[j]+'" style="max-height:90px;margin:5px;"/></a>'
					+'</div>'
				+'</div></li>';
			if(j == picPaths.length){
				dom +='</ul>';
			}
		}
		auctionProfessionList[i].picPaths = dom;
	}
	var data = getData(auctionProfessionList);
			   
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#professionList').append(rendered);
	}else{
		$('#professionList').html(rendered);
	}
}

function getData(auctionProfessionList){
	var instId = $('#instId').val();
	var data = {
				rows:auctionProfessionList,
				updateDom:function(){
					var professionId = this.id;
					var haveBegun = this.haveBegun || 1;
					var updateDom = '';
					if(haveBegun == 1){
						var url = '/aqAddOrUpdate?instId='+instId+'&professionId='+professionId
						updateDom = '<button type="button" class="btn btn-success" style="position: relative;top: 5px;left: 40px;" onclick="location.href=\''+url+'\'"> 修改 </button>';
					}else{
						updateDom = '<span style="position: relative;left:15px;">拍卖已结束</span>';
					}
					return updateDom;
				},
				statusDom:function(){
					var haveBegun = this.haveBegun || 1;
					var statusDom = '';
					if(haveBegun == 2){
						ADD_FLAG = false;
						statusDom = '<span class="colorRed">拍卖中</span>';
					}else if(haveBegun == 1){
						ADD_FLAG = false;
						statusDom = '<span class="colorGreen">预展</span>';
					}else if(haveBegun == 3){
						statusDom = '<span class="colorGray">结束</span>';
					}
					return statusDom;
				},
				gradeDom:function(){
					var grade = this.grade;
					var gradeDom = '';
					if(grade && grade > 100){
						var grade = grade.toString();
						var diamond = grade[1];
						var star = grade[2];
						for(var j = 0;j<star;j++){
							gradeDom += '<img src="/images/front/sale_img2.png" width="12" height:"12" class="mgH1 fr"/>';
						}
						for(var i = 0;i<diamond;i++){
							gradeDom += '<img src="/images/front/diamond_blue.png" width="15" height:"15" class="fr"/>';
						}
					}
					return gradeDom;
				},
				getStartTime:function(){
					var startTime = this.startTime;
					return formatDate(startTime,1);
				}
		   }
	return data;
}

function jumpToAddAuction(){
	if(ADD_FLAG){
		//var instId = $('#instId').val();
		//var url = '/professionAddOrUpdate?instId='+instId;
		var url = '/releaseGoods';
		var r = $("#r").val();
		if(r) url += "?r="+r;
		window.location.href = url;
	}else{
		lh.alert('您还有未结束的拍卖，等拍卖结束后再添加吧');
	}
}

