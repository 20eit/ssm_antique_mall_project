TOP_PRICE = null;
AUTO_OFFER_ID = null;
NOTICE_ID = null;
MY_SWIPER = null;
$(function(){
	$('#banner').show();
	initBanner();//common.js
	initData();
});

/** 初始化数据 - 是否开拍提醒，是否委托出价 */
function initData(){
	 MY_SWIPER = new Swiper ('.swiper-container', {
	    direction: 'horizontal',
	    loop: true,
	    // 如果需要分页器
	    pagination: '.swiper-pagination',
	    // 如果需要前进后退按钮
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    // 如果需要滚动条
	    scrollbar: '.swiper-scrollbar'
	  })        
	var paramObj = {}
	$.post('/getTaskAndAutoOffer',paramObj,function(rsp){
		if(rsp){
			if(rsp.success){
				var taskFlag = rsp.taskFlag;
				var autoOfferFlag = rsp.autoOfferFlag;
				if(taskFlag){
					$('#beginNotice').attr('state',2).text('取消提醒');
					//$('#offerShow').css('background', 'rgba(25,25,25,0.95) none repeat scroll !important;');
					$('#beginNotice').addClass('mainRedBg');
					NOTICE_ID = rsp.task.id;
				}
				if(autoOfferFlag){
					$('#offerShow').attr('state',2).text('取消委托出价');
					//$('#offerShow').css('background', 'rgba(25,25,25,0.95) none repeat scroll !important;');
					$('#offerShow').addClass('mainRedBg');
					TOP_PRICE = rsp.autoOffer.topPrice;
					AUTO_OFFER_ID = rsp.autoOffer.id;
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
	
}

/** 开拍提醒 - 打开关闭 */
function toggleNotice(){
	var $notice = $('#beginNotice');
	var state = $notice.attr('state');
	var operation = 'add';
	if(!state || state == 1){//未参与
		var professionId = $('#professionId').val();
		$.post('/addOrUpdateTask',{typeId:39,noticeId:1,linkId:professionId},function(rsp){
			if(rsp){
				if(rsp.success){
					$notice.attr('state',2).text('取消提醒');
					$notice.addClass('mainRedBg');
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}else{
		$.post('/delTask',{id:NOTICE_ID},function(rsp){
			if(rsp){
				if(rsp.success){
					$notice.attr('state',1).text('开拍提醒');
					$notice.removeClass('mainRedBg');
					NOTICE_ID = null;
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}
}

/** 显示委托竞价窗口 */
function showAutoOffer(){
	var $offerShow = $('#offerShow');
	var state = $offerShow.attr('state');
	if(!state || state == 1){//未参与
		$('#offerTip').text('输入最高委托金额(元):');
		$('#offerIptDiv').show();
	}else{
		$('#offerTip').text('您设置的委托金额为'+TOP_PRICE+'元，是否确认取消委托出价？');
		$('#offerIptDiv').hide();
	}
	$('#offerWin').show();
}

/** 委托竞价：确认，取消 */
function doAutoOffer(operation,paramObj){
	if(operation == 'cancel'){
		$('#offerWin').hide();
	}else if(operation == 'save'){
		var state = $('#offerShow').attr('state');
		if(state && state == 2){//已参与 - 取消
			delAutoOffer();return;
		}
		var topPrice = $('#topPrice').val();
		if(!topPrice){
			lh.alert('请输入您的委托金额');return;
		}else{
			if($.isNumeric(topPrice)){
				topPrice = parseInt(topPrice);
				$('#topPrice').val(topPrice);
				if(topPrice <= 10){
					lh.alert('金额最低为10元');return;
				}else if(topPrice > 100000){
					lh.alert('您输入的金额太高，请联系客服人员操作');return;
				}
			}else{
				lh.alert('输入的金额只能为数字');return;
			}
		}
		paramObj.topPrice = topPrice;
		TOP_PRICE = topPrice;
		addOrUpdateAutoOffer(paramObj);
	}
}

function addOrUpdateTask(operation){
	var url = '';
	if(operation == 'add'){
		
	}else if(operation == 'del'){
		
	}
	var param = {};
	$.post('/addOrUpdateTask',param,function(rsp){
		if(rsp){
			if(rsp.success){
				
			}else{
				
			}
		}
	},'json');
}

function addOrUpdateAutoOffer(paramObj){
	if(!paramObj)return;
	$.post('/addOrUpdateAutoOffer',paramObj,function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.success){
				$('#offerShow').attr('state',2).text('取消委托出价');
				$('#offerWin').hide();
				$('#offerShow').addClass('mainRedBg');
				AUTO_OFFER_ID = rsp.id;
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function delAutoOffer(){
	if(!AUTO_OFFER_ID)return;
	$.post('/delAutoOffer',{id:AUTO_OFFER_ID},function(rsp){
		if(rsp){
			if(rsp.success){
				$('#offerShow').attr('state',1).text('委托出价');
				$('#offerWin').hide();
				$('#offerShow').removeClass('mainRedBg');
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}
