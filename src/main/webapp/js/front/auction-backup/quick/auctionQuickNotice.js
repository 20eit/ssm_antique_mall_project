CURRENT_PAGE = 1;
SAVEING_FLAG = false;

$(function(){
	initNoticeTypeIds();

});

function initNoticeTypeIds(){
	var noticeTypeIds = $('#noticeTypeIds').val();
	if(noticeTypeIds){
		var typeAry = noticeTypeIds.split(',');
		for(var i in typeAry){
			$('#goodsType_'+typeAry[i]).prop('checked', "checked");
		}
	}
}


function aqNoticeAddOrUpdate(){
	if(SAVEING_FLAG)return;
	SAVEING_FLAG = true;
	var goodsType_121 = $('#goodsType_121').prop('checked');
	var goodsType_122 = $('#goodsType_122').prop('checked');
	var goodsType_123 = $('#goodsType_123').prop('checked');
	var goodsType_124 = $('#goodsType_124').prop('checked');
	var goodsType_125 = $('#goodsType_125').prop('checked');
	var goodsType_126 = $('#goodsType_126').prop('checked');
	var goodsType_127 = $('#goodsType_127').prop('checked');
	var goodsType_128 = $('#goodsType_128').prop('checked');
	var goodsType_129 = $('#goodsType_129').prop('checked');
	
	var goodsTypeIds = '';
	if(goodsType_121)goodsTypeIds += ','+121;
	if(goodsType_122)goodsTypeIds += ','+122;
	if(goodsType_123)goodsTypeIds += ','+123;
	if(goodsType_124)goodsTypeIds += ','+124;
	if(goodsType_125)goodsTypeIds += ','+125;
	if(goodsType_126)goodsTypeIds += ','+126;
	if(goodsType_127)goodsTypeIds += ','+127;
	if(goodsType_128)goodsTypeIds += ','+128;
	if(goodsType_129)goodsTypeIds += ','+129;
	
	if(goodsTypeIds.length>1)goodsTypeIds = goodsTypeIds.substring(1);
	var param = {goodsTypeIds:goodsTypeIds};
	
	frontBaseLoadingOpen();//加载遮罩
	$.post('/aqNoticeAddOrUpdate',param,function(rsp){
		SAVEING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			if(rsp.success){
				/*var r = $('#r').val();
				var url = '/aq';
				if(r)url += '?r='+r;
				location.href = url;*/
				lh.alert('已经成功保存提醒设置');
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
	
}




