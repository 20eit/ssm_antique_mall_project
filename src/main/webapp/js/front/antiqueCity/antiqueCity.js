/** 鬼市（逛逛）主JS  */
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadAntiqueCityList();//加载店铺列表（包含每个店铺的部分商品）
	lh.scrollBottom(loadAntiqueCityList);
});



function loadAntiqueCityList(){
	var param = {};
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	param.mainStatus = 1;
	$.post('/getAntiqueCityList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count  = rsp.total;
				if(count && count > 0){
					var antiqueCityList = rsp.antiqueCityList;
					makeAntiqueCityListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeAntiqueCityListDom(antiqueCityList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:antiqueCityList});
	if(isAppend){
		$('#antiqueCity').append(rendered);
	}else{
		$('#antiqueCity').html(rendered);
	}
}