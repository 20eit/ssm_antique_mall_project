SAVING_FLAG = false;
$(function(){
	initUploadSimple({showEdBtns:true});//调用完整方法
	init();
});

function init(){
	$("#province").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    size: 10
	});
	$('#province').on('changed.bs.select', function (e) {
		getCity();
	});
	$("#city").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
		title:'',
	    showTick:true,
	    size: 10
	});
	var antiqueCityCity = $("#antiqueCityCity").val();
	if(antiqueCityCity){
		getCity(antiqueCityCity);
	}
}

function getCity(city){
	var provinceId = $("#province").val();
	$.post('/getCity',{provinceId:provinceId},function(rsp){
		if(rsp){
			var dom = '<select id="city""><option value="">请选择</option>';
			$("#cityDiv").empty();
			for(var i = 0;i < rsp.length;i++){
				dom +=' <option';
				if(city && city == rsp[i].id)dom += ' selected="selected" ';
				dom += ' value="'+rsp[i].id+'">'+rsp[i].name+'</option>';
			}
			dom += '</select>';
			$("#cityDiv").append(dom);
			$("#city").selectpicker({
				title:'',
			    showTick:true,
			    size: 10
			});
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}

function commitAntiqueCity(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	//var antiqueCityId = $("#antiqueCityId").val();
	var name = $("#name").val();
	var address = $("#address").val();
	var introduce = $("#introduce").val();
	var filePaths = $("#filePaths").val();
	var tel = $("#tel").val();
	var province = $("#province").val();
	var city = $("#city").val();
	if(!name){lh.alert('请选择商圈所在的省或直辖市');SAVING_FLAG = false;return;}
	if(!province){lh.alert('请选择商圈所在的省或直辖市');SAVING_FLAG = false;return;}
	if(!city){lh.alert('请选择商圈所在的城市');SAVING_FLAG = false;return;}
	if(!address){lh.alert('请填写商圈所在的地址');SAVING_FLAG = false;return;}
	if(!tel){lh.alert('请填写商圈所在地的联系电话');SAVING_FLAG = false;return;}
	if(!introduce){lh.alert('请填写商圈的简介');SAVING_FLAG = false;return;}
	var filePathArr = new Array();
	if(filePaths.indexOf(',') >= 0){
		filePaths = filePaths.substring(1);
	}
	filePathArr = filePaths.split(",");
	if(filePathArr.length > 1){
		lh.alert('请先删除以前的图片,再重新上传');
		SAVING_FLAG = false;
		return;
	}
	if(!filePathArr[0]){lh.alert('请上传古玩城logo');SAVING_FLAG = false;return;}
	var obj = {};
	//obj.id = antiqueCityId;
	obj.name = name;
	obj.address = address;
	obj.introduce = introduce;
	obj.province = province;
	obj.city = city;
	obj.picPath = filePathArr[0];
	obj.tel = tel;
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateAntiqueCity',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				frontBaseConfirm('您的古玩城添加成功，等待管理员的审核.','jumpToAntiqueCity()');
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function jumpToAntiqueCity(){
	var url = '/antiqueCity';
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}