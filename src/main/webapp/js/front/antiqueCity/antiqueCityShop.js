/** 鬼市（逛逛）主JS  */
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	loadAntiqueCityShopList();//加载店铺列表（包含每个店铺的部分商品）
	lh.scrollBottom(loadAntiqueCityShopList);
});



function loadAntiqueCityShopList(){
	var relationId = $("#relationId").val();
	var param = {};
	param.relationType = '64';
	param.relationId = relationId;
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	$.post('/getUserRelationList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count = rsp.total;
				if(count &&  count > 0){
					var shopList = rsp.shopList;
					makeAntiqueCityShopListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
			SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeAntiqueCityShopListDom(shopList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:shopList});
	if(isAppend){
		$('#shopItems').append(rendered);
	}else{
		$('#shopItems').html(rendered);
	}
}

