/** 鬼市（逛逛）主JS  */
SAVING_FLAG = false;
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	//loadAntiqueCityList();//加载店铺列表（包含每个店铺的部分商品）
	//lh.scrollBottom(loadAntiqueCityList);
	$("#province").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    size: 10
	});
	$('#province').on('changed.bs.select', function (e) {
		getCity();
	});
	$("#city").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
		title:'',
	    showTick:true,
	    size: 10
	});
	var antiqueCityCity = $("#antiqueCityCity").val();
	if(antiqueCityCity){
		getCity(antiqueCityCity);
	}
});

function getCity(city){
	var provinceId = $("#province").val();
	$.post('/getCity',{provinceId:provinceId},function(rsp){
		if(rsp){
			var dom = '<select id="city" disabled="disabled"><option value="">请选择</option>';
			$("#cityDiv").empty();
			for(var i = 0;i < rsp.length;i++){
				dom +=' <option';
				if(city && city == rsp[i].id)dom += ' selected="selected" ';
				dom += ' value="'+rsp[i].id+'">'+rsp[i].name+'</option>';
			}
			dom += '</select>';
			$("#cityDiv").append(dom);
			$("#city").selectpicker({
				title:'',
			    showTick:true,
			    size: 10
			});
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}

function editData(){
	$("#name,#address,#introduce,#tel").removeAttr('readonly');
	//$("#province,#city").removeAttr('disabled');
	$("#province,#city").prop('disabled', false);
	$("#province,#city").selectpicker('refresh');
	$("#edit").show();
	$("#antiqueCityShop").hide();
}

function cancelData(){
	$("#name,#address,#introduce,#tel").attr('readonly','readonly');
	//$("#province,#city").attr('disabled','disabled');
	$("#province,#city").prop('disabled', true);
	$("#province,#city").selectpicker('refresh');
	$("#edit").hide();
	$("#antiqueCityShop").show();
}

function commitAntiqueCity(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var antiqueCityId = $("#antiqueCityId").val();
	var name = $("#name").val();
	var address = $("#address").val();
	var introduce = $("#introduce").val();
	var province = $("#province").val();
	var city = $("#city").val();
	var tel = $("#tel").val();
	var obj = {};
	obj.id = antiqueCityId;
	obj.name = name;
	obj.address = address;
	obj.introduce = introduce;
	obj.province = province;
	obj.city = city;
	obj.tel = tel;
	frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/addOrUpdateAntiqueCity',obj,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			frontLoginCheck(rsp);//登陆检查
			if(rsp.status == 'success'){
				window.location.reload();
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function loadAntiqueCityList(){
	var param = {};
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	$.post('/getAntiqueCityList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				var count = rsp.total;
				if(count && count > 0){
					var antiqueCityList = rsp.antiqueCityList;
					makeAntiqueCityListDom(rsp.rows,1);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeAntiqueCityListDom(antiqueCityList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:antiqueCityList});
	if(isAppend){
		$('#antiqueCity').append(rendered);
	}else{
		$('#antiqueCity').html(rendered);
	}
}