CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 7;//取多少条数据
PARAM = {}

$(function(){
	loadActivities();
	lh.scrollBottom(loadActivities);
});

function loadActivities(){
	var param = {};
	param.typeId = '41';
	param.mainStatus = 1;
	param.rows = PAGE_COUNT;
	param.page = CURRENT_PAGE;
	if(PARAM.userId){
		param.userId = PARAM.userId;
	}
	if(PARAM.authorId){
		param.authorId = PARAM.authorId;
	}
	if(PARAM.myComment){
		param.myComment = PARAM.myComment;
	}
	if(PARAM.sevenInDate){
		param.sevenInDate = PARAM.sevenInDate;
	}
	$.post('/getArticleList',param,function(rsp){
		if(rsp){
			if(rsp.status == 'success'){
				var count = rsp.total;
				if(count && count > 0){
					makeActivitiesListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}


function makeActivitiesListDom(activitiesList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	for(var a in activitiesList){
		activitiesList[a].startDate = formatDate(activitiesList[a].startDate);
		activitiesList[a].endDate = formatDate(activitiesList[a].endDate);
	}
	var rendered = Mustache.render(template, {rows:activitiesList});
	if(isAppend){
		$('#activities').append(rendered);
	}else{
		$('#activities').html(rendered);
	}
}

function allActivities(){
	PARAM = {};
	$("#activities").empty();
	$("#allActivities").css({'color':'red','border-bottom':'1px solid red'});
	$("#myActivities").css({'color':'','border-bottom':''});
	$("#sevenDay").css({'color':'','border-bottom':''});
	$("#myComment").css({'color':'','border-bottom':''});
	CURRENT_PAGE = 1;
	loadActivities();
}

function myActivities(){
	PARAM = {};
	var userId = $("#userId").val();
	$("#myActivities").css({'color':'red','border-bottom':'1px solid red'});
	$("#allActivities").css({'color':'','border-bottom':''});
	$("#sevenDay").css({'color':'','border-bottom':''});
	$("#myComment").css({'color':'','border-bottom':''});
	CURRENT_PAGE = 1;
	PARAM.userId = userId;
	$("#activities").empty();
	loadActivities();
}

function sevenDay(){
	PARAM = {};
	$("#sevenDay").css({'color':'red','border-bottom':'1px solid red'});
	$("#myActivities").css({'color':'','border-bottom':''});
	$("#allActivities").css({'color':'','border-bottom':''});
	$("#myComment").css({'color':'','border-bottom':''});
	var sevenInDate = 1
	CURRENT_PAGE = 1;
	PARAM.sevenInDate = sevenInDate;
	$("#activities").empty();
	loadActivities();
}

function myComment(){
	PARAM = {};
	var myComment = 1
	$("#myComment").css({'color':'red','border-bottom':'1px solid red'});
	$("#myActivities").css({'color':'','border-bottom':''});
	$("#sevenDay").css({'color':'','border-bottom':''});
	$("#allActivities").css({'color':'','border-bottom':''});
	var authorId = $("#userId").val();
	PARAM.myComment = myComment;
	PARAM.authorId = authorId;
	CURRENT_PAGE = 1;
	$("#activities").empty();
	loadActivities();
}



