$(function(){
	loadAuction();
});

function loadAuction(){
	var param = {};
	$.post('/getAuctionList',param,function(rsp){
		if(rsp.success){
			makeAuctionListDom(rsp.rows,1);
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}


function makeAuctionListDom(auctionList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:auctionList});
	if(isAppend){
		$('#auctionList').append(rendered);
	}else{
		$('#auctionList').html(rendered);
	}
}
