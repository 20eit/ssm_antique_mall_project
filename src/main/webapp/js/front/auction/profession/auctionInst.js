
$(function(){
	/*TouchSlide({
		slideCell : "#slide_sale",
		effect : "leftLoop"
	});*/
	loadAuctionInst();//加载专场
	lh.scrollBottom(loadAuctionInst);//下拉加载更多数据
});

function loadAuctionInst(){
	if(!lh.preventRepeat()){
		return lh.showFrontRepeatTip();//提示重复提交
	}
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {page:lh.page.currentPage,rows:lh.page.rows};
	lh.post('front', '/ap/getAuctionInstList', param, function(rsp){
		$('#loadingTip').hide();
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeAuctionInstDom(data,true);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
		}
	},'json');
}


function makeAuctionInstDom(auctionInstList,isAppend){
	var template;
	if(lh.status.currentLayout == 1){//默认缩略布局
		template = $('#template').html();
	}else{//大图布局
		template = $('#templateLayout2').html();
	}
	if(lh.page.currentPage == 1)isAppend = false;
	Mustache.parse(template);   // optional, speeds up future uses
	var data = {
					rows:auctionInstList,
					statusDom:function(){
						var prefessionStatus = this.prefessionStatus;
						var statusDom = '';
						if(prefessionStatus == 3){
							statusDom = '<img src="/images/front/sale_img3.png" width="100%" />';//开拍
						}else if(prefessionStatus == 2){
							statusDom = '<img src="/images/front/sale_img4.png" width="100%" />';//预展
						}
						return statusDom;
					},
					gradeDom:function(){
						var grade = this.grade;
						var gradeDom = '';
						if(grade && grade > 100){
							var grade = grade.toString();
							var diamond = grade[1];
							var star = grade[2];
							for(var i = 0;i<diamond;i++){
								gradeDom += '<img src="/images/front/diamond_blue.png" width="10" height:"10"/>';
							}
							for(var j = 0;j<star;j++){
								gradeDom += '<img src="/images/front/sale_img2.png" class="mgH1" width="8" height:"8"/>';
							}
						}
						return gradeDom;
					}
			   }
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#container').append(rendered);
	}else{
		$('#container').html(rendered);
	}
}

/** 切换布局显示 */
function switchLaout(){
	CURRENT_PAGE = 1;
	if(CURRENT_LAYOUT == 1){
		$('#switchLaout').text('缩略');
		CURRENT_LAYOUT = 2;
	}else{
		$('#switchLaout').text('大图');
		CURRENT_LAYOUT = 1;
	}
	loadAuctionInst();
}

function toggleOptBtn(){
	if(INST_CHECK){
		$('#pf_point').slideToggle();return;
	}
	$.post('/getMyAuctionInst', null, function(rsp){
		frontLoginCheck(rsp);//登陆检查 
		if(rsp){
			INST_CHECK = true;
			if(rsp.success){
				$('#myInst').show();
				$('#instApply').hide();
			}else{
				$('#myInst').hide();
				$('#instApply').show();
			}
		}
		$('#pf_point').slideToggle();
	},'json');
	
}



