
$(function(){
	//iniPage();
	iniData();
});


function iniPage(){
}

function iniData(){
	getMyProfessionList('pre');
}

function getMyProfessionList(show){
	if(!lh.status.currentShow)lh.status.currentShow = show;
	if(lh.status.currentShow != show){//切换时清空dom，查询第一页数据
		lh.status.currentShow = show;
		lh.page.currentPage = 1;
		$('#data-container').empty();
		$('#auction_pre,#auction_ing,#auction_done').removeClass('active');
		$('#auction_'+show).addClass('active');
		
	}
	var obj = {show:show, page:lh.page.currentPage, rows:lh.page.rows};
	lh.post('front', '/ap/getMyProfessionList',obj,function(rsp){
		if(rsp.success){
			if(rsp.rows && rsp.rows.length > 0){
				makeAuctionGoodsDom(rsp.rows, true);
				lh.page.currentPage++;
			}
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json');
}

function makeAuctionGoodsDom(dataList, isAppend){
	lh.plugins.template = $('#template').html();
	Mustache.parse(lh.plugins.template);   // optional, speeds up future uses
	var data = {
		rows:dataList,
		startTimeStr: function(){
			var time = this.startTime;
			return moment(time).format('YYYY年MM月DD日 HH:mm');
		},
		statusDom: function(){
			var haveBegun = this.haveBegun;
			var dom = '';
			if(!haveBegun || haveBegun == 4){
				dom = '<div class="blue_tint">未提交</div>';
			}else if(haveBegun == 1){
				dom = '<div class="color_green">预展中</div>';
			}else if(haveBegun == 2){
				dom = '<div class="color_orange">拍卖中</div>';
			}else if(haveBegun == 3){
				dom = '<div class="color_gray">已结束</div>';
			}else if(haveBegun == 5){
				dom = '<div class="blue_tint">审核中</div>';
			}
			return dom;
		}
    }
	var rendered = Mustache.render(lh.plugins.template, data);
	if(lh.page.currentPage > 1){
		$('#data-container').append(rendered);
	}else{
		$('#data-container').html(rendered);
	}
}

function switchCurrent(apSerial, haveBegun){
	lh.current.apSerial = apSerial;
	if(!haveBegun || haveBegun == 4){//未提交
	}else if(haveBegun == 1){
		lh.jumpR('/ag/page/agList/'+apSerial);return;//预展中
	}else if(haveBegun == 2){
		lh.jumpR('/ag/page/agList/'+apSerial);return;//拍卖中
	}else if(haveBegun == 3){
		lh.jumpR('/ag/page/agList/'+apSerial);return;//已结束
	}else if(haveBegun == 5){//审核中
	}
	$('.spmc_bg,.spmc').show();
}

function updateAuction(){
	if(lh.current.apSerial){
		lh.jumpR('/ap/page/addOrUpdate?apSerial='+lh.current.apSerial);
	}else{
		lh.jumpR('/ap/page/manage');
	}
}

function previewAuction(){
	var apSerial = lh.current.apSerial;
	if(apSerial){
		lh.jumpR('/ag/page/agList/'+apSerial);return;
		//lh.jumpR('/ap/page/preview?apSerial='+lh.current.apSerial);
	}else{
		lh.jumpR('/ap/page/manage');
	}
}

function submitAuction(){
	if(lh.current.apSerial){
		lh.post('front', '/ap/doSubmit', {serial:lh.current.apSerial},function(rsp){
			if(rsp.success){
				lh.reload();
			}else{
				lh.alert(rsp.msg, '提示');
			}
		},'json');
	}
}

function confirmDelete(){
	lh.confirm('是否确定删除该场拍卖？', '提示', deleteAuction);
}

function deleteAuction(){
	if(lh.current.apSerial){
		lh.post('front', '/ap/doDelete', {serial:lh.current.apSerial},function(rsp){
			if(rsp.success){
				lh.reload();
			}else{
				lh.alert(rsp.msg, '提示');
			}
		},'json');
	}
}