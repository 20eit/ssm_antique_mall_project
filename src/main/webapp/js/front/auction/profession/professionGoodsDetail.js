
if(!lh.param)lh.param = {};
$(function(){
	$('#banner').show();
	initBanner();//common.js
	initData();
});

/** 初始化数据 - 是否开拍提醒，是否委托出价 */
function initData(){
	$('#apoContainer>div:first').addClass('orange');//更新出价记录第一条的显示效果
	$('#apoContainer>div:first .icon-thumbs-down').addClass('icon-thumbs-up').removeClass('icon-thumbs-down');
	
	/* lh.param.mySwiper = new Swiper ('.swiper-container', {
	    direction: 'horizontal',
	    loop: true,
	    // 如果需要分页器
	    pagination: '.swiper-pagination',
	    // 如果需要前进后退按钮
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    // 如果需要滚动条
	    scrollbar: '.swiper-scrollbar'
	  })        */
	//var paramObj = {}
	//getTask(paramObj);
	if(lh.param.overFoucs){
		$('#focusBtn').attr('state',2).text('取消关注');
	}
}

/** 开拍提醒 - 打开关闭 */
function toggleNotice(){
	var $notice = $('#beginNotice');
	var state = $notice.attr('state');
	var operation = 'add';
	if(!state || state == 1){//未参与
		var professionId = lh.param.ap.id;
		lh.post('front', '/addOrUpdateTask',{taskTypeId:1,noticeId:1,linkId:professionId},function(rsp){
			if(rsp.success){
				//$notice.attr('state',2).text('取消提醒');$notice.addClass('mainRedBg');
				$notice.attr('state',2).text('取消提醒');
				lh.param.noticeTaskId = rsp.taskId;
			}else{
				lh.alert(rsp.msg);
			}
		},'json');
	}else{
		lh.post('front', '/delTask',{id: lh.param.noticeTaskId},function(rsp){
			if(rsp.success){
				//$notice.attr('state',1).text('开拍提醒');$notice.removeClass('mainRedBg');
				$notice.attr('state',1).text('开拍提醒');
				lh.param.noticeTaskId = null;
			}else{
				lh.alert(rsp.msg);
			}
		},'json');
	}
}

/** 关注 - 打开关闭 */
function toggleFocus(userSerial){
	var $focus = $('#focusBtn');
	var state = $focus.attr('state');
	if(!state || state == 1){//未参与
		lh.post('front', '/addOrUpdateFans',{userSerial : userSerial},function(rsp){
			if(rsp.success){
				//$notice.attr('state',2).text('取消提醒');$notice.addClass('mainRedBg');
				$focus.attr('state',2).text('取消关注');
			}else{
				lh.alert(rsp.msg);
			}
		},'json');
	}else{
		lh.post('front', '/cancelFocus',{userSerial : userSerial},function(rsp){
			if(rsp.success){
				//$notice.attr('state',1).text('开拍提醒');$notice.removeClass('mainRedBg');
				$focus.attr('state',1).text('关注');
			}else{
				lh.alert(rsp.msg);
			}
		},'json');
	}
}

function creditOrHall(apId){
	
	lh.jumpR('/ap/hall/'+apId);
}

function focus(userSerial){
	
}


/*
TOP_PRICE = null;
AUTO_OFFER_ID = null;
显示委托竞价窗口 *
function showAutoOffer(){
	var $offerShow = $('#offerShow');
	var state = $offerShow.attr('state');
	if(!state || state == 1){//未参与
		$('#offerTip').text('输入最高委托金额(元):');
		$('#offerIptDiv').show();
	}else{
		$('#offerTip').text('您设置的委托金额为'+TOP_PRICE+'元，是否确认取消委托出价？');
		$('#offerIptDiv').hide();
	}
	$('#offerWin').show();
}
 委托竞价：确认，取消 
function doAutoOffer(operation,paramObj){
	if(operation == 'cancel'){
		$('#offerWin').hide();
	}else if(operation == 'save'){
		var state = $('#offerShow').attr('state');
		if(state && state == 2){//已参与 - 取消
			delAutoOffer();return;
		}
		var topPrice = $('#topPrice').val();
		if(!topPrice){
			lh.alert('请输入您的委托金额');return;
		}else{
			if($.isNumeric(topPrice)){
				topPrice = parseInt(topPrice);
				$('#topPrice').val(topPrice);
				if(topPrice <= 10){
					lh.alert('金额最低为10元');return;
				}else if(topPrice > 100000){
					lh.alert('您输入的金额太高，请联系客服人员操作');return;
				}
			}else{
				lh.alert('输入的金额只能为数字');return;
			}
		}
		paramObj.topPrice = topPrice;
		TOP_PRICE = topPrice;
		addOrUpdateAutoOffer(paramObj);
	}
}
function addOrUpdateAutoOffer(paramObj){
if(!paramObj)return;
$.post('/addOrUpdateAutoOffer',paramObj,function(rsp){
	if(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp.success){
			$('#offerShow').attr('state',2).text('取消委托出价');
			$('#offerWin').hide();
			$('#offerShow').addClass('mainRedBg');
			AUTO_OFFER_ID = rsp.id;
		}else{
			lh.alert(rsp.msg);
		}
	}
},'json');
}

function delAutoOffer(){
if(!AUTO_OFFER_ID)return;
$.post('/delAutoOffer',{id:AUTO_OFFER_ID},function(rsp){
	if(rsp){
		if(rsp.success){
			$('#offerShow').attr('state',1).text('委托出价');
			$('#offerWin').hide();
			$('#offerShow').removeClass('mainRedBg');
		}else{
			lh.alert(rsp.msg);
		}
	}
},'json');
}
*/

function jumpToPayCreditMoney(apSerial){
	var fromUrl = location.href;
	localStorage.setItem("fromUrl", fromUrl);
	location.href = '/ap/page/creditMoney?apSerial='+apSerial;
}