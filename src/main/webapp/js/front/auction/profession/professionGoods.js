
$(function(){
	//iniPage();
	iniData();
});


function iniPage(){
}

function iniData(){
	getAuctionGoodsList();
}

function getAuctionGoodsList(show){
	//var professionId = lh.param.professionId;
	var auctionSerial = lh.param.ap.serial;
	var obj = {auctionSerial:auctionSerial,page:lh.page.currentPage, rows:50};
	lh.post('front', '/ag/getList',obj,function(rsp){
		if(rsp.success){
			if(rsp.rows && rsp.rows.length > 0){
				makeAuctionGoodsDom(rsp.rows, true);
				lh.page.currentPage++;
			}
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json');
}

function makeAuctionGoodsDom(dataList, isAppend){
	lh.plugins.template = $('#template').html();
	Mustache.parse(lh.plugins.template);   // optional, speeds up future uses
	var data = {
		rows:dataList,
		getStartTime:function(){
			var startTime = this.startTime;
			return lh.formatDate(startTime, 1);//moment(startTime).format("YYYY-MM-DD HH:mm")
		},
		getPicPath:function(){
			var picPath = this.picPath;
			if(picPath){
				picPath += buildOSSZoom(100,100); //@@_OSS_IMG_@@
			}
			return picPath;
		}
    }
	var rendered = Mustache.render(lh.plugins.template, data);
	if(lh.page.currentPage > 1){
		$('#data-container').append(rendered);
	}else{
		$('#data-container').html(rendered);
	}
}

function jumpToPayCreditMoney(apSerial){
	var fromUrl = location.href;
	localStorage.setItem("fromUrl", fromUrl);
	location.href = '/ap/page/creditMoney?apSerial='+apSerial;
}