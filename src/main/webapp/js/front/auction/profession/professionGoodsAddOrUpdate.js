
$(function(){
	//iniPage();
	iniData();
});


function iniPage(){
}

function iniData(){
	var ap = lh.param.ap;
	if(!ap)return;
	loadAuctionProfessionGoods(ap.serial);
}

function loadAuctionProfessionGoods(auctionSerial){
	//, page:lh.page.currentPage, rows:lh.page.rows
	var obj = {auctionSerial:auctionSerial};
	$.post('/ag/getList', obj, function(rsp){
		if(rsp.success){
			if(rsp.rows && rsp.rows.length > 0){
				makeAuctionGoodsDom(rsp.rows, true);
				lh.page.currentPage++;
			}
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json');
}

function makeAuctionGoodsDom(dataList, isAppend){
	lh.plugins.template = $('#template').html();
	Mustache.parse(lh.plugins.template);   // optional, speeds up future uses
	var data = {
		rows:dataList,
		getPicPath:function(){
			var picPath = this.picPath;
			if(picPath){
				picPath += buildOSSZoom(120,120); //@@_OSS_IMG_@@
			}
			return picPath;
		}
    }
	var rendered = Mustache.render(lh.plugins.template, data);
	$("#agNum").text('已添加'+dataList.length+"件拍品");
	if(lh.page.currentPage > 1){
		$('#data-container').append(rendered);
	}else{
		$('#data-container').html(rendered);
	}
}

function promptUpdateItem(field){
	lh.jumpR('/ap/page/addOrUpdate?apSerial='+lh.param.ap.serial);return;//TODO 暂时直接跳转到编辑页面
	
	if(!field)return;
	var ap = lh.param.ap;
	if(field == 'auctionSerial'){
		var title = '专场场次';
		$('#weui-prompt-input').val(ap.auctionSerial);
	}
	
	$.prompt(title, function(text) { //点击确认后的回调函数,text 是用户输入的内容
	 	lh.updateFlag = true;
	 	if(!text){
	 		lh.alert();
	 	}
	 	lh.param.ap = auctionSerial;
	  }, function() {//点击取消后的回调函数
	  
	 });
}

function updateItem(){
	
}

function doSubmit(){
	var apSerial = lh.param.apSerial;
	lh.post('front', '/ap/doSubmit', {serial:apSerial},function(rsp){
		if(rsp.success){
			lh.alert('您已成功提交专场拍卖，正在审核中', '提示', function(){
				lh.jumpR('/ap/page/manage');
			});
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json', {requesting:'submitAP'});
}

function confirmRemove(agId){
	if(!agId)return;
	lh.param.currentAgId = agId;
	lh.confirm('是否确认移除该拍品？', '提示', removeAG, null);
}

function removeAG(agId){
	if(!agId)agId = lh.param.currentAgId;
	if(!agId)return;
	lh.post('front', '/ag/removeAG', {agId:agId},function(rsp){
		if(rsp.success){
			lh.param.currentAgId = null;
			$('#auction_goods_'+agId).remove();
			lh.alert('您已经成功将该拍品移除', '提示');
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json', {requesting:'removeAG'});
}