CURRENT_PAGE = 1;
ADD_FLAG = true;
DATA_OBJ = {};
$(function(){
	initData();
	getMyGoodsList();//加载拍到的专场藏品
	//lh.scrollBottom(loadAuctionGoods);//下拉加载更多数据
});

function initData(){
	DATA_OBJ.auctionId = $('#auctionId').val();
	DATA_OBJ.instTel = $('#instTel').val();
	DATA_OBJ.auctionName = $('#auctionName').val();
}

function getMyGoodsList(){
	var auctionId = DATA_OBJ.auctionId;
	if(!auctionId)return;
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {auctionId:auctionId, sc_order:'mainStatus___DESC',orderByNotA:1,page:CURRENT_PAGE,rows:20};
	$.post('/getMyAutionGoods',param,function(rsp){
		$('#loadingTip').hide();
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionGoodsDom(data,1);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}


function makeAuctionGoodsDom(auctionGoodsList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var data = getData(auctionGoodsList);
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#goodsList').append(rendered);
	}else{
		$('#goodsList').html(rendered);
	}
}

function getData(auctionGoodsList){
	//var auctionId = $('#auctionId').val();
	var data = {
		rows:auctionGoodsList,
		getInstTel:function(){
			return DATA_OBJ.instTel;
		},
		getAuctionName:function(){
			return DATA_OBJ.auctionName;
		},
		getDealTime:function(){
			return formatDate(this.dealTime,1);
		},
		getStatus:function(){
			return '未付款';
		}
		
    }
	return data;
}

function jumpToAddAuction(){
	if(ADD_FLAG){
		var instId = $('#instId').val();
		var url = '/professionAddOrUpdate?instId='+instId;
		var r = $("#r").val();
		if(r) url += "?r="+r;
		window.location.href = url;
	}else{
		lh.alert('您还有未结束的拍卖，等拍卖结束后再添加吧');
	}
}

