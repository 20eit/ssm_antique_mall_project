
CURRENT_GOODS_ID = null;
SELECTED_GOODS_ARY = [];
SAVING_FLAG = false;
TEMPLATE = null;

$(function(){
	loadProfessionSendGoods();
	initData();
});

function initData(){
    TEMPLATE = $('#template').html();
	Mustache.parse(TEMPLATE);   // optional, speeds up future uses
	
}

function loadProfessionSendGoods(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	$.post('/getProfessionSendGoods',{},function(rsp){
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionInstDom(data, true);
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				lh.alert(rps.msg);
			}
		}
	},'json');
}

function makeAuctionInstDom(goodsList,isAppend,domId){
	if(!domId)domId = '#goodsList';
	for(var i=0; i<goodsList.length; i++){
		var goods = goodsList[i];
		goods.promoteStartDate = formatDate(goods.promoteStartDate);
	}
	var data = {
					rows:goodsList
					/*,statusDom:function(){
						var statusDom = '';
						return statusDom;
					},
					gradeDom:function(){
						var gradeDom = '';
						return gradeDom;
					}*/
			   }
	var rendered = Mustache.render(TEMPLATE, data);
	if(isAppend){
		$(domId).append(rendered);
	}else{
		$(domId).html(rendered);
	}
}

function rejectSendGoods(goodsId){
	if(!goodsId)return;
	$.post('/rejectSendGoods',{goodsId:goodsId},function(rsp){
		if(rsp){
			if(rsp.success){
				lh.alert('已经成功退回该拍品', 'reloadPage');//点击确定后重新加载本页面
			}else{
				lh.alert(rps.msg);
			}
		}
	},'json');
}


