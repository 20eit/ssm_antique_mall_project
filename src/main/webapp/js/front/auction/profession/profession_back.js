
CURRENT_GOODS_ID = null;
SELECTED_GOODS_ARY = [];
SAVING_FLAG = false;
TEMPLATE = null;

$(function(){
	initUploadSimple();//初始化上传组件
	loadProfessionGoods();
	loadMyGoods();
	initData();
});

function initData(){
	var date = new Date();
	date.setMonth(date.getMonth()+1)
	var date2 = new Date();
	date2.setHours(date2.getHours()+1);
	 $("#startTime").mobiscroll().datetime({  
        theme: "android-ics light",  
        lang: "zh",  
        cancelText: '取消',  
        dateFormat: 'yy-mm-dd', //返回结果格式化为年月格式  
        dateOrder: 'yymmdd',
        timeFormat: 'HH:ii:ss', //返回结果格式化为年月格式  
        maxDate: date,
        minDate: date2,
        stepSecond:10,
        headerText: function (valueText) { //自定义弹出框头部格式  
            return '请选择离当前时间一个月内的时间';  
        }  
    });  
    
    TEMPLATE = $('#template').html();
	Mustache.parse(TEMPLATE);   // optional, speeds up future uses
	
}

function loadProfessionGoods(){
	var professionId = $('#professionId').val();
	if(!professionId)return;
	$.post('/loadProfessionGoods',{professionId:professionId},function(rsp){
		if(rsp){
			if(rsp.success){
				if(rsp.rows){
					makeAuctionInstDom(rsp.rows,true,'#auctionList');
					checkGoodsBeforeSave();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function loadMyGoods(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	$.post('/getMyGoods',{},function(rsp){
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionInstDom(data, true);
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function makeAuctionInstDom(goodsList,isAppend,domId){
	if(!domId)domId = '#goodsList';
	for(var i=0; i<goodsList.length; i++){
		var goods = goodsList[i];
		if(goods.mainStatus != 74 || !goods.auctionPrice){
			goods.auctionPrice = 0;
		}
	}
	var data = {
					rows:goodsList
					/*,statusDom:function(){
						var statusDom = '';
						return statusDom;
					},
					gradeDom:function(){
						var gradeDom = '';
						return gradeDom;
					}*/
			   }
	var rendered = Mustache.render(TEMPLATE, data);
	if(isAppend){
		$(domId).append(rendered);
	}else{
		$(domId).html(rendered);
	}
}

function selectGoods(goodsId){
	var $a = $('#auctionList');
	var $g = $('#goodsList');
	var $img = $('#img_'+goodsId);
	var $p = $img.parent();
	if($p[0].id == 'goodsList'){
		$a.append($img);
		CURRENT_GOODS_ID = goodsId;
		//$('#priceWin').show();
	}else{
		$g.append($img);
	}
	//$g.remove();
}

function showPriceBegin(goodsId){
	var $a = $('#auctionList');
	var $g = $('#goodsList');
	var $img = $('#img_'+goodsId);
	var $p = $img.parent();
	if($p[0].id == 'auctionList'){
		CURRENT_GOODS_ID = goodsId;
		$('#priceWin').show();
	}else{
		selectGoods(goodsId);
	}
}

function doPriceBegin(operation){
	if(operation == 'cancel'){
		CURRENT_GOODS_ID = null;
		$('#priceWin').hide();
	}else if(operation == 'save'){
		if(CURRENT_GOODS_ID){//save
			var priceBegin = $('#priceBegin').val();
			priceBegin = parseInt(priceBegin);
			if(priceBegin < 0)priceBegin = 0;
			if(priceBegin > 10000000){
				lh.alert('您输入的起拍价格太高，请重新输入');return;
			}
			var $price = $('#priceBegin_'+CURRENT_GOODS_ID).text(priceBegin);
			$('#priceWin').hide();
		}
	}
}

function checkGoodsBeforeSave(){
	SELECTED_GOODS_ARY = [];
	var $autions = $('#auctionList .goodsDiv');
	if($autions.length <= 0){
		lh.alert('请选择本场拍品');return false;
	}
	for(var i = 0;i<$autions.length;i++){
		var $auction = $autions[i];
		var goodsId = $auction.id.replace('img_','');
		var priceBegin = $('#priceBegin_'+goodsId).text();
		goodsId = parseInt(goodsId);
		priceBegin = parseInt(priceBegin);
		var ag = {goodsId:goodsId,priceBegin:priceBegin};
		SELECTED_GOODS_ARY.push(ag);
	}
	if(!SELECTED_GOODS_ARY || SELECTED_GOODS_ARY.length<=0){
		return false;
	}
	return true;
}


function saveProfession(){
	if(SAVING_FLAG)return;
	var payPassword = $('#payPassword').val();
	if(!payPassword){
		lh.alert('请输入支付密码');return false;
	}
	SAVING_FLAG = true;
	var paths = UPLOAD_OBJ.pathStr;
	if(paths){
		paths = paths.substring(paths.lastIndexOf(',')+1);
	}
	var instId = $('#instId').val();
	var professionId = $('#professionId').val();
	var auctionName = $('#auctionName').val();
	var startTime = $("#startTime").mobiscroll('getDate');//http://docs.mobiscroll.com/2-13-2/datetime#!method-setDate
	//var startTime = $('#startTime').val();
	var bail = $('#bail').val();
	var spreadPackets = $('#spreadPackets').val();
	if(!instId){
		SAVING_FLAG = false;
		location.href="/";return;
	}
	if(!auctionName){
		SAVING_FLAG = false;
		lh.alert('场次名称不能为空');return;
	}
	if(!startTime){
		SAVING_FLAG = false;
		lh.alert('开始时间不能为空');return;
	}
	
	var flag = checkGoodsBeforeSave();
	if(!flag){
		SAVING_FLAG = false;
		return;
	}
	var instId = $('#instId').val();
	var param = {instId:instId,auctionName:auctionName,startTime:formatDate(startTime,1,1),payPassword:payPassword};
	if(paths)param.picPaths = paths;
	if(professionId)param.id = professionId;
	if(bail)param.bail = bail;
	if(spreadPackets)param.spreadPackets = spreadPackets;
	param.auctionGoodsAry = JSON.stringify(SELECTED_GOODS_ARY);
	frontBaseLoadingOpen();//加载遮罩
	$.post('/addOrUpdateAuctionProfession',param,function(rsp){
		SAVING_FLAG = false;
		frontBaseLoadingClose();//解除遮罩
		if(rsp){
			if(rsp.success){
				var url = '/myAuctionInst';
				var r = $("#r").val();
				if(r) url += "?r="+r;
				window.location.href = url;
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}


