
$(function(){
	initWxSDK(['chooseImage','previewImage','uploadImage','downloadImage']);
	if(!lh.upload)lh.upload = {};
	if(!lh.upload.pathStr)lh.upload.pathStr ='';
	iniPage();
	initData();
	
});


function iniPage(){
	/*$("#startTime").datetimePicker({
		title: "请选择拍卖开始时间"//,
		min:function(){ return moment().format('YYYY-MM-DD'); },
		max:function(){ return moment().add(1, 'months').format('YYYY-MM-DD HH:mm'); }
	});*/
	/*$("#goodsType").select({
	  title: "选择拍品类型",
	  items: lh.param.goodsTypeAry,
	  onChange:function(){
	  	var typeCode = $("#goodsType").val();
	  	var index = typeCode.indexOf('<span>');
	  	if(index>0){
		  	typeCode = typeCode.substring(0, index);
	  		$("#goodsType").val(typeCode);
	  	}
	  	return true;
	  }
	});*/
}

function initData(){
	var ap = lh.param.ap;
	if(!ap)return;
	$("#auctionName").val(ap.auctionName);
	$("#description").val(ap.description);
	/*$("#startTime").val(moment(ap.startTime).format("YYYY-MM-DD HH:mm"));*/
	$("#goodsType").val(ap.goodsTypeName);
	/*$("#goodsType").attr('data-values', ap.goodsTypeCode);*/
	if(ap.picPaths){
		$("#picPaths").val(ap.picPaths);
		showImgDirectly(ap.serial, ap.picPaths);
	}
}

function addOrUpdateAuctionProfession(){
	//if(!lh.ajax.addAPOverFlag)return;
	/*var payPassword = $("#payPassword").val();
	if(!payPassword){
		$("#tips").text('请输入支付密码').show();
		return;
	}
	TEMPDATA.payPassword = payPassword;*/

	var auctionName = $("#auctionName").val();
	var description = $("#description").val();
//	var startTime = $("#startTime").val();
	var goodsTypeCode = $("#goodsType").val();
	alert(goodsTypeCode);
	//var picPaths = $("#picPaths").val();
	var typeId = 1;
	if(!typeId){
		lh.jumpR('/ap/page/typeChoose');return;
	}
	if(!auctionName){
		lh.alert('请填写机构名称');return;
	}
	/*if(!startTime){
		lh.alert('请选择拍卖开始时间');return;
	}*/
	/*else{
		startTime = startTime+':00';
		//var startDate = new Date(startTime);
		var flag = moment().add(1, 'days').isBefore(startTime);
		if(!flag){
			lh.alert('拍卖开始时间只能选择24小时以后的时间');return;
		}
	}*/
	var picServerId = lh.upload.pathStr;
	if(picServerId && picServerId.lastIndexOf(',')>0){
		picServerId = picServerId.substring(picServerId.lastIndexOf(',')+1, picServerId.length);
	}
	var ap = {
			auctionName:auctionName, 	 description:description, 
		 	 typeId:typeId,
			goodsTypeCode:goodsTypeCode, picServerId:picServerId
		};
	if(lh.picPath){
		ap.ossPicPath = lh.picPath;
	}
	if(lh.param.ap){
		ap.id = lh.param.ap.id;
		ap.serial = lh.param.ap.serial;
		ap.picPaths = lh.param.ap.picPaths;
	}
	lh.loading('正在保存数据');//加载遮罩
	lh.post('front', '/ap/addprofessionApply', ap,function(rsp){
		if(rsp.success){
			lh.alert(rsp.msg,lh.back);
		}else{
			if(rsp.code == 'jumpToMyShop'){
				lh.confirm(rsp.msg, "lh.jumpR('/myShop')");
			}else{
				lh.alert(rsp.msg, '提示');
			}
		}
	},'json', {requesting:'addAP'});
	
}

/**显示图片**/
function showImg(localId, serverId){
	if(!localId || !serverId)return;
	//var domId = getIdFromDate();
	var domId = serverId;
	//lh.alert('domId:'+domId+'-localId:'+localId);
	/*var dom = '<li class="weui_uploader_file" id="weui_uploader_file_'+domId+'" style="background-image:url('+localId+')">'
				+'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;bottom: 5px;background-color: black;"></i>'
			+'</li>';*/
	uploadToOSSByWxServerId(serverId);
	/*var dom = 
	'<div class="col-xs-12 pl0 pr7" id="weui_uploader_file_'+domId+'" style="padding:0px 15px;">'+
		'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;top: 8px;background-color: black;"></i>'+
		'<img src="'+localId+'" class="img-responsive" style="width:330px;max-height:120px;">'+
	'</div>';
	
	$("#fileUpload").html(dom);//将图片显示出来
*/}

function showImgDirectly(domId, picPath){
	if(!domId || !picPath)return;
	lh.picPath = picPath;
	var ossPicPath = picPath + buildOSSZoom(420,150); //@@_OSS_IMG_@@ //330,120
	var dom = 
	'<div class="col-xs-12 pl0" id="weui_uploader_file_'+domId+'" style="padding:0px 15px;">'+
		'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;top: 8px;background-color: black;"></i>'+
		'<img src="'+ossPicPath+'" class="img-responsive" style="">'+
	'</div>';
	$("#fileUpload").html(dom);//将图片显示出来
}

function uploadToOSSByWxServerId(serverId){
	lh.picPath = null;
	lh.post('front', '/uploadToOSSByWxServerId', {serverId:serverId},function(rsp){
		if(rsp.success){
			var picPath = rsp.picPaths;
			lh.picPath = picPath;
			showImgDirectly(serverId, picPath);
		}else{
			lh.alert(rsp.msg, '提示');
		}
	},'json', {requesting:'uploadToOSSByWxServerId'});
}


/**删除图片**/
function removeSelf(domId){
	lh.picPath = null;
	lh.upload.pathStr = lh.upload.pathStr.replace(','+domId, '');
	lh.upload.pathStr = lh.upload.pathStr.replace(domId, '');
	$("#weui_uploader_file_"+domId).remove();
	
	if(lh.param.ap && lh.param.ap.serial == domId){
		lh.param.ap.picPaths = null;
	}
}

