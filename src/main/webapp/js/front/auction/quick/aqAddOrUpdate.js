SAVING_FLAG = false;
COMEFROM = null;
TEMPOBJ = null;
$(function(){
	initUploadSimple();//调用完整方法
	init();
});

function init(){
	//http://silviomoreto.github.io/bootstrap-select/options/
	$('#day,#bonusTypeId,#typeId,#endTime').selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    size: 10
	});
	
	var apTypeId = $("#apTypeId").val();
	if(apTypeId){$("#typeId").selectpicker('val', apTypeId);}
	
	var date2 = new Date();
	date2.setHours(date2.getHours()+1);
	$("#startTime").mobiscroll().datetime({  
        theme: "android-ics light",  
        lang: "zh",  
        cancelText: '取消',  
        dateFormat: 'yy-mm-dd', //返回结果格式化为年月格式  
        dateOrder: 'yymmdd',
        timeFormat: 'HH:ii:ss', //返回结果格式化为年月格式  
        //maxDate: date,
        minDate: date2,
        stepSecond:10,
        headerText: function (valueText) { //自定义弹出框头部格式  
            return '请选择拍卖开始时间';  
        }  
    });  
	$("#endTime").mobiscroll().datetime({  
		theme: "android-ics light",  
		lang: "zh",  
		cancelText: '取消',  
		dateFormat: 'yy-mm-dd', //返回结果格式化为年月格式  
		dateOrder: 'yymmdd',
		timeFormat: 'HH:ii:ss', //返回结果格式化为年月格式  
		//maxDate: date,
		minDate: date2,
		stepSecond:10,
		headerText: function (valueText) { //自定义弹出框头部格式  
			return '请选择拍卖结束时间';  
		}  
	});  
}

function addAuction(){
	if(SAVING_FLAG)return;
	SAVING_FLAG = true;
	var comeFrom = $("#comeFrom").val();
	var apId = $("#apId").val();
	var asId = $("#asId").val();
	var aqId = $("#aqId").val();
	var professionId = $("#professionId").val();
	var auctionQuickId = $("#auctionQuickId").val();
	if(comeFrom == 1){
		var startTime = $("#startTime").val();
		var bail = $("#bail").val();
		var auctionName = $("#auctionName").val();
		if(!auctionName){
			lh.alert('请填写拍卖场次');
			SAVING_FLAG = false;
			return;
		}
		if(!startTime){
			lh.alert('请选择开拍时间');
			SAVING_FLAG = false;
			return;
		}
		if(!bail){
			bail = 0;
		}else{
			bail = parseInt(bail);
			if(!bail){
				lh.alert('请填写正确的保证金金额');
				SAVING_FLAG = false;
				return;
			}
		}
		$("#bail").val(bail);
	}
	if(comeFrom == 3){
		//var day = $("#day").val();
		var endTime = $("#endTime").val();
	}
	if(comeFrom == 2){
		var typeId = $("#typeId").val();
		if(!typeId){
			lh.alert('请选择拍卖类型');
			SAVING_FLAG = false;
			return;
		}
	}
	var url = "/editAuction";
	var r = $("#r").val();
	if(r) url += "?r="+r;
	localStorage.setItem("comeFrom",comeFrom);
	localStorage.setItem("apId",apId);
	localStorage.setItem("professionId",professionId);
	localStorage.setItem("auctionQuickId",auctionQuickId);
	localStorage.setItem("asId",asId);
	localStorage.setItem("aqId",aqId);
	localStorage.setItem("bail",bail);
	localStorage.setItem("startTime",startTime);
	localStorage.setItem("endTime",endTime);
	localStorage.setItem("auctionName",auctionName);
	localStorage.setItem("typeId",typeId);
	//localStorage.setItem("day",day);
	//localStorage.setItem("bonusTypeId",bonusTypeId);
	//localStorage.setItem("bonus",bonus);
	location.href = url;
}

