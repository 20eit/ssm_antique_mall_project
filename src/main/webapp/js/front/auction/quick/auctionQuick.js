CURRENT_PAGE = 1;
MUS_TEMPLATE = null;
MUS_TEMPLATE_OFFERS = null;
ASG_LIST = {};
SAVING_FLAG = false;
CURRENT_OBJ = {
	aqgId:null,
	auctionId:null,
	goodsId:null,
	currentPrice:0
};
$(function(){
	initPage();
	loadAuctionQuick();//加载专场
	lh.scrollBottom(loadAuctionQuick);//下拉加载更多数据
	autoRefresh();
});


function initPage(){
	$('.sstext').click(function () {
	    var id = $(this).attr('id');
	    if ($(this).text().trim() == '藏品') {
	        $(this).html('商铺&nbsp;<img src="/images/front/top_img5.png" width="5" height="4" />');
	        $('#' + id + '_input').attr('placeholder', '输入商铺名称');
	    } else {
	        $(this).html('藏品&nbsp;<img src="/images/front/top_img5.png" width="5" height="4" />');
	        $('#' + id + '_input').attr('placeholder', '输入藏品名称');
	    }
	})
	/*countDown("2018/8/10 23:59:59", "#demo02 .day", "#hour2", "#minute2", "#second2");
	countDown("2018/8/10 23:59:59", "#demo01 .day", "#hour1", "#minute1", "#second1");*/
	
	MUS_TEMPLATE = $('#template').html();
	Mustache.parse(MUS_TEMPLATE);   // optional, speeds up future uses
	
}

function loadAuctionQuick(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {page:CURRENT_PAGE,rows:15,notOver:1};
	var aqId = $('#aqId').val();
	if(aqId)param.auctionId = aqId;
	var typeId = $('#typeId').val();
	if(typeId)param.typeId = typeId;
	//alert('loadAuctionQuick-typeId-'+typeId);
	
	//TODO 在加载数据时就把 开拍提醒，是否关注用户的状态加载出来
	
	$.post('/getAuctionQuickList',param,function(rsp){
		$('#loadingTip').hide();
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionQuickDom(data,true);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg).show();
			}
		}
	},'json');
}

function makeAuctionQuickDom(data,isAppend){
	var data = {
		rows:data,
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)picPaths = '';
			var picsDom = '';
			var pics = picPaths.split(',');
			var length = pics.length;;
			if(length > 8)length = 8;
			for(var i=0;i<length;i++){
				//picsDom += '<li><img src="'+pics[i]+'" onclick="location.href=\'/agList/'+this.id+'\'" width="100%"/></li>';
				picsDom += '<li style="width: initial;">'
					+'<div class="img_jp">'
						+'<div class="div_gg fl">'
							+'<a href="/aqgList/'+this.id+'"><img src="'+pics[i]+'" style="max-height:90px;margin:5px;"/></a>'
						+'</div>'
					+'</div>'
					+'</li>';
			}
			return picsDom;
		},
		picNum:function(){
			var picPaths = this.picPaths;
			var pics = picPaths.split(',');
			var length = pics.length;;
			return length;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = formatDate(createdAt);
			return createdAt;
		},
		focusDom:function(){
			var isFocus = this.isFocus;
			var focusDom = '<a href="javascript:void(0);" role="1" id="userFocus_id_'+this.userId+'" class="a_gz" onclick="doFocus('+this.userId+');return false;">+关注</a>';
			if(isFocus && isFocus >= 0){
				focusDom = '<a href="javascript:void(0);" role="2" id="userFocus_id_'+this.userId+'" class="a_qxgz" onclick="doFocus('+this.userId+');return false;">取消关注</a>';
			}
			return focusDom;
		},
		noticeDom:function(){
			var isNotice = this.isNotice;
			var noticeDom = '<a id="beginNotice'+this.id+'" href="javascript:void(0);" state="1" style="color:#B93415;background-color:#383737;" onclick="toggleNotice('+this.id+');" class="a_outprice">'+
								'<img src="/images/front/day_img8.png" width="22" height="22" />&nbsp;开拍提醒'+
							'</a>';
			if(isNotice && isNotice >= 0){
				noticeDom = '<a id="beginNotice'+this.isNotice+'" href="javascript:void(0);" state="2" style="color:#B93415;background-color:#383737;" onclick="toggleNotice('+this.isNotice+');" class="a_outprice">'+
								'<img src="/images/front/day_img8.png" width="22" height="22" />&nbsp;取消提醒'+
							'</a>';
				//$notice.attr('state',2).text('取消提醒');
				//$notice.addClass('mainRedBg');
			}
			
			
			return noticeDom;
		}
    }
	var rendered = Mustache.render(MUS_TEMPLATE, data);
	isAppend = false;//临时
	if(isAppend){
		$('#auctionQuickList').append(rendered);
	}else{
		$('#auctionQuickList').html(rendered);
	}
}

function doFocus(userId){
	if(!userId)return;
	var role = $('#userFocus_id_'+userId).attr('role');
	if(role == 1){
		 focusUser(userId);
	}else{
		unFocusUser(userId);
	}
}

function focusUser(userId){
	$.post('/addOrUpdateFans', {userId:userId},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查 
			if(rsp.success){
				$('#userFocus_id_'+userId).removeClass('a_gz').addClass('a_qxgz').text('取消关注').attr('role',2);
			}
		}
	},'json');
}

function unFocusUser(userId){
	$.post('/deleteFans', {userId:userId},function(rsp){
		if(rsp){
			frontLoginCheck(rsp);//登陆检查 
			if(rsp.success){
				$('#userFocus_id_'+userId).removeClass('a_qxgz').addClass('a_gz').text('+关注').attr('role',1);
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
}

function autoRefresh(){
	 var timer = setInterval(function () {
       location.reload();
    }, 240000);//每240秒刷新一次
}

function jumpToQuickHall(){
	var status = $('#status').val();
	if(status && status == 'done'){
		lh.alert('目前没有正在拍卖的机构');return;
	}
	var url = "/auctionQuickHall";
	var typeId = $("#typeId").val();
	var r = $("#r").val();
	if(typeId){
		url += "?typeId="+typeId;
		if(r)url += "&r="+r;
	}else{
		if(r)url += "?r="+r;
	}
	window.location.href = url;
}


function showShare(){
	$('#shareMask,#share').show();
}
function hideShare(){
	$('#shareMask,#share').hide();
}


//QUICK_NOTICE_SAVING = false;
/** 开拍提醒 - 打开关闭 */
function toggleNotice(auctionId){
	if(!auctionId)return;//|| !QUICK_NOTICE_SAVING
	var $notice = $('#beginNotice'+auctionId);
	var state = $notice.attr('state');
	//var operation = 'add';
	//QUICK_NOTICE_SAVING = true;
	if(!state || state == 1){//未参与
		//var professionId = $('#professionId').val();
		$.post('/addOrUpdateTask',{typeId:39,noticeId:3,linkId:auctionId},function(rsp){
			//QUICK_NOTICE_SAVING = false;
			if(rsp){
				if(rsp.success){
					$notice.attr('state',2).text('取消提醒');
					//$notice.addClass('mainRedBg');
					//location.reload();
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}else{
		$.post('/delTask',{id:auctionId},function(rsp){
			//QUICK_NOTICE_SAVING = false;
			if(rsp){
				if(rsp.success){
					$notice.attr('state',1).text('开拍提醒');
					//$notice.removeClass('mainRedBg');
					//location.reload();
					//NOTICE_ID = null;
				}else{
					lh.alert(rsp.msg);
				}
			}
		},'json');
	}
}

