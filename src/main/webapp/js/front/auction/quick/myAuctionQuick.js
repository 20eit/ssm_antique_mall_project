CURRENT_PAGE = 1;
ADD_FLAG = true;
$(function(){
	getMyQuickList();//加载我的即时拍
	//lh.scrollBottom(loadAuctionQuick);//下拉加载更多数据
});

function getMyQuickList(){
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {sc_order:'mainStatus___DESC',orderByNotA:1,page:CURRENT_PAGE,rows:15};
	var instId = $('#instId').val();
	if(instId)param.instId = instId;
	$.post('/getMyAuctionQuickList',param,function(rsp){
		$('#loadingTip').hide();
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeAuctionQuickDom(data,1);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}


function makeAuctionQuickDom(auctionQuickList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var data = getData(auctionQuickList);
	var rendered = Mustache.render(template, data);
	if(isAppend){
		$('#auctionQuickList').append(rendered);
	}else{
		$('#auctionQuickList').html(rendered);
	}
}

function getData(auctionQuickList){
	var instId = $('#instId').val();
	var data = {
				rows:auctionQuickList,
				updateDom:function(){
					var auctionId = this.id;
					var haveBegun = this.haveBegun || 1;
					var updateDom = '';
					if(haveBegun == 1){
						var url = '/aqAddOrUpdate?instId='+instId+'&auctionId='+auctionId
						updateDom = '<button type="button" class="btn btn-success" style="position: relative;bottom: 8px;left:20px;" onclick="location.href=\''+url+'\'"> 修改 </button>';
					}else if(haveBegun == 2){
						updateDom = '<span class="colorRed" style="position: relative;left:20px;">拍卖中</span>';
					}else{
						updateDom = '<span class="colorGray" style="position: relative;left:20px;">已结束</span>';
					}
					return updateDom;
				},
				statusDom:function(){
					var haveBegun = this.haveBegun || 1;
					var statusDom = '';
					if(haveBegun == 2){
						ADD_FLAG = false;
						statusDom = '<span class="colorRed">拍卖中</span>';
					}else if(haveBegun == 1){
						ADD_FLAG = false;
						statusDom = '<span class="colorGreen">预展</span>';
					}else if(haveBegun == 3){
						statusDom = '<span class="colorGray">结束</span>';
					}
					return statusDom;
				},
				gradeDom:function(){
					var grade = this.grade;
					var gradeDom = '';
					if(grade && grade > 100){
						var grade = grade.toString();
						var diamond = grade[1];
						var star = grade[2];
						for(var j = 0;j<star;j++){
							gradeDom += '<img src="/images/front/sale_img2.png" width="12" height:"12" class="mgH1 fr"/>';
						}
						for(var i = 0;i<diamond;i++){
							gradeDom += '<img src="/images/front/diamond_blue.png" width="15" height:"15" class="fr"/>';
						}
					}
					return gradeDom;
				},
				getStartTime:function(){
					var startTime = this.startTime;
					return formatDate(startTime,1);
				},
				getEndTime:function(){
					var endTime = this.endTime;
					return formatDate(endTime,1);
				}
		   }
	return data;
}

function jumpToAddAuction(){
	if(ADD_FLAG){
		//var instId = $('#instId').val();
		//location.href='/aqAddOrUpdate?instId='+instId;
		var url = '/releaseGoods';
		var r = $("#r").val();
		if(r) url += "?r="+r;
		window.location.href = url;
	}else{
		lh.alert('您还有未结束的拍卖，等拍卖结束后再添加吧');
	}
}

