CURRENT_PAGE = 1;

$(function(){
	
	lh.plugins.template = $('#template').html();
	Mustache.parse(lh.plugins.template);   // optional, speeds up future uses
	
	lh.plugins.templateOffers = $('#template_offers').html();
	Mustache.parse(lh.plugins.templateOffers);   // optional, speeds up future uses
	getMyMicroList('ing');
});

function getMyMicroList(show){
	if(!lh.status.currentShow)lh.status.currentShow = show;
	if(lh.status.currentShow != show){//切换时清空dom，查询第一页数据
		lh.status.currentShow = show;
		lh.page.currentPage = 1;
		$('#data-container').empty();
		$('#auction_ing,#auction_done,#auction_goods').removeClass('active');
		$('#auction_'+show).addClass('active');
		
	}
	var param = {show:show, page:lh.page.currentPage, rows:lh.page.rows};
	var auctionId = lh.param.auctionId;
	//var userId = lh.param.userId;
	//if(userId)param.userId = userId;
	if(auctionId)param.auctionId = auctionId;
	lh.post('front', '/getMyAuctionMicroList', param, function(rsp){
		$('#loadingTip').hide();
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeAuctionMicroDom(data, true);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
			}
		}else{
			lh.page.currentPage = 1;
			lh.alert(rsp.msg, '提示');
		}
	},'json');
}

function makeAuctionMicroDom(data, isAppend){
	for(var i = 0; i<data.length; i++){
		var am = data[i];
		if(am.userAvatar){
			am.userAvatar += buildOSSZoom(50, 50); //@@_OSS_IMG_@@
		}else{
			am.userAvatar = '/images/front/default_avatar.png';
		}
		lh.cache.amList = _.concat(lh.cache.amList, data);
		var amoAry = am.amoList;
		if(amoAry && amoAry.length > 0){
			for(var j = 0; j < amoAry.length; j++){
				var amo = amoAry[j];
				if(amo.userAvatar){
					amo.userAvatar += buildOSSZoom(50, 50); //@@_OSS_IMG_@@
				}else{
					amo.userAvatar = '/images/front/default_avatar.png';
				}
				//amo.offerAt = moment().from(amo.offerAt); 
				amo.offerAt = lh.formatDate({date:new Date(amo.offerAt), flag:'datetime'});
				if(amo.offerPrice == am.offerPrice){
					amo.priceLead = 1;
				}
			}
		}
		if(am.postageFee == 2){
			am.postageFee = "";
		}
		if(am.isSevenReturn == 2){
			am.isSevenReturn = "";
		}
		if(data[i].creditMargin == 0){
			data[i].creditMargin = "";
		}
		var auctionId = am.id;
		autoRefreshOffer(auctionId);//自动定时刷新出价
	}
	var data = {
		rows:data,
		firstPic:function(){
			var goodsPicPaths = this.goodsPicPaths;
			if(!goodsPicPaths)return '';
			var pics = goodsPicPaths.split(',');
			var pic = pics[0];
			if(pic){
				pic += buildOSSZoom(80, 80); //@@_OSS_IMG_@@
				return pic;
			}
			return null;
		},
		picsDom:function(){
			var goodsPicPaths = this.goodsPicPaths;
			if(!goodsPicPaths)return '';
			var pics = goodsPicPaths.split(',');
			var length = pics.length;
			if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				//TODO
				//pic += '@120h_120w_1e_1c_50Q'; //OSS图片@100h_100w_1e_1c
				pic += buildOSSZoom(80, 80); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="80" /></li>';
			}
			return picsDom;
		},
		picsUrlsDom:function(){
			var goodsPicPaths = this.goodsPicPaths;
			if(!goodsPicPaths)return '';
			var pics = goodsPicPaths.split(',');
			var length = pics.length;
			var picsUrl = pics;
			var picsUrlsDom = '';
			for(var j=0;j<length;j++){
				//TODO
				//picsUrl[j] = "'"+picsUrl[j]+"@640w_50Q'";//OSS图片
				picsUrl[j] = "'"+picsUrl[j]+"'";
			}
			picsUrlsDom = 'onclick="scanPic(['+picsUrl+'])"';
			return picsUrlsDom;
		},
		/*picNum:function(){
			var goodsPicPaths = this.asg.goodsPicPaths;
			var pics = goodsPicPaths.split(',');
			var length = pics.length;;
			if(length > 9)length = 9;
			return length;
		},*/
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate(new Date(createdAt));
			return createdAt;
		},
    }
	var rendered = Mustache.render(lh.plugins.template, data);
	//isAppend = false;//临时
	if(isAppend){
		$('#data-container').append(rendered);
	}else{
		$('#data-container').html(rendered);
	}
}

function autoRefreshOffer(auctionId){
	 var timer = setInterval(function () {
       checkLastOffer(auctionId);
   	/*if (sys_second > 0) {
           clearInterval(timer);
       }*/

   }, 50000);//50000:每50秒刷新一次
}

function checkLastOffer(auctionId){
	if(!auctionId)return;
	lh.post('front', '/getAuctionMicroOffer',{auctionId:auctionId},function(rsp){
		if(rsp.success){
			var asoList = rsp.asoList;
			var freshAm = rsp.am;
			if(!asoList)return;
			var am = _.find(lh.cache.amList, {'id': auctionId});
			am = freshAm;
			am.asoList = asoList;
			freshAuctionMicroData(am);
		}
	},'json');
}

function freshAuctionMicroData(am){
	var auctionId = am.id;
	var lastOffer = am.asoList[0];
	if(lastOffer){
		console.log(lastOffer.offerPrice);
		$('#top_offer_'+auctionId).text(lastOffer.offerPrice);
	}
}

function switchCurrent(amSerial, userSerial){
	lh.current.amSerial = amSerial;
	lh.current.userSerial = userSerial;
	$('.spmc_bg,.spmc').show();
}

function updateAuction(){
	if(lh.current.amSerial){
		var am = _.find(lh.cache.amList, {'serial': lh.current.amSerial});
		if(am && am.offerTimes > 0){
			lh.alert('本场拍卖已经有客户出价，不能再进行修改');return;
		}
		lh.jumpR('/am/page/addOrUpdate?amSerial='+lh.current.amSerial);
	}else{
		lh.jumpR('/am/page/manage');
	}
}

function previewAuction(){
	var amSerial = lh.current.amSerial;
	var userSerial = lh.current.userSerial;
	var url = '/am?amSerial='+amSerial+'&userSerial='+userSerial;
	lh.jumpR(url);
	/*if(amSerial){
		lh.jumpR('/ag/page/agList/'+amSerial);return;
	}else{
		lh.jumpR('/ap/page/manage');
	}*/
}

function submitAuction(){
	lh.alert('该拍品已经上架');return;
	if(lh.current.amSerial){
		lh.post('front', '/am/doSubmit', {serial:lh.current.amSerial},function(rsp){
			if(rsp.success){
				lh.reload();
			}else{
				lh.alert(rsp.msg, '提示');
			}
		},'json');
	}
}

function confirmDelete(){
	lh.confirm('是否确定删除该场拍卖？', '提示', deleteAuction);
}

function deleteAuction(){
	if(lh.current.amSerial){
		lh.post('front', '/am/doDelete', {serial:lh.current.amSerial},function(rsp){
			if(rsp.success){
				lh.reload();
			}else{
				lh.alert(rsp.msg, '提示');
			}
		},'json');
	}
}

function doChoose(){
	/*$.actions({
	  actions: [{
	    text: "编辑",
	    onClick: function() {
	      //do something
	    }
	  },{
	    text: "删除",
	    onClick: function() {
	      //do something
	    }
	  }]
	});*/
}
