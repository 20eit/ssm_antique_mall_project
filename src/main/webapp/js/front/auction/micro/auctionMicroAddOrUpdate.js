
$(function(){
	iniPage();
	initData();
});

function iniPage(){
	$("#endTime").datetimePicker({
		title: "请选择拍卖截止时间"//,
		/*min:function(){ return moment().format('YYYY-MM-DD'); },
		max:function(){ return moment().add(1, 'months').format('YYYY-MM-DD HH:mm'); }*/
	});
	/*$("#goodsType").select({
	  title: "选择拍品类型",
	  items: lh.param.goodsTypeAry
	});*/
	
	$("#bail").select({
	  title: "请选择保证金",
	  items:[
              {title: "无",    value: 0},
              {title: "2元",   value: 2},
              {title: "10元",  value: 10},
              {title: "50元",  value: 50},
              {title: "100元", value: 100}
            ]
	});
	
}

function initData(){
	var am = lh.param.am;
	var goods = lh.param.goods;
	if(!am && !goods)return;
	//lh.am.goodsId = am.goodsId;
	//$("#description").val(am.description);
	if(am){
		$("#endTime").val(moment(am.endTime).format("YYYY-MM-DD HH:mm"));
		$("#priceBegin").val(am.priceBegin);
		$("#increaseRangePrice").val(am.increaseRangePrice);
		$("#bail").val(am.bail);
	}
	if(goods){
		$("#goodsName").val(goods.goodsName);
		$("#isSevenReturn").val(goods.isSevenReturn);
		$("#postageFee").val(goods.postageFee);
	}
}

function addOrUpdateAuctionMicro(){
	var endTime = $("#endTime").val();
	var priceBegin = $("#priceBegin").val();
	var increaseRangePrice = $("#increaseRangePrice").val();
	var postageFee = $("#postageFee").val();
	//var bail = $("#bail").val();
	var bail = $("#bail").attr('data-values');//select组件的获值方式
	var isSevenReturn = $("#isSevenReturn").prop('checked');
	if(isSevenReturn){
		isSevenReturn = 1;
	}else{
		isSevenReturn = 2;//不包退
	}
	/*if(!auctionName){
		lh.alert('请填写专场名称');return;
	}*/
	
	if(!endTime){
		lh.alert('请选择拍卖截止时间');return;
	}else{
		endTime = endTime+':00';
		//var startDate = new Date(startTime);
		var flag = moment().isAfter(endTime);
		if(flag){
			lh.alert('拍卖截止时间不能在当前时间之前');return;
		}
	}
	
	if(priceBegin && ( priceBegin < 0 || priceBegin >= 100000 )){
		lh.alert('起拍价只能在0至10万元之间');return;
	}
	if(increaseRangePrice && ( increaseRangePrice < 1 || increaseRangePrice >= 10000 )){
		lh.alert('加载幅度只能在1至1万元之间');return;
	}
	if(postageFee && ( postageFee < 0 || postageFee >= 10000 )){
		lh.alert('邮费只能在0至1万元之间');return;
	}
	if(bail && ( bail < 0 || bail >= 100000 )){
		lh.alert('保证金只能在0至10万元之间');return;
	}
	
	//var picServerId = lh.upload.pathStr;
	//if(picServerId && picServerId.lastIndexOf(',')>0)picServerId = picServerId.substring(picServerId.lastIndexOf(',')+1, picServerId.length);
	var am = {
		endTime:endTime, 	 
		priceBegin:priceBegin, 
		increaseRangePrice:increaseRangePrice, 	 	 
		postageFee:postageFee,
		bail:bail, 
		isSevenReturn:isSevenReturn
	};
	if(lh.param.am){
		am.id = lh.param.am.id;
		am.goodsId = lh.param.am.goodsId;
		am.serial = lh.param.am.serial;
	}
	if(lh.param.goods){
		am.goodsId = lh.param.goods.id;
	}
	lh.loading('正在保存数据');//加载遮罩
	lh.post('front', '/am/addOrUpdate', am,function(rsp){
		if(rsp.success){
			//var apSerial = rsp.apSerial;
			lh.jumpR('/am/page/manage');
		}else{//相同代码：auctionMicroAdd.js,auctionMicro.js
			if(rsp.code == 'creditMoney_null'){
				lh.param.moneyLack = rsp.moneyLack;
				lh.param.bail = rsp.bail;
				lh.confirm('该场拍卖需要交纳'+rsp.moneyLack+'元保证金，是否交纳保证金？', '提示', doCreditMoneyPay);
			}else if(rsp.code == 'creditMoney_lack'){
				lh.param.moneyLack = rsp.moneyLack;
				lh.param.bail = rsp.bail;
				lh.confirm('您目前交纳的保证金为'+rsp.creditMoney+'元，该场拍卖需要交纳'+rsp.bail+'元保证金，是否增加交纳保证金？', '提示', doCreditMoneyPay);
			}else if(rsp.code == 'jumpToMyShop'){
				lh.confirm(rsp.msg, "lh.jumpR('/myShop')");
			}else{
				lh.alert(rsp.msg, '提示');
			}
		}
	},'json', {requesting:'addAM'});
	
}

function doCreditMoneyPay(){
	if(lh.param.moneyLack){
		localStorage.setItem("fromUrl", location.href);
		lh.jumpR('/creditMoney?from=am&bail='+lh.param.bail);
	}
}
