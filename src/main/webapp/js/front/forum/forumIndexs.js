var pageNum = 15;//每页加载的条数
$(function() {
	loadMainList();
	rollingLoad();//滚动加载
	initPage();
	downRefresh();//下拉刷新
});

//var pageNum = 15;// 每页加载的条数
//$(function() {
//	
//	loadMainList();
//	loadComment();
//	downRefresh();// 下拉刷新
//	rollingLoad();// 滚动加载
//	
//	initPage();
//	$.mCustomScrollbar.defaults.theme = "light-2"; // set "light-2" as the// default theme
//	/*$("#ho1,#ho2").mCustomScrollbar({
//		axis : "x",
//		advanced : {
//			autoExpandHorizontalScroll : true
//		}
//	});*/
//});

//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
 		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}

//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadMainList();
	    loading = false;
	  }, 1000);   //模拟延迟
});
}

function loadComment() {
	var forumArticleId = lh.param.forumArticleId;
	var commentTypeCode = "comment_forumArticle";
	lh.post('front', '/forumIndexs', {
		page : lh.page.currentPage,
		rows : 4,
		forumArticleId : forumArticleId,
		commentTypeCode : commentTypeCode,
		forumName : 1
	}, function(rsp) {
		if (rsp.success) {
			var data1 = rsp;
			$("#comenttotal").html("全部跟帖（" + rsp.total + "）");

			makeMainListDoms(data1, true);
			lh.page.currentPage++;

		} else {
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			$(".weui-infinite-scroll").text('没有更多数据');
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function getEssenceArticle(id){
	var typeId = $("#typeId").val();
	var usersId = $("#usersId").val();
	var isJoin = $("#isJoin").val();
	var forumsId = $("#forumsId").val();
	var param = {showArticleCount:1,praiseNum:1,
	usersId:usersId,authorId:usersId,praiseType:1,forumId:forumsId,isEssence:2};
	lh.post('front', '/getForumArticleList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, false);
				lh.page.currentPage ++;
			}else{
				lh.alert('当前没有精华贴哟');
				$('#resultTip').text('没有更多数据').show();
				$(".weui-infinite-scroll").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}
function loadMainList() {
	var typeId = $("#typeId").val();
	var usersId = $("#usersId").val();
	var isJoin = $("#isJoin").val();
	var forumsId = $("#forumsId").val();
	//param.orderBy = "visit_num";
	//param.ascOrdesc = "DESC";
	var param = {page:lh.page.currentPage, rows:lh.page.rows,showArticleCount:1,praiseNum:1,
	usersId:usersId,praiseType:1,forumId:forumsId};
	if (typeId) {
		param.typeId = typeId;
	}
	
	lh.post('front', '/getForumArticleList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, true);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
				$(".weui-infinite-scroll").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function renderEvent(){
	$("#data-container .scrollbar_ul").mCustomScrollbar({
		axis : "x",
		advanced : {
			autoExpandHorizontalScroll : true
		}
	});
}

function makeMainListDom(mainList, isAppend) {
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
		rows : mainList,
		titles:function(){
			var titles = "";
			if(this.isEssence > 1){
				titles = '<img width="25" src="/images/front/jing.png" ' +
						'class="img-responsive center-block dis-lin">';
			}else if(this.isTop > 1){
				titles = '<img width="25" src="/images/front/ding.png" ' +
						'class="img-responsive center-block dis-lin">';
			}else if(this.isHot > 1){
				titles = '<img width="25" src="/images/front/re.png" ' +
						'class="img-responsive center-block dis-lin">';
			}else if(this.isRecommend > 1){
				titles = '<img width="25" src="/images/front/tui.png" ' +
						'class="img-responsive center-block dis-lin">';
			}else{
				titles = '';
			}
			return titles;
		},
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)return '';
			var pics = picPaths.split(',');
			var length = pics.length;
			//if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
			}
			return picsDom;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		},
		isPraisesOrNot:function(){
			var isPraise = this.isPraise;
			var isPraisesOrNot = "";
				isPraisesOrNot = '<input type="hidden" value="'+this.id+'"/>' +
						'<i onclick="addPraise('+this.id+');" class="icon-heart-empty"></i>&nbsp;' +
						'<span  id="praiseNum_'+this.id+'">'+this.praiseNum+'</span>';
			return isPraisesOrNot;
		},
		isManager:function(){
			var isManager = '';
			if($("#isManager").val() != 1){
			}else{
				isManager = ' <div class="col-xs-12 plr0 ptb10 text-right" id="articleController">' +
						'<a rel="button" class="btn pa010 btn-orange-remove"  onclick="updateForumArticle('+this.id+',1)">' +
						'置顶</a><a rel="button" class="btn pa010 btn-orange-remove"  ' +
						'onclick="updateForumArticle('+this.id+',2)">精华</a>' +
						'<a rel="button" class="btn pa010 btn-orange-remove" ' +
						' onclick="updateForumArticle('+this.id+',3)">热门</a></div>';
			}
			return isManager;
		},
		shai:function(){
			var typeCode = this.typeCode;
			var shai = '';
			if(typeCode == 'shaihaowu'){
				var goodsName = getOrderGoods(this.typeId,this.id);
				shai = "分享藏品:<span id='showGoodsName_"+this.id+"'></span>";
			}
			return shai;
		},
		userAvatars:function(){
			var userAvatars = '';
			var pic = this.userAvatar;
			pic += buildOSSZoom(35,35);
			userAvatars += '<img width="30" src="'+pic+'" class="img-responsive center-block">';
			
			return userAvatars;
		}	
	});
	if (isAppend) {
		$('#data-container').append(rendered);
	} else {
		$('#data-container').html(rendered);
	}
	renderEvent();
	checkDom();
}
function getOrderGoods(goodsId,ids){
	var parmar = {id:goodsId};
		lh.post('front', '/getOrderGoodsById', parmar, function(rsp) {
			$("#showGoodsName_"+ids).text(rsp.orderGoods.goodsName);
		},'json');
}
function checkDom(){
	var domNum = $(".mt6").size()
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<15){
		$(".weui-infinite-scroll").text("没有更多数据");
	}
}

//function deleteOrAddUserPraise(){
//	//取消赞
//	$(".icon-heart-emptys").click(function(){
//		var praiseId = $(this).parent().find("input").val();
//		var params = {praiseId:praiseId};
//		lh.post('front', '/deleteUserPraise', params, function(rsp){
//			if(rsp.success == "success"){
//				lh.jumpR("/forumIndexs/"+$("#forumsId").val()+"");
//			}
//		});
//	});
//	//点赞
//	$(".icon-heart-empty").click(function(){
//		var praiseId = $(this).parent().find("input").val();
//		var params = {praiseId:praiseId,praiseType:1};
//		lh.post('front', '/addUserPraise', params, function(rsp){
//			if(rsp.success == "success"){
//				lh.jumpR("/forumIndexs/"+$("#forumsId").val()+"");
//			}
//		});
//	});
//}
function gongGao(){
	var forumsId = $("#forumsId").val();
	lh.jumpR('/announcement/'+forumsId);
}
function GongGaoInfo(anId){
	var text = $("#gongGao").text();
	var forumsId = $("#forumsId").val();
	if($.trim(text) == $.trim('当前没有公告')){
	}else{
		lh.jumpR('/announcementInfo/'+anId);
	}
}

function joinForum(id){
	var userId = $("#usersId").val();
	var params = {forumId:id,userId:userId};
	lh.post('front', '/joinForum', params, function(rsp) {
	if(rsp.status == "success"){
		$(".modelAdd").show(300);
		$('.modelAdd').delay(1000).hide(300);
		}
		lh.jumpR("/forumIndexs/"+id);
	});
						
	$(this).removeClass("btn-add").addClass("btn-tc").html("退出");
	var recommendCircle = $(this).parent().parent().parent().html();
	$(".myCircle").append("<div class='col-xs-12 ptb10 bdb'>"
						+ recommendCircle + "</div>");
	$(this).parent().parent().parent().remove();
}
//TODO 社区导航
$("#navigation  li").click(function(){
	 var index = $(this).index()+1;
	 $("#navigation  li").attr("class","col4");
	switch (index){
		case 1:
		$(this).attr("class","col4 active");
		lh.jumpR('/forumIndex');
		break;
		case 2:
		$(this).attr("class","col4 active");
		lh.jumpR("/hotForumSquare");
		break;
		case 3:
		$(this).attr("class","col4 active");
		lh.jumpR("/myForumSquare");
		break;
		case 4:
		$(this).attr("class","col4 active");
		lh.jumpR("/forumSquare");
		break;
	}
});
function initPage(){
	var isJoin = $("#isJoin").val();
	var forumsId = $("#forumsId").val();
		if(isJoin > 0){
		$("#isJionedText").text("圈子首页");
		$("#isJionedImg").attr("src","/images/front/icon_ok.png");
	}else{
		$("#isJionedText").text("加入圈子");
		$("#isJionedImg").attr("src","/images/front/add_white.png");
		$("#joinForum").click(function(){
			joinForum(forumsId);
		});
	}
	
}
function updateForumArticle(id,type){
	var params = '';
	var imgSrc = '';
	if(type==1){
		imgSrc = '/images/front/ding.png';
		params = {id:id,isEssence:0,isTop:2,isHot:0};
	}else if(type==2){
		imgSrc = '/images/front/jing.png';
		params = {id:id,isEssence:2,isTop:0,isHot:0};
	}else if(type==3){
		imgSrc = '/images/front/re.png';
		params = {id:id,isEssence:0,isTop:0,isHot:2};
	}
	lh.post('front', '/updateForumArticle', params, function(rsp){
		if(rsp.success){
			if($('#title_'+id +'> img').length>0){
				$('#title_'+id+'> img').attr('src', imgSrc);
			}else{
				$('#title_'+id + '>a').before('<img width="25" src="'+imgSrc+'" ' +
						'class="img-responsive center-block dis-lin">');
			}
		}else{
			lh.alert(rsp.msg);
		}
	});
}
function loadForumArticle(userId){
	var preUrl = window.location.href;
	localStorage.setItem('forum_user_preUrl', preUrl);
	lh.jumpR("/forumUserArticle/"+userId);
}

function addPraise(id){
	var params = {praiseId:id,praiseType:1};
	lh.post('front', '/addUserPraise', params, function(rsp){
			if(rsp.success == "success"){
				if(rsp.code == 'alreadyExist_error'){
					lh.alert('已经点过赞了');return;
				}
				var num = $('#praiseNum_'+id).text();
				if(!num)num = 0;
				$('#praiseNum_'+id).text(++num);
			}else{
				lh.alert(rsp.msg);
			}
		});
}
//function loadcircleSquareList(){
//	
//}

