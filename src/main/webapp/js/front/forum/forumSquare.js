var pageNum = 6;//每页加载的条数
$(function() {
	loadMainList();
	rollingLoad();//滚动加载
	downRefresh();//下拉刷新
});


//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
 		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}

//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadMainList();
	    loading = false;
	  }, 1000);   //模拟延迟
});
}

function loadMainList() {
	var typeId = $("#typeId").val();
	var userId = $("#usersId").val();
	var param = {page:lh.page.currentPage, rows:pageNum, showMemberCount:1,sessionUsersId:userId};
	if (typeId) {
		param.typeId = typeId;
	}
	lh.post('front', '/getForumList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, true);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
				$(".weui-infinite-scroll").text("没有更多数据");
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function makeMainListDom(mainList, isAppend) {
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
		rows : mainList,
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)return '';
			var pics = picPaths.split(',');
			var length = pics.length;
			//if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
			}
			return picsDom;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		},
		isJoinOrBack:function(){
			var isJoin = this.isJoin;
			
			var isJoinOrBack = '';
				if(isJoin == 1){
				isJoinOrBack = '<div class="col-xs-2 plr0 pt20"><a rel="button" onclick="backForum('+this.id+')" class="btn btn-xs btn-tc">退出</a>' +
						'<div class="fs12 gray pt5">'+this.memberCount+'人</div></div>';
			}else if(isJoin == 0){
				isJoinOrBack =  '<div class="col-xs-2 plr0 pt10"><a rel="button" onclick="joinForum('+this.id+')" class="btn btn-xs btn-add">加入</a>' +
						'<div class="fs12 gray pt5">'+this.memberCount+'人</div></div>';
			}
			
			return isJoinOrBack;
		},
		logos:function(){
				var pic = this.logo;
				var logos = '';
				pic += buildOSSZoom(70, 70);
				logos += '<img src="'+pic+'" class="img-responsive center-block">';
				return logos;
			}
	});
	if (isAppend) {
		$('#data-container').append(rendered);
	} else {
		$('#data-container').html(rendered);
	}
	checkDom();
//	renderEvent();
}
function checkDom(){
	var domNum = $(".mt6").size();
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<5){
		$(".weui-infinite-scroll").text("没有更多数据");
	}
}


//TODO 社区导航
$("#navigation  li").click(function(){
	 var index = $(this).index()+1;
	 $("#navigation  li").attr("class","col4");
	switch (index){
		case 1:
		$(this).attr("class","col4 active");
		lh.jumpR("/forumIndex");
		break;
		case 2:
		$(this).attr("class","col4 active");
		lh.jumpR("/hotForumSquare");
		break;
		case 3:
		$(this).attr("class","col4 active");
		lh.jumpR("/myForumSquare");
		break;
		case 4:
		$(this).attr("class","col4 active");
		lh.jumpR("/forumSquare");
		break;
	}
});


function backForum(id){
	var userId = $("#usersId").val();
	var params = {forumId:id,userId:userId};
	lh.post('front', '/backForum', params, function(rsp) {
	if(rsp.status == "success"){
		$(".modelTc").show(300);
		$('.modelTc').delay(1000).hide(300);
		}
		lh.jumpR("/myForumSquare");
		lh.jumpR("/myForumSquare");
		});
				
	$(this).parent().parent().parent().remove();	
}
function joinForum(id){
	var userId = $("#usersId").val();
	var params = {forumId:id,userId:userId};
	lh.post('front', '/joinForum', params, function(rsp) {
	if(rsp.status == "success"){
		$(".modelAdd").show(300);
		$('.modelAdd').delay(1000).hide(300);
		}
		lh.jumpR("/myForumSquare");
	});
						
	$(this).removeClass("btn-add").addClass("btn-tc").html("退出");
	var recommendCircle = $(this).parent().parent().parent().html();
	$(".myCircle").append("<div class='col-xs-12 ptb10 bdb'>"
						+ recommendCircle + "</div>");
	$(this).parent().parent().parent().remove();
}
//function renderEvent(){
//	$("#data-container .scrollbar_ul").mCustomScrollbar({
//		axis : "x",
//		advanced : {
//			autoExpandHorizontalScroll : true
//		}
//	});
//}




