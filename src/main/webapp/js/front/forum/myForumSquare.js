var pageNum = 5;//每页加载的条数
$(function() {
	loadMainList(1);
	rollingLoad();//滚动加载
	loadRecommendList(2);
	downRefresh();//下拉刷新
});

//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}
//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
		loadRecommendList(2);
	    loading = false;
	  }, 1000);   //模拟延迟
});
}
//加载推荐圈子
function loadRecommendList(type) {
	var typeId = $("#typeId").val();
	var usersIds = $("#usersId").val();
	var template = $('#templates').html();
	//param.orderBy = "visit_num";
//	param.ascOrdesc = "DESC";
	var param = {page:lh.page.currentPage, rows:pageNum,showMemberCount:1,
	userIdForExceptHaveJoined:usersIds,articleMemberCount:1};
	if (typeId) {
		param.typeId = typeId;
	}
	lh.post('front', '/getForumList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, true,template,type);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
				$(".weui-infinite-scroll").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}
//加载我的圈子
function loadMainList(type) {
	var typeId = $("#typeId").val();
	var usersId = $("#usersId").val();
	var template = $('#template').html();
	//param.orderBy = "visit_num";
//	param.ascOrdesc = "DESC";
	var param = {forumMemberUserId:usersId,showMemberCount:1
	,articleMemberCount:1};
	if (typeId) {
		param.typeId = typeId;
	}
	lh.post('front', '/getForumList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, true,template,type);
			}else{
				$(".weui-infinite-scroll").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}


function makeMainListDom(mainList, isAppend,template,type) {
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
		rows : mainList,
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)return '';
			var pics = picPaths.split(',');
			var length = pics.length;
			//if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
			}
			return picsDom;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		},
		logos:function(){
				var pic = this.logo;
				var logos = '';
				pic += buildOSSZoom(70, 70);
				logos += '<img src="'+pic+'" class="img-responsive center-block">';
				return logos;
			}
	});
	if(type=='1'){
		if (isAppend) {
			$('#data-container').append(rendered);
		} else {
			$('#data-container').html(rendered);
		}
	}else{
		if (isAppend) {
			$('#data-containers').append(rendered);
		} else {
			$('#data-containers').html(rendered);
		}
		checkDom();
	}
//	renderEvent();
	
}

function checkDom(){
	var domNum = $(".mt6").size();
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<5){
		$(".weui-infinite-scroll").text("没有更多数据");
	}
}


//TODO 社区导航
$("#navigation  li").click(function(){
	 var index = $(this).index()+1;
	 $("#navigation  li").attr("class","col4");
	switch (index){
		case 1:
		$(this).attr("class","col4 active");
		lh.jumpR("/forumIndex");
		break;
		case 2:
		$(this).attr("class","col4 active");
		lh.jumpR("/hotForumSquare");
		break;
		case 3:
		$(this).attr("class","col4 active");
		lh.jumpR("/myForumSquare");
		break;
		case 4:
		$(this).attr("class","col4 active");
		lh.jumpR("/forumSquare");
		break;
	}
});


function backForum(id){
	var userId = $("#usersId").val();
	var params = {forumId:id,userId:userId};
	lh.post('front', '/backForum', params, function(rsp) {
	if(rsp.status == "success"){
		$(".modelTc").show(300);
		$('.modelTc').delay(1000).hide(300);
		}
		lh.jumpR("/myForumSquare");
		lh.jumpR("/myForumSquare");
		});
				
	$(this).parent().parent().parent().remove();	
}
function joinForum(id){
	var userId = $("#usersId").val();
	var params = {forumId:id,userId:userId};
	lh.post('front', '/joinForum', params, function(rsp) {
	if(rsp.status == "success"){
		$(".modelAdd").show(300);
		$('.modelAdd').delay(1000).hide(300);
		}
		lh.jumpR("/myForumSquare");
	});
						
	$(this).removeClass("btn-add").addClass("btn-tc").html("退出");
	var recommendCircle = $(this).parent().parent().parent().html();
	$(".myCircle").append("<div class='col-xs-12 ptb10 bdb'>"
						+ recommendCircle + "</div>");
	$(this).parent().parent().parent().remove();
}