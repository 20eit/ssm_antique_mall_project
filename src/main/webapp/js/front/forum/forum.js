$(function() {
	loadForumList();
	$.mCustomScrollbar.defaults.theme = "light-2"; // set "light-2" as the// default theme
	/*$("#ho1,#ho2,#ho3").mCustomScrollbar({
		axis : "x",
		advanced : {
			autoExpandHorizontalScroll : true
		}
	});*/
});

function loadForumList() {
	var typeId = $("#typeId").val();
	//param.orderBy = "visit_num";
	//param.ascOrdesc = "DESC";
//	param.orderBy = "visit_num";
//	param.ascOrdesc = "DESC";
	var param = {page:lh.page.currentPage, rows:lh.page.rows};
	if (typeId) {
		param.typeId = typeId;
	}
	
	var forumId = lh.param.forumId;
	
	lh.post('front', '/getForumArticleList', {forumId:forumId}, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			
			if(data && data.length>0){
				makeMainListDom(data, true);
				lh.page.currentPage ++;
			}else{
				$('#resultTip').text('没有更多数据').show();
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function renderEvent(){
	$("#data-container .scrollbar_ul").mCustomScrollbar({
		axis : "x",
		advanced : {
			autoExpandHorizontalScroll : true
		}
	});
}

function makeMainListDom(mainList, isAppend) {
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
		rows : mainList,
		//{{&picsDom}}
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)return '';
			var pics = picPaths.split(',');
			var length = pics.length;
			//if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
			}
			return picsDom;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		}
	});
	if (isAppend) {
		$('#data-container').append(rendered);
	} else {
		$('#data-container').html(rendered);
	}
	renderEvent();
}
