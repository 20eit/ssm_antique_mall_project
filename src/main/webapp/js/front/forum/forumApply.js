$(function() {
	initWxSDK(['chooseImage','previewImage','uploadImage','downloadImage']);
});

/**显示图片**/
function showImg(localId, serverId){
	if(!localId || !serverId)return;
	//var domId = getIdFromDate();
	var domId = serverId;
	//lh.alert('domId:'+domId+'-localId:'+localId);
	/*var dom = '<li class="weui_uploader_file" id="weui_uploader_file_'+domId+'" style="background-image:url('+localId+')">'
				+'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;bottom: 5px;background-color: black;"></i>'
			+'</li>';*/
	var dom = 
	'<div class="col-xs-12 plr0" id="weui_uploader_file_'+domId+'">'+
		'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;top: 22px;background-color: black;"></i>'+
		'<img src="'+localId+'" class="img-responsive">'+
	'</div>';
	$("#fileUpload").html(dom);//将图片显示出来
}

/**删除图片**/
function removeSelf(domId){
	lh.upload.pathStr = lh.upload.pathStr.replace(','+domId, '');
	lh.upload.pathStr = lh.upload.pathStr.replace(domId, '');
	$("#weui_uploader_file_"+domId).remove();
}

function addForum() {
	var attr1 = $("#attr1").val();
	var attr2 = $("#attr2").val();
	var attr3 = $("#attr3").val();
	var attr4 = $("#attr4").val();
	
	if(lh.upload && lh.upload.pathStr){
		var filePaths = lh.upload.pathStr;
		if(_.startsWith(filePaths, ',')){
			filePaths = filePaths.substring(1);
		}
		if(filePaths.indexOf(',')>0){
			filePaths = filePaths.substring(filePaths.lastIndexOf(','));
		}
		filePaths = filePaths.substring(1);
	}
	
	if(!attr1 || attr1.length<2 || attr1.length>30){
		lh.alert('请输入圈子名称，字数在2至30个之间');return;
	}
	
	var param = {attr1:attr1,attr2:attr2,attr3:attr3,attr4:attr4};
	if(filePaths)param.file1 = filePaths;
	lh.post('front', '/addForum', param, function(rsp) {
		if(rsp.success){
			lh.alert("申请圈子成功，正在等待审核", jumpToForumIndex);
		}else{
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function jumpToForumIndex(){
	lh.jumpR("/forumIndex");
}



