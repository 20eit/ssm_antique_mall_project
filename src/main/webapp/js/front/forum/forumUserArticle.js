var pageNum = 15;//每页加载的条数
$(function() {
	loadMainList();
	rollingLoad();//滚动加载
	downRefresh();//下拉刷新
});

//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
 		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}

//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadMainList();
	    loading = false;
	  }, 1000);   //模拟延迟
});
}
//$(function() {
//			loadMainList();
//			$.mCustomScrollbar.defaults.theme = "light-2"; // set "light-2" as
//															// the// default theme
//			getDate();
//
//		});

function loadMainList() {
	var typeId = $("#typeId").val();
	var userId = $("#userId").val();
	// param.orderBy = "visit_num";
	// param.ascOrdesc = "DESC";
	var param = {
		page : lh.page.currentPage,
		rows : lh.page.rows,
		showArticleCount : 1,
		praiseNum : 1,
		authorId:userId
	};
	if (typeId) {
		param.typeId = typeId;
	}
	if (lh.param.userSerial) {
		param.userSerial = lh.param.userSerial;
	}
	lh.post('front', '/getForumArticleList', param, function(rsp) {
				if (rsp.success) {
					var data = rsp.rows;
					if (data && data.length > 0) {
						makeMainListDom(data, true);
						lh.page.currentPage++;
					} else {
						$('#resultTip').text('没有更多数据').show();
						$(".weui-infinite-scroll").text('没有更多数据');
					}
				} else {
					lh.page.currentPage = 1;
					$('#resultTip').text(rsp.msg).show();
					lh.alert(rsp.msg);
				}
			}, 'json');
}

function renderEvent() {
	$("#data-container .scrollbar_ul").mCustomScrollbar({
				axis : "x",
				advanced : {
					autoExpandHorizontalScroll : true
				}
			});
}

function makeMainListDom(mainList, isAppend) {
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
				rows : mainList,
				picsDom : function() {
					var picPaths = this.picPaths;
					if (!picPaths)
						return '';
					var pics = picPaths.split(',');
					var length = pics.length;
					// if(length > 9)length = 9;
					var picsDom = '';
					for (var i = 0; i < length; i++) {
						var pic = pics[i];
						pic += buildOSSZoom(100, 100); // @@_OSS_IMG_@@
						picsDom += '<li><img src="' + pic
								+ '" width="100" /></li>';
					}
					return picsDom;
				},
				date : function() {
					var createdAt = this.createdAt;
					createdAt = lh.formatDate({
								date : new Date(createdAt),
								flag : 'datetime'
							});
					return createdAt;
				},
				isPraisesOrNot : function() {
					var isPraise = this.isPraise;
					var isPraisesOrNot = "";
					isPraisesOrNot = '<input type="hidden" value="' + this.id
							+ '"/>' + '<i onclick="addPraise(' + this.id
							+ ');" class="icon-heart-empty"></i>&nbsp;'
							+ '<span  id="praiseNum_' + this.id + '">'
							+ this.praiseNum + '</span>';
					return isPraisesOrNot;
				},
		shai:function(){
			var typeCode = this.typeCode;
			var shai = '';
			if(typeCode == 'shaihaowu'){
				var goodsName = getOrderGoods(this.typeId,this.id);
				shai = "<span class='orange'>已入手</span>分享自:<span id='showGoodsName_"+this.id+"'></span>";
			}
			return shai;
		},
		userAvatars:function(){
			var userAvatars = '';
			var pic = this.userAvatar;
			pic += buildOSSZoom(35,35);
			userAvatars += '<img width="30" src="'+pic+'" class="img-responsive center-block">';
			
			return userAvatars;
		}			
			});
	if (isAppend) {
		$('#data-container').append(rendered);
	} else {
		$('#data-container').html(rendered);
	}
	renderEvent();
	// deleteOrAddUserPraise();
	checkDom();
}

function getOrderGoods(goodsId,ids){
	var parmar = {id:goodsId};
		lh.post('front', '/getOrderGoodsById', parmar, function(rsp) {
			$("#showGoodsName_"+ids).text(rsp.orderGoods.goodsName);
		},'json');
}

function checkDom(){
	var domNum = $(".mt6").size()
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<15){
		$(".weui-infinite-scroll").text('没有更多数据');
	}
}

function addPraise(id) {
	var params = {
		praiseId : id,
		praiseType : 1
	};
	lh.post('front', '/addUserPraise', params, function(rsp) {
				if (rsp.success == "success") {
					if (rsp.code == 'alreadyExist_error') {
						lh.alert('已经点过赞了');
						return;
					}
					var num = $('#praiseNum_' + id).text();
					if (!num)
						num = 0;
					$('#praiseNum_' + id).text(++num);
				} else {
					lh.alert(rsp.msg);
				}
			});
}

function deleteOrAddUserPraise() {
	// 取消赞
	$(".icon-heart-emptys").click(function() {
				var praiseId = $(this).parent().find("input").val();
				var params = {
					praiseId : praiseId
				};
				lh.post('front', '/deleteUserPraise', params, function(rsp) {
							if (rsp.success == "success") {
								lh.jumpR("/forumIndex");
							}
						});
			});
	// 点赞
	$(".icon-heart-empty").click(function() {
				var praiseId = $(this).parent().find("input").val();
				var params = {
					praiseId : praiseId,
					praiseType : 1
				};
				lh.post('front', '/addUserPraise', params, function(rsp) {
							if (rsp.success == "success") {
								lh.jumpR("/forumIndex");
							}
						});
			});
}
function getDate() {
	var createdAt = $("#cheatedAt").val();
	createdAt = lh.formatDate({
				date : new Date(createdAt),
				flag : 'datetime'
			});
	var curdate = moment();
	var days = curdate.diff(createdAt, 'days');
	$("#days").html("已在江湖混迹" + days + "天");
}
function loadBackUrl() {
	var preUrl = localStorage.getItem('forum_user_preUrl');
	if (preUrl) {
		lh.jump(preUrl);
	}
}
