
$(function(){
	//initWxSDK(['chooseImage','previewImage','uploadImage','downloadImage'], 'jumpToUserCenter', choosePic);//TODO 测试
	initWxSDK(['chooseImage','previewImage','uploadImage','downloadImage']);
	if(!lh.upload)lh.upload = {};
	if(!lh.upload.pathStr)lh.upload.pathStr ='';
	iniPage();
	iniData();
});


function iniPage(){
	$("#typeCode").select({
	  title: "选择拍品类型",
	  items: lh.param.goodsTypeAry,
	  onChange:function(){
	  	var typeCode = $("#typeCode").val();
	  	var index = typeCode.indexOf('<span>');
	  	if(index>0){
		  	typeCode = typeCode.substring(0, index);
	  		$("#typeCode").val(typeCode);
	  	}
	  	return true;
	  }
	});
}

function iniData(){
	var from = lh.param.from;
	if(from){
		if(from == 'am'){
			lh.param.pageFrom = 'am';
		}else{
			var param = from.split('-');
			lh.param.pageFrom = param[0];
			lh.param.paramSerial = param[1];
			if(lh.param.pageFrom == 'ap' && lh.param.paramSerial){
			}else{
				$("#shopPriceGap,#shopPriceDiv").show();
				$("#postageFeeGap,#postageFeeDiv").show();
			}
		}
	}else{
		$("#shopPriceGap,#shopPriceDiv").show();
		$("#postageFeeGap,#postageFeeDiv").show();
	}
	//getMyGoodsList('all');
}
/**显示图片**/
function showImg(localId, serverId){
	if(!localId || !serverId)return;
	//var domId = getIdFromDate();
	var domId = serverId;
	//lh.alert('domId:'+domId+'-localId:'+localId);
	/*var dom = '<li class="weui_uploader_file" id="weui_uploader_file_'+domId+'" style="background-image:url('+localId+')">'
				+'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;bottom: 5px;background-color: black;"></i>'
			+'</li>';*/
	var dom = 
	'<div class="col-xs-3 pl0 pr7" id="weui_uploader_file_'+domId+'">'+
		'<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;top: 22px;background-color: black;"></i>'+
		'<img src="'+localId+'" class="img-responsive">'+
	'</div>';
	$("#fileUpload").append(dom);//将图片显示出来
}

/**删除图片**/
function removeSelf(domId){
	lh.upload.pathStr = lh.upload.pathStr.replace(','+domId, '');
	lh.upload.pathStr = lh.upload.pathStr.replace(domId, '');
	$("#weui_uploader_file_"+domId).remove();
}

//function addUploadGoodsToAP(){
//	if(lh.param.pageFrom == 'ap' && lh.param.paramSerial){
//		addGoods({from:'ap', apSerial:lh.param.paramSerial});
//	}
//}

function buildGoods(){
	//TODO 测试数据
	//var goods = {goodsName:'test', priceBegin:100, goodsBrief:'test',typeCode:'goods_type_yushi',remainNumber:10, picPaths:'/images/front/pic_01.png'};
	//return goods;
	var content = $("#goodsIntroduce").val();
	var typeCode = 'shaihaowu';
	var typeId = $("#goodsId").val();
	var forumId = $("#forumId").val(); 
	var userId =  $("#userId").val();
	var authorId = $("#userId").val();
	/*if(!lh.upload || !lh.upload.pathStr){lh.alert('请上传产品图片');return;}*/
	var filePaths = lh.upload.pathStr;
	if(!content){lh.alert('请填写使用心得');return;}
	
	if(content.length>1000){lh.alert('使用心得不能超过1000个字');return;}
	
	/*if(!filePaths){lh.alert('请上传产品图片');return;}*/
	if(_.startsWith(filePaths, ',')){
		filePaths = filePaths.substring(1);
	}
	var filePathArr = filePaths.split(",");
	if(filePathArr.length > 9){lh.alert('最多只能上传9张图片');return;} 
	var article = {content:content,title:content,typeCode:typeCode,typeId:typeId,forumId:forumId,
	userId:userId,authorId:authorId, picPaths:filePaths};
	return article;
}
function addGoods(paramObj){
	var article = buildGoods();
	if(!article)return;
	/*if(paramObj){
		goods = _.assign(goods, paramObj);//合并对象
	}*/
	
	if(lh.param.pageFrom == 'ap' && lh.param.paramSerial){
		goods.apSerial = lh.param.paramSerial;
	}
	lh.loading('正在保存数据');//加载遮罩
	lh.post('front', '/addActicle', article,function(rsp){
		if(rsp.success){
			jumpToAmAddOrUpdate();
			var apSerial = rsp.apSerial;
		}else{
			lh.alert(rsp.msg, '提示', lh.reload);
		}
	},'json');
}

function jumpToAmAddOrUpdate(){
	lh.jumpR('/forumIndexs/'+$("#forumId").val());
}

