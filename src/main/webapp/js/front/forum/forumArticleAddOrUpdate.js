$(function() {
			initWxSDK(['chooseImage', 'previewImage', 'uploadImage',
					'downloadImage']);
			if (!lh.upload)
				lh.upload = {};
			if (!lh.upload.pathStr)
				lh.upload.pathStr = '';
			iniPage();
			iniData();
		});

function iniPage() {
	var forumId = lh.param.forumId;
	$("#userId").val(lh.param.user.id);
	lh.post('front', '/getForum', {
				forumId : forumId
			}, function(rsp) {
				var data = rsp;
				$("#forumName").html("分享到： "+rsp.forum.name);
			}, 'json');
}

function iniData() {
	var mainObj = lh.param.forumArticle;
	if (mainObj) {
		$("#title").val(mainObj.title);
		$("#content").val(mainObj.content);
	}
}

/** 显示图片* */
function showImg(localId, serverId) {
	if (!localId || !serverId)
		return;
	// var domId = getIdFromDate();
	var domId = serverId;
	// lh.alert('domId:'+domId+'-localId:'+localId);
	/*
	 * var dom = '<li class="weui_uploader_file"
	 * id="weui_uploader_file_'+domId+'" style="background-
	 * 
	 * image:url('+localId+')">' +'<i class="weui_icon_cancel fr"
	 * onclick="removeSelf(\''+domId+'\')"
	 * 
	 * style="position: relative;bottom: 5px;background-color: black;"></i>' +'</li>';
	 */
	var dom = '<div class="col-xs-4 pl7 pt5" id="weui_uploader_file_'+ domId+ '">'
			+ '<i class="weui_icon_cancel fr" onclick="removeSelf(\''+domId+'\')"  style="position: relative;top: 22px;background-color: black;"></i>'
			+ '<img src="' + localId + '" class="img-responsive">' + '</div>';
	$("#fileUpload").append(dom);// 将图片显示出来
}

/** 删除图片* */
function removeSelf(domId) {
	lh.upload.pathStr = lh.upload.pathStr.replace(',' + domId, '');
	lh.upload.pathStr = lh.upload.pathStr.replace(domId, '');
	$("#weui_uploader_file_" + domId).remove();
}

function buildMainObj() {

	var title = $("#title").val();
	var content = $("#content").val();
	var userId = $("#userId").val();
	if (!title) {
		lh.alert('请填写标题');
		return;
	}
	if (!content) {
		lh.alert('请填写内容');
		return;
	}

	if (title.length > 30) {
		lh.alert('标题不能超过30个字');
		return;
	}
	if (content.length > 1000) {
		lh.alert('内容不能超过1000个字');
		return;
	}

	if (lh.upload && lh.upload.pathStr) {
		var filePaths = lh.upload.pathStr;
		if (filePaths) {
			if (_.startsWith(filePaths, ',')) {
				filePaths = filePaths.substring(1);
			}
			var filePathArr = filePaths.split(",");
			if (filePathArr.length > 6) {
				lh.alert('最多只能上传6张藏品图片');
				return;
			}
		}
	}
	var mainObj = {
		title : title,
		content : content,
		authorId : userId
	};
	if (filePaths)
		mainObj.picPaths = filePaths;
	var forumId = lh.param.forumId;
	mainObj.forumId = forumId;
	mainObj.typeCode = "huati";
    mainObj.userId=$("#userId").val()
	// mainObj.authorId=lh.param.userId;
	return mainObj;

}

function addMainObj() {
	var mainObj = buildMainObj();
	if (!mainObj)
		return;
	if (lh.param.forumArticle) {
		mainObj.id = lh.param.forumArticle.id;
	}
	lh.loading('正在保存数据');// 加载遮罩
	lh.post('front', '/addActicle', mainObj, function(rsp) {
				if (rsp.success) {
					lh.alert('您已成功发布话题', lh.back);
				} else {
					lh.alert(rsp.msg, '提示', lh.reload);
				}
			}, 'json', {
				requesting : 'addOrUpdateForumArticle'
			});
}

function setShowLength(obj, maxlength, id) {
	var rem = maxlength - obj.value.length;
	var wid = id;
	if (rem < 0) {
		rem = 0;
	}
	document.getElementById(wid).innerHTML = "还可以输入" + rem + "字数";
}
