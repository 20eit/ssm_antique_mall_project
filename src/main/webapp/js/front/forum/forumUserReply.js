
var pageNum = 10;//每页加载的条数
$(function() {
	loadMainList();
	$.mCustomScrollbar.defaults.theme = "light-2"; // set "light-2" as the// default theme
	getDate();
	rollingLoad();//滚动加载
	downRefresh();//下拉刷新
});
//下拉刷新
function downRefresh(){
	$("#weui-layer").show();
	$(document.body).pullToRefresh();
	$(document.body).on("pull-to-refresh", function() {
		lh.reload();
	});
	$(document.body).pullToRefreshDone();
}
//滚动加载
function rollingLoad(){
	var loading = false;  //状态标记
	$(document.body).infinite().on("infinite", function() {
	  if(loading) return;
	  loading = true;
	  setTimeout(function() {
	    //$(document.body).destroyInfinite();
//	    $(".weui-infinite-scroll").hide();
	  	loadMainList();
	    loading = false;
	  }, 1000);   //模拟延迟
});
}

function loadMainList() {
	var typeId = $("#typeId").val();
	//param.orderBy = "visit_num";
	//param.ascOrdesc = "DESC";
	var userId = $("#userId").val();
	
	var param = {page:lh.page.currentPage, rows:pageNum,commentTypeCode:'comment_forumArticle',
				userId:userId,showReplyCount:1};
	if (typeId) {
		param.typeId = typeId;
	}
	lh.post('front', '/getForumReplyList', param, function(rsp) {
		if(rsp.success){
			var data = rsp.rows;
			if(data && data.length>0){
				makeMainListDom(data, true);
				lh.page.currentPage ++;
			}else{
//				$('#resultTip').text('没有更多数据').show();
				$(".infinite-preloader").text('没有更多数据');
			}
		}else{
			lh.page.currentPage = 1;
			$('#resultTip').text(rsp.msg).show();
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function renderEvent(){
	$("#data-container .scrollbar_ul").mCustomScrollbar({
		axis : "x",
		advanced : {
			autoExpandHorizontalScroll : true
		}
	});
}

function makeMainListDom(mainList, isAppend) {
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {
		rows : mainList,
		picsDom:function(){
			var picPaths = this.picPaths;
			if(!picPaths)return '';
			var pics = picPaths.split(',');
			var length = pics.length;
			//if(length > 9)length = 9;
			var picsDom = '';
			for(var i=0;i<length;i++){
				var pic = pics[i];
				pic += buildOSSZoom(100, 100); //@@_OSS_IMG_@@
				picsDom += '<li><img src="'+pic+'" width="100" /></li>';
			}
			return picsDom;
		},
		date:function(){
			var createdAt = this.createdAt;
			createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
			return createdAt;
		},
		userAvatars:function(){
			var userAvatars = '';
			var pic = this.userAvatar;
			pic += buildOSSZoom(50,50);
			userAvatars += '<img src="'+pic+'" class="img-responsive center-block">';
			
			return userAvatars;
		}
	});
	if (isAppend) {
		$('#data-container').append(rendered);
	} else {
		$('#data-container').html(rendered);
	}
	renderEvent();
	checkDom();
}

function checkDom(){
	var domNum = $(".mt6").size()
	if(domNum>=100){
		$(".mt6:lt(20)").remove();
	}
	if(domNum<10){
		$(".weui-infinite-scroll").text('没有更多数据');
	}
}

function getDate(){
	var createdAt = $("#cheatedAt").val();
	createdAt = lh.formatDate({date:new Date(createdAt), flag:'datetime'});
	var curdate = moment();
	var days = curdate.diff(createdAt, 'days');
	$("#days").html("已在江湖混迹"+days+"天");
}
function loadBackUrl() {
	var preUrl = localStorage.getItem('forum_user_preUrl');
	if (preUrl) {
		lh.jump(preUrl);
	}
}
