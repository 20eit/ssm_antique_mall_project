SAVING_FLAG = false;

$(function(){
	init();
});

function init(){
	
 	var orderGoods = lh.param.orderGoods; 
	var express = JSON.parse(orderGoods.expressJson);
	console.log(express);
	
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:express.data});
	$('#data-container').html(rendered);
		
	$('#data-container .cd-timeline-img:first img').prop('width', 20).attr('src', '/images/front/flag_01.png');
	$('#data-container .cd-timeline-content:first').addClass('green');
	
}

