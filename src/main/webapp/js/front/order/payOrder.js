CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
SAVING_FLAG = false;
$(function(){
	getAllReceiveAddressList();
	//lh.scrollBottom(getAllReceiveAddressList);
});

function getAllReceiveAddressList(){
	var param = {page:1, rows:100};
	lh.post('front', '/getUserAddressList',param,function(rsp){
		if(rsp.success){
			var rows = rsp.rows;
			if(rows.length <= 0){
				lh.confirm('您还没有添加收货地址,请先到个人信息下的设置收货地址中添加收货地址.取消返回上一页面，确定将跳转到添加收货地址页面.', '提示', jumpToAddress, goBack);return;
			}else{
				makeAllReceiveAddressListDom(rows, 1);
			}
		}else{
			lh.alert(rsp.msg);
		}
	},'json');
}

function jumpToAddress(){
	lh.jumpR('/address');
}
function goBack(){
	//lh.jumpR('/waitPayMoney');
	lh.back();
}

function makeAllReceiveAddressListDom(dataList,isAppend){
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var rendered = Mustache.render(template, {rows:dataList});
	if(isAppend){
		$('#receiveAddressList').prepend(rendered);
	}else{
		$('#receiveAddressList').html(rendered);
	}
}

function checkedAddress(id){
	lh.current.addressId = id;
	$(".addressItems").css('background-color','white');
	$(".addressItems").css('color','black');
	$("#check_"+id).css('background-color','#42A541');
	$("#check_"+id).css('color','white');
}

function updateOrderAddressAndJumpToPay(){
	if(SAVING_FLAG)return;
	if(!lh.current.addressId || !lh.param.orderGoodsId){
		lh.alert('参数不足，无法进行支付');return;
	}
	lh.loading('正在保存收货地址...');//加载遮罩
	SAVING_FLAG = true;
	$.post('/updateOrderAddress', {addressId:lh.current.addressId, orderGoodsId:lh.param.orderGoodsId},function(rsp){
		SAVING_FLAG = false;
		if(rsp.success){
			lh.jumpR('/goodsPay?orderGoodsId='+lh.param.orderGoodsId);
			//redirectToPay({payType:2,orderGoodsId:orderGoodsId});
		}else{
			lh.alert(rsp.msg);
		}
	},'json')
}




