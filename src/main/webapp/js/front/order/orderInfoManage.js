SAVING_FLAG = false;
lh.current = {statusFlag:''};
$(function() {
	init();
	//getOrderInfoList('all');
	lh.scrollBottom(getOrderInfoList);
});

function init(){
	if(localStorage){
		var orderStatus = localStorage.getItem('orderInfoManage_orderStatus');
		if(orderStatus){
			getOrderInfoList(orderStatus);
		}else{
			etOrderInfoList('all');
		}
	}
}

function getOrderInfoList(statusFlag) {
	var buyerOrSeller = localStorage.getItem("userCenter_buyerOrSeller");//从localStorage中读取
	if(!buyerOrSeller)buyerOrSeller = 1;
	lh.buyerOrSeller = buyerOrSeller;
	if (!statusFlag)statusFlag = lh.current.statusFlag;
	$('#navUl li').removeClass('active');
	$('#status_' + statusFlag).addClass('active');
	if (statusFlag != lh.current.statusFlag) {
		lh.page.currentPage = 1;
		$('#data-container').empty();
		lh.current.statusFlag = statusFlag;
		localStorage.setItem('orderInfoManage_orderStatus', statusFlag);
		$("#showAllMyDiv").hide();
		lh.param.amSerial = null;// 点击了顶部切换按钮，就加载全部
		lh.param.userSerial = null;
	}
	lh.ajaxBefore();// ajax发起请求前执行
	var param = {
		buyerOrSeller : buyerOrSeller,
		orderStatus : statusFlag,
		page : lh.page.currentPage,
		rows : lh.page.rows,
		notOver : 1
	};
	lh.post('front', '/getOrderGoodsList', param, function(rsp) {
		// frontLoginCheck(rsp);//登陆检查
		if (rsp.success) {
			var count = rsp.total;
			var data = rsp.rows;
			if (data && data.length > 0) {
				makeOrderInfoDom(rsp, true);
				lh.page.currentPage++;
			} else {
				$('#resultTip').text('没有更多数据').show();
			}
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function makeOrderInfoDom(rsp, isAppend) {
	/*
	 * for(var i in orderGoodsList){ }
	 */
	var data = {
		rows : rsp.rows,
		getButtons : function(d) {
			if (this.orderDoneStatus == 'order_done_status_yes') {
				return '已结束';
			}
			if(lh.buyerOrSeller == 2){//卖家
				var dom = '<button onclick="lh.jumpR(\'/chat/'+this.buyerUserSerial+'\');" type="button" class="btn btn-sm btn-default">联系买家</button>';
				if (this.payStatusCode == 'pay_status_todo') {
					dom += '<button onclick="cancelOrder('+this.orderId+');" type="button" class="btn btn-sm btn-danger fl">取消订单</button>';
				} else if (this.payStatusCode == 'pay_status_done') {
					if (this.shippingStatusCode == 'shipping_status_todo') {
						dom += '<button onclick="sendGoods('+this.orderId+','+this.id+');" type="button" class="btn btn-sm btn-success">发货</button>';
					} else if (this.shippingStatusCode == 'shipping_status_done') {
					} else if (this.shippingStatusCode == 'shipping_status_receive') {
					}else if (this.shippingStatusCode == 'order_status_apply_return') {
						dom += '<button onclick="agreeReturnGoods('+this.orderId+');" type="button" class="btn btn-sm btn-default">同意退货</button>';
						dom += '<button onclick="disagreeReturnGoods('+this.orderId+');" type="button" class="btn btn-sm btn-default">拒绝退货</button>';
					}
				}
			}else if(lh.buyerOrSeller == 1){//买家
				var dom = '<button onclick="lh.jumpR(\'/chat/'+this.sellerUserSerial+'\');" type="button" class="btn btn-sm btn-default">联系卖家</button>';
				if (this.payStatusCode == 'pay_status_todo') {
					dom += '<button onclick="cancelOrder('+this.orderId+');" type="button" class="btn btn-sm btn-danger fl">取消订单</button>';
					dom += '<button onclick="payGoods('+this.id+');" type="button" class="btn btn-sm btn-success">付款</button>';
				} else if (this.payStatusCode == 'pay_status_done') {
					if (this.shippingStatusCode == 'shipping_status_todo') {
					} else if (this.shippingStatusCode == 'shipping_status_done') {
						dom += '<button onclick="receiveGoods('+this.orderId+');" type="button" class="btn btn-sm btn-info">确认收货</button>';
						dom += '<button onclick="scanExpressJson('+this.id+');" type="button" class="btn btn-sm btn-success">查看物流</button>';
						dom += '<button onclick="applyReturnGoods('+this.orderId+');" type="button" class="btn btn-sm btn-danger fl">申请退货</button>';
					} else if (this.shippingStatusCode == 'shipping_status_receive') {
					}else if (this.shippingStatusCode == 'order_status_apply_return') {
						dom += '<button onclick="cancelReturnGoods('+this.orderId+');" type="button" class="btn btn-sm btn-default">取消申请退货</button>';
					}
				}
			}
			return dom;
		},
		getStatusName:function(){
			var statusName = '';
			if (this.orderDoneStatus == 'order_done_status_yes') {
				return '';
			}
			if (this.payStatusCode == 'pay_status_todo') {
				statusName = '等待买家付款';
			} else if (this.payStatusCode == 'pay_status_done') {
				// statusName = '已付款';
				if (this.shippingStatusCode == 'shipping_status_todo') {
					statusName += '等待卖家发货';
				} else if (this.shippingStatusCode == 'shipping_status_done') {
					statusName += '卖家已发货';
				} else if (this.shippingStatusCode == 'shipping_status_receive') {
					statusName += '买家已收货';
				}
			}
			return statusName;
		}
	}
	if(lh.buyerOrSeller == 1)data.buyer = 1;//买家页面
	else if(lh.buyerOrSeller == 2)data.seller = 1;//买家页面
	var template = $('#template').html();
	Mustache.parse(template); // optional, speeds up future uses
	var rendered = Mustache.render(template, data);
	if (isAppend) {
		$('#data-container').prepend(rendered);
	} else {
		$('#data-container').html(rendered);
	}
}

function forceCancelOrder(){
	cancelOrder(null, true);
}

function cancelOrder(orderId, isForce) {
	if (SAVING_FLAG)return;
	if(!orderId){
		orderId = lh.current.orderId;
	}else{
		lh.current.orderId = orderId;
	}
	if (!isForce) {
		lh.confirm('该快递您还没有签收，是否确认收货？', forceCancelOrder);
		return;
	}
	lh.loading('正在保存数据...');//加载遮罩
	SAVING_FLAG = true;
	lh.post('front', '/cancelOrder', {orderId : orderId}, function(rsp) {
		SAVING_FLAG = false;
		if (rsp.success) {
			lh.alert('您已经成功取消该订单', lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function payGoods(orderGoodsId){
	lh.jumpR('/payOrder/'+orderGoodsId);
}

function sendGoods(orderId, orderGoodsId){
	lh.jumpR('/goodsSend/'+orderId+'/'+orderGoodsId);
}

function scanExpressJson(orderGoodsId){
	lh.jumpR('/scanExpressJson/'+orderGoodsId);
}

function forceReceiveGoods(){
	receiveGoods(null, null, true);
}

function receiveGoods(orderId, expressState, isForce) {
	if (SAVING_FLAG)return;
	if(!orderId){
		orderId = lh.current.orderId;
	}else{
		lh.current.orderId = orderId;
	}
	if(!expressState){
		expressState = lh.current.expressState;
	}else{
		lh.current.expressState = expressState;
	}
	if (expressState && expressState != 3 && !isForce) {
		lh.confirm('该快递您还没有签收，是否确认收货？', forceReceiveGoods);
		return;
	}else if(!isForce){
		lh.confirm('是否确认收货？', forceReceiveGoods);
		return;
	}
	var obj = {orderId : orderId};
	lh.loading('正在保存数据...');//加载遮罩
	SAVING_FLAG = true;
	lh.post('front', '/receiveGoods', obj, function(rsp) {
		SAVING_FLAG = false;
		if (rsp.success) {
			lh.alert('您已经成功确认收货', lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function forceApplyReturnGoods(){
	applyReturnGoods(null, true);
}

function applyReturnGoods(orderGoodsId, isForce) {
	if (SAVING_FLAG)return;
	if(!orderGoodsId){
		orderGoodsId = lh.current.orderGoodsId;
	}else{
		lh.current.orderGoodsId = orderGoodsId;
	}
	if (!isForce) {lh.confirm('是否确认发起退货申请？', forceApplyReturnGoods);
		return;
	}
	var obj = {orderGoodsId : orderGoodsId};
	lh.loading('正在保存数据...');//加载遮罩
	SAVING_FLAG = true;
	lh.post('front', '/applyReturnOrderGoods', obj, function(rsp) {
		SAVING_FLAG = false;
		if (rsp.success) {
			lh.alert('您已经成功发起退货申请，商家同意后即可退款', lh.reload);
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}

function forceAgreeReturnGoods(){
	returnGoods(lh.current.orderGoodsId, 1);
}

function forceDisAgreeReturnGoods(){
	returnGoods(lh.current.orderGoodsId, 2);
}

function forceCancelReturnGoods(){
	returnGoods(lh.current.orderGoodsId, 3);
}

function agreeReturnGoods(orderGoodsId) {
	if (SAVING_FLAG)return;
	if(!orderGoodsId){
		orderGoodsId = lh.current.orderGoodsId;
	}else{
		lh.current.orderGoodsId = orderGoodsId;
	}
	lh.confirm('是否确认同意退货？', forceAgreeReturnGoods);
	return;
}

function disagreeReturnGoods(orderGoodsId) {
	if (SAVING_FLAG)return;
	if(!orderGoodsId){
		orderGoodsId = lh.current.orderGoodsId;
	}else{
		lh.current.orderGoodsId = orderGoodsId;
	}
	lh.confirm('是否确认拒绝退货？', forceDisAgreeReturnGoods);
}

function cancelReturnGoods(orderGoodsId) {
	if (SAVING_FLAG)return;
	if(!orderGoodsId){
		orderGoodsId = lh.current.orderGoodsId;
	}else{
		lh.current.orderGoodsId = orderGoodsId;
	}
	lh.confirm('是否确认取消退货？', forceDisAgreeReturnGoods);
}

function returnGoods(orderGoodsId, opt) {
	if (SAVING_FLAG)return;
	lh.loading('正在保存数据...');//加载遮罩
	SAVING_FLAG = true;
	lh.post('front', '/returnOrderGoods', {orderGoodsId : orderGoodsId,opt : opt}, function(rsp) {
		if (rsp.success) {
			if (opt == 1) {
				lh.alert('您已经成功同意退货', lh.reload);
			} else if (opt == 2)  {
				lh.alert('您已经成功拒绝退货', lh.reload);
			} else if (opt == 3)  {
				lh.alert('您已经成功取消退货', lh.reload);
			}
		} else {
			lh.alert(rsp.msg);
		}
	}, 'json');
}