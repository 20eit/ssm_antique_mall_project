SAVING_FLAG = false;

$(function(){
	init();
});

function init(){
	$("#express").selectpicker({
	    //style: 'btn-info',
		//actionsBox:true,
		//header:'请选择',
	    showTick:true,
	    mobile:true,
	    width:'100%',
	    size:20
	});
}

function sendGoods(orderId){
	if(SAVING_FLAG)return;
	var orderGoodsId = $("#orderGoodsId").val();
	var expressId = $("#express").val();
	var expressOrder = $("#expressOrder").val();
	if(!express){
		lh.alert('请填写快递公司');
		return;
	}
	if(!expressOrder){
		lh.alert('请填写快递单号');
		return;
	}
	var obj = {expressOrder:expressOrder, expressId:expressId, orderGoodsId:orderGoodsId};
	lh.loading('正在保存数据...');//加载遮罩
	SAVING_FLAG = true;
	lh.post('front', '/sendGoods', obj,function(rsp){
		SAVING_FLAG= false;
		if(rsp.success){
			lh.alert('您已经成功发货，即将返回订单管理页面', lh.back);
		}else{
			lh.alert(rsp.msg);
		}
	},'json');	
}