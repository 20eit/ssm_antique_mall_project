SAVING_FLAG = false;
var content = $("#Input").val();
	var username = $("#username").val();
	var userId = $("#userId").val();
	var objectId = lh.param.forumArticleId;
function addPraise() {
	var params = {
		praiseId : objectId,
		praiseType : 1
	};
	lh.post('front', '/addUserPraise', params, function(rsp) {
				if (rsp.success == "success") {
					if (rsp.code == 'alreadyExist_error') {
						lh.alert('已经点过赞了');
						return;
					}
				} else {
					lh.alert(rsp.msg);
				}
			});
}

function addComment() {
	if (SAVING_FLAG)
		return;
	SAVING_FLAG = true;
	if (!content) {
		lh.alert('请填写评论的内容');
		SAVING_FLAG = false;
		return;
	}
	var obj = {};
	// obj.commentTypeId = commentTypeId;
	obj.content = content;
	obj.objectId = objectId;
	obj.username = username;
	obj.userId = userId;
	obj.commentTypeCode = "comment_forumArticle";
	// frontBaseLoadingOpen('正在保存数据...');//加载遮罩
	$.post('/comment/addOrUpdateComment', obj, function(rsp) {
				SAVING_FLAG = false;
				// frontBaseLoadingClose();//解除遮罩
				if (rsp) {
					frontLoginCheck(rsp);// 登陆检查
					if (rsp.status == 'success') {
						
					} else {
						if (rsp.noPhone == 'noPhone') {
							lh.alert(rsp.msg, "jumpToBindUserPhone()");
						} else {
							lh.alert(rsp.msg);
						}
					}
				}
			}, 'json');

}