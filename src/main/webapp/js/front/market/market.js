/** 鬼市（逛逛）主JS  */
CURRENT_PAGE = 1;//当前页数
PAGE_COUNT = 10;//取多少条数据
$(function(){
	initPage();
	loadShopList();//加载店铺列表（包含每个店铺的部分商品）
	lh.scrollBottom(loadShopList);
});

function initPage(){
	initTopSearch();
	switchBottomMenu('menu_market');
}

function loadShopList(){
	var param = {withGoods:1, forMarket:1, page:CURRENT_PAGE, rows:PAGE_COUNT,orderBy:"main_status,credit_margin DESC,u.is_real_auth DESC,A.created_at",ascOrdesc:"desc"};
	var searchName = $("#searchName").val();
	if(searchName)param.nameLike = searchName;
	$.post('/getShopList',param,function(rsp){
		if(rsp){
			if(rsp.success){
				//var shopList = rsp.shopList;
				var count =  rsp.total;
				$("#shopCount").text(count);
				if( count && count > 0){
					makeShopListDom(rsp.rows,1);
					CURRENT_PAGE++;
				}else{
					$('#resultTip').text('没有更多数据').show();
				}
			}else{
				lh.alert(rsp.msg);
			}
		}
		SCROLL_LOADING = false;//设置为加载完毕
	},'json');
}

function makeShopListDom(shopList,isAppend){
	for(var i = 0;i<shopList.length;i++){
		var s = shopList[i];
		if(s.userAvatar){
			s.userAvatar += '@50h_50w_1e'; //OSS图片
		}else{
			s.userAvatar = '/images/front/default_avatar.png';
		}
	}
	var template = $('#template').html();
	Mustache.parse(template);   // optional, speeds up future uses
	var data = {
		rows:shopList,
		picDom:function(){
			var picPathList = this.picPathList;
			var picDom = '';
			if(picPathList.length > 0){
				for(var i=0; i<picPathList.length; i++){
					var picPath = picPathList[i];
					if(!picPath)continue;
					picPath = picPath.replace(',','');
					picPath += '@60h_50w_1e'; //OSS图片
				   picDom += 
				   '<div class="div_gg fl" style="width:45px;overflow:hidden;margin:5px">'
						  +'<a href="javascript:void(0);" ><img src="'+picPath+'" style="height:50px;"/></a>'
				  +'</div>'
				}
			}
			return picDom;
		}
   }
	var rendered = Mustache.render(template, data);
	
	if(isAppend){
		$('#shopItems').append(rendered);
	}else{
		$('#shopItems').html(rendered);
	}
}
/*
function search(){
	var ss1_input = $('#ss1_input').val();
	var searchType = SEARCH_TYPE;//搜索类型，1：藏品，2：店铺
	if(searchType == 1){
		if(ss1_input){
			window.location.href="/goods?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/goods?searchType="+searchType;
		}
	}else{
		if(ss1_input){
			window.location.href="/market?searchName="+ss1_input+"&searchType="+searchType;
		}else{
			window.location.href="/market?searchType="+searchType;
		}
	}
}
*/
function jumpToShop(){
	var url = "/shop";
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}