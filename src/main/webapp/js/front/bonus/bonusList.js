CURRENT_PAGE = 1;
ADD_FLAG = true;
TEMPLATE = "";
$(function(){
	//initData();
	getBonusList();
	lh.scrollBottom(getBonusList);//下拉加载更多数据
});

function initData(){
	 $("#startDate").mobiscroll().date({  
        theme: "android-ics light",  
        lang: "zh",  
        cancelText: '取消',  
        preset : 'date',
        dateFormat: 'yy-mm-dd', //返回结果格式化为年月格式  
        dateOrder: 'yymmdd',
        //timeFormat: 'HH:ii:ss', //返回结果格式化为年月格式  
        /*maxDate: date,minDate: date2,*/
        //stepSecond:10,
        //defaultValue:undefined,
        headerText: function (valueText) { //自定义弹出框头部格式  
            return '请选择开始日期';  
        }  
    });  
    $("#endDate").mobiscroll().date({  
        theme: "android-ics light",  
        lang: "zh",  
        cancelText: '取消',  
        preset : 'date',
        dateFormat: 'yy-mm-dd', //返回结果格式化为年月格式  
        dateOrder: 'yymmdd',
        //timeFormat: 'HH:ii:ss', //返回结果格式化为年月格式  
        /*maxDate: date,minDate: date2,*/
        //stepSecond:10,
       // defaultValue:undefined,
        headerText: function (valueText) { //自定义弹出框头部格式  
            return '请选择结束日期';  
        }  
    });  
    var d = lh.formatDate(new Date());
    $("#startDate,#endDate").val(d);
    TEMPLATE = $('#template').html();
	Mustache.parse(TEMPLATE);   // optional, speeds up future uses
}

function getBonusList(isNotAppend){
	$('#resultTip').hide();
	$('#loadingTip').show();
	if(isNotAppend)CURRENT_PAGE = 1;
	var createTime = $("#createTime").text();
	var param = {page:CURRENT_PAGE, rows:15};
	//if('object' != typeof startDate)param.startDate = startDate;
	if('object' != typeof createTime)param.createTime = createTime;
	var url = '/getBonusReceivedList';
	if(IS_SEND)url = '/getBonusSendList';
	$.post(url, param, function(rsp){
		$('#loadingTip').hide();
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeBonusListDom(data, isNotAppend);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}

function makeBonusListDom(dataList, isNotAppend){
	var data ={
		rows:dataList,
		statusDom:function(){
			var status = this.mainStatus;
			var dom = '';
			if(status == 2){
				dom = '<span style="color:gray;font-size: 12px;">（已发完）</span>';
			}else{
				dom = '<span style="color:red;font-size: 12px;">（有剩余）</span>';
			}
			return dom;
		}
	}
	var rendered = Mustache.render(TEMPLATE, data);
	if(isNotAppend){
		$('#dataUl').html(rendered);
	}else{
		$('#dataUl').append(rendered);
	}
}

