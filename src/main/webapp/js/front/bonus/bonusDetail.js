CURRENT_PAGE = 1;
ADD_FLAG = true;
TEMPLATE = null;
CURRENT_BONUS_ID = null;
$(function(){
	initData();
	getBonusDetailList();
	lh.scrollBottom(getBonusDetailList);//下拉加载更多数据
});

function initData(){
	var bonusId = $('#bonusId').val();
	CURRENT_BONUS_ID = bonusId;
    TEMPLATE = $('#template').html();
	Mustache.parse(TEMPLATE);   // optional, speeds up future uses
}

function getBonusDetailList(isNotAppend){
	if(!CURRENT_BONUS_ID){
		lh.alert('未能正常获取红包数据');return;
	}
	$('#resultTip').hide();
	$('#loadingTip').show();
	var param = {page:CURRENT_PAGE, rows:100, bonusId:CURRENT_BONUS_ID};
	$.post('/getBonusDetailList', param, function(rsp){
		$('#loadingTip').hide();
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				var data = rsp.rows;
				if(data && data.length>0){
					makeBonusDetailDom(data, isNotAppend);
					CURRENT_PAGE ++;
				}else{
					$('#resultTip').text('没有更多数据');
					$('#resultTip').show();
				}
			}else{
				CURRENT_PAGE = 1;
				$('#resultTip').text(rsp.msg);
				$('#resultTip').show();
			}
		}
	},'json');
}

function makeBonusDetailDom(dataList, isNotAppend){
	
	for(var i=0; i<dataList.length; i++){
		var data = dataList[i];
		data.dealTime = formatDate(data.dealTime, 1, 1);
	}
	
	var rendered = Mustache.render(TEMPLATE, {rows:dataList});
	if(isNotAppend){
		$('#dataContainer').html(rendered);
	}else{
		$('#dataContainer').append(rendered);
	}
	
}

function sendBonusMsg(bonusId){
	if(!bonusId)return;
	var msg = $('#msg').val();
	if(!msg){
		lh.alert('请输入祝福语');return;
	}
	var param = {msg:msg, bonusId:bonusId};
	$.post('/updateBonusDetailMsg', param, function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp){
			if(rsp.success){
				location.reload();
			}else{
				lh.alert(rsp.msg);
			}
		}
	},'json');
	
}


