
$(function(){
	//initData();
	$("#dropdown").click(function(){
	$("#dropdown").attr("class","dropdown open");
});
});


function calculateTotalMoney(){
	var totalMoney = parseFloat($('#totalMoney').val());
	$("#tatalMoneys").text("￥"+totalMoney.toFixed(2));
}

/** randomBonus - 红包数量 */
function checkCount(){
	var bonusCount = $('#bonusCount').val();
	bonusCount = bonusCount || 1;
	bonusCount = parseInt(bonusCount);
	if(bonusCount < 1)bonusCount = 1;
	$('#bonusCount').val(bonusCount);
}

/** randomBonus - 总金额 */
function checkRandomPayTotal(){
	var totalMoney = $('#totalMoney').val();
	totalMoney = totalMoney || 1;
	if(totalMoney < 1)totalMoney = 1;
	$('#totalMoney').val(totalMoney);
	$('#tatalMoneys').text(parseFloat(totalMoney).toFixed(2));
}

/** commonBonus - 红包数量和单个金额 */
function checkCountAndEachMoney(){
	var bonusCount = $('#bonusCount').val();
	bonusCount = bonusCount || 1;
	bonusCount = parseInt(bonusCount);
	if(bonusCount < 1)bonusCount = 1;
	$('#bonusCount').val(bonusCount);
	
	var eachMoney = $('#eachMoney').val();
	eachMoney = eachMoney || 1;
	eachMoney = new Number(eachMoney);
	if(eachMoney < 1)eachMoney = 1;
	$('#eachMoney').val(eachMoney);
	totalMoney = eachMoney * bonusCount;

	$("#tatalMoneys").text(parseFloat(totalMoney).toFixed(2));
	
	$('#totalMoney').val(totalMoney);
}

function addRandomBonus(){
	var bonusCount = $('#bonusCount').val();
	var totalMoney = $('#totalMoney').val();
	var msg = $('#msg').val();
	var payPass = $('#payPass').val();
	if(!bonusCount){
		lh.alert('请确定发多少红包');return;
	}
	if(!totalMoney){
		lh.alert('请确定总共发多少钱');return;
	}
	if(!payPass){
		lh.alert('请输入支付密码,如果您还未设置支付密码，请先到个人中心设置');return;
	}
	if(!$.isNumeric(bonusCount) || bonusCount < 1){
		lh.alert('请输入正确的红包数量');return;
	}
	if(!$.isNumeric(totalMoney) || totalMoney < 1){
		lh.alert('请输入正确的红包金额');return;
	}
	if(!msg)msg = '恭喜发财，大吉大利';
	var param = {count:bonusCount, totalMoney:totalMoney, msg:msg, payPass:payPass, typeId:1};
	$.post('/addBonus', param, function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp.success){
			if(lh.param.from){
				lh.alert("红包已经准备好了，开始发红包吧", jumpToFromPage);
			}else{
				lh.alert("红包已经准备好了，开始发红包吧", jumpSelectUser);
			}
		}else{
			if(rsp.code == 'avaliableMoney_lack'){
				redirectToCharge();return;
			}
			lh.alert(rsp.msg);
		}
	},'json');
}

function addCommonBonus(){
	var bonusCount = $('#bonusCount').val();
	var eachMoney = $('#eachMoney').val();
	var totalMoney = $('#tatalMoneys').text();
	var msg = $('#msg').val();
	var payPass = $('#payPass').val();
	if(!bonusCount){
		lh.alert('请确定发多少红包');return;
	}
	if(!eachMoney){
		lh.alert('请确定单个红包金额');return;
	}
	if(!payPass){
		lh.alert('请输入支付密码,如果您还未设置支付密码，请先到个人中心设置');return;
	}
	if(!$.isNumeric(bonusCount) || bonusCount < 1){
		lh.alert('请输入正确的红包数量');return;
	}
	if(!$.isNumeric(eachMoney) || eachMoney < 1){
		lh.alert('请输入正确的单个红包金额');return;
	}
	if(!totalMoney || !$.isNumeric(totalMoney) || totalMoney < 1){
		lh.alert('请输入正确的红包数量和单个红包金额');return;
	}
	if(!msg)msg = '恭喜发财，大吉大利';
	var param = {count:bonusCount, totalMoney:totalMoney, eachMoney:eachMoney, msg:msg, payPass:payPass, typeId:2};
	$.post('/addBonus', param, function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp.success){
			if(lh.param.from){
				lh.alert("红包已经准备好了，开始发红包吧", jumpToFromPage);
			}else{
				lh.alert("红包已经准备好了，开始发红包吧", jumpSelectUser);
			}
		}else{
			if(rsp.code == 'avaliableMoney_lack'){
				redirectToCharge();return;
			}
			lh.alert(rsp.msg);
		}
	},'json');
}

function jumpSelectUser(){
	var url = '/bonusSend';
	lh.jumpR(url);
}

function jumpToFromPage(){
	lh.jumpR(lh.param.from);
}

