TEMPLATE_SELF = null;
TEMPLATE_OTHER = null;
BONUS_SAVING = false;
$(function(){
	//initData();
});

function addBonusChat(receiverId){
	var serial = createSerial();
	var content = '红包';
	var param = {typeId:2,serial:serial,receiverId:receiverId,content:content};
	$.post('/addBonusChat', param, function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp.status == 'success'){
			lh.alert('已经成功发送红包');
		}else{
			if(rsp.error_desc == 'bonus_lack'){
				jumpToBonus();return;
			}
			lh.alert(rsp.msg);
		}
	},'json');
}

function addBatchBonusChat(){
	if(BONUS_SAVING || !CURRENT_TAB)return;//依赖fans.js中的CURRENT_TAB
	if(CURRENT_TAB == 'fans_tab1'){//我关注的
		var param = {typeId:1};
	}else if(CURRENT_TAB == 'fans_tab2'){//关注我的
		var param = {typeId:2};
	}
	if(!param)return;
	BONUS_SAVING = true;
	frontBaseLoadingOpen();
	$.post('/addBatchBonusChat', param, function(rsp){
		frontBaseLoadingClose();
		BONUS_SAVING = false;
		frontLoginCheck(rsp);//登陆检查
		if(rsp.status == 'success'){
			lh.alert('已经成功群发红包');
		}else{
			if(rsp.error_desc == 'bonus_lack'){
				jumpToBonus();return;
			}
			lh.alert(rsp.msg);
		}
	},'json');
}

function receiveBonus(senderId, id, reloadFlag){
	var param = {bossId:senderId};
	if(id)param.id = id;
	$.post('/receiveBonus', param, function(rsp){
		frontLoginCheck(rsp);//登陆检查
		if(rsp.status == 'success'){
			var money = rsp.money;
			var bonusId = rsp.bonusId;
			if(!money || !bonusId){
				lh.alert('红包已经被抢光了');
				if(reloadFlag){
					window.location.reload();
				}
				return;
			}
			frontBaseConfirm('恭喜您成功抢到一个红包，金额为：'+money+'元，是否查看详情', 'jumpToBonusDetail('+bonusId+')');
		}else{
			lh.alert(rsp.msg);
			if(reloadFlag){
				setTimeout(function(){
					window.location.reload();
				},1000);
			}
		}
	},'json');
}

function jumpToBonusDetail(bonusId){
	if(!bonusId)return;
	var url = '/bonusDetail/'+bonusId;
	var r = $("#r").val();
	if(r) url += "?r="+r;
	window.location.href = url;
}
