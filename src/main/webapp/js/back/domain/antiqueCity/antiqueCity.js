var HEIGHT = document.documentElement.clientHeight;
var WIDTH = document.documentElement.clientWidth;
if(WIDTH<1000)WIDTH=1000;
IS_SHOW_TRASH = false;
SAVING_FLAG = 0;
$(function() {
	GRID_QUERYOBJ = {};//查询条件
	loadGrid();
	initData();
	initUploadSimple({showDelBtn:true});//调用完整方法
});

/**加载用户列表*/
function loadGrid(){
	$('#datagrid').datagrid({
	    url:'/back/getAntiqueCityList',
	    pagination:true,//允许分页
		pageSize:20,//每页10条数据
		loadMsg:'',
		width:WIDTH-20,
		height:HEIGHT-125,
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		queryParams:GRID_QUERYOBJ,
		fitColumns:true,
		selectOnCheck:true,
		checkOnSelect:true,
		singleSelect:true,
		striped:true,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
	        {field:'name',title:'古玩城名称',width:100,align:'center'},
	        {field:'address',title:'古玩城地址',width:100,align:'center'},
	        {field:'picPath',title:'古玩城logo',width:140,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:60px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:60px;">暂无图片<span>';
	        	return logo;
	        }},
	        //{field:'content',title:'内容',width:220,align:'center'},
	        {field:'operate',title:'操作',width:120,align:'center',formatter: function(value,row,index){
	        	return  '<span class="g_alive" ><span style="cursor: pointer;color: #EC4949" onclick="openAntiqueCityDetailWin('+index+',\'edit\')">修改</span>'
	        	+'&nbsp;|&nbsp;<span style="cursor: pointer;color: green" onclick="openAntiqueCityDetailWin('+index+',\'read\')">查看</span></span>'
	        	+'<span class="g_trash"><span  style="cursor: pointer;color: #EC4949" onclick="batchThoroughDelete('+row.id+')">彻底删除</span>'
	        	+'&nbsp;|&nbsp;<span  style="cursor: pointer;color: green" onclick="batchRecover('+row.id+')">恢复</span></span>';
	        }},
	        {field:'mainStatus',title:'状态',width:60,align:'center',formatter: function(value,row,index){
	        	var status = "已审核";
	        	if(value == 2){status = '<span style="color:orange">未审核</span>';}
	        	return status;
	        }},
	        {field:'deletedBy',title:'删除人',width:80,align:'center'},
	        {field:'deletedAt',title:'删除时间',width:80,align:'center',formatter: function(value,row,index){
	        	var dateStr = "";
	        	if(value){
	        		var d = new Date(value);
	        		var month = d.getMonth()+1;
	        		dateStr = ''+d.getFullYear()+'/'+month+'/'+d.getDate();
	        	}
	        	return dateStr;
	        }},
	        {field:'createdBy',title:'创建人',width:80,align:'center'},
	        {field:'createdAt',title:'创建时间',width:80,align:'center',formatter: function(value,row,index){
	        	var dateStr = "";
	        	if(value){
	        		var d = new Date(value);
	        		var month = d.getMonth()+1;
	        		dateStr = ''+d.getFullYear()+'/'+month+'/'+d.getDate();
	        	}
	        	return dateStr;
	        }}
	    ]],
	    /*onSelect:function(rowIndex, rowData){
	    	onClickRowOfGrid(rowIndex, rowData);*//**当点击表格中的某行数据时执行*//*
	    },
	    onClickRow: function(index, row){
			
		},
	    loadFilter: function(data){
	    	return data;
	    },*/
	    onDblClickRow: function(index, row){
	    	openAntiqueCityDetailWin(index, 'read');
        },
	    onLoadError: function(data){
	    	backDatagridErrorCheck(data);
	    },
		onLoadSuccess:function(data){
			if(IS_SHOW_TRASH){
	       		$('.g_alive').hide();
	       		$('.g_trash').show();
	       	}else{
	       		$('.g_alive').show();
	       		$('.g_trash').hide();
	       	}
		}
	});
}

function onClickRowOfGrid(){

}

function initData(){
	$("#mainStatus").combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	$("#type").combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:200,
	    url:'/back/getAntiqueCity'
	});
}

/**根据查询条件查询匹配的数据*/
function doSearch(){
	var mainStatus = $("#mainStatus").combobox('getValue');
	var typeId = $("#type").combobox('getValue');
	var name = $("#name").textbox('getValue');
	var introduce = $("#introduce").textbox('getValue');
	var address = $("#address").textbox('getValue');
	
	var common_queryObj = GRID_QUERYOBJ;
	common_queryObj.mainStatus = mainStatus;
	common_queryObj.name = name;
	common_queryObj.introduce = introduce;
	common_queryObj.address = address;
	common_queryObj.id = typeId;
	$('#datagrid').datagrid('load',common_queryObj);  
}
/**重置查询查询条件*/
function clearSearch(){
	$("#mainStatus").combobox('setValue','')
	$("#type").combobox('setValue','')
	$("#name").textbox('setValue','');
	$("#introduce").textbox('setValue','');
	$("#address").textbox('setValue','');
}

function loadCombo(){
	$('#f_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:true,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
}

/**添加数据*/
function addAntiqueCity(){
	openAntiqueCityDetailWin(-1,'add');
}

function openAntiqueCityDetailWin(index,operation){
	var rows,data;
	if(index >= 0){
		rows = $('#datagrid').datagrid('getRows');
		data = rows[index];//获取该行的数据
	}
	var $form = $('#antiqueCityDetailForm');
	
	$form.form({
	    url:'',
	    onSubmit: function(){
	       $('#antiqueCityDetailForm').form('enableValidation');
	       var flag = $('#antiqueCityDetailForm').form('validate');
	       var filePaths = $("#filePaths").val();
			var filePathArr = new Array();
			if(filePaths.indexOf(',') >= 0){
				filePaths = filePaths.substring(1);
			}
			filePathArr = filePaths.split(",");
			if(filePathArr.length > 1){
				$.messager.alert('提示','请先删除以前的图片,再重新上传');
				return;
			}
			if(!filePathArr[0]){$.messager.alert('提示','请上传批发城logo');return;}
	       if(flag){
	       		var id;
	       		if(operation != 'add')id = data.id;
	       		var antiqueCity = getAntiqueCityDetail(id);//得到需要的字段信息
	       		antiqueCity.picPath = filePathArr[0];
	       	    $.post('/back/addOrUpdateAntiqueCity',antiqueCity,function(rsp){
					 if(rsp.success){
						 $('#datagrid').datagrid('reload');
						 //$('#datagrid').datagrid('clearSelections');
						 //$('#datagrid').datagrid('clearChecked');
				       	 setTimeout(function(){
				       		closeAntiqueCityDetailWin();
				       	 },500);
					 }else{
						 $.messager.alert('提示',rsp.msg);
					 }
				},'json');
	       }
	       return false;
	    }
	});
	
	loadCombo();
	$('#antiqueCityDetailWin').window('open');
	$form.form('clear');
	$('#upload_outer_div').empty();//清空附件DOM
	$form.form('disableValidation');
	$('#dictDetailTip').html('');
	$('#browse').show();
	
	var domIds = "#f_mainStatus,#f_name,#f_address,#f_introduce";
	
	if(operation == 'add'){//添加
		//$('#antiqueCityDetailTable .readOnlyTr').hide();//隐藏只读字段
		$('#antiqueCityDetailDisAgree,#antiqueCityDetailAgree').hide();
		$('#antiqueCityDetailSave,#antiqueCityDetailBack').show();
		$(domIds).textbox('readonly',false);//新增时设置为可编辑
	}else{//查看或修改
		//$('#antiqueCityDetailTable .readOnlyTr').show();//显示只读字段
		var isReadOnly = false;
		if(operation == 'read'){//查看
			isReadOnly = true;
			$('#antiqueCityDetailSave,#antiqueCityDetailBack,#browse').hide();
		}else{//修改
			$('#antiqueCityDetailSave,#antiqueCityDetailBack').show();
		}
		$(domIds).textbox('readonly',isReadOnly);//设置只读还是可编辑
		setAntiqueCityDetail(data,operation);//设置用户详细信息字段值
	}
}

/** 得到相关信息的字段值 */
function getAntiqueCityDetail(id){
	var name = $('#f_name').textbox('getValue');
	var address = $('#f_address').textbox('getValue');
	var introduce = $('#f_introduce').textbox('getValue');
	//var reply = $('#f_reply').textbox('getValue');
	var mainStatus = $('#f_mainStatus').combobox('getValue');
	
	var antiqueCity = {
		name:name,
		address:address,
		introduce:introduce,
		mainStatus:mainStatus,
		//reply:reply
	};
	if(id){//有id为更新，无id为新增
		antiqueCity.id = id;
	}
	return antiqueCity;
}

/** 设置相关信息的字段值 */
function setAntiqueCityDetail(data,operation){
	var mainStatus = data.mainStatus;
	$('#f_antiqueCityId').val(data.id);
	$('#f_name').textbox('setValue',data.name);
	$('#f_address').textbox('setValue',data.address);
	$('#f_introduce').textbox('setValue',data.introduce);
	$('#f_mainStatus').combobox('setValue', data.mainStatus);
	//$('#f_reply').textbox('setValue',data.reply);
	if(data.picPath){
		$('#upload_outer_div').empty();//清空附件DOM upload_outer_div
		$("#filePaths").val(data.picPath);
		var dom = '<div id="pic"><a href="'+data.picPath+'" target="_blank"><img src="'+data.picPath+'" class="height80"/></a>'
		if(operation == "edit"){
			dom +='<a id="picDelete" style="text-decoration: none;" href="javascript:deletePicPath()" class="delete" title="删除">删除</a></div>'
		}
		$('#upload_outer_div').append(dom);//添加到DOM中
		$("#browse").hide();
	}
	//以下为只读字段：
}
//提交数据
function submitAntiqueCityDetail(){
	//$('#antiqueCityDetailForm').submit();
	var timeRec = preventRepeat(10, SAVING_FLAG);
	if(timeRec){
		SAVING_FLAG = timeRec;
		$('#antiqueCityDetailForm').submit();//执行操作
	}else{//重复提交
		return;//可进行提示或其他操作，这里直接返回，即重复提交时没有反应
	}
}
//关闭窗口
function closeAntiqueCityDetailWin(){
	$('#antiqueCityDetailWin').window('close');
}

function deletePicPath(){
	$("#filePaths").val('');
	//$("#pic img").remove();
	//$("#pic a").remove();
	$("#upload_outer_div").empty();
	$("#browse").show();
}

/**批量删除数据**/
function batchDelete(){
	commonBatchDelete('/back/updateAntiqueCityDelete');//common_back:通用批量恢复
}

/**批量彻底删除**/
function batchThoroughDelete(id){
	commonBatchThoroughDelete('/back/deleteAntiqueCityThorough',null,id);//common_back:通用批量强制删除
}

/**批量恢复数据**/
function batchRecover(id){
	commonBatchRecover('/back/updateAntiqueCityRecover',null,id);//common_back:通用批量恢复
}

/**读取删除数据*/
function showTrash(){
	IS_SHOW_TRASH = true;
	commonShowTrash('#batchRecover,#batchThoroughDelete,#returnBack','#batchDelete,#addAntiqueCity,#showTrash');
}
/**读取未删除数据**/
function returnBack(){
	IS_SHOW_TRASH = false;
	commonReturnBack('#batchDelete,#addAntiqueCity,#showTrash','#batchRecover,#batchThoroughDelete,#returnBack');
}

function jumpToUserInfo(){
	var url = "/back/userInfo";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	subShowMain('用户信息', url)
}