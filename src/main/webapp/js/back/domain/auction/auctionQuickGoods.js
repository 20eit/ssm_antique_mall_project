var HEIGHT = document.documentElement.clientHeight;
var WIDTH = document.documentElement.clientWidth;
if(WIDTH<1000)WIDTH=1000;
IS_SHOW_TRASH = false;
SAVING_FLAG = 0;
$(function() {
	GRID_QUERYOBJ = {};//查询条件
	loadGrid();
	initSearch();
	//paramMapData();
});

/**加载用户列表*/
function loadGrid(){
	$('#datagrid').datagrid({
	    url:'/back/getAuctionQuickGoodsList',
	    pagination:true,//允许分页
		pageSize:20,//每页10条数据
		loadMsg:'',
		width:WIDTH-1,
		height:HEIGHT-125,
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		queryParams:GRID_QUERYOBJ,
		fitColumns:false,
		selectOnCheck:false,
		checkOnSelect:false,
		singleSelect:true,
		striped:true,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
	        {field:'goodsName',title:'商品名称',width:260,align:'center',formatter: function(value,row,index){
	        	if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.goodsSn+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'auctionQuickInstName',title:'拍卖机构名称',width:260,align:'center',formatter: function(value,row,index){
	        	if(!value)return '';
	        	var dom = '<span>'+value+'</span>' +
	        	'<button class="copy_btn pointer fr" data-clipboard-text="'+row.auctionInstSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'picPath',title:'商品图片',width:140,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:40px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:40px;">暂无图标<span>';
	        	return logo;
	        }},
	        {field:'username',title:'所属用户',width:140,align:'center'},
	        {field:'operate',title:'操作',width:100,align:'center',formatter: function(value,row,index){
	        	return '<span class="g_alive"><span class="opt_cuation" onclick="openAuctionQuickGoodsWin('+index+',\'update\')">修改</span>'+//jumpToAuctionQuickGoodsUpdate
	        		   ' |  <span class="opt_green" onclick="openAuctionQuickGoodsWin('+index+',\'read\')">查看</span></span>'+//jumpToAuctionQuickGoods
	        		   '<span class="g_trash"><span class="opt_cuation" onclick="batchThoroughDelete('+row.id+')">彻底删除</span>'+
	        		   ' |  <span class="opt_green" onclick="batchRecover('+row.id+')">恢复</span></span>';
	        }},
	        {field:'createdBy',title:'添加人',width:140,align:'center'},
	        {field:'updatedAt',title:'修改时间',width:160,align:'center',formatter: function(value,row,index){
	        	return getDateStr(value);
	        }},
	        {field:'updatedBy',title:'修改人',width:140,align:'center'},
	        {field:'deletedAt',title:'删除时间',width:160,align:'center',formatter: function(value,row,index){
	        	return getDateStr(value);
	        }},
	        {field:'deletedBy',title:'删除人',width:140,align:'center'}
	        ]],
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
		    	backDatagridErrorCheck(data);
		    },
		    onDblClickRow: function(index, row){
		    	openAuctionQuickGoodsWin(index, 'read');
	        },
	    onLoadSuccess:function(data){
	       	if(IS_SHOW_TRASH){
	       		$('.g_alive').hide();
	       		$('.g_trash').show();
	       	}else{
	       		$('.g_alive').show();
	       		$('.g_trash').hide();
	       	}
	       	initClipboard();//复制功能
	   }  
	});
}

/** grid 行点击事件 */
/**function onClickRowOfGrid(rowIndex, rowData){}*/

function loadCombo(){
	
	$('#f_userId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
	    url:'/back/getUser',
	    onSelect:function(rec){
	    	 var url = '/back/getGoods?userId='+rec.id;
	         $('#f_goodsId').combobox('reload', url);
	    }
	});
	$('#f_auctionInstId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
		required:false,
		panelHeight:'auto',
		url:'/back/getAuctionQuickInst'
	});
	$('#f_goodsId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
		required:false,
		panelHeight:'auto'
	});
	
}

/** 关闭窗口 */
function closeAuctionQuickGoodsWin(){
	$('#auctionQuickGoodsWin').window('close');
	//$('#datagrid').datagrid('clearSelections');
	//$('#datagrid').datagrid('clearChecked');
}

/** 提交表单 */
function submitAuctionQuickGoods(){
	//$('#auctionQuickGoodsForm').submit();
	var timeRec = preventRepeat(10, SAVING_FLAG);
	if(timeRec){
		SAVING_FLAG = timeRec;
		$('#auctionQuickGoodsForm').submit();//执行操作
	}else{//重复提交
		return;//可进行提示或其他操作，这里直接返回，即重复提交时没有反应
	}
}

/** 添加用户 */
function addAuctionQuickGoods(){
	openAuctionQuickGoodsWin(-1,'add');
}

/** 打开窗口 */
function openAuctionQuickGoodsWin(index,operation){
	var rows,data;
	if(index >= 0){
		rows = $('#datagrid').datagrid('getRows');
		data = rows[index];//获取该行的数据
	}
	var $form = $('#auctionQuickGoodsForm');
	
	$form.form({
	    url:'',
	    onSubmit: function(){
	       $('#auctionQuickGoodsForm').form('enableValidation');
	       var flag = $('#auctionQuickGoodsForm').form('validate');
	       if(flag){
	       		var id;
	       		if(operation != 'add')id = data.id;
	       		var auctionQuickGoods = getAuctionQuickGoods(id);//得到用户信息的字段值
	       	    $.post('/back/addOrUpdateAuctionQuickGoods',auctionQuickGoods,function(rsp){
					 if(rsp.success){
						 $('#datagrid').datagrid('reload');
						 //$('#datagrid').datagrid('clearSelections');
						 //$('#datagrid').datagrid('clearChecked');
				       	 setTimeout(function(){
				       		closeAuctionQuickGoodsWin();
				       	 },500);
					 }else{
						 $.messager.alert('提示',rsp.msg);
					 }
				},'json');
	       }
	       return false;
	    }
	});
	
	loadCombo();//加载下拉列表数据
	$('#auctionQuickGoodsWin').window('open');
	$form.form('clear');
	$form.form('disableValidation');
	$('#auctionQuickGoodsTip').html('');
	
	var domIds = "#f_userId,#f_auctionInstId,#f_goodsId,#f_priceBegin";
	
	if(operation == 'add'){//添加
		$('#winSearchTr,#winSearchDivisionTr').show();//显示查询按钮
		$('#auctionQuickGoodsSave').show();
		$('#auctionQuickGoodsTable .readOnlyTr').hide();//隐藏只读字段
		$(domIds).textbox('readonly',false);//新增时设置为可编辑
	}else{//查看或修改
		$('#winSearchTr,#winSearchDivisionTr').hide();//隐藏查询按钮
		$('#auctionQuickGoodsTable .readOnlyTr').show();//显示只读字段
		var isReadOnly = false;
		if(operation == 'read'){//查看
			isReadOnly = true;
			$('#auctionQuickGoodsSave').hide();
		}else{//修改
			$('#auctionQuickGoodsSave').show();
		}
		$(domIds).textbox('readonly',isReadOnly);//设置只读还是可编辑
		setAuctionQuickGoods(data);//设置用户详细信息字段值
	}
	
}

/** 得到用户信息的字段值 */
function getAuctionQuickGoods(id){
	var userId = $('#f_userId').combobox('getValue');
	var auctionInstId = $('#f_auctionInstId').combobox('getValue');
	var goodsId = $('#f_goodsId').combobox('getValue');
	var priceBegin = $('#f_priceBegin').textbox('getValue');
	var auctionQuickGoods = {
		userId:userId,
		auctionId:auctionInstId,
		priceBegin:priceBegin,
		goodsId:goodsId
	};
	if(id){//有id为更新，无id为新增
		auctionQuickGoods.id = id;
	}
	return auctionQuickGoods;
}

/** 设置用户信息的字段值 */
function setAuctionQuickGoods(data){
	$('#f_userId').combobox('setValue',data.userId);
	$('#f_priceBegin').textbox('setValue',data.priceBegin);
	$('#f_auctionInstId').combobox('setValue',data.auctionId);
	$('#f_goodsId').combobox('setValue',data.goodsId);
	
	$('#f_auctionQuickGoodsId').val(data.id);//隐藏字段
}

function initSearch(){
	
	$('#sc_user').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
	    url:'/back/getUser'
	});
	$('#sc_auctionInst').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
		required:false,
		panelHeight:'auto',
		url:'/back/getAuctionQuickInst'
	});
	
}


/**根据查询条件查询匹配的数据*/
function doSearch(){
	var  sc_user = $('#sc_user').combobox('getValue');
	var sc_auctionInst = $('#sc_auctionInst').textbox('getValue');
	var sc_goods = $('#sc_goods').textbox('getValue');
	
	var common_queryObj = GRID_QUERYOBJ;
	common_queryObj.userId = sc_user;
	common_queryObj.auctionNameLike = sc_auctionInst;
	common_queryObj.nameLike = sc_goods;
	
	$('#datagrid').datagrid('load',common_queryObj);  
}
/**重置查询查询条件*/
function clearSearch(){
	$('#sc_user').combobox('reset');
	$('#sc_auctionInst').textbox('reset');
	$('#sc_goods').textbox('reset');
}

/** 批量删除用户 */
function batchDelete(){
	commonBatchDelete('/back/updateAuctionQuickGoodsDelete');//common_back:通用批量删除
}
/** 切换到回收站 */
function showTrach(){
	IS_SHOW_TRASH = true;
	commonShowTrash('#batchRecover,#batchThoroughDelete,#returnBack','#batchDelete,#addAuctionQuickGoods,#userInfoLink,#auctionInstLink,#showTrash');
}

/** 从回收站返回 */
function returnBack(){
	IS_SHOW_TRASH = false;
	commonReturnBack('#batchDelete,#addAuctionQuickGoods,#userInfoLink,#auctionInstLink,#showTrash','#batchRecover,#batchThoroughDelete,#returnBack');
}

/** 批量恢复用户 */
function batchRecover(id){
	commonBatchRecover('/back/updateAuctionQuickGoodsRecover',null,id);//common_back:通用批量恢复
}

/** 批量强制删除用户 */
function batchThoroughDelete(id){
	commonBatchThoroughDelete('/back/deleteAuctionQuickGoodsThorough',null,id);//common_back:通用批量强制删除
}
