/** 基础数据和基础设置 */
lh.config = {
	mainObjLowerName : 'auctionProfession',
	mainObjUpperName : 'AuctionProfession',
}

$(function() {
	loadGrid();
	initQueryComponent();
	//initComboData();
});

/**加载用户列表*/
function loadGrid(){
	$('#datagrid').datagrid({
		loadMsg:'',
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		fitColumns:false,
		selectOnCheck:false,
		checkOnSelect:false,
		singleSelect:true,
		striped:true,
		url:lh.config.gridUrl,
	    queryParams:lh.config.queryObj,//查询参数
	    pageSize:lh.grid.pageSize,//每页数据条数
	    pageList:lh.grid.pageList,//每页数据条数选择数组
	    width:lh.dom.clientSafeWidth-1,
	    height:lh.dom.clientHeight-122,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
			{field:'instSerial',title:'',hidden:true},
			{field:'username',title:'',hidden:true},
	        {field:'instName',title:'机构名称',width:200,align:'center',formatter: function(value,row,index){
	        	if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.instSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'instLogo',title:'机构图标',width:140,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:40px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:40px;">暂无图标<span>';
	        	return logo;
	        }},
	        {field:'picPaths',title:'专场图标',width:140,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:40px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:40px;">暂无图标<span>';
	        	return logo;
	        }},
	        {field:'auctionName',title:'专场名称',width:140,align:'center'},
	        {field:'auctionSerial',title:'拍卖场次',width:140,align:'center'},
	        {field:'mainStatus',title:'状态',width:80,align:'center',formatter: function(value,row,index){
	        	var status;
	        	if(value == 1){status = '<span style="color:green">预展中</span>';}
	        	else if(value == 2){status = '<span style="color:#FF6000">正在进行中</span>';}
	        	else if(value == 3){status = '<span style="color:gray">已结束</span>';}
	        	return status;
	        }},
	        {field:'operate',title:'操作',width:200,align:'center',formatter: function(value,row,index){
	        	return '<span class="opt_alive"><span class="opt_cuation" onclick="openMainObjWin('+index+',\'update\')">修改</span>'+//jumpToAuctionInstUpdate
     		   ' |  <span class="opt_green" onclick="openMainObjWin('+index+',\'read\')">查看</span></span>'+//jumpToAuctionInst
    		   '<span class="opt_trash"><span class="opt_cuation"  onclick="lh.commonBatchThoroughDelete('+row.id+')">彻底删除</span>'+
    		   ' |  <span class="opt_green" onclick="lh.commonBatchRecover('+row.id+')">恢复</span></span>';
	        }},
	        {field:'startTime',title:'拍卖开始时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDateTime(value);
	        }},
	        {field:'endTime',title:'拍卖结束时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDateTime(value);
	        }},
	        {field:'joinNum',title:'参拍人数（人）',width:100,align:'center'},
	        {field:'visitNum',title:'围观次数（次）',width:100,align:'center'},
	        {field:'bail',title:'保证金（元）',width:100,align:'center'},
	        {field:'spreadPackets',title:'推广红包（元）',width:100,align:'center'},
	        {field:'createdAt',title:'添加时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value)
	        }},
	        {field:'createdBy',title:'添加人',width:120,align:'center'},
	        {field:'updatedAt',title:'修改时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value);
	        }},
	        {field:'updatedBy',title:'修改人',width:120,align:'center'},
	        {field:'deletedAt',title:'删除时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value);
	        }},
	        {field:'deletedBy',title:'删除人',width:120,align:'center'}
	        ]],
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
	        	lh.onGridLoadError(data);
		    },
		    onDblClickRow: function(index, row){
		    	lh.onGridDblClickRow(index, row);
	        },
		    onLoadSuccess:function(data){
		    	lh.onGridLoadSuccess(data);
		   }  
		});
	lh.clipboard();//复制功能
}

/** 初始化表单中的组件及数据 */
function initFormComponent(){
	
	initUploadSimple({showEdBtns:true,showItemDiv:true,multiFlag:false,multiReplace:true,
		successFun:function(fileId, filePath){
			$("#upld_container_"+fileId).remove();
			$("#pic").attr('src', filePath);
	}});
	$("#upload_outer_div").empty();
	
	
	$('#f_parentCode').combobox({
		valueField : 'code',
		textField : 'codeName',
		editable : false,
		multiple : false,
	    required : true,
	    panelHeight : 200,
	    data:lh.getDataFromCache('rootDictArray')
	    //url:'/back/getRootDictArrayForExpand'
	});
}

/** 初始化查询条件中的组件及数据 */
function initQueryComponent(){
	$('#sc_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '预展中'
		},{
			'id' : 2,
			'name' : '正在进行中'
		},{
			'id' : 3,
			'name' : '已结束'
		}]
	
	});
	
	$('#f_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	lh.$grid.datagrid({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '已结束'
		},{
			'id' : 2,
			'name' : '预展中'
		},{
			'id' : 3,
			'name' : '正在进行中'
		}]
	});
	
}
//初始化机构信息
function afterOpenWin(data, operation, isReadOnly){
	if(!data){
		$('#f_mainStatus').combobox('setValue', 1);
		$("#upload_outer_div").empty();
		$("#pic").attr('src', null);
		return;
	}
	$("#pic").attr('src', data.logo);
	$("#filePaths").val(data.logo);
	$("#fileDBIds").val(data.logoPicId);
	
	var rows = lh.$grid.datagrid('getSelected');
	if(data){
	 $("#f_serial").textbox('setValue',data.instSerial);
	 $("#f_username").textbox('setValue',data.username);
	 $("#f_instName").textbox('setValue',data.instName);
	 $("#f_startTime").textbox('setValue',lh.formatGridDateTime(data.startTime));
	 $("#f_endTime").textbox('setValue',lh.formatGridDateTime(data.endTime));
	 }
	 return true;
	}

function preAddOrUpdate(mainObj){
	var filePaths = $("#filePaths").val();
	if(!filePaths){
	}else{
		var ids = UPLOAD_OBJ.idsStr;
		if(filePaths.substring(0,1) != "/"){
			filePaths = filePaths.substring(1);
			ids = ids.substring(1);
		}
		mainObj.picPaths = filePaths;
	}
		var instId=$("#f_instId").attr("value");
		mainObj.startTime += " 00:00:00";
		mainObj.endTime += " 00:00:00";
		mainObj.instId = instId;
		mainObj.userId= $("#f_userId").attr("value");
	return true;
}

/** 查询拍卖机构的拍卖状态数据（检查机构是否有正在进行或预展中的拍卖） */
function getInstAuctionStatus(instId){
	if(!instId)return;
	$.post('/back/getAuctionProfessionList',{instId:instId,endTimeNull:1},function(rsp){
		 if(rsp.success){
			var rows = rsp.rows;
			if(rows){
				var length = rows.length;
				if(length>0){
					$.messager.confirm('提示','该机构还有【'+length+'】场拍卖未结束，是否确定继续添加？',function(r){
						if(!r){
							closeAuctionProfessionWin();
						}
					});
				}
			}
		 }else{
			 $.messager.alert('提示',rsp.msg);
		 }
	},'json');
	
}


function searchInst(){
	lh.searchInstBySerial();//common_back:通用跟据机构编号查询机构
}

/** 跳转到 修改用户 页面 */
/**function jumpToAuctionProfessionUpdate(){}*/
/** 跳转到 用户详情 页面 */
/**function jumpToAuctionProfession(){}*/


/** 跳转：用户信息 */
function jumpToUserInfo(){
	var url = "/back/page/userInfo";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('用户信息', url)
}

/** 跳转：拍卖机构 */
function jumpToAuctionInst(){
	var url = "/back/page/auctionInst";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('拍卖机构', url)
}

/** 跳转：藏品 */
function jumpToGoods(){
	var url = "/back/goods";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('藏品', url)
}

