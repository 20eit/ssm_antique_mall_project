/** 基础数据和基础设置 */
lh.config = {
	mainObjLowerName : 'auctionGoods',
	mainObjUpperName : 'AuctionGoods'
}

$(function() {
	loadGrid();
	initQueryComponent();
	// initComboData();
});

/** 加载用户列表 */
function loadGrid() {
	lh.$grid
			.datagrid({
				loadMsg : '',
				idField : 'id',
				sortName : 'addTime',
				sortOrder : 'DESC',
				fitColumns : false,
				selectOnCheck : false,
				checkOnSelect : false,
				singleSelect : true,
				striped : true,
				url : lh.config.gridUrl,
				queryParams : lh.config.queryObj,// 查询参数
				pageSize : lh.grid.pageSize,// 每页数据条数
				pageList : lh.grid.pageList,// 每页数据条数选择数组
				width : lh.dom.clientSafeWidth - 1,
				height : lh.dom.clientHeight - 122,
				columns : [ [
						{
							field : 'checkbox',
							title : '多选框',
							checkbox : true
						},
						{field : 'id',title : '',hidden : true},
						{field : 'goodsName',title : '商品名称',width : 260,align : 'center',formatter : function(value, row, index) {
								if (!value)return '';var dom = '<span>'+ value+ '</span>'+ '<button class="copy_btn pointer fr" data-clipboard-text="'
										+ row.goodsSn + '">复制编号</button>';return dom;
							}
						},
						{
							field : 'auctionName',
							title : '专场名称',
							width : 260,
							align : 'center',
							formatter : function(value, row, index) {
								if (!value)
									return '';
								var dom = '<span>'+ value+ '</span>'+ '<button class="copy_btn pointer fr" data-clipboard-text="'
										+ row.auctionInstSerial+ '">复制编号</button>';
								return dom;
							}
						},
						{field : 'picPath',title : '商品图片',width : 140,align : 'center',
							formatter : function(value, row, index) {var logo = '<a href="'+ value
										+ '" target="_blank"><img style="height:40px;cursor:pointer;" src="'+ value + '"/></a>';
								if (!value)
									logo = '<span style="line-height:40px;">暂无图标<span>';
								return logo;
							}
						},
						{field : 'username',title : '所属用户',width : 120,align : 'center'}
						,
						{field : 'priceBegin',title : '起拍价格',width : 120,align : 'center'}
						,
						{field : 'operate',title : '操作',width : 200,align : 'center',
							formatter : function(value, row, index) {
								return '<span class="opt_alive"><span class="opt_cuation" onclick="openMainObjWin('
										+ index+ ',\'update\')">修改</span>'+ // jumpToAuctionInstUpdate
										' |  <span class="opt_green" onclick="openMainObjWin('+ index+ ',\'read\')">查看</span></span>'+ // jumpToAuctionInst
										'<span class="opt_trash"><span class="opt_cuation"  onclick="lh.commonBatchThoroughDelete('+ row.id+ ')">彻底删除</span>'
										+ ' |  <span class="opt_green" onclick="lh.commonBatchRecover('
										+ row.id + ')">恢复</span></span>';
							}
						}, {field : 'createdBy',title : '添加人',width : 140,align : 'center'
						}, {field : 'updatedAt',title : '修改时间',width : 140,align : 'center',
							formatter : function(value, row, index) {return lh.formatGridDate(value);}
						}, {field : 'updatedBy',title : '修改人',width : 140,align : 'center'
						}, {field : 'deletedAt',title : '删除时间',width : 140,align : 'center',
							formatter : function(value, row, index) {return lh.formatGridDate(value);}
						}, {field : 'deletedBy',title : '删除人',width : 140,align : 'center'
						} ] ],
				/*
				 * ,onSelect:function(rowIndex,
				 * rowData){onClickRowOfGrid(rowIndex, rowData);}, onClickRow:
				 * function(index, row){}, loadFilter: function(data){return
				 * data;}, onLoadSuccess:function(data){}
				 */
				onLoadError : function(data) {
					lh.onGridLoadError(data);
				},
				onDblClickRow : function(index, row) {
					lh.onGridDblClickRow(index, row);
				},
				onLoadSuccess : function(data) {
					lh.onGridLoadSuccess(data);
				}
			});
	lh.clipboard();// 复制功能
}
/** 新增修改操作执行之前的拦截方法，返回false则不执行新增修改，如无对应操作可不用申明此方法 */
//function preAddOrUpdate(mainObj) {
//	var auctionInstId = $("#f_auctionInstId").combobox("getValues");
//	mainObj.auctiontId = auctionInstId;
//	return true;
//}

function initQueryComponent() {

	$('#sc_userId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getusernameArray'
	});

	$('#f_auctionId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getAuctionProssion'
	});
	$('#f_goodsId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getAuctionGoods'
	});

}

/** 初始化表单中的组件及数据 */
function initFormComponent() {

	// if(!lh.hasInit){
	// lh.initUploadSimple({showEdBtns:true,showItemDiv:true,multiFlag:false,multiReplace:true,
	// successFun:function(fileId, filePath){
	// $("#upld_container_"+fileId).remove();
	// $("#picPaths").attr('src', filePath);
	// }});
	// }
	//	
	// $("#upload_outer_div").empty();

	$('#f_userId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getusernameArray'
	});
}

function initSearch() {

	$('#sc_userId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getUser'
	});

	$('#sc_auctionInst').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		url : '/back/getAuctionInst'
	});

}
