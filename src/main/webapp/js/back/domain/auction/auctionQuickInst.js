var HEIGHT = document.documentElement.clientHeight;
var WIDTH = document.documentElement.clientWidth;
if(WIDTH<1000)WIDTH=1000;
IS_SHOW_TRASH = false;
SAVING_FLAG = 0;
$(function() {
	GRID_QUERYOBJ = {};//查询条件
	loadGrid();
	initSearch();
	initUploadSimple({showDelBtn:true});//调用完整方法
	//paramMapData();
});

/**加载用户列表*/
function loadGrid(){
	$('#datagrid').datagrid({
	    url:'/back/getAuctionQuickInstList',
	    pagination:true,//允许分页
		pageSize:20,//每页10条数据
		loadMsg:'',
		width:WIDTH-1,
		height:HEIGHT-125,
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		queryParams:GRID_QUERYOBJ,
		fitColumns:false,
		selectOnCheck:false,
		checkOnSelect:false,
		singleSelect:true,
		striped:true,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
			{field:'serial',title:'机构编号',width:160,align:'center',formatter: function(value,row,index){
				if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+value+'">复制</button>';
	        	return dom;
	        }},
	        {field:'name',title:'机构名称',width:180,align:'center'},
	        {field:'operate',title:'操作',width:100,align:'center',formatter: function(value,row,index){
	        	return '<span class="g_alive"><span class="opt_cuation" onclick="openAuctionQuickInstWin('+index+',\'update\')">修改</span>'+//jumpToAuctionQuickInstUpdate
	        		   ' |  <span class="opt_green" onclick="openAuctionQuickInstWin('+index+',\'read\')">查看</span></span>'+//jumpToAuctionQuickInst
	        		   '<span class="g_trash"><span class="opt_cuation" onclick="batchThoroughDelete('+row.id+')">彻底删除</span>'+
	        		   ' |  <span class="opt_green" onclick="batchRecover('+row.id+')">恢复</span></span>';
	        }},
	        {field:'picPaths',title:'机构图标',width:140,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:40px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:40px;">暂无图标<span>';
	        	return logo;
	        }},
	        {field:'username',title:'所属用户',width:200,align:'center',formatter: function(value,row,index){
	        	if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.userSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'shopName',title:'关联店铺',width:200,align:'center',formatter: function(value,row,index){
	        	if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.shopSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'mainStatus',title:'状态',width:60,align:'center',formatter: function(value,row,index){
	        	var status = "启用";
	        	if(value == 2){status = '<span style="color:orange">停用</span>';}
	        	return status;
	        }},
	        {field:'tel',title:'电话',width:120,align:'center'},
	        {field:'createdAt',title:'注册时间',width:120,align:'center',formatter: function(value,row,index){
	        	return getDateStr(value);
	        }},
	        {field:'updatedAt',title:'修改时间',width:120,align:'center',formatter: function(value,row,index){
	        	return getDateStr(value);
	        }},
	        {field:'updatedBy',title:'修改人',width:120,align:'center'},
	        {field:'deletedAt',title:'删除时间',width:120,align:'center',formatter: function(value,row,index){
	        	return getDateStr(value);
	        }},
	        {field:'deletedBy',title:'删除人',width:120,align:'center'}
	        ]],
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
		    	backDatagridErrorCheck(data);
		    },
		    onDblClickRow: function(index, row){
		    	openAuctionQuickInstWin(index, 'read');
	        },
	    onLoadSuccess:function(data){
	       	if(IS_SHOW_TRASH){
	       		$('.g_alive').hide();
	       		$('.g_trash').show();
	       	}else{
	       		$('.g_alive').show();
	       		$('.g_trash').hide();
	       	}
	    	initClipboard();//复制功能
	   }  
	});
}

/** grid 行点击事件 */
/**function onClickRowOfGrid(rowIndex, rowData){}*/

function loadCombo(){
	
	$('#f_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	
}

/** 关闭窗口 */
function closeAuctionQuickInstWin(){
	$('#auctionQuickInstWin').window('close');
	//$('#datagrid').datagrid('clearSelections');
	//$('#datagrid').datagrid('clearChecked');
}

/** 提交表单 */
function submitAuctionQuickInst(){
	//$('#auctionQuickInstForm').submit();
	var timeRec = preventRepeat(10, SAVING_FLAG);
	if(timeRec){
		SAVING_FLAG = timeRec;
		$('#auctionQuickInstForm').submit();//执行操作
	}else{//重复提交
		return;//可进行提示或其他操作，这里直接返回，即重复提交时没有反应
	}
}

/** 添加用户 */
function addAuctionQuickInst(){
	openAuctionQuickInstWin(-1,'add');
}

/** 打开窗口 */
function openAuctionQuickInstWin(index,operation){
	$("#upload_outer_div").empty();
	var rows,data;
	if(index >= 0){
		rows = $('#datagrid').datagrid('getRows');
		data = rows[index];//获取该行的数据
	}
	var $form = $('#auctionQuickInstForm');
	
	$form.form({
	    url:'',
	    onSubmit: function(){
	       $('#auctionQuickInstForm').form('enableValidation');
	       var flag = $('#auctionQuickInstForm').form('validate');
	       var filePaths = $("#filePaths").val();
			var filePathArr = new Array();
			if(filePaths.indexOf(',') >= 0){
				filePaths = filePaths.substring(1);
			}
			filePathArr = filePaths.split(",");
			if(filePathArr.length >= 2){
				$.messager.alert('提示', '只能上传1张机构图标');
				return;
			}
			//if(!filePaths){$.messager.alert('提示','请上传机构图标');return;}
	       if(flag){
	       		var id;
	       		if(operation != 'add')id = data.id;
	       		var auctionQuickInst = getAuctionQuickInst(id);//得到用户信息的字段值
	       		auctionQuickInst.picPaths = filePaths;
	       	    $.post('/back/addOrUpdateAuctionQuickInst',auctionQuickInst,function(rsp){
					 if(rsp.success){
						 $('#datagrid').datagrid('reload');
						 //$('#datagrid').datagrid('clearSelections');
						 //$('#datagrid').datagrid('clearChecked');
				       	 setTimeout(function(){
				       		closeAuctionQuickInstWin();
				       	 },500);
					 }else{
						 $.messager.alert('提示',rsp.msg);
					 }
				},'json');
	       }
	       return false;
	    }
	});
	
	loadCombo();//加载下拉列表数据（省市）
	$('#auctionQuickInstWin').window('open');
	$form.form('clear');
	$form.form('disableValidation');
	$('#auctionQuickInstTip').html('');
	
	var domIds = "#f_name,#f_tel,#f_picPaths,#f_mainStatus";
	
	if(operation == 'add'){//添加用户
		$('#winSearchTr,#winSearchDivisionTr').show();//显示查询按钮
		$('#auctionQuickInstSave').show();
		$('#auctionQuickInstTable .readOnlyTr').hide();//隐藏只读字段
		$(domIds).textbox('readonly',false);//新增时设置为可编辑
	}else{//查看或修改
		$('#winSearchTr,#winSearchDivisionTr').hide();//隐藏查询按钮
		$('#auctionQuickInstTable .readOnlyTr').show();//显示只读字段
		var isReadOnly = false;
		if(operation == 'read'){//查看
			isReadOnly = true;
			$('#auctionQuickInstSave').hide();
		}else{//修改
			$('#auctionQuickInstSave').show();
		}
		$(domIds).textbox('readonly',isReadOnly);//设置只读还是可编辑
		setAuctionQuickInst(data);//设置用户详细信息字段值
	}
	
}

/** 得到用户信息的字段值 */
function getAuctionQuickInst(id){
	var name = $('#f_name').textbox('getValue');
	var tel = $('#f_tel').textbox('getValue');
	var picPaths = '';//图片LOGO
	var mainStatus = $('#f_mainStatus').combobox('getValue');
	if(mainStatus != 2){
		mainStatus = 1;
	}
	//picPaths = $("#f_picPaths").textbox('getValue');
	var userId = $('#f_userId').val();
	var shopId = $('#f_shopId').val();
	
	var auctionQuickInst = {
		name:name,
		tel:tel,
		userId:userId,
		shopId:shopId,
		mainStatus:mainStatus
		//picPaths:picPaths
	};
	if(id){//有id为更新，无id为新增
		auctionQuickInst.id = id;
	}
	return auctionQuickInst;
}

/** 设置用户信息的字段值 */
function setAuctionQuickInst(data){
	$('#f_serial').textbox('setValue', data.serial);
	$('#f_username').textbox('setValue', data.username);
	$('#f_realName').textbox('setValue', data.realName);
	$('#f_name').textbox('setValue', data.name);
	$('#f_tel').textbox('setValue', data.tel);
	var mainStatus = data.mainStatus;
	if(!mainStatus)mainStatus = 1;
	$('#f_mainStatus').combobox('setValue', mainStatus);
	//图片LOGO设置
	//$('#filePaths').val(data.picPaths);
	//查询产生的只读字段：
	$('#f_serial').textbox('setValue', data.userSerial);
	$('#f_username').textbox('setValue', data.username);
	$('#f_realName').textbox('setValue', data.realName);
	$('#f_userId').val(data.userId);//隐藏字段-用户ID
	$('#f_shopId').val(data.shopId);//隐藏字段-店铺ID
	//以下为只读字段：
}

function search(typeId){
	$('#datagrid').datagrid('load',{
		typeId : typeId
	});
}

function initSearch(){
	
	$('#sc_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	
}


/**根据查询条件查询匹配的数据*/
function doSearch(){
	var sc_name = $('#sc_name').textbox('getValue');
	var sc_username = $('#sc_username').textbox('getValue');
	var sc_tel = $('#sc_tel').textbox('getValue');
	var sc_mainStatus = $('#sc_mainStatus').combobox('getValue');

	if(!sc_mainStatus){sc_mainStatus=""}
	
	var common_queryObj = GRID_QUERYOBJ;
	common_queryObj.name = sc_name;
	common_queryObj.username = sc_username;
	common_queryObj.tel = sc_tel;
	common_queryObj.mainStatus = sc_mainStatus;
	
	$('#datagrid').datagrid('load',common_queryObj);  
}
/**重置查询查询条件*/
function clearSearch(){
	$('#sc_name,#sc_userame,#sc_tel').textbox('reset');
	$('#sc_mainStatus').combobox('reset');
	
}

function searchUser(){
	searchUserBySerial();//common_back:通用跟据用户编号查询用户
}

/** 跳转到 修改用户 页面 */
/**function jumpToAuctionQuickInstUpdate(){}*/
/** 跳转到 用户详情 页面 */
/**function jumpToAuctionQuickInst(){}*/

/** 批量删除用户 */
function batchDelete(){
	commonBatchDelete('/back/updateAuctionQuickInstDelete');//common_back:通用批量删除
}

/** 切换到回收站 */
function showTrach(){
	IS_SHOW_TRASH = true;
	commonShowTrash('#batchRecover,#batchThoroughDelete,#returnBack','#batchDelete,#addAuctionQuickInst,#userInfoLink,#shopLink,#showTrash');
}

/** 从回收站返回 */
function returnBack(){
	IS_SHOW_TRASH = false;
	commonReturnBack('#batchDelete,#addAuctionQuickInst,#userInfoLink,#shopLink,#showTrash','#batchRecover,#batchThoroughDelete,#returnBack');
}

/** 批量恢复用户 */
function batchRecover(id){
	commonBatchRecover('/back/updateAuctionQuickInstRecover',null,id);//common_back:通用批量恢复
}

/** 批量强制删除用户 */
function batchThoroughDelete(id){
	commonBatchThoroughDelete('/back/deleteAuctionQuickInstThorough',null,id);//common_back:通用批量强制删除
}

/** 跳转：用户信息 */
function jumpToUserInfo(){
	var url = "/back/userInfo";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	subShowMain('用户信息', url)
}

/** 跳转：用户控制 */
function jumpToShop(){
	var url = "/back/shop";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	subShowMain('店铺', url)
}

/** 跳转：新增或修改 */
function jumpToAuctionQuickInstAddOrUpdate(id){
	var url = "/back/auctionQuickInstAddOrUpdate";
	if(id){
		url += "?auctionQuickInstId="+id;
	}
	window.location.href = url;
}

