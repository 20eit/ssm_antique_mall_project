/** 基础数据和基础设置 */
lh.config = {
	mainObjLowerName : 'accountLog',
	mainObjUpperName : 'AccountLog'
}

$(function() {
	loadGrid();
	initQueryComponent();
	initComboData();
});


/**加载用户列表*/
function loadGrid(){
	lh.$grid.datagrid({
		loadMsg:'',
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		fitColumns:false,
		selectOnCheck:false,
		checkOnSelect:false,
		singleSelect:true,
		striped:true,
		url:lh.config.gridUrl,
		pagination:true,
	    queryParams:lh.config.queryObj,//查询参数
	    pageSize:lh.grid.pageSize,//每页数据条数
	    pageList:lh.grid.pageList,//每页数据条数选择数组
	    width:lh.dom.clientSafeWidth-1,
	    height:lh.dom.clientHeight-122,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
	        {field:'username',title:'卖方用户名',width:140,align:'center'},
	        {field:'tradeUsername',title:'买方用户名',width:140,align:'center'},
	        {field:'operate',title:'操作',width:200,align:'center',formatter: function(value,row,index){return '<span class="opt_alive"><span class="opt_cuation" onclick="openMainObjWin('+index+',\'update\')">修改</span>'+//jumpToAuctionInstUpdate
     		   ' |  <span class="opt_green" onclick="openMainObjWin('+index+',\'read\')">查看</span></span>'+//jumpToAuctionInst
    		   '<span class="opt_trash"><span class="opt_cuation"  onclick="lh.commonBatchThoroughDelete('+row.id+')">彻底删除</span>'+
    		   ' |  <span class="opt_green" onclick="lh.commonBatchRecover('+row.id+')">恢复</span></span>';
     		}},
     		{field:'email',title:'卖方邮箱',width:120,align:'center'},
     		{field:'tradeUserEmail',title:'买方邮箱',width:120,align:'center'},
     		{field:'phone',title:'卖方电话',width:120,align:'center'},
     		{field:'tradeUserPhone',title:'买方电话',width:120,align:'center'},
     		{field:'discount',title:'折扣',width:120,align:'center'},
     		{field:'money',title:'金额',width:120,align:'center'},
	        {field:'payTime',title:'支付时间',width:100,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value);
	        }},
     		{field:'payName',title:'支付方式',width:120,align:'center'},
     		{field:'avaliableMoney',title:'卖方可用资金',width:120,align:'center'},
     		{field:'frozenMoney',title:'卖方冻结资金',width:120,align:'center'},
     		{field:'userIntegral',title:'积分',width:120,align:'center'},
     		/*{field:'description',title:'描述',width:120,align:'center'},*/
     		{field:'attrStr',title:'是否提现',width:120,align:'center'}
	        ]],
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
	        	lh.onGridLoadError(data);
		    },
		    onDblClickRow: function(index, row){
		    	lh.onGridDblClickRow(index, row);
	        },
		    onLoadSuccess:function(data){
		    	lh.onGridLoadSuccess(data);
		   }  
		});
	lh.clipboard();//复制功能
	
}

/** 初始化下拉列表数据，存入缓存，便于复用 */
function initComboData(){
	lh.loadComboDataToCache({url:'/back/getRootDictArray',cacheName:'rootDictArray', domId:'#sc_parentCode'});
}

/** 初始化查询条件中的组件及数据 */
function initQueryComponent(){
	
	$('#sc_payName').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		data : [{
			'id' : '微信',
			'name' : '微信'
		},{
			'id' : '余额',
			'name' : '余额'
		}]
	});
	
	$('#sc_city').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180
	    /*,onSelect: function(rec){
            var url = '/getCity?provinceId='+rec.id;
            $('#area').combobox('reload', url);
        }*/
	});

	$('#sc_province').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180,
		url:'/getProvince',
		onSelect: function(rec){
			var $city = $('#sc_city');
			$city.combobox('clear');
            var url = '/getCity?provinceId='+rec.id;
            $city.combobox('reload', url);
        }
	});
	
}
//初始化组件
function initFormComponent(){
	
	$('#f_payName').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : '微信',
			'name' : '微信'
		},{
			'id' : '余额',
			'name' : '余额'
		}]
	});
	


}


function search(typeId){
	$('#datagrid').datagrid('load',{
		typeId : typeId
	});
}

/** 新增修改操作执行之前的拦截方法，返回false则不执行新增修改，如无对应操作可不用申明此方法 */
function preAddOrUpdate(mainObj){
	/*mainObj.lastLoginTime+=" 00:00:00";
	mainObj.createdAt+=" 00:00:00";*/
	return true;
}


/** 跳转：用户信息 */
function jumpToUserInfo(){
	var url = "/back/page/userInfo";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('用户信息', url)
}

function afterOpenWin(data, operation, isReadOnly){
	$("#f_username").textbox("readonly","readonly");
	$("#f_tradeUsername").textbox("readonly","readonly");
	$("#f_email").textbox("readonly","readonly");
	$("#f_tradeUserEmail").datetimebox("readonly","readonly");
	$("#f_phone").textbox("readonly","readonly");
	$("#f_tradeUserPhone").textbox("readonly","readonly");
	$("#f_money").textbox("readonly","readonly");
	$("#f_payTime").datetimebox("readonly","readonly");
	/*$("#f_payName").textbox("readonly","readonly");*/
	$("#f_description").textbox("readonly","readonly");
}



