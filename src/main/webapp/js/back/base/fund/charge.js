/** 基础数据和基础设置 */
lh.config = {
	mainObjLowerName : 'charge',
	mainObjUpperName : 'Charge'
}

$(function() {
	loadGrid();
	initQueryComponent();
	initComboData();
});


/**加载用户列表*/
function loadGrid(){
	lh.$grid.datagrid({
		loadMsg:'',
		idField:'id',
		sortName:'addTime',
		sortOrder:'DESC',
		fitColumns:false,
		selectOnCheck:false,
		checkOnSelect:false,
		singleSelect:true,
		striped:true,
		pagination:true,
		url:lh.config.gridUrl,
	    queryParams:lh.config.queryObj,//查询参数
	    pageSize:lh.grid.pageSize,//每页数据条数
	    pageList:lh.grid.pageList,//每页数据条数选择数组
	    width:lh.dom.clientSafeWidth-1,
	    height:lh.dom.clientHeight-122,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
			{field:'userSerial',title:'用户编号',width:160,align:'center',formatter: function(value,row,index){
				if(!value)return '';
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+value+'">复制</button>';
	        	return dom;
	        }},
	        {field:'username',title:'用户名',width:140,align:'center'},
	        {field:'realName',title:'真实姓名',width:140,align:'center'},
	        {field:'operate',title:'操作',width:200,align:'center',formatter: function(value,row,index){return '<span class="opt_alive"><span class="opt_cuation" onclick="openMainObjWin('+index+',\'update\')">修改</span>'+//jumpToAuctionInstUpdate
     		   ' |  <span class="opt_green" onclick="openMainObjWin('+index+',\'read\')">查看</span></span>'+//jumpToAuctionInst
    		   '<span class="opt_trash"><span class="opt_cuation"  onclick="lh.commonBatchThoroughDelete('+row.id+')">彻底删除</span>'+
    		   ' |  <span class="opt_green" onclick="lh.commonBatchRecover('+row.id+')">恢复</span></span>';
     		}},
	        {field:'chargeDate',title:'充值时间',width:100,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value);
	        }},
	        
	        {field:'chargeMoney',title:'充值金额',width:120,align:'center'},
	        {field:'fee',title:'手续费',width:120,align:'center'},
	        {field:'msg',title:'备注消息',width:180,align:'center'},
	        {field:'dealDate',title:'处理时间',width:100,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDate(value);
	        }},
	        {field:'dealStatus',title:'处理状态',width:60,align:'center',formatter: function(value,row,index){
	        	var status = "";
	        	if(value == 1){status = '<span style="color:orange">发起充值</span>';}
	        	if(value == 2){status = '<span style="color:orange">充值成功</span>';}
	        	if(value == 3){status = '<span style="color:orange">取消充值</span>';}
	        	if(value == 4){status = '<span style="color:orange">充值失败</span>';}
	        	return status;
	        }},
	       
	        {field:'payType',title:'付款类型',width:60,align:'center',formatter: function(value,row,index){
	        	var status = "";
	        	if(value == 1){status = '<span style="color:orange">交纳保证金</span>';}
	        	if(value == 2){status = '<span style="color:orange">购买商品</span>';}
	        	return status;
	        }}
	        ]],
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
	        	lh.onGridLoadError(data);
		    },
		    onDblClickRow: function(index, row){
		    	lh.onGridDblClickRow(index, row);
	        },
		    onLoadSuccess:function(data){
		    	lh.onGridLoadSuccess(data);
		   }  
		});
	lh.clipboard();//复制功能
	
}

/** 初始化下拉列表数据，存入缓存，便于复用 */
function initComboData(){
	lh.loadComboDataToCache({url:'/back/getRootDictArray',cacheName:'rootDictArray', domId:'#sc_parentCode'});
}

/** 初始化查询条件中的组件及数据 */
function initQueryComponent(){
	
	$('#sc_dealStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		data : [{
			'id' : 1,
			'name' : '发起充值'
		},{
			'id' : 2,
			'name' : '充值成功'
		},{
			'id' : 3,
			'name' : '取消充值'
		},{
			'id' : 4,
			'name' : '充值失败'
		}]
	});
	
	$('#sc_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	
	$('#sc_isRealAuth').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '未认证'
		},{
			'id' : 2,
			'name' : '已认证'
		}]
	});
	
	$('#sc_city').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180
	    /*,onSelect: function(rec){
            var url = '/getCity?provinceId='+rec.id;
            $('#area').combobox('reload', url);
        }*/
	});

	$('#sc_province').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180,
		url:'/getProvince',
		onSelect: function(rec){
			var $city = $('#sc_city');
			$city.combobox('clear');
            var url = '/getCity?provinceId='+rec.id;
            $city.combobox('reload', url);
        }
	});
	
}
//初始化组件
function initFormComponent(){

	$('#f_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	
	$('#f_isRealAuth').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '未认证'
		},{
			'id' : 2,
			'name' : '已认证'
		}]
	});
	
	$('#f_city').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180
	    /*,onSelect: function(rec){
            var url = '/getCity?provinceId='+rec.id;
            $('#area').combobox('reload', url);
        }*/
	});
$('#f_dealStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		data : [{
			'id' : 1,
			'name' : '发起充值'
		},{
			'id' : 2,
			'name' : '充值成功'
		},{
			'id' : 3,
			'name' : '取消充值'
		},{
			'id' : 4,
			'name' : '充值失败'
		}]
	});
$('#f_payType').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple : false,
		required : false,
		panelHeight : 'auto',
		data : [{
			'id' : 1,
			'name' : '交纳保证金'
		},{
			'id' : 2,
			'name' : '购买商品'
		}]
	});
	$('#f_province').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight: 180,
		url:'/getProvince',
		onSelect: function(rec){
			var $city = $('#f_city');
			$city.combobox('clear');
            var url = '/getCity?provinceId='+rec.id;
            $city.combobox('reload', url);
        }
	});
}


function search(typeId){
	$('#datagrid').datagrid('load',{
		typeId : typeId
	});
}

/** 新增修改操作执行之前的拦截方法，返回false则不执行新增修改，如无对应操作可不用申明此方法 */
function preAddOrUpdate(mainObj){
	/*mainObj.lastLoginTime+=" 00:00:00";
	mainObj.createdAt+=" 00:00:00";*/
	return true;
}

/** 跳转：用户资金 *//*
function jumpToUserMoney(){
	var url = "/back/page/userFund";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('用户资金', url)
}

*//** 跳转：用户控制 *//*
function jumpToUserControl(){
	var url = "/back/page/userControl";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('用户控制', url)
}*/
/** 跳转：用户信息 */
function jumpToUserInfo(){
	var url = "/back/page/userInfo";
	var id = $('#userId').val();
	if(id){
		url += "?userId="+id;
	}
	lh.subShowMain('用户信息', url)
}


function afterOpenWin(data, operation, isReadOnly){
	$("#f_userSerial").textbox("readonly","readonly");
	$("#f_username").textbox("readonly","readonly");
	$("#f_realName").textbox("readonly","readonly");
	$("#f_chargeDate").datetimebox("readonly","readonly");
	$("#f_chargeMoney").textbox("readonly","readonly");
	$("#f_fee").textbox("readonly","readonly");
	$("#f_msg").textbox("readonly","readonly");
	$("#f_payType").textbox("readonly","readonly");
}


