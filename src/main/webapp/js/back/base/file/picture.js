/** 基础数据和基础设置 */
lh.config = {
	mainObjLowerName : 'picture',
	mainObjUpperName : 'Picture'
}
var HEIGHT = document.documentElement.clientHeight;
var WIDTH = document.documentElement.clientWidth;
if(WIDTH<1000)WIDTH=1000;
IS_SHOW_TRASH = false;
CURRENT_PIC_TYPE = 0;
SAVING_FLAG = 0;
$(function() {
//	lh.GRID_QUERYOBJ = {typeIdISNOTNULL:1};//查询条件
	loadGrid();
//	initSearch();
//	paramMapData();
});

/**加载用户列表*/
function loadGrid(){
	lh.$grid.datagrid({
	    loadMsg:'',
		idField:'id',
		sortName:'id',
		sortOrder:'desc',
		striped:true,
		fitColumns:false,
		singleSelect:true,
		selectOnCheck:false,
		checkOnSelect:false,
		pagination:true,
		url:lh.config.gridUrl,
	    queryParams:lh.config.queryObj,//查询参数
	    pageSize:6,//每页数据条数
	    pageList:[2,4,6,8,10],//每页数据条数选择数组
	    width:lh.dom.clientSafeWidth-1,
	    height:lh.dom.clientHeight-122,
	    columns:[
		[
			{field:'checkbox',title:'多选框',checkbox:true},
			{field:'id',title:'',hidden:true},
			{field:'operate',title:'操作',width:120,align:'center',formatter: function(value,row,index){
				return  '<span class="opt_alive"><span style="cursor: pointer;color: #EC4949" onclick="openMainObjWin('+index+',\'update\')">修改</span>'
				+'&nbsp;|&nbsp;<span class="update" style="cursor: pointer;color: green" onclick="openMainObjWin('+index+',\'read\')">查看</span></span>'
				+'<span class="opt_trash"><span style="cursor: pointer;color: #EC4949;" onclick="lh.commonBatchThoroughDelete('+row.id+')">彻底删除</span>'
				+'&nbsp;|&nbsp;<span style="cursor: pointer;color: green" onclick="lh.commonBatchRecover('+row.id+')">恢复</span></span>';
			}},
			{field:'serial',title:'图片编号',width:160,align:'center',formatter: function(value,row,index){
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+value+'">复制</button>';
	        	return dom;
	        }},
	        {field:'title',title:'图片名称',width:180,align:'center'},
	        {field:'picPath',title:'图片展示',width:200,align:'center',formatter: function(value,row,index){
	        	var logo = '<a href="'+value+'" target="_blank"><img style="height:60px;cursor:pointer;" src="'+value+'"/></a>';
	        	if(!value)logo = '<span style="line-height:60px;">暂无图片<span>';
	        	return logo;
	        }},
	        {field:'typeId',title:'图片类型',width:120,align:'center',formatter: function(value,row,index){
	        	var status = "普通图片";
	        	if(value == 82){status = '<span style="color:blue">商品图片</span>';}
	        	if(value == 83){status = '<span style="color:blue">用户头像</span>';}
	        	if(value == 84){status = '<span style="color:blue">店铺图标</span>';}
	        	if(value == 85){status = '<span style="color:blue">机构图标</span>';}
	        	if(value == 86){status = '<span style="color:blue">资讯图片</span>';}
	        	if(value == 87){status = '<span style="color:blue">系统图片</span>';}
	        	if(value == 88){status = '<span style="color:blue">广告图片</span>';}
	        	if(value == 89){status = '<span style="color:blue">论坛图片</span>';}
	        	return status;
	        }},
//	        {field:'typeName',title:'图片类型',width:120,align:'center'},
	       /* {field:'albumName',title:'所属相册',width:120,align:'center'},*/
	        /*{field:'username',title:'所属用户',width:200,align:'center',formatter: function(value,row,index){
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.userSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'shopName',title:'关联店铺',width:200,align:'center',formatter: function(value,row,index){
				var dom = '<span>'+value+'</span>' +
				'<button class="copy_btn pointer fr" data-clipboard-text="'+row.shopSerial+'">复制编号</button>';
	        	return dom;
	        }},
	        {field:'goodsName',title:'关联商品',width:120,align:'center'},*/
	        {field:'createdAt',title:'添加时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDateTime(value);
	        }},
	        {field:'createdBy',title:'添加人',width:120,align:'center'},
	        {field:'updatedAt',title:'修改时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDateTime(value);
	        }},
	        {field:'updatedBy',title:'修改人',width:120,align:'center'},
	        {field:'deletedAt',title:'删除时间',width:120,align:'center',formatter: function(value,row,index){
	        	return lh.formatGridDateTime(value);
	        }},
	        {field:'deletedBy',title:'删除人',width:120,align:'center'}
	        ]],
////		url:'/back/getPictureList',
//			url:lh.config.gridUrl,
//		    pagination:true,//允许分页
//			pageSize:20,//每页10条数据
//			loadMsg:'',
//			width:WIDTH-1,
//			height:HEIGHT-125,
//			idField:'id',
//			sortName:'id',
//			sortOrder:'DESC',
//			
//			queryParams:lh.config.queryObj,//查询参数	
//			fitColumns:false,
//			selectOnCheck:false,
//			checkOnSelect:false,
//			singleSelect:true,
//			striped:true,
//	        {field:'operate',title:'操作',width:100,align:'center',formatter: function(value,row,index){
//	    	return '<span class="g_alive"><span class="opt_cuation" onclick="openPictureWin('+index+',\'update\')">修改</span>'+//jumpToPictureUpdate
//	    		   ' |  <span class="opt_green" onclick="openPictureWin('+index+',\'read\')">查看</span></span>'+//jumpToPicture
//	    		   '<span class="g_trash"><span class="opt_cuation" onclick="batchThoroughDelete('+row.id+')">彻底删除</span>'+
//	    		   ' |  <span class="opt_green" onclick="batchRecover('+row.id+')">恢复</span></span>';
	    /*,onSelect:function(rowIndex, rowData){onClickRowOfGrid(rowIndex, rowData);},
	    onClickRow: function(index, row){},
	    loadFilter: function(data){return data;},
		onLoadSuccess:function(data){} */
	        onLoadError: function(data){
		    	lh.backDatagridErrorCheck(data);
		    },
		    onDblClickRow: function(index, row){
		    	openMainObjWin(index, 'read');
	        },
		    onLoadSuccess:function(data){
		    	lh.filtGridOperation();
		    	lh.clipboard();//复制功能
		   },
//	    	initClipboard();//复制功能 
	});
}





/** 初始化查询条件中的组件及数据 */
function initQueryComponent(){
	
    //根据Typeid进行显示选择录入图片信息6
	$('#sc_typeId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
	    data : [{
			'id' : 81,
			'name' : '普通图片'
		},{
			'id' : 82,
			'name' : '商品图片'
		},{
			'id' : 83,
			'name' : '用户头像'
		},{
			'id' : 84,
			'name' : '店铺图标'
		},{
			'id' : 85,
			'name' : '机构图标'
		},{
			'id' : 86,
			'name' : '资讯图片'
		},{
			'id' : 87,
			'name' : '系统图片'
		},{
			'id' : 88,
			'name' : '广告图片'
		},{
			'id' : 89,
			'name' : '论坛图片'
		}]
	});
	
	$('#sc_mainStatus').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
		data : [{
			'id' : 1,
			'name' : '启用'
		},{
			'id' : 2,
			'name' : '停用'
		}]
	});
	
}






/** 新增修改操作执行之前的拦截方法，返回false则不执行新增修改，如无对应操作可不用申明此方法 */
function preAddOrUpdate(mainObj){
	var filePaths = $("#filePaths").val();
	if(!filePaths){
		//$.messager.alert('提示',"请上传用户头像"); return;
	}else{
		var ids = UPLOAD_OBJ.idsStr;
		if(filePaths.substring(0,1) != "/"){
			filePaths = filePaths.substring(1);
			ids = ids.substring(1);
		}
		mainObj.picPath = filePaths;
//		mainObj.logoPicId = ids;
	}
	return true;
}


function afterOpenWin(data, operation, isReadOnly){
	if(!data)return;
	$("#pic").attr('src', data.picPath);
//	$("#filePaths").val(data.picPath);
//	$("#fileDBIds").val(data.avatarPicId);
}
function initFormComponent(){
	if(!UPLOAD_OBJ.hasInit){
		initUploadSimple({showEdBtns:true,showItemDiv:true,multiFlag:false,multiReplace:true,
			successFun:function(fileId, filePath){
				$("#upld_container_"+fileId).remove();
				$("#pic").attr('src', filePath);
		}});
	}
    //根据Typeid进行显示选择录入图片信息6
	$('#f_typeId').combobox({
		valueField : 'id',
		textField : 'name',
		editable : false,
		multiple:false,
	    required:false,
	    panelHeight:'auto',
	    data : [{
			'id' : 81,
			'name' : '普通图片'
		},{
			'id' : 82,
			'name' : '商品图片'
		},{
			'id' : 83,
			'name' : '用户头像'
		},{
			'id' : 84,
			'name' : '店铺图标'
		},{
			'id' : 85,
			'name' : '机构图标'
		},{
			'id' : 86,
			'name' : '资讯图片'
		},{
			'id' : 87,
			'name' : '系统图片'
		},{
			'id' : 88,
			'name' : '广告图片'
		},{
			'id' : 89,
			'name' : '论坛图片'
		}]
	});
}




