 //拨打页

var callPageFun = function(){
	var that = this;
	var $thisBox = $('#callPage');
	var $numView = $('#numView',$thisBox);
	// 数字键
	$('.numKey',$thisBox).click(function(){
	    var thisNum = $('h1',this).html();
	    $numView.append(thisNum);
	});
	// 退格键
	$('.backBtn',$thisBox).click(function(){
	    var thisViewValue = $numView.html();
	    $numView.html(  thisViewValue.substr(0,thisViewValue.length-1) );
	});
	// 清除键
	$('.call_clear_num',$thisBox).click(function(){
	    var thisViewValue = $numView.html();
	    $numView.html('');
	});
}

function showCallActionSheet(param){
	if(!param)param = {};
	if(!param.opt)param.opt = 'pay';
	if(param.opt == 'amBid'){
		$('#call_current_price').text(param.price);
		$('#numView').text(param.nextPrice || '');
		// param.buyoutPrice = 200;
		if(param.buyout){
			$('#call_opt_bid3').show();
			$('#buyoutPrice').text(param.buyoutPrice);
			$('#call_opt_bid3').css('width','40%').show();
			$('#call_opt_bid4').css('width','55%');
		}else{
			$('#call_opt_bid4').css({width:'100%',marginLeft:"0%"});
			$('#call_opt_bid3').hide();
		}
	}else if(param.opt == 'pay'){
	}else if(param.opt == 'charge'){
	}else if(param.opt == 'professionBid'){
		$('#call_current_price').text(param.price);
		$('#numView').text(param.nextPrice || '');
	}
	
	var mask = $('#call_mask');
    var weuiActionsheet = $('#call_weui_actionsheet');
    weuiActionsheet.addClass('weui_actionsheet_toggle');//页面打开显示键盘
    mask.show().addClass('weui_fade_toggle').click(function () {//页面打开隐藏
    	hideCallActionSheet(weuiActionsheet, mask);
    });
    $('#call_actionsheet_cancel').click(function () {
    	hideCallActionSheet(weuiActionsheet, mask);
    });
    weuiActionsheet.unbind('transitionend').unbind('webkitTransitionEnd');
}

function hideCallActionSheet(weuiActionsheet, mask) {
	if(!weuiActionsheet)weuiActionsheet = $('#call_weui_actionsheet');
	if(!mask)mask = $('#call_mask');
	weuiActionsheet.removeClass('weui_actionsheet_toggle');
	mask.removeClass('weui_fade_toggle');
	weuiActionsheet.on('transitionend', function () {
		mask.hide();//自动关闭背景模层
	}).on('webkitTransitionEnd', function () {
		mask.hide();
	})
}


