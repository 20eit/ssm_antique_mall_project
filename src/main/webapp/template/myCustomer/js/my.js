$(function(){
    //页面滚动时隐藏导航菜单
    $(window).bind('scrollstart', function(){
        $('.performance_fixed').addClass('hide');
    });
    $(window).bind('scrollstop', function(e){
        $('.performance_fixed').removeClass('hide');
    });
    //点击发布按钮后的操作
    $("#release").on("click",function(){
        $(".release_click,.closeX").show();
    });
    $("#closeX").on("click",function(){
        $(".release_click,.closeX").hide();
    });
    //出价位置的加价按钮
    $(".partakeAdd").on("click",function(){
        var price = $(this).prev().html();
        var addNum = 100;
        $(this).prev().html((Digit.round(price, 2)*1000000 + Digit.round(addNum, 2)*1000000)/1000000);
    });
    $(".partakeSub").on("click",function(){
        var price = $(this).next().html();
        var subNum = 100;
        $(this).next().html((Digit.round(price, 2)*1000000 - Digit.round(subNum, 2)*1000000)/1000000);
    });
    $(".my-distribution").on("click",function(){
        $(".distributionHide").hide();
        $(this).next().show("500");
    })
    /*我的圈子点击发布分享*/
    $(".myCircleIndex").on("click",function(){
        $(".releaseShare_bg").show();
        $(".releaseShare").show();
    });
    $("#cirleCancel,.releaseShare_bg").on("click",function(){
        $(".releaseShare_bg").hide();
        $(".releaseShare").hide();
    });
});




